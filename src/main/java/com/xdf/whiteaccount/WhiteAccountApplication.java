package com.xdf.whiteaccount;

import com.xdf.whiteaccount.config.ShiroConfig;
import com.xdf.whiteaccount.config.datasource.DynamicDataSource;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.config.ShiroAnnotationProcessorConfiguration;
import org.apache.shiro.spring.config.ShiroBeanConfiguration;
import org.apache.shiro.spring.config.ShiroConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableCaching
@EnableAspectJAutoProxy
@EnableSwagger2
@EnableAsync
@EnableScheduling
public class WhiteAccountApplication {
    public static void main(String[] args) {
        SpringApplication.run(WhiteAccountApplication.class, args);
    }
}