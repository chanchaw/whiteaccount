package com.xdf.whiteaccount.aop;

import com.xdf.whiteaccount.service.base.BaseService;
import org.apache.commons.lang3.ArrayUtils;
import org.aspectj.lang.JoinPoint;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-05-19 17:28
 **/
public class AbstractAop extends BaseService {
    protected Object getParam1(JoinPoint joinPoint) {
        Object[] param = joinPoint.getArgs();
        if (ArrayUtils.isEmpty(param)) return null;
        return param[0];
    }

    protected Object getParam2(JoinPoint joinPoint) {
        Object[] param = joinPoint.getArgs();
        if (ArrayUtils.isEmpty(param) || param.length <= 1) return null;
        return param[1];
    }
}
