package com.xdf.whiteaccount.aop;

import com.xdf.whiteaccount.dao.DyeFabricInboundMapper;
import com.xdf.whiteaccount.entity.DyeFabricInbound;
import com.xdf.whiteaccount.enums.PriceStandard;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.service.CallService;
import com.xdf.whiteaccount.utils.CalculateUtil;
import com.xdf.whiteaccount.utils.Example;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Optional;

/**
 * @program: white-account
 * @description: 色布入库
 * @author: 张柯
 * @create: 2021-07-20 14:46
 **/
@Component
@Aspect
@Slf4j
public class DyeFabricInboundServiceAspect extends AbstractAop {
    @Autowired
    private DyeFabricInboundMapper dyeFabricInboundMapper;
    @Autowired
    private CallService callService;

    @Pointcut("execution(* com.xdf.whiteaccount.service.DyeFabricInboundService.insert*(..))")
    private void insertCut() {
    }

    @Pointcut("execution(* com.xdf.whiteaccount.service.DyeFabricInboundService.update*(..))")
    private void updateCut() {
    }

    @Pointcut("execution(* com.xdf.whiteaccount.service.DyeFabricInboundService.cancelByPrimaryKey(..))")
    private void cancelCut() {
    }

    /**
     * 新增
     *
     * @param joinPoint
     */
    @Before("insertCut()")
    @Transactional(rollbackFor = Exception.class)
    public void beforeInsert(JoinPoint joinPoint) throws Exception {
        Object param = getParam1(joinPoint);
        if (param instanceof DyeFabricInbound) {
            DyeFabricInbound record = (DyeFabricInbound) param;
            record.setOrderCode(callService.dyeFabricInboundCodeGen(false));
            record.setState(1);
            record.setCreateUserId(getLoginUserId());
            if (record.getIsSendDirectly() == null) {
                record.setIsSendDirectly(false);
            }
            record.setLinkedOrderCode(null);
            //  动态计算金额
            PriceStandard e = PriceStandard.match(record.getPriceStandard());
            Assert.notNull(e, "计价标准必须输入！");
            switch (e) {
                case KILO:
                case EXPRESS_METER:
                    record.setMoney(CalculateUtil.mul(record.getPrice(), record.getInputKilo()));
                    break;
                case NET_KILO:
                    record.setMoney(CalculateUtil.mul(record.getPrice(), record.getNetKilo()));
                    break;
                case REAL_METER:
                    record.setMoney(CalculateUtil.mul(record.getPrice(), record.getInputMeter()));
                    break;
            }
        }
    }

    /**
     * 作废
     *
     * @param joinPoint
     */
    @Before("cancelCut()")
    @Transactional(rollbackFor = Exception.class)
    public void beforeCancel(JoinPoint joinPoint) {
        Object param = getParam1(joinPoint);
        if (param instanceof Long) {
            DyeFabricInbound record = dyeFabricInboundMapper.selectByPrimaryKey((Long) param);
            Assert.notNull(record, ResponseEnum.CANCEL_ORDER.getName());
            Assert.state(!Optional.ofNullable(record).map(DyeFabricInbound::getIsAudit).orElse(false), ResponseEnum.IS_AUDIT.getName());
        }
    }

    /**
     * 修改
     *
     * @param joinPoint
     */
    @Before("updateCut()")
    @Transactional(rollbackFor = Exception.class)
    public void beforeUpdate(JoinPoint joinPoint) throws Exception {
        Object param = getParam1(joinPoint);
        if (param instanceof DyeFabricInbound) {
            DyeFabricInbound record = (DyeFabricInbound) param;
            DyeFabricInbound prev = dyeFabricInboundMapper.selectByPrimaryKey(record.getId());
            Assert.notNull(prev, ResponseEnum.NO_ORDER.getName());
            Assert.state(!Optional.ofNullable(record).map(DyeFabricInbound::getIsAudit).orElse(false), ResponseEnum.IS_AUDIT.getName());
            record.setIsSendDirectly(prev.getIsSendDirectly());
            if (record.getIsSendDirectly()) {
                //  直发
                log.debug("当前类型为直发，单据编号：{}", prev.getOrderCode());
                DyeFabricInbound modified = (DyeFabricInbound) record.clone();
                modified.setOrderCode(null);
                modified.setId(null);
                modified.setLinkedOrderCode(null);
                if (StringUtils.isEmpty(prev.getLinkedOrderCode())) {
                    dyeFabricInboundMapper.updateByExampleSelective(modified, new Example().andEq("linked_order_code", prev.getOrderCode()));
                } else {
                    dyeFabricInboundMapper.updateByExampleSelective(modified, new Example().andEq("order_code", prev.getLinkedOrderCode()));
                }
            }
        }
    }
}
