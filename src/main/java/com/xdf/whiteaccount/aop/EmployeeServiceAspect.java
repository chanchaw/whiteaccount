package com.xdf.whiteaccount.aop;

import com.xdf.whiteaccount.dao.EmployeeMapper;
import com.xdf.whiteaccount.entity.Employee;
import com.xdf.whiteaccount.utils.Example;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @program: white-account
 * @description: 部门员工切面
 * @author: 张柯
 * @create: 2021-05-20 13:57
 **/
@Component
@Slf4j
@Aspect
public class EmployeeServiceAspect extends AbstractAop {
    @Autowired
    private EmployeeMapper employeeMapper;

    @Pointcut("execution(* com.xdf.whiteaccount.service.EmployeeService.insert*(..))")
    public void insert() {
    }

    /**
     * 部门员工切点
     *
     * @param joinPoint
     */
    @Before("insert()")
    public void beforeInsert(JoinPoint joinPoint) {
        Object param = getParam1(joinPoint);
        if (param != null && param instanceof Employee) {
            Employee e = (Employee) param;
            Assert.state(StringUtils.isNotEmpty(e.getEmpName()), "名称必须输入！");
            e.setEmpName(StringUtils.trim(e.getEmpName()));
            e.setCreateUserId(getLoginUserId());
            Assert.state(employeeMapper.countByExample(new Example().andEq("emp_name", e.getEmpName())) <= 0, "名称不允许重复！");
            e.setState(1);
        }
    }
}