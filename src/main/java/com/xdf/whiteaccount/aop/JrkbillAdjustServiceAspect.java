package com.xdf.whiteaccount.aop;

import com.xdf.whiteaccount.entity.JrkbillAdjust;
import com.xdf.whiteaccount.entity.Plan;
import com.xdf.whiteaccount.enums.ModifyTypeEnum;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.service.PlanService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-05 08:53
 **/
@Component
@Slf4j
@Aspect
public class JrkbillAdjustServiceAspect extends AbstractAop {
    @Autowired
    private PlanService planService;

    @Pointcut("execution(* com.xdf.whiteaccount.service.JrkbillAdjustService.insert*(..))")
    public void insert() {
    }

    @Pointcut("execution(* com.xdf.whiteaccount.service.JrkbillAdjustService.update*(..))")
    public void update() {
    }

    @Pointcut("execution(* com.xdf.whiteaccount.service.JrkbillAdjustService.insert*(..))")
    public void cancel() {
    }

    /**
     * 新增前
     */
    @Before("insert()")
    public void beforeInsert(JoinPoint joinPoint) throws Exception {
        Object param = getParam1(joinPoint);
        Assert.notNull(param, ResponseEnum.NOT_ALLOW_EMPTY_DATA.getName());
        if (param instanceof JrkbillAdjust) {
            JrkbillAdjust record = (JrkbillAdjust) param;
            Plan plan = planService.selectByPrimaryKey(record.getPlanId());
            Assert.notNull(plan, "计划单不存在！");
            Assert.state(1 == plan.getState(), "计划单已作废！");
            Assert.state(plan.getMarkFinished() == null || !plan.getMarkFinished(), "计划已完工,无法新增调整！");
            ModifyTypeEnum e = ModifyTypeEnum.match(record.getBillType());
            Assert.notNull(e, "类型错误！");
            if (ModifyTypeEnum.OUT == e) {
                //  出库则修改数量
                record.setPairs(Math.abs(record.getPairs()));
                record.setQty(record.getQty().abs());
            }
            record.setState(1);
            record.setUpload(0);
            record.setCreateUserId(getLoginUserId());
            record.setCreateTime(new Date());
        }
    }

    /**
     * 修改前
     *
     * @param joinPoint
     */
    @Before("update()")
    public void beforeUpdate(JoinPoint joinPoint) throws Exception {
        Object param = getParam1(joinPoint);
        Assert.notNull(param, ResponseEnum.NOT_ALLOW_EMPTY_DATA.getName());
        if (param instanceof JrkbillAdjust) {
            JrkbillAdjust record = (JrkbillAdjust) param;
            Assert.notNull(record.getId(), ResponseEnum.NOT_ALLOW_EMPTY_DATA.getName());
            Plan plan = planService.selectByPrimaryKey(record.getPlanId());
            Assert.notNull(plan, "计划单不存在！");
            Assert.state(1 == plan.getState(), "计划单已作废！");
            Assert.state(plan.getMarkFinished() == null || !plan.getMarkFinished(), "计划已完工,无法新增调整！");
            ModifyTypeEnum e = ModifyTypeEnum.match(record.getBillType());
            Assert.notNull(e, "类型错误！");
            if (ModifyTypeEnum.OUT == e) {
                //  出库则修改数量
                record.setPairs(Math.abs(record.getPairs()));
                record.setQty(record.getQty().abs());
            }
            record.setModifyUserId(getLoginUserId());
            record.setUpload(0);
            record.setModifyTime(new Date());
        }
    }
}
