package com.xdf.whiteaccount.aop;

import com.xdf.whiteaccount.entity.MaterialAccountPayable;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-07-02 12:10
 **/
@Component
@Aspect
@Slf4j
public class MaterialAccountPayableServiceAspect extends AbstractAop {
    @Pointcut("execution(* com.xdf.whiteaccount.service.MaterialAccountPayableService.insert*(..))")
    private void cutInsert() {
    }

    @Pointcut("execution(* com.xdf.whiteaccount.service.MaterialAccountPayableService.update*(..))")
    private void cutUpdate() {
    }

    /**
     * 新增前事件
     *
     * @param joinPoint
     */
    @Transactional(rollbackFor = Exception.class)
    @Before("cutInsert()")
    public void beforeInsert(JoinPoint joinPoint) {
        Object param = getParam1(joinPoint);
        if (param instanceof MaterialAccountPayable) {
            MaterialAccountPayable record = (MaterialAccountPayable) param;
            Assert.notNull(record.getBillDate(), "日期不能为空!");
            record.setUpload(0);
            record.setIsPush(false);
        }
    }

    /**
     * 修改前去除标记
     * @param joinPoint
     */
    @Before("cutUpdate()")
    public void beforeUpdate(JoinPoint joinPoint) {
        Object param = getParam1(joinPoint);
        if (param instanceof MaterialAccountPayable) {
            MaterialAccountPayable record = (MaterialAccountPayable) param;
            record.setUpload(0);
        }
    }
}
