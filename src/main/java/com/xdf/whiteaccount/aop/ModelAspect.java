package com.xdf.whiteaccount.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

/**
 * @program: white-account
 * @description: 模型
 * @author: 张柯
 * @create: 2021-06-01 17:44
 **/
@Aspect
@Component
@Slf4j
public class ModelAspect extends AbstractAop {
    @Value("${reportJavascriptURL}")
    private String reportURL;
    private static final String REPORT_SERVER_URL = "REPORT_SERVER_URL";

    @Pointcut("execution(* com.xdf.whiteaccount.controller.RouterController.*(..))")
    public void cut() {
    }

    /**
     * 执行前事件
     *
     * @param joinPoint
     */
    @Before("cut()")
    public void beforeExe(JoinPoint joinPoint) {
        Object param = getParam1(joinPoint);
        if (param instanceof Model) {
            ((Model) param).addAttribute(REPORT_SERVER_URL, reportURL);
        }
    }
}
