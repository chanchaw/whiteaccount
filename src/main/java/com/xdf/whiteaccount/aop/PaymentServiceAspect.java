package com.xdf.whiteaccount.aop;

import com.xdf.whiteaccount.entity.Payment;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @program: white-account
 * @description: 付款业务层切面
 * @author: 张柯
 * @create: 2021-05-31 14:39
 **/
@Slf4j
@Component
@Aspect
public class PaymentServiceAspect extends AbstractAop {
    @Pointcut("execution(* com.xdf.whiteaccount.service.PaymentService.*nsert*(..))")
    public void insert() {
    }

    @Pointcut("execution(* com.xdf.whiteaccount.service.PaymentService.*pdate*(..))")
    public void update() {
    }

    /**
     * 新增前追加事件
     *
     * @param joinPoint
     */
    @Before("insert()")
    public void beforeInsert(JoinPoint joinPoint) {
        Object param = getParam1(joinPoint);
        if (param instanceof Payment) {
            Payment record = (Payment) param;
            record.setUpload(0);
        } else if (param instanceof List) {
            for (Object record : (List) param) {
                if (record instanceof Payment) {
                    Payment item = (Payment) param;
                    item.setUpload(0);
                }
            }
        }
    }
}
