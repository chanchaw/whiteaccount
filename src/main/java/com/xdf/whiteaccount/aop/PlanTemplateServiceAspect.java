package com.xdf.whiteaccount.aop;

import com.xdf.whiteaccount.entity.PlanTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @program: white-account
 * @description: 部门员工切面
 * @author: 张柯
 * @create: 2021-05-20 13:57
 **/
@Component
@Slf4j
@Aspect
public class PlanTemplateServiceAspect extends AbstractAop {
    @Pointcut("execution(* com.xdf.whiteaccount.service.PlanTemplateService.insert*(..))")
    public void insert() {
    }

    @Pointcut("execution(* com.xdf.whiteaccount.service.PlanTemplateService.update*(..))")
    public void update() {
    }

    /**
     * 部门员工切点
     *
     * @param joinPoint
     */
    @Before("insert() || update()")
    public void beforeInsert(JoinPoint joinPoint) {
        Object param = getParam1(joinPoint);
        if (param != null && param instanceof PlanTemplate) {
            PlanTemplate e = (PlanTemplate) param;
            if (StringUtils.isEmpty(e.getContactCompanyId())) {
                e.setContactCompanyId(null);
            }
            if (StringUtils.isEmpty(e.getBottomSupplierId())) {
                e.setBottomSupplierId(null);
            }
            if (StringUtils.isEmpty(e.getMaterialSupplierId())) {
                e.setMaterialSupplierId(null);
            }
            if (StringUtils.isEmpty(e.getMiddleSupplierId())) {
                e.setMiddleSupplierId(null);
            }
        }
    }
}