package com.xdf.whiteaccount.aop;

import com.xdf.whiteaccount.dao.DyeFabricPlanMapper;
import com.xdf.whiteaccount.dao.ProcessTypeMapper;
import com.xdf.whiteaccount.entity.DyeFabricPlan;
import com.xdf.whiteaccount.entity.ProcessType;
import com.xdf.whiteaccount.utils.Example;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * @program: white-account
 * @description: 工序类型切面
 * @author: 张柯
 * @create: 2021-06-25 11:33
 **/
@Slf4j
@Component
@Aspect
public class ProcessTypeServiceAspect extends AbstractAop {
    private DyeFabricPlanMapper dyeFabricPlanMapper;
    private ProcessTypeMapper processTypeMapper;

    @Autowired
    public void setDyeFabricPlanMapper(DyeFabricPlanMapper dyeFabricPlanMapper) {
        this.dyeFabricPlanMapper = dyeFabricPlanMapper;
    }

    @Autowired
    public void setProcessTypeMapper(ProcessTypeMapper processTypeMapper) {
        this.processTypeMapper = processTypeMapper;
    }

    @Pointcut("execution(* com.xdf.whiteaccount.service.ProcessTypeService.update*(..))")
    private void update() {
    }

    @Pointcut("execution(* com.xdf.whiteaccount.service.ProcessTypeService.deleteByPrimaryKey(..))")
    private void delete() {
    }

    /**
     * 修改前事件
     *
     * @param joinPoint
     */
    @Before("update()")
    @Transactional(rollbackFor = Exception.class)
    public void beforeUpdate(JoinPoint joinPoint) {
        Object param = getParam1(joinPoint);
        if (param instanceof ProcessType) {
            ProcessType record = (ProcessType) param;
            ProcessType prev = processTypeMapper.selectByPrimaryKey(record.getId());
            Assert.notNull(prev, "不存在,禁止修改！");
            if (StringUtils.isNotEmpty(record.getTypeName())) {
                //  更改后名称后同时修改对应的色布发货计划单中的类型
                dyeFabricPlanMapper.updateByExampleSelective(DyeFabricPlan.builder().processType(record.getTypeName()).build(),
                        new Example().andEq("process_type", prev.getTypeName()));
            }
        }
    }

    /**
     * 删除前事件
     *
     * @param joinPoint
     */
    @Before("delete()")
    public void beforeDelete(JoinPoint joinPoint) {
        Object param = getParam1(joinPoint);
        if (param instanceof Integer) {
            ProcessType processType = processTypeMapper.selectByPrimaryKey((Integer) param);
            Assert.notNull(processType, "已删除！");
            long count = dyeFabricPlanMapper.countByExample(new Example().andEq("process_type", processType.getTypeName()));
            Assert.state(count <= 0, "已使用,无法删除!");
        }
    }
}