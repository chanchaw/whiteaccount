package com.xdf.whiteaccount.aop;

import com.xdf.whiteaccount.entity.Receivables;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @program: white-account
 * @description: 应收款
 * @author: 张柯
 * @create: 2021-06-08 11:51
 **/
@Component
@Slf4j
@Aspect
public class ReceivablesAspect extends AbstractAop {
    @Pointcut("execution(* com.xdf.whiteaccount.service.ReceivablesService.insert*(..))")
    public void insert() {
    }

    @Pointcut("execution(* com.xdf.whiteaccount.service.ReceivablesService.update*(..))")
    public void update() {
    }

    /**
     * 新增前事件
     *
     * @param joinPoint
     */
    @Before("insert()")
    public void beforeInsert(JoinPoint joinPoint) {
        Object param = getParam1(joinPoint);
        if (param instanceof Receivables) {
            Receivables record = (Receivables) param;
            Assert.state(record.getMoney() != null && record.getMoney().doubleValue() != 0D, "金额不允许为0");
            record.setUpload(0);
        }
    }

    /**
     * 修改前事件
     *
     * @param joinPoint
     */
    @Before("update()")
    public void beforeUpdate(JoinPoint joinPoint) {
        Object param = getParam1(joinPoint);
        if (param instanceof Receivables) {
            Receivables record = (Receivables) param;
            Assert.state(record.getMoney() != null && record.getMoney().doubleValue() != 0D, "金额不允许为0");
            record.setUpload(0);
        }
    }
}
