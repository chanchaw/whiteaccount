package com.xdf.whiteaccount.async;

import com.xdf.whiteaccount.async.util.WechatPushBodyBuilder;
import com.xdf.whiteaccount.cloudentity.UserMillView;
import com.xdf.whiteaccount.entity.WechatBody;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-26 15:41
 **/
public class BasePushNotification {
    private static final String RED = "#FF1722";
    private static final String MODIFY_MSG = "发货单修改通知";
    private static final String SEND_MSG = "你有一个订单已经发货";

    /**
     * 生成用户与染厂的映射Map
     *
     * @param list
     * @return
     */
    protected Map<String, List<String>> getUserMillMap(List<UserMillView> list) {
        return Optional.ofNullable(list).orElse(new ArrayList<>())
                .stream().collect(Collectors.toMap(UserMillView::getBackendName, x -> {
                    List<String> p = new ArrayList<>();
                    p.add(x.getOpenId());
                    return p;
                }, (prev, curr) -> {
                    prev.addAll(curr);
                    return prev;
                }));
    }

    /**
     * 设置推送模板参数
     *
     * @param item
     * @return
     */
    protected List<Map<String, String>> getWechatBodyFirstParam(WechatBody item) {
        List<Map<String, String>> param = new ArrayList<>();
        param.add(SEND_MSG.equals(String.valueOf(item.getA())) ?
                WechatPushBodyBuilder.buildMap(item.getA()) : WechatPushBodyBuilder.buildMap(item.getA(), RED));
        param.add(WechatPushBodyBuilder.buildMap(item.getB()));
        param.add(WechatPushBodyBuilder.buildMap(item.getC()));
        param.add(WechatPushBodyBuilder.buildMap(item.getD()));
        param.add(WechatPushBodyBuilder.buildMap(item.getE(), RED));
        param.add(WechatPushBodyBuilder.buildMap(item.getF()));
        param.add(WechatPushBodyBuilder.buildMap(item.getG()));
        param.add(WechatPushBodyBuilder.buildMap(item.getH()));
        return param;
    }
}
