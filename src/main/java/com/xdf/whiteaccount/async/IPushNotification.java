package com.xdf.whiteaccount.async;

import java.util.List;
import java.util.concurrent.Future;

/**
 * @program: white-account
 * @description: 推送发送接口，本接口会在发货或者退货后自动发送请求到云服务器，
 * 云服务器会根据传的参数推送到客户手机微信
 * @author: 张柯
 * @create: 2021-06-25 15:35
 **/
public interface IPushNotification {
    /**
     * 入库推送
     *
     * @param openIdList openId集合
     * @return
     * @throws Exception
     */
    Future<Boolean> pushInbound(List<String> openIdList) throws Exception;

    /**
     * 出库推送
     *
     * @param openIdList openId集合
     * @return
     * @throws Exception
     */
    Future<Boolean> pushOutbound(List<String> openIdList) throws Exception;
}
