package com.xdf.whiteaccount.async;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @program: white-account
 * @description: 微信推送实体类
 * @author: 张柯
 * @create: 2021-06-26 08:58
 **/
@Data
@Builder
public class WechatPushNtfBody {
    /** 元数据 */
    private List<Map<String, String>> metaList = new ArrayList<>();

    /** openID数组 */
    private List<String> openidList = new ArrayList<>();

    /** 推送模板ID */
    private String templateId;

    @Tolerate
    public WechatPushNtfBody() { }
}