package com.xdf.whiteaccount.async.enums;

/**
 * @program: white-account
 * @description: 模块枚举
 * @author: 张柯
 * @create: 2021-06-26 10:30
 **/
public enum ModuleEnum {
    GREY_FABRIC("GREY_FABRIC"),
    MATERIAL("MATERIAL"),
    DYE_FABRIC("DYE_FABRIC");
    private String moduleName;

    ModuleEnum(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleName() {
        return moduleName;
    }

    /**
     * 匹配
     *
     * @param moduleName
     * @return
     */
    public static ModuleEnum match(String moduleName) {
        if (moduleName == null || moduleName.trim().length() <= 0) return null;
        for (ModuleEnum m : values()) {
            if (m.getModuleName().equals(moduleName)) {
                return m;
            }
        }
        return null;
    }
}
