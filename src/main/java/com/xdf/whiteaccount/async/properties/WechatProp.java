package com.xdf.whiteaccount.async.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @program: white-account
 * @description: 微信
 * @author: 张柯
 * @create: 2021-06-26 09:18
 **/
@Data
@Component
@ConfigurationProperties(prefix = "wechat.config")
public class WechatProp {
    private String url;
    private String openidUrl;
    private String templateId;
    private String cron;
}
