package com.xdf.whiteaccount.async.util;

import com.xdf.whiteaccount.async.WechatPushNtfBody;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 * @program: white-account
 * @description: 微信构建器
 * @author: 张柯
 * @create: 2021-06-26 09:07
 **/
@Slf4j
public class WechatPushBodyBuilder {
    private WechatPushBodyBuilder() {
    }

    /**
     * 构建微信消息推送模板
     *
     * @param metaList   元数据
     * @param openIdList openId列表
     * @param templateId 模板ID
     * @return
     */
    public static WechatPushNtfBody buildBody(List<Map<String, String>> metaList, List<String> openIdList, String templateId) {
        Assert.state(CollectionUtils.isNotEmpty(openIdList), "open不能为空");
        Assert.state(CollectionUtils.isNotEmpty(metaList), "元数据不能为空");
        return WechatPushNtfBody.builder()
                .metaList(metaList)
                .openidList(openIdList)
                .templateId(templateId)
                .build();
    }

    /**
     * Map构建
     *
     * @param value 值
     * @param color 颜色
     * @return
     */
    public static Map<String, String> buildMap(String value, String color) {
        Map<String, String> map = new HashMap<>();
        map.put("value", value);
        if (StringUtils.isNotEmpty(color)) {
            map.put("color", color);
        }
        return map;
    }

    /**
     * Map构建
     *
     * @param value 值
     * @return
     */
    public static Map<String, String> buildMap(String value) {
        return buildMap(value, null);
    }
}