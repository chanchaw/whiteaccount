package com.xdf.whiteaccount.cache;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: xzy-management-system
 * @description: 缓存管理器
 * @author: 张柯
 * @create: 2021-01-02 14:46
 **/
@Component
@Slf4j
public class EhCacheManager implements CacheManager {
    private EhCacheShiro ehCacheShiro;

    @Autowired
    public void setEhCacheShiro(EhCacheShiro ehCacheShiro) {
        this.ehCacheShiro = ehCacheShiro;
    }

    @Override
    public <K, V> Cache<K, V> getCache(String s) throws CacheException {
        log.debug("获取缓存Ehcache");
        return ehCacheShiro;
    }
}
