package com.xdf.whiteaccount.cache;

import lombok.extern.slf4j.Slf4j;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.SerializationUtils;

import java.util.*;

/**
 * @program: xzy-management-system
 * @description: Ehcache
 * @author: 张柯
 * @create: 2021-01-02 14:47
 **/
@Component
@Slf4j
public class EhCacheShiro<K, V> implements Cache<K, V> {
    private CacheManager cacheManager;

    @Autowired
    public void setCacheManager(CacheManager cacheManager) {
        log.debug("获取Cache");
        this.cacheManager = cacheManager;
    }

    @Value("${ehCacheName}")
    private String ehCacheName;

    private static final String PREFIX = "SHIRO_CACHE_";

    private String getKey(K k) {
        return PREFIX + String.valueOf(k);
    }

    private Ehcache getEhCache() {
        return cacheManager.getEhcache(ehCacheName);
    }

    /**
     * 序列化key
     *
     * @param k
     * @return
     */
    private byte[] serializeKey(K k) {
        if (k == null) return null;
        if (k instanceof String) {
            return ((String) k + PREFIX).getBytes();
        }
        return SerializationUtils.serialize(k);
    }

    /**
     * 序列化Value
     *
     * @param v
     * @return
     */
    private byte[] serializeValue(V v) {
        if (v == null) return null;
        if (v instanceof String) {
            return ((String) v).getBytes();
        }
        return SerializationUtils.serialize(v);
    }

    /**
     * 反序列化Value
     *
     * @param bytes
     * @return
     */
    private V deserializeValue(byte[] bytes) {
        if (bytes == null) return null;
        return (V) SerializationUtils.deserialize(bytes);
    }

    /**
     * 反序列化Key
     *
     * @param bytes
     * @return
     */
    private K deserializeKey(byte[] bytes) {
        if (bytes == null) return null;
        return (K) SerializationUtils.deserialize(bytes);
    }

    @Override
    public V get(K k) throws CacheException {
        log.debug("获取缓存");
        Element e = getEhCache().get(getKey(k));
        log.debug("当前key数量:" + size());
        log.debug("缓存是否为空?" + (e == null));
        if (e == null) return null;
        return (V) e.getObjectValue();
    }

    @Override
    public V put(K k, V v) throws CacheException {
        log.debug("添加缓存记录");
        Element e = new Element(getKey(k), v);
        Ehcache cache = getEhCache();
        cache.put(e);
        log.debug("当前key数量:" + size());
        return v;
    }

    @Override
    public V remove(K k) throws CacheException {
        log.debug("清空缓存记录");
        V value = get(k);
        if (getEhCache().remove(getKey(k))) {
            return value;
        }
        return null;
    }

    @Override
    public void clear() throws CacheException {
        getEhCache().removeAll();
    }

    @Override
    public int size() {
        return getEhCache().getSize();
    }

    @Override
    public Set<K> keys() {
        List keysList = getEhCache().getKeys();
        if (keysList == null || keysList.size() <= 0) return null;
        Set<K> set = new HashSet<>();
        for (Object o : keysList) {
            if (o instanceof String) {
                if (((String) o).contains(PREFIX)) {
                    String origin = ((String) o).replace(PREFIX, "");
                    set.add((K) origin);
                }
            }
        }
        return set;
    }

    @Override
    public Collection<V> values() {
        List keysList = getEhCache().getKeys();
        if (keysList == null || keysList.size() <= 0) return null;
        Ehcache ehcache = getEhCache();
        List<V> values = new ArrayList<>();
        for (Object k : keysList) {
            Element e = ehcache.get(k);
            if (e != null)
                values.add((V) e.getObjectValue());
        }
        return values;
    }
}
