package com.xdf.whiteaccount.cloudcontroller;

import com.xdf.whiteaccount.enums.ResponseEnum;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.cloudentity.ResponseResult;

import java.util.List;

import com.xdf.whiteaccount.cloudentity.UserMill;
import com.xdf.whiteaccount.cloudservice.UserMillService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-06-11 09:32:46
 */
@Api(tags = "用户工厂关联表")
@CrossOrigin
@RestController
@RequestMapping("/userMill")
public class UserMillController {

    @Autowired
    private UserMillService service;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-06-11 09:32:46
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(UserMill record) throws Exception {
        service.insertSelective(record);
        return new ResponseResult<Void>();
    }

    /**
     * @Description : 批量新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-06-11 09:32:46
     */
    @ApiOperation(value = "批量新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequestMapping(value = "/multiInsert", method = RequestMethod.POST)
    public ResponseResult<Void> multiInsert(@RequestBody List<UserMill> list) throws Exception {
        service.multiInsert(list);
        return new ResponseResult<Void>();
    }

    /**
     * @Description : 选择修改
     * @Return : UserMill
     * @Author : 张柯
     * @Date : 2021-06-11 09:32:46
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(UserMill record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return new ResponseResult<Void>();
    }

    /**
     * @Description : 批量修改
     * @Return : UserMill
     * @Author : 张柯
     * @Date : 2021-06-11 09:32:46
     */
    @ApiOperation(value = "批量修改", httpMethod = "POST", notes = "自动生成的修改方法")
    @RequestMapping(value = "/multiUpdate", method = RequestMethod.POST)
    public ResponseResult<Void> multiUpdate(@RequestBody List<UserMill> list) throws Exception {
        service.multiUpdate(list);
        return new ResponseResult<Void>();
    }

    /**
     * @Description : 根据主键查询
     * @Return : UserMill
     * @Author : 张柯
     * @Date : 2021-06-11 09:32:46
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public UserMill selectByPrimaryKey(@PathVariable("id") Integer id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-06-11 09:32:46
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<UserMill> query(UserMill record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-06-11 09:32:46
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequestMapping(value = "/deleteByPk/{id}", method = RequestMethod.POST)
    public ResponseResult<Void> deleteByPrimaryKey(@PathVariable("id") Integer id) throws Exception {
        service.deleteByPrimaryKey(id);
        return new ResponseResult<Void>();
    }

    /**
     * 获取关注二维码的URL
     *
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "获取关注二维码的URL", httpMethod = "GET/POSR", notes = "获取关注二维码的URL")
    @RequestMapping(value = "/getFocusQrURL", method = RequestMethod.POST)
    public ResponseResult<String> getFocusQrURL() throws Exception {
        return new ResponseResult<String>(true, "200", ResponseEnum.OK.getName(), service.getFocusQr());
    }
}