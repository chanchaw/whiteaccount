package com.xdf.whiteaccount.clouddao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.cloudentity.Mill;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 工厂名单
* @Author : 张柯
* @Date : 2021-06-11 09:32:46
*/
@Repository
public interface MillMapper{
    int insert(Mill record);

    int insertSelective(Mill record);

    int multiInsert(List<Mill> list);

    int insertDumplicateKeyUpdate(Mill record);

    int updateByPrimaryKey(Mill record);

    int updateByPrimaryKeySelective(Mill record);

    int multiUpdate(List<Mill> list);

    int deleteByPrimaryKey(Integer id);

    Mill selectByPrimaryKey(Integer id);

    List<Mill> selectAll();

    List<Mill> selectByParam(Mill record);

    int updateByExampleSelective(@Param("record") Mill record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<Mill> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") Mill record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}