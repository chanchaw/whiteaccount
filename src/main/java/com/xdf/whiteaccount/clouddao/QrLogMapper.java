package com.xdf.whiteaccount.clouddao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.cloudentity.QrLog;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 扫描二维码的日志
* @Author : 张柯
* @Date : 2021-06-11 09:32:46
*/
@Repository
public interface QrLogMapper{
    int insert(QrLog record);

    int insertSelective(QrLog record);

    int multiInsert(List<QrLog> list);

    int insertDumplicateKeyUpdate(QrLog record);

    int updateByPrimaryKey(QrLog record);

    int updateByPrimaryKeySelective(QrLog record);

    int multiUpdate(List<QrLog> list);

    int deleteByPrimaryKey(Integer id);

    QrLog selectByPrimaryKey(Integer id);

    List<QrLog> selectAll();

    List<QrLog> selectByParam(QrLog record);

    int updateByExampleSelective(@Param("record") QrLog record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<QrLog> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") QrLog record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}