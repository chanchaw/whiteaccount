package com.xdf.whiteaccount.clouddao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.cloudentity.ReqsLog;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 请求和相应的日志表
* @Author : 张柯
* @Date : 2021-06-11 09:32:46
*/
@Repository
public interface ReqsLogMapper{
    int insert(ReqsLog record);

    int insertSelective(ReqsLog record);

    int multiInsert(List<ReqsLog> list);

    int insertDumplicateKeyUpdate(ReqsLog record);

    int updateByPrimaryKey(ReqsLog record);

    int updateByPrimaryKeySelective(ReqsLog record);

    int multiUpdate(List<ReqsLog> list);

    int deleteByPrimaryKey(Integer id);

    ReqsLog selectByPrimaryKey(Integer id);

    List<ReqsLog> selectAll();

    List<ReqsLog> selectByParam(ReqsLog record);

    int updateByExampleSelective(@Param("record") ReqsLog record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<ReqsLog> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") ReqsLog record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}