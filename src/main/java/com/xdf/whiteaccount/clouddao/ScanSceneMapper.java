package com.xdf.whiteaccount.clouddao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.cloudentity.ScanScene;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 扫描记录
* @Author : 张柯
* @Date : 2021-06-11 09:32:46
*/
@Repository
public interface ScanSceneMapper{
    int insert(ScanScene record);

    int insertSelective(ScanScene record);

    int multiInsert(List<ScanScene> list);

    int insertDumplicateKeyUpdate(ScanScene record);

    int updateByPrimaryKey(ScanScene record);

    int updateByPrimaryKeySelective(ScanScene record);

    int multiUpdate(List<ScanScene> list);

    int deleteByPrimaryKey(Integer id);

    ScanScene selectByPrimaryKey(Integer id);

    List<ScanScene> selectAll();

    List<ScanScene> selectByParam(ScanScene record);

    int updateByExampleSelective(@Param("record") ScanScene record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<ScanScene> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") ScanScene record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}