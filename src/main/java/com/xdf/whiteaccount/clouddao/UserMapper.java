package com.xdf.whiteaccount.clouddao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.cloudentity.User;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 用户表
* @Author : 张柯
* @Date : 2021-06-11 09:32:46
*/
@Repository
public interface UserMapper{
    int insert(User record);

    int insertSelective(User record);

    int multiInsert(List<User> list);

    int insertDumplicateKeyUpdate(User record);

    int updateByPrimaryKey(User record);

    int updateByPrimaryKeySelective(User record);

    int multiUpdate(List<User> list);

    int deleteByPrimaryKey(Integer id);

    User selectByPrimaryKey(Integer id);

    List<User> selectAll();

    List<User> selectByParam(User record);

    int updateByExampleSelective(@Param("record") User record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<User> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") User record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}