package com.xdf.whiteaccount.clouddao;

import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

import com.xdf.whiteaccount.cloudentity.UserMill;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 用户工厂关联表
* @Author : 张柯
* @Date : 2021-06-11 09:32:46
*/
@Repository
public interface UserMillMapper{
    int insert(UserMill record);

    int insertSelective(UserMill record);

    int multiInsert(List<UserMill> list);

    int insertDumplicateKeyUpdate(UserMill record);

    int updateByPrimaryKey(UserMill record);

    int updateByPrimaryKeySelective(UserMill record);

    int multiUpdate(List<UserMill> list);

    int deleteByPrimaryKey(Integer id);

    UserMill selectByPrimaryKey(Integer id);

    List<UserMill> selectAll();

    List<UserMill> selectByParam(UserMill record);

    int updateByExampleSelective(@Param("record") UserMill record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<UserMill> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") UserMill record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

    List<Map<String, Object>> queryMillList(String openid);
}