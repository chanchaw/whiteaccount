package com.xdf.whiteaccount.clouddao;

import com.xdf.whiteaccount.cloudentity.UserMillView;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @program: white-account
 * @description: 自定义Sql
 * @author: 张柯
 * @create: 2021-06-26 11:05
 **/
@Repository
public interface UserMillViewMapper {
    List<UserMillView> selectByOpenIdList(List<String> openIdList);

    UserMillView selectByOpenId(String openIdList);
}
