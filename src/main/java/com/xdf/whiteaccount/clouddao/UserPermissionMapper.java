package com.xdf.whiteaccount.clouddao;

import com.xdf.whiteaccount.mobileentity.WechatPermission;
import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.cloudentity.UserPermission;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 用户公众号权限
 * @Author : 张柯
 * @Date : 2021-07-05 13:49:18
 */
@Repository
public interface UserPermissionMapper {
    int insert(UserPermission record);

    int insertSelective(UserPermission record);

    int multiInsert(List<UserPermission> list);

    int insertDumplicateKeyUpdate(UserPermission record);

    int updateByPrimaryKey(UserPermission record);

    int updateByPrimaryKeySelective(UserPermission record);

    int multiUpdate(List<UserPermission> list);

    int deleteByPrimaryKey(Integer id);

    UserPermission selectByPrimaryKey(Integer id);

    List<UserPermission> selectAll();

    List<UserPermission> selectByParam(UserPermission record);

    int updateByExampleSelective(@Param("record") UserPermission record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<UserPermission> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") UserPermission record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

    WechatPermission queryByOpenid(String openid);

    List<WechatPermission> listQueryByOpenid(String openid);

    List<WechatPermission> listQueryByMill(String mill);
}