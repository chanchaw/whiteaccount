package com.xdf.whiteaccount.cloudentity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : 工厂名单(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-11 09:32:46
    */
@Data
@Builder
public class Mill implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 无备注 */
    private Integer iid;

    /** 工厂名 */
    private String sid;

    /** 后端项目端口 */
    private Integer backendPort;

    /** 后端项目名称 */
    private String backendName;

    /** 前端项目名称 */
    private String frontendName;

    /** 无备注 */
    private Date createTime;
    /**
    * 无参构造方法
    */
    @Tolerate
    public Mill(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}