package com.xdf.whiteaccount.cloudentity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : 扫描二维码的日志(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-11 09:32:46
    */
@Data
@Builder
public class QrLog implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer iid;

    /** 兑换二维码的票据 */
    private String ticket;

    /** 二维码的创建时间 */
    private Date qrTime;

    /** 二维码的场景值 */
    private String sceneStr;

    /** 微信用户的openid */
    private String openid;

    /** 创建时间 */
    private Date createTime;
    /**
    * 无参构造方法
    */
    @Tolerate
    public QrLog(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}