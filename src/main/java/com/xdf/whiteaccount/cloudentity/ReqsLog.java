package com.xdf.whiteaccount.cloudentity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : 请求和相应的日志表(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-11 09:32:46
    */
@Data
@Builder
public class ReqsLog implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 无备注 */
    private Integer iid;

    /** 类型,请求=req，响应=res */
    private String type;

    /** 请求或者响应的数据 */
    private String bodyData;

    /** 日志内容 */
    private String des;

    /** 创建时间,DB自动填充 */
    private Date createTime;
    /**
    * 无参构造方法
    */
    @Tolerate
    public ReqsLog(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}