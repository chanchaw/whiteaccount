package com.xdf.whiteaccount.cloudentity;

import java.util.Objects;

/**
* @description: 自定义响应体
* @author: 张柯
* @create: 2021-06-11 09:32:46
**/
public class ResponseResult<T> {
    private boolean success;
    private String state;
    private String message;
    private T data;

    public boolean isSuccess() {
    return success;
    }

    public void setSuccess(boolean success) {
    this.success = success;
    }

    public String getState() {
    return state;
    }

    public void setState(String state) {
    this.state = state;
    }

    public String getMessage() {
    return message;
    }

    public void setMessage(String message) {
    this.message = message;
    }

    public T getData() {
    return data;
    }

    public void setData(T data) {
    this.data = data;
    }

    @Override
    public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ResponseResult<?> that = (ResponseResult<?>) o;
    return success == that.success &&
    Objects.equals(state, that.state) &&
    Objects.equals(message, that.message) &&
    Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
    return Objects.hash(success, state, message, data);
    }

    @Override
    public String toString() {
    final StringBuilder sb = new StringBuilder("ResponseResult{");
    sb.append("success=").append(success);
    sb.append(", state='").append(state).append('\'');
    sb.append(", message='").append(message).append('\'');
    sb.append(", data=").append(data);
    sb.append('}');
    return sb.toString();
    }

    public ResponseResult() {
    }

    public ResponseResult(boolean success) {
    this.success = success;
    }

    public ResponseResult(boolean success, String state) {
    this.success = success;
    this.state = state;
    }

    public ResponseResult(boolean success, String state, String message) {
    this.success = success;
    this.state = state;
    this.message = message;
    }

    public ResponseResult(boolean success, String state, String message, T data) {
    this.success = success;
    this.state = state;
    this.message = message;
    this.data = data;
    }
}