package com.xdf.whiteaccount.cloudentity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : 扫描记录(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-11 09:32:46
    */
@Data
@Builder
public class ScanScene implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 无备注 */
    private Integer iid;

    /** 微信openid */
    private String openId;

    /** 昵称 */
    private String nickName;

    /** 扫描的二维码的场景值 */
    private String sceneStr;

    /** 创建时间,DB自动填充 */
    private Date createTime;
    /**
    * 无参构造方法
    */
    @Tolerate
    public ScanScene(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}