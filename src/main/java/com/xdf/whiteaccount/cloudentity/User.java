package com.xdf.whiteaccount.cloudentity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : 用户表(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-11 09:32:46
    */
@Data
@Builder
public class User implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 无备注 */
    private Integer iid;

    /** 微信openid */
    private String openId;

    /** 微信昵称 */
    private String nickName;

    /** 真实姓名 */
    private String realName;

    /** 创建时间 */
    private Date createTime;

    /** 公司内部人员,默认0，1表示新东方公司人员 */
    private Boolean company;

    /** 审核,默认0，1表示有新用户关注会推送审核消息 */
    private Boolean audit;

    /** 无备注 */
    private Boolean me;
    /**
    * 无参构造方法
    */
    @Tolerate
    public User(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}