package com.xdf.whiteaccount.cloudentity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : 用户工厂关联表(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-07-09 08:54:18
    */
@Data
@Builder
public class UserMill implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 无备注 */
    private Integer iid;

    /** 微信openid */
    private String openId;

    /** 工厂名 */
    private String millSid;

    /** 用户编号 */
    private String userid;

    /** 用户名 */
    private String username;

    /** 默认工厂,1表示用户关联多个工厂时的默认工厂 */
    private Boolean millDefault;

    /** 有效,1表示关系有效 */
    private Boolean effective;

    /** 无备注 */
    private Date createTime;

    /** 推送计划,默认1表示对应染厂有新计划则推送 */
    private Boolean pushPlan;

    /** 推送原料,默认1表示推送原料入库消息 */
    private Boolean pushMaterial;

    /** 推送发货 */
    private Boolean pushDeliver;

    /** 审核权限,默认0表示没有审核权限 */
    private Boolean audit;
    /**
    * 无参构造方法
    */
    @Tolerate
    public UserMill(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}