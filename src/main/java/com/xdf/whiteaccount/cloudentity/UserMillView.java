package com.xdf.whiteaccount.cloudentity;

import lombok.Data;

/**
 * @program: white-account
 * @description: 用户与染厂关系
 * @author: 张柯
 * @create: 2021-06-26 11:08
 **/
@Data
public class UserMillView {
    private String openId;
    private String mill;
    private String backendName;
    private Boolean pushPlan;
    private Boolean pushMaterial;
    private Boolean pushDeliver;
}
