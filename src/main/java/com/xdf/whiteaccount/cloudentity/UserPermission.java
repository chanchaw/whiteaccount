package com.xdf.whiteaccount.cloudentity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 用户公众号权限(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-07-05 13:49:18
    */
@Data
@Builder
public class UserPermission implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** openid */
    private String openid;

    /** 已排计划 */
    private Boolean wxPlan;

    /** 客户订单 */
    private Boolean wxClientOrder;

    /** 日产量表 */
    private Boolean wxDailyYield;

    /** 应收应付 */
    private Boolean wxReceivablePayable;

    /** 白坯库存 */
    private Boolean wxFabricStore;

    /** 色布计划 */
    private Boolean wxDyePlan;

    /** 色布客户订单 */
    private Boolean wxDyeClientOrder;

    /** 色布库存 */
    private Boolean wxWxDyeFabricStore;

    /** 原料库存 */
    private Boolean wxMaterialStore;

    /** 权限管理 */
    private Boolean wxPermission;
    /**
    * 无参构造方法
    */
    @Tolerate
    public UserPermission(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}