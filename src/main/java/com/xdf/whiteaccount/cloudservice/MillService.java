package com.xdf.whiteaccount.cloudservice;

import com.xdf.whiteaccount.cloudentity.Mill;
import com.xdf.whiteaccount.utils.Example;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-11 09:32:46
 */
public interface MillService {
    int insert(Mill record) throws Exception;

    int insertSelective(Mill record) throws Exception;

    int multiInsert(List<Mill> list) throws Exception;

    int updateByPrimaryKey(Mill record) throws Exception;

    int updateByPrimaryKeySelective(Mill record) throws Exception;

    int update(Mill record) throws Exception;

    int updateByExample(Mill record, Example example) throws Exception;

    int multiUpdate(List<Mill> list) throws Exception;

    Mill selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<Mill> listQuery(Mill record) throws Exception;

    Mill selectMillByOpenid(String openid) throws Exception;

    Mill selectMillByBackendName(String mill) throws Exception;

    Long countByExample(Example example) throws Exception;
}