package com.xdf.whiteaccount.cloudservice;

import com.xdf.whiteaccount.cloudentity.QrLog;

import java.util.List;
import java.util.Map;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-11 09:32:46
 */
public interface QrLogService {
    String USER_NAME = "USER_NAME";
    String DATASOURCE_NAME = "DATASOURCE_NAME";
    String OPENID = "OPENID";
    String NICK_NAME = "NICK_NAME";

    int insert(QrLog record) throws Exception;

    int insertSelective(QrLog record) throws Exception;

    int multiInsert(List<QrLog> list) throws Exception;

    int updateByPrimaryKey(QrLog record) throws Exception;

    int updateByPrimaryKeySelective(QrLog record) throws Exception;

    int update(QrLog record) throws Exception;

    int multiUpdate(List<QrLog> list) throws Exception;

    QrLog selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<QrLog> listQuery(QrLog record) throws Exception;

    Map<String, Object> getLoginMsg(String uuid) throws Exception;
}