package com.xdf.whiteaccount.cloudservice;

import com.xdf.whiteaccount.cloudentity.ReqsLog;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-11 09:32:46
 */
public interface ReqsLogService {
     int insert(ReqsLog record) throws Exception;

     int insertSelective(ReqsLog record) throws Exception;

     int multiInsert(List<ReqsLog> list) throws Exception;

     int updateByPrimaryKey(ReqsLog record) throws Exception;

     int updateByPrimaryKeySelective(ReqsLog record) throws Exception;

     int update(ReqsLog record) throws Exception;

     int multiUpdate(List<ReqsLog> list) throws Exception;

     ReqsLog selectByPrimaryKey(Integer id) throws Exception;

     int deleteByPrimaryKey(Integer id) throws Exception;

     List<ReqsLog> listQuery(ReqsLog record) throws Exception;
}