package com.xdf.whiteaccount.cloudservice;

import com.xdf.whiteaccount.cloudentity.ScanScene;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-11 09:32:46
 */
public interface ScanSceneService {
     int insert(ScanScene record) throws Exception;

     int insertSelective(ScanScene record) throws Exception;

     int multiInsert(List<ScanScene> list) throws Exception;

     int updateByPrimaryKey(ScanScene record) throws Exception;

     int updateByPrimaryKeySelective(ScanScene record) throws Exception;

     int update(ScanScene record) throws Exception;

     int multiUpdate(List<ScanScene> list) throws Exception;

     ScanScene selectByPrimaryKey(Integer id) throws Exception;

     int deleteByPrimaryKey(Integer id) throws Exception;

     List<ScanScene> listQuery(ScanScene record) throws Exception;
}