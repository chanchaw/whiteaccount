package com.xdf.whiteaccount.cloudservice;

import com.xdf.whiteaccount.cloudentity.UserMill;
import com.xdf.whiteaccount.utils.Example;
import java.util.Map;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-11 09:32:46
 */
public interface UserMillService {
    int insert(UserMill record) throws Exception;

    int insertSelective(UserMill record) throws Exception;

    int multiInsert(List<UserMill> list) throws Exception;

    int updateByPrimaryKey(UserMill record) throws Exception;

    int updateByPrimaryKeySelective(UserMill record) throws Exception;

    int update(UserMill record) throws Exception;

    int updateByExample(UserMill record, Example example) throws Exception;

    int multiUpdate(List<UserMill> list) throws Exception;

    UserMill selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<UserMill> listQuery(UserMill record) throws Exception;

    long countByExample(Example example) throws Exception;

    String getFocusQr() throws Exception;

    List<Map<String, Object>> queryMillList(String openid) throws Exception;
}