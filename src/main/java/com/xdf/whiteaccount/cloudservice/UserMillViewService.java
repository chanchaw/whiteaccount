package com.xdf.whiteaccount.cloudservice;

import com.xdf.whiteaccount.cloudentity.UserMillView;
import java.util.List;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-26 11:18
 **/
public interface UserMillViewService {
    List<UserMillView> selectByOpenIdList(List<String> openIdList);

    UserMillView selectByOpenId(String openIdList);
}
