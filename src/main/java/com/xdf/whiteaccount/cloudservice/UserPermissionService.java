package com.xdf.whiteaccount.cloudservice;

import com.xdf.whiteaccount.cloudentity.UserPermission;
import com.xdf.whiteaccount.mobileentity.WechatPermission;
import com.xdf.whiteaccount.utils.Example;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-07-05 13:49:18
 */
public interface UserPermissionService {
    int insert(UserPermission record) throws Exception;

    int insertSelective(UserPermission record) throws Exception;

    int insertDumplicateKeyUpdate(UserPermission record);

    int multiInsert(List<UserPermission> list) throws Exception;

    int updateByPrimaryKey(UserPermission record) throws Exception;

    int updateByPrimaryKeySelective(UserPermission record) throws Exception;

    int updateByExampleSelective(UserPermission record, Example example) throws Exception;

    int multiUpdate(List<UserPermission> list) throws Exception;

    UserPermission selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<UserPermission> listQuery(UserPermission record) throws Exception;

    WechatPermission queryByOpenid(String openid) throws Exception;

    List<WechatPermission> listQueryByOpenid(String openid) throws Exception;

    long countByExample(Example example) throws Exception;

    List<WechatPermission> listQueryByMill(String mill);
}