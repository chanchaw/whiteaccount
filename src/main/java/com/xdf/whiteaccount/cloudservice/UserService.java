package com.xdf.whiteaccount.cloudservice;

import com.xdf.whiteaccount.cloudentity.User;
import com.xdf.whiteaccount.utils.Example;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-11 09:32:46
 */
public interface UserService {
     int insert(User record) throws Exception;

     int insertSelective(User record) throws Exception;

     int multiInsert(List<User> list) throws Exception;

     int updateByPrimaryKey(User record) throws Exception;

     int updateByPrimaryKeySelective(User record) throws Exception;

     int insertDumplicateKeyUpdate(User record) throws Exception;

     int updateByExampleSelective(User record, Example example) throws Exception;

     int update(User record) throws Exception;

     int multiUpdate(List<User> list) throws Exception;

     User selectByPrimaryKey(Integer id) throws Exception;

     User selectByOpenId(String openid) throws Exception;

     int deleteByPrimaryKey(Integer id) throws Exception;

     List<User> listQuery(User record) throws Exception;
}