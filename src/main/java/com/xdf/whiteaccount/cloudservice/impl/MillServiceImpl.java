package com.xdf.whiteaccount.cloudservice.impl;

import com.xdf.whiteaccount.clouddao.UserMillMapper;
import com.xdf.whiteaccount.cloudentity.UserMill;
import com.xdf.whiteaccount.utils.Example;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.cloudentity.Mill;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.xdf.whiteaccount.clouddao.MillMapper;
import com.xdf.whiteaccount.cloudservice.MillService;

/**
 * @Description : com.xdf.whiteaccount.cloudservice.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-06-11 09:32:46
 */
@Service
public class MillServiceImpl implements MillService {
    @Autowired
    private MillMapper dao;
    @Autowired
    private UserMillMapper userMillMapper;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(Mill record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(Mill record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<Mill> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(Mill record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(Mill record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(Mill record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * 根据条件修改
     * @param record
     * @param example
     * @return
     * @throws Exception
     */
    @Override
    public int updateByExample(Mill record, Example example) throws Exception {
        return dao.updateByExampleSelective(record, example);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<Mill> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    public Mill selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    public List<Mill> listQuery(Mill record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 根据openid查询
     *
     * @param openid
     * @return
     * @throws Exception
     */
    @Override
    public Mill selectMillByOpenid(String openid) throws Exception {
        List<UserMill> record = userMillMapper.selectByExample(new Example().andEq("open_id", openid)
                .setRowSize(1L)
                .setOrderSql(Example.orderByCondition("iid", Example.DESC)));
        if (CollectionUtils.isEmpty(record)) return null;
        List<Mill> mills = dao.selectByParam(Mill.builder()
                .sid(record.get(0).getMillSid()).build());
        return CollectionUtils.isEmpty(mills) ? null : mills.get(0);
    }

    /**
     * 根据名称查询
     *
     * @param mill
     * @return
     * @throws Exception
     */
    @Override
    public Mill selectMillByBackendName(String mill) throws Exception {
        return Optional.ofNullable(dao.selectByExample(new Example().andEq("backend_name", mill))).orElse(new ArrayList<>())
                .stream().findFirst().orElse(null);
    }

    /**
     * 记数
     * @param example
     * @return
     * @throws Exception
     */
    @Override
    public Long countByExample(Example example) throws Exception {
        return dao.countByExample(example);
    }
}