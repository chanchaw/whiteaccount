package com.xdf.whiteaccount.cloudservice.impl;

import com.xdf.whiteaccount.clouddao.UserMapper;
import com.xdf.whiteaccount.cloudentity.Mill;
import com.xdf.whiteaccount.cloudentity.User;
import com.xdf.whiteaccount.cloudservice.MillService;
import com.xdf.whiteaccount.exception.NotallowException;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.cloudentity.QrLog;
import java.util.*;
import com.xdf.whiteaccount.clouddao.QrLogMapper;
import com.xdf.whiteaccount.cloudservice.QrLogService;

/**
 * @Description : com.xdf.whiteaccount.cloudservice.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-06-11 09:32:46
 */
@Service
public class QrLogServiceImpl implements QrLogService {
    @Autowired
    private QrLogMapper dao;
    @Autowired
    private MillService millService;
    @Autowired
    private UserMapper userMapper;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(QrLog record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(QrLog record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<QrLog> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(QrLog record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(QrLog record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(QrLog record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<QrLog> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    public QrLog selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    public List<QrLog> listQuery(QrLog record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 根据uuid获取扫码的登录信息
     *
     * @param uuid uuid参数
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> getLoginMsg(String uuid) throws Exception {
        List<QrLog> list = dao.selectByParam(QrLog.builder().sceneStr(uuid).build());
        if (CollectionUtils.isEmpty(list)) return null;
        Map<String, Object> map = new HashMap<>();
        String openId = list.stream().findFirst().map(QrLog::getOpenid).orElseThrow(() -> new NotallowException("openid不存在！"));
        Mill mill = millService.selectMillByOpenid(openId);
        User userObject = Optional.ofNullable(
                userMapper.selectByParam(User.builder().openId(openId).build())).orElse(new ArrayList<>())
                .stream().findFirst().orElse(new User());
        String realName = userObject.getRealName();
        String nickName = userObject.getNickName();
        //  设置当前数据库名称(Key)
        map.put(DATASOURCE_NAME, mill.getBackendName());
        //  返回openid
        map.put(OPENID, openId);
        //  返回真实姓名
        map.put(USER_NAME, realName);
        //  返回昵称
        map.put(NICK_NAME, nickName);
        return map;
    }
}