package com.xdf.whiteaccount.cloudservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.cloudentity.ScanScene;
import java.util.List;
import com.xdf.whiteaccount.clouddao.ScanSceneMapper;
import com.xdf.whiteaccount.cloudservice.ScanSceneService;

 /**
 * @Description : com.xdf.whiteaccount.cloudservice.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-06-11 09:32:46
 */
@Service
public class ScanSceneServiceImpl implements ScanSceneService{
@Autowired
private ScanSceneMapper dao;

     /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(ScanScene record) throws Exception{
        return dao.insert(record);
    }

     /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(ScanScene record) throws Exception{
        return dao.insertSelective(record);
    }

     /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<ScanScene> list) throws Exception{
        return dao.multiInsert(list);
    }

     /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(ScanScene record) throws Exception{
        return dao.updateByPrimaryKey(record);
    }

     /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(ScanScene record) throws Exception{
        return dao.updateByPrimaryKeySelective(record);
    }

     /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(ScanScene record) throws Exception{
        return dao.updateByPrimaryKey(record);
    }

     /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<ScanScene> list) throws Exception{
        return dao.multiUpdate(list);
    }

     /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    public ScanScene selectByPrimaryKey(Integer id) throws Exception{
        return dao.selectByPrimaryKey(id);
    }

     /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception{
        return dao.deleteByPrimaryKey(id);
    }

     /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    public List<ScanScene> listQuery(ScanScene record) throws Exception{
        return dao.selectByParam(record);
    }
}