package com.xdf.whiteaccount.cloudservice.impl;

import com.xdf.whiteaccount.cloudentity.User;
import com.xdf.whiteaccount.cloudservice.UserService;
import com.xdf.whiteaccount.qrcodeutil.WechatQrCodeUtils;
import com.xdf.whiteaccount.utils.Example;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.cloudentity.UserMill;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import com.xdf.whiteaccount.clouddao.UserMillMapper;
import com.xdf.whiteaccount.cloudservice.UserMillService;
import org.springframework.util.Assert;

/**
 * @Description : com.xdf.whiteaccount.cloudservice.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-06-11 09:32:46
 */
@Service
@Slf4j
public class UserMillServiceImpl implements UserMillService {
    @Autowired
    private UserMillMapper dao;
    @Autowired
    private WechatQrCodeUtils utils;
    @Autowired
    private UserService userService;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(UserMill record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(UserMill record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<UserMill> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(UserMill record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(UserMill record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(UserMill record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * 根据条件修改
     *
     * @param record
     * @param example
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByExample(UserMill record, Example example) throws Exception {
        if (example == null) example = new Example();
        return dao.updateByExampleSelective(record, example);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<UserMill> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    public UserMill selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    public List<UserMill> listQuery(UserMill record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 记数
     *
     * @param example
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public long countByExample(Example example) throws Exception {
        return dao.countByExample(example);
    }

    /**
     * 获取关注二维码
     *
     * @return
     * @throws Exception
     */
    @Override
    public String getFocusQr() throws Exception {
        //  获取当前登录用户的openId
        String openId = String.valueOf(SecurityUtils.getSubject().getPrincipal());
        Assert.state(StringUtils.isNotEmpty(openId), "openId不存在！");
        UserMill userMill = Optional.ofNullable(dao.selectByParam(UserMill.builder().openId(openId).build()))
                .orElse(new ArrayList<>())
                .stream().findFirst().orElse(null);
        if (userMill != null) {
            String mill = userMill.getMillSid();
            log.debug("当前公司名称：{}", mill);
            return utils.getQrCode(mill);
        }
        return null;
    }

    /**
     * 查询所有权限
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> queryMillList(String openid) throws Exception {
        User user = userService.selectByOpenId(openid);
        if (!Optional.ofNullable(user).map(User::getCompany).orElse(false)) {
            return null;
        }
        return dao.queryMillList(openid);
    }
}