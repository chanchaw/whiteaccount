package com.xdf.whiteaccount.cloudservice.impl;

import com.xdf.whiteaccount.clouddao.UserMillViewMapper;
import com.xdf.whiteaccount.cloudentity.UserMillView;
import com.xdf.whiteaccount.cloudservice.UserMillViewService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-26 11:19
 **/
@Service
public class UserMillViewServiceImpl implements UserMillViewService {
    @Autowired
    private UserMillViewMapper userMillViewMapper;

    @Override
    public List<UserMillView> selectByOpenIdList(List<String> openIdList) {
        if (CollectionUtils.isNotEmpty(openIdList)) {
            return userMillViewMapper.selectByOpenIdList(openIdList);
        }
        return null;
    }

    @Override
    public UserMillView selectByOpenId(String openIdList) {
        return userMillViewMapper.selectByOpenId(openIdList);
    }
}
