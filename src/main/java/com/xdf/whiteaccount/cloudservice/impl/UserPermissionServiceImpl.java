package com.xdf.whiteaccount.cloudservice.impl;

import com.xdf.whiteaccount.mobileentity.WechatPermission;
import com.xdf.whiteaccount.utils.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.cloudentity.UserPermission;
import java.util.List;
import com.xdf.whiteaccount.clouddao.UserPermissionMapper;
import com.xdf.whiteaccount.cloudservice.UserPermissionService;

/**
 * @Description : com.xdf.whiteaccount.cloudservice.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-07-05 13:49:18
 */
@Service
public class UserPermissionServiceImpl implements UserPermissionService {
    @Autowired
    private UserPermissionMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-07-05 13:49:18
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(UserPermission record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-07-05 13:49:18
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(UserPermission record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * 新增并修改
     *
     * @param record
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertDumplicateKeyUpdate(UserPermission record) {
        return dao.insertDumplicateKeyUpdate(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-07-05 13:49:18
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<UserPermission> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-07-05 13:49:18
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(UserPermission record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-07-05 13:49:18
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(UserPermission record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * 根据Example查询
     *
     * @param record
     * @param example
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByExampleSelective(UserPermission record, Example example) throws Exception {
        return dao.updateByExampleSelective(record, example);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-07-05 13:49:18
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<UserPermission> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-07-05 13:49:18
     */
    @Override
    public UserPermission selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-07-05 13:49:18
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-07-05 13:49:18
     */
    @Override
    public List<UserPermission> listQuery(UserPermission record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 根据openid查询
     *
     * @param openid
     * @return
     * @throws Exception
     */
    @Override
    public WechatPermission queryByOpenid(String openid) throws Exception {
        return dao.queryByOpenid(openid);
    }

    /**
     * 查询
     *
     * @param openid
     * @return
     * @throws Exception
     */
    @Override
    public List<WechatPermission> listQueryByOpenid(String openid) throws Exception {
        return dao.listQueryByOpenid(openid);
    }

    /**
     * 根据模板查询
     *
     * @param example
     * @return
     * @throws Exception
     */
    @Override
    public long countByExample(Example example) throws Exception {
        if (example == null) example = new Example();
        return dao.countByExample(example);
    }

    @Override
    public List<WechatPermission> listQueryByMill(String mill) {
        return dao.listQueryByMill(mill);
    }
}