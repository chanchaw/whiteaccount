package com.xdf.whiteaccount.cloudservice.impl;

import com.xdf.whiteaccount.utils.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.cloudentity.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.xdf.whiteaccount.clouddao.UserMapper;
import com.xdf.whiteaccount.cloudservice.UserService;

/**
 * @Description : com.xdf.whiteaccount.cloudservice.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-06-11 09:32:46
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(User record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(User record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<User> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(User record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(User record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * 新增或修改
     * @param record
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertDumplicateKeyUpdate(User record) throws Exception {
        return dao.insertDumplicateKeyUpdate(record);
    }

    /**
     * 根据模板修改
     * @param record
     * @param example
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByExampleSelective(User record, Example example) throws Exception {
        return dao.updateByExampleSelective(record, example);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(User record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<User> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    public User selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @param openid
     * @return
     * @throws Exception
     */
    @Override
    public User selectByOpenId(String openid) throws Exception {
        return Optional.ofNullable(dao.selectByParam(User.builder().openId(openid).build()))
                .orElse(new ArrayList<>()).stream().findFirst().orElse(null);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-06-11 09:32:46
     */
    @Override
    public List<User> listQuery(User record) throws Exception {
        return dao.selectByParam(record);
    }
}