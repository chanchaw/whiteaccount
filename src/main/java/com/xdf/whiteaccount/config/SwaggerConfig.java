package com.xdf.whiteaccount.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @program: xzy-management-system
 * @description: Swagger配置
 * @author: 张柯
 * @create: 2020-12-31 14:53
 **/
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket(Environment environment) {
        Profiles profiles = Profiles.of("dev");
        boolean flag = environment.acceptsProfiles(profiles);
        return new Docket(DocumentationType.SPRING_WEB)
                .apiInfo(apiInfo())
                .enable(flag)
                .select()
                .build();
    }


    public ApiInfo apiInfo() {
        return ApiInfo.DEFAULT;
    }
}
