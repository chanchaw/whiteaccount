package com.xdf.whiteaccount.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @program: white-account
 * @description: 主数据源配置文件内容
 * @author: 张柯
 * @create: 2021-06-11 10:00
 **/
@Data
@Component
@ConfigurationProperties(prefix = "primarydb.datasource")
public class PrimaryDbParam {
    private String driverClassName;
    private String username;
    private String password;
    private String url;
    private int initializationFailTimeout;
    private int minimumIdle;
    private int loginTimeout;
    private long idleTimeout;
    private long connectionTimeout;
    private long maxLifeTime;
    private int maximumPoolSize;
    private int initialSize;
    private String validationQuery;
    private boolean testOnBorrow;
    private long timeBetweenEvictionRunsMillis;
    private int minEvictableIdleTimeMillis;
}
