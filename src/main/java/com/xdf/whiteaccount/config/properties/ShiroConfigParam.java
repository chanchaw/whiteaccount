package com.xdf.whiteaccount.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @program: xzy-management-system
 * @description:
 * @author: 张柯
 * @create: 2021-01-21 21:45
 **/
@ConfigurationProperties(prefix = "jwt")
@Data
public class ShiroConfigParam {
    private String anonymousUrls;
    private long expireTime;
}
