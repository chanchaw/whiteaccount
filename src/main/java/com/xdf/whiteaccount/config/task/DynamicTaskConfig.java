package com.xdf.whiteaccount.config.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

/**
 * @program: white-account
 * @description: 动态任务组件
 * @author: 张柯
 * @create: 2021-07-24 14:59
 **/
@Component
@Slf4j
public class DynamicTaskConfig {
    //  保存当前任务
    private static final ConcurrentHashMap<String, TaskObject> scheduleMap = new ConcurrentHashMap<>();
    //  线程任务池
    private ThreadPoolTaskScheduler scheduler;

    @Autowired
    public void setScheduler(ThreadPoolTaskScheduler scheduler) {
        this.scheduler = scheduler;
    }

    /**
     * 增加任务
     *
     * @param taskName 任务名
     * @param task     任务
     * @param cron     cron表达式
     * @return
     */
    public boolean addTask(String taskName, Runnable task, String cron) {
        if (scheduleMap.containsKey(taskName)) {
            return true;
        }
        ScheduledFuture<?> scheduledFuture = scheduler.schedule(task, new CronTrigger(cron));
        if (scheduledFuture != null) {
            scheduleMap.put(taskName, new TaskObject(scheduledFuture, cron));
            return true;
        }
        return false;
    }

    /**
     * 更新定时任务
     *
     * @param taskName 任务名
     * @param task     任务
     * @param cron     cron表达式
     * @return
     */
    public boolean updateTask(String taskName, Runnable task, String cron) {
        if (scheduleMap.containsKey(taskName)) {
            stopTask(taskName);
        }
        log.debug("更新任务：{}", taskName);
        return addTask(taskName, task, cron);
    }

    /**
     * 获取任务
     *
     * @param taskName 任务名
     * @return
     */
    public ScheduledFuture<?> getScheduledFuture(String taskName) {
        if (scheduleMap.containsKey(taskName)) {
            return scheduleMap.get(taskName).getScheduledFuture();
        }
        return null;
    }

    /**
     * 获取cron
     *
     * @param taskName 任务名
     * @return
     */
    public String getCron(String taskName) {
        if (scheduleMap.containsKey(taskName)) {
            return scheduleMap.get(taskName).getCron();
        }
        return null;
    }

    /**
     * 判断任务是否存在
     *
     * @param taskName
     * @return
     */
    public boolean hasTask(String taskName) {
        return scheduleMap.containsKey(taskName);
    }

    /**
     * 停止任务
     *
     * @param taskName
     */
    public void stopTask(String taskName) {
        if (scheduleMap.containsKey(taskName)) {
            ScheduledFuture<?> future = scheduleMap.get(taskName).getScheduledFuture();
            if (future != null) {
                if (future.cancel(true)) {
                    log.debug("删除任务：{}", taskName);
                    scheduleMap.remove(taskName);
                }
            }
        }
    }
}