package com.xdf.whiteaccount.config.task;

import java.io.Serializable;
import java.util.concurrent.ScheduledFuture;

/**
 * @program: white-account
 * @description: 任务对象
 * @author: 张柯
 * @create: 2021-07-27 12:46
 **/
public class TaskObject implements Serializable {
    private static final long serialVersionUID = -5201093730619152874L;
    private ScheduledFuture<?> scheduledFuture;

    private String cron;

    public ScheduledFuture<?> getScheduledFuture() {
        return scheduledFuture;
    }

    public void setScheduledFuture(ScheduledFuture<?> scheduledFuture) {
        this.scheduledFuture = scheduledFuture;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public TaskObject() { }

    public TaskObject(ScheduledFuture<?> scheduledFuture, String cron) {
        this.scheduledFuture = scheduledFuture;
        this.cron = cron;
    }
}
