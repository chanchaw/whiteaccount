package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.ResponseEnum;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;
import java.util.List;
import com.xdf.whiteaccount.entity.CpJrkbillfh;
import com.xdf.whiteaccount.service.CpJrkbillfhService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-06-25 09:44:52
 */
@Api(tags = "")
@CrossOrigin
@RestController
@RequestMapping("/cpJrkbillfh")
public class CpJrkbillfhController {

    @Autowired
    private CpJrkbillfhService service;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-06-25 09:44:52
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(CpJrkbillfh record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 批量新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-06-25 09:44:52
     */
    @ApiOperation(value = "批量新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequestMapping(value = "/multiInsert", method = RequestMethod.POST)
    public ResponseResult<Void> multiInsert(@RequestBody List<CpJrkbillfh> list) throws Exception {
        service.multiInsert(list);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : CpJrkbillfh
     * @Author : 张柯
     * @Date : 2021-06-25 09:44:52
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(CpJrkbillfh record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 批量修改
     * @Return : CpJrkbillfh
     * @Author : 张柯
     * @Date : 2021-06-25 09:44:52
     */
    @ApiOperation(value = "批量修改", httpMethod = "POST", notes = "自动生成的修改方法")
    @RequestMapping(value = "/multiUpdate", method = RequestMethod.POST)
    public ResponseResult<Void> multiUpdate(@RequestBody List<CpJrkbillfh> list) throws Exception {
        service.multiUpdate(list);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : CpJrkbillfh
     * @Author : 张柯
     * @Date : 2021-06-25 09:44:52
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public CpJrkbillfh selectByPrimaryKey(@PathVariable("id") Integer id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-06-25 09:44:52
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<CpJrkbillfh> query(CpJrkbillfh record) throws Exception {
        return service.listQuery(record);
    }
}