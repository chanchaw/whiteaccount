package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.entity.ResponseResult;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.service.CustomizeService;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-12 10:16
 **/
@RestController
@RequiresUser
@RequestMapping("/customize")
public class CustomizeController {
    @Autowired
    private CustomizeService customizeService;

    /**
     * 获取当前数据库名称
     *
     * @return
     */
    @GetMapping("/getDatabase")
    public ResponseResult<String> getDatabase() {
        return ResponseResult.<String>builder()
                .success(true)
                .message(ResponseEnum.OK.getName())
                .data(customizeService.getDataBase())
                .build();
    }
}
