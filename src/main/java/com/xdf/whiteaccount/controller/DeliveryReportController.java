package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.MenuConstant;
import com.xdf.whiteaccount.enums.OperateConstant;
import com.xdf.whiteaccount.enums.SeparatorConstant;
import com.xdf.whiteaccount.service.CallService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @program: white-account
 * @description: 发货管理(报表)
 * @author: 张柯
 * @create: 2021-05-30 16:31
 **/
@RestController
@RequestMapping("/delivery")
@RequiresPermissions(MenuConstant.MENU201 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
public class DeliveryReportController {
    private CallService callService;

    /**
     * @param callService
     */
    @Autowired
    public void setCallService(CallService callService) {
        this.callService = callService;
    }

    /**
     * 收货管理 - 应收应付 - 已收款
     * @param start
     * @param end
     * @param clientName
     * @param checkType
     * @return
     * @throws Exception
     */
    @GetMapping("/report/fhdtl")
    public List<Map<String, Object>> getFHhistoryDetail(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, String clientName, String checkType) throws Exception {
        return callService.getFHhistoryDetail(start, end, clientName, checkType);
    }

    /**
     * 应收应付 - 应收管理 - 应收账款
     *
     * @param start
     * @param end
     * @param clientName
     * @return
     * @throws Exception
     */
    @GetMapping("/report/receivedtl")
    public List<Map<String, Object>> getReceivablesDetail(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
                                                          @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
                                                          @RequestParam(value = "clientName", required = false) String clientName,
                                                          @RequestParam(value = "value", defaultValue = "0") Integer value) throws Exception {
        return callService.getReceivablesDetail(start, end, clientName, value);
    }

    /**
     * 2021年8月14日 16:28:27 chanchaw
     * 白坯管理 - 第二个标签页 - 白坯入库 - 表格数据源
     * @param start
     * @param end
     * @param clientName
     * @return
     * @throws Exception
     */
    @GetMapping("/report/greyfabric/input")
    public List<Map<String, Object>> getGreyfabricInput(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
                                                        @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
                                                        @RequestParam(value = "clientName", required = false) String clientName) throws Exception {
        return callService.getGreyfabricInput(start, end, clientName);
    }

    /**
     * @return
     * @throws Exception
     */
    @GetMapping("/report/receivesum")
    public List<Map<String, Object>> getReceivablesAllSum() throws Exception {
        return callService.getReceivablesAllSum();
    }

    /**
     * 获取白坯库存
     *
     * @return
     * @throws Exception
     */
    @GetMapping("/report/greyfabric/storage")
    public List<Map<String, Object>> getGreyfabricStorage(@RequestParam(value = "clientName", required = false) String clientName,
                                                          @RequestParam(value = "value", defaultValue = "0") Integer value,
                                                          @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "end") Date end) throws Exception {
        return callService.getGreyfabricStorage(clientName, value,end);
    }

    /**
     * 应收应付 - 应收管理 - 已收账款
     *
     * @param start      起始日期
     * @param end        终止日期
     * @param clientName 客户
     * @param value      是否汇总(默认否)
     * @return
     * @throws Exception
     */
    @GetMapping("/report/received")
    public List<Map<String, Object>> getCollectionOld(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
                                                      @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
                                                      @RequestParam(value = "clientName", required = false) String clientName,
                                                      @RequestParam(value = "value", defaultValue = "0") String value) throws Exception {
        return callService.getCollectionOld(start, end, clientName, value);
    }

    /**
     * 获取未剖幅报表
     *
     * @param start      起始日期
     * @param end        终止日期
     * @param clientName 客户
     * @return
     */
    @GetMapping("/report/unfinishedOutward")
    public List<Map<String, Object>> getUnfinishedOutward(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
                                                          @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
                                                          String clientName) throws Exception {
        return callService.getUnfinishedOutward(start, end, clientName);
    }

    /**
     * 废布入库
     *
     * @param start      起始日期
     * @param end        终止日期
     * @param clientName 客户
     * @return
     */
    @GetMapping("/report/waste")
    public List<Map<String, Object>> getProductWaste(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
                                                     @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
                                                     String clientName) throws Exception {
        return callService.getProductWaste(start, end, clientName);
    }
}
