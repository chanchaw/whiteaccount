package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.dto.TreeDTO;
import com.xdf.whiteaccount.entity.Employee;
import com.xdf.whiteaccount.enums.MenuConstant;
import com.xdf.whiteaccount.enums.OperateConstant;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.enums.SeparatorConstant;
import com.xdf.whiteaccount.service.EmployeeService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;
import java.util.List;
import com.xdf.whiteaccount.entity.Department;
import com.xdf.whiteaccount.service.DepartmentService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-05-19 20:26:32
 */
@Api(tags = "部门")
@CrossOrigin
@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private DepartmentService service;
    @Autowired
    private EmployeeService employeeService;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2020-12-31 14:42:38
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequiresPermissions(MenuConstant.MENU408 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(Department record) throws Exception {
        Assert.notNull(record, ResponseEnum.NOT_ALLOW_EMPTY_DATA.getName());
        Assert.notNull(record.getParentId(), "禁止新增根节点");
        if (record.getDDefault() == null)
            record.setDDefault(false);
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : Department
     * @Author : 张柯
     * @Date : 2020-12-31 14:42:38
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequiresPermissions(MenuConstant.MENU408 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(Department record) throws Exception {
        Department department = service.selectByPrimaryKey(record.getId());
        Assert.notNull(department, ResponseEnum.NO_ORDER.getName());
        Assert.state(department.getParentId() != null, "无法修改根节点");
        if (record.getDDefault() == null)
            record.setDDefault(false);
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : Department
     * @Author : 张柯
     * @Date : 2020-12-31 14:42:38
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public Department selectByPrimaryKey(@PathVariable("id") Integer id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2020-12-31 14:42:38
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<Department> query(Department record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2020-12-31 14:42:38
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequiresPermissions(MenuConstant.MENU408 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @RequestMapping(value = "/deleteByPk", method = RequestMethod.POST)
    public ResponseResult<Void> deleteByPrimaryKey(@RequestParam(value = "id") Integer id) throws Exception {
        Department department = service.selectByPrimaryKey(id);
        Assert.notNull(department, ResponseEnum.NO_ORDER.getName());
        Assert.state(department.getParentId() != null, "无法删除根节点");
        Assert.state(CollectionUtils.isEmpty(employeeService.listQuery(Employee.builder().departmentId(id).build())),
                "有关联员工，不允许删除！");
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 获取树
     *
     * @param parentId
     * @return
     */
    @GetMapping("/tree")
    public List<TreeDTO> getTreeNodes(Integer parentId) throws Exception {
        return service.listQueryTreeNodes(parentId);
    }
}