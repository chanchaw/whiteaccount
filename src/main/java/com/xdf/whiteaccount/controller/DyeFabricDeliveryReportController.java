package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.MenuConstant;
import com.xdf.whiteaccount.enums.OperateConstant;
import com.xdf.whiteaccount.enums.SeparatorConstant;
import com.xdf.whiteaccount.service.CallService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @program: white-account
 * @description: 色布发货管理(报表)
 * @author: 张柯
 * @create: 2021-05-30 16:31
 **/
@RestController
@RequestMapping("/dyeFabricDelivery")
@RequiresPermissions(MenuConstant.MENU801 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
public class DyeFabricDeliveryReportController {
    private CallService callService;

    /**
     * @param callService
     */
    @Autowired
    public void setCallService(CallService callService) {
        this.callService = callService;
    }

    /**
     * 色布发货
     *
     * @param start      起始日期
     * @param end        终止日期
     * @param clientName 客户名
     * @param checkType  类型
     * @return
     * @throws Exception
     */
    @GetMapping("/report/fhdtl")
    public List<Map<String, Object>> getFHhistoryDetail(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, String clientName, String checkType) throws Exception {
        return callService.getDyeFabricDelivery(start, end, clientName, checkType);
    }

    /**
     * 色布入库
     *
     * @param start
     * @param end
     * @param clientName
     * @return
     * @throws Exception
     */
    @GetMapping("/report/dyeFabric/input")
    public List<Map<String, Object>> getDyeFabricInput(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
                                                       @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
                                                       @RequestParam(value = "clientName", required = false) String clientName) throws Exception {
        return callService.getDyeInboundReport(start, end, clientName);
    }

    /**
     * 获取色布库存
     *
     * @return
     * @throws Exception
     */
    @GetMapping("/report/dyeFabric/storage")
    public List<Map<String, Object>> getGreyfabricStorage(@RequestParam(value = "clientName", required = false) String clientName,
                                                          @RequestParam(value = "value", defaultValue = "0") Integer value) throws Exception {
        List<Map<String, Object>> ret = callService.getDyeStorageReport(clientName, value);
        //System.out.println(ret);
        return ret;
    }
}