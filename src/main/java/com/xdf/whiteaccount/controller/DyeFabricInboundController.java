package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;
import java.util.List;
import com.xdf.whiteaccount.entity.DyeFabricInbound;
import com.xdf.whiteaccount.service.DyeFabricInboundService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-07-20 14:42:34
 */
@Api(tags = "色布入库单据 - dye_fabric_inbound")
@CrossOrigin
@RestController
@RequestMapping("/dyeFabricInbound")
public class DyeFabricInboundController {

    @Autowired
    private DyeFabricInboundService service;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-07-20 14:42:34
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    @RequiresPermissions(MenuConstant.MENU801 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    public ResponseResult<Void> insertSelective(@Validated DyeFabricInbound record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : DyeFabricInbound
     * @Author : 张柯
     * @Date : 2021-07-20 14:42:34
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    @RequiresPermissions(MenuConstant.MENU801 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    public ResponseResult<Void> updateSelective(@Validated DyeFabricInbound record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : DyeFabricInbound
     * @Author : 张柯
     * @Date : 2021-07-20 14:42:34
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public DyeFabricInbound selectByPrimaryKey(@PathVariable("id") Long id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-07-20 14:42:34
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<DyeFabricInbound> query(DyeFabricInbound record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 根据主键作废
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-07-20 14:42:34
     */
    @ApiOperation(value = "根据主键作废", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequestMapping(value = "/cancelByPk/{id}", method = RequestMethod.POST)
    @RequiresPermissions(MenuConstant.MENU801 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    public ResponseResult<Void> cancelByPrimaryKey(@PathVariable("id") Long id) throws Exception {
        service.cancelByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-07-20 14:42:34
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequestMapping(value = "/deleteByPk/{id}", method = RequestMethod.POST)
    @RequiresPermissions(MenuConstant.MENU801 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    public ResponseResult<Void> deleteByPrimaryKey(@PathVariable("id") Long id) throws Exception {
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据主键审核
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键审核", httpMethod = "GET", notes = "根据主键消审")
    @RequiresPermissions(MenuConstant.MENU801 + SeparatorConstant.SEPARATOR + OperateConstant.AUDIT)
    @PostMapping("/audit")
    public ResponseResult<Void> auditByPrimaryKey(Long[] id) throws Exception {
        service.auditById(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据主键审核
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键取消审核", httpMethod = "GET", notes = "根据主键取消审核")
    @RequiresRoles(value = RoleConstant.ADMIN_ROLE)
    @PostMapping("/cancelAudit")
    public ResponseResult<Void> cancelAuditByPrimaryKey(Long[] id) throws Exception {
        service.cancelAuditById(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }
}