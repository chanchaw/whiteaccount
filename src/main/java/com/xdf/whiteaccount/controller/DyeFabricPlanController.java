package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.entity.SysConfigDecimal;
import com.xdf.whiteaccount.enums.*;
import com.xdf.whiteaccount.service.CallService;
import com.xdf.whiteaccount.service.SysConfigDecimalService;
import com.xdf.whiteaccount.vo.DyeFabricPlanDetailVO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.xdf.whiteaccount.entity.DyeFabricPlan;
import com.xdf.whiteaccount.service.DyeFabricPlanService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-06-23 16:01:16
 */
@Api(tags = "色布计划 - 成品计划 - dye_fabric_plan")
@CrossOrigin
@RestController
@RequestMapping("/dyeFabricPlan")
public class DyeFabricPlanController {
    @Autowired
    private DyeFabricPlanService service;
    @Autowired
    private CallService callService;
    @Autowired
    private SysConfigDecimalService sysConfigDecimalService;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-06-23 16:01:16
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(@RequestBody DyeFabricPlan record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : DyeFabricPlan
     * @Author : 张柯
     * @Date : 2021-06-23 16:01:16
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(@RequestBody DyeFabricPlan record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : DyeFabricPlan
     * @Author : 张柯
     * @Date : 2021-06-23 16:01:16
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public DyeFabricPlan selectByPrimaryKey(@PathVariable("id") Long id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-06-23 16:01:16
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<DyeFabricPlan> query(DyeFabricPlan record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 查询报表
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "序时表", httpMethod = "GET/POST", notes = "序时表")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report")
    public List<Map<String, Object>> getPlanReport(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, Boolean markFinished, String textField, String valueField) throws Exception {
        if( markFinished == null ) markFinished = false;
//        System.out.println(start);
//        System.out.println(end);
//        System.out.println(markFinished);
//        System.out.println(textField);
//        System.out.println(valueField);
        return callService.getDyeFabricPlanReport(start, end, markFinished, textField, valueField);
    }

    /**
     * @Description : 已作废报表
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "已作废报表", httpMethod = "GET/POST", notes = "序时表")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report/canceled")
    public List<Map<String, Object>> getCanceledPlanReport() throws Exception {
        return callService.getCanceledDyeFabricPlan();
    }

    /**
     * @Description : 计划明细
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-06-27 09:40:12
     */
    @ApiOperation(value = "根据主键查询计划单明细", httpMethod = "GET", notes = "根据主键查询计划单明细")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report/detail/{id}")
    public DyeFabricPlanDetailVO getPlanDtl(@PathVariable("id") Long id) throws Exception {
        return callService.getDyePlanDtl(id);
    }

    /**
     * @Description : 设置完工状态
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "设置完工状态", httpMethod = "POST", notes = "自动生成的查询方法")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @RequestMapping(value = "/finished", method = RequestMethod.POST)
    public ResponseResult<Void> setFinishedState(@RequestParam("id") Long id, @RequestParam("finished") boolean finished) throws Exception {
        service.setFinishedMark(id, finished);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据主键删除
     *
     * @param id 主键
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键作废", httpMethod = "GET", notes = "根据主键作废")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @PostMapping("/cancelByPk")
    public ResponseResult<Void> cancelPlanByPk(@RequestParam("id") Long id) throws Exception {
        service.cancelByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据主键删除
     *
     * @param id 主键
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键删除", httpMethod = "GET", notes = "根据主键删除")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @PostMapping("/deleteByPk")
    public ResponseResult<Void> deletePlanByPk(@RequestParam("id") Long id) throws Exception {
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据主键恢复
     *
     * @param id 主键
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键恢复", httpMethod = "GET", notes = "根据主键恢复")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @PostMapping("/recoverByPk")
    public ResponseResult<Void> recoverPlanByPk(@RequestParam("id") Long id) throws Exception {
        service.recoverByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据主键审核
     *
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键审核", httpMethod = "GET", notes = "根据主键消审")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.AUDIT)
    @PostMapping("/audit")
    public ResponseResult<Void> auditByPrimaryKey(Long[] id) throws Exception {
        service.auditById(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据主键审核
     *
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键取消审核", httpMethod = "GET", notes = "根据主键取消审核")
    @RequiresRoles(value = RoleConstant.ADMIN_ROLE)
    @PostMapping("/cancelAudit")
    public ResponseResult<Void> cancelAuditByPrimaryKey(Long[] id) throws Exception {
        service.cancelAuditById(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 修改单价
     *
     * @param accountPrice
     * @param planid
     * @return
     */
    @ApiOperation(value = "修改单价", httpMethod = "GET", notes = "修改单价")
    @RequiresRoles(value = RoleConstant.ADMIN_ROLE)
    @PostMapping("/modify/price")
    public ResponseResult<Void> modifyPriceByPk(@RequestParam("accountPrice") BigDecimal accountPrice, @RequestParam("planid") Long planid) throws Exception {
        service.modifyPriceByPk(accountPrice, planid);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 获取计价标准
     *
     * @return
     */
    @ApiOperation(value = "获取计价标准", httpMethod = "GET", notes = "获取计价标准")
    @GetMapping("/list/pricestandard")
    public List<Map<String, Object>> getPriceStandard() throws Exception {
        return service.getPriceStandard();
    }
}