package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.MenuConstant;
import com.xdf.whiteaccount.enums.OperateConstant;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.enums.SeparatorConstant;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;
import java.util.List;
import com.xdf.whiteaccount.entity.DyeFabricPlanTemplate;
import com.xdf.whiteaccount.service.DyeFabricPlanTemplateService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-06-24 10:06:23
 */
@Api(tags = "色布计划模板 - dye_fabric_plan_template")
@CrossOrigin
@RestController
@RequestMapping("/dyeFabricPlanTemplate")
public class DyeFabricPlanTemplateController {

    @Autowired
    private DyeFabricPlanTemplateService service;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-06-24 10:06:23
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequiresPermissions(MenuConstant.MENU414 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(DyeFabricPlanTemplate record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : DyeFabricPlanTemplate
     * @Author : 张柯
     * @Date : 2021-06-24 10:06:23
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequiresPermissions(MenuConstant.MENU414 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(DyeFabricPlanTemplate record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : DyeFabricPlanTemplate
     * @Author : 张柯
     * @Date : 2021-06-24 10:06:23
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public DyeFabricPlanTemplate selectByPrimaryKey(@PathVariable("id") Long id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-06-24 10:06:23
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<DyeFabricPlanTemplate> query(DyeFabricPlanTemplate record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-06-24 10:06:23
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequiresPermissions(MenuConstant.MENU414 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @RequestMapping(value = "/deleteByPk", method = RequestMethod.POST)
    public ResponseResult<Void> deleteByPrimaryKey(@RequestParam("id") Long id) throws Exception {
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择默认模板
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-20 19:00:08
     */
    @ApiOperation(value = "查询默认模板", httpMethod = "DELETE", notes = "查询默认模板方法")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/listQuery/default")
    public DyeFabricPlanTemplate selectDefaultTemplate() throws Exception {
        return service.selectDefaultTemplate();
    }

    /**
     * @Description : 修改默认模板
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-06-24 19:00:08
     */
    @ApiOperation(value = "根据默认模板修改", httpMethod = "POST", notes = "根据默认模板修改")
    @RequiresPermissions(value = {MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY, MenuConstant.MENU414 + SeparatorConstant.SEPARATOR + OperateConstant.ADD}, logical = Logical.OR)
    @PostMapping(value = "/updateSelective/default")
    public ResponseResult<DyeFabricPlanTemplate> updateDefaultTemplate(DyeFabricPlanTemplate record) throws Exception {
        service.updateSelectiveDefault(record);
        return ResponseResult.<DyeFabricPlanTemplate>builder().success(true).message(ResponseEnum.OK.getName()).data(record).build();
    }
}