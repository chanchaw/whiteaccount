package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.dto.ExcelOutputParamDTO;
import com.xdf.whiteaccount.utils.ExcelUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;

/**
 * @program: xzy-management-system
 * @description:
 * @author: 张柯
 * @create: 2021-04-07 21:42
 **/
@RequestMapping(value = "/file")
@RestController
public class FileController {
    @Autowired
    private ExcelUtil excelUtil;

    /**
     * 导出Excel
     *
     * @param param
     * @param response
     */
    @ApiOperation(value = "导出Excel", httpMethod = "POST", notes = "导出Excel")
    @PostMapping("/export")
    public void exportExcel(@RequestBody ExcelOutputParamDTO param, HttpServletResponse response) throws Exception {
        OutputStream outputStream = response.getOutputStream();
        response.setHeader("Content-type", "application/vnd.ms-excel");
        if (StringUtils.isEmpty(param.getFileName())) {
            param.setFileName("自定义");
        }
        param.setFileSuffix(".xlsx");
        response.setHeader("Content-Disposition", "attachment;filename=" + param.getFileName() + param.getFileSuffix());
        excelUtil.writeExcel(outputStream, param);
    }
}
