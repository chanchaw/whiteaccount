package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.dto.MigrateStockDTO;
import com.xdf.whiteaccount.entity.JrkbillAdjust;
import com.xdf.whiteaccount.entity.ResponseResult;
import com.xdf.whiteaccount.enums.MenuConstant;
import com.xdf.whiteaccount.enums.OperateConstant;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.enums.SeparatorConstant;
import com.xdf.whiteaccount.service.JrkbillAdjustService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-06-04 17:17:21
 */
@Api(tags = "打卷调整表 - 白坯")
@CrossOrigin
@RestController
@RequestMapping("/jrkbillAdjust")
public class JrkbillAdjustController {

    @Autowired
    private JrkbillAdjustService service;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-06-04 17:17:21
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(JrkbillAdjust record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : JrkbillAdjust
     * @Author : 张柯
     * @Date : 2021-06-04 17:17:21
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(JrkbillAdjust record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 库存调整
     * @Return : JrkbillAdjust
     * @Author : 张柯
     * @Date : 2021-06-04 17:17:21
     */
    @ApiOperation(value = "库存调整", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    @RequestMapping(value = "/insertSelective/adjust", method = RequestMethod.POST)
    public ResponseResult<Void> storeModifyByPlanId(JrkbillAdjust record) throws Exception {
        service.storeModifyByPlanId(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : JrkbillAdjust
     * @Author : 张柯
     * @Date : 2021-06-04 17:17:21
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public JrkbillAdjust selectByPrimaryKey(@PathVariable("id") Integer id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-06-04 17:17:21
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<JrkbillAdjust> query(JrkbillAdjust record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-06-04 17:17:21
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @RequestMapping(value = "/deleteByPk", method = RequestMethod.POST)
    public ResponseResult<Void> deleteByPrimaryKey(@RequestParam("id") Integer id) throws Exception {
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    @ApiOperation(value = "移库", httpMethod = "POST", notes = "自动生成的修改方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    @RequestMapping(value = "/migrateStock", method = RequestMethod.POST)
    public ResponseResult<Integer> migrateStock(@RequestBody MigrateStockDTO params) throws Exception {
//        System.out.println("传入的参数：" + params.toString());
//        Long ido = Long.valueOf((String)params.get("ido"));
//        Long idt = Long.valueOf((String)params.get("idt"));
//        Integer pairs = Integer.valueOf((String) params.get("pairs"));
//        BigDecimal qty = BigDecimal.valueOf( Long.valueOf((String)params.get("qty")));
//
//        System.out.println("解析后的参数依次是：" + ido + "," + idt + "," + pairs + "," + qty);
//        System.out.println(params.toString());
        Integer ret = service.migrateStock(params.getIdo(),params.getIdt(),params.getPairs(),params.getQty());

        return ResponseResult.<Integer>builder().success(true).message(ResponseEnum.OK.getName()).data(ret).build();
    }
}