package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.MenuConstant;
import com.xdf.whiteaccount.enums.OperateConstant;
import com.xdf.whiteaccount.enums.SeparatorConstant;
import com.xdf.whiteaccount.service.CallService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-07-28 12:55
 **/
@RestController
@RequestMapping("/jrkbill")
public class JrkbillReportController {
    private CallService callService;

    /**
     * @param callService
     */
    @Autowired
    public void setCallService(CallService callService) {
        this.callService = callService;
    }


    /**
     * 白坯产量明细表
     *
     * @param start      起始日期
     * @param end        终止日期
     * @param machineNum 机台号
     * @return
     * @throws Exception
     */
    @GetMapping("/report/greyfabric")
    @RequiresPermissions(MenuConstant.MENU202 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    public List<Map<String, Object>> getGreyfabricDetail(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
                                                         @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
                                                         @RequestParam(value = "machineNum", required = false) String machineNum)
            throws Exception {
        return callService.getGreyFabricJrkbillReport(start, end, machineNum);
    }

    /**
     * 白坯产量汇总表
     *
     * @param start      起始日期
     * @param end        终止日期
     * @return
     * @throws Exception
     */
    @GetMapping("/report/greysummary")
    @RequiresPermissions(MenuConstant.MENU202 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    public List<Map<String, Object>> getGreysummary(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
                                                    @DateTimeFormat(pattern = "yyyy-MM-dd") Date end ) throws Exception {
        return callService.getGreySummaryJrkbillReport(start, end);
    }
}
