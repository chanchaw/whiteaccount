package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.*;
import com.xdf.whiteaccount.service.CallService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;

import java.util.*;

import com.xdf.whiteaccount.entity.MaterialOrder;
import com.xdf.whiteaccount.service.MaterialOrderService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-05-25 17:02:01
 */
@Api(tags = "原料入库")
@CrossOrigin
@RestController
@RequestMapping("/materialOrder")
public class MaterialOrderController {
    @Autowired
    private MaterialOrderService service;
    @Autowired
    private CallService callService;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-25 17:02:01
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequiresPermissions(MenuConstant.MENU301 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<MaterialOrder> insertSelective(MaterialOrder record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<MaterialOrder>builder().success(true).message(ResponseEnum.OK.getName()).data(record).build();
    }

    /**
     * @Description : 选择修改
     * @Return : MaterialOrder
     * @Author : 张柯
     * @Date : 2021-05-25 17:02:01
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequiresPermissions(MenuConstant.MENU301 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<MaterialOrder> updateSelective(MaterialOrder record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<MaterialOrder>builder().success(true).message(ResponseEnum.OK.getName()).data(record).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : MaterialOrder
     * @Author : 张柯
     * @Date : 2021-05-25 17:02:01
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public MaterialOrder selectByPrimaryKey(@PathVariable("id") Long id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-05-25 17:02:01
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<MaterialOrder> query(MaterialOrder record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-25 17:02:01
     */
    @ApiOperation(value = "根据主键作废方法", httpMethod = "DELETE", notes = "自动生成的作废方法")
    @RequiresPermissions(MenuConstant.MENU301 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @RequestMapping(value = "/cancelByPk", method = RequestMethod.POST)
    public ResponseResult<Void> cancelByPrimaryKey(@RequestParam("id") Long id) throws Exception {
        service.cancelByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 原料管理 - 第一个标签页 - 原料入库
     * @param start        起始日期
     * @param end          终止日期
     * @param supplierName 供应商
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "报表查询", httpMethod = "GET/POST", notes = "报表查询")
    @RequiresPermissions(MenuConstant.MENU301 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report")
    public List<Map<String, Object>> getMaterialOrderReport(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
                                                            @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
                                                            String supplierName) throws Exception {
        return callService.getMaterialOrderReport(start, end, supplierName);
    }

    /**
     * 原料管理 - 第二个标签页 - 原料库存
     *
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "原料库存", httpMethod = "GET/POST", notes = "报表查询")
    @RequiresPermissions(MenuConstant.MENU301 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report/storage")
    public List<Map<String, Object>> getMaterialStorage(String supplierName) throws Exception {
        return callService.getMaterialStorage(supplierName);
    }

    /**
     * 原料库存
     *
     * @param materialSpec 原料规格
     * @param selected     选中的批次号
     * @param mode         模式(0;库存不为0;1:全部;)
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "原料库存", httpMethod = "GET/POST", notes = "报表查询")
    @RequiresPermissions(MenuConstant.MENU301 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/dropdown/storage")
    public List<Map<String, Object>> getMaterialStorageDropdownList(@RequestParam("materialSpec") String materialSpec,
                                                                    @RequestParam(value = "selected", required = false) String[] selected,
                                                                    @RequestParam(value = "mode", defaultValue = "0") int mode) throws Exception {
        return service.getMaterialStorageDropdownList(materialSpec, selected, mode);
    }

    /**
     * 原料管理 - 第三个标签页 - 原料出库
     *
     * @param start 起始日期
     * @param end   终止日期
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "出库报表", httpMethod = "GET/POST", notes = "报表查询")
    @RequiresPermissions(MenuConstant.MENU301 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report/outbound")
    public List<Map<String, Object>> getMaterialOutboundReport(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, String processName) throws Exception {
        return callService.getMaterialOutbound(start, end, processName);
    }

    /**
     * 应收应付 - 应付管理 - 应付款
     *
     * @param start         起始日期
     * @param end           终止日期
     * @param supplierName  供应商名称
     * @param value         是否汇总
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "应付款报表", httpMethod = "GET/POST", notes = "报表查询")
    @RequiresPermissions(MenuConstant.MENU301 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report/accountpayable")
    public List<Map<String, Object>> getAccountPayable(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, String supplierName, Integer value) throws Exception {
        return callService.getAccountPayable(start, end, supplierName, value);
    }

    /**
     * 应收应付 - 应付管理 - 已付款
     *
     * @param start        起始日期
     * @param end          终止日期
     * @param supplierName 供应商名称
     * @param value        是否汇总
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "已付款报表", httpMethod = "GET/POST", notes = "报表查询")
    @RequiresPermissions(MenuConstant.MENU301 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report/accountpayment")
    public List<Map<String, Object>> getAccountPayment(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, String supplierName, Integer value) throws Exception {
        return callService.getAccountPayment(start, end, supplierName, value);
    }

    /**
     * 收款方式
     *
     * @return
     */
    @ApiOperation(value = "收款方式", httpMethod = "GET/POST", notes = "收款方式")
    @RequiresPermissions(MenuConstant.MENU301 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/listQuery/accountType")
    public List<Map<String, Object>> getAccountType() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (AccountEnum e : AccountEnum.values()) {
            Map<String, Object> map = new HashMap<>();
            map.put("value", e.getValue());
            list.add(map);
        }
        return list;
    }

    /**
     * 根据主键审核
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键审核", httpMethod = "GET", notes = "根据主键消审")
    @RequiresPermissions(MenuConstant.MENU301 + SeparatorConstant.SEPARATOR + OperateConstant.AUDIT)
    @PostMapping("/audit")
    public ResponseResult<Void> auditByPrimaryKey(Long[] id) throws Exception {
        service.auditById(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据主键审核
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键取消审核", httpMethod = "GET", notes = "根据主键取消审核")
    @RequiresRoles(value = RoleConstant.ADMIN_ROLE)
    @PostMapping("/cancelAudit")
    public ResponseResult<Void> cancelAuditByPrimaryKey(Long[] id) throws Exception {
        service.cancelAuditById(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }
}