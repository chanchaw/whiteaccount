package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.entity.MeterLength;
import com.xdf.whiteaccount.entity.ResponseResult;
import com.xdf.whiteaccount.enums.MenuConstant;
import com.xdf.whiteaccount.enums.OperateConstant;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.enums.SeparatorConstant;
import com.xdf.whiteaccount.service.MeterLengthService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-08-04 15:05
 **/
@RestController
@RequestMapping("/meterLength")
public class MeterLengthController {
    @Autowired
    private MeterLengthService service;

    /**
     * 新增
     *
     * @param record
     * @return
     */
    @PostMapping("/insertSelective")
    @RequiresPermissions(MenuConstant.MENU420 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    public ResponseResult<Void> insert(MeterLength record) {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 修改
     *
     * @param record
     * @return
     */
    @PostMapping("/updateSelective")
    @RequiresPermissions(MenuConstant.MENU420 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    public ResponseResult<Void> update(MeterLength record) {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @PostMapping("/deleteByPk")
    @RequiresPermissions(MenuConstant.MENU420 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    public ResponseResult<Void> deleteByPk(Integer id) {
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 查询
     *
     * @param record
     * @return
     */
    @GetMapping("/listQuery")
    public List<MeterLength> query(MeterLength record) {
        return service.listQuery(record);
    }
}