package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.ResponseEnum;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;

import java.util.List;

import com.xdf.whiteaccount.entity.PlanBatchnum;
import com.xdf.whiteaccount.service.PlanBatchnumService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-07-19 09:24:54
 */
@Api(tags = "白坯计划与批次号的对应关系")
@CrossOrigin
@RestController
@RequestMapping("/planBatchnum")
public class PlanBatchnumController {

    @Autowired
    private PlanBatchnumService service;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-07-19 09:24:54
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(PlanBatchnum record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : PlanBatchnum
     * @Author : 张柯
     * @Date : 2021-07-19 09:24:54
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(PlanBatchnum record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : PlanBatchnum
     * @Author : 张柯
     * @Date : 2021-07-19 09:24:54
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public PlanBatchnum selectByPrimaryKey(@PathVariable("id") Long id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-07-19 09:24:54
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<PlanBatchnum> query(PlanBatchnum record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-07-19 09:24:54
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery/planCode", method = RequestMethod.GET)
    public List<PlanBatchnum> queryByPlanCode(String planCode) throws Exception {
        return service.queryByPlanCode(planCode);
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-07-19 09:24:54
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequestMapping(value = "/deleteByPk/{id}", method = RequestMethod.POST)
    public ResponseResult<Void> deleteByPrimaryKey(@PathVariable("id") Long id) throws Exception {
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }
}