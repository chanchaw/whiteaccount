package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.controller.base.BaseController;
import com.xdf.whiteaccount.dto.PlanDTO;
import com.xdf.whiteaccount.entity.Plan;
import com.xdf.whiteaccount.entity.ResponseResult;
import com.xdf.whiteaccount.enums.*;
import com.xdf.whiteaccount.service.CallService;
import com.xdf.whiteaccount.service.PlanDTOService;
import com.xdf.whiteaccount.service.PlanService;
import com.xdf.whiteaccount.vo.PlanDetailVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
@Api(tags = "计划")
@CrossOrigin
@RestController
@RequestMapping("/plan")
public class PlanController extends BaseController {
    @Autowired
    private PlanService service;
    @Autowired
    private PlanDTOService planDTOService;
    @Autowired
    private CallService callService;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<PlanDTO> insertSelective(@Validated @RequestBody PlanDTO record) throws Exception {
        PlanDTO result = planDTOService.insertSelective(record);
        return ResponseResult.<PlanDTO>builder().success(true).message(ResponseEnum.OK.getName()).data(result).build();
    }

    /**
     * @Description : 选择修改
     * @Return : Plan
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<PlanDTO> updateSelective(@Validated @RequestBody PlanDTO record) throws Exception {
        PlanDTO result = planDTOService.updateSelective(record);
        return ResponseResult.<PlanDTO>builder().success(true).message(ResponseEnum.OK.getName()).data(result).build();
    }

    /**
     * @Description : 根据主键查询计划全部信息
     * @Return : Plan
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/queryDtoByPk/{id}", method = RequestMethod.GET)
    public PlanDTO selectDtoByPrimaryKey(@PathVariable("id") Long id) throws Exception {
        if (id == null) return null;
        return planDTOService.selectByPrimaryKey(id);
    }

    /**
     * @Description : 根据主键查询
     * @Return : Plan
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public Plan selectByPrimaryKey(@PathVariable("id") Long id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<Plan> query(Plan record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 根据条件查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/queryByExample", method = RequestMethod.GET)
    public List<Map<String, Object>> getPlanReportDtl(@RequestParam("state") Integer state, @RequestParam("markFinished") Boolean markFinished) {
        return callService.getPlanReportDtl(state, markFinished);
    }

    /**
     * @Description : 设置完工状态
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "设置完工状态", httpMethod = "POST", notes = "自动生成的查询方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @RequestMapping(value = "/finished", method = RequestMethod.POST)
    public ResponseResult<Void> setFinishedState(@RequestParam("id") Long id, @RequestParam("finished") boolean finished) throws Exception {
        service.setFinishedMark(id, finished);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 查询报表
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "序时表", httpMethod = "GET/POST", notes = "序时表")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report")
    public List<Map<String, Object>> getPlanReport(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, Boolean markFinished, String textField, String valueField) throws Exception {
        if( markFinished == null ) markFinished = false;
//        System.out.println(start);
//        System.out.println(end);
//        System.out.println(markFinished);
//        System.out.println(textField);
//        System.out.println(valueField);
        return callService.getPlanReport(start, end, markFinished, textField, valueField);
    }

    /**
     * @Description : 已作废报表
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "已作废报表", httpMethod = "GET/POST", notes = "序时表")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report/canceled")
    public List<Map<String, Object>> getCanceledPlanReport() throws Exception {
        return callService.getCanceledPlan();
    }

    /**
     * @Description : 计划明细
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "根据主键查询计划单明细", httpMethod = "GET", notes = "根据主键查询计划单明细")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report/detail/{id}")
    public PlanDetailVO getPlanDtl(@PathVariable("id") Long id) throws Exception {
        return callService.getPlanDtl(id);
    }

    /**
     * 根据主键删除
     *
     * @param id 主键
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键作废", httpMethod = "GET", notes = "根据主键作废")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @PostMapping("/cancelByPk")
    public ResponseResult<Void> cancelPlanByPk(@RequestParam("id") Long id) throws Exception {
        service.cancelByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据主键删除
     *
     * @param id 主键
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键删除", httpMethod = "GET", notes = "根据主键删除")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @PostMapping("/deleteByPk")
    public ResponseResult<Void> deletePlanByPk(@RequestParam("id") Long id) throws Exception {
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据主键恢复
     *
     * @param id 主键
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键恢复", httpMethod = "GET", notes = "根据主键恢复")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @PostMapping("/recoverByPk")
    public ResponseResult<Void> recoverPlanByPk(@RequestParam("id") Long id) throws Exception {
        service.recoverPlanByPk(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据主键审核
     *
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键审核", httpMethod = "GET", notes = "根据主键消审")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.AUDIT)
    @PostMapping("/audit")
    public ResponseResult<Void> auditByPrimaryKey(Long[] id) throws Exception {
        service.auditById(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据主键审核
     *
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键取消审核", httpMethod = "GET", notes = "根据主键取消审核")
    @RequiresRoles(value = RoleConstant.ADMIN_ROLE)
    @PostMapping("/cancelAudit")
    public ResponseResult<Void> cancelAuditByPrimaryKey(Long[] id) throws Exception {
        service.cancelAuditById(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 审核所有
     *
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "审核所有", httpMethod = "GET", notes = "审核所有")
    @RequiresRoles(value = RoleConstant.ADMIN_ROLE)
    @PostMapping("/auditAll")
    public ResponseResult<Void> 审核所有() throws Exception {
        service.auditAll();
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 修改单价
     *
     * @param accountPrice
     * @param planid
     * @return
     */
    @ApiOperation(value = "修改单价", httpMethod = "GET", notes = "修改单价")
    @RequiresRoles(value = RoleConstant.ADMIN_ROLE)
    @PostMapping("/modify/price")
    public ResponseResult<Void> modifyPriceByPk(@RequestParam("accountPrice") BigDecimal accountPrice, @RequestParam("planid") Long planid) throws Exception {
        service.modifyPriceByPk(accountPrice, planid);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 设置排单显示的状态
     *
     * @param state
     * @param id
     * @return
     */
    @ApiOperation(value = "设置排单显示的状态", httpMethod = "POST", notes = "设置排单显示的状态")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @PostMapping("/modify/showstate")
    public ResponseResult<Void> setPlanOrderShowState(@RequestParam("state") boolean state, @RequestParam("id") Long id) throws Exception {
        service.setPlanOrderShowState(state, new Long[]{id});
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    @ApiOperation(value = "移库模态窗表格数据源", httpMethod = "GET/POST", notes = "移库模态窗表格数据源")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/reportExclude")
    public ResponseResult<List<Map<String, Object>>> getPlanReportExclude(@RequestBody Map<String,Object> params) throws Exception {
        System.out.println("前端传递来的参数："+ params.toString());
//        Date start = DatetimeUtils.parseDate((String) params.get("start"),"yyyy-MM-dd");
//        Date end = DatetimeUtils.parseDate((String) params.get("end"),"yyyy-MM-dd");
        Date start = new Date();
        Date end = new Date();
        Long id = Long.valueOf((String) params.get("id"));
        List<Map<String, Object>> ret = callService.getPlanReportExclude(start, end, id);
        return ResponseResult.<List<Map<String, Object>>>builder().success(true).message(ResponseEnum.OK.getName()).data(ret).build();
    }
}