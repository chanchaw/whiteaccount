package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.MenuConstant;
import com.xdf.whiteaccount.enums.OperateConstant;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.enums.SeparatorConstant;
import com.xdf.whiteaccount.service.CallService;
import com.xdf.whiteaccount.utils.CustumDozerMapperUtil;
import com.xdf.whiteaccount.vo.PlanTemplateVO;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;

import java.util.List;

import com.xdf.whiteaccount.entity.PlanTemplate;
import com.xdf.whiteaccount.service.PlanTemplateService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-05-20 19:00:08
 */
@Api(tags = "计划单模板")
@CrossOrigin
@RestController
@RequestMapping("/planTemplate")
public class PlanTemplateController {
    @Autowired
    private PlanTemplateService service;
    @Autowired
    private CustumDozerMapperUtil utils;
    @Autowired
    private CallService callService;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-20 19:00:08
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequiresPermissions(MenuConstant.MENU409 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(PlanTemplate record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : PlanTemplate
     * @Author : 张柯
     * @Date : 2021-05-20 19:00:08
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequiresPermissions(MenuConstant.MENU409 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(PlanTemplate record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : PlanTemplate
     * @Author : 张柯
     * @Date : 2021-05-20 19:00:08
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public PlanTemplate selectByPrimaryKey(@PathVariable("id") Long id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-05-20 19:00:08
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequiresPermissions(MenuConstant.MENU409 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<PlanTemplateVO> query(PlanTemplate record) throws Exception {
        List<PlanTemplate> list = service.listQuery(record);
        return utils.transSourceToTarget(list, PlanTemplateVO.class);
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-20 19:00:08
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequiresPermissions(MenuConstant.MENU409 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @RequestMapping(value = "/deleteByPk", method = RequestMethod.POST)
    public ResponseResult<Void> deleteByPrimaryKey(@RequestParam("id") Long id) throws Exception {
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择默认模板
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-20 19:00:08
     */
    @ApiOperation(value = "查询默认模板", httpMethod = "GET/POST", notes = "查询默认模板的方法")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/listQuery/default")
    public PlanTemplateVO selectDefaultTemplate() throws Exception {
        return service.selectDefaultTemplate();
    }

    /**
     * @Description : 复制
     * @Return : ResponseResult<Void>
     * @Author : szx
     * @Date : 2021年11月4日13:26:27
     */
    @ApiOperation(value = "复制", httpMethod = "GET/POST", notes = "复制效果")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/listQuery/copy")
    public PlanTemplateVO copy() throws Exception {
        return service.copy();
    }

    @GetMapping("/getNewPlanCode")
    public ResponseResult<String> getNewPlanCode(){
        String newPlanCode = callService.planCodeGen();
        return ResponseResult.<String>builder().success(true).message(ResponseEnum.OK.getName()).data(newPlanCode).build();
    }

    /**
     * @Description : 修改默认模板
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-20 19:00:08
     */
    @ApiOperation(value = "根据默认模板修改", httpMethod = "POST", notes = "根据默认模板修改")
    @RequiresPermissions(value = {MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY,
            MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.ADD}, logical = Logical.OR)
    @PostMapping(value = "/updateSelective/default")
    public ResponseResult<PlanTemplate> updateDefaultTemplate(PlanTemplate record) throws Exception {
        service.updateSelectiveDefault(record);
        return ResponseResult.<PlanTemplate>builder().success(true).message(ResponseEnum.OK.getName()).data(record).build();
    }
}