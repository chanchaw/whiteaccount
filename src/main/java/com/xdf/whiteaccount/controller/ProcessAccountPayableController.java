package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.MenuConstant;
import com.xdf.whiteaccount.enums.OperateConstant;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.enums.SeparatorConstant;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;
import java.util.List;
import com.xdf.whiteaccount.entity.ProcessAccountPayable;
import com.xdf.whiteaccount.service.ProcessAccountPayableService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-07-16 12:10:29
 */
@Api(tags = "外加工应付款")
@CrossOrigin
@RestController
@RequiresPermissions(MenuConstant.MENU703 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
@RequestMapping("/processAccountPayable")
public class ProcessAccountPayableController {

    @Autowired
    private ProcessAccountPayableService service;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-07-16 12:10:29
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    @RequiresPermissions(MenuConstant.MENU703 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    public ResponseResult<Void> insertSelective(ProcessAccountPayable record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : ProcessAccountPayable
     * @Author : 张柯
     * @Date : 2021-07-16 12:10:29
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    @RequiresPermissions(MenuConstant.MENU703 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    public ResponseResult<Void> updateSelective(ProcessAccountPayable record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : ProcessAccountPayable
     * @Author : 张柯
     * @Date : 2021-07-16 12:10:29
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public ProcessAccountPayable selectByPrimaryKey(@PathVariable("id") Integer id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-07-16 12:10:29
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<ProcessAccountPayable> query(ProcessAccountPayable record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-07-16 12:10:29
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequestMapping(value = "/deleteByPk", method = RequestMethod.POST)
    @RequiresPermissions(MenuConstant.MENU703 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    public ResponseResult<Void> deleteByPrimaryKey(@RequestParam("id") Integer id) throws Exception {
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }
}