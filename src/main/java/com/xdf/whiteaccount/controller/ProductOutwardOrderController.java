package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.*;
import com.xdf.whiteaccount.service.CallService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.xdf.whiteaccount.entity.ProductOutwardOrder;
import com.xdf.whiteaccount.service.ProductOutwardOrderService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-07-02 17:20:51
 */
@Api(tags = "外发入库单据")
@CrossOrigin
@RestController
@RequestMapping("/productOutwardOrder")
public class ProductOutwardOrderController {
    @Autowired
    private ProductOutwardOrderService service;
    @Autowired
    private CallService callService;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-07-02 17:20:51
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequiresPermissions(MenuConstant.MENU601 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(ProductOutwardOrder record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : ProductOutwardOrder
     * @Author : 张柯
     * @Date : 2021-07-02 17:20:51
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequiresPermissions(MenuConstant.MENU601 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(ProductOutwardOrder record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : ProductOutwardOrder
     * @Author : 张柯
     * @Date : 2021-07-02 17:20:51
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public ProductOutwardOrder selectByPrimaryKey(@PathVariable("id") Long id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-07-02 17:20:51
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<ProductOutwardOrder> query(ProductOutwardOrder record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-07-02 17:20:51
     */
    @ApiOperation(value = "根据主键作废", httpMethod = "DELETE", notes = "根据主键作废")
    @RequiresPermissions(MenuConstant.MENU601 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @RequestMapping(value = "/cancelByPk", method = RequestMethod.POST)
    public ResponseResult<Void> cancelByPk(@RequestParam("id") Long id) throws Exception {
        service.cancelByPk(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 外发管理 - 第一个标签页 - 原料外发表格数据源
     *
     * @param start
     * @param end
     * @param processName
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "原料外发", httpMethod = "GET", notes = "原料外发")
    @RequiresPermissions(MenuConstant.MENU601 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report/materialoutward", method = RequestMethod.GET)
    public List<Map<String, Object>> getMaterialOutward(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, @RequestParam(value = "processName", required = false) String processName) throws Exception {
        return callService.getMaterialOutward(start, end, processName);
    }

    /**
     * 外发管理 - 第二个标签页 - 坯布入库表格数据源
     * @param start
     * @param end
     * @param processName
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "外发管理 - 第二个标签页 - 坯布入库", httpMethod = "GET", notes = "外发管理 - 第二个标签页 - 坯布入库")
    @RequiresPermissions(MenuConstant.MENU601 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report/productinbound", method = RequestMethod.GET)
    public List<Map<String, Object>> getProductInboundReport(@DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, @RequestParam(value = "processName", required = false) String processName) throws Exception {
        return callService.getProductInboundReport(start, end, processName);
    }

    /**
     * 报表-原料外发入库
     *
     * @param start
     * @param end
     * @param processName
     * @param mode
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "加工户对账单", httpMethod = "GET", notes = "加工户对账单")
    @RequiresPermissions(MenuConstant.MENU703 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @RequestMapping(value = "/report/account", method = RequestMethod.GET)
    public List<Map<String, Object>> getProcessCompanyAccount(
            @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
            @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
            @RequestParam(value = "processName", required = false) String processName,
            @RequestParam(value = "mode", defaultValue = "0") Integer mode) {
        return callService.getProcesscompanyAccount(start, end, processName, mode);
    }

    /**
     * 根据主键审核
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键审核", httpMethod = "GET", notes = "根据主键消审")
    @RequiresPermissions(MenuConstant.MENU601 + SeparatorConstant.SEPARATOR + OperateConstant.AUDIT)
    @PostMapping("/audit")
    public ResponseResult<Void> auditByPrimaryKey(Long[] id) throws Exception {
        service.auditById(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据主键审核
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据主键取消审核", httpMethod = "GET", notes = "根据主键取消审核")
    @RequiresRoles(value = RoleConstant.ADMIN_ROLE)
    @PostMapping("/cancelAudit")
    public ResponseResult<Void> cancelAuditByPrimaryKey(Long[] id) throws Exception {
        service.cancelAuditById(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 2021年8月20日 09:47:16 chanchaw
     * 测试发现 1.7 版本的 shiro 在处理 get 请求中带有中文时会报错 400
     * 后来弃用该方法，采用 post 方式获取数据，即使用上面的 getUnImportedJRK
     * @param params map 类型，只有一个属性 processCompname
     * @return 返回打卷表没有导入过的数据
     * @throws Exception 一般的异常
     */
    @PostMapping("/getUnImportedJRK")
    public ResponseResult<List<Map<String, Object>>> getUnImportedJRK(@RequestBody Map<String,Object> params) throws Exception {
        String processCompname = (String) params.get("processCompname");
        List<Map<String, Object>> list =  callService.getUnImportedJRK(processCompname);
        return ResponseResult.<List<Map<String, Object>>>builder().success(true).data(list).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 2021年8月20日 09:50:48 chanchaw
     * 将指定的加工户对应的机台号的打卷数据导入为外发白坯入库的数据
     * 在 2021年8月20日 09:51:27 之前是按照规格汇总导入
     * 此后发现应该是按照计划单主键汇总导入
     * @param params key:processCompanyName、ids
     * @return 新增的外发入库的行数
     * @throws Exception 新增数据时产生的异常
     */
    @PostMapping("/insertByGatheredJrk")
    public ResponseResult<Integer> insertByGatheredJrk(@RequestBody Map<String,Object> params) throws Exception {
        String processCompanyName = (String) params.get("processCompanyName");
        List<Long> ids = (List<Long>) params.get("ids");
        Integer ret =  service.insertByGatheredJrk(ids,processCompanyName);
        return ResponseResult.<Integer>builder().success(true).data(ret).message(ResponseEnum.OK.getName()).build();
    }

    @PostMapping("/mapping8jrk")
    public ResponseResult<Integer> mapping8jrk(@RequestBody Map<String,Object> params) throws Exception {
        String processCompanyName = (String) params.get("processCompanyName");
        List<Long> ids = (List<Long>) params.get("ids");
        Integer ret =  service.mapping8jrk(ids,processCompanyName);
        return ResponseResult.<Integer>builder().success(true).data(ret).message(ResponseEnum.OK.getName()).build();
    }
}