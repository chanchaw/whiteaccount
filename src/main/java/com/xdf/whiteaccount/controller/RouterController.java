package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.controller.base.BaseController;
import com.xdf.whiteaccount.enums.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @program: white-account
 * @description: 路由页面
 * @author: 张柯
 * @create: 2021-05-17 08:08
 **/
@Api(tags = "网页路径路由")
@Controller
public class RouterController extends BaseController {
    @Value("${app.version}")
    private String version;
    private static final String MENUID = "MENU_ID";
    private static final String CLIENT_TITLE = "CLIENT_TITLE";
    private static final String USER_NAME = "USER_NAME";
    private static final String REPORT_DATE_RANGE = "REPORT_DATE_RANGE";

    /**
     * 登录
     *
     * @return
     */
    @RequestMapping("/login")
    @ApiOperation(value = "登录页面", httpMethod = "GET", notes = "页面接口")
    public String login(Model model) throws Exception {
        if (isLogin()) {
            return "redirect:/home";
        }
        model.addAttribute("主页-标题", "后台管理系统");
        return "pages/login/login";
    }

    /**
     * 主页
     *
     * @param model
     * @return
     */
    @RequestMapping(value = {"/home", "/"})
    @RequiresUser
    @ApiOperation(value = "主页面", httpMethod = "GET", notes = "页面接口")
    public String indexPage(Model model) throws Exception {
        model.addAttribute(CLIENT_TITLE, getStringValueByConfigKey(ConfigVarEnum.CLIENT_NAME.getField()));
        model.addAttribute(REPORT_DATE_RANGE, getIntegerValueByConfigKey(ConfigIntEnum.REPORT_DATE_RANGE.getField()));
        model.addAttribute(USER_NAME, getLoginUserName());
        model.addAttribute("VERSION", version);
        return "index";
    }

    /**
     * 导航
     *
     * @param model
     * @return
     */
    @RequestMapping(value = {"/navigate"})
    @RequiresUser
    @ApiOperation(value = "导航", httpMethod = "GET", notes = "页面接口")
    public String navigatePage(Model model) throws Exception {
        return "navigate/navigate";
    }

    /**
     * 生产计划-计划单序时表
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jh_plan_report")
    @RequiresPermissions(MenuConstant.MENU101 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "计划管理-计划单序时表", httpMethod = "GET", notes = "页面接口")
    public String jhPlanReport(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU101);
        return "pages/jh/jh_plan_report";
    }

    /**
     * 生产计划-色布计划
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jh_dye_fabric_plan_report")
    @RequiresPermissions(MenuConstant.MENU102 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "计划管理-色布计划单序时表", httpMethod = "GET", notes = "页面接口")
    public String jhDyeFabricPlanReport(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU101);
        return "pages/jh/jh_dye_plan_report";
    }

    /**
     * 色布发货
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/fh_dye_delivery_report")
    @RequiresPermissions(MenuConstant.MENU801 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "成品管理-成品管理", httpMethod = "GET", notes = "页面接口")
    public String fhDyeDeliveryReport(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU801);
        return "pages/fh/fh_dye_delivery_report";
    }

    /**
     * 白坯发货
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/fh_delivery_report")
    @RequiresPermissions(MenuConstant.MENU201 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "发货-白坯发货", httpMethod = "GET", notes = "页面接口")
    public String fhDeliveryReport(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU201);
        return "pages/fh/fh_delivery_report";
    }

    /**
     * 生产计划-机台产量
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/fh_machine_report")
    @RequiresPermissions(MenuConstant.MENU202 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "计划管理-机台产量", httpMethod = "GET", notes = "页面接口")
    public String jhMachineReport(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU202);
        return "pages/fh/fh_machine_report";
    }

    /**
     * 发货管理
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/yf_material_order_report")
    @RequiresPermissions(MenuConstant.MENU301 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "应付管理-原料序时表", httpMethod = "GET", notes = "页面接口")
    public String yfMaterialOrderReport(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU301);
        return "pages/yf/yf_material_order_report";
    }

    /**
     * 发货管理
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/yf_received_report")
    @RequiresPermissions(MenuConstant.MENU302 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "收货管理-应收应付", httpMethod = "GET", notes = "页面接口")
    public String yfReceivedReport(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU302);
        return "pages/yf/yf_received_report";
    }

    /**
     * 外发管理-原料外发
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/wf_material_send_report")
    @RequiresPermissions(MenuConstant.MENU601 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "外发管理-原料外发", httpMethod = "GET", notes = "页面接口")
    public String wfMaterialSendReport(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU601);
        return "pages/wf/wf_material_send_report";
    }

    /**
     * 应收应付-应收账款
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/ysyf_receivable_report")
    @RequiresPermissions(MenuConstant.MENU701 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "应收应付-应收账款", httpMethod = "GET", notes = "页面接口")
    public String ysyfReceivableReport(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU701);
        return "pages/ysyf/ysyf_receivable_report";
    }

    /**
     * 应收应付-应付账款
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/ysyf_payable_report")
    @RequiresPermissions(MenuConstant.MENU702 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "应收应付-应付账款", httpMethod = "GET", notes = "页面接口")
    public String ysyfPayableReport(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU702);
        return "pages/ysyf/ysyf_payable_report";
    }

    /**
     * 应收应付-外发对账
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/ysyf_outward_report")
    @RequiresPermissions(MenuConstant.MENU703 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "应收应付-外发对账", httpMethod = "GET", notes = "页面接口")
    public String ysyfOutwardReport(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU703);
        return "pages/ysyf/ysyf_outward_report";
    }

    /**
     * 基础资料-往来单位
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_contactcompany")
    @RequiresPermissions(MenuConstant.MENU401 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-往来单位", httpMethod = "GET", notes = "页面接口")
    public String jcContactCompany(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU401);
        return "pages/jc/jc_contactcompany";
    }

    /**
     * 基础资料-供应商
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_supplier")
    @RequiresPermissions(MenuConstant.MENU402 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-供应商", httpMethod = "GET", notes = "页面接口")
    public String jcSupplier(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU402);
        return "pages/jc/jc_supplier";
    }

    /**
     * 基础资料-品名
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_product_info")
    @RequiresPermissions(MenuConstant.MENU403 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-品名", httpMethod = "GET", notes = "页面接口")
    public String jcProductInfo(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU403);
        return "pages/jc/jc_product_info";
    }

    /**
     * 基础资料-色布品名
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_dye_product_info")
    @RequiresPermissions(MenuConstant.MENU415 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-色布品名", httpMethod = "GET", notes = "页面接口")
    public String jcDyeProductInfo(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU415);
        return "pages/jc/jc_dye_product_info";
    }

    /**
     * 基础资料-色布品名
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_delivery_company")
    @RequiresPermissions(MenuConstant.MENU417 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-代发客户", httpMethod = "GET", notes = "页面接口")
    public String jcDeliveryCompany(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU417);
        return "pages/jc/jc_delivery_company";
    }

    /**
     * 基础资料-加工户
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_process_company")
    @RequiresPermissions(MenuConstant.MENU418 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-加工户", httpMethod = "GET", notes = "页面接口")
    public String jcProcessCompany(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU418);
        return "pages/jc/jc_process_company";
    }

    /**
     * 基础资料-米长
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_meter_length")
    @RequiresPermissions(MenuConstant.MENU420 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-米长", httpMethod = "GET", notes = "页面接口")
    public String jcMeterLength(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU420);
        return "pages/jc/jc_meter_length";
    }

    /**
     * 基础资料-仓库
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_storage")
    @RequiresPermissions(MenuConstant.MENU419 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-仓库", httpMethod = "GET", notes = "页面接口")
    public String storage(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU419);
        return "pages/jc/jc_storage";
    }

    /**
     * 基础资料-色布规格
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_dye_specification")
    @RequiresPermissions(MenuConstant.MENU416 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-色布规格", httpMethod = "GET", notes = "页面接口")
    public String jcDyeSpecification(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU416);
        return "pages/jc/jc_dye_specification";
    }

    /**
     * 基础资料-原料规格
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_specification")
    @RequiresPermissions(MenuConstant.MENU404 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-原料规格", httpMethod = "GET", notes = "页面接口")
    public String jcSpecification(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU404);
        return "pages/jc/jc_specification";
    }

    /**
     * 基础资料-白坯克重
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_weight_of_fabric")
    @RequiresPermissions(MenuConstant.MENU405 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-白坯克重", httpMethod = "GET", notes = "页面接口")
    public String jcWeightOfFabric(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU405);
        return "pages/jc/jc_weight_of_fabric";
    }

    /**
     * 基础资料-白坯门幅
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_width_of_fabric")
    @RequiresPermissions(MenuConstant.MENU406 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-白坯门幅", httpMethod = "GET", notes = "页面接口")
    public String jcWidthOfFabric(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU406);
        return "pages/jc/jc_width_of_fabric";
    }

    /**
     * 基础资料-头份
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_first_copies")
    @RequiresPermissions(MenuConstant.MENU412 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-头份", httpMethod = "GET", notes = "页面接口")
    public String jcFirstCopies(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU412);
        return "pages/jc/jc_first_copies";
    }

    /**
     * 基础资料-加工类型
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_process_type")
    @RequiresPermissions(MenuConstant.MENU413 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-加工类型", httpMethod = "GET", notes = "页面接口")
    public String jcProcessType(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU413);
        return "pages/jc/jc_process_type";
    }

    /**
     * 基础资料-色布计划模板
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_dye_fabric_plan_template")
    @RequiresPermissions(MenuConstant.MENU414 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-色布计划模板", httpMethod = "GET", notes = "页面接口")
    public String jcDyeFabricPlanTemplate(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU414);
        return "pages/jc/jc_dye_fabric_plan_template";
    }

    /**
     * 基础资料-机台号
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_machine_type")
    @RequiresPermissions(MenuConstant.MENU407 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-机台号", httpMethod = "GET", notes = "页面接口")
    public String jcMachineType(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU407);
        return "pages/jc/jc_machine_type";
    }

    /**
     * 基础资料-部门员工
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_employee")
    @RequiresPermissions(MenuConstant.MENU408 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-部门员工", httpMethod = "GET", notes = "页面接口")
    public String jcEmployee(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU408);
        return "pages/jc/jc_employee";
    }

    /**
     * 基础资料-计划模板
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_plan_template")
    @RequiresPermissions(MenuConstant.MENU409 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-计划模板", httpMethod = "GET", notes = "页面接口")
    public String jcPlanTemplate(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU409);
        return "pages/jc/jc_plan_template";
    }

    /**
     * 基础资料-单据类型
     *
     * @param model
     * @return
     */
    @RequestMapping("/jc_bill_type")
    @RequiresPermissions(MenuConstant.MENU411 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-单据类型", httpMethod = "GET", notes = "页面接口")
    public String jcBillTypePage(Model model) {
        model.addAttribute(MENUID, MenuConstant.MENU411);
        return "pages/jc/jc_bill_type";
    }

    /**
     * 基础资料-收货单位
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/jc_take_delivery")
    @RequiresPermissions(MenuConstant.MENU410 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "基础资料-收货单位", httpMethod = "GET", notes = "页面接口")
    public String jcTakeDelivery(Model model) throws Exception {
        model.addAttribute(MENUID, MenuConstant.MENU410);
        return "pages/jc/jc_take_delivery";
    }

    /**
     * 基础资料-权限表
     *
     * @return
     */
    @RequestMapping("/jc_permission")
    @RequiresPermissions(MenuConstant.MENU501 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "设置-权限表", httpMethod = "GET", notes = "页面接口")
    public String jcPermissionPage(Model model) {
        model.addAttribute(MENUID, MenuConstant.MENU501);
        return "pages/sz/jc_permission";
    }

    /**
     * 基础资料-用户
     *
     * @return
     */
    @RequestMapping("/jc_user")
    @RequiresPermissions(MenuConstant.MENU502 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "设置-用户", httpMethod = "GET", notes = "页面接口")
    public String jcUserPage(Model model) {
        model.addAttribute(MENUID, MenuConstant.MENU502);
        return "pages/sz/jc_user";
    }

    /**
     * 设置-角色组
     *
     * @return
     */
    @RequestMapping("/sz_role_group")
    @RequiresPermissions(MenuConstant.MENU503 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "设置-角色组", httpMethod = "GET", notes = "页面接口")
    public String szRoleGroup(Model model) {
        model.addAttribute(MENUID, MenuConstant.MENU503);
        return "pages/sz/sz_role_group";
    }

    /**
     * 参数配置
     *
     * @param model
     * @return
     */
    @RequestMapping("/sz_config")
    @RequiresPermissions(MenuConstant.MENU504 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "设置-配置信息", httpMethod = "GET", notes = "页面接口")
    public String szConfig(Model model) {
        model.addAttribute(MENUID, MenuConstant.MENU504);
        return "pages/sz/sz_config";
    }

    /**
     * 重置账套
     *
     * @param model
     * @return
     */
    @RequestMapping("/sz_reset_account")
    @RequiresPermissions(MenuConstant.MENU505 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    @ApiOperation(value = "设置-重置账套", httpMethod = "GET", notes = "页面接口")
    public String szResetAccount(Model model) {
        model.addAttribute(MENUID, MenuConstant.MENU505);
        return "pages/sz/sz_reset_account";
    }
}