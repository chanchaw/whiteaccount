package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.entity.ResponseResult;
import com.xdf.whiteaccount.entity.SysConfigDecimal;
import com.xdf.whiteaccount.enums.*;
import com.xdf.whiteaccount.service.SysConfigDecimalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Api(tags = "配置表")
@CrossOrigin
@RestController
@RequestMapping("/sysConfigDecimal")
public class SysConfigDecimalController {
    @Autowired
    private SysConfigDecimalService sysConfigDecimalService;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : szx
     * @Date : 2021年10月19日15:45:00
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequiresPermissions(MenuConstant.MENU504 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(SysConfigDecimal record) throws Exception {
        sysConfigDecimalService.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : szx
     * @Date : 2021年10月19日15:38:15
     * @return
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequiresPermissions(MenuConstant.MENU504 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @RequestMapping(value = "/deleteByPrimaryKey", method = RequestMethod.POST)
    public ResponseResult<Void> deleteByPrimaryKey(@RequestBody Map<String,String> params) throws Exception {
        String config_field = params.get("config_field");
        sysConfigDecimalService.deleteByPrimaryKey(config_field);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : SysConfigDecimal
     * @Author : szx
     * @Date : 2021年10月19日15:41:23
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequiresPermissions(MenuConstant.MENU504 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(SysConfigDecimal record) throws Exception {
        sysConfigDecimalService.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询{"config_field":"空加隐藏"}
     * @Return : SysConfigDecimal
     * @Author : szx
     * @Date : 2021年10月19日15:38:29
     * @return
     */
    @RequestMapping(value = "/selectAllowModifyPriceConfig1", method = RequestMethod.POST)
    public SysConfigDecimal selectByPrimaryKey1(@RequestBody Map<String,String> params) throws Exception {
        String config_field = params.get("config_field");
        if (config_field == null) return null;
        return sysConfigDecimalService.selectByPrimaryKey(config_field);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : szx
     * @Date : 2021年10月19日15:40:00
     * @return
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<SysConfigDecimal> listquery(SysConfigDecimal record) throws Exception {
        List<SysConfigDecimal> ret = sysConfigDecimalService.listQuery(record);
        return ret;
    }

    /**
     * @Description : 根据主键修改{"config_field":"纸管（kg）"}
     * @Return : SysConfigDecimal
     * @Author : szx
     * @Date : 2021年10月19日15:38:29
     * @return
     */
    @ApiOperation(value = "根据主键修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequestMapping(value = "/ZGQuery", method = RequestMethod.POST)
    public ResponseResult<Void> query(@RequestBody SysConfigDecimal record) throws Exception {
        sysConfigDecimalService.Query(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }
}
