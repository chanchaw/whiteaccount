package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.MenuConstant;
import com.xdf.whiteaccount.enums.OperateConstant;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.enums.SeparatorConstant;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;
import java.util.List;
import com.xdf.whiteaccount.entity.SysConfigVarchar;
import com.xdf.whiteaccount.service.SysConfigVarcharService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-05-26 14:22:09
 */
@Api(tags = "字符配置")
@CrossOrigin
@RestController
@RequestMapping("/sysConfigVarchar")
public class SysConfigVarcharController {

    @Autowired
    private SysConfigVarcharService service;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-26 14:22:09
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequiresPermissions(MenuConstant.MENU504 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(@Validated SysConfigVarchar record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : SysConfigVarchar
     * @Author : 张柯
     * @Date : 2021-05-26 14:22:09
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequiresPermissions(MenuConstant.MENU504 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(@Validated SysConfigVarchar record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : SysConfigVarchar
     * @Author : 张柯
     * @Date : 2021-05-26 14:22:09
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public SysConfigVarchar selectByPrimaryKey(@PathVariable("id") String id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-05-26 14:22:09
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<SysConfigVarchar> query(SysConfigVarchar record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-26 14:22:09
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequiresPermissions(MenuConstant.MENU504 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @RequestMapping(value = "/deleteByPk/{id}", method = RequestMethod.POST)
    public ResponseResult<Void> deleteByPrimaryKey(@PathVariable("id") String id) throws Exception {
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }
}