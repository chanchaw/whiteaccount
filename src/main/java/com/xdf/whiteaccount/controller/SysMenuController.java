package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.dto.TreeDTO;
import com.xdf.whiteaccount.entity.SysPermission;
import com.xdf.whiteaccount.entity.SysRole;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.service.SysPermissionService;
import com.xdf.whiteaccount.service.SysRoleService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;

import java.util.List;
import java.util.stream.Collectors;

import com.xdf.whiteaccount.entity.SysMenu;
import com.xdf.whiteaccount.service.SysMenuService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
@Api(tags = "菜单表")
@CrossOrigin
@RestController
@RequestMapping("/sysMenu")
public class SysMenuController {
    @Autowired
    private SysMenuService service;
    @Autowired
    private SysPermissionService permissionService;
    @Autowired
    private SysRoleService roleService;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-01-03 14:31:54
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    @Transactional(rollbackFor = Exception.class)
    public ResponseResult<Void> insertSelective(SysMenu record) throws Exception {
        Integer id = record.getId();
        Assert.state(service.insert(record) > 0, ResponseEnum.SAVE_DATA_ERROR.getName());
        List<SysRole> roleList = roleService.listQuery(SysRole.builder().build());
        if (CollectionUtils.isNotEmpty(roleList)) {
            List<SysPermission> list = roleList.stream().map(x ->
                    SysPermission.builder()
                            .sysRoleId(x.getId())
                            .sysMenuId(id)
                            .build()
            ).collect(Collectors.toList());
            permissionService.multiInsert(list);
        }
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : SysMenu
     * @Author : 张柯
     * @Date : 2021-01-03 14:31:54
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(SysMenu record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 批量修改
     * @Return : SysMenu
     * @Author : 张柯
     * @Date : 2021-01-03 14:31:54
     */
    @ApiOperation(value = "批量修改", httpMethod = "POST", notes = "自动生成的修改方法")
    @RequestMapping(value = "/multiUpdate", method = RequestMethod.POST)
    public ResponseResult<Void> multiUpdate(@RequestBody List<SysMenu> list) throws Exception {
        service.multiUpdate(list);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : SysMenu
     * @Author : 张柯
     * @Date : 2021-01-03 14:31:54
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public SysMenu selectByPrimaryKey(@PathVariable("id") Integer id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-01-03 14:31:54
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<SysMenu> query(SysMenu record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-01-03 14:31:54
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping(value = "/deleteByPk", method = RequestMethod.POST)
    public ResponseResult<Void> deleteByPrimaryKey(@RequestParam("id") Integer id) throws Exception {
        permissionService.deleteBySysMenuId(id);
        Assert.state(service.deleteByPrimaryKey(id) > 0, ResponseEnum.ERROR.getName());
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 显示树
     *
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/tree")
    public List<TreeDTO> listQueryTree() throws Exception {
        return service.listQueryByParentId(null);
    }
}