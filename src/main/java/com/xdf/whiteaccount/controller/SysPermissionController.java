package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.MenuConstant;
import com.xdf.whiteaccount.enums.OperateConstant;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.enums.SeparatorConstant;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;

import java.util.List;

import com.xdf.whiteaccount.entity.SysPermission;
import com.xdf.whiteaccount.service.SysPermissionService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
@Api(tags = "权限表")
@CrossOrigin
@RestController
@RequestMapping("/sysPermission")
public class SysPermissionController {

    @Autowired
    private SysPermissionService service;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    @RequiresPermissions(MenuConstant.MENU501 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    public ResponseResult<Void> insertSelective(SysPermission record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 批量新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "批量新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequestMapping(value = "/multiInsert", method = RequestMethod.POST)
    @RequiresPermissions(MenuConstant.MENU501 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    public ResponseResult<Void> multiInsert(@RequestBody List<SysPermission> list) throws Exception {
        service.multiInsert(list);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : SysPermission
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    @RequiresPermissions(MenuConstant.MENU501 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    public ResponseResult<Void> updateSelective(SysPermission record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 批量修改
     * @Return : SysPermission
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "批量修改", httpMethod = "POST", notes = "自动生成的修改方法")
    @RequestMapping(value = "/multiUpdate", method = RequestMethod.POST)
    @RequiresPermissions(MenuConstant.MENU501 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    public ResponseResult<Void> multiUpdate(@RequestBody List<SysPermission> list) throws Exception {
        service.multiUpdate(list);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : SysPermission
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public SysPermission selectByPrimaryKey(@PathVariable("id") Integer id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<SysPermission> query(SysPermission record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequestMapping(value = "/deleteByPk", method = RequestMethod.POST)
    @RequiresPermissions(MenuConstant.MENU501 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    public ResponseResult<Void> deleteByPrimaryKey(@RequestParam("id") Integer id) throws Exception {
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }
}