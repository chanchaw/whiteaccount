package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.config.ShiroRealm;
import com.xdf.whiteaccount.controller.base.BaseController;
import com.xdf.whiteaccount.dto.JsonResult;
import com.xdf.whiteaccount.dto.QueryParams;
import com.xdf.whiteaccount.dto.SysUserDTO;
import com.xdf.whiteaccount.entity.ResponseResult;
import com.xdf.whiteaccount.entity.SysUser;
import com.xdf.whiteaccount.enums.MenuConstant;
import com.xdf.whiteaccount.enums.OperateConstant;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.enums.SeparatorConstant;
import com.xdf.whiteaccount.exception.NotallowException;
import com.xdf.whiteaccount.service.SysUserService;
import com.xdf.whiteaccount.utils.CustumDozerMapperUtil;
import com.xdf.whiteaccount.utils.Example;
import com.xdf.whiteaccount.vo.SysUserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2020-12-31 14:42:38
 */
@Api(tags = "登录用户表")
@CrossOrigin
@RestController
@RequestMapping("/sysUser")
@RequiresUser
public class SysUserController extends BaseController {

    @Autowired
    private SysUserService service;
    @Autowired
    private CustumDozerMapperUtil util;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2020-12-31 14:42:38
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequiresPermissions(MenuConstant.MENU502 + SeparatorConstant.SEPARATOR + OperateConstant.ADD)
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(SysUserVO record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : SysUser
     * @Author : 张柯
     * @Date : 2020-12-31 14:42:38
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequiresPermissions(MenuConstant.MENU502 + SeparatorConstant.SEPARATOR + OperateConstant.MODIFY)
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(SysUserVO record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : SysUser
     * @Author : 张柯
     * @Date : 2020-12-31 14:42:38
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public SysUser selectByPrimaryKey(@PathVariable("id") Integer id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2020-12-31 14:42:38
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    @RequiresPermissions(MenuConstant.MENU502 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    public List<SysUser> query(SysUser record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2020-12-31 14:42:38
     */
    @RequestMapping(value = "/view/listQuery", method = RequestMethod.GET)
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequiresPermissions(MenuConstant.MENU502 + SeparatorConstant.SEPARATOR + OperateConstant.SHOW)
    public List<SysUserVO> query(@RequestParam(value = "state", defaultValue = "1") Integer state) throws Exception {
        return service.listQuery(state);
    }

    /**
     * 获取用户列表
     *
     * @return
     * @throws Exception
     */
    @GetMapping("/query")
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "用户界面显示")
    public List<SysUserDTO> listQuery() throws Exception {
        List<SysUser> list = service.listQuery(new SysUser());
        return util.transSourceToTarget(list, SysUserDTO.class);
    }

    /**
     * 查询当前登录用户
     *
     * @return
     */
    @ApiOperation(value = "查询当前登录用户", httpMethod = "POST", notes = "查询当前登录用户")
    @RequiresUser
    @RequestMapping(value = "/query/login", method = RequestMethod.POST)
    public SysUserDTO getCurrentLoginUser() throws Exception {
        return service.selectCurrentLoginUser();
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2020-12-31 14:42:38
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/jsonQuery", method = RequestMethod.GET)
    public JsonResult<SysUser> query(QueryParams params) throws Exception {
        if (params == null) return null;
        return JsonResult.<SysUser>builder()
                .rows(service.listQueryByExample(new Example().setRowSize(params.getRows()).setStartRow(params.getStartRow()).setRowSize(params.getRows())))
                .total(service.count(new Example()))
                .build();
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2020-12-31 14:42:38
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequiresPermissions(MenuConstant.MENU502 + SeparatorConstant.SEPARATOR + OperateConstant.DELETE)
    @RequestMapping(value = "/deleteByPk", method = RequestMethod.POST)
    public ResponseResult<Void> deleteByPrimaryKey(@RequestParam("id") Integer id) throws Exception {
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 修改密码
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2020-12-31 14:42:38
     */
    @ApiOperation(value = "修改密码", httpMethod = "POST", notes = "修改密码")
    @RequiresUser
    @RequestMapping(value = "/password/edit", method = RequestMethod.POST)
    public ResponseResult<Void> deleteByPrimaryKey(String lastPwd, String currPwd) throws Exception {
        Integer sysUserId = getLoginUserId();
        SysUser sysUser = service.selectByPrimaryKey(sysUserId);
        if (sysUser == null) throw new NotallowException("用户不存在!");
        if (sysUser.getUserState() == 0) throw new NotallowException("用户已停用!");
        if (!Objects.equals(sysUser.getLoginPassword(), lastPwd)) throw new NotallowException("原密码输入错误!");
        Assert.state(StringUtils.isNotEmpty(currPwd), "新密码不能为空!");
        service.updateByPrimaryKeySelective(SysUser.builder().id(sysUserId).loginPassword(currPwd).build());
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 获取当前全部权限
     *
     * @return
     * @throws Exception
     */
    @GetMapping("/permissions")
    @RequiresUser
    public Set<String> getAllPermissions() throws Exception {
        return ShiroRealm.getPermissions(service.selectPermissionByUserId(getLoginUserId()));
    }
}