package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.enums.ResponseEnum;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;

import java.util.List;

import com.xdf.whiteaccount.entity.SysUserRoleGroup;
import com.xdf.whiteaccount.service.SysUserRoleGroupService;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
@Api(tags = "用户与角色组对应关系表")
@CrossOrigin
@RestController
@RequestMapping("/sysUserRoleGroup")
public class SysUserRoleGroupController {

    @Autowired
    private SysUserRoleGroupService service;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(SysUserRoleGroup record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 批量新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "批量新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequestMapping(value = "/multiInsert", method = RequestMethod.POST)
    public ResponseResult<Void> multiInsert(@RequestBody List<SysUserRoleGroup> list) throws Exception {
        service.multiInsert(list);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 选择修改
     * @Return : SysUserRoleGroup
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "选择修改", httpMethod = "PUT", notes = "自动生成的修改方法")
    @RequestMapping(value = "/updateSelective", method = RequestMethod.POST)
    public ResponseResult<Void> updateSelective(SysUserRoleGroup record) throws Exception {
        service.updateByPrimaryKeySelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 批量修改
     * @Return : SysUserRoleGroup
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "批量修改", httpMethod = "POST", notes = "自动生成的修改方法")
    @RequestMapping(value = "/multiUpdate", method = RequestMethod.POST)
    public ResponseResult<Void> multiUpdate(@RequestBody List<SysUserRoleGroup> list) throws Exception {
        service.multiUpdate(list);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * @Description : 根据主键查询
     * @Return : SysUserRoleGroup
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/queryByPk/{id}", method = RequestMethod.GET)
    public SysUserRoleGroup selectByPrimaryKey(@PathVariable("id") Integer id) throws Exception {
        if (id == null) return null;
        return service.selectByPrimaryKey(id);
    }

    /**
     * @Description : 查询方法
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "查询方法", httpMethod = "GET", notes = "自动生成的查询方法")
    @RequestMapping(value = "/listQuery", method = RequestMethod.GET)
    public List<SysUserRoleGroup> query(SysUserRoleGroup record) throws Exception {
        return service.listQuery(record);
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-05-16 09:40:12
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequestMapping(value = "/deleteByPk", method = RequestMethod.POST)
    public ResponseResult<Void> deleteByPrimaryKey(@RequestParam(value = "id") Integer id) throws Exception {
        service.deleteByPrimaryKey(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }
}