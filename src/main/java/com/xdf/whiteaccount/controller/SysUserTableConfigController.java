package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.controller.base.BaseController;
import com.xdf.whiteaccount.dto.ColumnConfigDTO;
import com.xdf.whiteaccount.entity.ResponseResult;
import com.xdf.whiteaccount.entity.SysUserTableConfig;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.service.SysUserTableConfigService;
import com.xdf.whiteaccount.utils.CustumDozerMapperUtil;
import com.xdf.whiteaccount.utils.Example;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @Description : 配置记忆列宽后端实现
 * @Author : 张柯
 * @Date : 2020-12-31 15:23:24
 */
@Api(tags = "用户报表列参数配置")
@CrossOrigin
@RestController
@RequestMapping("/sysUserTableConfig")
@RequiresUser
@Slf4j
public class SysUserTableConfigController extends BaseController {

    @Autowired
    private SysUserTableConfigService service;
    @Autowired
    private CustumDozerMapperUtil util;

    /**
     * 列拖拽
     *
     * @param record
     * @return
     */
    @PutMapping("/modify/column")
    public ResponseResult<Void> columnResize(ColumnConfigDTO record) throws Exception {
        SysUserTableConfig config = util.transObject(record, SysUserTableConfig.class);
        config.setSysUserId(getLoginUserId());
        service.updateBySysUserIdMenuId(config);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 列位置更改
     *
     * @param record
     * @return
     * @throws Exception
     */
    @PostMapping("/modify/reorder")
    public ResponseResult<Void> columnReorder(@RequestBody List<ColumnConfigDTO> record) throws Exception {
        List<SysUserTableConfig> list = util.transSourceToTarget(record, SysUserTableConfig.class);
        Integer loginUserId = getLoginUserId();
        list.stream().map(x -> {
            x.setSysUserId(loginUserId);
            return x;
        });
        for (SysUserTableConfig config : list) {
            if (StringUtils.isNotEmpty(config.getFieldName()))
                service.updateBySysUserIdMenuId(config);
        }
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 列重置
     *
     * @param sysMenuId
     * @return
     */
    @PutMapping("/reset/all")
    public Map<String, ColumnConfigDTO> resetConfig(Integer sysMenuId) throws Exception {
        Integer userLoginId = getLoginUserId();
        service.updateByExample(new Example().andEq("sys_menu_id", sysMenuId).andEq("sys_user_id", userLoginId),
                SysUserTableConfig.builder()
                        .fieldAlign("center")
                        .fieldWidth(100D)
                        .fieldHidden(false)
                        .build());
        return convent(service.listQuery(SysUserTableConfig.builder().sysMenuId(sysMenuId).sysUserId(userLoginId).build()));
    }

    /**
     * @Description : 获取记忆列宽
     * @Return : java.utils.List
     * @Author : 张柯
     * @Date : 2020-12-31 15:23:24
     */
    @ApiOperation(value = "查询方法", httpMethod = "POST", notes = "自动生成的查询方法")
    @RequestMapping(value = "/calculate", method = RequestMethod.POST)
    public Map<String, ColumnConfigDTO> calculate(@RequestBody List<ColumnConfigDTO> record) throws Exception {
        List<SysUserTableConfig> list = util.transSourceToTarget(record, SysUserTableConfig.class);
        log.debug(list != null ? list.toString() : "NULL");
        Integer userLoginId = getLoginUserId();
        if (CollectionUtils.isEmpty(list)) return null;
        list.stream().map(x -> {
            x.setSysUserId(userLoginId);
            return x;
        }).filter(x -> x.getFieldName() != null).collect(Collectors.toList());

        Integer menuId = list.get(0).getSysMenuId();
        List<SysUserTableConfig> exists = Optional.ofNullable(
                service.listQuery(SysUserTableConfig.builder()
                        .sysUserId(userLoginId)
                        .sysMenuId(menuId).build())).orElse(new ArrayList<>());
        Set<String> s = exists.stream().map(x -> x.getFieldName()).collect(Collectors.toSet());
        List<SysUserTableConfig> notExist = list.stream().filter(x -> !s.contains(x.getFieldName())).collect(Collectors.toList());
        AtomicInteger i = new AtomicInteger(notExist.size());
        if (CollectionUtils.isNotEmpty(notExist)) {
            service.multiInsert(notExist.stream().map(x -> {
                if ("".equals(x.getFieldName()))
                    x.setFieldSerialNum(0);
                x.setSysUserId(userLoginId);
                x.setFieldAlign("center");
                if (x.getFieldHidden() == null)
                    x.setFieldHidden(false);
                if (x.getFieldWidth() == null)
                    x.setFieldWidth(100D);
                x.setFieldSerialNum(i.getAndIncrement());
                return x;
            }).collect(Collectors.toList()));
            exists.addAll(notExist);
        }
        return convent(exists);
    }

    /**
     * 转换数据类型
     *
     * @param list
     * @return
     */
    private Map<String, ColumnConfigDTO> convent(List<SysUserTableConfig> list) {
        if (CollectionUtils.isEmpty(list)) return null;
        return list.stream().collect(Collectors.toMap(x -> x.getFieldName(),
                (x -> util.transObject(x, ColumnConfigDTO.class)),
                ((x, y) -> x)));
    }
}