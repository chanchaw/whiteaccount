package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.controller.base.BaseController;
import com.xdf.whiteaccount.enums.ResponseEnum;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.xdf.whiteaccount.entity.ResponseResult;
import java.util.List;
import com.xdf.whiteaccount.entity.SysUserVisitList;
import com.xdf.whiteaccount.service.SysUserVisitListService;
import java.util.Map;

/**
 * @Description : 自动生成的控制层
 * @Author : 张柯
 * @Date : 2021-07-31 16:39:12
 */
@Api(tags = "用户常用访问表")
@CrossOrigin
@RestController
@RequestMapping("/sysUserVisitList")
public class SysUserVisitListController extends BaseController {
    @Autowired
    private SysUserVisitListService service;

    /**
     * @Description : 选择新增
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-07-31 16:39:12
     */
    @ApiOperation(value = "选择新增", httpMethod = "POST", notes = "自动生成的新增方法")
    @RequestMapping(value = "/insertSelective", method = RequestMethod.POST)
    public ResponseResult<Void> insertSelective(SysUserVisitList record) throws Exception {
        service.insertSelective(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据用户ID查询
     *
     * @return
     */
    @ApiOperation(value = "根据用户ID查询", httpMethod = "GET", notes = "根据用户ID查询")
    @GetMapping("/querybyUserId")
    public List<Map<String, Object>> queryByUserId() {
        return service.queryByUserId(getLoginUserId());
    }

    /**
     * @Description : 根据主键删除方法
     * @Return : ResponseResult<Void>
     * @Author : 张柯
     * @Date : 2021-07-31 16:39:12
     */
    @ApiOperation(value = "根据主键删除方法", httpMethod = "DELETE", notes = "自动生成的删除方法")
    @RequestMapping(value = "/deleteByMenuId", method = RequestMethod.POST)
    public ResponseResult<Void> deleteByMenuId(@RequestParam("id") Integer id) throws Exception {
        service.deleteByMenuId(id);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }
}