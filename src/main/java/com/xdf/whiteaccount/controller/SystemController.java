package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.entity.ResponseResult;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.enums.RoleConstant;
import com.xdf.whiteaccount.service.CustomizeService;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: white-account
 * @description: 系统设置
 * @author: 张柯
 * @create: 2021-06-21 13:37
 **/
@RestController
@RequestMapping("/system")
public class SystemController {
    @Autowired
    private CustomizeService customizeService;

    /**
     * 重置数据库
     *
     * @param type
     * @return
     */
    @PostMapping("/resetting")
    @RequiresRoles(value = RoleConstant.ADMIN_ROLE)
    public ResponseResult<Void> resetSystem(@RequestParam("type") String[] type, @RequestParam("password") String password) {
        Assert.state(customizeService.backupAndReset(type, password), "重置数据库失败！！");
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }
}