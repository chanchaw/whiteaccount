package com.xdf.whiteaccount.controller;

import com.xdf.whiteaccount.entity.ResponseResult;
import com.xdf.whiteaccount.enums.ResponseEnum;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-03 09:30
 **/
@RequestMapping("/test")
@RestController
public class TestController {
    @GetMapping("/gen")
    public ResponseResult<String> generateGUID() {
        UUID uuid = UUID.randomUUID();
        return ResponseResult.<String>builder()
                .success(true)
                .message(ResponseEnum.OK.getName())
                .data(uuid.toString())
                .build();
    }
}
