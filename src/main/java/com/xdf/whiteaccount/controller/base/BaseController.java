package com.xdf.whiteaccount.controller.base;

import com.xdf.whiteaccount.entity.SysConfigInt;
import com.xdf.whiteaccount.entity.SysConfigVarchar;
import com.xdf.whiteaccount.service.SysConfigIntService;
import com.xdf.whiteaccount.service.SysConfigVarcharService;
import com.xdf.whiteaccount.service.base.BaseService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;

/**
 * @program: white-account
 * @description: 基类
 * @author: 张柯
 * @create: 2021-05-17 08:09
 **/
public class BaseController extends BaseService {
    @Autowired
    private SysConfigIntService configIntService;
    @Autowired
    private SysConfigVarcharService configVarcharService;

    /**
     * 获取字符配置
     * @param key
     * @return
     * @throws Exception
     */
    protected String getStringValueByConfigKey(String key) throws Exception {
        return Optional.ofNullable(configVarcharService.selectByPrimaryKey(key)).map(SysConfigVarchar::getConfigValue).orElse("");
    }

    /**
     * 获取整形配置
     * @param key
     * @return
     * @throws Exception
     */
    protected Integer getIntegerValueByConfigKey(String key) throws Exception {
        return Optional.ofNullable(configIntService.selectByPrimaryKey(key)).map(SysConfigInt::getConfigValue).orElse(null);
    }

    /**
     * 判断当前用户是否登录
     *
     * @return
     */
    protected boolean isLogin() {
        return SecurityUtils.getSubject().isAuthenticated();
    }
}
