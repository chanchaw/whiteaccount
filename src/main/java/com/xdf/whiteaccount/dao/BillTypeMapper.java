package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.BillType;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 单据类型
 * @Author : 张柯
 * @Date : 2021-05-25 11:08:48
 */
@Repository
public interface BillTypeMapper {
    int insert(BillType record);

    int insertSelective(BillType record);

    int multiInsert(List<BillType> list);

    int insertDumplicateKeyUpdate(BillType record);

    int updateByPrimaryKey(BillType record);

    int updateByPrimaryKeySelective(BillType record);

    int multiUpdate(List<BillType> list);

    int deleteByPrimaryKey(String id);

    BillType selectByPrimaryKey(String id);

    List<BillType> selectAll();

    List<BillType> selectByParam(BillType record);

    int updateByExampleSelective(@Param("record") BillType record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<BillType> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") BillType record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}