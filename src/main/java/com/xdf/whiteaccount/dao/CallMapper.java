package com.xdf.whiteaccount.dao;

import com.xdf.whiteaccount.entity.WechatBody;
import com.xdf.whiteaccount.vo.DyeFabricPlanDetailVO;
import com.xdf.whiteaccount.vo.PaymentVO;
import com.xdf.whiteaccount.vo.PlanDetailVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-05-18 18:42
 **/
@Repository
public interface CallMapper {
    List<Map<String, Object>> getPlanReport(@Param("start") Date start, @Param("end") Date end, @Param("markFinished") Boolean markFinished, @Param("Strcolumn") String Strcolumn, @Param("Strvalue") String Strvalue);

    List<Map<String, Object>> getDyeFabricPlanReport(@Param("start") Date start, @Param("end") Date end, @Param("markFinished") Boolean markFinished, @Param("Strcolumn") String Strcolumn, @Param("Strvalue") String Strvalue);

    String planCodeGen(String prefix);

    String dyeFabricplanCodeGen(String prefix);

    String dyeFabricInboundCodeGen(String prefix);

    String materialOrderCodeGen(String prefix);

    String productOutwardOrderCodeGen(String prefix);

    PlanDetailVO getPlanDtl(Long id);

    List<Map<String, Object>> getJrkbillDtl(Long id);

    List<Map<String, Object>> getJrkbillfh(Long id);

    List<Map<String, Object>> getMaterialOrderReport(@Param("start") Date start, @Param("end") Date end, @Param("supplierName") String supplierName);

    List<Map<String, Object>> getFHhistoryDetail(@Param("start") Date start, @Param("end") Date end, @Param("clientName") String clientName, @Param("checkType") String checkType);

    List<Map<String, Object>> getReceivablesDetail(@Param("start") Date start, @Param("end") Date end, @Param("clientName") String clientName, @Param("value") Integer value);

    List<Map<String, Object>> getReceivablesAllSum();

    List<Map<String, Object>> getCollectionOld(@Param("start") Date start, @Param("end") Date end, @Param("clientName") String clientName, @Param("value") String value);

    List<PaymentVO> getPayRecords(@Param("start") Date start, @Param("end") Date end) throws Exception;

    List<Map<String, Object>> getPlanStorageModify(Long id) throws Exception;

    List<Map<String, Object>> getGreyfabricInput(@Param("start") Date start, @Param("end") Date end) throws Exception;

    List<Map<String, Object>> getGreyfabricStorage(int mode,Date end) throws Exception;

    List<Map<String, Object>> getDyeFabricDelivery(@Param("start") Date start, @Param("end") Date end, @Param("clientName") String clientName, @Param("checkout") String checkout) throws Exception;

    List<WechatBody> selectByModule(String module);

    DyeFabricPlanDetailVO getDyePlanDtl(Long id);

    List<Map<String, Object>> getDyeFhDtl(Long id);

    List<Map<String, Object>> getDyeInboundDtl(Long id);

    List<Map<String, Object>> getDyePlanStorageModify(Long id);

    List<Map<String, Object>> getDyeInboundReport(@Param("start") Date start, @Param("end") Date end, @Param("clientName") String clientName);

    List<Map<String, Object>> getProductInboundReport(@Param("start") Date start, @Param("end") Date end, @Param("supplierName") String supplierName);

    List<Map<String, Object>> getDyeStorageReport(@Param("clientName") String clientName, @Param("value") Integer value);

    List<Map<String, Object>> getMaterialStorage(String supplierName);

    List<Map<String, Object>> getMaterialOutbound(@Param("start") Date start, @Param("end") Date end, @Param("supplierName") String supplierName);

    List<Map<String, Object>> getAccountPayable(@Param("start") Date start, @Param("end") Date end, @Param("clientName") String clientName, @Param("sign") Integer sign);

    List<Map<String, Object>> getAccountPayment(@Param("start") Date start, @Param("end") Date end, @Param("supplierName") String supplierName, @Param("value") Integer value);

    List<Map<String, Object>> getMaterialOutward(@Param("start") Date start, @Param("end") Date end, @Param("processName") String processName);

    List<Map<String, Object>> getUnfinishedOutward(@Param("start") Date start, @Param("end") Date end, @Param("clientName") String clientName) throws Exception;

    List<Map<String, Object>> getProcesscompanyAccount(@Param("start") Date start, @Param("end") Date end, @Param("processName") String processName, @Param("mode") Integer mode);

    List<Map<String, Object>> getCanceledPlan();

    List<Map<String, Object>> getCanceledDyeFabricPlan();

    List<Map<String, Object>> getProductWaste(@Param("start") Date start, @Param("end") Date end, @Param("clientName") String clientName);

    List<Map<String, Object>> getGreyFabricJrkbillReport(@Param("start") Date start, @Param("end") Date end, @Param("machineNum") String machineNum);

    List<Map<String, Object>> getPlanReportDtl(@Param("state") Integer state, @Param("markFinished") Boolean markFinished);

    List<Map<String, Object>> getUnImportedJRK(String processCompname);

    List<Map<String, Object>> getPlanReportSummary(@Param("start") Date start, @Param("end") Date end);
}