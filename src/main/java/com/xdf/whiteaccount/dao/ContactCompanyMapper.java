package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.ContactCompany;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 往来单位
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface ContactCompanyMapper{
    int insert(ContactCompany record);

    int insertSelective(ContactCompany record);

    int multiInsert(List<ContactCompany> list);

    int insertDumplicateKeyUpdate(ContactCompany record);

    int updateByPrimaryKey(ContactCompany record);

    int updateByPrimaryKeySelective(ContactCompany record);

    int multiUpdate(List<ContactCompany> list);

    int deleteByPrimaryKey(String id);

    ContactCompany selectByPrimaryKey(String id);

    List<ContactCompany> selectAll();

    List<ContactCompany> selectByParam(ContactCompany record);

    int updateByExampleSelective(@Param("record") ContactCompany record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<ContactCompany> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") ContactCompany record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}