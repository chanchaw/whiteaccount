package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.CpAdjust;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 打卷调整表
 * @Author : 张柯
 * @Date : 2021-06-25 16:52:50
 */
@Repository
public interface CpAdjustMapper {
    int insert(CpAdjust record);

    int insertSelective(CpAdjust record);

    int multiInsert(List<CpAdjust> list);

    int insertDumplicateKeyUpdate(CpAdjust record);

    int updateByPrimaryKey(CpAdjust record);

    int updateByPrimaryKeySelective(CpAdjust record);

    int multiUpdate(List<CpAdjust> list);

    int deleteByPrimaryKey(Integer id);

    CpAdjust selectByPrimaryKey(Integer id);

    List<CpAdjust> selectAll();

    List<CpAdjust> selectByParam(CpAdjust record);

    int updateByExampleSelective(@Param("record") CpAdjust record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<CpAdjust> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") CpAdjust record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}