package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.CpBillfh;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 
* @Author : 张柯
* @Date : 2021-06-25 09:44:52
*/
@Repository
public interface CpBillfhMapper{
    int insert(CpBillfh record);

    int insertSelective(CpBillfh record);

    int multiInsert(List<CpBillfh> list);

    int insertDumplicateKeyUpdate(CpBillfh record);

    int updateByPrimaryKey(CpBillfh record);

    int updateByPrimaryKeySelective(CpBillfh record);

    int multiUpdate(List<CpBillfh> list);

    int deleteByPrimaryKey(Integer id);

    CpBillfh selectByPrimaryKey(Integer id);

    List<CpBillfh> selectAll();

    List<CpBillfh> selectByParam(CpBillfh record);

    int updateByExampleSelective(@Param("record") CpBillfh record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<CpBillfh> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") CpBillfh record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}