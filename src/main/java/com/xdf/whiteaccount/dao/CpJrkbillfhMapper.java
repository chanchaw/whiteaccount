package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.CpJrkbillfh;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description :
 * @Author : 张柯
 * @Date : 2021-06-25 09:44:52
 */
@Repository
public interface CpJrkbillfhMapper {
    int insert(CpJrkbillfh record);

    int insertSelective(CpJrkbillfh record);

    int multiInsert(List<CpJrkbillfh> list);

    int insertDumplicateKeyUpdate(CpJrkbillfh record);

    int updateByPrimaryKey(CpJrkbillfh record);

    int updateByPrimaryKeySelective(CpJrkbillfh record);

    int multiUpdate(List<CpJrkbillfh> list);

    int deleteByPrimaryKey(Integer id);

    CpJrkbillfh selectByPrimaryKey(Integer id);

    List<CpJrkbillfh> selectAll();

    List<CpJrkbillfh> selectByParam(CpJrkbillfh record);

    int updateByExampleSelective(@Param("record") CpJrkbillfh record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<CpJrkbillfh> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") CpJrkbillfh record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}