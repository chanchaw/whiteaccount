package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.CpReceivables;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 发货调整收款表
* @Author : 张柯
* @Date : 2021-06-25 16:52:50
*/
@Repository
public interface CpReceivablesMapper{
    int insert(CpReceivables record);

    int insertSelective(CpReceivables record);

    int multiInsert(List<CpReceivables> list);

    int insertDumplicateKeyUpdate(CpReceivables record);

    int updateByPrimaryKey(CpReceivables record);

    int updateByPrimaryKeySelective(CpReceivables record);

    int multiUpdate(List<CpReceivables> list);

    int deleteByPrimaryKey(Integer id);

    CpReceivables selectByPrimaryKey(Integer id);

    List<CpReceivables> selectAll();

    List<CpReceivables> selectByParam(CpReceivables record);

    int updateByExampleSelective(@Param("record") CpReceivables record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<CpReceivables> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") CpReceivables record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}