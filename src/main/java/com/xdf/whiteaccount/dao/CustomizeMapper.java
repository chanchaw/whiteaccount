package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

/**
 * @program: white-account
 * @description: 自定义查询
 * @author: 张柯
 * @create: 2021-06-12 10:08
 **/
@Repository
public interface CustomizeMapper {
    String getDataBase();

    int clearAllData();

    int clearBasicData();

    int clearDocument();
}
