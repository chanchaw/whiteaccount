package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.DeliveryCompany;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 代发客户
* @Author : 张柯
* @Date : 2021-06-29 12:30:54
*/
@Repository
public interface DeliveryCompanyMapper{
    int insert(DeliveryCompany record);

    int insertSelective(DeliveryCompany record);

    int multiInsert(List<DeliveryCompany> list);

    int insertDumplicateKeyUpdate(DeliveryCompany record);

    int updateByPrimaryKey(DeliveryCompany record);

    int updateByPrimaryKeySelective(DeliveryCompany record);

    int multiUpdate(List<DeliveryCompany> list);

    int deleteByPrimaryKey(Integer id);

    DeliveryCompany selectByPrimaryKey(Integer id);

    List<DeliveryCompany> selectAll();

    List<DeliveryCompany> selectByParam(DeliveryCompany record);

    int updateByExampleSelective(@Param("record") DeliveryCompany record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<DeliveryCompany> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") DeliveryCompany record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}