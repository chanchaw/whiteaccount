package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.Department;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 部门
 * @Author : 张柯
 * @Date : 2021-05-19 20:26:32
 */
@Repository
public interface DepartmentMapper {
    int insert(Department record);

    int insertSelective(Department record);

    int multiInsert(List<Department> list);

    int insertDumplicateKeyUpdate(Department record);

    int updateByPrimaryKey(Department record);

    int updateByPrimaryKeySelective(Department record);

    int multiUpdate(List<Department> list);

    int deleteByPrimaryKey(Integer id);

    Department selectByPrimaryKey(Integer id);

    List<Department> selectAll();

    List<Department> selectByParam(Department record);

    int updateByExampleSelective(@Param("record") Department record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<Department> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") Department record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}