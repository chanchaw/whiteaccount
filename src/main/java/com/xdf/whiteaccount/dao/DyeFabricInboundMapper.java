package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.DyeFabricInbound;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 色布入库单据
 * @Author : 张柯
 * @Date : 2021-07-20 14:42:34
 */
@Repository
public interface DyeFabricInboundMapper {
    int insert(DyeFabricInbound record);

    int insertSelective(DyeFabricInbound record);

    int multiInsert(List<DyeFabricInbound> list);

    int insertDumplicateKeyUpdate(DyeFabricInbound record);

    int updateByPrimaryKey(DyeFabricInbound record);

    int updateByPrimaryKeySelective(DyeFabricInbound record);

    int multiUpdate(List<DyeFabricInbound> list);

    int deleteByPrimaryKey(Long id);

    DyeFabricInbound selectByPrimaryKey(Long id);

    List<DyeFabricInbound> selectAll();

    List<DyeFabricInbound> selectByParam(DyeFabricInbound record);

    int updateByExampleSelective(@Param("record") DyeFabricInbound record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<DyeFabricInbound> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") DyeFabricInbound record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}