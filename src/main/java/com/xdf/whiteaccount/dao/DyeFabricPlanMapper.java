package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.DyeFabricPlan;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 色布计划
 * @Author : 张柯
 * @Date : 2021-06-23 16:01:16
 */
@Repository
public interface DyeFabricPlanMapper {
    int insert(DyeFabricPlan record);

    int insertSelective(DyeFabricPlan record);

    int multiInsert(List<DyeFabricPlan> list);

    int insertDumplicateKeyUpdate(DyeFabricPlan record);

    int updateByPrimaryKey(DyeFabricPlan record);

    int updateByPrimaryKeySelective(DyeFabricPlan record);

    int multiUpdate(List<DyeFabricPlan> list);

    int deleteByPrimaryKey(Long id);

    DyeFabricPlan selectByPrimaryKey(Long id);

    List<DyeFabricPlan> selectAll();

    List<DyeFabricPlan> selectByParam(DyeFabricPlan record);

    int updateByExampleSelective(@Param("record") DyeFabricPlan record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<DyeFabricPlan> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") DyeFabricPlan record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}