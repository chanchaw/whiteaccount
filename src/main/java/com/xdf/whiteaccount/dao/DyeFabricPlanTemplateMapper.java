package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.DyeFabricPlanTemplate;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 色布计划模板
 * @Author : 张柯
 * @Date : 2021-06-24 10:06:23
 */
@Repository
public interface DyeFabricPlanTemplateMapper {
    int insert(DyeFabricPlanTemplate record);

    int insertSelective(DyeFabricPlanTemplate record);

    int multiInsert(List<DyeFabricPlanTemplate> list);

    int insertDumplicateKeyUpdate(DyeFabricPlanTemplate record);

    int updateByPrimaryKey(DyeFabricPlanTemplate record);

    int updateByPrimaryKeySelective(DyeFabricPlanTemplate record);

    int multiUpdate(List<DyeFabricPlanTemplate> list);

    int deleteByPrimaryKey(Long id);

    DyeFabricPlanTemplate selectByPrimaryKey(Long id);

    List<DyeFabricPlanTemplate> selectAll();

    List<DyeFabricPlanTemplate> selectByParam(DyeFabricPlanTemplate record);

    int updateByExampleSelective(@Param("record") DyeFabricPlanTemplate record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<DyeFabricPlanTemplate> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") DyeFabricPlanTemplate record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}