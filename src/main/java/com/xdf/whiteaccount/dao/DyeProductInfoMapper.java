package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.DyeProductInfo;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 色布品名
 * @Author : 张柯
 * @Date : 2021-06-25 09:44:52
 */
@Repository
public interface DyeProductInfoMapper {
    int insert(DyeProductInfo record);

    int insertSelective(DyeProductInfo record);

    int multiInsert(List<DyeProductInfo> list);

    int insertDumplicateKeyUpdate(DyeProductInfo record);

    int updateByPrimaryKey(DyeProductInfo record);

    int updateByPrimaryKeySelective(DyeProductInfo record);

    int multiUpdate(List<DyeProductInfo> list);

    int deleteByPrimaryKey(Integer id);

    DyeProductInfo selectByPrimaryKey(Integer id);

    List<DyeProductInfo> selectAll();

    List<DyeProductInfo> selectByParam(DyeProductInfo record);

    int updateByExampleSelective(@Param("record") DyeProductInfo record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<DyeProductInfo> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") DyeProductInfo record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}