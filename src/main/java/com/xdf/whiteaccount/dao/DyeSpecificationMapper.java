package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.DyeSpecification;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 色布规格
* @Author : 张柯
* @Date : 2021-06-26 08:36:35
*/
@Repository
public interface DyeSpecificationMapper{
    int insert(DyeSpecification record);

    int insertSelective(DyeSpecification record);

    int multiInsert(List<DyeSpecification> list);

    int insertDumplicateKeyUpdate(DyeSpecification record);

    int updateByPrimaryKey(DyeSpecification record);

    int updateByPrimaryKeySelective(DyeSpecification record);

    int multiUpdate(List<DyeSpecification> list);

    int deleteByPrimaryKey(Integer id);

    DyeSpecification selectByPrimaryKey(Integer id);

    List<DyeSpecification> selectAll();

    List<DyeSpecification> selectByParam(DyeSpecification record);

    int updateByExampleSelective(@Param("record") DyeSpecification record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<DyeSpecification> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") DyeSpecification record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}