package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.Employee;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 部门员工
 * @Author : 张柯
 * @Date : 2021-07-15 08:51:40
 */
@Repository
public interface EmployeeMapper {
    int insert(Employee record);

    int insertSelective(Employee record);

    int multiInsert(List<Employee> list);

    int insertDumplicateKeyUpdate(Employee record);

    int updateByPrimaryKey(Employee record);

    int updateByPrimaryKeySelective(Employee record);

    int multiUpdate(List<Employee> list);

    int deleteByPrimaryKey(Integer id);

    Employee selectByPrimaryKey(Integer id);

    List<Employee> selectAll();

    List<Employee> selectByParam(Employee record);

    int updateByExampleSelective(@Param("record") Employee record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<Employee> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") Employee record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}