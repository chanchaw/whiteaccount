package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.FirstCopies;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 头份
 * @Author : 张柯
 * @Date : 2021-05-26 17:52:25
 */
@Repository
public interface FirstCopiesMapper {
    int insert(FirstCopies record);

    int insertSelective(FirstCopies record);

    int multiInsert(List<FirstCopies> list);

    int insertDumplicateKeyUpdate(FirstCopies record);

    int updateByPrimaryKey(FirstCopies record);

    int updateByPrimaryKeySelective(FirstCopies record);

    int multiUpdate(List<FirstCopies> list);

    int deleteByPrimaryKey(Integer id);

    FirstCopies selectByPrimaryKey(Integer id);

    List<FirstCopies> selectAll();

    List<FirstCopies> selectByParam(FirstCopies record);

    int updateByExampleSelective(@Param("record") FirstCopies record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<FirstCopies> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") FirstCopies record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}