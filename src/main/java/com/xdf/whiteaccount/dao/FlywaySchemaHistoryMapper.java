package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.FlywaySchemaHistory;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface FlywaySchemaHistoryMapper{
    int insert(FlywaySchemaHistory record);

    int insertSelective(FlywaySchemaHistory record);

    int multiInsert(List<FlywaySchemaHistory> list);

    int insertDumplicateKeyUpdate(FlywaySchemaHistory record);

    int updateByPrimaryKey(FlywaySchemaHistory record);

    int updateByPrimaryKeySelective(FlywaySchemaHistory record);

    int multiUpdate(List<FlywaySchemaHistory> list);

    int deleteByPrimaryKey(Integer id);

    FlywaySchemaHistory selectByPrimaryKey(Integer id);

    List<FlywaySchemaHistory> selectAll();

    List<FlywaySchemaHistory> selectByParam(FlywaySchemaHistory record);

    int updateByExampleSelective(@Param("record") FlywaySchemaHistory record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<FlywaySchemaHistory> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") FlywaySchemaHistory record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}