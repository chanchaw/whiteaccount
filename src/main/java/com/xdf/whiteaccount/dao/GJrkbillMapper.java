package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.GJrkbill;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description :
 * @Author : 张柯
 * @Date : 2021-05-22 08:16:02
 */
@Repository
public interface GJrkbillMapper {
    int insert(GJrkbill record);

    int insertSelective(GJrkbill record);

    int multiInsert(List<GJrkbill> list);

    int insertDumplicateKeyUpdate(GJrkbill record);

    int updateByPrimaryKey(GJrkbill record);

    int updateByPrimaryKeySelective(GJrkbill record);

    int multiUpdate(List<GJrkbill> list);

    int deleteByPrimaryKey(Integer id);

    GJrkbill selectByPrimaryKey(Integer id);

    List<GJrkbill> selectAll();

    List<GJrkbill> selectByParam(GJrkbill record);

    int updateByExampleSelective(@Param("record") GJrkbill record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<GJrkbill> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") GJrkbill record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}