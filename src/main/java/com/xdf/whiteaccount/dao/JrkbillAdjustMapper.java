package com.xdf.whiteaccount.dao;

import com.xdf.whiteaccount.entity.JrkbillAdjust;
import com.xdf.whiteaccount.utils.Example;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
* @Description : 打卷调整表
* @Author : 张柯
* @Date : 2021-06-06 11:01:08
*/
@Repository
public interface JrkbillAdjustMapper{
    int insert(JrkbillAdjust record);

    int insertSelective(JrkbillAdjust record);

    int multiInsert(List<JrkbillAdjust> list);

    int insertDumplicateKeyUpdate(JrkbillAdjust record);

    int updateByPrimaryKey(JrkbillAdjust record);

    int updateByPrimaryKeySelective(JrkbillAdjust record);

    int multiUpdate(List<JrkbillAdjust> list);

    int deleteByPrimaryKey(Integer id);

    JrkbillAdjust selectByPrimaryKey(Integer id);

    List<JrkbillAdjust> selectAll();

    List<JrkbillAdjust> selectByParam(JrkbillAdjust record);

    int updateByExampleSelective(@Param("record") JrkbillAdjust record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<JrkbillAdjust> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") JrkbillAdjust record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

    int deleteByOpbatch(String opbatch);
}