package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.Jrkbillfh;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description :
 * @Author : 张柯
 * @Date : 2021-05-22 08:16:02
 */
@Repository
public interface JrkbillfhMapper {
    int insert(Jrkbillfh record);

    int insertSelective(Jrkbillfh record);

    int multiInsert(List<Jrkbillfh> list);

    int insertDumplicateKeyUpdate(Jrkbillfh record);

    int updateByPrimaryKey(Jrkbillfh record);

    int updateByPrimaryKeySelective(Jrkbillfh record);

    int multiUpdate(List<Jrkbillfh> list);

    int deleteByPrimaryKey(Integer id);

    Jrkbillfh selectByPrimaryKey(Integer id);

    List<Jrkbillfh> selectAll();

    List<Jrkbillfh> selectByParam(Jrkbillfh record);

    int updateByExampleSelective(@Param("record") Jrkbillfh record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<Jrkbillfh> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") Jrkbillfh record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}