package com.xdf.whiteaccount.dao;

import com.xdf.whiteaccount.entity.ProductOutwardOrder;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.Jrkbillsum;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description :
 * @Author : 张柯
 * @Date : 2021-06-02 19:49:07
 */
@Repository
public interface JrkbillsumMapper {
    int insert(Jrkbillsum record);

    int insertSelective(Jrkbillsum record);

    int multiInsert(List<Jrkbillsum> list);

    int insertDumplicateKeyUpdate(Jrkbillsum record);

    int updateByPrimaryKey(Jrkbillsum record);

    int updateByPrimaryKeySelective(Jrkbillsum record);

    int multiUpdate(List<Jrkbillsum> list);

    int deleteByPrimaryKey(Integer id);

    Jrkbillsum selectByPrimaryKey(Integer id);

    List<Jrkbillsum> selectAll();

    List<Jrkbillsum> selectByParam(Jrkbillsum record);

    int updateByExampleSelective(@Param("record") Jrkbillsum record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<Jrkbillsum> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") Jrkbillsum record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

    int multiUpdateOutwardId(ProductOutwardOrder record);

}