package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.MachineType;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 机台号
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface MachineTypeMapper{
    int insert(MachineType record);

    int insertSelective(MachineType record);

    int multiInsert(List<MachineType> list);

    int insertDumplicateKeyUpdate(MachineType record);

    int updateByPrimaryKey(MachineType record);

    int updateByPrimaryKeySelective(MachineType record);

    int multiUpdate(List<MachineType> list);

    int deleteByPrimaryKey(String id);

    MachineType selectByPrimaryKey(String id);

    List<MachineType> selectAll();

    List<MachineType> selectByParam(MachineType record);

    int updateByExampleSelective(@Param("record") MachineType record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<MachineType> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") MachineType record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}