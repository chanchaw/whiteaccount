package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.MaterialAccountPayable;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 原料应付款
 * @Author : 张柯
 * @Date : 2021-07-01 16:40:45
 */
@Repository
public interface MaterialAccountPayableMapper {
    int insert(MaterialAccountPayable record);

    int insertSelective(MaterialAccountPayable record);

    int multiInsert(List<MaterialAccountPayable> list);

    int insertDumplicateKeyUpdate(MaterialAccountPayable record);

    int updateByPrimaryKey(MaterialAccountPayable record);

    int updateByPrimaryKeySelective(MaterialAccountPayable record);

    int multiUpdate(List<MaterialAccountPayable> list);

    int deleteByPrimaryKey(Integer id);

    MaterialAccountPayable selectByPrimaryKey(Integer id);

    List<MaterialAccountPayable> selectAll();

    List<MaterialAccountPayable> selectByParam(MaterialAccountPayable record);

    int updateByExampleSelective(@Param("record") MaterialAccountPayable record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<MaterialAccountPayable> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") MaterialAccountPayable record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}