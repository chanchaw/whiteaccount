package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

import com.xdf.whiteaccount.entity.MaterialOrder;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 原料入库
 * @Author : 张柯
 * @Date : 2021-05-25 17:02:01
 */
@Repository
public interface MaterialOrderMapper {
    int insert(MaterialOrder record);

    int insertSelective(MaterialOrder record);

    int multiInsert(List<MaterialOrder> list);

    int insertDumplicateKeyUpdate(MaterialOrder record);

    int updateByPrimaryKey(MaterialOrder record);

    int updateByPrimaryKeySelective(MaterialOrder record);

    int multiUpdate(List<MaterialOrder> list);

    int deleteByPrimaryKey(Long id);

    MaterialOrder selectByPrimaryKey(Long id);

    List<MaterialOrder> selectAll();

    List<MaterialOrder> selectByParam(MaterialOrder record);

    int updateByExampleSelective(@Param("record") MaterialOrder record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<MaterialOrder> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") MaterialOrder record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

    List<Map<String, Object>> getMaterialStorageDropdownList(@Param("materialSpec") String materialSpec, @Param("mode") int mode, @Param("array") String[] array);
}