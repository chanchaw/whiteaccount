package com.xdf.whiteaccount.dao;

import com.xdf.whiteaccount.entity.MeterLength;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MeterLengthMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MeterLength record);

    int insertSelective(MeterLength record);

    MeterLength selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MeterLength record);

    int updateByPrimaryKey(MeterLength record);

    List<MeterLength> listQuery(MeterLength record);
}