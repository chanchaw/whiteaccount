package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.Payment;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 付款
 * @Author : 张柯
 * @Date : 2021-05-31 14:14:23
 */
@Repository
public interface PaymentMapper {
    int insert(Payment record);

    int insertSelective(Payment record);

    int multiInsert(List<Payment> list);

    int insertDumplicateKeyUpdate(Payment record);

    int updateByPrimaryKey(Payment record);

    int updateByPrimaryKeySelective(Payment record);

    int multiUpdate(List<Payment> list);

    int deleteByPrimaryKey(Integer id);

    Payment selectByPrimaryKey(Integer id);

    List<Payment> selectAll();

    List<Payment> selectByParam(Payment record);

    int updateByExampleSelective(@Param("record") Payment record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<Payment> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") Payment record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}