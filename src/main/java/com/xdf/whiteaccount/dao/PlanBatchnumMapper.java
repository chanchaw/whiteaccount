package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.PlanBatchnum;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 白坯计划与批次号的对应关系
 * @Author : 张柯
 * @Date : 2021-07-19 09:24:54
 */
@Repository
public interface PlanBatchnumMapper {
    int insert(PlanBatchnum record);

    int insertSelective(PlanBatchnum record);

    int multiInsert(List<PlanBatchnum> list);

    int insertDumplicateKeyUpdate(PlanBatchnum record);

    int updateByPrimaryKey(PlanBatchnum record);

    int updateByPrimaryKeySelective(PlanBatchnum record);

    int multiUpdate(List<PlanBatchnum> list);

    int deleteByPrimaryKey(Long id);

    PlanBatchnum selectByPrimaryKey(Long id);

    List<PlanBatchnum> selectAll();

    List<PlanBatchnum> selectByParam(PlanBatchnum record);

    int updateByExampleSelective(@Param("record") PlanBatchnum record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<PlanBatchnum> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") PlanBatchnum record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);
}