package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.Plan;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 计划
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface PlanMapper{
    int insert(Plan record);

    int insertSelective(Plan record);

    int multiInsert(List<Plan> list);

    int insertDumplicateKeyUpdate(Plan record);

    int updateByPrimaryKey(Plan record);

    int updateByPrimaryKeySelective(Plan record);

    int multiUpdate(List<Plan> list);

    int deleteByPrimaryKey(Long id);

    Plan selectByPrimaryKey(Long id);

    List<Plan> selectAll();

    List<Plan> selectByParam(Plan record);

    int updateByExampleSelective(@Param("record") Plan record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<Plan> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") Plan record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

    int auditAll();

}