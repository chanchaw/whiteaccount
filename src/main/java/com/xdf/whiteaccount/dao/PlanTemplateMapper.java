package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.PlanTemplate;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 计划单模板
 * @Author : 张柯
 * @Date : 2021-05-20 19:00:08
 */
@Repository
public interface PlanTemplateMapper {
    int insert(PlanTemplate record);

    int insertSelective(PlanTemplate record);

    int multiInsert(List<PlanTemplate> list);

    int insertDumplicateKeyUpdate(PlanTemplate record);

    int updateByPrimaryKey(PlanTemplate record);

    int updateByPrimaryKeySelective(PlanTemplate record);

    int multiUpdate(List<PlanTemplate> list);

    int deleteByPrimaryKey(Long id);

    PlanTemplate selectByPrimaryKey(Long id);

    PlanTemplate selectAll();

    List<PlanTemplate> selectByParam(PlanTemplate record);

    int updateByExampleSelective(@Param("record") PlanTemplate record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<PlanTemplate> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") PlanTemplate record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}