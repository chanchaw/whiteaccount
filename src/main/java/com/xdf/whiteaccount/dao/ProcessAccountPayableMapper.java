package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.ProcessAccountPayable;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 外加工应付款
 * @Author : 张柯
 * @Date : 2021-07-16 12:10:29
 */
@Repository
public interface ProcessAccountPayableMapper {
    int insert(ProcessAccountPayable record);

    int insertSelective(ProcessAccountPayable record);

    int multiInsert(List<ProcessAccountPayable> list);

    int insertDumplicateKeyUpdate(ProcessAccountPayable record);

    int updateByPrimaryKey(ProcessAccountPayable record);

    int updateByPrimaryKeySelective(ProcessAccountPayable record);

    int multiUpdate(List<ProcessAccountPayable> list);

    int deleteByPrimaryKey(Integer id);

    ProcessAccountPayable selectByPrimaryKey(Integer id);

    List<ProcessAccountPayable> selectAll();

    List<ProcessAccountPayable> selectByParam(ProcessAccountPayable record);

    int updateByExampleSelective(@Param("record") ProcessAccountPayable record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<ProcessAccountPayable> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") ProcessAccountPayable record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}