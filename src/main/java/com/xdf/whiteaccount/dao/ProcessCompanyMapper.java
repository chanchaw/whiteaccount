package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.ProcessCompany;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 加工户
 * @Author : 张柯
 * @Date : 2021-06-30 12:54:45
 */
@Repository
public interface ProcessCompanyMapper {
    int insert(ProcessCompany record);

    int insertSelective(ProcessCompany record);

    int multiInsert(List<ProcessCompany> list);

    int insertDumplicateKeyUpdate(ProcessCompany record);

    int updateByPrimaryKey(ProcessCompany record);

    int updateByPrimaryKeySelective(ProcessCompany record);

    int multiUpdate(List<ProcessCompany> list);

    int deleteByPrimaryKey(Integer id);

    ProcessCompany selectByPrimaryKey(Integer id);

    List<ProcessCompany> selectAll();

    List<ProcessCompany> selectByParam(ProcessCompany record);

    int updateByExampleSelective(@Param("record") ProcessCompany record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<ProcessCompany> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") ProcessCompany record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}