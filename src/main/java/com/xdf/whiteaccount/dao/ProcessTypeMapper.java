package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.ProcessType;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 加工类型
 * @Author : 张柯
 * @Date : 2021-06-23 16:01:16
 */
@Repository
public interface ProcessTypeMapper {
    int insert(ProcessType record);

    int insertSelective(ProcessType record);

    int multiInsert(List<ProcessType> list);

    int insertDumplicateKeyUpdate(ProcessType record);

    int updateByPrimaryKey(ProcessType record);

    int updateByPrimaryKeySelective(ProcessType record);

    int multiUpdate(List<ProcessType> list);

    int deleteByPrimaryKey(Integer id);

    ProcessType selectByPrimaryKey(Integer id);

    List<ProcessType> selectAll();

    List<ProcessType> selectByParam(ProcessType record);

    int updateByExampleSelective(@Param("record") ProcessType record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<ProcessType> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") ProcessType record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}