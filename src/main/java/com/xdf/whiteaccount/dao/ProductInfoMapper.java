package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.ProductInfo;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 基础资料-品名
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface ProductInfoMapper{
    int insert(ProductInfo record);

    int insertSelective(ProductInfo record);

    int multiInsert(List<ProductInfo> list);

    int insertDumplicateKeyUpdate(ProductInfo record);

    int updateByPrimaryKey(ProductInfo record);

    int updateByPrimaryKeySelective(ProductInfo record);

    int multiUpdate(List<ProductInfo> list);

    int deleteByPrimaryKey(Integer id);

    ProductInfo selectByPrimaryKey(Integer id);

    List<ProductInfo> selectAll();

    List<ProductInfo> selectByParam(ProductInfo record);

    int updateByExampleSelective(@Param("record") ProductInfo record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<ProductInfo> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") ProductInfo record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}