package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.ProductOutwardOrder;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 外发入库单据
 * @Author : 张柯
 * @Date : 2021-07-02 17:20:51
 */
@Repository
public interface ProductOutwardOrderMapper {
    int insert(ProductOutwardOrder record);

    int insertSelective(ProductOutwardOrder record);

    int multiInsert(List<ProductOutwardOrder> list);

    int insertDumplicateKeyUpdate(ProductOutwardOrder record);

    int updateByPrimaryKey(ProductOutwardOrder record);

    int updateByPrimaryKeySelective(ProductOutwardOrder record);

    int multiUpdate(List<ProductOutwardOrder> list);

    int deleteByPrimaryKey(Long id);

    ProductOutwardOrder selectByPrimaryKey(Long id);

    List<ProductOutwardOrder> selectAll();

    List<ProductOutwardOrder> selectByParam(ProductOutwardOrder record);

    int updateByExampleSelective(@Param("record") ProductOutwardOrder record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<ProductOutwardOrder> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") ProductOutwardOrder record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

    List<ProductOutwardOrder> gatherByJrk(List<Long> jrkIds);

    /**
     * 2021年8月21日 16:53:09 chanchaw
     * 修改之前的设计了，从白坯打卷数据导入为外发入库时，一对一生成
     * @param jrkIds
     * @return
     */
    List<ProductOutwardOrder> mapping8jrk(List<Long> jrkIds);
}