package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.Qrlogin;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 二维码登录日志表
* @Author : 张柯
* @Date : 2021-06-03 19:48:25
*/
@Repository
public interface QrloginMapper{
    int insert(Qrlogin record);

    int insertSelective(Qrlogin record);

    int multiInsert(List<Qrlogin> list);

    int insertDumplicateKeyUpdate(Qrlogin record);

    int updateByPrimaryKey(Qrlogin record);

    int updateByPrimaryKeySelective(Qrlogin record);

    int multiUpdate(List<Qrlogin> list);

    int deleteByPrimaryKey(Integer id);

    Qrlogin selectByPrimaryKey(Integer id);

    List<Qrlogin> selectAll();

    List<Qrlogin> selectByParam(Qrlogin record);

    int updateByExampleSelective(@Param("record") Qrlogin record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<Qrlogin> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") Qrlogin record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}