package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.Receivables;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 
* @Author : 张柯
* @Date : 2021-06-07 16:35:39
*/
@Repository
public interface ReceivablesMapper{
    int insert(Receivables record);

    int insertSelective(Receivables record);

    int multiInsert(List<Receivables> list);

    int insertDumplicateKeyUpdate(Receivables record);

    int updateByPrimaryKey(Receivables record);

    int updateByPrimaryKeySelective(Receivables record);

    int multiUpdate(List<Receivables> list);

    int deleteByPrimaryKey(Integer id);

    Receivables selectByPrimaryKey(Integer id);

    List<Receivables> selectAll();

    List<Receivables> selectByParam(Receivables record);

    int updateByExampleSelective(@Param("record") Receivables record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<Receivables> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") Receivables record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}