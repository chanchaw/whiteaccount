package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.Specification;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 规格
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface SpecificationMapper{
    int insert(Specification record);

    int insertSelective(Specification record);

    int multiInsert(List<Specification> list);

    int insertDumplicateKeyUpdate(Specification record);

    int updateByPrimaryKey(Specification record);

    int updateByPrimaryKeySelective(Specification record);

    int multiUpdate(List<Specification> list);

    int deleteByPrimaryKey(Long id);

    Specification selectByPrimaryKey(Long id);

    List<Specification> selectAll();

    List<Specification> selectByParam(Specification record);

    int updateByExampleSelective(@Param("record") Specification record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<Specification> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") Specification record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}