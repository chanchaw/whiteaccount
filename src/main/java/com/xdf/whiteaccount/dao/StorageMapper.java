package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.Storage;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 仓库
 * @Author : 张柯
 * @Date : 2021-07-23 09:48:38
 */
@Repository
public interface StorageMapper {
    int insert(Storage record);

    int insertSelective(Storage record);

    int multiInsert(List<Storage> list);

    int insertDumplicateKeyUpdate(Storage record);

    int updateByPrimaryKey(Storage record);

    int updateByPrimaryKeySelective(Storage record);

    int multiUpdate(List<Storage> list);

    int deleteByPrimaryKey(Integer id);

    Storage selectByPrimaryKey(Integer id);

    List<Storage> selectAll();

    List<Storage> selectByParam(Storage record);

    int updateByExampleSelective(@Param("record") Storage record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<Storage> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") Storage record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}