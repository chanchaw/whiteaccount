package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 供应商
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface SupplierMapper{
    int insert(Supplier record);

    int insertSelective(Supplier record);

    int multiInsert(List<Supplier> list);

    int insertDumplicateKeyUpdate(Supplier record);

    int updateByPrimaryKey(Supplier record);

    int updateByPrimaryKeySelective(Supplier record);

    int multiUpdate(List<Supplier> list);

    int deleteByPrimaryKey(String id);

    Supplier selectByPrimaryKey(String id);

    List<Supplier> selectAll();

    List<Supplier> selectByParam(Supplier record);

    int updateByExampleSelective(@Param("record") Supplier record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<Supplier> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") Supplier record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}