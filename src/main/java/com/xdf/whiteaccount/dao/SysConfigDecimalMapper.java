package com.xdf.whiteaccount.dao;

import com.xdf.whiteaccount.entity.SysConfigDecimal;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysConfigDecimalMapper {
    int insert(SysConfigDecimal record);

    int insertSelective(SysConfigDecimal record);

    SysConfigDecimal selectByPrimaryKey(String config_field);

    int deleteByPrimaryKey(String config_field);

    int updateByPrimaryKey(SysConfigDecimal record);

    int updateByPrimaryKeySelective(SysConfigDecimal record);

    List<SysConfigDecimal> selectByParam(SysConfigDecimal record);

    int updateByZGParam(SysConfigDecimal record);
}