package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.SysConfigInt;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 配置表
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
@Repository
public interface SysConfigIntMapper {
    int insert(SysConfigInt record);

    int insertSelective(SysConfigInt record);

    int multiInsert(List<SysConfigInt> list);

    int insertDumplicateKeyUpdate(SysConfigInt record);

    int updateByPrimaryKey(SysConfigInt record);

    int updateByPrimaryKeySelective(SysConfigInt record);

    int multiUpdate(List<SysConfigInt> list);

    int deleteByPrimaryKey(String id);

    SysConfigInt selectByPrimaryKey(String id);

    List<SysConfigInt> selectAll();

    List<SysConfigInt> selectByParam(SysConfigInt record);

    int updateByExampleSelective(@Param("record") SysConfigInt record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<SysConfigInt> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") SysConfigInt record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}