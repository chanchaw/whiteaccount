package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.SysConfigVarchar;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 字符配置
 * @Author : 张柯
 * @Date : 2021-05-26 14:22:09
 */
@Repository
public interface SysConfigVarcharMapper {
    int insert(SysConfigVarchar record);

    int insertSelective(SysConfigVarchar record);

    int multiInsert(List<SysConfigVarchar> list);

    int insertDumplicateKeyUpdate(SysConfigVarchar record);

    int updateByPrimaryKey(SysConfigVarchar record);

    int updateByPrimaryKeySelective(SysConfigVarchar record);

    int multiUpdate(List<SysConfigVarchar> list);

    int deleteByPrimaryKey(String id);

    SysConfigVarchar selectByPrimaryKey(String id);

    List<SysConfigVarchar> selectAll();

    List<SysConfigVarchar> selectByParam(SysConfigVarchar record);

    int updateByExampleSelective(@Param("record") SysConfigVarchar record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<SysConfigVarchar> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") SysConfigVarchar record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}