package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.SysMenu;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 菜单表
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface SysMenuMapper{
    int insert(SysMenu record);

    int insertSelective(SysMenu record);

    int multiInsert(List<SysMenu> list);

    int insertDumplicateKeyUpdate(SysMenu record);

    int updateByPrimaryKey(SysMenu record);

    int updateByPrimaryKeySelective(SysMenu record);

    int multiUpdate(List<SysMenu> list);

    int deleteByPrimaryKey(Integer id);

    SysMenu selectByPrimaryKey(Integer id);

    List<SysMenu> selectAll();

    List<SysMenu> selectByParam(SysMenu record);

    int updateByExampleSelective(@Param("record") SysMenu record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<SysMenu> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") SysMenu record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}