package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.SysPassword;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 密码
 * @Author : 张柯
 * @Date : 2021-06-21 15:19:40
 */
@Repository
public interface SysPasswordMapper {
    int insert(SysPassword record);

    int insertSelective(SysPassword record);

    int multiInsert(List<SysPassword> list);

    int insertDumplicateKeyUpdate(SysPassword record);

    int updateByPrimaryKey(SysPassword record);

    int updateByPrimaryKeySelective(SysPassword record);

    int multiUpdate(List<SysPassword> list);

    int deleteByPrimaryKey(String id);

    SysPassword selectByPrimaryKey(String id);

    List<SysPassword> selectAll();

    List<SysPassword> selectByParam(SysPassword record);

    int updateByExampleSelective(@Param("record") SysPassword record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<SysPassword> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") SysPassword record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}