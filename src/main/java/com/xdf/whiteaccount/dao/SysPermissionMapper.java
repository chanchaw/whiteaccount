package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.SysPermission;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 权限表
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface SysPermissionMapper{
    int insert(SysPermission record);

    int insertSelective(SysPermission record);

    int multiInsert(List<SysPermission> list);

    int insertDumplicateKeyUpdate(SysPermission record);

    int updateByPrimaryKey(SysPermission record);

    int updateByPrimaryKeySelective(SysPermission record);

    int multiUpdate(List<SysPermission> list);

    int deleteByPrimaryKey(Integer id);

    SysPermission selectByPrimaryKey(Integer id);

    List<SysPermission> selectAll();

    List<SysPermission> selectByParam(SysPermission record);

    int updateByExampleSelective(@Param("record") SysPermission record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<SysPermission> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") SysPermission record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}