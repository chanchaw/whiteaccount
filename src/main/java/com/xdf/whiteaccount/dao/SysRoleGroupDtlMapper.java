package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.SysRoleGroupDtl;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 角色组与角色的对应关系表
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface SysRoleGroupDtlMapper{
    int insert(SysRoleGroupDtl record);

    int insertSelective(SysRoleGroupDtl record);

    int multiInsert(List<SysRoleGroupDtl> list);

    int insertDumplicateKeyUpdate(SysRoleGroupDtl record);

    int updateByPrimaryKey(SysRoleGroupDtl record);

    int updateByPrimaryKeySelective(SysRoleGroupDtl record);

    int multiUpdate(List<SysRoleGroupDtl> list);

    int deleteByPrimaryKey(Integer id);

    SysRoleGroupDtl selectByPrimaryKey(Integer id);

    List<SysRoleGroupDtl> selectAll();

    List<SysRoleGroupDtl> selectByParam(SysRoleGroupDtl record);

    int updateByExampleSelective(@Param("record") SysRoleGroupDtl record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<SysRoleGroupDtl> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") SysRoleGroupDtl record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}