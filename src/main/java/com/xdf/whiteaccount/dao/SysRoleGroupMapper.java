package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.SysRoleGroup;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 角色组
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface SysRoleGroupMapper{
    int insert(SysRoleGroup record);

    int insertSelective(SysRoleGroup record);

    int multiInsert(List<SysRoleGroup> list);

    int insertDumplicateKeyUpdate(SysRoleGroup record);

    int updateByPrimaryKey(SysRoleGroup record);

    int updateByPrimaryKeySelective(SysRoleGroup record);

    int multiUpdate(List<SysRoleGroup> list);

    int deleteByPrimaryKey(Integer id);

    SysRoleGroup selectByPrimaryKey(Integer id);

    List<SysRoleGroup> selectAll();

    List<SysRoleGroup> selectByParam(SysRoleGroup record);

    int updateByExampleSelective(@Param("record") SysRoleGroup record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<SysRoleGroup> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") SysRoleGroup record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}