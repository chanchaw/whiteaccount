package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.SysRole;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 角色表
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface SysRoleMapper{
    int insert(SysRole record);

    int insertSelective(SysRole record);

    int multiInsert(List<SysRole> list);

    int insertDumplicateKeyUpdate(SysRole record);

    int updateByPrimaryKey(SysRole record);

    int updateByPrimaryKeySelective(SysRole record);

    int multiUpdate(List<SysRole> list);

    int deleteByPrimaryKey(Integer id);

    SysRole selectByPrimaryKey(Integer id);

    List<SysRole> selectAll();

    List<SysRole> selectByParam(SysRole record);

    int updateByExampleSelective(@Param("record") SysRole record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<SysRole> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") SysRole record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}