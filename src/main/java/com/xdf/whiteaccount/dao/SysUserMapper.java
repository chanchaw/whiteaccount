package com.xdf.whiteaccount.dao;

import com.xdf.whiteaccount.vo.SysUserVO;
import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.SysUser;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 登录用户表
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface SysUserMapper{
    int insert(SysUser record);

    int insertSelective(SysUser record);

    int multiInsert(List<SysUser> list);

    int insertDumplicateKeyUpdate(SysUser record);

    int updateByPrimaryKey(SysUser record);

    int updateByPrimaryKeySelective(SysUser record);

    int multiUpdate(List<SysUser> list);

    int deleteByPrimaryKey(Integer id);

    SysUser selectByPrimaryKey(Integer id);

    List<SysUser> selectAll();

    List<SysUser> selectByParam(SysUser record);

    int updateByExampleSelective(@Param("record") SysUser record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<SysUser> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") SysUser record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

    List<SysUserVO> selectByState(@Param("state") Integer state);
}