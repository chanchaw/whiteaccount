package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.SysUserRoleGroup;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 用户与角色组对应关系表
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface SysUserRoleGroupMapper{
    int insert(SysUserRoleGroup record);

    int insertSelective(SysUserRoleGroup record);

    int multiInsert(List<SysUserRoleGroup> list);

    int insertDumplicateKeyUpdate(SysUserRoleGroup record);

    int updateByPrimaryKey(SysUserRoleGroup record);

    int updateByPrimaryKeySelective(SysUserRoleGroup record);

    int multiUpdate(List<SysUserRoleGroup> list);

    int deleteByPrimaryKey(Integer id);

    SysUserRoleGroup selectByPrimaryKey(Integer id);

    List<SysUserRoleGroup> selectAll();

    List<SysUserRoleGroup> selectByParam(SysUserRoleGroup record);

    int updateByExampleSelective(@Param("record") SysUserRoleGroup record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<SysUserRoleGroup> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") SysUserRoleGroup record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}