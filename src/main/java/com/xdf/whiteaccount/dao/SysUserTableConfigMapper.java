package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.SysUserTableConfig;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 用户报表列参数配置
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface SysUserTableConfigMapper{
    int insert(SysUserTableConfig record);

    int insertSelective(SysUserTableConfig record);

    int multiInsert(List<SysUserTableConfig> list);

    int insertDumplicateKeyUpdate(SysUserTableConfig record);

    int updateByPrimaryKey(SysUserTableConfig record);

    int updateByPrimaryKeySelective(SysUserTableConfig record);

    int multiUpdate(List<SysUserTableConfig> list);

    int deleteByPrimaryKey(Integer id);

    SysUserTableConfig selectByPrimaryKey(Integer id);

    List<SysUserTableConfig> selectAll();

    List<SysUserTableConfig> selectByParam(SysUserTableConfig record);

    int updateByExampleSelective(@Param("record") SysUserTableConfig record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<SysUserTableConfig> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") SysUserTableConfig record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}