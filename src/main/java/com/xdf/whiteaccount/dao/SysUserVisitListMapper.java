package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

import com.xdf.whiteaccount.entity.SysUserVisitList;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 用户常用访问表
* @Author : 张柯
* @Date : 2021-07-31 16:39:12
*/
@Repository
public interface SysUserVisitListMapper{
    int insert(SysUserVisitList record);

    int insertSelective(SysUserVisitList record);

    int multiInsert(List<SysUserVisitList> list);

    int insertDumplicateKeyUpdate(SysUserVisitList record);

    int updateByPrimaryKey(SysUserVisitList record);

    int updateByPrimaryKeySelective(SysUserVisitList record);

    int multiUpdate(List<SysUserVisitList> list);

    int deleteByPrimaryKey(Integer id);

    SysUserVisitList selectByPrimaryKey(Integer id);

    List<SysUserVisitList> selectAll();

    List<SysUserVisitList> selectByParam(SysUserVisitList record);

    int updateByExampleSelective(@Param("record") SysUserVisitList record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<SysUserVisitList> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") SysUserVisitList record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

    List<Map<String, Object>> queryByUserId(Integer userId);
}