package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.xdf.whiteaccount.entity.TakeDeliveryAddress;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;

/**
 * @Description : 收货地址
 * @Author : 张柯
 * @Date : 2021-05-22 14:01:02
 */
@Repository
public interface TakeDeliveryAddressMapper {
    int insert(TakeDeliveryAddress record);

    int insertSelective(TakeDeliveryAddress record);

    int multiInsert(List<TakeDeliveryAddress> list);

    int insertDumplicateKeyUpdate(TakeDeliveryAddress record);

    int updateByPrimaryKey(TakeDeliveryAddress record);

    int updateByPrimaryKeySelective(TakeDeliveryAddress record);

    int multiUpdate(List<TakeDeliveryAddress> list);

    int deleteByPrimaryKey(Integer id);

    TakeDeliveryAddress selectByPrimaryKey(Integer id);

    List<TakeDeliveryAddress> selectAll();

    List<TakeDeliveryAddress> selectByParam(TakeDeliveryAddress record);

    int updateByExampleSelective(@Param("record") TakeDeliveryAddress record, @Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<TakeDeliveryAddress> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") TakeDeliveryAddress record, @Param("example") Example example);

    long countByExample(@Param("example") Example example);

}