package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.WeightOfFabric;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 克重
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface WeightOfFabricMapper{
    int insert(WeightOfFabric record);

    int insertSelective(WeightOfFabric record);

    int multiInsert(List<WeightOfFabric> list);

    int insertDumplicateKeyUpdate(WeightOfFabric record);

    int updateByPrimaryKey(WeightOfFabric record);

    int updateByPrimaryKeySelective(WeightOfFabric record);

    int multiUpdate(List<WeightOfFabric> list);

    int deleteByPrimaryKey(Long id);

    WeightOfFabric selectByPrimaryKey(Long id);

    List<WeightOfFabric> selectAll();

    List<WeightOfFabric> selectByParam(WeightOfFabric record);

    int updateByExampleSelective(@Param("record") WeightOfFabric record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<WeightOfFabric> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") WeightOfFabric record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}