package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.Whitebillfh;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 
* @Author : 张柯
* @Date : 2021-05-22 08:16:02
*/
@Repository
public interface WhitebillfhMapper{
    int insert(Whitebillfh record);

    int insertSelective(Whitebillfh record);

    int multiInsert(List<Whitebillfh> list);

    int insertDumplicateKeyUpdate(Whitebillfh record);

    int updateByPrimaryKey(Whitebillfh record);

    int updateByPrimaryKeySelective(Whitebillfh record);

    int multiUpdate(List<Whitebillfh> list);

    int deleteByPrimaryKey(Integer id);

    Whitebillfh selectByPrimaryKey(Integer id);

    List<Whitebillfh> selectAll();

    List<Whitebillfh> selectByParam(Whitebillfh record);

    int updateByExampleSelective(@Param("record") Whitebillfh record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<Whitebillfh> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") Whitebillfh record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}