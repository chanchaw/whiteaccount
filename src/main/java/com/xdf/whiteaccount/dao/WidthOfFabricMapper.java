package com.xdf.whiteaccount.dao;

import org.springframework.stereotype.Repository;
import java.util.List;
import com.xdf.whiteaccount.entity.WidthOfFabric;
import org.apache.ibatis.annotations.Param;
import com.xdf.whiteaccount.utils.Example;
/**
* @Description : 门幅
* @Author : 张柯
* @Date : 2021-05-16 09:40:12
*/
@Repository
public interface WidthOfFabricMapper{
    int insert(WidthOfFabric record);

    int insertSelective(WidthOfFabric record);

    int multiInsert(List<WidthOfFabric> list);

    int insertDumplicateKeyUpdate(WidthOfFabric record);

    int updateByPrimaryKey(WidthOfFabric record);

    int updateByPrimaryKeySelective(WidthOfFabric record);

    int multiUpdate(List<WidthOfFabric> list);

    int deleteByPrimaryKey(Long id);

    WidthOfFabric selectByPrimaryKey(Long id);

    List<WidthOfFabric> selectAll();

    List<WidthOfFabric> selectByParam(WidthOfFabric record);

    int updateByExampleSelective(@Param("record") WidthOfFabric record,@Param("example") Example example);

    int deleteByExample(@Param("example") Example example);

    List<WidthOfFabric> selectByExample(@Param("example") Example example);

    int insertByExampleSelective(@Param("record") WidthOfFabric record,@Param("example") Example example);

    long countByExample(@Param("example") Example example);

}