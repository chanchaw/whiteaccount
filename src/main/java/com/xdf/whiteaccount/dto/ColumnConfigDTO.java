package com.xdf.whiteaccount.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-05-17 10:50
 **/
@Data
@Builder
public class ColumnConfigDTO {
    private String field;
    private Double width;
    private Boolean hidden;
    private String align;
    private Integer sysMenuId;
    private Integer serialNum;

    @Tolerate
    public ColumnConfigDTO() { }
}
