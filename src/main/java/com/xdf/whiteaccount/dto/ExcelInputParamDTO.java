package com.xdf.whiteaccount.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: xzy-management-system
 * @description: Excel导入参数
 * @author: 张柯
 * @create: 2021-01-17 14:01
 **/
@Data
@Builder
public class ExcelInputParamDTO {
    /** 文件名称 */
    private String fileName;

    /** 列字段配置，格式:[{字段名:Excel列序号},...] */
    private Map<String, Integer> fieldSetting = new HashMap<>();

    /** 从第几列开始 */
    private Integer from;

    /** 从第几列结束 */
    private Integer to;

    /** 长度 */
    private Integer len;

    /** 第几个sheet */
    private Integer sheetIndex;

    @Tolerate
    public ExcelInputParamDTO() { }
}
