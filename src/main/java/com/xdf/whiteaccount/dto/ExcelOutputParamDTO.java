package com.xdf.whiteaccount.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: xzy-management-system
 * @description: Excel导出参数
 * @author: 张柯
 * @create: 2021-04-06 10:57
 **/
@Data
@Builder
public class ExcelOutputParamDTO {
    /** 文件名 */
    private String fileName;
    /** 文件后缀 */
    private String fileSuffix;
    /** 数据源 */
    private List<ExcelSheet> dataSource = new ArrayList<>();
    @Tolerate
    public ExcelOutputParamDTO() { }
}
