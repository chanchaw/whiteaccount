package com.xdf.whiteaccount.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: xzy-management-system
 * @description: Excel sheet数据源
 * @author: 张柯
 * @create: 2021-04-07 14:38
 **/
@Data
@Builder
public class ExcelSheet {
    private String sheetName;
    private String topic;
    private Map<Integer, String> columns = new HashMap<>();
    private List<Map<Integer, Object>> data = new ArrayList();
    private int topicRowLine = 0;
    private int startRowLine;

    @Tolerate
    public ExcelSheet() {
    }
}