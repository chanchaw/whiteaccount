package com.xdf.whiteaccount.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: xzy-management-system
 * @description: datagrid的数据源
 * @author: 张柯
 * @create: 2021-02-01 14:26
 **/
@Data
@Builder
public class JsonResult<T> {
    private List<T> rows = new ArrayList<>();
    private Long total;
    @Tolerate
    public JsonResult() {
    }
}
