package com.xdf.whiteaccount.dto;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * 2021年9月2日 13:22:55 chanchaw
 * 移库请求的 DTO
 * com.xdf.whiteaccount.controller.JrkbillAdjustController.migrateStock
 * 方法用到的 DTO
 */
@Data
@ToString
public class MigrateStockDTO {
    private Long ido;
    private Long idt;
    private Integer pairs;
    private BigDecimal qty;

    private static final long serialVersionUID = 1L;
}
