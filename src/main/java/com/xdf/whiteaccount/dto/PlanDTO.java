package com.xdf.whiteaccount.dto;

import com.xdf.whiteaccount.entity.Plan;
import com.xdf.whiteaccount.entity.PlanBatchnum;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @program: white-account
 * @description: 白坯计划
 * @author: 张柯
 * @create: 2021-07-19 09:29
 **/
@Data
@Builder
public class PlanDTO {
    /** 计划 */
    @NotNull(message = "计划对象不允许为空！")
    private Plan plan;

    /** 对应批次号明细 */
    private List<PlanBatchnum> detail;

    @Tolerate
    public PlanDTO() { }
}
