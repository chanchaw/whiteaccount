package com.xdf.whiteaccount.dto;

import lombok.*;
import lombok.experimental.Tolerate;

/**
 * @program: xzy-management-system
 * @description: 查询参数, 服务端过滤
 * @author: 张柯
 * @create: 2021-02-01 09:37
 **/
@Data
@Builder
public class QueryParams {
    private Long page;
    private Long rows;
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private Long startRow;

    public Long getStartRow() {
        if (this.startRow != null) return this.startRow;
        if (this.rows == null) return 0L;
        long page = this.page != null ? this.page : 1;
        return this.startRow = (page - 1) * rows;
    }

    public void setStartRow(Long startRow) {
        this.startRow = startRow;
    }

    @Tolerate
    public QueryParams() {
    }
}
