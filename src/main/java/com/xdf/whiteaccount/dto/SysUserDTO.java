package com.xdf.whiteaccount.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

/**
 * @program: xzy-management-system
 * @description: UserDTO
 * @author: 张柯
 * @create: 2021-01-06 12:53
 **/
@Data
@Builder
public class SysUserDTO {
    /** 无备注 */
    private Integer id;

    /** 登录用户名 */
    private String loginName;

    /** 姓名 */
    private String userName;

    /** 性别：男(1);女(0) */
    private Boolean userSex;

    /** 备注 */
    private String remarks;

    /** 状态(1:在职,2:离职,0:停用) */
    private Integer userState;

    /** 工程名 */
    private String projectId;

    @Tolerate
    public SysUserDTO() { }
}
