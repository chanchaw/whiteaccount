package com.xdf.whiteaccount.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: xzy-management-system
 * @description: tree
 * @author: 张柯
 * @create: 2021-01-03 13:58
 **/
@Data
@Builder
public class TreeDTO {
    /**
     * 编号
     */
    private Integer id;
    /**
     * 名称
     */
    private String text;
    /**
     * 图标名
     */
    private String iconCls;
    /**
     * 选中状态
     */
    private int checked;
    /**
     * 排序字段
     */
    private Integer serialNo;
    /**
     * 子节点
     */
    private List<TreeDTO> children = new ArrayList<TreeDTO>();
    /**
     * 附加元素
     */
    private Map<String, Object> attribute = new HashMap<String, Object>();
    /**
     * 展开或关闭状态(closed;open),如果为closed,则点击当前节点会将节点id返回后台并根据此id查询该节点下的子节点
     */
    private String state;

    @Tolerate
    public TreeDTO() { }
}
