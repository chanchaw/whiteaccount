package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 单据类型(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-07-16 15:55:56
    */
@Data
@Builder
public class BillType implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 单据类型编号 */
    private String id;

    /** 单据类型名称 */
    private String billTypeName;

    /** 数据影响(0,1,-1) */
    private Integer accessMode;

    /** 是否允许手动生成 */
    private Boolean allowManual;

    /** 所属仓库编号 */
    private String storageId;

    /** 扣库类型(本期入库:1;本期出库:-1) */
    private Integer inoutType;

    /** 是否默认选中 */
    private Boolean isSelected;

    /** 是否盘点的类型(0:否;1:是;) */
    private Boolean isCheck;

    /** 是否退回的类型(0:否;1:是;) */
    private Boolean isReturn;

    /** 0:原料商;1:加工户; */
    private Integer company;
    /**
    * 无参构造方法
    */
    @Tolerate
    public BillType(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}