package com.xdf.whiteaccount.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : 往来单位(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-16 09:40:12
    */
@Data
@Builder
public class ContactCompany implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 公司名称 */
    private String companyName;

    /** 公司编码 */
    private String companyCode;

    /** 公司简称 */
    private String companyAlias;

    /** 结算客户 */
    private String settlementClient;

    /** 业务员 */
    private String businessManager;

    /** 电话号码 */
    private String phoneNumber;

    /** 地区 */
    private String area;

    /** 地址 */
    private String address;

    /** 邮编 */
    private String postCode;

    /** 开户行 */
    private String bankOfDeposit;

    /** 银行账号 */
    private String bankAccount;

    /** 税号 */
    private String dutyParagraph;

    /** 创建人编号 */
    private Integer createUserId;

    /** 创建时间，DB自动生成 */
//    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /** 备注 */
    private String remarks;

    /** 排序序号 */
    private Integer serialNo;

    /** 状态(0:删除;1:正常;) */
    private Integer state;

    // 2021年11月20日 13:18:02 chanchaw 追加客户分类
    private String companyCategories;
    /**
    * 无参构造方法
    */
    @Tolerate
    public ContactCompany(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}