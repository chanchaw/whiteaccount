package com.xdf.whiteaccount.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.math.BigDecimal;
import java.io.Serializable;
    /**
    * @Description : (该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-08-07 16:38:26
    */
@Data
@Builder
public class CpBillfh implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 发货主表主键（色布发货） */
    private Integer id;

    /** 计划单主键 */
    private Integer planid;

    /** 单据编号 */
    private String billcode;

    /** 客户 */
    private String client;

    /** 发货时间 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date datafh;

    /** 匹数 */
    private String ps;

    /** 重量 */
    private String qty;

    /** 对账单价 */
    private BigDecimal accountPrice;

    /** 件数 */
    private Integer countable;

    /** 单价 */
    private BigDecimal price;

    /** 是否修改 （1：修改，0/空：未修改） */
    private Integer updatefh;

    /** 是否上传 (1：已上传) */
    private Integer upload;

    /** 记账 */
    private String checkout;

    /** 发货备注 */
    private String memo;

    /** 经手人 */
    private String manager;

    /** 车牌号 */
    private String carnumber;

    /** 是否已经推送(0：未推送；1：已推送；) */
    private Boolean isPush;

    /** 代发客户打印发货单抬头名称 */
    private Integer printheader;

    /** 无备注 */
    private String signstr;
    /**
    * 无参构造方法
    */
    @Tolerate
    public CpBillfh(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}