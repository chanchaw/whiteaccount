package com.xdf.whiteaccount.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : 发货调整收款表(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-25 16:52:50
    */
@Data
@Builder
public class CpReceivables implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 应收款表 主键 */
    private Integer id;

    /** 单据类型（收款/收款调整） */
    private String billtype;

    /** 客户 */
    private String client;

    /** 金额 */
    private BigDecimal money;

    /** 单据时间 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date billdate;

    /** 收款类型（转账/承兑/现金） */
    private String collectiontype;

    /** 备注 */
    private String billmemo;

    /** 是否上传云端 */
    private Integer upload;
    /**
    * 无参构造方法
    */
    @Tolerate
    public CpReceivables(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}