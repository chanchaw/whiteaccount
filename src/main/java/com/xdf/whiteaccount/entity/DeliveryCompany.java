package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 代发客户(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-29 12:30:54
    */
@Data
@Builder
public class DeliveryCompany implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 无备注 */
    private String companyName;

    /** 地址 */
    private String companyAddress;

    /** 无备注 */
    private String remarks;
    /**
    * 无参构造方法
    */
    @Tolerate
    public DeliveryCompany(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}