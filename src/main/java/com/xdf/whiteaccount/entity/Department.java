package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 部门(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-19 20:26:32
    */
@Data
@Builder
public class Department implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 上级元素 */
    private Integer parentId;

    /** 名称 */
    private String dName;

    /** 编号(不重复) */
    private String dCode;

    /** 状态(1:正常;0:停用) */
    private Integer dState;

    /** 默认 */
    private Boolean dDefault;
    /**
    * 无参构造方法
    */
    @Tolerate
    public Department(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}