package com.xdf.whiteaccount.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import java.math.BigDecimal;
import java.io.Serializable;

    /**
    * @Description : 色布入库单据(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-08-09 11:04:37
    */
@Data
@Builder
public class DyeFabricInbound implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Long id;

    /** 单据日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date orderDate;

    /** 单据编号 */
    private String orderCode;

    /** 染厂 */
    private String dyeCompany;

    /** 计划单号 */
    private String dyePlanCode;

    /** 加工类型 */
    private String processType;

    /** 外键,往来单位 */
    private String contactCompanyId;

    /** 毛净差 */
    private BigDecimal netDiff;

    /** 米克系数 */
    private BigDecimal rate;

    /** 是否直发(0:否;1:是;) */
    private Boolean isSendDirectly;

    /** 色布规格 */
    private String specification;

    /** 匹数 */
    private BigDecimal inputPairs;

    /** 毛重 */
    private BigDecimal inputKilo;

    /** 净重 */
    private BigDecimal netKilo;

    /** 米数 */
    private BigDecimal inputMeter;

    /** 经手人 */
    private String handlerName;

    /** 单价 */
    private BigDecimal price;

    /** 金额 */
    private BigDecimal money;

    /** 创建人编号 */
    private Integer createUserId;

    /** 上传标记 */
    private Integer upload;

    /** 是否推送(0:是;1:否;) */
    private Boolean isPush;

    /** 备注 */
    private String remarks;

    /** 创建日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /** 关联单据编号 */
    private String linkedOrderCode;

    /** 单据状态(0:作废;1:正常;) */
    private Integer state;

    /** 是否审核(0:否;1:是;) */
    private Boolean isAudit;

    /** 审核人编号 */
    private Integer auditUserId;

    /** 对账单价 */
    private BigDecimal accountPrice;

    /** 计价标准(1:毛重;2:净重;3:公式计米;4:实际计米;) */
    private Integer priceStandard;
    /**
    * 无参构造方法
    */
    @Tolerate
    public DyeFabricInbound(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}