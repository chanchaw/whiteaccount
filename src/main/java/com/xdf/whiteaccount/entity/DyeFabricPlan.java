package com.xdf.whiteaccount.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.math.BigDecimal;
import java.io.Serializable;
    /**
    * @Description : 色布计划(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-08-09 09:29:53
    */
@Data
@Builder
public class DyeFabricPlan implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Long id;

    /** 外键,往来单位 */
    private String contactCompanyId;

    /** 合同号 */
    private String contractCode;

    /** 计划单号 */
    private String planCode;

    /** 计划日期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date planDate;

    /** 订单号 */
    private String orderCode;

    /** 品名 */
    private String productName;

    /** 类型 */
    private String processType;

    /** 规格 */
    private String materialSpecification;

    /** 色号/花号 */
    private String colorNo;

    /** 颜色/花型 */
    private String color;

    /** 毛净差 */
    private BigDecimal netDiff;

    /** 米克系数 */
    private BigDecimal rate;

    /** 计价标准(1:毛重;2:净重;3:公式计米;4:实际计米;) */
    private Integer priceStandard;

    /** 计米 */
    private Integer meterCounter;

    /** 计划匹数 */
    private BigDecimal planPairs;

    /** 计划重量 */
    private BigDecimal planKilo;

    /** 投坯匹数 */
    private BigDecimal planUsePairs;

    /** 投坯公斤 */
    private BigDecimal planUseKilo;

    /** 价格 */
    private BigDecimal materialPrice;

    /** 跟单人员 */
    private String planEmployeeId;

    /** 备注 */
    private String remarks;

    /** 纸管 */
    private BigDecimal papertube;

    /** 袋重 */
    private BigDecimal bagweight;

    /** 空加 */
    private BigDecimal emptyaddition;

    /** 空加隐藏 */
    private BigDecimal emptyadditionhide;

    /** 交期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date submitDate;

    /** 完工状态(0:未完工;1:已完工;) */
    private Boolean markFinished;

    /** 外键,创建人 */
    private Integer createUserId;

    /** 创建日期，DB自动填充 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 上传字段(0:未上传;1:已上传;) */
    private Integer upload;

    /** 状态(0:作废;1:正常;) */
    private Integer state;

    /** 作废人.编号 */
    private Integer cancelUserId;

    /** 是否审核(0:否;1:是;) */
    private Boolean isAudit;

    /** 审核人编号 */
    private Integer auditUserId;
    /**
    * 无参构造方法
    */
    @Tolerate
    public DyeFabricPlan(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}