package com.xdf.whiteaccount.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import java.math.BigDecimal;
import java.io.Serializable;
    /**
    * @Description : 色布计划模板(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-24 10:06:23
    */
@Data
@Builder
public class DyeFabricPlanTemplate implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Long id;

    /** 外键,往来单位 */
    private String contactCompanyId;

    /** 合同号 */
    private String contractCode;

    /** 计划日期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date planDate;

    /** 订单号 */
    private String orderCode;

    /** 品名 */
    private String productName;

    /** 类型 */
    private String processType;

    /** 规格 */
    private String materialSpecification;

    /** 色号/花号 */
    private String colorNo;

    /** 颜色/花型 */
    private String color;

    /** 毛净差 */
    private BigDecimal netDiff;

    /** 米克系数 */
    private BigDecimal rate;

    /** 计划重量 */
    private BigDecimal planKilo;

    /** 价格 */
    private BigDecimal materialPrice;

    /** 跟单人员 */
    private String planEmployeeId;

    /** 备注 */
    private String remarks;

    /** 交期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date submitDate;

    /** 是否默认模板 */
    private Boolean setDefault;

    /** 外键,创建人 */
    private Integer createUserId;

    /** 数据库中没有的字段 */
    private String planCode;
    /**
    * 无参构造方法
    */
    @Tolerate
    public DyeFabricPlanTemplate(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}