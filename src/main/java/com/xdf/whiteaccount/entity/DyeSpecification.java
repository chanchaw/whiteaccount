package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 色布规格(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-26 08:36:35
    */
@Data
@Builder
public class DyeSpecification implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 规格名称 */
    private String specName;
    /**
    * 无参构造方法
    */
    @Tolerate
    public DyeSpecification(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}