package com.xdf.whiteaccount.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : 部门员工(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-07-15 08:51:40
    */
@Data
@Builder
public class Employee implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Integer id;

    /** 姓名 */
    private String empName;

    /** 性别(1:男;0:女;) */
    private Boolean empSex;

    /** 电话号码 */
    private String empPhoneNumber;

    /** 员工年龄 */
    private Integer empAge;

    /** 员工住址 */
    private String address;

    /** 备注 */
    private String remarks;

    /** 状态(0:禁用;1:在职;2:离职) */
    private Integer state;

    /** 创建时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /** 外键,创建人 */
    private Integer createUserId;

    /** 外键,对应department.id */
    private Integer departmentId;
    /**
    * 无参构造方法
    */
    @Tolerate
    public Employee(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}