package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 头份(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-26 17:52:25
    */
@Data
@Builder
public class FirstCopies implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 无备注 */
    private String firstCopies;

    /** 备注 */
    private String remarks;
    /**
    * 无参构造方法
    */
    @Tolerate
    public FirstCopies(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}