package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.sql.Timestamp;
import java.io.Serializable;
    /**
    * @Description : (该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-16 09:40:12
    */
@Data
@Builder
public class FlywaySchemaHistory implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 无备注 */
    private Integer installedRank;

    /** 无备注 */
    private String version;

    /** 无备注 */
    private String description;

    /** 无备注 */
    private String type;

    /** 无备注 */
    private String script;

    /** 无备注 */
    private Integer checksum;

    /** 无备注 */
    private String installedBy;

    /** 无备注 */
    private Timestamp installedOn;

    /** 无备注 */
    private Integer executionTime;

    /** 无备注 */
    private Boolean success;
    /**
    * 无参构造方法
    */
    @Tolerate
    public FlywaySchemaHistory(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}