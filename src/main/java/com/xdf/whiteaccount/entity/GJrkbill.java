package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : (该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-22 08:16:02
    */
@Data
@Builder
public class GJrkbill implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 无备注 */
    private Integer bItemid;

    /** 无备注 */
    private Long bId;

    /** 无备注 */
    private BigDecimal bGj;

    /** 无备注 */
    private String bAorb;

    /** 无备注 */
    private String bJth;

    /** 无备注 */
    private String bClass;

    /** 无备注 */
    private Date bDate;

    /** 无备注 */
    private String bCn;

    /** 无备注 */
    private String bIp;

    /** 无备注 */
    private String bBcfc;

    /** 无备注 */
    private Integer bPh;

    /** 无备注 */
    private String bEdp;

    /** 无备注 */
    private Date bDtrk;

    /** 无备注 */
    private Integer bFpdid;

    /** 无备注 */
    private Integer bUpload;

    /** 无备注 */
    private String bBarcode;
    /**
    * 无参构造方法
    */
    @Tolerate
    public GJrkbill(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}