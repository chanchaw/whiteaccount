package com.xdf.whiteaccount.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.math.BigDecimal;
import java.io.Serializable;
    /**
    * @Description : 打卷调整表(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-26 09:56:33
    */
@Data
@Builder
public class JrkbillAdjust implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 外键,对应plan.id */
    private Long planId;

    /** 日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date jrkbillDate;

    /** 匹数 */
    private Integer pairs   ;

    /** 公斤 */
    private BigDecimal qty;

    /** 调整类型 */
    private String billType;

    /** 创建时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /** 创建人编号 */
    private Integer createUserId;

    /** 备注 */
    private String remarks;

    /** 修改人编号 */
    private Integer modifyUserId;

    /** 修改时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date modifyTime;

    /** 作废人编号 */
    private Integer cancelUserId;

    /** 作废时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date cancelTime;

    /** 状态(0:作废;1:正常;) */
    private Integer state;

    /** 1：已上传，0：待上传 */
    private Integer upload;

    /** 客户名称 */
    private String clientName;

    /** 收货单位 */
    private String address;

    /** 单价 */
    private BigDecimal price;

    /** 是否已经推送(0：未推送；1：已推送；) */
    private Boolean isPush;

    private String opbatch;
    /**
    * 无参构造方法
    */
    @Tolerate
    public JrkbillAdjust(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}