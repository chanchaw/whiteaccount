package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.math.BigDecimal;
import java.io.Serializable;
    /**
    * @Description : (该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-22 08:16:02
    */
@Data
@Builder
public class Jrkbillfh implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 无备注 */
    private Integer id;

    /** 发货时每匹公斤数 */
    private BigDecimal qty;

    /** AB面 */
    private String aorb;

    /** 发货序号 */
    private Integer isorder;

    /** 计算机名 */
    private String computername;

    /** 标记是否发货 1表示发货 */
    private Integer isfh;

    /** 标记当前行数据是否删除 1表示已经删除 */
    private Integer isdelete;

    /** 对应订单的主键(即计划主键) */
    private Long planid;

    /** 生成发货单对应发货明细表主键 */
    private Integer fid;
    /**
    * 无参构造方法
    */
    @Tolerate
    public Jrkbillfh(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}