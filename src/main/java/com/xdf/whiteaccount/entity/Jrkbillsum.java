package com.xdf.whiteaccount.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : (该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-02 19:49:07
    */
@Data
@Builder
public class Jrkbillsum implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 无备注 */
    private Integer id;

    /** 计划主键 */
    private Integer planid;

    /** 匹数 */
    private Integer ps;

    /** 公斤 */
    private BigDecimal qty;

    /** AB面 */
    private String aorb;

    /** 班组 */
    private String className;

    /** 机台号 */
    private String jth;

    /** 计算机名 */
    private String cn;

    /** 上传日期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createdate;

    private Integer imported;

    /** =product.outward_order.id */
    private Long outwardid;

    /**
    * 无参构造方法
    */
    @Tolerate
    public Jrkbillsum(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}