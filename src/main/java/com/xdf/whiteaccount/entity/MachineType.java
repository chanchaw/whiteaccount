package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 机台号(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-06 16:55:18
    */
@Data
@Builder
public class MachineType implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Integer id;

    /** 机台名称 */
    private String machineName;

    /** 排序字段 */
    private Integer serialNum;

    /** 加工户名称 */
    private String processCompname;

    /**
    * 无参构造方法
    */
    @Tolerate
    public MachineType(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}