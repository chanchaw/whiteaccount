package com.xdf.whiteaccount.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.math.BigDecimal;
import java.io.Serializable;
    /**
    * @Description : 原料应付款(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-07-01 16:40:45
    */
@Data
@Builder
public class MaterialAccountPayable implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 单据日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date billDate;

    /** 单据类型 */
    private String billtype;

    /** 客户名称 */
    private String clientName;

    /** 单价 */
    private BigDecimal price;

    /** 金额 */
    private BigDecimal money;

    /** 类型(预付) */
    private String payableType;

    /** 件数 */
    private Integer boxAmount;

    /** 备注 */
    private String billmemo;

    /** 是否已经上传(0:未上传;1:已上传;) */
    private Integer upload;

    /** 是否推送字段(0:未推送;1:已推送;) */
    private Boolean isPush;
    /**
    * 无参构造方法
    */
    @Tolerate
    public MaterialAccountPayable(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}