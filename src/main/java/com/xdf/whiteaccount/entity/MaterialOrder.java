package com.xdf.whiteaccount.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.math.BigDecimal;
import java.io.Serializable;
    /**
    * @Description : 原料入库(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-07-22 10:33:01
    */
@Data
@Builder
public class MaterialOrder implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Long id;

    /** 制单日期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @NotNull(message = "单据日期必须输入！")
    private Date orderDate;

    /** 单据编号 */
    private String orderCode;

    /** 单据类型 */
    private String billType;

    /** 付款类型(现金,欠账) */
    private String accountType;

    /** 供应商 */
    private String supplierId;

    /** 批次号 */
    private String batchNum;

    /** 原料规格 */
    private String materialSpec;

    /** 箱数 */
    private BigDecimal boxAmount;

    /** 入库数量 */
    private BigDecimal inputAmount;

    /** 入库重量 */
    private BigDecimal inputKilo;

    /** 单价 */
    private BigDecimal price;

    /** 金额 */
    private BigDecimal inputMoney;

    /** 备注 */
    private String remarks;

    /** 加工户 */
    private String processCompany;

    /** 经手人 */
    private String handlerName;

    /** 外键,对应sys_user.id */
    private Integer createUserId;

    /** 创建时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /** 状态(0:作废;1:正常;) */
    private Integer state;

    /** 上传下载同步数据标记（1已同步，0或者空未同步) */
    private Integer upload;

    /** 是否直发(0:否;1:是;) */
    private Boolean isSendDirectly;

    /** 关联单据编号 */
    private String linkedOrderCode;

    /** 是否审核(0:否;1:是;) */
    private Boolean isAudit;

    /** 审核人编号 */
    private Integer auditUserId;
    /**
    * 无参构造方法
    */
    @Tolerate
    public MaterialOrder(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}