package com.xdf.whiteaccount.entity;

import java.io.Serializable;

public class MeterLength implements Serializable {
    private Integer id;

    private String meter_length;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMeter_length() {
        return meter_length;
    }

    public void setMeter_length(String meter_length) {
        this.meter_length = meter_length == null ? null : meter_length.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", meter_length=").append(meter_length);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}