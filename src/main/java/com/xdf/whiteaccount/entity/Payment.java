package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : 付款(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-31 14:14:23
    */
@Data
@Builder
public class Payment implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 应付款表 主键 */
    private Integer id;

    /** 单据类型（付款/付款调整） */
    private String billtype;

    /** 供应商 */
    private String supplier;

    /** 金额 */
    private BigDecimal money;

    /** 单据时间 */
    private Date billdate;

    /** 付款类型（转账/承兑/现金） */
    private String collectiontype;

    /** 备注 */
    private String billmemo;

    /** 是否上传云端 */
    private Integer upload;
    /**
    * 无参构造方法
    */
    @Tolerate
    public Payment(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}