package com.xdf.whiteaccount.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.math.BigDecimal;
import java.io.Serializable;
    /**
    * @Description : 计划(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-07-22 10:33:01
    */
@Data
@Builder
public class Plan implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Long id;

    /** 外键,往来单位 */
    private String contactCompanyId;

    /** 合同号 */
    private String contractCode;

    /** 计划单号 */
    private String planCode;

    /** 计划日期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @NotNull(message = "单据日期不能为空！")
    private Date planDate;

    /** 通知日期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date noticeDate;

    /** 订单号 */
    private String orderCode;

    /** 品名 */
    @NotNull(message = "品名不能为空！")
    private String productName;

    /** 原料规格 */
    private String materialSpecification;

    /** 原料批号 */
    private String materialLotNo;

    /** 原料供应商 */
    private String materialSupplierId;

    /** 白坯克重 */
    private String greyfabricWeight;

    /** 白坯门幅 */
    private String greyfabricWidth;

    /** 头份(规格) */
    private String greyfabricSpecification;

    /** 坯布价 */
    private BigDecimal greyfabricPrice;

    /** 毛高 */
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private String mHeight;

    /** 米长 */
    private String meterLength;

    /** 计划重量 */
    private BigDecimal planKilo;

    /** 白坯布日期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date greyfabricDate;

    /** 大货交期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date submitDate;

    /** 原料价格 */
    private BigDecimal materialPrice;

    /** 加工费 */
    private BigDecimal processingCost;

    /** 中丝规格 */
    private String middleSpecification;

    /** 外键,中丝供应商 */
    private String middleSupplierId;

    /** 中丝头份 */
    private String middleTf;

    /** 底丝规格 */
    private String bottomSpecification;

    /** 底丝供应商 */
    private String bottomSupplierId;

    /** 底丝头份 */
    private String bottomTf;

    /** 机型 */
    private String machineType;

    /** 针数 */
    private BigDecimal needleAmount;

    /** 下机克重 */
    private String lowWeight;

    /** 下机门幅 */
    private String lowWidth;

    /** 计划人员 */
    private String planEmployeeId;

    /** 生产人员 */
    private String productEmployeeId;

    /** 复核人员 */
    private String checkEmployeeId;

    /** 喷码厂名 */
    private String markCompanyName;

    /** 坯布要求 */
    private String fabricRequirement;

    /** 备注 */
    private String remarks;

    /** 外键,创建人 */
    private Integer createUserId;

    /** 作废人.编号 */
    private Integer cancelUserId;

    /** 创建日期，DB自动填充 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /** 是否完工字段 */
    private Boolean markFinished;

    /** 状态(0:作废;1:正常;) */
    private Integer state;

    /** 上传字段(0:未上传;1:已上传;) */
    private Integer upload;

    /** 是否喷码(0:否;1:是;) */
    private Boolean isMarking;

    /** 是否审核(0:否;1:是;) */
    private Boolean isAudit;

    /** 审核人编号 */
    private Integer auditUserId;

    /** 是否显示派单 */
    private Boolean isShowOrder;
    /**
    * 无参构造方法
    */
    @Tolerate
    public Plan(){}

        public String getmHeight() {
            return mHeight;
        }

        public void setmHeight(String mHeight) {
            this.mHeight = mHeight;
        }

        @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}