package com.xdf.whiteaccount.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : 白坯计划与批次号的对应关系(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-07-19 09:24:54
    */
@Data
@Builder
public class PlanBatchnum implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Long id;

    /** 计划单号 */
    private String planCode;

    /** 机台号 */
    @NotEmpty(message = "批次号不能为空！")
    private String batchNum;

    /** 排序字段 */
    private Integer serialNum;

    /** 白坯布日期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date greyfabricDate;

    /** 大货交期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date submitDate;

    /**  制单日期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @NotNull(message = "单据日期不能为空！")
    private Date planDate;

    /** 备注 */
    private String remarks;

    /** 创建时间 */
    private Date createTime;
    /**
    * 无参构造方法
    */
    @Tolerate
    public PlanBatchnum(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}