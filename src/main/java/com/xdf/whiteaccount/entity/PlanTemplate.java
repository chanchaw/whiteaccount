package com.xdf.whiteaccount.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : 计划单模板(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-04 13:11:52
    */
@Data
@Builder
@Repository
public class PlanTemplate implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Long id;

    /** 外键,往来单位 */
    private String contactCompanyId;

    /** 合同号 */
    private String contractCode;

    /** 计划单号 */
    private String planCode;

    /** 订单号 */
    private String orderCode;

    /** 品名 */
    private String productName;

    /** 原料规格 */
    private String materialSpecification;

    /** 原料批号 */
    private String materialLotNo;

    /** 原料供应商 */
    private String materialSupplierId;

    /** 白坯克重 */
    private String greyfabricWeight;

    /** 白坯门幅 */
    private String greyfabricWidth;

    /** 头份(规格) */
    private String greyfabricSpecification;

    /** 坯布价 */
    private BigDecimal greyfabricPrice;

    /** 毛高 */
    private String mHeight;

    /** 计划重量 */
    private BigDecimal planKilo;

    /** 白坯布日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date greyfabricDate;

    /** 大货交期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date submitDate;

    /** 原料价格 */
    private BigDecimal materialPrice;

    /** 加工费 */
    private BigDecimal processingCost;

    /** 中丝规格 */
    private String middleSpecification;

    /** 外键,中丝供应商 */
    private String middleSupplierId;

    /** 底丝规格 */
    private String bottomSpecification;

    /** 底丝供应商 */
    private String bottomSupplierId;

    /** 机型 */
    private String machineType;

    /** 针数 */
    private BigDecimal needleAmount;

    /** 下机克重 */
    private String lowWeight;

    /** 下机门幅 */
    private String lowWidth;

    /** 计划人员 */
    private String planEmployeeId;

    /** 生产人员 */
    private String productEmployeeId;

    /** 复核人员 */
    private String checkEmployeeId;

    /** 喷码厂名 */
    private String markCompanyName;

    /** 坯布要求 */
    private String fabricRequirement;

    /** 是否设置为默认模板 */
    private Boolean setDefault;

    /** 备注 */
    private String remarks;
    /**
    * 无参构造方法
    */
    @Tolerate
    public PlanTemplate(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}