package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 加工户(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-07-01 16:05:10
    */
@Data
@Builder
public class ProcessCompany implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 加工户名称 */
    private String companyName;

    /** 是否默认选中(0:默认不选;1:默认选中;) */
    private Boolean isSelected;
    /**
    * 无参构造方法
    */
    @Tolerate
    public ProcessCompany(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}