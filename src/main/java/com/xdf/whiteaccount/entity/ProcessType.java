package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 加工类型(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-23 16:01:16
    */
@Data
@Builder
public class ProcessType implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 加工类型名称 */
    private String typeName;
    /**
    * 无参构造方法
    */
    @Tolerate
    public ProcessType(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}