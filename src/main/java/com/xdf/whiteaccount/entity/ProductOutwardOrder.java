package com.xdf.whiteaccount.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.math.BigDecimal;
import java.io.Serializable;
    /**
    * @Description : 外发入库单据(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-07-23 14:44:34
    */
@Data
@Builder
public class ProductOutwardOrder implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Long id;

    /** 单据日期 */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date orderDate;

    /** 单据编号 */
    private String orderCode;

    /** 加工户 */
    private String processCompany;

    /** 批次号 */
    private String batchNum;

    /** 客户 */
    private String clientName;

    /** 供应商 */
    private String supplierName;

    /** 外键,对应白坯计划计划单号 */
    private String planCode;

    /** 原料规格 */
    private String materialSpecification;

    /** 坯布规格 */
    private String greyfabricSpecification;

    /** 件数 */
    private BigDecimal boxAmount;

    /** 匹数 */
    private BigDecimal inputPairs;

    /** 重量 */
    private BigDecimal inputKilo;

    /** 加工费 */
    private BigDecimal processingCost;

    /** 金额 */
    private BigDecimal money;

    /** 经手人 */
    private String handlerName;

    /** 外键,制单人,对应sys_user.id */
    private Integer createUserId;

    /** 备注 */
    private String remarks;

    /** 创建时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /** 是否成品(0:半成品;1:卷布;) */
    private Boolean isFinishedProduct;

    /** AB面 */
    private String abSurface;

    /** 收货单位 */
    private String consignee;

    /** 状态(0:作废;1:正常;) */
    private Integer state;

    /** 是否直发(0:否;1:是;) */
    private Boolean isSendDirectly;

    /** 对应的单据编号 */
    private String linkedOrderCode;

    /** 是否审核(0:否;1:是;) */
    private Boolean isAudit;

    /** 审核人编号 */
    private Integer auditUserId;

    /** 仓库编号 */
    private Integer storageId;

    /** = plan.id */
    private Long planid;

    /** 手动输入、自动导入 */
    private String mode;

    /** = jrkbillsum.id */
    private Integer jrkid;

    /**
    * 无参构造方法
    */
    @Tolerate
    public ProductOutwardOrder(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}