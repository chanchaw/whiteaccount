package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.util.Date;
import java.io.Serializable;
    /**
    * @Description : 二维码登录日志表(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-03 19:48:25
    */
@Data
@Builder
public class Qrlogin implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 无备注 */
    private Integer iid;

    /** 登录标识 */
    private String guid;

    /** 登录类型,平板、浏览器 */
    private String type;

    /** 微信的openid */
    private String openid;

    /** 备注 */
    private String remark;

    /** 登录标识,默认0为登录失败，1为成功 */
    private Boolean success;

    /** 登录账号 */
    private String loginName;

    /** 登录系统的用户名 */
    private String userName;

    /** 创建时间,DB自动填充 */
    private Date createTime;
    /**
    * 无参构造方法
    */
    @Tolerate
    public Qrlogin(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}