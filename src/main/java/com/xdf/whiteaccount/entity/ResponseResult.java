package com.xdf.whiteaccount.entity;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Objects;

/**
* @description: 自定义响应体
* @author: 张柯
* @create: 2021-05-16 09:40:12
**/
@Data
@Builder
public class ResponseResult<T> {
    private boolean success;
    private String responseCode;
    private T data;
    private String message;

    @Tolerate
    public ResponseResult() {
    }
}