package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 仓库(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-07-23 09:48:38
    */
@Data
@Builder
public class Storage implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Integer id;

    /** 仓库名称 */
    private String storageName;
    /**
    * 无参构造方法
    */
    @Tolerate
    public Storage(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}