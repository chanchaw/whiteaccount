package com.xdf.whiteaccount.entity;

import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.math.BigDecimal;

public class SysConfigDecimal implements Serializable {

    /**
     * 实现了序列化接口自动生成的序列号
     */
    private static final long serialVersionUID = 1L;

    /** 属性名 */
    private String config_field;

    /** 属性默认值 */
    private BigDecimal config_default;

    /** 属性最小值 */
    private BigDecimal config_smallvalue;

    /** 属性最大值 */
    private BigDecimal config_bigvalue;

    /** 属性描述 */
    private String config_desc;

    /**
     * 无参构造方法
     */
    @Tolerate
    public SysConfigDecimal(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getConfig_field() {
        return config_field;
    }

    public void setConfig_field(String config_field) {
        this.config_field = config_field == null ? null : config_field.trim();
    }

    public BigDecimal getConfig_default() {
        return config_default;
    }

    public void setConfig_default(BigDecimal config_default) {
        this.config_default = config_default;
    }

    public String getConfig_desc() {
        return config_desc;
    }

    public void setConfig_desc(String config_desc) {
        this.config_desc = config_desc == null ? null : config_desc.trim();
    }

    public BigDecimal getConfig_smallvalue() {
        return config_smallvalue;
    }

    public BigDecimal getConfig_bigvalue() {
        return config_bigvalue;
    }

    public void setConfig_smallvalue(BigDecimal config_smallvalue) {
        this.config_smallvalue = config_smallvalue;
    }

    public void setConfig_bigvalue(BigDecimal config_bigvalue) {
        this.config_bigvalue = config_bigvalue;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", config_field=").append(config_field);
        sb.append(", config_default=").append(config_default);
        sb.append(", config_smallvalue=").append(config_smallvalue);
        sb.append(", config_bigvalue=").append(config_bigvalue);
        sb.append(", config_desc=").append(config_desc);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}