package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 配置表(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-16 09:40:12
    */
@Data
@Builder
public class SysConfigInt implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 属性名 */
    private String configField;

    /** 属性值 */
    private Integer configValue;

    /** 属性描述 */
    private String configDesc;
    /**
    * 无参构造方法
    */
    @Tolerate
    public SysConfigInt(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}