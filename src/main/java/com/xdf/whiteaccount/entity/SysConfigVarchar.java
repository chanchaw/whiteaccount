package com.xdf.whiteaccount.entity;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
    /**
    * @Description : 字符配置(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-26 14:22:09
    */
@Data
@Builder
public class SysConfigVarchar implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @NotEmpty(message = "配置项不能为空！")
    private String configField;

    /** 配置值 */
    private String configValue;

    /** 说明 */
    private String configDesc;
    /**
    * 无参构造方法
    */
    @Tolerate
    public SysConfigVarchar(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}