package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 菜单表(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-07-05 17:32:19
    */
@Data
@Builder
public class SysMenu implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 父节点 */
    private Integer parentId;

    /** 菜单名 */
    private String menuName;

    /** 菜单相对路径 */
    private String menuPath;

    /** 菜单图标 */
    private String menuIcon;

    /** 排序字段 */
    private Integer serialNo;
    /**
    * 无参构造方法
    */
    @Tolerate
    public SysMenu(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}