package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 密码(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-06-21 15:19:40
    */
@Data
@Builder
public class SysPassword implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 密码类型 */
    private String sysField;

    /** 密码 */
    private String sysPassword;
    /**
    * 无参构造方法
    */
    @Tolerate
    public SysPassword(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}