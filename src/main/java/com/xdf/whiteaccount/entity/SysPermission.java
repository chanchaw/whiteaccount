package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 权限表(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-16 09:40:12
    */
@Data
@Builder
public class SysPermission implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 外键,菜单编号 */
    private Integer sysMenuId;

    /** 角色编号 */
    private Integer sysRoleId;

    /** 显示权限 */
    private Integer sysShow;

    /** 新增权限 */
    private Integer sysAdd;

    /** 修改权限 */
    private Integer sysModify;

    /** 删除权限 */
    private Integer sysDelete;

    /** 审核权限 */
    private Integer sysAudit;

    /** 打印权限 */
    private Integer sysPrint;

    /** 导出权限 */
    private Integer sysExport;

    /** 单价权限 */
    private Integer sysPrice;

    /** 金额权限 */
    private Integer sysMoney;
    /**
    * 无参构造方法
    */
    @Tolerate
    public SysPermission(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}