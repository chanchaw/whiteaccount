package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 角色表(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-16 09:40:12
    */
@Data
@Builder
public class SysRole implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 角色名称 */
    private String roleName;

    /** 是否管理员(1:是;0:否;) */
    private Boolean roleAdminSign;

    /** 角色状态(1:正常;0:停用) */
    private Integer roleState;
    /**
    * 无参构造方法
    */
    @Tolerate
    public SysRole(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}