package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 角色组(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-07-26 12:23:28
    */
@Data
@Builder
public class SysRoleGroup implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 角色组名称 */
    private String groupName;

    /** 是否管理员(1:是;0:否;) */
    private Boolean roleGroupAdminSign;

    /** 是否默认选中 */
    private Boolean isInitialSelect;
    /**
    * 无参构造方法
    */
    @Tolerate
    public SysRoleGroup(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}