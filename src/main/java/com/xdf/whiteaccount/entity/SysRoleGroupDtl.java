package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 角色组与角色的对应关系表(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-16 09:40:12
    */
@Data
@Builder
public class SysRoleGroupDtl implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 角色编号 */
    private Integer roleId;

    /** 角色组编号 */
    private Integer roleGroupId;
    /**
    * 无参构造方法
    */
    @Tolerate
    public SysRoleGroupDtl(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}