package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 用户与角色组对应关系表(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-16 09:40:12
    */
@Data
@Builder
public class SysUserRoleGroup implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 用户表主键 */
    private Integer sysUserId;

    /** 角色组主键 */
    private Integer sysRoleGroupId;
    /**
    * 无参构造方法
    */
    @Tolerate
    public SysUserRoleGroup(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}