package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 用户报表列参数配置(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-16 09:40:12
    */
@Data
@Builder
public class SysUserTableConfig implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 菜单对应编号 */
    private Integer sysMenuId;

    /** 用户编号 */
    private Integer sysUserId;

    /** 网格列名称 */
    private String fieldName;

    /** 列宽 */
    private Double fieldWidth;

    /** 列对齐方式 */
    private String fieldAlign;

    /** 列隐藏 */
    private Boolean fieldHidden;

    /** 列排序字段 */
    private Integer fieldSerialNum;

    /** 列字段类别(string,number,date) */
    private String fieldType;

    /** 列初始值 */
    private String fieldDefaultValue;
    /**
    * 无参构造方法
    */
    @Tolerate
    public SysUserTableConfig(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}