package com.xdf.whiteaccount.entity;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import java.io.Serializable;
    /**
    * @Description : 收货地址(该类由代码生成器自动生成)
    * @Author : 张柯
    * @Date : 2021-05-22 14:01:02
    */
@Data
@Builder
public class TakeDeliveryAddress implements Serializable,Cloneable {

    /**
    * 实现了序列化接口自动生成的序列号
    */
    private static final long serialVersionUID = 1L;

    /** 自增主键 */
    private Integer id;

    /** 收货单位 */
    private String company;

    /** 详细地址 */
    private String address;

    /** 电话号码 */
    private String phone;
    /**
    * 无参构造方法
    */
    @Tolerate
    public TakeDeliveryAddress(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}