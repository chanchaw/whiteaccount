package com.xdf.whiteaccount.entity;

import lombok.Data;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-26 14:15
 **/
@Data
public class WechatBody {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private Integer accessMode;
    private Long id;
}
