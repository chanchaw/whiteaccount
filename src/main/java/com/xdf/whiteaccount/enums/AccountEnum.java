package com.xdf.whiteaccount.enums;

/**
 * @program: white-account
 * @description: 结账类型
 * @author: 张柯
 * @create: 2021-05-31 13:25
 **/
public enum AccountEnum {
    DEBT("欠账"), CASH("现金"), PAYMENT_ADVANCE("预付");
    private String value;

    public String getValue() {
        return value;
    }

    AccountEnum(String value) {
        this.value = value;
    }

    /**
     * 根据值匹配
     *
     * @param value
     * @return
     */
    public static AccountEnum match(String value) {
        if (value == null || value.trim().length() <= 0) return null;
        for (AccountEnum e : values()) {
            if (e.getValue().equals(value)) return e;
        }
        return null;
    }
}
