package com.xdf.whiteaccount.enums;

/**
 * @program: white-account
 * @description: 参数常量
 * @author: 张柯
 * @create: 2021-06-08 13:23
 **/
public class ArgumentConstant {
    public static final String ALL_CLIENT = "全部";
    public static final String DEFAULT_SUMMARIZE = "0";
    public static final String CLIENT_FIELD = "client";
}
