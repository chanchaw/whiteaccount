package com.xdf.whiteaccount.enums;

/**
 * @program: white-account
 * @description: 单据类型
 * @author: 张柯
 * @create: 2021-07-23 13:30
 **/
public enum BillType {
    BPCK("BPCK"), BPLLTH("BPLLTH"), BPPD("BPPD"), BPTH("BPTH");
    private String billType;

    public String getBillType() {
        return billType;
    }

    BillType(String billType) {
        this.billType = billType;
    }
}
