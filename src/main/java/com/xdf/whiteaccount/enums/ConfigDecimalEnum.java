package com.xdf.whiteaccount.enums;

/**
 * @program: white-account
 * @description:
 * @author: szx
 * @create 2021年10月26日10:27:40
 **/
public enum ConfigDecimalEnum {
    PLAN_FINISHED_CHECK("按配置说明添加数据"),
    AUTO_AUDIT_AT("按配置说明修改数据");

    private String field;

    public String getField() {
        return field;
    }

    ConfigDecimalEnum(String field) {
        this.field = field;
    }

    public static ConfigDecimalEnum match(String name) {
        if (name == null || name.length() <= 0) return null;
        for (ConfigDecimalEnum e : values()) {
            if (e.getField().equals(name)) return e;
        }
        return null;
    }
}
