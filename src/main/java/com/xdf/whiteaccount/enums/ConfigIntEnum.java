package com.xdf.whiteaccount.enums;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-07-09 10:49
 **/
public enum ConfigIntEnum {
    PLAN_FINISHED_CHECK("计划单完工按钮校验是否开启"),
    AUTO_AUDIT_AT("每日自动审核时间"),
    ALLOW_MODIFY_WHOLE_PRICE("应收账款是否允许修改本订单的所有单价"),
    PLAN_REPORT_CONFIG("白坯计划排序规则"),
    PLAN_SAVE_VALIDATE("禁止保存计划单上面多个要素完全相同的单据"),
    REPORT_DATE_RANGE("报表起始日期距离当前天数");

    private String field;

    public String getField() {
        return field;
    }

    ConfigIntEnum(String field) {
        this.field = field;
    }

    public static ConfigIntEnum match(String name) {
        if (name == null || name.length() <= 0) return null;
        for (ConfigIntEnum e : values()) {
            if (e.getField().equals(name)) return e;
        }
        return null;
    }
}
