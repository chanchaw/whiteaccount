package com.xdf.whiteaccount.enums;

/**
 * @program: xzy-management-system
 * @description: 配置
 * @author: 张柯
 * @create: 2021-01-16 12:42
 **/
public enum ConfigVarEnum {
    CLIENT_TITLE("主页-标题"),
    PLAN_REPORT_TITLE("结算通知单标题"),
    CLIENT_NAME("公司名称"),
    CLIENT_NICK_NAME("公司简称"),
    CLIENT_ADDRESS("公司地址");

    private String field;

    public String getField() {
        return field;
    }

    ConfigVarEnum(String field) {
        this.field = field;
    }

    public static ConfigVarEnum get(String name) {
        if (name == null || name.length() <= 0) return null;
        for (ConfigVarEnum e : values()) {
            if (e.getField().equals(name)) return e;
        }
        return null;
    }
}