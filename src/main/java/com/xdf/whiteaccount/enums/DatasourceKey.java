package com.xdf.whiteaccount.enums;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-11 10:54
 **/
public class DatasourceKey {
    public static final String PRIMARY_KEY = "primary";
    public static final String CURRENT_DATASOURCE_KEY = "CURRENT_DATASOURCE_KEY";
}
