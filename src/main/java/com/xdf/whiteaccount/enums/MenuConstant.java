package com.xdf.whiteaccount.enums;

/**
 * @program: white-account
 * @description: 菜单
 * @author: 张柯
 * @create: 2021-05-17 09:43
 **/
public class MenuConstant {
    //  计划管理
    public static final String MENU100 = "100";
    //  白坯计划
    public static final String MENU101 = "101";
    //  色布计划
    public static final String MENU102 = "102";
    //  发货管理
    public static final String MENU200 = "200";
    //  白坯发货
    public static final String MENU201 = "201";
    //  白坯产量
    public static final String MENU202 = "202";
    //  收货管理
    public static final String MENU300 = "300";
    //  原料序时表
    public static final String MENU301 = "301";
    //  应收应付
    public static final String MENU302 = "302";
    //  基础资料
    public static final String MENU400 = "400";
    //  基础资料-客户
    public static final String MENU401 = "401";
    //  基础资料-供应商
    public static final String MENU402 = "402";
    //  基础资料-品名
    public static final String MENU403 = "403";
    //  基础资料-原料规格
    public static final String MENU404 = "404";
    //  基础资料-白坯克重
    public static final String MENU405 = "405";
    //  基础资料-白坯门幅
    public static final String MENU406 = "406";
    //  基础资料-机台号
    public static final String MENU407 = "407";
    //  基础资料-部门员工
    public static final String MENU408 = "408";
    //  基础资料-计划模板
    public static final String MENU409 = "409";
    //  基础资料-收货单位
    public static final String MENU410 = "410";
    //  基础资料-单据类型
    public static final String MENU411 = "411";
    //  基础资料-头份
    public static final String MENU412 = "412";
    //  基础资料-加工类型
    public static final String MENU413 = "413";
    //  基础资料-色布计划模板
    public static final String MENU414 = "414";
    //  基础资料-色布品名
    public static final String MENU415 = "415";
    //  基础资料-色布规格
    public static final String MENU416 = "416";
    //  基础资料-代发客户
    public static final String  MENU417 = "417";
    //  基础资料-加工户
    public static final String  MENU418 = "418";
    //  基础资料-仓库
    public static final String  MENU419 = "419";
    //  基础资料-米长
    public static final String  MENU420 = "420";
    //  系统管理
    public static final String MENU500 = "500";
    //  权限表
    public static final String MENU501 = "501";
    //  用户
    public static final String MENU502 = "502";
    //  角色组
    public static final String MENU503 = "503";
    //  配置
    public static final String MENU504 = "504";
    //  重置账套
    public static final String MENU505 = "505";
    //  外发管理-原料外发
    public static final String MENU601 = "601";
    //  应收应付-应收账款
    public static final String MENU701 = "701";
    //  应收应付-应付账款
    public static final String MENU702 = "702";
    //  应收应付-外发对账
    public static final String MENU703 = "703";
    //  成品管理
    public static final String MENU801 = "801";
}
