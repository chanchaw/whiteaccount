package com.xdf.whiteaccount.enums;

/**
 * @program: white-account
 * @description: 调整枚举
 * @author: 张柯
 * @create: 2021-06-06 10:21
 **/
public enum ModifyTypeEnum {
    IN("手动入库"), OUT("手动出库"), CHECKOUT("盘点");
    private String name;

    ModifyTypeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static ModifyTypeEnum match(String name) {
        if (name == null || name.length() <= 0) return null;
        for (ModifyTypeEnum m : values()) {
            if (m.getName().equals(name)) return m;
        }
        return null;
    }
}
