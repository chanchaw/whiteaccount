package com.xdf.whiteaccount.enums;

/**
 * @program: xzy-management-system
 * @description: 操作
 * @author: 张柯
 * @create: 2021-04-27 10:20
 **/
public enum Operate {
    ADD("新增"), MODIFY("修改"), DELETE("删除");
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    Operate(String name) {
        this.name = name;
    }
}
