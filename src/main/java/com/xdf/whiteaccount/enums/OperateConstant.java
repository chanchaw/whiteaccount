package com.xdf.whiteaccount.enums;

/**
 * @program: xzy-management-system
 * @description: 操作名称
 * @author: 张柯
 * @create: 2021-01-22 10:28
 **/
public class OperateConstant {
    public static final String SHOW = "SHOW";
    public static final String ADD = "ADD";
    public static final String MODIFY = "MODIFY";
    public static final String DELETE = "DELETE";
    public static final String AUDIT = "AUDIT";
    public static final String PRINT = "PRINT";
    public static final String EXPORT = "EXPORT";
    public static final String PRICE = "PRICE";
    public static final String MONEY = "MONEY";
}
