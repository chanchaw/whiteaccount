package com.xdf.whiteaccount.enums;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-21 15:21
 **/
public enum PasswordFieldEnum {
    PASSWORD_FIELD("账套重置密码");
    private String field;

    public String getField() {
        return field;
    }

    PasswordFieldEnum(String field) {
        this.field = field;
    }
}
