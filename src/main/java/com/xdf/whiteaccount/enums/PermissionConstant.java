package com.xdf.whiteaccount.enums;

/**
 * @program: xzy-management-system
 * @description: 权限
 * @author: 张柯
 * @create: 2021-01-21 08:34
 **/
public enum PermissionConstant {
    SHOW, ADD, MODIFY, DELETE, AUDIT, PRINT, EXPORT, PRICE, MONEY;
}
