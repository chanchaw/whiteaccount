package com.xdf.whiteaccount.enums;

/**
 * @program: white-account
 * @description: 单据编号前缀
 * @author: 张柯
 * @create: 2021-05-19 17:05
 **/
public enum PrefixEnum {
    MCP, MTP, YL, LL, JB, BC, F, CPRK, SS
}
