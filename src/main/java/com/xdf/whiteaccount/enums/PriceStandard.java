package com.xdf.whiteaccount.enums;

/**
 * @program: white-account
 * @description: 计价标准
 * @author: 张柯
 * @create: 2021-08-09 09:31
 **/
public enum PriceStandard {
    KILO("毛重", 1),
    NET_KILO("净重", 2),
    EXPRESS_METER("公式计米", 3),
    REAL_METER("实际计米", 4),
    ;
    private String name;
    private Integer value;

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }

    PriceStandard(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    /**
     * 匹配
     * @param value
     * @return
     */
    public static PriceStandard match(Integer value) {
        if (value == null) return null;
        for (PriceStandard e : values()) {
            if (e.getValue().equals(value)) {
                return e;
            }
        }
        return null;
    }
}
