package com.xdf.whiteaccount.enums;

/**
 * @program: white-account
 * @description: 收款类型
 * @author: 张柯
 * @create: 2021-06-07 16:41
 **/
public enum ReceivedTypeEnum {
    A("转账"), B("承兑"), C("现金");
    private String value;

    public String getValue() {
        return value;
    }

    ReceivedTypeEnum(String value) {
        this.value = value;
    }

    /**
     * 根据条件匹配
     *
     * @param name
     * @return
     */
    public static ReceivedTypeEnum match(String name) {
        if (name == null || name.length() <= 0) return null;
        for (ReceivedTypeEnum e : values()) {
            if (e.getValue().equals(name)) return e;
        }
        return null;
    }
}
