package com.xdf.whiteaccount.enums;

/**
 * @program: xzy-management-system
 * @description:
 * @author: 张柯
 * @create: 2021-01-04 15:57
 **/
public enum ResponseEnum {
    OK("成功"),
    NOT_ALLOW_DELETE("有关联产品，不可删除"),
    SAVE_DATA_ERROR("保存数据出错!"),
    NOT_ALLOW_EMPTY_DATA("禁止保存空数据"),
    IS_AUDIT("已审核,禁止更改"),
    IS_AUTH("无权进行此操作"),
    NO_ORDER("无此单据"),
    CANCEL_ORDER("单据已作废"),
    AUDIT_ORDER("单据已审核"),
    NOT_AUDIT_ORDER("单据未审核"),
    NOT_ALLOW_MODIFY("只能由创建人修改!"),
    ERROR("错误");
    private String name;

    ResponseEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
