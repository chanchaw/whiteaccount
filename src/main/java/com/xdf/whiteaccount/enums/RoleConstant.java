package com.xdf.whiteaccount.enums;

/**
 * @program: xzy-management-system
 * @description: 角色
 * @author: 张柯
 * @create: 2021-01-21 09:01
 **/
public class RoleConstant {
    public static final String ADMIN_ROLE = "ADMIN_ROLE";
}
