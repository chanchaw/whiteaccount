package com.xdf.whiteaccount.enums;

/**
 * @program: xzy-management-system
 * @description:
 * @author: 张柯
 * @create: 2021-01-22 10:28
 **/
public class SeparatorConstant {
    public static final String SEPARATOR = ":";
}
