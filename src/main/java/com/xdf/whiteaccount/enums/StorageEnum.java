package com.xdf.whiteaccount.enums;

/**
 * @program: white-account
 * @description: 仓库
 * @author: 张柯
 * @create: 2021-05-25 11:13
 **/
public enum StorageEnum {
    MATERIAL_STORAGE("1"), PRODUCT_STORAGE("2"), GREY_FABRIC_STORAGE("3");
    private String storageId;

    public void setStorageId(String storageId) {
        this.storageId = storageId;
    }

    StorageEnum(String storageId) {
        this.storageId = storageId;
    }

    /**
     * 自动匹配
     *
     * @param id
     * @return
     */
    public static StorageEnum match(String id) {
        if (id == null) return null;
        for (StorageEnum s : values()) {
            if (s.storageId.equals(id)) return s;
        }
        return null;
    }
}
