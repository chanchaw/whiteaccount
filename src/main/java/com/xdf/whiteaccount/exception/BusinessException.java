package com.xdf.whiteaccount.exception;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-10 19:44
 **/
public class BusinessException extends RuntimeException {
    public BusinessException(String message) {
        super(message);
    }
}
