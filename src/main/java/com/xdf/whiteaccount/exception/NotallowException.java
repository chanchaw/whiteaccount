package com.xdf.whiteaccount.exception;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-05-17 11:13
 **/
public class NotallowException extends RuntimeException{
    public NotallowException() {
        super();
    }

    public NotallowException(String message) {
        super(message);
    }
}
