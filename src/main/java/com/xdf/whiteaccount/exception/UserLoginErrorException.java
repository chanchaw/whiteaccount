package com.xdf.whiteaccount.exception;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-05-17 16:04
 **/
public class UserLoginErrorException extends RuntimeException{
    public UserLoginErrorException() {
        super();
    }

    public UserLoginErrorException(String message) {
        super(message);
    }
}
