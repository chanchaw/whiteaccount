package com.xdf.whiteaccount.mobilecontroller;

import com.xdf.whiteaccount.mobileservice.DataGetterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-27 17:21
 **/
@Api(tags = "微信数据查询接口")
@RestController
@CrossOrigin
@RequestMapping("/mobile")
public class DataGetterController {
    @Autowired
    private DataGetterService dataGetterService;

    /**
     * 存储过程调用
     *
     * @param openid
     * @param procName
     * @param params
     * @return
     */
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "openid", value = "微信中的openid参数", dataType = "String"),
            @ApiImplicitParam(name = "procName", value = "存储过程名称", dataType = "String"),
            @ApiImplicitParam(name = "params", value = "存储过程参数", dataType = "String[]")}
    )
    @ApiOperation(value = "执行存储过程", httpMethod = "GET/POST", notes = "根据传入的参数执行存储过程,存储过程名与openid参数必填")
    @RequestMapping(value = "/data/procedure", method = {RequestMethod.POST, RequestMethod.GET})
    public List<Map<String, Object>> getProcedureData(@RequestParam("openid") String openid, @RequestParam("procName") String procName, String[] params) throws Exception {
        return dataGetterService.procedureResultGetter(procName, ArrayUtils.isEmpty(params) ? new ArrayList<>() : new ArrayList<>(Arrays.asList(params)));
    }

    /**
     * SQL语句执行
     *
     * @param openid
     * @param sql
     * @return
     * @throws Exception
     */
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "openid", value = "微信中的openid参数", dataType = "String"),
            @ApiImplicitParam(name = "sql", value = "sql语句", dataType = "String")}
    )
    @ApiOperation(value = "执行Sql语句", httpMethod = "GET/POST", notes = "根据传入的参数执行,此功能存在一定危险,在之后的版本可能会去掉！")
    @RequestMapping(value = "/data/sql", method = {RequestMethod.POST, RequestMethod.GET})
    public List<Map<String, Object>> getSqlExecuteData(@RequestParam("openid") String openid, @RequestParam("sql") String sql) throws Exception {
        return dataGetterService.sqlResultGetter(sql);
    }
}