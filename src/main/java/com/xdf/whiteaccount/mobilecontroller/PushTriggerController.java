package com.xdf.whiteaccount.mobilecontroller;

import com.xdf.whiteaccount.schedule.ScheduleConfig;
import com.xdf.whiteaccount.entity.ResponseResult;
import com.xdf.whiteaccount.enums.ResponseEnum;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-07-14 09:08
 **/
@RestController
@CrossOrigin
@RequestMapping("/mobile/trigger")
@Slf4j
public class PushTriggerController {
    @Autowired
    private ScheduleConfig config;

    /**
     * 触发推送
     *
     * @param id 随机数
     * @return
     */
    @ApiOperation(value = "触发推送", httpMethod = "POST", notes = "触发微信推送")
    @PostMapping(value = "/push/{id}")
    public ResponseResult<Void> triggerPush(@PathVariable("id") Long id) {
        if (id > 0) {
            log.warn("推送请求成功执行！！！");
            config.pushInbound();
            config.pushOutbound();
        }
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }
}
