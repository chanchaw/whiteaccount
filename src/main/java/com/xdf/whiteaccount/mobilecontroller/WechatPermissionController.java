package com.xdf.whiteaccount.mobilecontroller;

import com.xdf.whiteaccount.cloudentity.UserMill;
import com.xdf.whiteaccount.cloudservice.UserMillService;
import com.xdf.whiteaccount.entity.ResponseResult;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.mobileentity.WechatPermission;
import com.xdf.whiteaccount.mobileservice.WechatPermissionService;
import com.xdf.whiteaccount.utils.Example;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-07-05 16:09
 **/
@Api(tags = "微信数据操作接口")
@CrossOrigin
@RestController
@RequestMapping("/mobile/wechat")
public class WechatPermissionController {
    @Autowired
    private WechatPermissionService wechatPermissionService;
    @Autowired
    private UserMillService userMillService;

    /**
     * 更改后台权限
     *
     * @param record
     * @return
     * @throws Exception
     */
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "openid", value = "微信中的openid参数", dataType = "String"),
            @ApiImplicitParam(name = "record", value = "参数", dataType = "UserPermission")}
    )
    @ApiOperation(value = "更改后台权限", httpMethod = "POST", notes = "更改公众号权限")
    @RequestMapping(value = "/permission/update", method = {RequestMethod.POST})
    public ResponseResult<Void> modifyPermission(WechatPermission record) throws Exception {
        wechatPermissionService.modifyPermission(record);
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 更改后台对应的客户
     *
     * @param record
     * @return
     */
    @ApiOperation(value = "更改后台对应的客户", httpMethod = "POST", notes = "更改公众号对应的客户")
    @PostMapping(value = "/mill/update")
    public ResponseResult<Void> modifyMill(UserMill record) throws Exception {
        userMillService.updateByExample(record, new Example().andEq("open_id", record.getOpenId()));
        return ResponseResult.<Void>builder().success(true).message(ResponseEnum.OK.getName()).build();
    }

    /**
     * 根据openid查询权限
     *
     * @param openid
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据openid查询当前用户的权限", httpMethod = "GET", notes = "查询接口")
    @RequestMapping(value = "/permission/query", method = {RequestMethod.GET})
    public WechatPermission queryByOpenid(String openid) throws Exception {
        return wechatPermissionService.query(openid);
    }

    /**
     * 根据openid查询权限
     *
     * @param openid
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据openid查询当前客户的权限", httpMethod = "GET", notes = "查询接口")
    @RequestMapping(value = "/permission/listQuery", method = {RequestMethod.GET})
    public List<WechatPermission> listQueryByOpenid(String openid) throws Exception {
        return wechatPermissionService.listQuery(openid);
    }

    /**
     * 根据openid查询权限
     *
     * @param mill
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据mill查询当前客户的权限", httpMethod = "GET", notes = "查询接口")
    @RequestMapping(value = "/permission/listQueryByMill", method = {RequestMethod.GET})
    public List<WechatPermission> listQueryByMill(@RequestParam("mill") String mill) throws Exception {
        return wechatPermissionService.listQueryByMill(mill);
    }

    /**
     * 根据openid查询权限
     *
     * @param openid
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "根据openid查询当前用户所在的公司", httpMethod = "GET", notes = "查询接口")
    @RequestMapping(value = "/mill", method = {RequestMethod.GET})
    public ResponseResult<String> getCompanyByOpenid(String openid) throws Exception {
        String mill = Optional.ofNullable(
                userMillService.listQuery(UserMill.builder().openId(openid).build()))
                .orElse(new ArrayList<>()).stream().findFirst().map(UserMill::getMillSid)
                .orElse("");
        return ResponseResult.<String>builder().success(true).message(ResponseEnum.OK.getName()).data(mill).build();
    }

    /**
     * 根据openid查询权限
     *
     * @return
     * @throws Exception
     * @Param openid
     */
    @ApiOperation(value = "查询当前染厂列表", httpMethod = "GET", notes = "查询接口")
    @RequestMapping(value = "/mill/list", method = {RequestMethod.GET})
    public List<Map<String, Object>> queryMillList(String openid) throws Exception {
        return userMillService.queryMillList(openid);
    }
}