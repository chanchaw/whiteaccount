package com.xdf.whiteaccount.mobiledao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @program: white-account
 * @description: 数据获取Mapper
 * @author: 张柯
 * @create: 2021-06-27 16:15
 **/
@Repository
public interface DataGetterMapper {
    List<Map<String, Object>> procedureResultGetter(@Param("name") String name,@Param("params") List<Object> params);

    List<Map<String, Object>> sqlResultGetter(String sql);
}
