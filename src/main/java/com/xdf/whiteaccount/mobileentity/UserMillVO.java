package com.xdf.whiteaccount.mobileentity;

import lombok.Data;

/**
 * @program: white-account
 * @description: 客户染厂映射
 * @author: 张柯
 * @create: 2021-07-09 14:46
 **/
@Data
public class UserMillVO {
    /** 微信openid */
    private String openId;

    /** 工厂名 */
    private String millSid;

    /** 用户名 */
    private String username;

    /** 默认工厂,1表示用户关联多个工厂时的默认工厂 */
    private Boolean millDefault;

    /** 有效,1表示关系有效 */
    private Boolean effective;

    /** 推送计划,默认1表示对应染厂有新计划则推送 */
    private Boolean pushPlan;

    /** 推送原料,默认1表示推送原料入库消息 */
    private Boolean pushMaterial;

    /** 推送发货 */
    private Boolean pushDeliver;
}