package com.xdf.whiteaccount.mobileentity;

import lombok.Data;

/**
 * @program: white-account
 * @description: 微信权限实体
 * @author: 张柯
 * @create: 2021-07-06 13:29
 **/
@Data
public class WechatPermission {
    /** 自增主键 */
    private Integer id;

    /** openid */
    private String openid;

    /** 已排计划 */
    private Boolean wxPlan;

    /** 客户订单 */
    private Boolean wxClientOrder;

    /** 日产量表 */
    private Boolean wxDailyYield;

    /** 应收应付 */
    private Boolean wxReceivablePayable;

    /** 白坯库存 */
    private Boolean wxFabricStore;

    /** 色布计划 */
    private Boolean wxDyePlan;

    /** 色布客户订单 */
    private Boolean wxDyeClientOrder;

    /** 色布库存 */
    private Boolean wxWxDyeFabricStore;

    /** 原料库存 */
    private Boolean wxMaterialStore;

    /** 权限管理 */
    private Boolean wxPermission;

    /** 推送计划 */
    private Boolean pushPlan;

    /** 推送原料 */
    private Boolean pushMaterial;

    /** 推送发货 */
    private Boolean pushDeliver;

    /** 审核 */
    private Boolean audit;

    /** 姓名 */
    private String realName;

    /** 昵称 */
    private String nickName;
}
