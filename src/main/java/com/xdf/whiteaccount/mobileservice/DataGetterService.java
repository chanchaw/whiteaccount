package com.xdf.whiteaccount.mobileservice;

import java.util.List;
import java.util.Map;

/**
 * @program: white-account
 * @description: 数据获取接口
 * @author: 张柯
 * @create: 2021-06-27 16:10
 **/
public interface DataGetterService {
    List<Map<String, Object>> procedureResultGetter(String procedureName, List<Object> params) throws Exception;

    List<Map<String, Object>> sqlResultGetter(String sql) throws Exception;
}
