package com.xdf.whiteaccount.mobileservice;

import com.xdf.whiteaccount.mobileentity.WechatPermission;

import java.util.List;

/**
 * @program: white-account
 * @description: 公众号权限操作接口
 * @author: 张柯
 * @create: 2021-07-05 13:53
 **/
public interface WechatPermissionService {
    int modifyPermission(WechatPermission permission) throws Exception;

    WechatPermission query(String openid) throws Exception;

    List<WechatPermission> listQuery(String openid) throws Exception;

    List<WechatPermission> listQueryByMill(String mill);
}
