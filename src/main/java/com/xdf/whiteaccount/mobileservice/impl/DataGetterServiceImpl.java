package com.xdf.whiteaccount.mobileservice.impl;

import com.xdf.whiteaccount.mobiledao.DataGetterMapper;
import com.xdf.whiteaccount.mobileservice.DataGetterService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import java.util.List;
import java.util.Map;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-27 16:12
 **/
@Service
@Slf4j
public class DataGetterServiceImpl implements DataGetterService {
    @Autowired
    private DataGetterMapper dao;

    /**
     * 存储过程调用
     *
     * @param procedureName 存储过程名称
     * @param params        参数
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> procedureResultGetter(String procedureName, List<Object> params) throws Exception {
        log.debug("请求参数:{},{}", procedureName, params.size());
        return dao.procedureResultGetter(procedureName, params);
    }

    /**
     * 执行sql语句
     *
     * @param sql sql语句
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> sqlResultGetter(String sql) throws Exception {
        if (StringUtils.isEmpty(sql)) return null;
        String newSql = sql.trim();
        Assert.state(newSql.toUpperCase().startsWith("SELECT") || newSql.toUpperCase().startsWith("CALL"), "只允许查询语句!");
        return dao.sqlResultGetter(newSql);
    }
}