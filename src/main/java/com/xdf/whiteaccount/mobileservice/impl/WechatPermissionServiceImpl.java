package com.xdf.whiteaccount.mobileservice.impl;

import com.xdf.whiteaccount.cloudentity.User;
import com.xdf.whiteaccount.cloudentity.UserMill;
import com.xdf.whiteaccount.cloudentity.UserPermission;
import com.xdf.whiteaccount.cloudservice.UserMillService;
import com.xdf.whiteaccount.cloudservice.UserPermissionService;
import com.xdf.whiteaccount.cloudservice.UserService;
import com.xdf.whiteaccount.mobileentity.WechatPermission;
import com.xdf.whiteaccount.mobileservice.WechatPermissionService;
import com.xdf.whiteaccount.utils.CustumDozerMapperUtil;
import com.xdf.whiteaccount.utils.Example;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-07-05 13:55
 **/
@Service
@Slf4j
public class WechatPermissionServiceImpl implements WechatPermissionService {
    @Autowired
    private CustumDozerMapperUtil utils;
    @Autowired
    private UserPermissionService userPermissionService;
    @Autowired
    private UserMillService userMillService;
    @Autowired
    private UserService userService;

    /**
     * 修改权限
     *
     * @param permission
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int modifyPermission(WechatPermission permission) throws Exception {
        UserPermission record = utils.transObject(permission, UserPermission.class);
        Assert.state(StringUtils.isNotEmpty(record.getOpenid()), "openid无效!");
        int result = 0;
        long count = userPermissionService.countByExample(new Example().andEq("openid", permission.getOpenid()));
        log.debug("当前数目：{}", count);
        //  修改权限
        if (count > 0) {
            result += userPermissionService.updateByExampleSelective(record, new Example().andEq("openid", record.getOpenid()));
        } else {
            result += userPermissionService.insertDumplicateKeyUpdate(record);
        }
        //  修改映射表的真实姓名
        result += userMillService.updateByExample(
                UserMill.builder()
                        .pushDeliver(permission.getPushDeliver())
                        .pushMaterial(permission.getPushMaterial())
                        .pushPlan(permission.getPushPlan())
                        .audit(permission.getAudit())
                        .username(permission.getRealName())
                        .build(),
                new Example().andEq("open_id", record.getOpenid()));
        //  修改用户表的真实姓名
        result += userService.updateByExampleSelective(User.builder().realName(
                permission.getRealName()).build(), new Example().andEq("open_id", permission.getOpenid()));
        return result;
    }

    /**
     * 查询权限
     *
     * @param openid
     * @return
     * @throws Exception
     */
    @Override
    public WechatPermission query(String openid) throws Exception {
        return userPermissionService.queryByOpenid(openid);
    }

    /**
     * 根据openid查询当前客户所有员工
     *
     * @param openid
     * @return
     */
    @Override
    public List<WechatPermission> listQuery(String openid) throws Exception {
        WechatPermission record = userPermissionService.queryByOpenid(openid);
        //  无权限返回空数组
        if (record == null || !Optional.ofNullable(record).map(WechatPermission::getWxPermission).orElse(false)) {
            return new ArrayList<>();
        }
        return userPermissionService.listQueryByOpenid(openid);
    }

    /**
     * 根据染厂名查询权限
     *
     * @param mill 染厂
     * @return
     */
    @Override
    public List<WechatPermission> listQueryByMill(String mill) {
        return userPermissionService.listQueryByMill(mill);
    }
}