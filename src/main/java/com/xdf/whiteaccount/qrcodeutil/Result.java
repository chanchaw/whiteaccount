package com.xdf.whiteaccount.qrcodeutil;

import lombok.Data;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-21 11:12
 **/
@Data
public class Result {
    private String code;
    private String msg;
    private String data;
}
