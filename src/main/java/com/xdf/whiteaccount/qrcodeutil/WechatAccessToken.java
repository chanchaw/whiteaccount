package com.xdf.whiteaccount.qrcodeutil;

public class WechatAccessToken {
    private String access_token;
    private long expireIn;// ���ڵ�ʱ���

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public long getExpireIn() {
        return expireIn;
    }

    public void setExpireIn(long expireIn) {
        this.expireIn = expireIn;
    }

    @Override
    public String toString() {
        return "AccessToken [access_token=" + access_token + ", expireIn=" + expireIn + "]";
    }

    public WechatAccessToken() {
    }

    public WechatAccessToken(String access_token, String expire_in) {
        super();
        resetWechatAccessToken(access_token, expire_in);
    }

    public void resetWechatAccessToken(String access_token, String expire_in) {
        this.access_token = access_token;
        try {
            this.expireIn = System.currentTimeMillis() + Integer.parseInt(expire_in) * 1000;
        } catch (Exception e) {
            this.expireIn = System.currentTimeMillis() + 1000;
        }
    }

    public boolean isExpired() {
        return System.currentTimeMillis() > this.expireIn;
    }
}
