package com.xdf.whiteaccount.qrcodeutil;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-21 11:08
 **/
@Component
@Slf4j
public class WechatQrCodeUtils {
    public static final String URL = "https://www.jzy.world/wxbe/qr/browserlogin";
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 获取二维码
     *
     * @param scene
     * @return
     */
    public String getQrCode(String scene) {
        Map<String, String> map = new HashMap<>();
        map.put("scene", scene);
        ResponseEntity<Result> rr = restTemplate.postForEntity(URL, map, Result.class);
        if (rr.getStatusCode() == HttpStatus.OK) {
            return Optional.ofNullable(rr.getBody()).map(Result::getData).orElse(null);
        }
        return null;
    }
}
