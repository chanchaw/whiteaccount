package com.xdf.whiteaccount.schedule;

import com.xdf.whiteaccount.service.PlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @program: white-account
 * @description: 自动审核单据 - 弃用
 * @author: 张柯
 * @create: 2021-07-24 11:03
 **/
@Component
public class AutoAuditSchedule {
    @Autowired
    private PlanService planService;
    private static final ConcurrentHashMap<String, Object> map = new ConcurrentHashMap<>();


}
