package com.xdf.whiteaccount.schedule;

import com.xdf.whiteaccount.async.IPushNotification;
import com.xdf.whiteaccount.cloudentity.UserMill;
import com.xdf.whiteaccount.cloudservice.UserMillService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @program: white-account
 * @description: 定时器设置：自动推送原料，白坯，色布的出入库数据
 * @author: 张柯
 * @create: 2021-06-26 08:16
 **/
@Component
@Slf4j
public class ScheduleConfig {
    @Autowired
    @Qualifier("dyeFabricPushNotificationImpl")
    private IPushNotification dyeFabricPushImpl;
    @Autowired
    @Qualifier("greyFabricPushNotificationImpl")
    private IPushNotification greyFabricPushImpl;
    @Autowired
    @Qualifier("materialPushNtfiImpl")
    private IPushNotification materialPushNtfiImpl;
    @Autowired
    private UserMillService userMillService;

    /**
     * 推送入库信息 - 2021年8月4日 13:26:49 没有推送入库消息
     * 2021年6月26日
     */
    //@Scheduled(cron = "${wechat.config.cron}")
    public void pushInbound() {
        try {
            List<String> list = Optional.ofNullable(userMillService.listQuery(UserMill.builder().pushDeliver(true).build()))
                    .orElse(new ArrayList<>())
                    .stream().map(UserMill::getOpenId).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(list)) {
                log.warn("开始推送！！！");
                dyeFabricPushImpl.pushInbound(list);
                greyFabricPushImpl.pushInbound(list);
                materialPushNtfiImpl.pushInbound(list);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    /**
     * 推送出库信息
     * 2021年6月26日
     */
    //@Scheduled(cron = "${wechat.config.cron}")
    public void pushOutbound() {
        try {
            List<String> list = Optional.ofNullable(userMillService.listQuery(UserMill.builder().pushDeliver(true).build()))
                    .orElse(new ArrayList<>())
                    .stream().map(UserMill::getOpenId).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(list)) {
                log.warn("开始推送！！！");
                dyeFabricPushImpl.pushOutbound(list);
                greyFabricPushImpl.pushOutbound(list);
                materialPushNtfiImpl.pushOutbound(list);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
}
