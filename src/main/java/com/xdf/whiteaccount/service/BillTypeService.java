package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.BillType;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-25 11:08:48
 */
public interface BillTypeService {
    int insert(BillType record) throws Exception;

    int insertSelective(BillType record) throws Exception;

    int multiInsert(List<BillType> list) throws Exception;

    int updateByPrimaryKey(BillType record) throws Exception;

    int updateByPrimaryKeySelective(BillType record) throws Exception;

    int update(BillType record) throws Exception;

    int multiUpdate(List<BillType> list) throws Exception;

    BillType selectByPrimaryKey(String id) throws Exception;

    int deleteByPrimaryKey(String id) throws Exception;

    List<BillType> listQuery(BillType record) throws Exception;
}