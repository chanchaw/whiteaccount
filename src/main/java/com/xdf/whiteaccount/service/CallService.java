package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.vo.DyeFabricPlanDetailVO;
import com.xdf.whiteaccount.vo.PlanDetailVO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-05-19 08:24
 **/
public interface CallService {
    String planCodeGen();

    String dyeFabricPlanCodeGen();

    String materialOrderCodeGen(int accessMode);

    String dyeFabricInboundCodeGen(boolean isFinishedProduct) throws Exception;

    String productOutwardOrderCodeGen(boolean isFinishedProduct);

    List<Map<String, Object>> getPlanReport(Date start, Date end, Boolean markFinished, String Strcolumn, String Strvalue) throws Exception;

    List<Map<String, Object>> getDyeFabricPlanReport(Date start, Date end, Boolean markFinished, String Strcolumn, String Strvalue) throws Exception;

    PlanDetailVO getPlanDtl(Long id) throws Exception;

    List<Map<String, Object>> getJrkbillDtl(Long id) throws Exception;

    List<Map<String, Object>> getJrkbillfh(Long id) throws Exception;

    List<Map<String, Object>> getMaterialOrderReport(Date start, Date end, String supplierName) throws Exception;

    List<Map<String, Object>> getFHhistoryDetail(Date start, Date end, String clientName, String checkType) throws Exception;

    List<Map<String, Object>> getReceivablesDetail(Date start, Date end, String clientName, Integer value) throws Exception;

    List<Map<String, Object>> getReceivablesAllSum() throws Exception;

    List<Map<String, Object>> getCollectionOld(Date start, Date end, String clientName, String value) throws Exception;

    List<Map<String, Object>> getPlanStorageModify(Long id) throws Exception;

    List<Map<String, Object>> getGreyfabricInput(Date start, Date end, String clientName) throws Exception;
//    List<Map<String, Object>> getGreyfabricStorage(String clientName, int mode) throws Exception;

List<Map<String, Object>> getGreyfabricStorage(String clientName, int mode,Date end) throws Exception;

    List<Map<String, Object>> getDyeFabricDelivery(Date start, Date end, String clientName, String checkout) throws Exception;

    DyeFabricPlanDetailVO getDyePlanDtl(Long id) throws Exception;

    List<Map<String, Object>> getDyeInboundReport(Date start, Date end, String clientName);

    List<Map<String, Object>> getDyeStorageReport(String clientName, Integer value);

    List<Map<String, Object>> getMaterialStorage(String supplierName) throws Exception;

    List<Map<String, Object>> getMaterialOutbound(Date start, Date end, String supplierName) throws Exception;

    List<Map<String, Object>> getAccountPayable(Date start, Date end, String clientName, Integer sign) throws Exception;

    List<Map<String, Object>> getAccountPayment(Date start, Date end, String supplierName, Integer value) throws Exception;

    List<Map<String, Object>> getMaterialOutward(Date start, Date end, String processName) throws Exception;

    List<Map<String, Object>> getProductInboundReport(Date start, Date end, String processName) throws Exception;

    List<Map<String, Object>> getUnfinishedOutward(Date start, Date end, String clientName) throws Exception;

    List<Map<String, Object>> getProcesscompanyAccount(Date start, Date end, String processName, Integer mode);

    List<Map<String, Object>> getCanceledPlan();

    List<Map<String, Object>> getCanceledDyeFabricPlan();

    List<Map<String, Object>> getProductWaste(Date start, Date end, String clientName) throws Exception;

    List<Map<String, Object>> getGreyFabricJrkbillReport(Date start, Date end, String machineNum);

    List<Map<String, Object>> getPlanReportDtl(Integer state, Boolean markFinished);

    List<Map<String, Object>> getUnImportedJRK(String processCompname);

    // 2021年8月27日 14:10:54 chanchaw
    // 白坯计划 的序时表中移库功能中弹出模态窗表格的数据源，排除指定的计划单后显示所有未完工的白坯计划
    List<Map<String, Object>> getPlanReportExclude(@Param("start") Date start, @Param("end") Date end, @Param("id") Long id) throws Exception;

    //白坯产量汇总表
    List<Map<String, Object>> getGreySummaryJrkbillReport(Date start, Date end);
}