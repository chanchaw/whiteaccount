package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.ContactCompany;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
public interface ContactCompanyService {
     int insert(ContactCompany record) throws Exception;

     int insertSelective(ContactCompany record) throws Exception;

     int multiInsert(List<ContactCompany> list) throws Exception;

     int updateByPrimaryKey(ContactCompany record) throws Exception;

     int updateByPrimaryKeySelective(ContactCompany record) throws Exception;

     int update(ContactCompany record) throws Exception;

     int multiUpdate(List<ContactCompany> list) throws Exception;

     ContactCompany selectByPrimaryKey(String id) throws Exception;

     int deleteByPrimaryKey(String id) throws Exception;

     List<ContactCompany> listQuery(ContactCompany record) throws Exception;
}