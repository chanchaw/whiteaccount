package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.CpAdjust;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-25 16:52:50
 */
public interface CpAdjustService {
    int insert(CpAdjust record) throws Exception;

    int insertSelective(CpAdjust record) throws Exception;

    int multiInsert(List<CpAdjust> list) throws Exception;

    int updateByPrimaryKey(CpAdjust record) throws Exception;

    int updateByPrimaryKeySelective(CpAdjust record) throws Exception;

    int update(CpAdjust record) throws Exception;

    int multiUpdate(List<CpAdjust> list) throws Exception;

    CpAdjust selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<CpAdjust> listQuery(CpAdjust record) throws Exception;

    int storeModifyByPlanId(CpAdjust record) throws Exception;
}