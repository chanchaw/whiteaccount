package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.CpBillfh;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-25 09:44:52
 */
public interface CpBillfhService {
    int insert(CpBillfh record) throws Exception;

    int insertSelective(CpBillfh record) throws Exception;

    int multiInsert(List<CpBillfh> list) throws Exception;

    int updateByPrimaryKey(CpBillfh record) throws Exception;

    int updateByPrimaryKeySelective(CpBillfh record) throws Exception;

    int update(CpBillfh record) throws Exception;

    int multiUpdate(List<CpBillfh> list) throws Exception;

    CpBillfh selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<CpBillfh> listQuery(CpBillfh record) throws Exception;
}