package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.CpJrkbillfh;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-25 09:44:52
 */
public interface CpJrkbillfhService {
    int insert(CpJrkbillfh record) throws Exception;

    int insertSelective(CpJrkbillfh record) throws Exception;

    int multiInsert(List<CpJrkbillfh> list) throws Exception;

    int updateByPrimaryKey(CpJrkbillfh record) throws Exception;

    int updateByPrimaryKeySelective(CpJrkbillfh record) throws Exception;

    int update(CpJrkbillfh record) throws Exception;

    int multiUpdate(List<CpJrkbillfh> list) throws Exception;

    CpJrkbillfh selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<CpJrkbillfh> listQuery(CpJrkbillfh record) throws Exception;
}