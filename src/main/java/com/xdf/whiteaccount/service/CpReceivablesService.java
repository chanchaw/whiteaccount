package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.CpReceivables;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-25 16:52:50
 */
public interface CpReceivablesService {
    int insert(CpReceivables record) throws Exception;

    int insertSelective(CpReceivables record) throws Exception;

    int multiInsert(List<CpReceivables> list) throws Exception;

    int updateByPrimaryKey(CpReceivables record) throws Exception;

    int updateByPrimaryKeySelective(CpReceivables record) throws Exception;

    int update(CpReceivables record) throws Exception;

    int multiUpdate(List<CpReceivables> list) throws Exception;

    CpReceivables selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<CpReceivables> listQuery(CpReceivables record) throws Exception;
}