package com.xdf.whiteaccount.service;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-12 10:11
 **/
public interface CustomizeService {
    String getDataBase();

    int clearAllData();

    int clearBasicData();

    int clearDocument();

    boolean backup();

    boolean backupAndReset(String[] type, String password);
}
