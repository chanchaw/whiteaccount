package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.DeliveryCompany;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-29 12:30:54
 */
public interface DeliveryCompanyService {
     int insert(DeliveryCompany record) throws Exception;

     int insertSelective(DeliveryCompany record) throws Exception;

     int multiInsert(List<DeliveryCompany> list) throws Exception;

     int updateByPrimaryKey(DeliveryCompany record) throws Exception;

     int updateByPrimaryKeySelective(DeliveryCompany record) throws Exception;

     int update(DeliveryCompany record) throws Exception;

     int multiUpdate(List<DeliveryCompany> list) throws Exception;

     DeliveryCompany selectByPrimaryKey(Integer id) throws Exception;

     int deleteByPrimaryKey(Integer id) throws Exception;

     List<DeliveryCompany> listQuery(DeliveryCompany record) throws Exception;
}