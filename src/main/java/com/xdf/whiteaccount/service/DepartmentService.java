package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.dto.TreeDTO;
import com.xdf.whiteaccount.entity.Department;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-19 20:26:32
 */
public interface DepartmentService {
    int insert(Department record) throws Exception;

    int insertSelective(Department record) throws Exception;

    int multiInsert(List<Department> list) throws Exception;

    int updateByPrimaryKey(Department record) throws Exception;

    int updateByPrimaryKeySelective(Department record) throws Exception;

    int update(Department record) throws Exception;

    int multiUpdate(List<Department> list) throws Exception;

    Department selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<Department> listQuery(Department record) throws Exception;

    List<TreeDTO> listQueryTreeNodes(Integer parentId) throws Exception;
}