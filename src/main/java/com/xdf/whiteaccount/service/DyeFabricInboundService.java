package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.DyeFabricInbound;
import com.xdf.whiteaccount.service.base.BaseAuditService;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-07-20 14:42:34
 */
public interface DyeFabricInboundService extends BaseAuditService {
    int insert(DyeFabricInbound record) throws Exception;

    int insertSelective(DyeFabricInbound record) throws Exception;

    int multiInsert(List<DyeFabricInbound> list) throws Exception;

    int updateByPrimaryKey(DyeFabricInbound record) throws Exception;

    int updateByPrimaryKeySelective(DyeFabricInbound record) throws Exception;

    int update(DyeFabricInbound record) throws Exception;

    int multiUpdate(List<DyeFabricInbound> list) throws Exception;

    DyeFabricInbound selectByPrimaryKey(Long id) throws Exception;

    int deleteByPrimaryKey(Long id) throws Exception;

    int cancelByPrimaryKey(Long id) throws Exception;

    List<DyeFabricInbound> listQuery(DyeFabricInbound record) throws Exception;
}