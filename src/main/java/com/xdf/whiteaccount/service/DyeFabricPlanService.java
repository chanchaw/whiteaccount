package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.DyeFabricPlan;
import com.xdf.whiteaccount.service.base.BaseAuditService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-23 16:01:16
 */
public interface DyeFabricPlanService extends BaseAuditService {
    int insert(DyeFabricPlan record) throws Exception;

    int insertSelective(DyeFabricPlan record) throws Exception;

    int multiInsert(List<DyeFabricPlan> list) throws Exception;

    int updateByPrimaryKey(DyeFabricPlan record) throws Exception;

    int updateByPrimaryKeySelective(DyeFabricPlan record) throws Exception;

    int update(DyeFabricPlan record) throws Exception;

    int multiUpdate(List<DyeFabricPlan> list) throws Exception;

    DyeFabricPlan selectByPrimaryKey(Long id) throws Exception;

    int deleteByPrimaryKey(Long id) throws Exception;

    List<DyeFabricPlan> listQuery(DyeFabricPlan record) throws Exception;

    int cancelByPrimaryKey(Long id);

    int recoverByPrimaryKey(Long id);

    int setFinishedMark(Long id, boolean finished);

    int modifyPriceByPk(BigDecimal accountPrice, Long planid) throws Exception;

    List<Map<String, Object>> getPriceStandard() throws Exception;
}