package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.DyeFabricPlanTemplate;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-24 10:06:23
 */
public interface DyeFabricPlanTemplateService {
    int insert(DyeFabricPlanTemplate record) throws Exception;

    int insertSelective(DyeFabricPlanTemplate record) throws Exception;

    int multiInsert(List<DyeFabricPlanTemplate> list) throws Exception;

    int updateByPrimaryKey(DyeFabricPlanTemplate record) throws Exception;

    int updateByPrimaryKeySelective(DyeFabricPlanTemplate record) throws Exception;

    int update(DyeFabricPlanTemplate record) throws Exception;

    int multiUpdate(List<DyeFabricPlanTemplate> list) throws Exception;

    DyeFabricPlanTemplate selectByPrimaryKey(Long id) throws Exception;

    int deleteByPrimaryKey(Long id) throws Exception;

    List<DyeFabricPlanTemplate> listQuery(DyeFabricPlanTemplate record) throws Exception;

    int updateSelectiveDefault(DyeFabricPlanTemplate record);

    DyeFabricPlanTemplate selectDefaultTemplate();
}