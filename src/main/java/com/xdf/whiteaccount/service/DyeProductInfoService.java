package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.DyeProductInfo;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-25 09:44:52
 */
public interface DyeProductInfoService {
    int insert(DyeProductInfo record) throws Exception;

    int insertSelective(DyeProductInfo record) throws Exception;

    int multiInsert(List<DyeProductInfo> list) throws Exception;

    int updateByPrimaryKey(DyeProductInfo record) throws Exception;

    int updateByPrimaryKeySelective(DyeProductInfo record) throws Exception;

    int update(DyeProductInfo record) throws Exception;

    int multiUpdate(List<DyeProductInfo> list) throws Exception;

    DyeProductInfo selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<DyeProductInfo> listQuery(DyeProductInfo record) throws Exception;
}