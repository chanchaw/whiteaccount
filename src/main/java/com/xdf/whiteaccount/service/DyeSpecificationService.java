package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.DyeSpecification;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-26 08:36:35
 */
public interface DyeSpecificationService {
    int insert(DyeSpecification record) throws Exception;

    int insertSelective(DyeSpecification record) throws Exception;

    int multiInsert(List<DyeSpecification> list) throws Exception;

    int updateByPrimaryKey(DyeSpecification record) throws Exception;

    int updateByPrimaryKeySelective(DyeSpecification record) throws Exception;

    int update(DyeSpecification record) throws Exception;

    int multiUpdate(List<DyeSpecification> list) throws Exception;

    DyeSpecification selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<DyeSpecification> listQuery(DyeSpecification record) throws Exception;
}