package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.Employee;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-07-15 08:51:40
 */
public interface EmployeeService {
    int insert(Employee record) throws Exception;

    int insertSelective(Employee record) throws Exception;

    int multiInsert(List<Employee> list) throws Exception;

    int updateByPrimaryKey(Employee record) throws Exception;

    int updateByPrimaryKeySelective(Employee record) throws Exception;

    int update(Employee record) throws Exception;

    int multiUpdate(List<Employee> list) throws Exception;

    Employee selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<Employee> listQuery(Employee record) throws Exception;

    List<Employee> queryByFk(Integer id) throws Exception;
}