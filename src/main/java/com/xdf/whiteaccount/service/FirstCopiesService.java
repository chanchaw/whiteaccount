package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.FirstCopies;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-26 17:52:25
 */
public interface FirstCopiesService {
    int insert(FirstCopies record) throws Exception;

    int insertSelective(FirstCopies record) throws Exception;

    int multiInsert(List<FirstCopies> list) throws Exception;

    int updateByPrimaryKey(FirstCopies record) throws Exception;

    int updateByPrimaryKeySelective(FirstCopies record) throws Exception;

    int update(FirstCopies record) throws Exception;

    int multiUpdate(List<FirstCopies> list) throws Exception;

    FirstCopies selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<FirstCopies> listQuery(FirstCopies record) throws Exception;
}