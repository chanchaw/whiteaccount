package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.FlywaySchemaHistory;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
public interface FlywaySchemaHistoryService {
     int insert(FlywaySchemaHistory record) throws Exception;

     int insertSelective(FlywaySchemaHistory record) throws Exception;

     int multiInsert(List<FlywaySchemaHistory> list) throws Exception;

     int updateByPrimaryKey(FlywaySchemaHistory record) throws Exception;

     int updateByPrimaryKeySelective(FlywaySchemaHistory record) throws Exception;

     int update(FlywaySchemaHistory record) throws Exception;

     int multiUpdate(List<FlywaySchemaHistory> list) throws Exception;

     FlywaySchemaHistory selectByPrimaryKey(Integer id) throws Exception;

     int deleteByPrimaryKey(Integer id) throws Exception;

     List<FlywaySchemaHistory> listQuery(FlywaySchemaHistory record) throws Exception;
}