package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.GJrkbill;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-22 08:16:02
 */
public interface GJrkbillService {
    int insert(GJrkbill record) throws Exception;

    int insertSelective(GJrkbill record) throws Exception;

    int multiInsert(List<GJrkbill> list) throws Exception;

    int updateByPrimaryKey(GJrkbill record) throws Exception;

    int updateByPrimaryKeySelective(GJrkbill record) throws Exception;

    int update(GJrkbill record) throws Exception;

    int multiUpdate(List<GJrkbill> list) throws Exception;

    GJrkbill selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<GJrkbill> listQuery(GJrkbill record) throws Exception;
}