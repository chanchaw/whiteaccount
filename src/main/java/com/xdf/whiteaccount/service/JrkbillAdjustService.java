package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.JrkbillAdjust;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-04 17:17:21
 */
public interface JrkbillAdjustService {
    int insert(JrkbillAdjust record) throws Exception;

    int insertSelective(JrkbillAdjust record) throws Exception;

    int multiInsert(List<JrkbillAdjust> list) throws Exception;

    int updateByPrimaryKey(JrkbillAdjust record) throws Exception;

    int updateByPrimaryKeySelective(JrkbillAdjust record) throws Exception;

    int update(JrkbillAdjust record) throws Exception;

    int multiUpdate(List<JrkbillAdjust> list) throws Exception;

    JrkbillAdjust selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<JrkbillAdjust> listQuery(JrkbillAdjust record) throws Exception;

    int storeModifyByPlanId(JrkbillAdjust record) throws Exception;

    int migrateStock(Long ido, Long idt, Integer pairs, BigDecimal qty);

    int deleteByOpbatch(String opbatch);
}