package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.Jrkbillfh;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-22 08:16:02
 */
public interface JrkbillfhService {
    int insert(Jrkbillfh record) throws Exception;

    int insertSelective(Jrkbillfh record) throws Exception;

    int multiInsert(List<Jrkbillfh> list) throws Exception;

    int updateByPrimaryKey(Jrkbillfh record) throws Exception;

    int updateByPrimaryKeySelective(Jrkbillfh record) throws Exception;

    int update(Jrkbillfh record) throws Exception;

    int multiUpdate(List<Jrkbillfh> list) throws Exception;

    Jrkbillfh selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<Jrkbillfh> listQuery(Jrkbillfh record) throws Exception;
}