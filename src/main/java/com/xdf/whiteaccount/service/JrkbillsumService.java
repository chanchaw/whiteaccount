package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.Jrkbillsum;
import com.xdf.whiteaccount.entity.ProductOutwardOrder;

import java.util.Date;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-02 19:49:07
 */
public interface JrkbillsumService {
    int insert(Jrkbillsum record) throws Exception;

    int insertSelective(Jrkbillsum record) throws Exception;

    int multiInsert(List<Jrkbillsum> list) throws Exception;

    int updateByPrimaryKey(Jrkbillsum record) throws Exception;

    int updateByPrimaryKeySelective(Jrkbillsum record) throws Exception;

    int update(Jrkbillsum record) throws Exception;

    int multiUpdate(List<Jrkbillsum> list) throws Exception;

    Jrkbillsum selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<Jrkbillsum> listQuery(Jrkbillsum record) throws Exception;

    List<Jrkbillsum> listQueryByDate(Date start, Date end) throws Exception;
    int multiUpdateOutwardId(ProductOutwardOrder record);
}