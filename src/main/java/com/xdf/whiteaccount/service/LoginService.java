package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.SysUser;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-11 16:37
 **/
public interface LoginService {
    int insertUser(SysUser sysUser) throws Exception;

    SysUser selectUser(String userId) throws Exception;

    int login(String qrCode) throws Exception;
}
