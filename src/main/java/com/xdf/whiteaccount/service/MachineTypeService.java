package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.MachineType;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
public interface MachineTypeService {
     int insert(MachineType record) throws Exception;

     int insertSelective(MachineType record) throws Exception;

     int multiInsert(List<MachineType> list) throws Exception;

     int updateByPrimaryKey(MachineType record) throws Exception;

     int updateByPrimaryKeySelective(MachineType record) throws Exception;

     int multiUpdate(List<MachineType> list) throws Exception;

     MachineType selectByPrimaryKey(String id) throws Exception;

     int deleteByPrimaryKey(String id) throws Exception;

     List<MachineType> listQuery(MachineType record) throws Exception;
}