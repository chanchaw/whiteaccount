package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.MaterialAccountPayable;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-07-01 16:40:45
 */
public interface MaterialAccountPayableService {
    int insert(MaterialAccountPayable record) throws Exception;

    int insertSelective(MaterialAccountPayable record) throws Exception;

    int multiInsert(List<MaterialAccountPayable> list) throws Exception;

    int updateByPrimaryKey(MaterialAccountPayable record) throws Exception;

    int updateByPrimaryKeySelective(MaterialAccountPayable record) throws Exception;

    int update(MaterialAccountPayable record) throws Exception;

    int multiUpdate(List<MaterialAccountPayable> list) throws Exception;

    MaterialAccountPayable selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<MaterialAccountPayable> listQuery(MaterialAccountPayable record) throws Exception;
}