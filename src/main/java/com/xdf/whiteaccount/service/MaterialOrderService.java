package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.MaterialOrder;
import com.xdf.whiteaccount.service.base.BaseAuditService;
import java.util.List;
import java.util.Map;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-25 17:02:01
 */
public interface MaterialOrderService extends BaseAuditService {
    int insert(MaterialOrder record) throws Exception;

    int insertSelective(MaterialOrder record) throws Exception;

    int multiInsert(List<MaterialOrder> list) throws Exception;

    int updateByPrimaryKey(MaterialOrder record) throws Exception;

    int updateByPrimaryKeySelective(MaterialOrder record) throws Exception;

    int update(MaterialOrder record) throws Exception;

    int multiUpdate(List<MaterialOrder> list) throws Exception;

    MaterialOrder selectByPrimaryKey(Long id) throws Exception;

    int deleteByPrimaryKey(Long id) throws Exception;

    int cancelByPrimaryKey(Long id) throws Exception;

    List<MaterialOrder> listQuery(MaterialOrder record) throws Exception;

    List<Map<String, Object>> getMaterialStorageDropdownList(String materialSpec, String[] selected, int mode);
}