package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.MeterLength;

import java.util.List;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-08-04 15:03
 **/
public interface MeterLengthService {
    int deleteByPrimaryKey(Integer id);

    int insert(MeterLength record);

    int insertSelective(MeterLength record);

    MeterLength selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MeterLength record);

    int updateByPrimaryKey(MeterLength record);

    List<MeterLength> listQuery(MeterLength record);
}
