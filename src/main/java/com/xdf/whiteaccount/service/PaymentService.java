package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.Payment;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-31 14:14:23
 */
public interface PaymentService {
    int insert(Payment record) throws Exception;

    int insertSelective(Payment record) throws Exception;

    int multiInsert(List<Payment> list) throws Exception;

    int updateByPrimaryKey(Payment record) throws Exception;

    int updateByPrimaryKeySelective(Payment record) throws Exception;

    int update(Payment record) throws Exception;

    int multiUpdate(List<Payment> list) throws Exception;

    Payment selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<Payment> listQuery(Payment record) throws Exception;
}