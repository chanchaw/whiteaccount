package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.PlanBatchnum;
import com.xdf.whiteaccount.utils.Example;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-07-19 09:24:54
 */
public interface PlanBatchnumService {
    int insert(PlanBatchnum record) throws Exception;

    int insertSelective(PlanBatchnum record) throws Exception;

    int multiInsert(List<PlanBatchnum> list) throws Exception;

    int updateByPrimaryKey(PlanBatchnum record) throws Exception;

    int updateByPrimaryKeySelective(PlanBatchnum record) throws Exception;

    int update(PlanBatchnum record) throws Exception;

    int multiUpdate(List<PlanBatchnum> list) throws Exception;

    PlanBatchnum selectByPrimaryKey(Long id) throws Exception;

    int deleteByPrimaryKey(Long id) throws Exception;

    List<PlanBatchnum> listQuery(PlanBatchnum record) throws Exception;

    List<PlanBatchnum> queryByPlanCode(String planCode) throws Exception;

    int deleteByExample(Example example) throws Exception;
}