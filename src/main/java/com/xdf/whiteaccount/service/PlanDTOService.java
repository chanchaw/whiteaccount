package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.dto.PlanDTO;

/**
 * @program: white-account
 * @description: 计划业务层
 * @author: 张柯
 * @create: 2021-07-19 09:38
 **/
public interface PlanDTOService {
    PlanDTO insertSelective(PlanDTO record) throws Exception;

    PlanDTO updateSelective(PlanDTO record) throws Exception;

    PlanDTO selectByPrimaryKey(Long id) throws Exception;
}
