package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.Plan;
import com.xdf.whiteaccount.service.base.BaseAuditService;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
public interface PlanService extends BaseAuditService {
    int insert(Plan record) throws Exception;

    int insertSelective(Plan record) throws Exception;

    int multiInsert(List<Plan> list) throws Exception;

    int updateByPrimaryKey(Plan record) throws Exception;

    int updateByPrimaryKeySelective(Plan record) throws Exception;

    int update(Plan record) throws Exception;

    int multiUpdate(List<Plan> list) throws Exception;

    Plan selectByPrimaryKey(Long id) throws Exception;

    int deleteByPrimaryKey(Long id) throws Exception;

    int setFinishedMark(Long id, boolean finished) throws Exception;

    int cancelByPrimaryKey(Long id) throws Exception;

    int recoverPlanByPk(Long id) throws Exception;

    List<Plan> listQuery(Plan record) throws Exception;

    int auditAll() throws Exception;

    int modifyPriceByPk(BigDecimal accountPrice, Long planid) throws Exception;

    List<Plan> queryByExample(Plan record);

    int setPlanOrderShowState(boolean state, Long[] id);
}