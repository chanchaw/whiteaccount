package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.PlanTemplate;
import com.xdf.whiteaccount.vo.PlanTemplateVO;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-20 19:00:08
 */
public interface PlanTemplateService {
    int insert(PlanTemplate record) throws Exception;

    int insertSelective(PlanTemplate record) throws Exception;

    int multiInsert(List<PlanTemplate> list) throws Exception;

    int updateByPrimaryKey(PlanTemplate record) throws Exception;

    int updateByPrimaryKeySelective(PlanTemplate record) throws Exception;

    int update(PlanTemplate record) throws Exception;

    int multiUpdate(List<PlanTemplate> list) throws Exception;

    PlanTemplate selectByPrimaryKey(Long id) throws Exception;

    int deleteByPrimaryKey(Long id) throws Exception;

    List<PlanTemplate> listQuery(PlanTemplate record) throws Exception;

    PlanTemplateVO selectDefaultTemplate() throws Exception;

    PlanTemplateVO copy() throws Exception;

    int updateSelectiveDefault(PlanTemplate planTemplate) throws Exception;
}