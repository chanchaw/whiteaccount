package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.ProcessAccountPayable;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-07-16 12:10:29
 */
public interface ProcessAccountPayableService {
    int insert(ProcessAccountPayable record) throws Exception;

    int insertSelective(ProcessAccountPayable record) throws Exception;

    int multiInsert(List<ProcessAccountPayable> list) throws Exception;

    int updateByPrimaryKey(ProcessAccountPayable record) throws Exception;

    int updateByPrimaryKeySelective(ProcessAccountPayable record) throws Exception;

    int update(ProcessAccountPayable record) throws Exception;

    int multiUpdate(List<ProcessAccountPayable> list) throws Exception;

    ProcessAccountPayable selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<ProcessAccountPayable> listQuery(ProcessAccountPayable record) throws Exception;
}