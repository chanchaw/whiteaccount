package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.ProcessCompany;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-30 12:54:45
 */
public interface ProcessCompanyService {
    int insert(ProcessCompany record) throws Exception;

    int insertSelective(ProcessCompany record) throws Exception;

    int multiInsert(List<ProcessCompany> list) throws Exception;

    int updateByPrimaryKey(ProcessCompany record) throws Exception;

    int updateByPrimaryKeySelective(ProcessCompany record) throws Exception;

    int update(ProcessCompany record) throws Exception;

    int multiUpdate(List<ProcessCompany> list) throws Exception;

    ProcessCompany selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<ProcessCompany> listQuery(ProcessCompany record) throws Exception;
}