package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.ProcessType;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-23 16:01:16
 */
public interface ProcessTypeService {
    int insert(ProcessType record) throws Exception;

    int insertSelective(ProcessType record) throws Exception;

    int multiInsert(List<ProcessType> list) throws Exception;

    int updateByPrimaryKey(ProcessType record) throws Exception;

    int updateByPrimaryKeySelective(ProcessType record) throws Exception;

    int update(ProcessType record) throws Exception;

    int multiUpdate(List<ProcessType> list) throws Exception;

    ProcessType selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<ProcessType> listQuery(ProcessType record) throws Exception;
}