package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.ProductInfo;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
public interface ProductInfoService {
     int insert(ProductInfo record) throws Exception;

     int insertSelective(ProductInfo record) throws Exception;

     int multiInsert(List<ProductInfo> list) throws Exception;

     int updateByPrimaryKey(ProductInfo record) throws Exception;

     int updateByPrimaryKeySelective(ProductInfo record) throws Exception;

     int update(ProductInfo record) throws Exception;

     int multiUpdate(List<ProductInfo> list) throws Exception;

     ProductInfo selectByPrimaryKey(Integer id) throws Exception;

     int deleteByPrimaryKey(Integer id) throws Exception;

     List<ProductInfo> listQuery(ProductInfo record) throws Exception;
}