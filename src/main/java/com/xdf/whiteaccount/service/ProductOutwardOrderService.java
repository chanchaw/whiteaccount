package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.ProductOutwardOrder;
import com.xdf.whiteaccount.service.base.BaseAuditService;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-07-02 17:20:51
 */
public interface ProductOutwardOrderService extends BaseAuditService {
    int insert(ProductOutwardOrder record) throws Exception;

    int insertSelective(ProductOutwardOrder record) throws Exception;

    int multiInsert(List<ProductOutwardOrder> list) throws Exception;

    int updateByPrimaryKey(ProductOutwardOrder record) throws Exception;

    int updateByPrimaryKeySelective(ProductOutwardOrder record) throws Exception;

    int update(ProductOutwardOrder record) throws Exception;

    int multiUpdate(List<ProductOutwardOrder> list) throws Exception;

    ProductOutwardOrder selectByPrimaryKey(Long id) throws Exception;

    int deleteByPrimaryKey(Long id) throws Exception;

    int cancelByPk(Long id) throws Exception;

    List<ProductOutwardOrder> listQuery(ProductOutwardOrder record) throws Exception;
    List<ProductOutwardOrder> gatherByJrk(List<Long> jrkIds);
    int insertByGatheredJrk(List<Long> jrkIds,String processCompanyName);
    int mapping8jrk(List<Long> jrkIds,String processCompanyName);
}