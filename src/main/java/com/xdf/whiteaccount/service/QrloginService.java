package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.Qrlogin;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-03 19:48:25
 */
public interface QrloginService {
    int insert(Qrlogin record) throws Exception;

    int insertSelective(Qrlogin record) throws Exception;

    int multiInsert(List<Qrlogin> list) throws Exception;

    int updateByPrimaryKey(Qrlogin record) throws Exception;

    int updateByPrimaryKeySelective(Qrlogin record) throws Exception;

    int update(Qrlogin record) throws Exception;

    int multiUpdate(List<Qrlogin> list) throws Exception;

    Qrlogin selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<Qrlogin> listQuery(Qrlogin record) throws Exception;
}