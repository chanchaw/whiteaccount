package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.Receivables;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-07 16:35:39
 */
public interface ReceivablesService {
    int insert(Receivables record) throws Exception;

    int insertSelective(Receivables record) throws Exception;

    int multiInsert(List<Receivables> list) throws Exception;

    int updateByPrimaryKey(Receivables record) throws Exception;

    int updateByPrimaryKeySelective(Receivables record) throws Exception;

    int update(Receivables record) throws Exception;

    int multiUpdate(List<Receivables> list) throws Exception;

    Receivables selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<Receivables> listQuery(Receivables record) throws Exception;
}