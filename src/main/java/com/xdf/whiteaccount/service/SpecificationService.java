package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.Specification;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
public interface SpecificationService {
     int insert(Specification record) throws Exception;

     int insertSelective(Specification record) throws Exception;

     int multiInsert(List<Specification> list) throws Exception;

     int updateByPrimaryKey(Specification record) throws Exception;

     int updateByPrimaryKeySelective(Specification record) throws Exception;

     int update(Specification record) throws Exception;

     int multiUpdate(List<Specification> list) throws Exception;

     Specification selectByPrimaryKey(Long id) throws Exception;

     int deleteByPrimaryKey(Long id) throws Exception;

     List<Specification> listQuery(Specification record) throws Exception;
}