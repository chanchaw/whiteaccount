package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.Storage;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-07-23 09:48:38
 */
public interface StorageService {
    int insert(Storage record) throws Exception;

    int insertSelective(Storage record) throws Exception;

    int multiInsert(List<Storage> list) throws Exception;

    int updateByPrimaryKey(Storage record) throws Exception;

    int updateByPrimaryKeySelective(Storage record) throws Exception;

    int update(Storage record) throws Exception;

    int multiUpdate(List<Storage> list) throws Exception;

    Storage selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<Storage> listQuery(Storage record) throws Exception;
}