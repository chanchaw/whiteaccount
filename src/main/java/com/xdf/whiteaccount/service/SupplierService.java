package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.Supplier;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
public interface SupplierService {
     int insert(Supplier record) throws Exception;

     int insertSelective(Supplier record) throws Exception;

     int multiInsert(List<Supplier> list) throws Exception;

     int updateByPrimaryKey(Supplier record) throws Exception;

     int updateByPrimaryKeySelective(Supplier record) throws Exception;

     int update(Supplier record) throws Exception;

     int multiUpdate(List<Supplier> list) throws Exception;

     Supplier selectByPrimaryKey(String id) throws Exception;

     int deleteByPrimaryKey(String id) throws Exception;

     List<Supplier> listQuery(Supplier record) throws Exception;
}