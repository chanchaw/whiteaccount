package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.SysConfigDecimal;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface SysConfigDecimalService {

    int insert(SysConfigDecimal record) throws Exception;

    int insertSelective(SysConfigDecimal record) throws Exception;

    int updateByPrimaryKey(SysConfigDecimal record) throws Exception;

    int updateByPrimaryKeySelective(SysConfigDecimal record) throws Exception;

    SysConfigDecimal selectByPrimaryKey(String config_field) throws Exception;

    List<SysConfigDecimal> listQuery(SysConfigDecimal record) throws Exception;

    int Query(SysConfigDecimal record) throws Exception;

    int deleteByPrimaryKey(String config_field) throws Exception;
}
