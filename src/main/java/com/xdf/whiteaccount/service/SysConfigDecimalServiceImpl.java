package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.dao.SysConfigDecimalMapper;
import com.xdf.whiteaccount.entity.SysConfigDecimal;
import com.xdf.whiteaccount.enums.ConfigDecimalEnum;
import com.xdf.whiteaccount.enums.ConfigIntEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
public class SysConfigDecimalServiceImpl implements SysConfigDecimalService{
    @Autowired
    private SysConfigDecimalMapper dao;

    /**
     * @Describe 新增方法
     * @author szx
     * @Date 2021年10月20日09:30:36
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(SysConfigDecimal record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author szx
     * @Date 2021年10月20日09:30:24
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(SysConfigDecimal record) throws Exception {
        if (record.getConfig_smallvalue() != null) {
            if (record.getConfig_smallvalue().intValue() <= -10) record.setConfig_smallvalue(BigDecimal.valueOf(-10));
            if (record.getConfig_smallvalue().intValue() >= 0) record.setConfig_smallvalue(BigDecimal.valueOf(0));
        }
        if (record.getConfig_bigvalue() != null) {
            if (record.getConfig_bigvalue().intValue() <= 0) record.setConfig_bigvalue(BigDecimal.valueOf(0));
            if (record.getConfig_bigvalue().intValue() >= 10) record.setConfig_bigvalue(BigDecimal.valueOf(10));
        }
        int ret = dao.insertSelective(record);
        return ret;
    }

    /**
     * @Describe 根据主键删除
     * @author szx
     * @Date 2021年10月20日09:30:14
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(String config_field) throws Exception {
        int ret = dao.deleteByPrimaryKey(config_field);
        return ret;
    }

    /**
     * @Describe 选择修改
     * @author szx
     * @Date 2021年10月20日09:30:55
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(SysConfigDecimal record) throws Exception {
        int ret = dao.updateByPrimaryKeySelective(record);
        return ret;
    }

    /**
     * @Describe 根据主键查询
     * @author szx
     * @Date 2021年10月20日09:31:03
     */
    @Override
    public SysConfigDecimal selectByPrimaryKey(String config_field) throws Exception {
        SysConfigDecimal ret = dao.selectByPrimaryKey(config_field);
        return ret;
    }

    /**
     * @Describe 根据Entity查询
     * @author szx
     * @Date 2021年10月20日09:31:13
     */
    @Override
    public List<SysConfigDecimal> listQuery(SysConfigDecimal record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * @Describe 根据《纸管》更新
     * @author szx
     * @Date 2021年10月20日09:31:13
     */
    @Override
    public int Query(SysConfigDecimal record) throws Exception {
        if (record.getConfig_default() != null) {
            if (record.getConfig_default().intValue() <= record.getConfig_smallvalue().intValue())
                record.getConfig_smallvalue().intValue();
            if (record.getConfig_default().intValue() >= record.getConfig_bigvalue().intValue())
                record.getConfig_bigvalue().intValue();
        }
        int ret = dao.updateByZGParam(record);
        return ret;
    }

    /**
     * @Describe 修改
     * @author szx
     * @Date 2021年10月20日09:31:35
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(SysConfigDecimal record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }
}
