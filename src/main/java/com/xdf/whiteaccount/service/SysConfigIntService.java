package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.SysConfigInt;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
public interface SysConfigIntService {
     int insert(SysConfigInt record) throws Exception;

     int insertSelective(SysConfigInt record) throws Exception;

     int multiInsert(List<SysConfigInt> list) throws Exception;

     int updateByPrimaryKey(SysConfigInt record) throws Exception;

     int updateByPrimaryKeySelective(SysConfigInt record) throws Exception;

     int update(SysConfigInt record) throws Exception;

     int multiUpdate(List<SysConfigInt> list) throws Exception;

     SysConfigInt selectByPrimaryKey(String id) throws Exception;

     int deleteByPrimaryKey(String id) throws Exception;

     List<SysConfigInt> listQuery(SysConfigInt record) throws Exception;
}