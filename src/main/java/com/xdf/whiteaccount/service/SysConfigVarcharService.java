package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.SysConfigVarchar;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-26 14:22:09
 */
public interface SysConfigVarcharService {
    int insert(SysConfigVarchar record) throws Exception;

    int insertSelective(SysConfigVarchar record) throws Exception;

    int multiInsert(List<SysConfigVarchar> list) throws Exception;

    int updateByPrimaryKey(SysConfigVarchar record) throws Exception;

    int updateByPrimaryKeySelective(SysConfigVarchar record) throws Exception;

    int update(SysConfigVarchar record) throws Exception;

    int multiUpdate(List<SysConfigVarchar> list) throws Exception;

    SysConfigVarchar selectByPrimaryKey(String id) throws Exception;

    int deleteByPrimaryKey(String id) throws Exception;

    List<SysConfigVarchar> listQuery(SysConfigVarchar record) throws Exception;
}