package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.dto.TreeDTO;
import com.xdf.whiteaccount.entity.SysMenu;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-01-03 14:31:54
 */
public interface SysMenuService {
    int insert(SysMenu record) throws Exception;

    int insertSelective(SysMenu record) throws Exception;

    int multiInsert(List<SysMenu> list) throws Exception;

    int updateByPrimaryKey(SysMenu record) throws Exception;

    int updateByPrimaryKeySelective(SysMenu record) throws Exception;

    int update(SysMenu record) throws Exception;

    int multiUpdate(List<SysMenu> list) throws Exception;

    SysMenu selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<SysMenu> listQuery(SysMenu record) throws Exception;

    List<TreeDTO> listQueryByParentId(Integer id) throws Exception;

}