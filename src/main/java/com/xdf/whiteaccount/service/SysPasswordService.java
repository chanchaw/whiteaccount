package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.SysPassword;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-06-21 15:19:40
 */
public interface SysPasswordService {
    int insert(SysPassword record) throws Exception;

    int insertSelective(SysPassword record) throws Exception;

    int multiInsert(List<SysPassword> list) throws Exception;

    int updateByPrimaryKey(SysPassword record) throws Exception;

    int updateByPrimaryKeySelective(SysPassword record) throws Exception;

    int update(SysPassword record) throws Exception;

    int multiUpdate(List<SysPassword> list) throws Exception;

    SysPassword selectByPrimaryKey(String id) throws Exception;

    int deleteByPrimaryKey(String id) throws Exception;

    List<SysPassword> listQuery(SysPassword record) throws Exception;
}