package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.SysPermission;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2020-12-31 14:42:38
 */
public interface SysPermissionService {
    int insert(SysPermission record) throws Exception;

    int insertSelective(SysPermission record) throws Exception;

    int multiInsert(List<SysPermission> list) throws Exception;

    int updateByPrimaryKey(SysPermission record) throws Exception;

    int updateByPrimaryKeySelective(SysPermission record) throws Exception;

    int update(SysPermission record) throws Exception;

    int multiUpdate(List<SysPermission> list) throws Exception;

    SysPermission selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    int deleteBySysMenuId(Integer id) throws Exception;

    int deleteByRoleId(Integer id) throws Exception;

    List<SysPermission> listQuery(SysPermission record) throws Exception;

    List<SysPermission> queryRoleId(Integer... id) throws Exception;
}