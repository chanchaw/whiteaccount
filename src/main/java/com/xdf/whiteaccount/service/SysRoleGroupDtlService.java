package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.SysRoleGroupDtl;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2020-12-31 14:42:38
 */
public interface SysRoleGroupDtlService {
    int insert(SysRoleGroupDtl record) throws Exception;

    int insertSelective(SysRoleGroupDtl record) throws Exception;

    int multiInsert(List<SysRoleGroupDtl> list) throws Exception;

    int updateByPrimaryKey(SysRoleGroupDtl record) throws Exception;

    int updateByPrimaryKeySelective(SysRoleGroupDtl record) throws Exception;

    int update(SysRoleGroupDtl record) throws Exception;

    int multiUpdate(List<SysRoleGroupDtl> list) throws Exception;

    SysRoleGroupDtl selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    int deleteByRoleGroupId(Integer id) throws Exception;

    int deleteByRoleId(Integer id) throws Exception;

    List<SysRoleGroupDtl> listQuery(SysRoleGroupDtl record) throws Exception;

    List<SysRoleGroupDtl> selectByRoleGroupId(Integer... id) throws Exception;
}