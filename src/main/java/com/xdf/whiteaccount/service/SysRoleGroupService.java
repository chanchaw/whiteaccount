package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.SysRoleGroup;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2020-12-31 14:42:38
 */
public interface SysRoleGroupService {
    int insert(SysRoleGroup record) throws Exception;

    int insertSelective(SysRoleGroup record) throws Exception;

    int multiInsert(List<SysRoleGroup> list) throws Exception;

    int updateByPrimaryKey(SysRoleGroup record) throws Exception;

    int updateByPrimaryKeySelective(SysRoleGroup record) throws Exception;

    int update(SysRoleGroup record) throws Exception;

    int multiUpdate(List<SysRoleGroup> list) throws Exception;

    SysRoleGroup selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<SysRoleGroup> listQuery(SysRoleGroup record) throws Exception;

    List<SysRoleGroup> queryByUserPk(Integer id) throws Exception;

}