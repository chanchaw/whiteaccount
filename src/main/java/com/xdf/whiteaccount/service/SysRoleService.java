package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.SysRole;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2020-12-31 14:42:38
 */
public interface SysRoleService {
    int insert(SysRole record) throws Exception;

    int insertSelective(SysRole record) throws Exception;

    int multiInsert(List<SysRole> list) throws Exception;

    int updateByPrimaryKey(SysRole record) throws Exception;

    int updateByPrimaryKeySelective(SysRole record) throws Exception;

    int update(SysRole record) throws Exception;

    int multiUpdate(List<SysRole> list) throws Exception;

    SysRole selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<SysRole> listQuery(SysRole record) throws Exception;

    List<SysRole> selectByRoleGroupId(Integer[] id) throws Exception;
}