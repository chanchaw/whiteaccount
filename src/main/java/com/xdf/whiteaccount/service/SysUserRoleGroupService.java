package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.SysUserRoleGroup;
import com.xdf.whiteaccount.utils.Example;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2020-12-31 14:42:38
 */
public interface SysUserRoleGroupService {
     int insert(SysUserRoleGroup record) throws Exception;

     int insertSelective(SysUserRoleGroup record) throws Exception;

     int multiInsert(List<SysUserRoleGroup> list) throws Exception;

     int updateByPrimaryKey(SysUserRoleGroup record) throws Exception;

     int updateByPrimaryKeySelective(SysUserRoleGroup record) throws Exception;

     int update(SysUserRoleGroup record) throws Exception;

     int multiUpdate(List<SysUserRoleGroup> list) throws Exception;

     SysUserRoleGroup selectByPrimaryKey(Integer id) throws Exception;

     int deleteByPrimaryKey(Integer id) throws Exception;

     int deleteByRoleGroupId(Integer id) throws Exception;

     int deleteByUserId(Integer id) throws Exception;

     int deleteByExample(Example example) throws Exception;

     List<SysUserRoleGroup> listQuery(SysUserRoleGroup record) throws Exception;

}