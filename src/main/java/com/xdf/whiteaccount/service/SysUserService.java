package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.dto.SysUserDTO;
import com.xdf.whiteaccount.entity.SysPermission;
import com.xdf.whiteaccount.entity.SysRole;
import com.xdf.whiteaccount.entity.SysUser;
import com.xdf.whiteaccount.utils.Example;
import com.xdf.whiteaccount.vo.SysUserVO;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2020-12-31 14:42:38
 */
public interface SysUserService {
    int insert(SysUser record) throws Exception;

    int insertSelective(SysUser record) throws Exception;

    int insertSelective(SysUserVO record) throws Exception;

    int multiInsert(List<SysUser> list) throws Exception;

    int updateByPrimaryKey(SysUser record) throws Exception;

    int updateByPrimaryKeySelective(SysUser record) throws Exception;

    int updateByPrimaryKeySelective(SysUserVO record) throws Exception;

    int updateByPrimaryKeySelectiveWithoutTransactional(SysUser record) throws Exception;

    int update(SysUser record) throws Exception;

    int multiUpdate(List<SysUser> list) throws Exception;

    SysUser selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<SysUser> listQuery(SysUser record) throws Exception;

    List<SysUser> listQueryByExample(Example example);

    SysUser selectByLoginName(String loginName) throws Exception;

    List<SysRole> selectRolesByUserId(Integer id) throws Exception;

    List<SysPermission> selectPermissionByUserId(Integer id) throws Exception;

    SysUserDTO selectCurrentLoginUser() throws Exception;

    Long count(Example example);

    List<SysUserVO> listQuery(Integer state) throws Exception;
}