package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.SysUserTableConfig;
import com.xdf.whiteaccount.utils.Example;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2020-12-31 15:23:24
 */
public interface SysUserTableConfigService {
    int insert(SysUserTableConfig record) throws Exception;

    int insertSelective(SysUserTableConfig record) throws Exception;

    int multiInsert(List<SysUserTableConfig> list) throws Exception;

    int updateByPrimaryKey(SysUserTableConfig record) throws Exception;

    int updateByPrimaryKeySelective(SysUserTableConfig record) throws Exception;

    int update(SysUserTableConfig record) throws Exception;

    int multiUpdate(List<SysUserTableConfig> list) throws Exception;

    SysUserTableConfig selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    int updateByExample(Example example, SysUserTableConfig config) throws Exception;

    List<SysUserTableConfig> listQuery(SysUserTableConfig record) throws Exception;

    List<SysUserTableConfig> selectBySysUserIdMenuId(Integer sysUserId, Integer sysMenuId) throws Exception;

    int updateBySysUserIdMenuId(SysUserTableConfig config) throws Exception;
}