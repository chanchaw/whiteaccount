package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.SysUserVisitList;

import java.util.List;
import java.util.Map;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-07-31 16:39:12
 */
public interface SysUserVisitListService {
    int insert(SysUserVisitList record) throws Exception;

    int insertSelective(SysUserVisitList record) throws Exception;

    int multiInsert(List<SysUserVisitList> list) throws Exception;

    int updateByPrimaryKey(SysUserVisitList record) throws Exception;

    int updateByPrimaryKeySelective(SysUserVisitList record) throws Exception;

    int update(SysUserVisitList record) throws Exception;

    int multiUpdate(List<SysUserVisitList> list) throws Exception;

    SysUserVisitList selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    int deleteByMenuId(Integer id) throws Exception;

    List<SysUserVisitList> listQuery(SysUserVisitList record) throws Exception;

    List<Map<String, Object>> queryByUserId(Integer userId);
}