package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.TakeDeliveryAddress;

import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-22 14:01:02
 */
public interface TakeDeliveryAddressService {
    int insert(TakeDeliveryAddress record) throws Exception;

    int insertSelective(TakeDeliveryAddress record) throws Exception;

    int multiInsert(List<TakeDeliveryAddress> list) throws Exception;

    int updateByPrimaryKey(TakeDeliveryAddress record) throws Exception;

    int updateByPrimaryKeySelective(TakeDeliveryAddress record) throws Exception;

    int update(TakeDeliveryAddress record) throws Exception;

    int multiUpdate(List<TakeDeliveryAddress> list) throws Exception;

    TakeDeliveryAddress selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<TakeDeliveryAddress> listQuery(TakeDeliveryAddress record) throws Exception;
}