package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.WeightOfFabric;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
public interface WeightOfFabricService {
     int insert(WeightOfFabric record) throws Exception;

     int insertSelective(WeightOfFabric record) throws Exception;

     int multiInsert(List<WeightOfFabric> list) throws Exception;

     int updateByPrimaryKey(WeightOfFabric record) throws Exception;

     int updateByPrimaryKeySelective(WeightOfFabric record) throws Exception;

     int update(WeightOfFabric record) throws Exception;

     int multiUpdate(List<WeightOfFabric> list) throws Exception;

     WeightOfFabric selectByPrimaryKey(Long id) throws Exception;

     int deleteByPrimaryKey(Long id) throws Exception;

     List<WeightOfFabric> listQuery(WeightOfFabric record) throws Exception;
}