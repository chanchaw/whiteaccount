package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.Whitebillfh;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-22 08:16:02
 */
public interface WhitebillfhService {
    int insert(Whitebillfh record) throws Exception;

    int insertSelective(Whitebillfh record) throws Exception;

    int multiInsert(List<Whitebillfh> list) throws Exception;

    int updateByPrimaryKey(Whitebillfh record) throws Exception;

    int updateByPrimaryKeySelective(Whitebillfh record) throws Exception;

    int update(Whitebillfh record) throws Exception;

    int multiUpdate(List<Whitebillfh> list) throws Exception;

    Whitebillfh selectByPrimaryKey(Integer id) throws Exception;

    int deleteByPrimaryKey(Integer id) throws Exception;

    List<Whitebillfh> listQuery(Whitebillfh record) throws Exception;
}