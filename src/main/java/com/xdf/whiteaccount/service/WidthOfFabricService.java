package com.xdf.whiteaccount.service;

import com.xdf.whiteaccount.entity.WidthOfFabric;
import java.util.List;

/**
 * @Description : 业务层接口,该类为生成器自动生成.
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
public interface WidthOfFabricService {
     int insert(WidthOfFabric record) throws Exception;

     int insertSelective(WidthOfFabric record) throws Exception;

     int multiInsert(List<WidthOfFabric> list) throws Exception;

     int updateByPrimaryKey(WidthOfFabric record) throws Exception;

     int updateByPrimaryKeySelective(WidthOfFabric record) throws Exception;

     int update(WidthOfFabric record) throws Exception;

     int multiUpdate(List<WidthOfFabric> list) throws Exception;

     WidthOfFabric selectByPrimaryKey(Long id) throws Exception;

     int deleteByPrimaryKey(Long id) throws Exception;

     List<WidthOfFabric> listQuery(WidthOfFabric record) throws Exception;
}