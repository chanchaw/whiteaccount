package com.xdf.whiteaccount.service.base;

/**
 * @program: white-account
 * @description: 审核与反审核
 * @author: 张柯
 * @create: 2021-07-22 13:24
 **/
public interface BaseAuditService {
    int auditById(Long[] id) throws Exception;

    int cancelAuditById(Long[] id) throws Exception;
}
