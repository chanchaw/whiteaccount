package com.xdf.whiteaccount.service.base;

import com.xdf.whiteaccount.dao.SysUserMapper;
import com.xdf.whiteaccount.entity.SysUser;
import com.xdf.whiteaccount.exception.NotallowException;
import com.xdf.whiteaccount.vo.PlanTemplateVO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-05-17 08:24
 **/
@Component
public class BaseService {
    private SysUserMapper sysUserMapper;

    @Autowired
    public void setSysUserMapper(SysUserMapper sysUserMapper) {
        this.sysUserMapper = sysUserMapper;
    }

    /**
     * 获取当前用户编号
     *
     * @return
     */
    protected Integer getLoginUserId() {
        String userLoginName = String.valueOf(SecurityUtils.getSubject().getPrincipal());
        List<SysUser> userList = sysUserMapper.selectByParam(SysUser.builder().loginName(userLoginName).build());
        if (CollectionUtils.isEmpty(userList)) throw new NotallowException("无法找到当前用户！");
        return userList.stream().findFirst().orElse(SysUser.builder().build()).getId();
    }

    /**
     * 获取当前用户编号
     *
     * @return
     */
//    protected String setProductName() {
//        String userLoginName = String.valueOf(SecurityUtils.getSubject().getPrincipal());
//        List<SysUser> userList = sysUserMapper.selectByParam(SysUser.builder().loginName(userLoginName).build());
//        if (CollectionUtils.isEmpty(userList)) throw new NotallowException("无法找到当前用户！");
//        return userList.stream().findFirst().orElse(SysUser.builder().build()).getId();
//    }

    /**
     * 获取登陆者姓名
     *
     * @return
     */
    protected String getLoginUserName() {
        SysUser user = sysUserMapper.selectByPrimaryKey(getLoginUserId());
        return Optional.ofNullable(user).map(SysUser::getUserName).orElse("");
    }

    /**
     * 是否有某角色权限
     *
     * @param roleName
     * @return
     */
    protected boolean hasRole(String roleName) {
        return SecurityUtils.getSubject().hasRole(roleName);
    }

    /**
     * 是否有某权限
     *
     * @param permission
     * @return
     */
    protected boolean hasPermission(String permission) {
        return SecurityUtils.getSubject().isPermitted(permission);
    }

    /**
     * 是否拥有权限
     *
     * @param permission
     * @return
     */
    protected boolean hasPermission(String... permission) {
        return SecurityUtils.getSubject().isPermittedAll(permission);
    }

    /**
     * 获取请求中的数组
     *
     * @param request
     * @return
     */
    protected Map<String, Integer> conventToMap(HttpServletRequest request) {
        Enumeration<String> keys = request.getParameterNames();
        Map<String, Integer> map = new HashMap<>();
        while (keys.hasMoreElements()) {
            String name = keys.nextElement();
            String value = request.getParameter(name);
            Integer v = null;
            try {
                v = Integer.parseInt(value);
            } catch (Exception e) {
                continue;
            }
            if (v != null)
                map.put(name, v);
        }
        return map;
    }

    /**
     * 是否Excel
     *
     * @param fileName
     * @return
     */
    protected boolean isExcel(String fileName) {
        return !StringUtils.isEmpty(fileName) && (fileName.contains(".xls") || fileName.contains(".xlsx"));
    }
}
