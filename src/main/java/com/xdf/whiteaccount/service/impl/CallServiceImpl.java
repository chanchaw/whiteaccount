package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.dao.CallMapper;
import com.xdf.whiteaccount.enums.ArgumentConstant;
import com.xdf.whiteaccount.enums.PrefixEnum;
import com.xdf.whiteaccount.service.CallService;
import com.xdf.whiteaccount.vo.DyeFabricPlanDetailVO;
import com.xdf.whiteaccount.vo.PlanDetailVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-05-19 08:25
 **/
@Service
public class CallServiceImpl implements CallService {
    private CallMapper dao;

    @Autowired
    public void setDao(CallMapper dao) {
        this.dao = dao;
    }

    /**
     * 计划单号生成
     *
     * @return
     */
    @Override
    public String planCodeGen() {
        return dao.planCodeGen(PrefixEnum.MTP.name());
    }

    /**
     * 色布编号生成
     *
     * @return
     */
    @Override
    public String dyeFabricPlanCodeGen() {
        return dao.dyeFabricplanCodeGen(PrefixEnum.MCP.name());
    }

    /**
     * 原料编号身体工程存错过程
     *
     * @return
     */
    @Override
    public String materialOrderCodeGen(int accessMode) {
        String suffix;
        if (accessMode > 0) {
            suffix = PrefixEnum.YL.name();
        } else {
            suffix = PrefixEnum.LL.name();
        }
        return dao.materialOrderCodeGen(suffix);
    }

    /**
     * 色布入库单据编号生成
     *
     * @return
     * @throws Exception
     */
    @Override
    public String dyeFabricInboundCodeGen(boolean isFinishedProduct) throws Exception {
        String suffix;
        if (isFinishedProduct) {
            suffix = PrefixEnum.SS.name();
        } else {
            suffix = PrefixEnum.CPRK.name();
        }
        return dao.dyeFabricInboundCodeGen(suffix);
    }

    /**
     * 外发入库单据编号
     *
     * @param isFinishedProduct 是否成品
     * @return
     */
    @Override
    public String productOutwardOrderCodeGen(boolean isFinishedProduct) {
        String suffix = isFinishedProduct ? PrefixEnum.JB.name() : PrefixEnum.F.name();
        return dao.productOutwardOrderCodeGen(suffix);
    }

    /**
     * 计划单序时表
     *
     * @param start
     * @param end
     * @param markFinished
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getPlanReport(Date start, Date end, Boolean markFinished, String Strcolumn, String Strvalue) throws Exception {
        if (start == null) {
            start = new Date();
        }
        if (end == null) {
            end = new Date();
        }

        if( markFinished == null ) markFinished = false;
        if( Strcolumn == null ) Strcolumn ="";
        if( Strvalue == null ) Strvalue ="";
        return dao.getPlanReport(start, end, markFinished, Strcolumn, Strvalue);
    }

    /**
     * 计划单明细
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public PlanDetailVO getPlanDtl(Long id) throws Exception {
        PlanDetailVO vo = dao.getPlanDtl(id);
        if (vo == null) return null;
        vo.setSendDtl(dao.getJrkbillfh(id));
        vo.setInputDtl(dao.getJrkbillDtl(id));
        vo.setStorageDtl(dao.getPlanStorageModify(id));
        return vo;
    }

    /**
     * 获取打卷数据
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getJrkbillDtl(Long id) throws Exception {
        return dao.getJrkbillDtl(id);
    }

    /**
     * 获取发货数据
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getJrkbillfh(Long id) throws Exception {
        return dao.getJrkbillfh(id);
    }

    /**
     * 原料管理 - 第一个标签页 - 原料入库
     *
     * @param start        起始日期
     * @param end          终止日期
     * @param supplierName 供应商
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getMaterialOrderReport(Date start, Date end, String supplierName) throws Exception {
        return dao.getMaterialOrderReport(start, end, supplierName);
    }

    /**
     * 收货管理 - 应收应付 - 已收款
     *
     * @param start
     * @param end
     * @param clientName
     * @param checkType
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getFHhistoryDetail(Date start, Date end, String clientName, String checkType) throws Exception {
        if (StringUtils.isEmpty(checkType)) {
            checkType = ArgumentConstant.ALL_CLIENT;
        }
        if (StringUtils.isEmpty(clientName)) {
            clientName = ArgumentConstant.ALL_CLIENT;
        }
        return dao.getFHhistoryDetail(start, end, clientName, checkType);
    }

    /**
     * 应收应付 - 应收管理 - 应收账款
     *
     * @param start
     * @param end
     * @param clientName
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getReceivablesDetail(Date start, Date end, String clientName, Integer value) throws Exception {
        if (StringUtils.isEmpty(clientName)) {
            clientName = ArgumentConstant.ALL_CLIENT;
        }
        return dao.getReceivablesDetail(start, end, clientName, value);
    }

    /**
     * 因收款汇总
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getReceivablesAllSum() throws Exception {
        return dao.getReceivablesAllSum();
    }

    /**
     * 应收应付 - 应收管理 - 已收账款
     *
     * @param start
     * @param end
     * @param clientName
     * @param value
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getCollectionOld(Date start, Date end, String clientName, String value) throws Exception {
        if (start == null) start = new Date();
        if (end == null) end = new Date();
        if (StringUtils.isEmpty(clientName)) {
            clientName = ArgumentConstant.ALL_CLIENT;
        }
        if (StringUtils.isEmpty(value)) {
            value = ArgumentConstant.DEFAULT_SUMMARIZE;
        }
        return dao.getCollectionOld(start, end, clientName, value);
    }

    /**
     * 获取白坯库存调整
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getPlanStorageModify(Long id) throws Exception {
        return dao.getPlanStorageModify(id);
    }

    /**
     * 获取白坯入库数
     *
     * @param start      起始日期
     * @param end        终止日期
     * @param clientName 客户
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getGreyfabricInput(Date start, Date end, String clientName) throws Exception {
        List<Map<String, Object>> ds = dao.getGreyfabricInput(start, end);
        if (StringUtils.isNotEmpty(clientName)) {
            return Optional.ofNullable(ds).orElse(new ArrayList<>())
                    .stream().filter(item -> item.get(ArgumentConstant.CLIENT_FIELD) != null && String.valueOf(item.get(ArgumentConstant.CLIENT_FIELD)).equals(clientName))
                    .collect(Collectors.toList());
        }
        return ds;
    }

    /**
     * 获取白坯库存
     *
     * @param mode
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getGreyfabricStorage(String clientName, int mode,Date end) throws Exception {
        List<Map<String, Object>> ds = Optional.ofNullable(dao.getGreyfabricStorage(mode <= 0 ? 0 : 1,end))
                .orElse(new ArrayList<>());
        if (StringUtils.isNotEmpty(clientName)) {
            return ds.stream().filter(x -> x.get(ArgumentConstant.CLIENT_FIELD) != null && String.valueOf(x.get(ArgumentConstant.CLIENT_FIELD)).equals(clientName))
                    .collect(Collectors.toList());
        }
        return ds;
    }

    /**
     * 色布计划序时表
     *
     * @param start        起始日期
     * @param end          终止日期
     * @param markFinished 完工标记
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getDyeFabricPlanReport(Date start, Date end, Boolean markFinished, String Strcolumn, String Strvalue) throws Exception {
        if (start == null) start = new Date();
        if (end == null) end = new Date();
        if( markFinished == null ) markFinished = false;
        if( Strcolumn == null ) Strcolumn ="";
        if( Strvalue == null ) Strvalue ="";
        List<Map<String, Object>> list = Optional.ofNullable(dao.getDyeFabricPlanReport(start, end, markFinished, Strcolumn, Strvalue)).orElse(new ArrayList<>());
        return list;
    }

    /**
     * 色布发货存储过程
     *
     * @param start      起始日期
     * @param end        终止日期
     * @param clientName 客户名
     * @param checkout   结账类型
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getDyeFabricDelivery(Date start, Date end, String clientName, String checkout) throws Exception {
        if (start == null) start = new Date();
        if (end == null) end = new Date();
        return dao.getDyeFabricDelivery(start, end, clientName, checkout);
    }

    /**
     * 色布计划明细
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public DyeFabricPlanDetailVO getDyePlanDtl(Long id) throws Exception {
        if (id == null) return null;
        return dao.getDyePlanDtl(id);
    }

    /**
     * 色布入库
     *
     * @param start      起始日期
     * @param end        终止日期
     * @param clientName 客户
     * @return
     */
    @Override
    public List<Map<String, Object>> getDyeInboundReport(Date start, Date end, String clientName) {
        if (start == null) start = new Date();
        if (end == null) end = new Date();
        return dao.getDyeInboundReport(start, end, clientName);
    }

    /**
     * 色布库存
     *
     * @param clientName 客户名
     * @param value      是否汇总(0:明细;1:汇总;)
     * @return
     */
    @Override
    public List<Map<String, Object>> getDyeStorageReport(String clientName, Integer value) {
        return dao.getDyeStorageReport(clientName, value);
    }

    /**
     * 原料管理 - 第一个标签页 - 原料入库
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getMaterialStorage(String supplierName) throws Exception {
        return dao.getMaterialStorage(supplierName);
    }

    /**
     * 原料出库
     *
     * @param start
     * @param end
     * @param supplierName
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getMaterialOutbound(Date start, Date end, String supplierName) throws Exception {
        return dao.getMaterialOutbound(start, end, supplierName);
    }

    /**
     * 应收应付 - 应付管理 - 应付款
     *
     * @param start      起始日期
     * @param end        终止日期
     * @param clientName 供应商名称
     * @param sign       是否汇总
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getAccountPayable(Date start, Date end, String clientName, Integer sign) throws Exception {
        if (start == null) start = new Date();
        if (end == null) end = new Date();
        if (StringUtils.isEmpty(clientName)) clientName = "";
        if (sign == null) sign = 0;
        return dao.getAccountPayable(start, end, clientName, sign);
    }

    /**
     * 应收应付 - 应付管理 - 已付款
     *
     * @param start
     * @param end
     * @param supplierName
     * @param value
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getAccountPayment(Date start, Date end, String supplierName, Integer value) throws Exception {
        if (start == null) start = new Date();
        if (end == null) end = new Date();
        if (StringUtils.isEmpty(supplierName)) supplierName = "";
        if (value == null) value = 0;
        return dao.getAccountPayment(start, end, supplierName, value);
    }

    /**
     * 原料外发
     *
     * @param start       起始日期
     * @param end         终止日期
     * @param processName 加工户
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getMaterialOutward(Date start, Date end, String processName) throws Exception {
        if (start == null) start = new Date();
        if (end == null) end = new Date();
        if (StringUtils.isEmpty(processName)) processName = "";
        return dao.getMaterialOutward(start, end, processName);
    }

    /**
     * 获取外发入库
     *
     * @param start       起始日期
     * @param end         终止日期
     * @param processName 加工户
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getProductInboundReport(Date start, Date end, String processName) throws Exception {
        return dao.getProductInboundReport(start, end, processName);
    }

    /**
     * 获取未剖幅报表
     *
     * @param start
     * @param end
     * @param clientName
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getUnfinishedOutward(Date start, Date end, String clientName) throws Exception {
        return dao.getUnfinishedOutward(start, end, clientName);
    }

    /**
     * 加工户对账单
     *
     * @param start       起始日期
     * @param end         终止日期
     * @param processName 加工户
     * @param mode        1：汇总；0：明细
     * @return
     */
    @Override
    public List<Map<String, Object>> getProcesscompanyAccount(Date start, Date end, String processName, Integer mode) {
        if (mode == null) mode = 0;
        if (end == null) start = new Date();
        if (end == null) end = new Date();
        if (processName != null) {
            processName = processName.trim();
        }
        return dao.getProcesscompanyAccount(start, end, processName, mode);
    }

    /**
     * @return
     */
    @Override
    public List<Map<String, Object>> getCanceledPlan() {
        return dao.getCanceledPlan();
    }

    /**
     * @return
     */
    @Override
    public List<Map<String, Object>> getCanceledDyeFabricPlan() {
        return dao.getCanceledDyeFabricPlan();
    }

    /**
     * 获取白坯废布入库
     *
     * @param start
     * @param end
     * @param clientName
     * @return
     * @throws Exception
     */
    @Override
    public List<Map<String, Object>> getProductWaste(Date start, Date end, String clientName) throws Exception {
        return dao.getProductWaste(start, end, clientName);
    }

    /**
     * 白坯产量明细表存储过程
     * @param start         起始日期
     * @param end           终止日期
     * @param machineNum    机台号
     * @return
     */
    @Override
    public List<Map<String, Object>> getGreyFabricJrkbillReport(Date start, Date end, String machineNum) {
        return dao.getGreyFabricJrkbillReport(start, end, machineNum);
    }

    /**
     * 计划单存储过程
     * @param state         状态
     * @param markFinished  标记
     * @return
     */
    @Override
    public List<Map<String, Object>> getPlanReportDtl(Integer state, Boolean markFinished) {
        return dao.getPlanReportDtl(state, markFinished);
    }

    @Override
    public List<Map<String, Object>> getUnImportedJRK(String processCompname) {
        return dao.getUnImportedJRK(processCompname);
    }

    // 白坯计划 的序时表中移库功能中弹出模态窗表格的数据源，排除指定的计划单后显示所有未完工的白坯计划
    // 查询指定时间段内的所有未完工的白坯计划，之后排除当前指定主键的一行，返回其他数据
    @Override
    public List<Map<String, Object>> getPlanReportExclude(Date start, Date end, Long id) throws Exception {
        if( id == null || id<=0 ) throw new RuntimeException("传入了空的主键，无法排除指定计划单！");
        List<Map<String,Object>> list = getPlanReport(start,end,false,"","");
        if( list.size()<=0 ) return list;
        List<Map<String, Object>> ret = list.stream().filter(item -> !id.equals((Long)item.get("id"))).collect(Collectors.toList());
        return ret;
    }

    /**
     * 白坯产量汇总表存储过程
     * @param start         起始日期
     * @param end           终止日期
     * @return
     */
    @Override
    public List<Map<String, Object>> getGreySummaryJrkbillReport(Date start, Date end) {
        return dao.getPlanReportSummary(start, end);
    }
}

