package com.xdf.whiteaccount.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.ContactCompany;
import java.util.List;
import com.xdf.whiteaccount.dao.ContactCompanyMapper;
import com.xdf.whiteaccount.service.ContactCompanyService;

 /**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
@Service
public class ContactCompanyServiceImpl implements ContactCompanyService{
@Autowired
private ContactCompanyMapper dao;

     /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(ContactCompany record) throws Exception{
        String category = record.getCompanyCategories();
        if( category == null || category.length() <= 0 ) record.setCompanyCategories("全部");
        return dao.insert(record);
    }

     /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(ContactCompany record) throws Exception{
        String category = record.getCompanyCategories();
        if( category == null || category.length() <= 0 ) record.setCompanyCategories("全部");
        return dao.insertSelective(record);
    }

     /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<ContactCompany> list) throws Exception{
        for(int i=0;i<list.size();i++){
            String category = list.get(i).getCompanyCategories();
            if( category == null || category.length() <= 0 ) list.get(i).setCompanyCategories("全部");
        }
        return dao.multiInsert(list);
    }

     /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(ContactCompany record) throws Exception{
        return dao.updateByPrimaryKey(record);
    }

     /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(ContactCompany record) throws Exception{
        return dao.updateByPrimaryKeySelective(record);
    }

     /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(ContactCompany record) throws Exception{
        return dao.updateByPrimaryKey(record);
    }

     /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<ContactCompany> list) throws Exception{
        return dao.multiUpdate(list);
    }

     /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    public ContactCompany selectByPrimaryKey(String id) throws Exception{
        return dao.selectByPrimaryKey(id);
    }

     /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(String id) throws Exception{
        return dao.deleteByPrimaryKey(id);
    }

     /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    public List<ContactCompany> listQuery(ContactCompany record) throws Exception{
        return dao.selectByParam(record);
    }
}