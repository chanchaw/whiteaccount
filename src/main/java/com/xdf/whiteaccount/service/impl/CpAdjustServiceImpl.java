package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.dao.CallMapper;
import com.xdf.whiteaccount.enums.ModifyTypeEnum;
import com.xdf.whiteaccount.service.base.BaseService;
import com.xdf.whiteaccount.utils.CalculateUtil;
import com.xdf.whiteaccount.vo.DyeFabricPlanDetailVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.CpAdjust;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import com.xdf.whiteaccount.dao.CpAdjustMapper;
import com.xdf.whiteaccount.service.CpAdjustService;
import org.springframework.util.Assert;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-06-25 16:52:50
 */
@Service
@Slf4j
public class CpAdjustServiceImpl extends BaseService implements CpAdjustService {
    @Autowired
    private CpAdjustMapper dao;
    @Autowired
    private CallMapper callMapper;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-06-25 16:52:50
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(CpAdjust record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-06-25 16:52:50
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(CpAdjust record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-06-25 16:52:50
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<CpAdjust> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-25 16:52:50
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(CpAdjust record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-06-25 16:52:50
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(CpAdjust record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-25 16:52:50
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(CpAdjust record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-06-25 16:52:50
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<CpAdjust> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-06-25 16:52:50
     */
    @Override
    public CpAdjust selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-06-25 16:52:50
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-06-25 16:52:50
     */
    @Override
    public List<CpAdjust> listQuery(CpAdjust record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 自动调整库存
     *
     * @param record 更新实体
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int storeModifyByPlanId(CpAdjust record) throws Exception {
        DyeFabricPlanDetailVO vo = callMapper.getDyePlanDtl(record.getPlanId());
        Assert.notNull(vo, "计划单不存在！");
        Integer ps = (int) CalculateUtil.sub(record.getPairs(),
                Optional.ofNullable(vo).map(DyeFabricPlanDetailVO::getRemainPairs).orElse(new BigDecimal(0)).doubleValue());
        BigDecimal qty = CalculateUtil.sub(record.getQty(), vo.getRemainKilo());
        if (ps == 0 && qty.doubleValue() == 0D) return 0;
        CpAdjust item = CpAdjust.builder()
                .billType(ModifyTypeEnum.CHECKOUT.getName())
                .cancelTime(new Date())
                .cancelUserId(getLoginUserId())
                .isPush(false)
                .upload(0)
                .billDate(new Date())
                .pairs(ps)
                .qty(qty)
                .planId(record.getPlanId())
                .build();
        return dao.insertSelective(item);
    }
}