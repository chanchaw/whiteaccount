package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.config.properties.DbConfig;
import com.xdf.whiteaccount.dao.CustomizeMapper;
import com.xdf.whiteaccount.dao.SysPasswordMapper;
import com.xdf.whiteaccount.entity.SysPassword;
import com.xdf.whiteaccount.enums.PasswordFieldEnum;
import com.xdf.whiteaccount.service.CustomizeService;
import com.xdf.whiteaccount.service.base.BaseService;
import com.xdf.whiteaccount.utils.BackupUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.File;
import java.util.Optional;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-06-12 10:12
 **/
@Service
public class CustomizeServiceImpl extends BaseService implements CustomizeService {
    @Autowired
    private CustomizeMapper customizeMapper;
    @Autowired
    private SysPasswordMapper sysPasswordMapper;
    @Autowired
    private DbConfig dbConfig;

    /**
     * 获取当前数据库信息
     *
     * @return
     */
    @Override
    public String getDataBase() {
        return customizeMapper.getDataBase();
    }

    /**
     * 清空当前数据库全部信息(配置除外)
     *
     * @return
     */
    @Override
    public int clearAllData() {
        return customizeMapper.clearAllData();
    }

    /**
     * 清空所有基础资料
     *
     * @return
     */
    @Override
    public int clearBasicData() {
        return customizeMapper.clearBasicData();
    }

    /**
     * 清空所有单据
     *
     * @return
     */
    @Override
    public int clearDocument() {
        return customizeMapper.clearDocument();
    }

    /**
     * 备份数据源
     *
     * @return
     */
    @Override
    public boolean backup() {
        String database = customizeMapper.getDataBase();
        File jar = new ApplicationHome(getClass()).getSource();
        File backupFile = new File(jar.getParent() + File.separator + "backup" + File.separator + database);
        if (!backupFile.exists()) {
            backupFile.mkdirs();
        }
        return BackupUtil.backup(dbConfig.getHost(), String.valueOf(dbConfig.getPort()),
                database, dbConfig.getUsername(), dbConfig.getPassword(),
                backupFile.getPath() + File.separator + BackupUtil.backupFileNameCreator("", ".sql"));
    }

    /**
     * 备份数据库并且清空数据
     *
     * @param type
     * @param password
     * @return
     */
    @Override
    public boolean backupAndReset(String[] type, String password) {
        String pwd = Optional.ofNullable(sysPasswordMapper.selectByPrimaryKey(PasswordFieldEnum.PASSWORD_FIELD.getField()))
                .map(SysPassword::getSysPassword).orElse("");
        Assert.state(pwd.equals(password), "密码错误！");
        Assert.state(backup(), "备份数据库失败！");
        for (String t : type) {
            switch (t) {
                case "0":
                    clearBasicData();
                    break;
                case "1":
                    clearDocument();
                    break;
            }
        }
        return true;
    }
}
