package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.dto.TreeDTO;
import com.xdf.whiteaccount.utils.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.Department;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.xdf.whiteaccount.dao.DepartmentMapper;
import com.xdf.whiteaccount.service.DepartmentService;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-05-19 20:26:32
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    private DepartmentMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-05-19 20:26:32
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(Department record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-05-19 20:26:32
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(Department record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-05-19 20:26:32
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<Department> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-19 20:26:32
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(Department record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-05-19 20:26:32
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(Department record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-19 20:26:32
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(Department record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-05-19 20:26:32
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<Department> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-05-19 20:26:32
     */
    @Override
    public Department selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-05-19 20:26:32
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-05-19 20:26:32
     */
    @Override
    public List<Department> listQuery(Department record) throws Exception {
        return dao.selectByParam(record);
    }

    @Override
    public List<TreeDTO> listQueryTreeNodes(Integer parentId) throws Exception {
        List<Department> list;
        if (parentId == null) {
            list = dao.selectByExample(new Example().andIsNull("parent_id"));
        } else {
            list = dao.selectByExample(new Example().andEq("parent_id", parentId));
        }
        if (list == null) return null;
        List<TreeDTO> treeDTOList = new ArrayList<>();
        for (Department department : list) {
            TreeDTO treeDTO = new TreeDTO();
            long count = dao.countByExample(new Example().andEq("parent_id", department.getId()));
            if (count > 0) {
                treeDTO.setChildren(listQueryTreeNodes(department.getId()));
            }
            treeDTO.setText(department.getDName());
            treeDTO.setId(department.getId());
            Map<String, Object> map = new LinkedHashMap<>();
            Boolean def = department.getDDefault();
            map.put("dDefault", def == null ? false : def);
            map.put("dCode", department.getDCode());
            treeDTO.setAttribute(map);
            treeDTOList.add(treeDTO);
        }
        return treeDTOList;
    }
}