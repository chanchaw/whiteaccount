package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.service.CallService;
import com.xdf.whiteaccount.service.base.BaseService;
import com.xdf.whiteaccount.utils.Example;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.DyeFabricInbound;
import java.util.List;
import com.xdf.whiteaccount.dao.DyeFabricInboundMapper;
import com.xdf.whiteaccount.service.DyeFabricInboundService;
import org.springframework.util.Assert;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-07-20 14:42:34
 */
@Service
public class DyeFabricInboundServiceImpl extends BaseService implements DyeFabricInboundService {
    @Autowired
    private DyeFabricInboundMapper dao;
    @Autowired
    private CallService callService;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-07-20 14:42:34
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(DyeFabricInbound record) throws Exception {
        int res = dao.insert(record);
        if (record.getIsSendDirectly()) {
            DyeFabricInbound copy = (DyeFabricInbound) record.clone();
            copy.setOrderCode(callService.dyeFabricInboundCodeGen(true));
            copy.setLinkedOrderCode(record.getOrderCode());
            copy.setId(null);
            res += dao.insert(copy);
        }
        return res;
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-07-20 14:42:34
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(DyeFabricInbound record) throws Exception {
        int res = dao.insertSelective(record);
        if (record.getIsSendDirectly()) {
            DyeFabricInbound copy = (DyeFabricInbound) record.clone();
            copy.setOrderCode(callService.dyeFabricInboundCodeGen(true));
            copy.setLinkedOrderCode(record.getOrderCode());
            copy.setId(null);
            res += dao.insertSelective(copy);
        }
        return res;
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-07-20 14:42:34
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<DyeFabricInbound> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-07-20 14:42:34
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(DyeFabricInbound record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-07-20 14:42:34
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(DyeFabricInbound record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-07-20 14:42:34
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(DyeFabricInbound record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-07-20 14:42:34
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<DyeFabricInbound> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-07-20 14:42:34
     */
    @Override
    public DyeFabricInbound selectByPrimaryKey(Long id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * 根据id主键审核
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int auditById(Long[] id) throws Exception {
        return dao.updateByExampleSelective(DyeFabricInbound.builder().isAudit(true).auditUserId(getLoginUserId()).build(), new Example().andIn("id", id));
    }

    /**
     * 根据主键取消审核
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int cancelAuditById(Long[] id) throws Exception {
        return dao.updateByExampleSelective(DyeFabricInbound.builder().isAudit(false).auditUserId(0).build(), new Example().andIn("id", id));
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-07-20 14:42:34
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Long id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * 根据主键作废,同时作废对应的出库单或入库单
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int cancelByPrimaryKey(Long id) throws Exception {
        DyeFabricInbound record = dao.selectByPrimaryKey(id);
        Assert.notNull(record, ResponseEnum.CANCEL_ORDER.getName());
        int res = 0;
        res += dao.updateByPrimaryKeySelective(DyeFabricInbound.builder().id(record.getId()).state(0).build());
        if (StringUtils.isEmpty(record.getLinkedOrderCode())) {
            res += dao.updateByExampleSelective(DyeFabricInbound.builder().state(0).build(), new Example().andEq("linked_order_code", record.getOrderCode()));
        } else {
            res += dao.updateByExampleSelective(DyeFabricInbound.builder().state(0).build(), new Example().andEq("order_code", record.getLinkedOrderCode()));
        }
        return res;
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-07-20 14:42:34
     */
    @Override
    public List<DyeFabricInbound> listQuery(DyeFabricInbound record) throws Exception {
        return dao.selectByParam(record);
    }
}