package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.service.CallService;
import com.xdf.whiteaccount.service.base.BaseService;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.DyeFabricPlanTemplate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import com.xdf.whiteaccount.dao.DyeFabricPlanTemplateMapper;
import com.xdf.whiteaccount.service.DyeFabricPlanTemplateService;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-06-24 10:06:23
 */
@Service
public class DyeFabricPlanTemplateServiceImpl extends BaseService implements DyeFabricPlanTemplateService {
    @Autowired
    private DyeFabricPlanTemplateMapper dao;
    @Autowired
    private CallService callService;


    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-06-24 10:06:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(DyeFabricPlanTemplate record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-06-24 10:06:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(DyeFabricPlanTemplate record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-06-24 10:06:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<DyeFabricPlanTemplate> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-24 10:06:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(DyeFabricPlanTemplate record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-06-24 10:06:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(DyeFabricPlanTemplate record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-24 10:06:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(DyeFabricPlanTemplate record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-06-24 10:06:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<DyeFabricPlanTemplate> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-06-24 10:06:23
     */
    @Override
    public DyeFabricPlanTemplate selectByPrimaryKey(Long id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-06-24 10:06:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Long id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-06-24 10:06:23
     */
    @Override
    public List<DyeFabricPlanTemplate> listQuery(DyeFabricPlanTemplate record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 根据模板参数更新
     *
     * @param record
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateSelectiveDefault(DyeFabricPlanTemplate record) {
        record.setId(null);
        DyeFabricPlanTemplate dyeFabricPlanTemplate = Optional.ofNullable(
                dao.selectByParam(DyeFabricPlanTemplate.builder().setDefault(true).build()))
                .orElse(new ArrayList<>()).stream().findFirst().orElse(null);
        record.setSetDefault(true);
        if (dyeFabricPlanTemplate == null) {
            return dao.insert(record);
        } else {
            record.setId(dyeFabricPlanTemplate.getId());
            return dao.updateByPrimaryKey(record);
        }
    }

    /**
     * 查询默认模板,发送到前端
     *
     * @return
     */
    @Override
    public DyeFabricPlanTemplate selectDefaultTemplate() {
        DyeFabricPlanTemplate record = Optional.ofNullable(
                dao.selectByParam(DyeFabricPlanTemplate.builder().setDefault(true).build()))
                .orElse(new ArrayList<>()).stream().findFirst().orElse(new DyeFabricPlanTemplate());
        record.setPlanDate(new Date());
        record.setSubmitDate(DateUtils.addDays(new Date(), 14));
        record.setCreateUserId(getLoginUserId());
        record.setPlanCode(callService.dyeFabricPlanCodeGen());
        return record;
    }
}