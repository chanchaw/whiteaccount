package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.DyeSpecification;
import java.util.List;
import com.xdf.whiteaccount.dao.DyeSpecificationMapper;
import com.xdf.whiteaccount.service.DyeSpecificationService;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-06-26 08:36:35
 */
@Service
public class DyeSpecificationServiceImpl extends BaseService implements DyeSpecificationService {
    @Autowired
    private DyeSpecificationMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-06-26 08:36:35
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(DyeSpecification record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-06-26 08:36:35
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(DyeSpecification record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-06-26 08:36:35
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<DyeSpecification> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-26 08:36:35
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(DyeSpecification record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-06-26 08:36:35
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(DyeSpecification record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-26 08:36:35
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(DyeSpecification record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-06-26 08:36:35
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<DyeSpecification> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-06-26 08:36:35
     */
    @Override
    public DyeSpecification selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-06-26 08:36:35
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-06-26 08:36:35
     */
    @Override
    public List<DyeSpecification> listQuery(DyeSpecification record) throws Exception {
        return dao.selectByParam(record);
    }
}