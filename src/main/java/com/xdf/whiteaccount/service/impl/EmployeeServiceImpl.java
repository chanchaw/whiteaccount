package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.utils.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.Employee;

import java.util.List;

import com.xdf.whiteaccount.dao.EmployeeMapper;
import com.xdf.whiteaccount.service.EmployeeService;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-07-15 08:51:40
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-07-15 08:51:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(Employee record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-07-15 08:51:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(Employee record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-07-15 08:51:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<Employee> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-07-15 08:51:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(Employee record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-07-15 08:51:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(Employee record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-07-15 08:51:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(Employee record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-07-15 08:51:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<Employee> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-07-15 08:51:40
     */
    @Override
    public Employee selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-07-15 08:51:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-07-15 08:51:40
     */
    @Override
    public List<Employee> listQuery(Employee record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 根据外键查询
     *
     * @param id 外键
     * @return
     * @throws Exception
     */
    @Override
    public List<Employee> queryByFk(Integer id) throws Exception {
        if (id == null) return dao.selectByExample(new Example().andEq("state", 1));
        return dao.selectByExample(new Example().andEq("state", 1).andEq("department_id", id));
    }
}