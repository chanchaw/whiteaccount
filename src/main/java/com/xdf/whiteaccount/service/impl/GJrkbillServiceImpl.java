package com.xdf.whiteaccount.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.GJrkbill;

import java.util.List;

import com.xdf.whiteaccount.dao.GJrkbillMapper;
import com.xdf.whiteaccount.service.GJrkbillService;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-05-22 08:16:02
 */
@Service
public class GJrkbillServiceImpl implements GJrkbillService {
    @Autowired
    private GJrkbillMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(GJrkbill record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(GJrkbill record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<GJrkbill> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(GJrkbill record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(GJrkbill record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(GJrkbill record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<GJrkbill> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    public GJrkbill selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    public List<GJrkbill> listQuery(GJrkbill record) throws Exception {
        return dao.selectByParam(record);
    }
}