package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.entity.ProductOutwardOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.Jrkbillsum;

import java.util.Date;
import java.util.List;

import com.xdf.whiteaccount.dao.JrkbillsumMapper;
import com.xdf.whiteaccount.service.JrkbillsumService;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-06-02 19:49:07
 */
@Service
public class JrkbillsumServiceImpl implements JrkbillsumService {
    @Autowired
    private JrkbillsumMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-06-02 19:49:07
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(Jrkbillsum record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-06-02 19:49:07
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(Jrkbillsum record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-06-02 19:49:07
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<Jrkbillsum> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-02 19:49:07
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(Jrkbillsum record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-06-02 19:49:07
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(Jrkbillsum record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-02 19:49:07
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(Jrkbillsum record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-06-02 19:49:07
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<Jrkbillsum> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-06-02 19:49:07
     */
    @Override
    public Jrkbillsum selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-06-02 19:49:07
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-06-02 19:49:07
     */
    @Override
    public List<Jrkbillsum> listQuery(Jrkbillsum record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 根据日期查询
     * @param start
     * @param end
     * @return
     * @throws Exception
     */
    @Override
    public List<Jrkbillsum> listQueryByDate(Date start, Date end) throws Exception {
        return null;
    }

    @Override
    public int multiUpdateOutwardId(ProductOutwardOrder record) {
        return dao.multiUpdateOutwardId(record);
    }
}