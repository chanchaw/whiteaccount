package com.xdf.whiteaccount.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.MaterialAccountPayable;
import java.util.List;
import com.xdf.whiteaccount.dao.MaterialAccountPayableMapper;
import com.xdf.whiteaccount.service.MaterialAccountPayableService;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-07-01 16:40:45
 */
@Service
public class MaterialAccountPayableServiceImpl implements MaterialAccountPayableService {
    @Autowired
    private MaterialAccountPayableMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-07-01 16:40:45
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(MaterialAccountPayable record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-07-01 16:40:45
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(MaterialAccountPayable record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-07-01 16:40:45
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<MaterialAccountPayable> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-07-01 16:40:45
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(MaterialAccountPayable record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-07-01 16:40:45
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(MaterialAccountPayable record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-07-01 16:40:45
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(MaterialAccountPayable record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-07-01 16:40:45
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<MaterialAccountPayable> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-07-01 16:40:45
     */
    @Override
    public MaterialAccountPayable selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-07-01 16:40:45
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-07-01 16:40:45
     */
    @Override
    public List<MaterialAccountPayable> listQuery(MaterialAccountPayable record) throws Exception {
        return dao.selectByParam(record);
    }
}