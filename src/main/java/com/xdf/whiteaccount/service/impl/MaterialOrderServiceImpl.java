package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.entity.BillType;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.service.BillTypeService;
import com.xdf.whiteaccount.service.CallService;
import com.xdf.whiteaccount.service.base.BaseService;
import com.xdf.whiteaccount.utils.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.MaterialOrder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import com.xdf.whiteaccount.dao.MaterialOrderMapper;
import com.xdf.whiteaccount.service.MaterialOrderService;
import org.springframework.util.Assert;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-05-25 17:02:01
 */
@Service
public class MaterialOrderServiceImpl extends BaseService implements MaterialOrderService {
    @Autowired
    private MaterialOrderMapper dao;
    @Autowired
    private CallService callService;
    @Autowired
    private BillTypeService billTypeService;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-05-25 17:02:01
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(MaterialOrder record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-05-25 17:02:01
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(MaterialOrder record) throws Exception {
        int result = 0;
        record.setInputMoney(record.getInputKilo().multiply(Optional.ofNullable(record.getPrice()).orElse(BigDecimal.valueOf(0))));
        result += dao.insertSelective(record);
        BillType billType = billTypeService.selectByPrimaryKey(record.getBillType());
        //  入库直发生成量两张单据，一张入库单一张出库单
        if (record.getIsSendDirectly() && billType.getAccessMode() > 0) {
            String orderCode = record.getOrderCode();
            record.setOrderCode(callService.materialOrderCodeGen(-1));
            BillType outboundType = Optional.ofNullable(
                    billTypeService.listQuery(BillType.builder().accessMode(-1).isSelected(true).build()))
                    .orElse(new ArrayList<>()).stream().findFirst().orElse(null);
            Assert.notNull(outboundType, "没有领料单据类型！");
            record.setBillType(outboundType.getId());
            record.setLinkedOrderCode(orderCode);
            record.setId(null);
            result += dao.insertSelective(record);
        }
        return result;
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-05-25 17:02:01
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<MaterialOrder> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-25 17:02:01
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(MaterialOrder record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-05-25 17:02:01
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(MaterialOrder record) throws Exception {
        record.setInputMoney(record.getInputKilo().multiply(Optional.ofNullable(record.getPrice()).orElse(BigDecimal.valueOf(0))));
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-25 17:02:01
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(MaterialOrder record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-05-25 17:02:01
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<MaterialOrder> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-05-25 17:02:01
     */
    @Override
    public MaterialOrder selectByPrimaryKey(Long id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-05-25 17:02:01
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Long id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * 作废
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int cancelByPrimaryKey(Long id) throws Exception {
        MaterialOrder record = dao.selectByPrimaryKey(id);
        Assert.notNull(record, ResponseEnum.NO_ORDER.getName());
        return dao.updateByPrimaryKeySelective(MaterialOrder.builder().id(id).state(0).build());
    }

    /**
     * 根据主键审核
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int auditById(Long[] id) throws Exception {
        return dao.updateByExampleSelective(MaterialOrder.builder().isAudit(true).auditUserId(getLoginUserId()).build(), new Example().andIn("id", id));
    }

    /**
     * 根据经主键取消审核
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int cancelAuditById(Long[] id) throws Exception {
        return dao.updateByExampleSelective(MaterialOrder.builder().isAudit(false).auditUserId(0).build(), new Example().andIn("id", id));
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-05-25 17:02:01
     */
    @Override
    public List<MaterialOrder> listQuery(MaterialOrder record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 计划页面下拉数据源
     *
     * @param materialSpec 原料规格
     * @param selected     选中的批号
     * @param mode         是否全部
     * @return
     */
    @Override
    public List<Map<String, Object>> getMaterialStorageDropdownList(String materialSpec, String[] selected, int mode) {
        return dao.getMaterialStorageDropdownList(materialSpec, mode, selected);
    }
}