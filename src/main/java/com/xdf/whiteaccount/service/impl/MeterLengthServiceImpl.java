package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.dao.MeterLengthMapper;
import com.xdf.whiteaccount.entity.MeterLength;
import com.xdf.whiteaccount.service.MeterLengthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-08-04 15:04
 **/
@Service
@Slf4j
public class MeterLengthServiceImpl implements MeterLengthService {
    @Autowired
    private MeterLengthMapper dao;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(MeterLength record) {
        return dao.insert(record);
    }

    @Override
    public int insertSelective(MeterLength record) {
        return dao.insertSelective(record);
    }

    @Override
    public MeterLength selectByPrimaryKey(Integer id) {
        return dao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(MeterLength record) {
        return dao.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(MeterLength record) {
        return dao.updateByPrimaryKey(record);
    }

    @Override
    public List<MeterLength> listQuery(MeterLength record) {
        return dao.listQuery(record);
    }
}
