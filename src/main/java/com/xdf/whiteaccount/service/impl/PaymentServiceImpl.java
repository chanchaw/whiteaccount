package com.xdf.whiteaccount.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.Payment;

import java.util.List;

import com.xdf.whiteaccount.dao.PaymentMapper;
import com.xdf.whiteaccount.service.PaymentService;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-05-31 14:14:23
 */
@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-05-31 14:14:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(Payment record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-05-31 14:14:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(Payment record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-05-31 14:14:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<Payment> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-31 14:14:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(Payment record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-05-31 14:14:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(Payment record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-31 14:14:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(Payment record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-05-31 14:14:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<Payment> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-05-31 14:14:23
     */
    @Override
    public Payment selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-05-31 14:14:23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-05-31 14:14:23
     */
    @Override
    public List<Payment> listQuery(Payment record) throws Exception {
        return dao.selectByParam(record);
    }
}