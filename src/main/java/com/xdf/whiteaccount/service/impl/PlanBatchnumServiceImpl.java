package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.utils.Example;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.PlanBatchnum;

import java.util.Date;
import java.util.List;

import com.xdf.whiteaccount.dao.PlanBatchnumMapper;
import com.xdf.whiteaccount.service.PlanBatchnumService;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-07-19 09:24:54
 */
@Service
public class PlanBatchnumServiceImpl implements PlanBatchnumService {
    @Autowired
    private PlanBatchnumMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-07-19 09:24:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(PlanBatchnum record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-07-19 09:24:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(PlanBatchnum record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-07-19 09:24:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<PlanBatchnum> list) throws Exception {
        if (CollectionUtils.isEmpty(list)) return 0;
        Date createTime = new Date();
        for (int i = 0, len = list.size(); i < len; i++) {
            PlanBatchnum item = list.get(i);
            item.setId(null);
            item.setCreateTime(createTime);
            item.setSerialNum(i + 1);
        }
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-07-19 09:24:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(PlanBatchnum record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-07-19 09:24:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(PlanBatchnum record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-07-19 09:24:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(PlanBatchnum record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-07-19 09:24:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<PlanBatchnum> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-07-19 09:24:54
     */
    @Override
    public PlanBatchnum selectByPrimaryKey(Long id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-07-19 09:24:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Long id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-07-19 09:24:54
     */
    @Override
    public List<PlanBatchnum> listQuery(PlanBatchnum record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     *
     * @param planCode
     * @return
     * @throws Exception
     */
    @Override
    public List<PlanBatchnum> queryByPlanCode(String planCode) throws Exception {
        if (StringUtils.isEmpty(planCode)) return null;
        return dao.selectByParam(PlanBatchnum.builder().planCode(planCode).build());
    }

    /**
     * 根据Example删除
     *
     * @param example
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByExample(Example example) throws Exception {
        return dao.deleteByExample(example);
    }
}