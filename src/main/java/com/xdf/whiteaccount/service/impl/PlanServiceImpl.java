package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.dao.*;
import com.xdf.whiteaccount.entity.Whitebillfh;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.service.CallService;
import com.xdf.whiteaccount.service.base.BaseService;
import com.xdf.whiteaccount.utils.Example;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.Plan;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import com.xdf.whiteaccount.service.PlanService;
import org.springframework.util.Assert;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
@Service
public class PlanServiceImpl extends BaseService implements PlanService {
    @Autowired
    private PlanMapper dao;
    @Autowired
    private WhitebillfhMapper whitebillfhMapper;
    @Autowired
    private JrkbillsumMapper jrkbillsumMapper;
    @Autowired
    private CpBillfhMapper cpBillfhMapper;
    @Autowired
    private CpAdjustMapper cpAdjustMapper;
    @Autowired
    private CallService callService;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(Plan record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(Plan record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<Plan> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(Plan record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(Plan record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(Plan record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<Plan> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    public Plan selectByPrimaryKey(Long id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @SuppressWarnings("unchecked")
    public int deleteByPrimaryKey(Long id) throws Exception {
        //  判断当前库存是否为空,如果为空就可以删除
        // PlanDetailVO vo = callService.getPlanDtl(id);
        // BigDecimal kilo = Optional.ofNullable(vo).map(PlanDetailVO::getRemainKilo).orElse(BigDecimal.ZERO);
        // BigDecimal pairs = Optional.ofNullable(vo).map(PlanDetailVO::getRemainPairs).orElse(BigDecimal.ZERO);
        // Assert.state(kilo.doubleValue() == 0D && pairs.doubleValue() == 0D, "当前计划有库存不允许删除！");
        //  没有入库单据与出库单据才允许删除计划单
        Plan record = dao.selectByPrimaryKey(id);
        Assert.notNull(record, "单据不存在或者已删除！");
        Assert.state(0 == record.getState(), "单据必须作废才允许删除！");
        Assert.state(jrkbillsumMapper.countByExample(new Example().andEq("planid", record.getId())) <= 0, "当前计划有库存不允许删除");
        Assert.state(cpBillfhMapper.countByExample(new Example().andEq("planid", record.getId())) <= 0, "当前计划有发货数据不允许删除");
        Assert.state(cpAdjustMapper.countByExample(new Example().andEq("plan_id", record.getId())) <= 0, "当前计划有调整数据不允许删除");
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * 设置完工状态
     *
     * @param id
     * @param finished
     * @return
     * @throws Exception
     */
    @Override
    public int setFinishedMark(Long id, boolean finished) throws Exception {
        Plan record = dao.selectByPrimaryKey(id);
        Assert.state(record != null && record.getState() == 1, "单据不存在或已作废！");
        return dao.updateByPrimaryKeySelective(Plan.builder().id(id).markFinished(finished).build());
    }

    /**
     * 根据主键作废
     *
     * @param id 主键
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int cancelByPrimaryKey(Long id) throws Exception {
        return dao.updateByPrimaryKeySelective(Plan.builder().id(id).state(0).cancelUserId(getLoginUserId()).upload(0).build());
    }

    /**
     * 恢复计划单
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int recoverPlanByPk(Long id) throws Exception {
        return dao.updateByPrimaryKeySelective(Plan.builder().id(id).state(1).cancelUserId(0).upload(0).build());
    }

    /**
     * 根据id审核
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int auditById(Long[] id) throws Exception {
        return dao.updateByExampleSelective(Plan.builder().isAudit(true).auditUserId(getLoginUserId()).build(), new Example().andIn("id", id));
    }

    /**
     * 根据id取消审核
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int cancelAuditById(Long[] id) throws Exception {
        return dao.updateByExampleSelective(Plan.builder().isAudit(false).auditUserId(0).build(), new Example().andIn("id", id));
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    public List<Plan> listQuery(Plan record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 一键完工
     *
     * @return
     * @throws Exception
     */
    @Override
    public int auditAll() throws Exception {
        return dao.auditAll();
    }

    /**
     * 更改单价
     *
     * @param accountPrice
     * @param planid
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int modifyPriceByPk(BigDecimal accountPrice, Long planid) throws Exception {
        Plan record = dao.selectByPrimaryKey(planid);
        //  校验计划单
        Assert.notNull(record, ResponseEnum.CANCEL_ORDER.getName());
        Assert.state(record.getState() == 1, "单据已作废！");
        Assert.state(!Optional.ofNullable(record).map(Plan::getIsAudit).orElse(false), ResponseEnum.AUDIT_ORDER.getName());
        //  修改单价
        int res = 0;
        res += dao.updateByPrimaryKeySelective(Plan.builder().id(planid).processingCost(accountPrice).build());
        res += whitebillfhMapper.updateByExampleSelective(
                Whitebillfh.builder().accountPrice(accountPrice).build(),
                new Example().andEq("planid", planid));
        return res;
    }

    /**
     * 根据毛高米长排序的查询方法
     *
     * @param record
     * @return
     */
    @Override
    public List<Plan> queryByExample(Plan record) {
        List<Plan> list = dao.selectByExample(new Example()
                .andEq("state", record.getState())
                .andEq("mark_finished", record.getMarkFinished())
                .setOrderSql(Example.orderByCondition("m_height,meter_length,greyfabric_specification", Example.ASC)));
        return list;
    }

    /**
     * 设置排单显示的状态
     *
     * @param state 状态
     * @param id    主键数组
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int setPlanOrderShowState(boolean state, Long[] id) {
        Assert.state(ArrayUtils.isNotEmpty(id), "主键不允许为空！");
        return dao.updateByExampleSelective(Plan.builder().isShowOrder(state).build(), new Example().andIn("id", id));
    }
}