package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.service.CallService;
import com.xdf.whiteaccount.service.base.BaseService;
import com.xdf.whiteaccount.utils.CustumDozerMapperUtil;
import com.xdf.whiteaccount.vo.PlanTemplateVO;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.PlanTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.xdf.whiteaccount.dao.PlanTemplateMapper;
import com.xdf.whiteaccount.service.PlanTemplateService;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-05-20 19:00:08
 */
@Service
public class PlanTemplateServiceImpl extends BaseService implements PlanTemplateService {
    @Autowired
    private PlanTemplateMapper dao;
    @Autowired
    private CallService callService;
    @Autowired
    private CustumDozerMapperUtil utils;
    @Autowired
    private PlanTemplate Template;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-05-20 19:00:08
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(PlanTemplate record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-05-20 19:00:08
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(PlanTemplate record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-05-20 19:00:08
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<PlanTemplate> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-20 19:00:08
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(PlanTemplate record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-05-20 19:00:08
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(PlanTemplate record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-20 19:00:08
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(PlanTemplate record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-05-20 19:00:08
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<PlanTemplate> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-05-20 19:00:08
     */
    @Override
    public PlanTemplate selectByPrimaryKey(Long id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-05-20 19:00:08
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Long id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-05-20 19:00:08
     */
    @Override
    public List<PlanTemplate> listQuery(PlanTemplate record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 选择默认模板
     *
     * @return
     * @throws Exception
     */
    @Override
    public PlanTemplateVO selectDefaultTemplate() throws Exception {
        List<PlanTemplate> defaultTemplates = dao.selectByParam(PlanTemplate.builder().setDefault(true).build());
        PlanTemplate item = Optional.ofNullable(defaultTemplates).orElse(new ArrayList<>()).stream().findFirst().orElse(new PlanTemplate());
        PlanTemplateVO vo = utils.transObject(item, PlanTemplateVO.class);
        vo.setSubmitDate(DateUtils.addDays(new Date(), 14));
        vo.setGreyfabricDate(DateUtils.addDays(new Date(), 7));
        vo.setPlanCode(callService.planCodeGen());
        vo.setCreateUserId(getLoginUserId());
        return vo;
    }

    /**
     * 复制
     *
     * @return
     * @throws Exception
     */
    @Override
    public PlanTemplateVO copy() throws Exception {
        List<PlanTemplate> defaultTemplates = dao.selectByParam(PlanTemplate.builder().setDefault(true).build());
        PlanTemplate item = Optional.ofNullable(defaultTemplates).orElse(new ArrayList<>()).stream().findFirst().orElse((PlanTemplate) Template.clone());
        PlanTemplateVO vo = utils.transObject(item, PlanTemplateVO.class);
        vo.setSubmitDate(DateUtils.addDays(new Date(), 14));
        vo.setGreyfabricDate(DateUtils.addDays(new Date(), 7));
        vo.setPlanCode(callService.planCodeGen());
        vo.setCreateUserId(getLoginUserId());
//        vo.setProductName(String.valueOf(dao.selectAll()));
//        vo.setMaterialSpecification(String.valueOf(dao.selectAll()));
//        vo.setPlanKilo(dao.selectAll());
        return vo;
    }

    /**
     * 修改默认模板
     *
     * @param planTemplate
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateSelectiveDefault(PlanTemplate planTemplate) throws Exception {
        planTemplate.setId(null);
        PlanTemplate record = Optional.ofNullable(dao.selectByParam(PlanTemplate.builder().setDefault(true).build()))
                .orElse(new ArrayList<>()).stream().findFirst().orElse(null);
        planTemplate.setSetDefault(true);
        if (record == null) {
            return dao.insert(planTemplate);
        } else {
            planTemplate.setId(record.getId());
            return dao.updateByPrimaryKey(planTemplate);
        }
    }
}