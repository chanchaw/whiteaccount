package com.xdf.whiteaccount.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.Storage;

import java.util.List;

import com.xdf.whiteaccount.dao.StorageMapper;
import com.xdf.whiteaccount.service.StorageService;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-07-23 09:48:38
 */
@Service
public class StorageServiceImpl implements StorageService {
    @Autowired
    private StorageMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-07-23 09:48:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(Storage record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-07-23 09:48:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(Storage record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-07-23 09:48:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<Storage> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-07-23 09:48:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(Storage record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-07-23 09:48:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(Storage record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-07-23 09:48:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(Storage record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-07-23 09:48:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<Storage> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-07-23 09:48:38
     */
    @Override
    public Storage selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-07-23 09:48:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-07-23 09:48:38
     */
    @Override
    public List<Storage> listQuery(Storage record) throws Exception {
        return dao.selectByParam(record);
    }
}