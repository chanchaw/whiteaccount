package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.enums.ConfigIntEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.SysConfigInt;

import java.util.List;

import com.xdf.whiteaccount.dao.SysConfigIntMapper;
import com.xdf.whiteaccount.service.SysConfigIntService;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
@Service
public class SysConfigIntServiceImpl implements SysConfigIntService {
    @Autowired
    private SysConfigIntMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(SysConfigInt record) throws Exception {
        if (ConfigIntEnum.AUTO_AUDIT_AT.getField().equals(record.getConfigField())) {
            if (record.getConfigValue() != null) {
                if (record.getConfigValue() < 0) record.setConfigValue(0);
                if (record.getConfigValue() > 24) record.setConfigValue(24);
            }
        }
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(SysConfigInt record) throws Exception {
        if (ConfigIntEnum.AUTO_AUDIT_AT.getField().equals(record.getConfigField())) {
            if (record.getConfigValue() != null) {
                if (record.getConfigValue() < 0) record.setConfigValue(0);
                if (record.getConfigValue() > 24) record.setConfigValue(24);
            }
        }
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<SysConfigInt> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(SysConfigInt record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(SysConfigInt record) throws Exception {
        if (ConfigIntEnum.AUTO_AUDIT_AT.getField().equals(record.getConfigField())) {
            if (record.getConfigValue() != null) {
                if (record.getConfigValue() < 0) record.setConfigValue(0);
                if (record.getConfigValue() > 24) record.setConfigValue(24);
            }
        }
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(SysConfigInt record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<SysConfigInt> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    public SysConfigInt selectByPrimaryKey(String id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(String id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    public List<SysConfigInt> listQuery(SysConfigInt record) throws Exception {
        return dao.selectByParam(record);
    }
}