package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.enums.ConfigVarEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.SysConfigVarchar;

import java.util.List;

import com.xdf.whiteaccount.dao.SysConfigVarcharMapper;
import com.xdf.whiteaccount.service.SysConfigVarcharService;
import org.springframework.util.Assert;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-05-26 14:22:09
 */
@Service
public class SysConfigVarcharServiceImpl implements SysConfigVarcharService {
    @Autowired
    private SysConfigVarcharMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-05-26 14:22:09
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(SysConfigVarchar record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-05-26 14:22:09
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(SysConfigVarchar record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-05-26 14:22:09
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<SysConfigVarchar> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-26 14:22:09
     */
    @Override
    public int updateByPrimaryKey(SysConfigVarchar record) throws Exception {
        if (ConfigVarEnum.CLIENT_NICK_NAME.getField().equals(record.getConfigField())) {
            Assert.state(StringUtils.isNotEmpty(record.getConfigValue()), ConfigVarEnum.CLIENT_NICK_NAME.getField() + "不能为空");
        }
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-05-26 14:22:09
     */
    @Override
    public int updateByPrimaryKeySelective(SysConfigVarchar record) throws Exception {
        if (ConfigVarEnum.CLIENT_NICK_NAME.getField().equals(record.getConfigField())) {
            Assert.state(StringUtils.isNotEmpty(record.getConfigValue()), ConfigVarEnum.CLIENT_NICK_NAME.getField() + "不能为空");
        }
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-26 14:22:09
     */
    @Override
    public int update(SysConfigVarchar record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-05-26 14:22:09
     */
    @Override
    public int multiUpdate(List<SysConfigVarchar> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-05-26 14:22:09
     */
    @Override
    public SysConfigVarchar selectByPrimaryKey(String id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-05-26 14:22:09
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(String id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-05-26 14:22:09
     */
    @Override
    public List<SysConfigVarchar> listQuery(SysConfigVarchar record) throws Exception {
        return dao.selectByParam(record);
    }
}