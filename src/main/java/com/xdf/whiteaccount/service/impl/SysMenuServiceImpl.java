package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.dao.SysMenuMapper;
import com.xdf.whiteaccount.dto.TreeDTO;
import com.xdf.whiteaccount.entity.SysMenu;
import com.xdf.whiteaccount.service.SysMenuService;
import com.xdf.whiteaccount.utils.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-01-03 14:31:54
 */
@Service
public class SysMenuServiceImpl implements SysMenuService {
    @Autowired
    private SysMenuMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-01-03 14:31:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(SysMenu record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-01-03 14:31:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(SysMenu record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-01-03 14:31:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<SysMenu> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-01-03 14:31:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(SysMenu record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-01-03 14:31:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(SysMenu record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-01-03 14:31:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(SysMenu record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-01-03 14:31:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<SysMenu> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-01-03 14:31:54
     */
    @Override
    public SysMenu selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-01-03 14:31:54
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-01-03 14:31:54
     */
    @Override
    public List<SysMenu> listQuery(SysMenu record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 获取菜单树
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public List<TreeDTO> listQueryByParentId(Integer id) throws Exception {
        List<SysMenu> list;
        if (id == null) {
            list = dao.selectByExample(new Example().andIsNull("parent_id"));
        } else {
            list = dao.selectByExample(new Example().andEq("parent_id", id));
        }
        List<TreeDTO> treeDTOList = new ArrayList<>();
        for (SysMenu sysMenu : list) {
            TreeDTO treeDTO = new TreeDTO();
            treeDTO.setIconCls(sysMenu.getMenuIcon());
            treeDTO.setId(sysMenu.getId());
            treeDTO.setText(sysMenu.getMenuName());
            treeDTO.setSerialNo(sysMenu.getSerialNo());
            HashMap<String, Object> map = new HashMap<>();
            map.put("path", sysMenu.getMenuPath());
            map.put("serialNo", sysMenu.getSerialNo());
            treeDTO.setAttribute(map);
            long count = dao.countByExample(new Example().andEq("parent_id", sysMenu.getId()));
            if (count > 0) {
                treeDTO.setChildren(listQueryByParentId(treeDTO.getId()));
            }
            treeDTOList.add(treeDTO);
        }
        return treeDTOList.stream().sorted(Comparator.nullsLast(Comparator.comparing(TreeDTO::getSerialNo, Comparator.nullsFirst(Comparator.naturalOrder()))))
                .collect(Collectors.toList());
    }
}