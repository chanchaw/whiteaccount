package com.xdf.whiteaccount.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.SysPassword;
import java.util.List;
import com.xdf.whiteaccount.dao.SysPasswordMapper;
import com.xdf.whiteaccount.service.SysPasswordService;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-06-21 15:19:40
 */
@Service
public class SysPasswordServiceImpl implements SysPasswordService {
    @Autowired
    private SysPasswordMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-06-21 15:19:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(SysPassword record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-06-21 15:19:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(SysPassword record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-06-21 15:19:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<SysPassword> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-21 15:19:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(SysPassword record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-06-21 15:19:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(SysPassword record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-06-21 15:19:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(SysPassword record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-06-21 15:19:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<SysPassword> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-06-21 15:19:40
     */
    @Override
    public SysPassword selectByPrimaryKey(String id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-06-21 15:19:40
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(String id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-06-21 15:19:40
     */
    @Override
    public List<SysPassword> listQuery(SysPassword record) throws Exception {
        return dao.selectByParam(record);
    }
}