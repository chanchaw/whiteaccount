package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.dao.SysPermissionMapper;
import com.xdf.whiteaccount.entity.SysPermission;
import com.xdf.whiteaccount.service.SysPermissionService;
import com.xdf.whiteaccount.utils.Example;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2020-12-31 14:42:38
 */
@Service
public class SysPermissionServiceImpl implements SysPermissionService {
    @Autowired
    private SysPermissionMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(SysPermission record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(SysPermission record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<SysPermission> list) throws Exception {
        if (CollectionUtils.isEmpty(list)) return 0;
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(SysPermission record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(SysPermission record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(SysPermission record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<SysPermission> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    public SysPermission selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * 根据菜单编号删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteBySysMenuId(Integer id) throws Exception {
        if (id != null) {
            return dao.deleteByExample(new Example().andEq("sys_menu_id", id));
        }
        return 0;
    }

    /**
     * 根据角色编号删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteByRoleId(Integer id) throws Exception {
        if (id != null) {
            return dao.deleteByExample(new Example().andEq("sys_role_id", id));
        }
        return 0;
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    public List<SysPermission> listQuery(SysPermission record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 根据角色编号查询
     *
     * @param id 角色编号
     * @return
     * @throws Exception
     */
    @Override
    public List<SysPermission> queryRoleId(Integer... id) throws Exception {
        if (id == null) return null;
        return dao.selectByExample(new Example().andIn("sys_role_id", id));
    }

}