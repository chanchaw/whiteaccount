package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.dao.SysRoleGroupDtlMapper;
import com.xdf.whiteaccount.entity.SysRoleGroupDtl;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.service.SysRoleGroupDtlService;
import com.xdf.whiteaccount.utils.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2020-12-31 14:42:38
 */
@Service
public class SysRoleGroupDtlServiceImpl implements SysRoleGroupDtlService {
    @Autowired
    private SysRoleGroupDtlMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(SysRoleGroupDtl record) throws Exception {
        Assert.state(validateUpdateRecord(record), "角色组已有此角色!");
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(SysRoleGroupDtl record) throws Exception {
        Assert.state(validateUpdateRecord(record), "角色组已有此角色!");
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<SysRoleGroupDtl> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(SysRoleGroupDtl record) throws Exception {
        Assert.state(validateUpdateRecord(record), "角色组已有此角色!");
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(SysRoleGroupDtl record) throws Exception {
        SysRoleGroupDtl row = dao.selectByPrimaryKey(record.getId());
        Assert.notNull(row, ResponseEnum.CANCEL_ORDER.getName());
        Assert.state(validateUpdateRecord(record), "角色组已有此角色!");
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(SysRoleGroupDtl record) throws Exception {
        Assert.state(validateUpdateRecord(record), "角色组已有此角色!");
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<SysRoleGroupDtl> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    public SysRoleGroupDtl selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * 根据角色组编号删除内容
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByRoleGroupId(Integer id) throws Exception {
        if (id == null) return 0;
        return dao.deleteByExample(new Example().andEq("role_group_id", id));
    }

    /**
     * 根据角色主键删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByRoleId(Integer id) throws Exception {
        if (id != null) {
            return dao.deleteByExample(new Example().andEq("role_id", id));
        }
        return 0;
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    public List<SysRoleGroupDtl> listQuery(SysRoleGroupDtl record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 根据主键
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public List<SysRoleGroupDtl> selectByRoleGroupId(Integer... id) throws Exception {
        if (id == null || id.length <= 0) return null;
        return dao.selectByExample(new Example().andIn("role_group_id", id));
    }

    /**
     * 验证是否可以更改
     *
     * @param record
     * @return
     */
    private boolean validateUpdateRecord(SysRoleGroupDtl record) {
        if (record.getId() != null) {
            return dao.countByExample(new Example()
                    .andEq("role_id", record.getRoleId())
                    .andNotEquals("id", record.getId())
                    .andEq("role_group_id", record.getRoleGroupId())) <= 0;
        } else {
            return dao.countByExample(new Example()
                    .andEq("role_id", record.getRoleId())
                    .andEq("role_group_id", record.getRoleGroupId())) <= 0;
        }
    }

}