package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.dao.SysRoleGroupMapper;
import com.xdf.whiteaccount.dao.SysUserRoleGroupMapper;
import com.xdf.whiteaccount.entity.SysRoleGroup;
import com.xdf.whiteaccount.entity.SysUserRoleGroup;
import com.xdf.whiteaccount.service.SysRoleGroupDtlService;
import com.xdf.whiteaccount.service.SysRoleGroupService;
import com.xdf.whiteaccount.service.SysUserRoleGroupService;
import com.xdf.whiteaccount.utils.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2020-12-31 14:42:38
 */
@Service
public class SysRoleGroupServiceImpl implements SysRoleGroupService {
    @Autowired
    private SysRoleGroupMapper dao;
    @Autowired
    private SysUserRoleGroupMapper sysUserRoleGroupMapper;
    @Autowired
    private SysRoleGroupDtlService sysRoleGroupDtlService;
    @Autowired
    private SysUserRoleGroupService sysUserRoleGroupService;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(SysRoleGroup record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(SysRoleGroup record) throws Exception {
        if (record.getRoleGroupAdminSign() == null) record.setRoleGroupAdminSign(false);
        if (record.getIsInitialSelect() == null) record.setIsInitialSelect(false);
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<SysRoleGroup> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(SysRoleGroup record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(SysRoleGroup record) throws Exception {
        if (record.getRoleGroupAdminSign() == null) record.setRoleGroupAdminSign(false);
        if (record.getIsInitialSelect() == null) record.setIsInitialSelect(false);
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(SysRoleGroup record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<SysRoleGroup> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    public SysRoleGroup selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        int result = 0;
        result += sysUserRoleGroupService.deleteByRoleGroupId(id);
        result += sysRoleGroupDtlService.deleteByRoleGroupId(id);
        result += dao.deleteByPrimaryKey(id);
        return result;
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    public List<SysRoleGroup> listQuery(SysRoleGroup record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 根据用户主键查询
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public List<SysRoleGroup> queryByUserPk(Integer id) throws Exception {
        if (id == null) return null;
        List<SysUserRoleGroup> userRoleGroups = Optional.ofNullable(sysUserRoleGroupMapper.selectByParam(SysUserRoleGroup.builder()
                .sysUserId(id)
                .build())).get();
        List<SysRoleGroup> list = dao.selectByExample(new Example().andIn("id",
                userRoleGroups.stream().map(x -> x.getSysRoleGroupId()).distinct()));
        return list;
    }
}