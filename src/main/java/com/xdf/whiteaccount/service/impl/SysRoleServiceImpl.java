package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.dao.SysRoleMapper;
import com.xdf.whiteaccount.entity.SysMenu;
import com.xdf.whiteaccount.entity.SysPermission;
import com.xdf.whiteaccount.entity.SysRole;
import com.xdf.whiteaccount.entity.SysRoleGroupDtl;
import com.xdf.whiteaccount.service.*;
import com.xdf.whiteaccount.utils.Example;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2020-12-31 14:42:38
 */
@Service
public class SysRoleServiceImpl implements SysRoleService {
    @Autowired
    private SysRoleMapper dao;
    @Autowired
    private SysRoleGroupDtlService sysRoleGroupDtlService;
    @Autowired
    private SysMenuService sysMenuService;
    @Autowired
    private SysPermissionService sysPermissionService;
    @Autowired
    private SysUserRoleGroupService sysUserRoleGroupService;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(SysRole record) throws Exception {
        record.setId(null);
        int res = dao.insert(record);
        Integer sysRoleId = record.getId();
        List<SysMenu> sysMenuList = sysMenuService.listQuery(SysMenu.builder().build());
        if (CollectionUtils.isNotEmpty(sysMenuList)) {
            sysPermissionService.multiInsert(sysMenuList.stream().map(x ->
                    SysPermission.builder()
                            .sysRoleId(sysRoleId)
                            .sysMenuId(x.getId())
                            .build()
            ).collect(Collectors.toList()));
        }
        return res;
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(SysRole record) throws Exception {
        record.setId(null);
        int res = dao.insertSelective(record);
        Integer sysRoleId = record.getId();
        List<SysMenu> sysMenuList = sysMenuService.listQuery(SysMenu.builder().build());
        if (CollectionUtils.isNotEmpty(sysMenuList)) {
            sysPermissionService.multiInsert(sysMenuList.stream().map(x ->
                    SysPermission.builder()
                            .sysRoleId(sysRoleId)
                            .sysMenuId(x.getId())
                            .build()
            ).collect(Collectors.toList()));
        }
        return res;
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<SysRole> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(SysRole record) throws Exception {
        if (record.getRoleAdminSign() == null) {
            record.setRoleAdminSign(false);
        }
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(SysRole record) throws Exception {
        if (record.getRoleAdminSign() == null) {
            record.setRoleAdminSign(false);
        }
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(SysRole record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<SysRole> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    public SysRole selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        int result = 0;
        result += sysPermissionService.deleteByRoleId(id);
        result += sysRoleGroupDtlService.deleteByRoleId(id);
        result += dao.deleteByPrimaryKey(id);
        return result;
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    public List<SysRole> listQuery(SysRole record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 根据角色组编号查询所有的角色
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public List<SysRole> selectByRoleGroupId(Integer[] id) throws Exception {
        if (id == null) return null;
        List<SysRoleGroupDtl> list = Optional.ofNullable(sysRoleGroupDtlService.selectByRoleGroupId(id)).orElse(new ArrayList<>());
        if (list.size() <= 0) return new ArrayList<>();
        Integer[] roleId = list.stream().map(x -> x.getRoleId()).distinct().toArray(Integer[]::new);
        if (ArrayUtils.isEmpty(roleId)) return null;
        List<SysRole> roleList = dao.selectByExample(new Example().andIn("id", roleId));
        return roleList;
    }
}