package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.dao.SysUserRoleGroupMapper;
import com.xdf.whiteaccount.entity.SysUserRoleGroup;
import com.xdf.whiteaccount.service.SysUserRoleGroupService;
import com.xdf.whiteaccount.utils.Example;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2020-12-31 14:42:38
 */
@Service
public class SysUserRoleGroupServiceImpl implements SysUserRoleGroupService {
    @Autowired
    private SysUserRoleGroupMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(SysUserRoleGroup record) throws Exception {
        Assert.state(validateUpdateRecord(record), "用户已有此角色组!");
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(SysUserRoleGroup record) throws Exception {
        Assert.state(validateUpdateRecord(record), "用户已有此角色组!");
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<SysUserRoleGroup> list) throws Exception {
        if(CollectionUtils.isEmpty(list)) return 0;
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(SysUserRoleGroup record) throws Exception {
        Assert.state(validateUpdateRecord(record), "用户已有此角色组!");
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(SysUserRoleGroup record) throws Exception {
        Assert.state(validateUpdateRecord(record), "用户已有此角色组!");
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(SysUserRoleGroup record) throws Exception {
        Assert.state(validateUpdateRecord(record), "用户已有此角色组!");
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<SysUserRoleGroup> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    public SysUserRoleGroup selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * 根据角色组编号删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByRoleGroupId(Integer id) throws Exception {
        if (id == null) return 0;
        return dao.deleteByExample(new Example().andEq("sys_role_group_id", id));
    }

    /**
     * 根据用户编号删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByUserId(Integer id) throws Exception {
        if (id == null) return 0;
        return dao.deleteByExample(new Example().andEq("sys_user_id", id));
    }

    /**
     * 根据条件删除
     * @param example
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByExample(Example example) throws Exception {
        return dao.deleteByExample(example);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2020-12-31 14:42:38
     */
    @Override
    public List<SysUserRoleGroup> listQuery(SysUserRoleGroup record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 验证是否修改
     *
     * @param record
     * @return
     */
    private boolean validateUpdateRecord(SysUserRoleGroup record) {
        Long count = 0L;
        if (record.getId() != null) {
            count = dao.countByExample(new Example()
                    .andEq("sys_role_group_id", record.getSysRoleGroupId())
                    .andNotEquals("id", record.getId())
                    .andEq("sys_user_id", record.getSysUserId()));
        } else {
            count = dao.countByExample(new Example()
                    .andEq("sys_role_group_id", record.getSysRoleGroupId())
                    .andEq("sys_user_id", record.getSysUserId()));
        }
        return count <= 0;
    }
}