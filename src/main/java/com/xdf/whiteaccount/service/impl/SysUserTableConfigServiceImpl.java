package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.dao.SysUserTableConfigMapper;
import com.xdf.whiteaccount.entity.SysUserTableConfig;
import com.xdf.whiteaccount.service.SysUserTableConfigService;
import com.xdf.whiteaccount.service.base.BaseService;
import com.xdf.whiteaccount.utils.Example;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * @Description : com.xdf.xzymanagementsystem.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2020-12-31 15:23:24
 */
@Service
public class SysUserTableConfigServiceImpl extends BaseService implements SysUserTableConfigService {
    @Autowired
    private SysUserTableConfigMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2020-12-31 15:23:24
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(SysUserTableConfig record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2020-12-31 15:23:24
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(SysUserTableConfig record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2020-12-31 15:23:24
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<SysUserTableConfig> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2020-12-31 15:23:24
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(SysUserTableConfig record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2020-12-31 15:23:24
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(SysUserTableConfig record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2020-12-31 15:23:24
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(SysUserTableConfig record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2020-12-31 15:23:24
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<SysUserTableConfig> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2020-12-31 15:23:24
     */
    @Override
    public SysUserTableConfig selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2020-12-31 15:23:24
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * 根据模板更改
     *
     * @param example
     * @param config
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByExample(Example example, SysUserTableConfig config) throws Exception {
        return dao.updateByExampleSelective(config, example);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2020-12-31 15:23:24
     */
    @Override
    public List<SysUserTableConfig> listQuery(SysUserTableConfig record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 根据菜单名与用户主键查询
     *
     * @param sysUserId
     * @param sysMenuId
     * @return
     * @throws Exception
     */
    @Override
    public List<SysUserTableConfig> selectBySysUserIdMenuId(Integer sysUserId, Integer sysMenuId) throws Exception {
        return dao.selectByParam(SysUserTableConfig.builder().sysUserId(sysUserId).sysMenuId(sysMenuId).build());
    }

    /**
     * 根据菜单编号与用户主键和列字段更改
     *
     * @param config
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateBySysUserIdMenuId(SysUserTableConfig config) throws Exception {
        if (config == null || config.getSysMenuId() == null) return 0;
        List<SysUserTableConfig> list = dao.selectByParam(SysUserTableConfig.builder()
                .sysMenuId(config.getSysMenuId())
                .sysUserId(getLoginUserId())
                .fieldName(config.getFieldName())
                .build());
        config.setSysUserId(getLoginUserId());
        if (CollectionUtils.isEmpty(list)) {
            config.setSysUserId(getLoginUserId());
            return dao.insertSelective(config);
        } else if (list.size() > 1) {
            Integer[] arr = list.stream().map(x -> x.getId()).skip(1).toArray(Integer[]::new);
            dao.deleteByExample(new Example().andIn("id", arr));
            config.setId(list.get(0).getId());
            return dao.updateByPrimaryKeySelective(config);
        } else {
            SysUserTableConfig record = list.get(0);
            config.setId(record.getId());
            return dao.updateByPrimaryKeySelective(config);
        }
    }

}