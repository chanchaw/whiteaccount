package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.dao.SysMenuMapper;
import com.xdf.whiteaccount.entity.SysMenu;
import com.xdf.whiteaccount.enums.ResponseEnum;
import com.xdf.whiteaccount.service.base.BaseService;
import com.xdf.whiteaccount.utils.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.SysUserVisitList;

import java.util.List;
import java.util.Map;

import com.xdf.whiteaccount.dao.SysUserVisitListMapper;
import com.xdf.whiteaccount.service.SysUserVisitListService;
import org.springframework.util.Assert;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-07-31 16:39:12
 */
@Service
public class SysUserVisitListServiceImpl extends BaseService implements SysUserVisitListService {
    @Autowired
    private SysUserVisitListMapper dao;
    @Autowired
    private SysMenuMapper sysMenuMapper;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-07-31 16:39:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(SysUserVisitList record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-07-31 16:39:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(SysUserVisitList record) throws Exception {
        Assert.notNull(record, ResponseEnum.NOT_ALLOW_EMPTY_DATA.getName());
        Assert.state(record.getSysMenuId() != null, ResponseEnum.NOT_ALLOW_EMPTY_DATA.getName());
        SysMenu menu = sysMenuMapper.selectByPrimaryKey(record.getSysMenuId());
        Assert.notNull(menu, "菜单不存在！");
        if (dao.countByExample(new Example().andEq("sys_menu_id", record.getSysMenuId()).andEq("sys_user_id", getLoginUserId())) > 0) {
            return 0;
        }
        record.setSysMenuParent(menu.getParentId());
        record.setSysUserId(getLoginUserId());
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-07-31 16:39:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<SysUserVisitList> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-07-31 16:39:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(SysUserVisitList record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-07-31 16:39:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(SysUserVisitList record) throws Exception {
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-07-31 16:39:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(SysUserVisitList record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-07-31 16:39:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<SysUserVisitList> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-07-31 16:39:12
     */
    @Override
    public SysUserVisitList selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-07-31 16:39:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * 根据MenuId删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public int deleteByMenuId(Integer id) throws Exception {
        return dao.deleteByExample(new Example().andEq("sys_menu_id", id)
                .andEq("sys_user_id", getLoginUserId()));
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-07-31 16:39:12
     */
    @Override
    public List<SysUserVisitList> listQuery(SysUserVisitList record) throws Exception {
        return dao.selectByParam(record);
    }

    /**
     * 根据用户ID查询
     *
     * @param userId
     * @return
     */
    @Override
    public List<Map<String, Object>> queryByUserId(Integer userId) {
        return dao.queryByUserId(userId);
    }
}