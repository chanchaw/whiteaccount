package com.xdf.whiteaccount.service.impl;

import com.xdf.whiteaccount.enums.ResponseEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.Whitebillfh;
import java.util.List;
import com.xdf.whiteaccount.dao.WhitebillfhMapper;
import com.xdf.whiteaccount.service.WhitebillfhService;
import org.springframework.util.Assert;

/**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-05-22 08:16:02
 */
@Service
public class WhitebillfhServiceImpl implements WhitebillfhService {
    @Autowired
    private WhitebillfhMapper dao;

    /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(Whitebillfh record) throws Exception {
        return dao.insert(record);
    }

    /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(Whitebillfh record) throws Exception {
        return dao.insertSelective(record);
    }

    /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<Whitebillfh> list) throws Exception {
        return dao.multiInsert(list);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(Whitebillfh record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(Whitebillfh record) throws Exception {
        Whitebillfh whitebillfh = dao.selectByPrimaryKey(record.getId());
        Assert.notNull(whitebillfh, ResponseEnum.CANCEL_ORDER.getName());
        return dao.updateByPrimaryKeySelective(record);
    }

    /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(Whitebillfh record) throws Exception {
        return dao.updateByPrimaryKey(record);
    }

    /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<Whitebillfh> list) throws Exception {
        return dao.multiUpdate(list);
    }

    /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    public Whitebillfh selectByPrimaryKey(Integer id) throws Exception {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return dao.deleteByPrimaryKey(id);
    }

    /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-05-22 08:16:02
     */
    @Override
    public List<Whitebillfh> listQuery(Whitebillfh record) throws Exception {
        return dao.selectByParam(record);
    }
}