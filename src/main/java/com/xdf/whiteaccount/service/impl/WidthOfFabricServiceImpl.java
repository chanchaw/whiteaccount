package com.xdf.whiteaccount.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xdf.whiteaccount.entity.WidthOfFabric;
import java.util.List;
import com.xdf.whiteaccount.dao.WidthOfFabricMapper;
import com.xdf.whiteaccount.service.WidthOfFabricService;

 /**
 * @Description : com.xdf.whiteaccount.service.impl(业务层实现类,该类为生成器自动生成).
 * @Author : 张柯
 * @Date : 2021-05-16 09:40:12
 */
@Service
public class WidthOfFabricServiceImpl implements WidthOfFabricService{
@Autowired
private WidthOfFabricMapper dao;

     /**
     * @Describe 新增方法
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(WidthOfFabric record) throws Exception{
        return dao.insert(record);
    }

     /**
     * @Describe 选择新增
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(WidthOfFabric record) throws Exception{
        return dao.insertSelective(record);
    }

     /**
     * @Describe 批量新增
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiInsert(List<WidthOfFabric> list) throws Exception{
        return dao.multiInsert(list);
    }

     /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(WidthOfFabric record) throws Exception{
        return dao.updateByPrimaryKey(record);
    }

     /**
     * @Describe 选择修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(WidthOfFabric record) throws Exception{
        return dao.updateByPrimaryKeySelective(record);
    }

     /**
     * @Describe 修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(WidthOfFabric record) throws Exception{
        return dao.updateByPrimaryKey(record);
    }

     /**
     * @Describe 批量修改
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int multiUpdate(List<WidthOfFabric> list) throws Exception{
        return dao.multiUpdate(list);
    }

     /**
     * @Describe 根据主键查询
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    public WidthOfFabric selectByPrimaryKey(Long id) throws Exception{
        return dao.selectByPrimaryKey(id);
    }

     /**
     * @Describe 根据主键删除
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Long id) throws Exception{
        return dao.deleteByPrimaryKey(id);
    }

     /**
     * @Describe 根据Entity查询
     * @author 张柯
     * @Date 2021-05-16 09:40:12
     */
    @Override
    public List<WidthOfFabric> listQuery(WidthOfFabric record) throws Exception{
        return dao.selectByParam(record);
    }
}