package com.xdf.whiteaccount.taskservice;

/**
 * @program: white-account
 * @description: 定时任务业务层
 * @author: 张柯
 * @create: 2021-07-26 14:36
 **/
public interface TaskService {
    void autoAudit();
}
