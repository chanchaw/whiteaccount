package com.xdf.whiteaccount.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Random;

/**
 * @program: white-account
 * @description: 数据库备份
 * @author: 张柯
 * @create: 2021-06-21 09:14
 **/
@Slf4j
public class BackupUtil {
    //  判断是否为Windows系统
    public static final boolean isWindows = System.getProperty("os.name").toUpperCase().contains("WINDOWS");
    //  判断是否为Linux系统
    public static final boolean isLinux = System.getProperty("os.name").toUpperCase().contains("LINUX");
    //  判断是否为Mac系统
    public static final boolean isMAC = System.getProperty("os.name").toUpperCase().contains("MAC");

    private BackupUtil() {
    }

    /**
     * 数据库备份
     *
     * @param host       主机
     * @param port       端口
     * @param dbName     数据库名
     * @param username   用户名
     * @param password   密码
     * @param backupPath 备份路径
     * @return
     */
    public static boolean backup(String host, String port, String dbName, String username, String password, String backupPath) {
        String cmd = "mysqldump --skip-opt -h{0} -P{1} -u{2} -p{3} -R {4} -r {5}";
        String format;
        if (isWindows) {
            format = "cmd /c " + MessageFormat.format(cmd, host, port, username, password, dbName, backupPath);
        } else if (isLinux) {
            format = MessageFormat.format(cmd, host, port, username, password, dbName, backupPath);
        } else if (isMAC) {
            format = MessageFormat.format(cmd, host, port, username, password, dbName, backupPath);
        } else {
            throw new RuntimeException("未知操作系统！");
        }
        log.warn("准备执行的批处理：" + format);
        try {
            Process process = Runtime.getRuntime().exec(format);
            process.waitFor();
            InputStreamReader reader = new InputStreamReader(process.getErrorStream(), "GBK");
            BufferedReader bufferedReader = new BufferedReader(reader);
            bufferedReader.lines().forEach(log::warn);
            bufferedReader.close();
            if (process.exitValue() == 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("备份失败！");
        }
        return false;
    }

    /**
     * 文件名称生成方法
     *
     * @param prefix
     * @param suffix
     * @return
     */
    public static String backupFileNameCreator(String prefix, String suffix) {
        Random random = new Random();
        int rand = random.nextInt(100);
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int date = calendar.get(Calendar.DATE);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        StringBuilder sb = new StringBuilder();
        sb.append(prefix).append(year).append(String.format("%02d", month)).append(String.format("%02d", date))
                .append(String.format("%02d", hour)).append(String.format("%02d", minute)).append(String.format("%02d", second))
                .append(String.format("%02d", rand)).append(suffix);
        return sb.toString();
    }
}