package com.xdf.whiteaccount.utils;

import java.math.BigDecimal;

/**
 * @program: xzy-management-system
 * @description: 计算工具类
 * @author: 张柯
 * @create: 2020-12-31 15:30
 **/
public class CalculateUtil {
    private CalculateUtil() {
    }

    /**
     * 加法
     *
     * @param var1
     * @param var2
     * @return
     */
    public static double add(Double var1, Double var2) {
        BigDecimal b1 = new BigDecimal(var1 == null ? 0D : var1);
        BigDecimal b2 = new BigDecimal(var2 == null ? 0D : var2);
        return b1.add(b2).doubleValue();
    }

    /**
     * @Description :加法
     * [val]
     * @Return : double
     * @Author : Zk
     * @Date : 2019/5/30 17:07
     * @Update : 2020/03/11
     */
    public static double add(Double... val) {
        BigDecimal bigDecimal = new BigDecimal(0D);
        for (Double v : val) {
            bigDecimal = bigDecimal.add(new BigDecimal(v == null ? 0D : v));
        }
        return bigDecimal.doubleValue();
    }

    /**
     * 加法
     * @param val
     * @return
     */
    public static BigDecimal add(BigDecimal... val) {
        BigDecimal result = new BigDecimal(0D);
        for (BigDecimal v : val) {
            result = result.add(v);
        }
        return result;
    }

    /**
     * 减法
     *
     * @param var1
     * @param var2
     * @return
     */
    public static double sub(double var1, double var2) {
        BigDecimal b1 = new BigDecimal(var1);
        BigDecimal b2 = new BigDecimal(var2);
        return b1.subtract(b2).doubleValue();
    }

    /**
     * 减法
     *
     * @param var1
     * @param var2
     * @return
     */
    public static BigDecimal sub(BigDecimal var1, BigDecimal var2) {
        var1 = var1 == null ? new BigDecimal(0) : var1;
        var2 = var2 == null ? new BigDecimal(0) : var2;
        return var1.subtract(var2);
    }

    /**
     * @Description :乘法
     * [var]
     * @Return : double
     * @Author : Zk
     * @Date : 2019/5/30 16:50
     */
    public static double mul(Double... val) {
        BigDecimal bigDecimal = new BigDecimal(1);
        for (Double d : val) {
            if (d == null) return new BigDecimal(0D).doubleValue();
            bigDecimal = bigDecimal.multiply(new BigDecimal(Double.toString(d)));
        }
        return bigDecimal.doubleValue();
    }

    /**
     * @Description :乘法
     * [val]
     * @Return : java.math.BigDecimal
     * @Author : Zk
     * @Date : 2019/5/30 16:55
     */
    public static BigDecimal mul(BigDecimal... val) {
        BigDecimal bigDecimal = new BigDecimal(0);
        for (BigDecimal bgdml : val) {
            bigDecimal = bigDecimal.multiply(bgdml != null ? bgdml : new BigDecimal(0));
        }
        return bigDecimal;
    }

    /**
     * 除法
     *
     * @param v1
     * @param v2
     * @param scale 精度，到小数点后几位
     * @return
     */
    public static double div(double v1, double v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException("The scale must be a positive integer or ");
        }
        if (v2 == 0D) return 0D;
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 除法
     *
     * @param v1
     * @param v2
     * @param scale 精度，到小数点后几位
     * @return
     */
    public static BigDecimal div(BigDecimal v1, BigDecimal v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException("The scale must be a positive integer or ");
        }
        BigDecimal result = new BigDecimal(0D);
        if (v2 == null || v2.doubleValue() == 0D) return result;
        if (v1 == null) v1 = new BigDecimal(0D);
        return v1.divide(v2, scale, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 四舍五入
     *
     * @param v
     * @param scale 精确位数
     * @return
     */
    public static double round(double v, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException("The scale must be a positive integer or zero");
        }
        BigDecimal b = new BigDecimal(v);
        BigDecimal one = new BigDecimal(1);
        return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
}