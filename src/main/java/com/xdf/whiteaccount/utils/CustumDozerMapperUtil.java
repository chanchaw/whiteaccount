package com.xdf.whiteaccount.utils;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: white-account
 * @description: 集合操作
 * @author: 张柯
 * @create: 2021-01-06 13:01
 **/
@Component
public class CustumDozerMapperUtil {
    private Mapper mapper;

    @Autowired
    public void setMapper(Mapper mapper) {
        this.mapper = mapper;
    }

    /**
     * 转换集合
     *
     * @param list
     * @param clazz
     * @return
     */
    public <T, V> List<V> transSourceToTarget(List<T> list, Class<V> clazz) {
        if (list == null) return null;
        List<V> target = new ArrayList<>();
        for (T t : list) {
            target.add(mapper.map(t, clazz));
        }
        return target;
    }

    /**
     * 转化对象
     *
     * @param source
     * @param clazz
     * @param <T>
     * @param <V>
     * @return
     */
    public <T, V> V transObject(T source, Class<V> clazz) {
        if (source == null) return null;
        return mapper.map(source, clazz);
    }

    /**
     * 是否为空
     *
     * @param list
     * @return
     */
    public static boolean isEmpty(List list) {
        return list == null || list.size() <= 0;
    }
}