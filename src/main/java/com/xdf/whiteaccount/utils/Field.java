package com.xdf.whiteaccount.utils;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

/**
 * @program: xzy-management-system
 * @description: 字段名
 * @author: 张柯
 * @create: 2021-04-11 14:05
 **/
@Data
@Builder
public class Field {
    private int serialNo;
    private String fieldName;
    private Class<?> fieldType;
    @Tolerate
    public Field() { }
}
