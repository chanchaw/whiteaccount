package com.xdf.whiteaccount.utils;

import java.io.File;

/**
 * @program: white-account
 * @description: 文件操作工具类
 * @author: 张柯
 * @create: 2021-06-19 14:35
 **/
public final class FileUtil {
    /**
     * 禁止实例化
     */
    private FileUtil() {
    }

    /**
     * 根据路径查询文件
     *
     * @param path
     * @return
     */
    public static File getFileByPath(String path) {
        return new File(path);
    }

}
