package com.xdf.whiteaccount.utils;

import org.springframework.util.DigestUtils;

/**
 * @program: xzy-management-system
 * @description: MD5工具
 * @author: 张柯
 * @create: 2021-01-27 21:03
 **/
public class MD5Util {
    /**
     * 转化MD5
     *
     * @param str
     * @return
     */
    public static String toMD5(String str) {
        if (str == null) return null;
        return DigestUtils.md5DigestAsHex(str.getBytes());
    }
}
