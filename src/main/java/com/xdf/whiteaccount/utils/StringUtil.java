package com.xdf.whiteaccount.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @program: xzy-management-system
 * @description:
 * @author: 张柯
 * @create: 2021-01-18 08:34
 **/
public class StringUtil {
    private StringUtil() {
    }

    /**
     * 首字母大写
     *
     * @param str
     * @return
     */
    public static String capitalize(String str) {
        if (StringUtils.isEmpty(str)) return null;
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    /**
     * 获取截取的字符串
     *
     * @param str
     * @param param
     * @param len
     * @return
     */
    public static String splitByParam(String str, String param, int len) {
        if (param == null) param = "";
        if (StringUtils.isEmpty(str)) return "";
        String[] arr = str.split(param);
        if (len > arr.length - 1) {
            return arr[arr.length - 1];
        }
        return arr[len];
    }

    /**
     * 转换大写
     * @param str
     * @return
     */
    public static String upper(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        return str.toUpperCase();
    }
}
