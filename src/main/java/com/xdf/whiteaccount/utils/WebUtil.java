package com.xdf.whiteaccount.utils;

import javax.servlet.http.HttpServletRequest;
import java.util.GregorianCalendar;

/**
 * @program: xzy-management-system
 * @description:
 * @author: 张柯
 * @create: 2021-01-22 12:27
 **/
public class WebUtil {
    /**
     * 是否Ajax请求
     *
     * @param request
     * @return
     */
    public static boolean isAjaxRequest(HttpServletRequest request) {
        String requestType = request.getHeader("X-Requested-With");
        if ("XMLHttpRequest".equals(requestType)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取欢迎语句
     *
     * @param userName
     * @return
     */
    public static String getWelcomeMessage(String userName) {
        GregorianCalendar calendar = new GregorianCalendar();
        int f = calendar.get(GregorianCalendar.AM_PM);
        int hour = calendar.get(GregorianCalendar.HOUR);
        StringBuilder sb = new StringBuilder();
        if (f == 0) {
            if (hour >= 11) {
                return sb.append("中午好!  ").append(userName).toString();
            } else if (hour <= 8) {
                return sb.append("早上好!  ").append(userName).toString();
            }
            return sb.append("上午好!  ").append(userName).toString();
        } else {
            if (hour >= 7) {
                return sb.append("晚上好!  ").append(userName).toString();
            }
            return sb.append("下午好!  ").append(userName).toString();
        }
    }
}
