package com.xdf.whiteaccount.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @program: white-account
 * @description: 色布计划实体类
 * @author: 张柯
 * @create: 2021-06-25 13:26
 **/
@Data
public class DyeFabricPlanDetailVO {
    private Long id;

    private String contactCompanyName;

    private String productName;

    private BigDecimal planKilo;

    private BigDecimal inputPairs;

    private BigDecimal inputKilo;

    private BigDecimal sendPairs;

    private BigDecimal sendKilo;

    private BigDecimal remainKilo;

    private BigDecimal planRemainKilo;

    private BigDecimal remainPairs;

    private BigDecimal price;

    private List<Map<String, Object>> inputDtl = new ArrayList<>();

    private List<Map<String, Object>> sendDtl = new ArrayList<>();

    private List<Map<String, Object>> storageDtl = new ArrayList<>();
}
