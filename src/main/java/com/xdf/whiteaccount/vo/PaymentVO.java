package com.xdf.whiteaccount.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @program: white-account
 * @description: 实体类
 * @author: 张柯
 * @create: 2021-05-31 16:55
 **/
@Data
@Builder
public class PaymentVO {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    /** 单据日期 */
    private Date orderDate;

    /** 客户名称 */
        private String clientName;

    /** 单据类型 */
    private String billType;

    /** 重量 */
    private BigDecimal qty;

    /** 单价 */
    private BigDecimal price;

    /** 金额 */
    private BigDecimal money;

    /** 入库类型 */
    private String type;

    /** 备注 */
    private String remarks;

    @Tolerate
    public PaymentVO() { }
}
