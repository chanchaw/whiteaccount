package com.xdf.whiteaccount.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigDecimal;

/**
 * @program: white-account
 * @description: 计划单明细
 * @author: 张柯
 * @create: 2021-05-22 08:57
 **/
@Data
public class PlanDetailVO {
    private String contactCompanyName;

    private String productName;

    private BigDecimal planKilo;

    private BigDecimal inputPairs;

    private BigDecimal inputKilo;

    private BigDecimal sendPairs;

    private BigDecimal sendKilo;

    private BigDecimal remainKilo;

    private BigDecimal planRemainKilo;

    private BigDecimal remainPairs;

    private BigDecimal greyfabricPrice;

    private List<Map<String, Object>> inputDtl = new ArrayList<>();

    private List<Map<String, Object>> sendDtl = new ArrayList<>();

    private List<Map<String, Object>> storageDtl = new ArrayList<>();
}
