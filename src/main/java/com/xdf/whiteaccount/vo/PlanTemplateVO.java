package com.xdf.whiteaccount.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xdf.whiteaccount.entity.PlanTemplate;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @program: white-account
 * @description: 页面显示
 * @author: 张柯
 * @create: 2021-05-20 19:06
 **/
@Data
public class PlanTemplateVO {
    /** 自增主键 */
    private Long id;

    /** 外键,往来单位 */
    private String contactCompanyId;

    /** 合同号 */
    private String contractCode;

    /** 计划单号 */
    private String planCode;

    /** 订单号 */
    private String orderCode;

    /** 品名 */
    private String productName;

    /** 原料规格 */
    private String materialSpecification;

    /** 原料批号 */
    private String materialLotNo;

    /** 原料供应商 */
    private String materialSupplierId;

    /** 白坯克重 */
    private String greyfabricWeight;

    /** 白坯门幅 */
    private String greyfabricWidth;

    /** 头份(规格) */
    private String greyfabricSpecification;

    /** 坯布价 */
    private BigDecimal greyfabricPrice;

    /** 毛高 */
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private String mHeight;

    /** 计划重量 */
    private BigDecimal planKilo;

    /** 白坯布日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date greyfabricDate;

    /** 大货交期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date submitDate;

    /** 原料价格 */
    private BigDecimal materialPrice;

    /** 加工费 */
    private BigDecimal processingCost;

    /** 中丝规格 */
    private String middleSpecification;

    /** 外键,中丝供应商 */
    private String middleSupplierId;

    /** 底丝规格 */
    private String bottomSpecification;

    /** 底丝供应商 */
    private String bottomSupplierId;

    /** 机型 */
    private String machineType;

    /** 针数 */
    private BigDecimal needleAmount;

    /** 下机克重 */
    private String lowWeight;

    /** 下机门幅 */
    private String lowWidth;

    /** 计划人员 */
    private String planEmployeeId;

    /** 生产人员 */
    private String productEmployeeId;

    /** 复核人员 */
    private String checkEmployeeId;

    /** 喷码厂名 */
    private String markCompanyName;

    /** 坯布要求 */
    private String fabricRequirement;

    /** 是否设置为默认模板 */
    private Boolean setDefault;

    /** 备注 */
    private String remarks;

    /** 创建人编号 */
    private Integer createUserId;

    public String getmHeight() {
        return mHeight;
    }

    public void setmHeight(String mHeight) {
        this.mHeight = mHeight;
    }
}
