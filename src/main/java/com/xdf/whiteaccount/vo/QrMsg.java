package com.xdf.whiteaccount.vo;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

/**
 * @program: white-account
 * @description: 二维码实体类
 * @author: 张柯
 * @create: 2021-06-10 20:06
 **/
@Data
@Builder
public class QrMsg {
    private String qrCodeUrl;
    private String token;
    private Long timeout;

    @Tolerate
    public QrMsg() { }
}
