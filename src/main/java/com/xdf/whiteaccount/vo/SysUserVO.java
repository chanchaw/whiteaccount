package com.xdf.whiteaccount.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @program: white-account
 * @description: 用户表
 * @author: 张柯
 * @create: 2021-07-26 08:15
 **/
@Data
@Builder
public class SysUserVO {

    /** 无备注 */
    private Integer id;

    /** 登录用户名 */
    private String loginName;

    /** 登录密码 */
    private String loginPassword;

    /** 姓名 */
    private String userName;

    /** 昵称 */
    private String nickName;

    /** 性别：男(1);女(0) */
    private Boolean userSex;

    /** 备注 */
    private String remarks;

    /** 住址 */
    private String userAddress;

    /** 用户电话号码 */
    private String userPhone;

    /** 状态(1:在职,2:离职,0:停用) */
    private Integer userState;

    /** 创建时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /** 微信的openid */
    private String openid;

    /** 本字段数据库中不存在 */
    private String groupId;

    /** 本字段数据库中不存在 */
    private String groupName;

    @Tolerate
    public SysUserVO() { }
}
