/**
  往来单位
  2021年5月14日
 */
create table contact_company
(
    id VARCHAR(50) not null comment '主键',
    company_name VARCHAR(100) not null comment '公司名称',
    company_code VARCHAR(100) null comment '公司编码',
    company_alias VARCHAR(100) null comment '公司简称',
    settlement_client VARCHAR(100) null comment '结算客户',
    business_manager VARCHAR(50) null comment '业务员',
    phone_number VARCHAR(60) null comment '电话号码',
    area VARCHAR(50) null comment '地区',
    address VARCHAR(100) null comment '地址',
    post_code VARCHAR(50) null comment '邮编',
    bank_of_deposit VARCHAR(100) null comment '开户行',
    bank_account VARCHAR(100) null comment '银行账号',
    duty_paragraph VARCHAR(100) null comment '税号',
    create_user_id INT null comment '创建人编号',
    create_time DATETIME default CURRENT_TIMESTAMP null comment '创建时间，DB自动生成',
    remarks VARCHAR(250) null comment '备注',
    serial_no int null comment '排序序号',
    state INT default 1 not null comment '状态(0:删除;1:正常;)',
    constraint contact_company_pk
        primary key (id)
)
    comment '往来单位';

create index contact_company_business_manager_index
    on contact_company (business_manager);

create index contact_company_company_code_index
    on contact_company (company_code);

create unique index contact_company_company_name_uindex
    on contact_company (company_name);

create index contact_company_create_user_id_index
    on contact_company (create_user_id);
