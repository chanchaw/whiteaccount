/**
  供应商
  2021年5月14日
 */
create table supplier
(
    id VARCHAR(50) not null comment '主键',
    supplier_name VARCHAR(100) not null comment '公司名称',
    supplier_code VARCHAR(100) null comment '公司编码',
    supplier_alias VARCHAR(100) null comment '公司简称',
    settlement_client VARCHAR(100) null comment '结算客户',
    phone_number VARCHAR(60) null comment '电话号码',
    area VARCHAR(50) null comment '地区',
    address VARCHAR(100) null comment '地址',
    post_code VARCHAR(50) null comment '邮编',
    create_user_id INT null comment '创建人编号',
    create_time DATETIME default CURRENT_TIMESTAMP null comment '创建时间，DB自动生成',
    remarks VARCHAR(250) null comment '备注',
    serial_no int null comment '排序序号',
    constraint supplier_pk
        primary key (id)
)
    comment '供应商';


create index supplier_supplier_code_index
    on supplier (supplier_code);

create unique index supplier_supplier_name_uindex
    on supplier (supplier_name);

create index supplier_create_user_id_index
    on supplier (create_user_id);
