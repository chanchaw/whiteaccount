/**
  规格
  2021年5月14日
 */
create table specification
(
    id BIGINT auto_increment comment '自增主键',
    spec_name VARCHAR(100) not null comment '规格名',
    remarks VARCHAR(200) null comment '备注',
    create_time DATETIME default CURRENT_TIMESTAMP null comment '创建时间',
    constraint specification_pk
        primary key (id)
)
    comment '规格';

create unique index specification_spec_name_uindex
    on specification (spec_name);
