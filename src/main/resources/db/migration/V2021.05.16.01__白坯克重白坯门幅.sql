create table weight_of_fabric
(
    id BIGINT auto_increment comment '自增主键',
    wof_name VARCHAR(150) null comment '白坯克重',
    remarks VARCHAR(255) null comment '备注',
    create_time DATETIME default CURRENT_TIMESTAMP null comment '创建时间',
    constraint weight_of_fabric_pk
        primary key (id)
)
    comment '克重';

create table width_of_fabric
(
    id BIGINT auto_increment comment '自增主键',
    wof_name VARCHAR(150) null comment '白坯门幅',
    remarks VARCHAR(255) null comment '备注',
    create_time DATETIME default CURRENT_TIMESTAMP null comment '创建时间',
    constraint weight_of_fabric_pk
        primary key (id)
)
    comment '门幅';