
CREATE TABLE machine_type
(
    id           VARCHAR(50)  NOT NULL COMMENT '主键',
    machine_name VARCHAR(100) NULL COMMENT '机台名称',
    CONSTRAINT machine_type_id_uindex UNIQUE (id)
) COMMENT '机台号';

ALTER TABLE machine_type
    ADD PRIMARY KEY (id);