create table product_info
(
    id int auto_increment comment '自增主键',
    product_name VARCHAR(100) not null comment '品名',
    product_code VARCHAR(100) null comment '产品编码',
    remarks VARCHAR(155) null comment '备注',
    constraint product_info_pk
        primary key (id)
)
    comment '基础资料-品名';

create unique index product_info_product_name_uindex
    on product_info (product_name);