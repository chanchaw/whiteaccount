create table employee
(
    id VARCHAR(50) not null comment '主键',
    emp_name VARCHAR(50) not null comment '姓名',
    emp_sex TINYINT(1) default 1 null comment '性别(1:男;0:女;)',
    emp_phone_number VARCHAR(50) null comment '电话号码',
    emp_age int default 18 null comment '员工年龄',
    address VARCHAR(100) null comment '员工住址',
    remarks VARCHAR(200) null comment '备注',
    state int null comment '状态(0:禁用;1:在职;2:离职)',
    create_time DATETIME default CURRENT_TIMESTAMP null comment '创建时间',
    create_user_id int null comment '外键,创建人',
    constraint employee_pk
        primary key (id)
)
    comment '部门员工';