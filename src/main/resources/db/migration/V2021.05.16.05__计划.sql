create table plan
(
    id BIGINT auto_increment comment '自增主键',
    contact_company_id VARCHAR(50) not null comment '外键,往来单位',
    contract_code VARCHAR(100) null comment '合同号',
    plan_code VARCHAR(100) null comment '计划单号',
    plan_date DATE null comment '计划日期',
    order_code VARCHAR(100) null comment '订单号',
    product_name VARCHAR(100) null comment '品名',
    material_specification VARCHAR(100) null comment '原料规格',
    material_lot_no VARCHAR(100) null comment '原料批号',
    material_supplier_id VARCHAR(50) null comment '原料供应商',
    greyfabric_weight VARCHAR(100) null comment '白坯克重',
    greyfabric_width VARCHAR(100) null comment '白坯门幅',
    greyfabric_specification VARCHAR(100) null comment '头份(规格)',
    greyfabric_price DECIMAL(60,2) null comment '坯布价',
    m_height DECIMAL(60,1) null comment '毛高',
    plan_kilo DECIMAL(60,1) null comment '计划重量',
    greyfabric_date DATETIME null comment '白坯布日期',
    submit_date DATETIME null comment '大货交期',
    material_price DECIMAL(60,2) null comment '原料价格',
    processing_cost DECIMAL(60,2) null comment '加工费',
    middle_specification VARCHAR(100) null comment '中丝规格',
    middle_supplier_id VARCHAR(50) null comment '外键,中丝供应商',
    bottom_specification VARCHAR(100) null comment '底丝规格',
    bottom_supplier_id VARCHAR(50) null comment '底丝供应商',
    machine_type VARCHAR(50) null comment '机型',
    needle_amount DECIMAL(60,1) null comment '针数',
    low_weight VARCHAR(100) null comment '下机克重',
    low_width VARCHAR(100) null comment '下机门幅',
    plan_employee_id VARCHAR(50) null comment '计划人员',
    product_employee_id VARCHAR(50) null comment '生产人员',
    check_employee_id VARCHAR(50) null comment '复核人员',
    mark_company_name VARCHAR(50) null comment '喷码厂名',
    fabric_requirement VARCHAR(150) null comment '坯布要求',
    remarks VARCHAR(300) null comment '备注',
    create_user_id int null comment '外键,创建人',
    create_time DATETIME default CURRENT_TIMESTAMP null comment '创建日期，DB自动填充',
    state INT default 1 null comment '状态(0:作废;1:正常;)',
    constraint plan_pk
        primary key (id),
    constraint plan_contact_company_id_fk
        foreign key (contact_company_id) references contact_company (id),
    constraint plan_bottom_supplier_id_supplier_id_fk
        foreign key (bottom_supplier_id) references supplier (id),
    constraint plan_material_supplier_id_supplier_id_fk
        foreign key (material_supplier_id) references supplier (id),
    constraint plan_middle_supplier_id_supplier_id_fk
        foreign key (middle_supplier_id) references supplier (id)
)
    comment '计划';

create unique index plan_plan_code_uindex
    on plan (plan_code);
