create table sys_user
(
    id int auto_increment,
    login_name VARCHAR(50) not null comment '登录用户名',
    login_password VARCHAR(100) default '' not null comment '登录密码',
    user_name VARCHAR(50) null comment '姓名',
    user_sex TINYINT(1) default 1 null comment '性别：男(1);女(0)',
    remarks VARCHAR(100) null comment '备注',
    user_address VARCHAR(100) null comment '住址',
    user_phone VARCHAR(68) null comment '用户电话号码',
    user_state int default 1 null comment '状态(1:在职,2:离职,0:停用)',
    create_time DATETIME DEFAULT CURRENT_TIMESTAMP comment '创建时间',
    constraint sys_user_pk
        primary key (id)
)
    comment '登录用户表';

create unique index sys_user_login_name_uindex
    on sys_user (login_name);