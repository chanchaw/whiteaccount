/**
  角色表
 */
create table sys_role
(
    id int auto_increment comment '自增主键',
    role_name VARCHAR(50) not null comment '角色名称',
    role_admin_sign TINYINT(1) default 0 null comment '是否管理员(1:是;0:否;)',
    role_state int default 1 not null comment '角色状态(1:正常;0:停用)',
    constraint sys_role_pk
        primary key (id)
)
    comment '角色表';

create unique index sys_role_role_name_uindex
    on sys_role (role_name);

/**
  菜单表
 */
create table sys_menu
(
    id int auto_increment comment '自增主键',
    menu_name VARCHAR(45) null comment '菜单名',
    menu_path VARCHAR(50) null comment '菜单相对路径',
    menu_icon VARCHAR(50) null comment '菜单图标',
    constraint sys_menu_pk
        primary key (id)
)
    comment '菜单表';

/**
  权限表
 */
create table sys_permission
(
    id int auto_increment comment '自增主键',
    sys_menu_id int not null comment '外键,菜单编号',
    sys_role_id int not null comment '角色编号',
    sys_show INT default 0 null comment '显示权限',
    sys_add int default 0 null comment '新增权限',
    sys_modify int default 0 null comment '修改权限',
    sys_delete int default 0 null comment '删除权限',
    sys_audit int default 0 null comment '审核权限',
    sys_print int default 0 null comment '打印权限',
    sys_export int default 0 null comment '导出权限',
    sys_price int default 0 null comment '单价权限',
    sys_money int default 0 null comment '金额权限',
    constraint sys_permission_pk
        primary key (id)
)
    comment '权限表';

/**
  角色组表
 */
create table sys_role_group
(
    id int null comment '自增主键',
    group_name VARCHAR(50) not null comment '角色组名称',
    role_group_admin_sign TINYINT(1) default 0 null comment '是否管理员(1:是;0:否;)'
)
    comment '角色组';

/**
  角色组对应表
 */
create table sys_role_group_dtl
(
    id int auto_increment comment '自增主键',
    role_id int not null comment '角色编号',
    role_group_id int not null comment '角色组编号',
    constraint sys_role_group_dtl_pk
        primary key (id)
)
    comment '角色组与角色的对应关系表';

create index sys_role_group_dtl_role_group_id_index
    on sys_role_group_dtl (role_group_id);

create index sys_role_group_dtl_role_id_index
    on sys_role_group_dtl (role_id);

/**
  用户与角色组对应关系表
 */
create table sys_user_role_group
(
    id int auto_increment comment '自增主键',
    sys_user_id int null comment '用户表主键',
    sys_role_group_id int null comment '角色组主键',
    constraint sys_user_role_group_pk
        primary key (id)
)
    comment '用户与角色组对应关系表';

create index sys_user_role_group_sys_role_group_id_index
    on sys_user_role_group (sys_role_group_id);

create index sys_user_role_group_sys_user_id_index
    on sys_user_role_group (sys_user_id);
