/**
  报表网格配置表
 */
create table sys_user_table_config
(
    id int auto_increment comment '自增主键',
    sys_menu_id int not null comment '菜单对应编号',
    sys_user_id int not null comment '用户编号',
    field_name VARCHAR(50) null comment '网格列名称',
    field_width DOUBLE null comment '列宽',
    field_align VARCHAR(20) null comment '列对齐方式',
    field_hidden TINYINT(1) default 1 null comment '列隐藏',
    field_serial_num int null comment '列排序字段',
    field_type VARCHAR(35) default 'string' null comment '列字段类别(string,number,date)',
    field_default_value VARCHAR(50) default '' null comment '列初始值',
    constraint sys_user_table_config_pk
        primary key (id)
)
    comment '用户报表列参数配置';

create index sys_user_table_config_sys_menu_id_index
    on sys_user_table_config (sys_menu_id);

create index sys_user_table_config_sys_user_id_index
    on sys_user_table_config (sys_user_id);

/**
  配置表
 */
create table sys_config_int
(
    config_field VARCHAR(100) not null comment '属性名',
    config_value int null comment '属性值',
    config_desc VARCHAR(100) null comment '属性描述'
)
    comment '配置表';

create unique index sys_config_int_config_field_uindex
    on sys_config_int (config_field);

alter table sys_config_int
    add constraint sys_config_int_pk
        primary key (config_field);
