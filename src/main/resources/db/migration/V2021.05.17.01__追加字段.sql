/**
  追加字段
 */
SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `sys_menu` ADD COLUMN `parent_id` int(11) NULL DEFAULT NULL COMMENT '父节点' AFTER `id`;
ALTER TABLE `sys_menu` ADD INDEX `sys_menu_parent_id_index`(`parent_id`) USING BTREE;
ALTER TABLE `sys_role_group` MODIFY COLUMN `id` int(11) NOT NULL COMMENT '自增主键' FIRST;
ALTER TABLE `sys_role_group` ADD PRIMARY KEY (`id`) USING BTREE;
ALTER TABLE `sys_role_group` MODIFY COLUMN `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键';
SET FOREIGN_KEY_CHECKS=1;