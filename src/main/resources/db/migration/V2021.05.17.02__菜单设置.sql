/*
    更改菜单表(新增字段)
*/
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE `sys_menu`;

INSERT INTO `sys_menu` VALUES (100, NULL, '计划管理', '/', 'eject-aside-icon-gur-project-08');
INSERT INTO `sys_menu` VALUES (101, 100, '计划单序时表', 'jh_plan_report', NULL);
INSERT INTO `sys_menu` VALUES (200, NULL, '应收管理', '/', 'eject-aside-icon-gur-project-01');
INSERT INTO `sys_menu` VALUES (300, NULL, '应付管理', '/', 'eject-aside-icon-gur-project-18');
INSERT INTO `sys_menu` VALUES (400, NULL, '基础资料', '/', 'eject-aside-icon-gur-project-34');
INSERT INTO `sys_menu` VALUES (401, 400, '客户', 'jc_contactcompany', '');
INSERT INTO `sys_menu` VALUES (402, 400, '供应商', 'jc_supplier', NULL);
INSERT INTO `sys_menu` VALUES (403, 400, '品名', 'jc_product_info', '');
INSERT INTO `sys_menu` VALUES (404, 400, '原料规格', 'jc_specification', NULL);
INSERT INTO `sys_menu` VALUES (405, 400, '白坯克重', 'jc_weight_of_fabric', NULL);
INSERT INTO `sys_menu` VALUES (406, 400, '白坯门幅', 'jc_width_of_fabric', '');
INSERT INTO `sys_menu` VALUES (407, 400, '机台号', 'jc_machine_type', '');
INSERT INTO `sys_menu` VALUES (500, NULL, '系统管理', '/', 'eject-aside-icon-gur-project-31');
INSERT INTO `sys_menu` VALUES (501, 500, '权限表', 'jc_permission', NULL);
INSERT INTO `sys_menu` VALUES (502, 500, '用户表', 'jc_user', NULL);
INSERT INTO `sys_menu` VALUES (503, 500, '角色组', 'sz_role_group', NULL);
INSERT INTO `sys_menu` VALUES (504, 500, '配置信息', 'sz_config', NULL);

TRUNCATE TABLE sys_permission;
INSERT INTO sys_permission (sys_menu_id, sys_role_id)
SELECT m.id, b.id
FROM sys_menu m
         JOIN sys_role b;

SET FOREIGN_KEY_CHECKS = 1;
