/**
  设置权限
 */
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE sys_role;
INSERT INTO sys_role (role_name, role_admin_sign, role_state)
VALUES ('管理员', 1, 1);
TRUNCATE TABLE sys_permission;
INSERT INTO sys_permission (sys_menu_id, sys_role_id, sys_show, sys_add, sys_modify, sys_delete, sys_audit, sys_print,
                            sys_export, sys_price, sys_money)
SELECT a.id,
       b.id,
       1,
       1,
       1,
       1,
       1,
       1,
       1,
       1,
       1
FROM sys_menu a
         JOIN sys_role b;
TRUNCATE TABLE sys_role_group;
INSERT INTO sys_role_group (group_name, role_group_admin_sign)
VALUES ('管理员', 1);
TRUNCATE TABLE sys_role_group_dtl;
INSERT INTO sys_role_group_dtl (role_id, role_group_id)
VALUES (1, 1);
SET FOREIGN_KEY_CHECKS = 1;