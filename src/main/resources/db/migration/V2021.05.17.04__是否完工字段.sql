/**
  是否完工字段
  2021年5月17日
 */
alter table plan
    add mark_finished TINYINT(1) default 0 null comment '是否完工字段' after create_time;