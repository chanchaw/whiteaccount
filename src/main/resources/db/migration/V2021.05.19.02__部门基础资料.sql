/*
 Navicat Premium Data Transfer

 Source Server         : 本地

 Date: 19/05/2021 20:20:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '上级元素',
  `d_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `d_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号(不重复)',
  `d_state` int(11) NULL DEFAULT 1 COMMENT '状态(1:正常;0:停用)',
  `d_default` tinyint(1) NULL DEFAULT 0 COMMENT '默认',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `department_d_code_uindex`(`d_code`) USING BTREE,
  INDEX `department_d_name_index`(`d_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
