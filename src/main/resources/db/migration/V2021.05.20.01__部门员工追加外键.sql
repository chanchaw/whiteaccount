/**
  部门员工追加外键
  2021年5月20日
 */
alter table employee
    add department_id int null comment '外键,对应department.id';

alter table employee
    add constraint employee_department_id_fk
        foreign key (department_id) references department (id);