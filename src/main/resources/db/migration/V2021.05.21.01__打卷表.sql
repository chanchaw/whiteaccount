/*

*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for g_jrkbill
-- ----------------------------
DROP TABLE IF EXISTS `g_jrkbill`;
CREATE TABLE `g_jrkbill`  (
  `B_ItemID` int(11) NOT NULL AUTO_INCREMENT,
  `B_ID` bigint NULL DEFAULT NULL,
  `B_GJ` decimal(10, 1) NULL DEFAULT NULL,
  `B_AorB` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `B_JTH` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `B_Class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `B_Date` datetime(0) NULL DEFAULT NULL,
  `B_CN` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `B_IP` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `B_BCFC` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `B_PH` int(11) NULL DEFAULT NULL,
  `B_EDP` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `B_DTRK` datetime(0) NULL DEFAULT NULL,
  `B_FPDID` int(11) NULL DEFAULT NULL,
  `B_upload` int(11) NULL DEFAULT 0,
  `b_barcode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`B_ItemID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
