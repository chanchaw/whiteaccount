/*
 
 Date: 21/05/2021 16:57:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for jrkbillfh
-- ----------------------------
DROP TABLE IF EXISTS `jrkbillfh`;
CREATE TABLE `jrkbillfh`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qty` decimal(50, 2) NULL DEFAULT NULL COMMENT '发货时每匹公斤数',
  `aorb` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AB面',
  `isorder` int(255) NULL DEFAULT NULL COMMENT '发货序号',
  `computername` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '计算机名',
  `isfh` int(255) NULL DEFAULT NULL COMMENT '标记是否发货 1表示发货',
  `isdelete` int(255) NULL DEFAULT NULL COMMENT '标记当前行数据是否删除 1表示已经删除',
  `planid` bigint NULL DEFAULT NULL COMMENT '对应订单的主键(即计划主键)',
  `fid` int(11) NULL DEFAULT NULL COMMENT '生成发货单对应发货明细表主键',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
