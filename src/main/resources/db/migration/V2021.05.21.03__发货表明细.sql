/*

*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for whitebilldetailfh
-- ----------------------------
DROP TABLE IF EXISTS `whitebilldetailfh`;
CREATE TABLE `whitebilldetailfh`  (
  `itemid` int(11) NOT NULL AUTO_INCREMENT COMMENT '发货明细表主键',
  `fid` int(11) NULL DEFAULT NULL COMMENT '主表主键',
  `qty` decimal(60, 1) NULL DEFAULT NULL COMMENT '明细总重量',
  `ps` int(10) NULL DEFAULT NULL COMMENT '匹数',
  `planid` bigint NULL DEFAULT NULL COMMENT '计划单主键',
  PRIMARY KEY (`itemid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
