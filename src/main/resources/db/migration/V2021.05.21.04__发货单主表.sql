/*

*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for whitebillfh
-- ----------------------------
DROP TABLE IF EXISTS `whitebillfh`;
CREATE TABLE `whitebillfh`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '发货主表主键',
  `billcode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `client` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '染厂地址',
  `datafh` datetime(0) NULL DEFAULT NULL COMMENT '发货时间',
  `aorb` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updatefh` int(11) NULL DEFAULT NULL COMMENT '是否修改 （1：修改，0/空：未修改）',
  `upload` int(11) NULL DEFAULT NULL COMMENT '是否上传 (1：已上传)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
