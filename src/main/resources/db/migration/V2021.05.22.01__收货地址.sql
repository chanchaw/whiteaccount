/**
  2021年5月22日 收货地址
 */
create table take_delivery_address
(
    id int auto_increment comment '自增主键',
    company VARCHAR(60) null comment '收货单位',
    address VARCHAR(100) null comment '详细地址',
    phone VARCHAR(20) null comment '电话号码',
    constraint take_delivery_address_pk
        primary key (id)
)
    comment '收货地址';