/**
  2021年5月22日 追加字段
 */
alter table plan
    add notice_date DATE null comment '通知日期' after plan_date;

alter table plan
    add middle_tf VARCHAR(100) null comment '中丝头份' after middle_supplier_id;

alter table plan
    add bottom_tf VARCHAR(100) null comment '底丝头份' after bottom_supplier_id;
