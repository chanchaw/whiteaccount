DROP PROCEDURE IF EXISTS `p_plan_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_plan_dtl`(IN `id` BIGINT)
BEGIN
    /**
      2021年5月22日 计划单明细查看
     */
    SELECT concat('客户:', b.company_name, '-订单日期:', DATE(a.plan_date), '-计划:', round(ifnull(a.plan_kilo, 0), 1),
                  'kg-已发:', round(ifnull(c.kilo, 0), 1), 'kg-剩余:', round(ifnull(c.kilo, 0) - ifnull(d.kilo, 0), 1),
                  'kg')                                                                   company_name,
           concat('品名:', ifnull(a.product_name, ''), '-规格:', ifnull(a.material_specification, ' '), '-毛高:',
                  ifnull(a.m_height, 0), '-头份:', ifnull(a.greyfabric_specification, ' ')) product_name,
           ifnull(a.plan_kilo, 0)                                                         plan_kilo,
           ifnull(c.pairs, 0)                                                             input_pairs,
           ifnull(c.kilo, 0)                                                              input_kilo,
           ifnull(d.kilo, 0)                                                              send_kilo,
           ifnull(d.pairs, 0)                                                             send_pairs,
           ifnull(round(ifnull(c.kilo, 0) - ifnull(d.kilo, 0), 1), 0)                     remain_kilo,
           ifnull(round(ifnull(c.pairs, 0) - ifnull(d.pairs, 0), 0), 0)                   remain_pairs,
           ifnull(a.greyfabric_price, 0)                                                  greyfabric_price
    FROM plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN (
        # 总入库公斤
        SELECT B_ID id, count(*) pairs, ifnull(sum(round(B_GJ, 1)), 0) kilo FROM g_jrkbill GROUP BY B_ID) c
                       ON c.id = a.id
             LEFT JOIN (
        # 已发公斤
        SELECT planid id, ifnull(sum(round(qty, 1)), 0) kilo, count(*) pairs FROM jrkbillfh GROUP BY planid) d
                       ON d.id = a.id
    WHERE a.id = `id`;
END;;
DELIMITER ;