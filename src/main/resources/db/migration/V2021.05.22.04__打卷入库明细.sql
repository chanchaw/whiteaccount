DROP PROCEDURE IF EXISTS `p_jrkbill_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_jrkbill_dtl`(IN `id` BIGINT)
BEGIN
    /**
       2021年5月22日 打卷入库明细
     */
    SELECT a.machine_type,
           count(*)              input_pairs,
           sum(round(b.B_GJ, 1)) input_kilo,
           b.B_AorB,
           a.greyfabric_specification,
           a.material_specification,
           b.B_DTRK
    FROM plan a
             LEFT JOIN g_jrkbill b ON a.id = b.B_ID
    WHERE a.id = `id`
    GROUP BY b.B_AorB, b.B_DTRK;
END;;
DELIMITER ;
