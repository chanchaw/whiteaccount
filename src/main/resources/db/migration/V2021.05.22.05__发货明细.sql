DROP PROCEDURE IF EXISTS `p_jrkbillfh_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_jrkbillfh_dtl`(IN `id` BIGINT)
BEGIN
    /**
       2021年5月22日 发货明细
     */
    SELECT a.client                   client_name,
           ifnull(b.ps, 0)            send_pairs,
           sum(round(b.planid, 1))    send_kilo,
           c.aorb                     aorb,
           d.greyfabric_specification greyfabric_specification,
           d.material_specification   material_specification,
           a.datafh                   send_date
    FROM whitebillfh a
             LEFT JOIN whitebilldetailfh b ON a.id = b.fid
             LEFT JOIN jrkbillfh c ON b.itemid = c.fid
             LEFT JOIN plan d ON b.planid = d.id
    WHERE b.planid = `id`;
END;;
DELIMITER ;