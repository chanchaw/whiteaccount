/**
  2021年5月25日 原料入库
 */
create table material_order
(
    id BIGINT auto_increment comment '自增主键',
    supplier_id VARCHAR(50) null comment '供应商',
    batch_num VARCHAR(50) null comment '批次号',
    material_spec VARCHAR(100) null comment '原料规格',
    box_amount DECIMAL(60,1) null comment '箱数',
    input_amount DECIMAL(60,1) null comment '入库数量',
    input_money DECIMAL(60,2) null comment '金额',
    remarks VARCHAR(255) null comment '备注',
    constraint material_order_pk
        primary key (id),
    constraint material_order_supplier_id_fk
        foreign key (supplier_id) references supplier (id)
)
    comment '原料入库';

alter table material_order
    add create_user_id int not null comment '外键,对应sys_user.id';

alter table material_order
	add create_time DATETIME default CURRENT_TIMESTAMP null comment '创建时间';
