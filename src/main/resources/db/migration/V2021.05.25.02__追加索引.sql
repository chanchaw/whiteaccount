/**
  2021年5月25日 追加索引
 */
create index material_order_create_user_id_index
    on material_order (create_user_id);