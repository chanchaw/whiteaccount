/**
    2021年5月25日 追加单据类型
 */
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `bill_type`;
CREATE TABLE `bill_type`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '单据类型编号',
  `bill_type_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '单据类型名称',
  `access_mode` int(11) NULL DEFAULT NULL COMMENT '数据影响(0,1,-1)',
  `allow_manual` tinyint(1) NULL DEFAULT 1 COMMENT '是否允许手动生成',
  `storage_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属仓库编号',
  `inout_type` int(11) NULL DEFAULT 1 COMMENT '扣库类型(本期入库:1;本期出库:-1)',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `bill_type_bill_type_name_uindex`(`bill_type_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据类型' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
