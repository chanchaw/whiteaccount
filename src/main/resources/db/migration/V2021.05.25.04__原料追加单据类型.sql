/**
  原料追加单据类型
 */
alter table material_order
    add bill_type VARCHAR(50) null comment '单据类型' after id;

create index material_order_bill_type_index
    on material_order (bill_type);
