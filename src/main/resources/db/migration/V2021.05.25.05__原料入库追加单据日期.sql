/**
  2021年5月25日 原料入库追加单据日期
 */
alter table material_order
    add order_date DATE null comment '制单日期' after id;