/**
  2021年5月25日 追加状态
 */
alter table material_order
    add state INT default 1 null comment '状态(0:作废;1:正常;)';