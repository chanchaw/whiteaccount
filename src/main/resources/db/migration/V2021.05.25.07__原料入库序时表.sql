DROP PROCEDURE IF EXISTS `p_material_order_report`;
DELIMITER ;;
CREATE PROCEDURE `p_material_order_report`(IN `start` DATE, IN `end` DATE)
BEGIN
    /**
      2021年5月25日  原料入库序时表
     */
    SELECT a.id,                                -- 主键
           DATE(a.order_date) order_date,       -- 制单日期,
           a.order_code       order_code,       -- 单据编号
           a.create_time,                       -- 创建时间
           a.input_money,                       -- 入库金额
           a.input_amount,                      -- 入库数量
           a.batch_num,                         -- 批次号
           a.box_amount,                        -- 箱数
           a.material_spec,                     -- 原料规格
           a.supplier_id,                       -- 供应商编号
           b.supplier_name,                     -- 供应商名称
           c.user_name        create_user_name, -- 创建人名称
           d.bill_type_name                     -- 单据类型名称
    FROM material_order a
             LEFT JOIN supplier b ON a.supplier_id = b.id
             LEFT JOIN sys_user c ON a.create_user_id = c.id
             LEFT JOIN bill_type d ON a.bill_type = d.id
    WHERE a.state = 1
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`);
END ;;