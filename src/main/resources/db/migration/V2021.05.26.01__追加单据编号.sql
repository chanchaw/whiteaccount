/**
  2021年5月26日 追加单据编号
 */
alter table material_order
    add order_code VARCHAR(60) not null comment '单据编号' after order_date;

create unique index material_order_order_code_uindex
    on material_order (order_code);
