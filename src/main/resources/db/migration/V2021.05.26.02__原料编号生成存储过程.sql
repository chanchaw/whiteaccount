/*
 材料生成存储过程
 Date: 18/05/2021 16:10:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP FUNCTION IF EXISTS `f_material_codegen`;
DELIMITER ;;
CREATE FUNCTION `f_material_codegen`(prefix VARCHAR(50)) RETURNS VARCHAR(100) CHARSET utf8
BEGIN
    DECLARE currentDate VARCHAR(50); -- 日期
    DECLARE curNum BIGINT; -- 当前记数
    DECLARE result VARCHAR(100); -- 结果
    DECLARE curNumCount INT; -- 记数位数
    DECLARE currentDateCount INT; -- 日期位数
    SELECT ifnull(config_value, 3) INTO curNumCount FROM sys_config_int WHERE config_field = '原料编号记数位数';
    SELECT ifnull(config_value, 6) INTO currentDateCount FROM sys_config_int WHERE config_field = '原料编号日期位数';
    IF ifnull(curNumCount, 0) = 0 THEN SET curNumCount = 3; END IF;
    IF ifnull(currentDateCount, 0) = 0 THEN SET currentDateCount = 6; END IF;
    SELECT right(concat(DATE_FORMAT(NOW(), '%Y'), DATE_FORMAT(NOW(), '%m'), DATE_FORMAT(NOW(), '%d')), currentDateCount)
    INTO currentDate;

    -- 创建临时表
    DROP TEMPORARY TABLE IF EXISTS t_num,t_num_tmp;
    CREATE TEMPORARY TABLE t_num
    (
        pk_id       BIGINT AUTO_INCREMENT PRIMARY KEY,
        count_value BIGINT
    );
    CREATE TEMPORARY TABLE t_num_tmp
    (
        pk_id       BIGINT AUTO_INCREMENT PRIMARY KEY,
        count_value BIGINT
    );
    INSERT INTO t_num (count_value)
    SELECT cast(substring(order_code, length(`prefix`) + length(`currentDate`) + 1, curNumCount) AS DECIMAL(24)) num
    FROM material_order
    WHERE order_code LIKE concat(`prefix`, `currentDate`, '%');

    INSERT INTO t_num_tmp (count_value)
    SELECT cast(substring(order_code, length(`prefix`) + length(`currentDate`) + 1, curNumCount) AS DECIMAL(24)) num
    FROM material_order
    WHERE order_code LIKE concat(`prefix`, `currentDate`, '%');

    SELECT ifnull(a.count_value + 1, 1)
    INTO `curNum`
    FROM t_num a
    WHERE a.count_value + 1 NOT IN (SELECT count_value FROM t_num_tmp)
    ORDER BY a.pk_id
    LIMIT 1;

    SELECT concat(`prefix`, substring(currentDate, 1), lpad(ifnull(curNum, 1), curNumCount, '0')) INTO result;
    RETURN result;
END
;;
SET FOREIGN_KEY_CHECKS = 1;
