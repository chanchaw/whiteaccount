/*
 追加配置表
 Date: 26/05/2021 14:08:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_config_varchar
-- ----------------------------
DROP TABLE IF EXISTS `sys_config_varchar`;
CREATE TABLE `sys_config_varchar`  (
  `config_field` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `config_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置值',
  `config_desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  PRIMARY KEY (`config_field`) USING BTREE,
  UNIQUE INDEX `sys_config_varchar_config_field_uindex`(`config_field`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字符配置' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
