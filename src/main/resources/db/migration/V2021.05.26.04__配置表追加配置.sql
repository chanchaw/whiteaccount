/**
  2021年5月26日 配置表追加配置
 */
REPLACE INTO sys_config_int (config_field, config_value, config_desc)
VALUES ('原料编号记数位数', 3, '原料编号记数位数'),
       ('原料编号日期位数', 6, '原料编号记数位数'),
       ('计划单号记数位数', 3, '原料编号记数位数'),
       ('计划单号日期位数', 6, '原料编号记数位数');