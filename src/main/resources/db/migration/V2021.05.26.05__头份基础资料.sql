/**
  2021年5月26日 头份基础资料
 */
create table first_copies
(
    id int auto_increment comment '自增主键',
    first_copies VARCHAR(100) not null,
    remarks VARCHAR(200) null comment '备注',
    constraint first_copies_pk
        primary key (id)
)
    comment '头份';