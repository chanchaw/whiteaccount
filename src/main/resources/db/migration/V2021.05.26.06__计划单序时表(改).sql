DROP PROCEDURE IF EXISTS `p_plan_report`;
DELIMITER ;;
CREATE PROCEDURE `p_plan_report`(IN `start` DATE, IN `end` DATE, IN `markFinished` TINYINT(1))
BEGIN
    /**
        2021年5月18日 计划单序时表
        2021年5月26日 追加是否完工字段
     */
    SET @baseSql = concat("
    SELECT a.id,
           a.contact_company_id,
           b.company_name,
           a.contract_code,
           a.plan_code,
           a.plan_date,
           a.order_code,
           a.product_name,
           a.material_specification,
           a.material_lot_no,
           a.material_supplier_id,
           d.supplier_name            material_supplier_name,
           a.greyfabric_weight,
           a.greyfabric_width,
           a.greyfabric_specification,
           a.greyfabric_price,
           a.m_height,
           a.plan_kilo,
           a.greyfabric_date,
           a.submit_date,
           a.material_price,
           a.processing_cost,
           a.middle_specification,
           a.middle_supplier_id,
           e.supplier_name            middle_supplier_name,
           a.bottom_specification,
           a.bottom_supplier_id,
           c.supplier_name            bottom_supplier_name,
           a.machine_type,
           a.needle_amount,
           a.low_weight,
           a.low_width,
           a.plan_employee_id,
           h.emp_name                 plan_employee_name,
           a.product_employee_id,
           i.emp_name                 product_employee_name,
           a.check_employee_id,
           g.emp_name                 check_employee_name,
           a.mark_company_name,
           a.fabric_requirement,
           a.remarks,
           a.create_user_id,
           ifnull(a.mark_finished, 0) mark_finished,
           j.user_name                create_user_name,
           a.create_time,
           a.state
    FROM plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN supplier c ON a.bottom_supplier_id = c.id
             LEFT JOIN supplier d ON a.material_supplier_id = d.id
             LEFT JOIN supplier e ON a.middle_supplier_id = e.id
             LEFT JOIN employee g ON a.check_employee_id = g.id
             LEFT JOIN employee h ON a.plan_employee_id = h.id
             LEFT JOIN employee i ON a.product_employee_id = i.id
             LEFT JOIN sys_user j ON a.create_user_id = j.id
    WHERE a.state = 1 ", if(isnull(`markFinished`), '', concat(" AND ifnull(a.mark_finished, 0)=", `markFinished`)),
                          if(isnull(`markFinished`),
                             concat("AND DATE(a.plan_date) BETWEEN DATE('", `start`, "') AND DATE('", `end`, "')"),
                             ''));
    PREPARE statement FROM @baseSql;
    EXECUTE statement;
END ;;
DELIMITER ;