DROP PROCEDURE IF EXISTS `getFHhistoryDetail`;
DELIMITER ;;
CREATE
    DEFINER = `root`@`%` PROCEDURE `getFHhistoryDetail`(m_SDate VARCHAR(100),
                                                        m_EDate VARCHAR(100),
                                                        m_Client VARCHAR(100),
                                                        m_CheckType VARCHAR(100) #结账方式
)
BEGIN


    SET @m_Date = CONCAT(" and DATE_FORMAT(a.datafh,'%Y-%m-%d') BETWEEN '", m_SDate, "' and '", m_EDate, "'");
    SET @m_Client = IF(m_Client = "全部", ' ', CONCAT(' and a.client=', "'", m_Client, "'"));
    SET @m_CheckType = IF(m_CheckType = "全部", '  ', CONCAT(' and a.checkout=', "'", m_CheckType, "'"));


    SET @SQL = "select a.id,a.client,a.address,
CONCAT_WS(' ',c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as pmgg,
b.ps,
round(b.qty,0)as qty,
round(a.price,1)as price,
ROUND(a.price*b.qty,1) as money,
a.checkout,
a.billcode,
DATE_FORMAT(a.datafh,'%m-%d %H:%i') as b_data

from whitebillfh a
left outer join whitebilldetailfh b
on a.id=b.fid
left outer join plan c
on b.planid=c.id
where 1=1 ";

    SET @SQL = CONCAT(@SQL, @m_Date, @m_Client, @m_CheckType, " ORDER BY a.id DESC");


    #SET @SQL = CONCAT(@SQL,);

    #select  @SQL;

    PREPARE stmt FROM @SQL;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

END;;
DELIMITER ;