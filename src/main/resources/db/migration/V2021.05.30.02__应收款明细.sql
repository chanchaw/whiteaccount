DROP PROCEDURE IF EXISTS `getReceivablesDetail`;
DELIMITER ;;
CREATE
    DEFINER = `root`@`%` PROCEDURE `getReceivablesDetail`(m_Sdate VARCHAR(100),
                                                          m_Edate VARCHAR(100),
                                                          m_Client VARCHAR(100))
BEGIN


    SELECT p.*
    FROM (SELECT m_Sdate                                 AS billdata,
                 '期初结余'                                  AS billtype,
                 NULL                                    AS qty,
                 NULL                                    AS price,
                 ifnull(m.money, 0) - ifnull(n.money, 0) AS money,
                 NULL                                    AS collectiontype,
                 NULL                                    AS billmemo,
                 0                                       AS SKid
          FROM (SELECT a.client, sum(a.money) AS money
                FROM (SELECT a.client, ROUND(sum(b.qty * ifnull(a.price, 0)), 2) AS money
                      FROM whitebillfh a
                               LEFT OUTER JOIN whitebilldetailfh b ON a.id = b.fid
                      WHERE DATE_FORMAT(a.datafh, '%Y-%m-%d') < m_Sdate
                        AND a.client = m_Client
                      GROUP BY a.client
                      UNION ALL
                      SELECT m_Client AS client, 0 AS money) a
                GROUP BY a.client) m

                   LEFT OUTER JOIN (SELECT client, sum(money) AS money
                                    FROM Receivables
                                    WHERE DATE_FORMAT(billdate, '%Y-%m-%d') < m_Sdate
                                      AND client = m_Client
                                    GROUP BY client) n ON m.client = n.client


          UNION ALL


          #发货明细
          SELECT DATE_FORMAT(a.datafh, '%Y-%m-%d')                                                   AS billdata,
                 CONCAT_WS(' ', c.order_code, c.product_name, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight),
                           c.m_height, c.greyfabric_specification, a.aorb, c.material_specification) AS billtype,
                 b.qty,
                 a.price,
                 ROUND(b.qty * ifnull(a.price, 0), 2)                                                AS money,
                 NULL                                                                                AS collectiontype,
                 a.memo                                                                              AS billmemo,
                 0                                                                                   AS SKid
          FROM whitebillfh a
                   LEFT OUTER JOIN whitebilldetailfh b ON a.id = b.fid
                   LEFT OUTER JOIN plan c ON b.planid = c.id
          WHERE DATE_FORMAT(a.datafh, '%Y-%m-%d') BETWEEN m_Sdate AND m_Edate
            AND a.client = m_Client

          UNION ALL

          #已经收款
          SELECT DATE_FORMAT(billdate, '%Y-%m-%d') AS billdate,
                 billtype,
                 NULL                              AS qty,
                 NULL                              AS price,
                 -money                            AS money,
                 collectiontype,
                 billmemo,
                 id                                AS SKid
          FROM Receivables
          WHERE DATE_FORMAT(billdate, '%Y-%m-%d') BETWEEN m_Sdate AND m_Edate
            AND client = m_Client) p
    ORDER BY p.billdata;

END;;
DELIMITER ;