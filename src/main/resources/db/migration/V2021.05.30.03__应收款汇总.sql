DROP PROCEDURE IF EXISTS `getReceivablesAllSum`;
DELIMITER ;;
CREATE
    DEFINER = `root`@`%` PROCEDURE `getReceivablesAllSum`()
BEGIN


    SELECT p.*, NULL AS str1
    FROM (SELECT m.client, ifnull(m.money, 0) - ifnull(n.money, 0) AS money
          FROM (SELECT a.client, ROUND(sum(b.qty * ifnull(a.price, 0)), 2) AS money
                FROM whitebillfh a
                         LEFT OUTER JOIN whitebilldetailfh b ON a.id = b.fid
                GROUP BY a.client) m

                   LEFT OUTER JOIN (SELECT client, sum(money) AS money FROM Receivables GROUP BY client) n
                                   ON m.client = n.client) p
    WHERE p.money > 0;
END;;
DELIMITER ;