DROP PROCEDURE IF EXISTS `getCollectionOld`;
DELIMITER ;;
CREATE
    DEFINER = `root`@`%` PROCEDURE `getCollectionOld`(m_SDate VARCHAR(100),
                                                      m_EDate VARCHAR(100),
                                                      m_Values VARCHAR(100) #0表示明细  1表示汇总
)
BEGIN

    IF m_Values = 0 THEN

        SELECT id, DATE_FORMAT(billdate, '%Y-%m-%d') AS billdate, client, billtype, money, collectiontype, billmemo
        FROM Receivables
        WHERE DATE_FORMAT(billdate, '%Y-%m-%d') BETWEEN m_SDate AND m_EDate;

    ELSEIF m_Values = 1 THEN

        SELECT 0          AS id,
               NULL       AS billdate,
               client,
               NULL       AS billtype,
               sum(money) AS money,
               NULL       AS collectiontype,
               NULL       AS billmemo
        FROM Receivables
        WHERE DATE_FORMAT(billdate, '%Y-%m-%d') BETWEEN m_SDate AND m_EDate
        GROUP BY client;

    END IF;

END;;
DELIMITER ;