SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `material_order` ADD COLUMN `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '单价' AFTER `input_amount`;
ALTER TABLE `material_order` ADD COLUMN `upload` int(11) NULL DEFAULT NULL COMMENT '上传下载同步数据标记（1已同步，0或者空未同步)' AFTER `state`;
ALTER TABLE `whitebillfh` ADD COLUMN `checkout` varchar(255) NULL DEFAULT NULL COMMENT '记账' AFTER `upload`;
ALTER TABLE `whitebillfh` ADD COLUMN `memo` varchar(255) NULL DEFAULT NULL COMMENT '发货备注' AFTER `checkout`;
ALTER TABLE `whitebillfh` ADD COLUMN `manager` varchar(255) NULL DEFAULT NULL COMMENT '经手人' AFTER `memo`;
ALTER TABLE `whitebillfh` ADD COLUMN `carnumber` varchar(100) NULL DEFAULT NULL COMMENT '车牌号' AFTER `manager`;
ALTER TABLE `whitebillfh` MODIFY COLUMN `aorb` varchar(100) NULL DEFAULT NULL COMMENT 'AB面' AFTER `datafh`;
SET FOREIGN_KEY_CHECKS=1;