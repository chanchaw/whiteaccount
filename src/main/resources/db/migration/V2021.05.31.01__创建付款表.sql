/**
  2021年5月31日 创建付款表
 */
SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE `payment`
(
    `id`             INT(11)        NOT NULL AUTO_INCREMENT COMMENT '应付款表 主键',
    `billtype`       VARCHAR(100)   NULL DEFAULT NULL COMMENT '单据类型（付款/付款调整）',
    `supplier`       VARCHAR(100)   NULL DEFAULT NULL COMMENT '供应商',
    `money`          DECIMAL(60, 2) NULL DEFAULT NULL COMMENT '金额',
    `billdate`       DATETIME    NULL DEFAULT NULL COMMENT '单据时间',
    `collectiontype` VARCHAR(100)   NULL DEFAULT NULL COMMENT '付款类型（转账/承兑/现金）',
    `billmemo`       VARCHAR(225)   NULL DEFAULT NULL COMMENT '备注',
    `upload`         INT(11)        NULL DEFAULT NULL COMMENT '是否上传云端',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT '付款';

SET FOREIGN_KEY_CHECKS = 1;