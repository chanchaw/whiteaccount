/**
  追加索引 2021年5月31日
 */
create index payment_billtype_index
    on payment (billtype);
create index payment_collectiontype_index
    on payment (collectiontype);
create index payment_supplier_index
    on payment (supplier);
create index receivables_billtype_index
    on receivables (billtype);
create index receivables_client_index
    on receivables (client);
create index receivables_collectiontype_index
    on receivables (collectiontype);