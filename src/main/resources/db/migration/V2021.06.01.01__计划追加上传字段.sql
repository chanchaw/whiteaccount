/**
    2021年6月1日 计划追加上传字段
 */
alter table plan
    add upload int default 0 null comment '上传字段(0:未上传;1:已上传;)';