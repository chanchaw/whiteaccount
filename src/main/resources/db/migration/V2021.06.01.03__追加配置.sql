/**
  2021年6月1日 追加配置
 */
REPLACE INTO sys_config_int (config_field, config_value, config_desc)
VALUES ('计划单号日期截取起始位置', 3, '最小值1'),
       ('计划单号日期截取长度', 2 , '最大值8');