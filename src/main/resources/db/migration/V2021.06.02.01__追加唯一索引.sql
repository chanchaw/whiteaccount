/**
  追加唯一索引
 */
create unique index weight_of_fabric_wof_name_uindex
    on weight_of_fabric (wof_name);
create unique index width_of_fabric_wof_name_uindex
    on width_of_fabric (wof_name);