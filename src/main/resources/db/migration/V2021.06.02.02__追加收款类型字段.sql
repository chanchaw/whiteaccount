/**
  2021年6月2日 追加收款类型字段
 */
alter table material_order
    add account_type VARCHAR(50) null comment '收款类型' after bill_type;