/*
 Navicat Premium Data Transfer


 Date: 02/06/2021 19:32:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for jrkbillsum
-- ----------------------------
DROP TABLE IF EXISTS `jrkbillsum`;
CREATE TABLE `jrkbillsum`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `planid` int(11) NULL DEFAULT NULL COMMENT '计划主键',
  `ps` int(11) NULL DEFAULT NULL COMMENT '匹数',
  `qty` decimal(10, 1) NULL DEFAULT NULL COMMENT '公斤',
  `aorb` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AB面',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '班组',
  `jth` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机台号',
  `cn` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '计算机名',
  `createdate` datetime(0) NULL DEFAULT NULL COMMENT '上传日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
