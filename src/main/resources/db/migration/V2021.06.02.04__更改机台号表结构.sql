/**
  2021年6月2日 更改机台号表结构
 */
SET FOREIGN_KEY_CHECKS = 0;
ALTER TABLE machine_type
    ADD serial_num INT DEFAULT 0 COMMENT '排序字段';
SET FOREIGN_KEY_CHECKS = 1;