/**
  2021年6月3日 计划表汇总毛高更改为字符型
 */
alter table plan modify m_height VARCHAR(50) null comment '毛高';
alter table plan_template modify m_height varchar(50) null comment '毛高';