/**
  创建调整表
 */
create table jrkbill_adjust
(
    id int auto_increment comment '自增主键',
    plan_id BIGINT null comment '外键,对应plan.id',
    pairs INT default 0 null comment '匹数',
    qty DECIMAL(20,1) null comment '公斤',
    bill_type VARCHAR(50) null comment '调整类型',
    create_time DATETIME default CURRENT_TIMESTAMP null comment '创建时间',
    create_user_id int null comment '创建人编号',
    remarks VARCHAR(255) null comment '备注',
    modify_user_id int null comment '修改人编号',
    modify_time DATETIME null comment '修改时间',
    cancel_user_id int null comment '作废人编号',
    cancel_time DATETIME null comment '作废时间',
    state int default 1 null comment '状态(0:作废;1:正常;)',
    constraint jrkbill_adjust_pk
        primary key (id)
)
    comment '打卷调整表';

create index jrkbill_adjust_cancel_user_id_index
    on jrkbill_adjust (cancel_user_id);

create index jrkbill_adjust_create_user_id_index
    on jrkbill_adjust (create_user_id);

create index jrkbill_adjust_modify_user_id_index
    on jrkbill_adjust (modify_user_id);

create index jrkbill_adjust_plan_id_index
    on jrkbill_adjust (plan_id);