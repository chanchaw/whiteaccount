DROP PROCEDURE IF EXISTS `p_plan_storage_modify`;
DELIMITER ;;
CREATE PROCEDURE `p_plan_storage_modify`(IN `id` BIGINT)
BEGIN
    /**
      2021年6月4日  库存调整存储过程
     */
SELECT c.id,
       a.id                                                           plan_id,
       date_format(ifnull(c.create_time, a.plan_date), '%m-%d %H:%i') plan_date,
       ifnull(c.bill_type, '库存')                                      bill_type,
       ifnull(c.pairs, 0)                                             pairs,
       ifnull(c.qty, 0)                                               kilo,
       ifnull(b.ps, 0) - ifnull(c.pairs, 0)                           remain_pairs,
       ifnull(b.qty, 0) - ifnull(c.qty, 0)                            remain_kilo,
       c.remarks                                                      remarks
FROM plan a
         LEFT JOIN (SELECT planid, sum(ps) ps, sum(qty) qty FROM jrkbillsum WHERE planid = `id` GROUP BY planid) b
                   ON a.id = b.planid
         LEFT JOIN jrkbill_adjust c ON a.id = c.plan_id AND c.state = 1
WHERE a.id = `id`;
END ;;
DELIMITER ;