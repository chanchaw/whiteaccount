/**
  2021年6月4日 打印页面标题配置
 */
REPLACE sys_config_varchar (config_field, config_value, config_desc)
VALUES ('结算通知单标题', '梦田针纺织加工生产发货结算通知单', '打印页面标题配置');