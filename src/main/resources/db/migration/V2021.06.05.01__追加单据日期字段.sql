/**
  2021年6月5日 追加单据日期字段
 */
alter table jrkbill_adjust
    add jrkbill_date DATE null comment '日期' after plan_id;