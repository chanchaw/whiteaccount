SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_jrkbill_dtl
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_jrkbill_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_jrkbill_dtl`(IN `id` BIGINT)
BEGIN
    /**
       2021年5月22日 打卷入库明细
     */
    SELECT b.id                                     id,
           ifnull(b.ps, 0)                          input_pairs,
           ifnull(round(b.qty, 1), 0)               input_kilo,
           b.aorb                                   aorb,
           a.greyfabric_specification               greyfabric_specification,
           a.material_specification                 material_specification,
           b.cn                                     computer_name,
           b.jth                                    machine_type,
           date_format(b.createdate, '%m-%d %H:%i') createdate
    FROM plan a
             LEFT JOIN jrkbillsum b ON a.id = b.planid
    WHERE a.id = `id`
      AND b.id IS NOT NULL
    ORDER BY b.createdate DESC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_jrkbillfh_dtl
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_jrkbillfh_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_jrkbillfh_dtl`(IN `id` BIGINT)
BEGIN
    /**
       2021年5月22日 发货明细
     */
    SELECT a.client                             client_name,
           a.billcode                           billcode,
           a.address                            address,
           ifnull(b.ps, 0)                      send_pairs,
           sum(round(b.qty, 1))                 send_kilo,
           d.greyfabric_specification           greyfabric_specification,
           d.material_specification             material_specification,
           date_format(a.datafh, '%m-%d %H:%i') send_date
    FROM whitebillfh a
             LEFT JOIN whitebilldetailfh b ON a.id = b.fid
             LEFT JOIN plan d ON b.planid = d.id
    WHERE b.planid = `id`
    ORDER BY a.datafh DESC;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;