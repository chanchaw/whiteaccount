DROP PROCEDURE IF EXISTS `p_plan_storage_modify`;
DELIMITER ;;
CREATE PROCEDURE `p_plan_storage_modify`(IN `id` BIGINT)
BEGIN
    /**
      2021年6月4日  库存调整存储过程
     */
    SELECT *
    FROM (SELECT report.*,
                 @storage_ps := ifnull(report.pairs, 0) + @storage_ps  remain_pairs,
                 @storage_qty := ifnull(report.kilo, 0) + @storage_qty remain_kilo
          FROM (SELECT NULL                                     id,
                       a.id                                     plan_id,
                       date_format(b.createdate, '%m-%d %H:%i') jrkbill_date,
                       '库存'                                     bill_type,
                       ifnull(b.ps, 0)                          pairs,
                       ifnull(b.qty, 0)                         kilo,
                       ''                                       remarks
                FROM plan a
                         LEFT JOIN jrkbillsum b ON a.id = b.planid
                WHERE a.id = `id`
                  AND b.id IS NOT NULL

                UNION ALL

                SELECT b.id                                       id,
                       a.id                                       plan_id,
                       date_format(b.jrkbill_date, '%m-%d %H:%i') jrkbill_date,
                       ifnull(b.bill_type, '库存')                  bill_type,
                       ifnull(b.pairs, 0)                         pairs,
                       ifnull(b.qty, 0)                           kilo,
                       b.remarks                                  remarks
                FROM plan a
                         LEFT JOIN jrkbill_adjust b ON a.id = b.plan_id AND b.state = 1
                WHERE a.id = `id`
                  AND b.id IS NOT NULL) report,
               (SELECT @storage_ps := 0) p,
               (SELECT @storage_qty := 0) q
          ORDER BY report.jrkbill_date) r
    ORDER BY r.jrkbill_date DESC;
END ;;
DELIMITER ;