/**
  2021年6月6日 是否上传字段
 */
alter table jrkbill_adjust
    add upload int default 0 null comment '是否上传字段';