SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_plan_dtl
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_plan_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_plan_dtl`(IN `id` BIGINT)
BEGIN
    /**
      2021年5月22日 计划单明细查看
     */
    SELECT b.company_name                                                                      company_name,
           concat(ifnull(a.plan_code, ''), ' ', ifnull(a.product_name, ''), ' ', a.greyfabric_weight, '*',
                  a.greyfabric_width, ' ', ifnull(a.m_height, 0), ' ', ifnull(a.greyfabric_specification, ''), ' ',
                  ifnull(a.material_specification, ''))                                        product_name,
           ifnull(a.plan_kilo, 0)                                                              plan_kilo,
           ifnull(c.pairs, 0) + ifnull(f.pairs, 0)                                             input_pairs,
           ifnull(c.kilo, 0) + ifnull(f.kilo, 0)                                               input_kilo,
           ifnull(d.kilo, 0) + ifnull(e.kilo, 0)                                               send_kilo,
           ifnull(d.pairs, 0) + ifnull(e.pairs, 0)                                             send_pairs,
           ifnull(round(ifnull(a.plan_kilo, 0) - ifnull(d.kilo, 0) - ifnull(e.kilo, 0), 1), 0) plan_remain_kilo,
           ifnull(round(ifnull(c.kilo, 0) + ifnull(f.kilo, 0) - ifnull(d.kilo, 0) - ifnull(e.kilo, 0) +
                        ifnull(g.kilo, 0), 1), 0)                                              remain_kilo,
           ifnull(round(ifnull(c.pairs, 0) + ifnull(f.pairs, 0) - ifnull(d.pairs, 0) - ifnull(e.pairs, 0) +
                        ifnull(g.pairs, 0), 0), 0)                                             remain_pairs,
           ifnull(a.greyfabric_price, 0)                                                       greyfabric_price
    FROM plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN (
        # 总入库公斤
        SELECT a.id, ifnull(b.pairs, 0) pairs, ifnull(b.kilo, 0) kilo
        FROM plan a
                 LEFT JOIN (SELECT planid, sum(ps) pairs, sum(qty) kilo FROM jrkbillsum GROUP BY planid) b
                           ON a.id = b.planid) c ON c.id = a.id
             LEFT JOIN (
        # 已发公斤
        SELECT b.planid id, ifnull(sum(round(b.qty, 1)), 0) kilo, ifnull(sum(b.ps), 0) pairs
        FROM whitebillfh a
                 LEFT JOIN whitebilldetailfh b ON a.id = b.fid
        GROUP BY b.planid) d ON d.id = a.id
             LEFT JOIN (
        # 手动出库
        SELECT ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo, plan_id
        FROM jrkbill_adjust
        WHERE state = 1
          AND bill_type = '手动出库'
        GROUP BY plan_id) e ON e.plan_id = a.id
             LEFT JOIN (
        #             手动入库
        SELECT ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo, plan_id
        FROM jrkbill_adjust
        WHERE state = 1
          AND bill_type IN ('手动入库')
        GROUP BY plan_id) f ON f.plan_id = a.id
             LEFT JOIN (
        #            盘点
        SELECT ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo, plan_id
        FROM jrkbill_adjust
        WHERE state = 1
          AND bill_type IN ('盘点')
        GROUP BY plan_id) g ON g.plan_id = a.id
    WHERE a.id = `id`;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_plan_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_plan_report`;
DELIMITER ;;
CREATE PROCEDURE `p_plan_report`(IN `start` DATE, IN `end` DATE, IN `markFinished` TINYINT(1))
BEGIN
    /**
        2021年5月18日 计划单序时表
        2021年5月26日 追加是否完工字段
     */
    SET @baseSql = concat("
SELECT a.id,
       a.contact_company_id,
       b.company_name,
       a.contract_code,
       a.plan_code,
       a.plan_date,
       a.order_code,
       a.product_name,
       a.material_specification,
       a.material_lot_no,
       a.material_supplier_id,
       d.supplier_name                         material_supplier_name,
       a.greyfabric_weight,
       a.greyfabric_width,
       a.greyfabric_specification,
       a.greyfabric_price,
       a.m_height,
       a.plan_kilo,
       a.greyfabric_date,
       a.submit_date,
       a.material_price,
       a.processing_cost,
       a.middle_specification,
       a.middle_supplier_id,
       e.supplier_name                         middle_supplier_name,
       a.bottom_specification,
       a.bottom_supplier_id,
       c.supplier_name                         bottom_supplier_name,
       a.machine_type,
       a.needle_amount,
       a.low_weight,
       a.low_width,
       a.plan_employee_id,
       h.emp_name                              plan_employee_name,
       a.product_employee_id,
       i.emp_name                              product_employee_name,
       a.check_employee_id,
       g.emp_name                              check_employee_name,
       a.mark_company_name,
       a.fabric_requirement,
       a.remarks,
       a.create_user_id,
       ifnull(a.mark_finished, 0)              mark_finished,
       j.user_name                             create_user_name,
       a.create_time,
       a.state,
       ifnull(k.pairs, 0)                      input_pairs,
       ifnull(k.kilo, 0)                       input_kilo,
       ifnull(l.kilo, 0)+ifnull(n.kilo,0)      delivery_kilo,
       ifnull(l.pairs, 0)+ifnull(n.pairs,0)    delivery_pairs,
       ifnull(k.pairs, 0)-ifnull(l.pairs, 0)-ifnull(n.pairs,0)+ifnull(m.pairs,0) remain_pairs,
       ifnull(k.kilo, 0)-ifnull(l.kilo, 0)-ifnull(n.kilo,0)+ifnull(m.kilo,0)   remain_kilo,
       a.notice_date                           notice_date
FROM plan a
         LEFT JOIN contact_company b ON a.contact_company_id = b.id
         LEFT JOIN supplier c ON a.bottom_supplier_id = c.id
         LEFT JOIN supplier d ON a.material_supplier_id = d.id
         LEFT JOIN supplier e ON a.middle_supplier_id = e.id
         LEFT JOIN employee g ON a.check_employee_id = g.id
         LEFT JOIN employee h ON a.plan_employee_id = h.id
         LEFT JOIN employee i ON a.product_employee_id = i.id
         LEFT JOIN sys_user j ON a.create_user_id = j.id
         LEFT JOIN (
    /* 打卷数据 */
     SELECT
                a.id,
                ifnull(b.pairs,0)+ifnull(c.pairs,0) pairs,
                 ifnull(b.kilo,0)+ifnull(c.kilo,0) kilo
                FROM plan a
                 LEFT JOIN (SELECT planid,sum(ps) pairs,sum(qty) kilo FROM jrkbillsum GROUP BY planid) b ON a.id = b.planid
                 LEFT JOIN (SELECT plan_id,sum(if(bill_type='手动出库',-ifnull(pairs,0),ifnull(pairs,0))) pairs,sum(if(bill_type='手动出库',-ifnull(qty,0),ifnull(qty,0))) kilo FROM jrkbill_adjust WHERE state = 1 and bill_type='手动入库' GROUP BY plan_id) c ON a.id = c.plan_id
     ) k ON k.id = a.id
         LEFT JOIN (
    /* 发货数据 */
    SELECT ifnull(sum(b.ps), 0) pairs, ifnull(sum(b.qty), 0) kilo, b.planid
    FROM whitebillfh a
             LEFT JOIN whitebilldetailfh b ON a.id = b.itemid
    GROUP BY b.planid) l ON l.planid = a.id
    left join (
        SELECT plan_id, ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo
            FROM jrkbill_adjust
            WHERE bill_type = '盘点'
            GROUP BY plan_id
    ) m on m.plan_id = a.id
     left join (
        SELECT plan_id, ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo
            FROM jrkbill_adjust
            WHERE bill_type = '手动出库'
            GROUP BY plan_id
    ) n on n.plan_id = a.id
    WHERE a.state = 1 ", if(isnull(`markFinished`), '', concat(" AND ifnull(a.mark_finished, 0)=", `markFinished`)),
                          if(isnull(`markFinished`),
                             concat("AND DATE(a.plan_date) BETWEEN DATE('", `start`, "') AND DATE('", `end`, "')"), ''),
                          " order by ifnull(a.mark_finished, 0),a.plan_date desc");
    PREPARE statement FROM @baseSql;
    EXECUTE statement;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;