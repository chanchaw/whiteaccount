/**
  2021年6月6日 追加字段
 */
alter table jrkbill_adjust
    add client_name VARCHAR(100) null comment '客户名称';

alter table jrkbill_adjust
    add address VARCHAR(100) null comment '收货单位';