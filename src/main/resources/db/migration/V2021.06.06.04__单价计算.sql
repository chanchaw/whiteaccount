/**
  2021年6月6日 单价计算
 */
alter table jrkbill_adjust
    add price DECIMAL(60,2) default 0 null comment '单价';