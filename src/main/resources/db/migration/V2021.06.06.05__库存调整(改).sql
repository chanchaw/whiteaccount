DROP PROCEDURE IF EXISTS `p_plan_storage_modify`;
DELIMITER ;;
CREATE PROCEDURE `p_plan_storage_modify`(IN `id` BIGINT)
BEGIN
    /**
      2021年6月4日  库存调整存储过程
      2021年6月6日  追加字段,客户，收货单位，供应商
     */
    SELECT *
    FROM (SELECT report.*,
                 @storage_ps := ifnull(report.pairs, 0) + @storage_ps  remain_pairs,
                 @storage_qty := ifnull(report.kilo, 0) + @storage_qty remain_kilo
          FROM (
                   --           库存
                   (SELECT NULL                                     id,
                           a.id                                     plan_id,
                           date_format(b.createdate, '%m-%d %H:%i') jrkbill_date,
                           '入库'                                     bill_type,
                           ifnull(sum(b.ps), 0)                     pairs,
                           ifnull(sum(b.qty), 0)                    kilo,
                           ''                                       remarks,
                           c.company_name                           client_name,
                           NULL                                     address,
                           NULL                                     price,
                           b.createdate                             order_date
                    FROM plan a
                             LEFT JOIN jrkbillsum b ON a.id = b.planid
                             LEFT JOIN contact_company c ON a.contact_company_id = c.id
                    WHERE a.id = `id`
                      AND b.id IS NOT NULL
                    GROUP BY b.createdate)

                   UNION ALL
                   --          手动调整
                   (SELECT b.id                                                              id,
                           a.id                                                              plan_id,
                           date_format(b.jrkbill_date, '%m-%d %H:%i')                        jrkbill_date,
                           ifnull(b.bill_type, '')                                           bill_type,
                           if(b.bill_type = '手动出库', -ifnull(b.pairs, 0), ifnull(b.pairs, 0)) pairs,
                           if(b.bill_type = '手动出库', -ifnull(b.qty, 0), ifnull(b.qty, 0))     kilo,
                           b.remarks                                                         remarks,
                           b.client_name                                                     client_name,
                           b.address                                                         address,
                           b.price                                                           price,
                           b.jrkbill_date                                                    order_date
                    FROM plan a
                             LEFT JOIN jrkbill_adjust b ON a.id = b.plan_id AND b.state = 1
                    WHERE a.id = `id`
                      AND b.id IS NOT NULL)

                   UNION ALL
                   --          发货
                   (SELECT NULL                                 id,
                           a.id                                 plan_id,
                           date_format(c.datafh, '%m-%d %H:%i') jrkbill_date,
                           '发货'                                 bill_type,
                           -ifnull(b.ps, 0)                     pairs,
                           -ifnull(b.qty, 0)                    kilo,
                           c.memo                               remarks,
                           c.client                             client_name,
                           c.address                            address,
                           c.price                              price,
                           c.datafh                             order_date
                    FROM plan a
                             LEFT JOIN whitebilldetailfh b ON a.id = b.planid
                             LEFT JOIN whitebillfh c ON b.fid = c.id
                    WHERE a.id = `id`
                      AND b.itemid IS NOT NULL)) report,
               (SELECT @storage_ps := 0) p,
               (SELECT @storage_qty := 0) q
          ORDER BY report.order_date) r
    ORDER BY r.order_date DESC;
END ;;
DELIMITER ;