SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_jrkbill_dtl
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_jrkbill_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_jrkbill_dtl`(IN `id` BIGINT)
BEGIN
    /**
       2021年5月22日 打卷入库明细
       2021年6月6日  拼接单据类型到机台号
     */
    SELECT *
    FROM ((SELECT b.id                                     id,
                  ifnull(sum(b.ps), 0)                     input_pairs,
                  ifnull(sum(round(b.qty, 1)), 0)          input_kilo,
                  b.aorb                                   aorb,
                  a.greyfabric_specification               greyfabric_specification,
                  a.material_specification                 material_specification,
                  b.cn                                     computer_name,
                  b.jth                                    machine_type,
                  date_format(b.createdate, '%m-%d %H:%i') createdate,
                  NULL                                     remarks,
                  '入库'                                     bill_type,
                  b.createdate                             order_date
           FROM plan a
                    LEFT JOIN jrkbillsum b ON a.id = b.planid
           WHERE a.id = `id`
             AND b.id IS NOT NULL
           GROUP BY b.createdate)
          UNION ALL
          (SELECT b.id                                       id,
                  ifnull(b.pairs, 0)                         input_pairs,
                  ifnull(round(b.qty, 1), 0)                 input_kilo,
                  NULL                                       aorb,
                  a.greyfabric_specification                 greyfabric_specification,
                  a.material_specification                   material_specification,
                  b.bill_type                                computer_name,
                  NULL                                       machine_type,
                  date_format(b.jrkbill_date, '%m-%d %H:%i') createdate,
                  b.remarks                                  remarks,
                  b.bill_type                                bill_type,
                  b.jrkbill_date                             order_date
           FROM plan a
                    LEFT JOIN jrkbill_adjust b ON a.id = b.plan_id
           WHERE a.id = `id`
             AND b.bill_type = '手动入库'
             AND b.id IS NOT NULL)) r
    ORDER BY r.order_date DESC;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_jrkbillfh_dtl
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_jrkbillfh_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_jrkbillfh_dtl`(IN `id` BIGINT)
BEGIN
    /**
       2021年5月22日 发货明细
       2021年6月6日  拼接字段,追加类型到单据列
     */
    SELECT r.*
    FROM ((SELECT a.client                             client_name,
                  a.billcode                           billcode,
                  a.address                            address,
                  ifnull(sum(b.ps), 0)                 send_pairs,
                  sum(round(b.qty, 1))                 send_kilo,
                  d.greyfabric_specification           greyfabric_specification,
                  d.material_specification             material_specification,
                  date_format(a.datafh, '%m-%d %H:%i') send_date,
                  a.memo                               remarks,
                  a.datafh                             order_date
           FROM whitebillfh a
                    LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                    LEFT JOIN plan d ON b.planid = d.id
           WHERE b.planid = `id`
             AND b.itemid IS NOT NULL
             AND a.id IS NOT NULL
           GROUP BY a.id)
          UNION ALL
          (SELECT c.company_name                             client_name,
                  b.bill_type                                billcode,
                  b.address                                  address,
                  ifnull(b.pairs, 0)                         send_pairs,
                  round(b.qty, 1)                            send_kilo,
                  a.greyfabric_specification                 greyfabric_specification,
                  a.material_specification                   material_specification,
                  date_format(b.jrkbill_date, '%m-%d %H:%i') send_date,
                  b.remarks                                  remarks,
                  b.jrkbill_date                             order_date
           FROM plan a
                    LEFT JOIN jrkbill_adjust b ON a.id = b.plan_id
                    LEFT JOIN contact_company c ON a.contact_company_id = c.id
           WHERE b.plan_id = `id`
             AND b.bill_type = '手动出库'
             AND b.id IS NOT NULL)) r
    ORDER BY r.order_date DESC;

END;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;