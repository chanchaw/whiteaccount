/**
  2021年6月6日 追加配置
 */
REPLACE INTO sys_config_int (config_field, config_value, config_desc)
VALUES ('计划单完工按钮校验是否开启', 1, '1:开启;0:关闭;关闭后完工计划不会校验是否有库存');
