/**
  2021年6月6日 更改主键
 */
alter table machine_type modify id INT auto_increment comment '主键';
alter table machine_type drop primary key;