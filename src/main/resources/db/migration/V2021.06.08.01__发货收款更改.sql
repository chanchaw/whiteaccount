/*

*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for getFHDetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `getFHDetail`;
delimiter ;;
CREATE PROCEDURE `getFHDetail`(m_fhid varchar(100))
BEGIN

select a.id,a.billcode,a.client,a.address,a.aorb,a.price,a.checkout,a.memo,a.manager,a.carnumber,
b.ps,b.qty,b.planid,
c.order_code,c.plan_code,c.material_specification,
CONCAT_WS(' ',c.order_code,product_name,CONCAT_WS('*',greyfabric_width,greyfabric_weight),IF(m_height>0,m_height,''),greyfabric_specification) as PMGG
from whitebillfh a
left outer join whitebilldetailfh b
on a.id=b.fid
left outer join plan c
on b.planid=c.id
where a.id=m_fhid
;

end
;;
delimiter ;

-- ----------------------------
-- Procedure structure for GetFHDetailNow
-- ----------------------------
DROP PROCEDURE IF EXISTS `GetFHDetailNow`;
delimiter ;;
CREATE PROCEDURE `GetFHDetailNow`()
BEGIN

 
select a.id,a.client,a.address,
CONCAT_WS(' ',c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as pmgg,
b.ps,
round(b.qty,0)as qty,
round(a.price,1)as price,
ROUND(a.price*b.qty,1) as money,
a.checkout,
DATE_FORMAT(a.datafh,'%m-%d %H:%i') as b_data

from whitebillfh a
left outer join whitebilldetailfh b
on a.id=b.fid
left outer join plan c
on b.planid=c.id

 where DATE_FORMAT(a.datafh,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d')
ORDER BY a.id DESC
;
 
 
end
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getFHhistoryDetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `getFHhistoryDetail`;
delimiter ;;
CREATE PROCEDURE `getFHhistoryDetail`(m_SDate varchar(100),
m_EDate varchar(100),
m_Client varchar(100),
m_CheckType varchar(100))
BEGIN


set @m_Date=CONCAT(" and DATE_FORMAT(a.datafh,'%Y-%m-%d') BETWEEN '",m_SDate,"' and '",m_EDate,"'");
set @m_Client=IF(m_Client="全部",' ',CONCAT(' and a.client=',"'",m_Client,"'"));
set @m_CheckType=IF(m_CheckType="全部",'  ',CONCAT(' and a.checkout=',"'",m_CheckType,"'"));




SET @SQL="select a.id,a.client,a.address,
CONCAT_WS(' ',c.order_code,c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as pmgg,
b.ps,
round(b.qty,0)as qty,
round(a.price,1)as price,
ROUND(a.price*b.qty,1) as money,
a.checkout,
a.billcode,
DATE_FORMAT(a.datafh,'%m-%d %H:%i') as b_data

from whitebillfh a
left outer join whitebilldetailfh b
on a.id=b.fid
left outer join plan c
on b.planid=c.id 
where 1=1 ";


SET @SQL = CONCAT(@SQL,@m_Date,@m_Client,@m_CheckType, " ORDER BY a.id DESC");
		

#SET @SQL = CONCAT(@SQL,);

#select  @SQL;

prepare stmt from @SQL;
execute stmt ; 
deallocate prepare stmt; 

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getReceivablesAllSum
-- ----------------------------
DROP PROCEDURE IF EXISTS `getReceivablesAllSum`;
delimiter ;;
CREATE PROCEDURE `getReceivablesAllSum`()
BEGIN


select p.*,null as str1
from
(
	select m.client,ifnull(m.money,0)-ifnull(n.money,0) as money
	from
	(
	select a.client,ROUND(sum(b.qty*ifnull(a.price,0)),2)as money
	from whitebillfh a
	left outer join whitebilldetailfh b
	on a.id=b.fid
	GROUP BY a.client
	)m

	left outer join
	(
	select client,sum(money)as money
	from receivables
	GROUP BY client
	)n on m.client=n.client

)p where p.money>0;

end
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getreceivablesDetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `getReceivablesDetail`;
delimiter ;;
CREATE PROCEDURE `getReceivablesDetail`(m_Sdate varchar(100),
m_Edate varchar(100),
m_Clent varchar(100),
m_Value VARCHAR(100))
BEGIN

    IF m_Value=0 THEN

        if m_Clent='全部' THEN
                    select p.*
                    from
                    (
                        select m_Sdate as billdate,'全部' as client,'期初结余' as billtype,null as qty,0 ps,null as price,
                        round(sum(ifnull(m.money,0))-sum(ifnull(n.money,0)),2) as money,
                        null as collectiontype,
                        null as billmemo,
                        0 as SKid
                        from
                        (
                        select a.client,sum(a.money)as money
                        from
                        (
                            select a.client,ROUND(sum(b.qty*ifnull(a.price,0)),2)as money
                            from whitebillfh a
                            left outer join whitebilldetailfh b
                            on a.id=b.fid
                            where DATE(a.datafh) < DATE(m_Sdate)
                            GROUP BY a.client
                            union all
                            select m_Clent as client,0 as money
                         )a GROUP BY a.client
                    )m

                        left outer join
                        (
                        select client,sum(money) as money
                        from receivables
                        where DATE(billdate) < DATE(m_Sdate)
                        GROUP BY client
                        )n on m.client=n.client

                        union all


                    #发货明细
                        select DATE_FORMAT(a.datafh,'%Y-%m-%d') as billdate,a.client,
                        CONCAT_WS(' ',c.order_code,c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as billtype,
                        b.qty,b.ps,a.price,
                        ROUND(b.qty*ifnull(a.price,0),2)as money,
                        a.checkout as collectiontype,
                        a.memo as billmemo,
                        0 as SKid
                        from whitebillfh a
                        left outer join whitebilldetailfh b
                        on a.id=b.fid
                        left outer join plan c
                        on b.planid=c.id
                        where DATE_FORMAT(a.datafh,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                        #and a.client=m_Clent

                        union ALL

                    #已经收款
                        select DATE_FORMAT(billdate,'%Y-%m-%d') as billdate,client,
                        billtype,null as qty,0 ps,null as price,-money as money,collectiontype,billmemo,
                        id as SKid
                        from receivables
                        where DATE_FORMAT(billdate,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                        #and client=m_Clent
                    )p
                    ORDER BY p.billdate;
        else
                select p.*
                from
                (
                    select m_Sdate as billdate,m.client as client,'期初结余' as billtype,null as qty,0 ps,null as price,ifnull(m.money,0)-ifnull(n.money,0) as money,
                    null as collectiontype,
                    null as billmemo,
                    NULL as SKid
                    from
                    (
                    select a.client,sum(a.money)as money
                    from
                    (
                        select a.client,ROUND(sum(b.qty*ifnull(a.price,0)),2)as money
                        from whitebillfh a
                        left outer join whitebilldetailfh b
                        on a.id=b.fid
                        where DATE_FORMAT(a.datafh,'%Y-%m-%d') < m_Sdate
                        and a.client=m_Clent
                        GROUP BY a.client
                        union all
                        select m_Clent as client,0 as money
                     )a GROUP BY a.client
                )m

                    left outer join
                    (
                    select client,sum(money)as money
                    from receivables
                    where DATE_FORMAT(billdate,'%Y-%m-%d') < m_Sdate
                    and client=m_Clent
                    GROUP BY client
                    )n on m.client=n.client


                    union all


                #发货明细
                    select DATE_FORMAT(a.datafh,'%Y-%m-%d') as billdate,a.client,
                    CONCAT_WS(' ',c.order_code,c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as billtype,
                    b.qty,b.ps,a.price,
                    ROUND(b.qty*ifnull(a.price,0),2)as money,
                    a.checkout as collectiontype,
                    a.memo as billmemo,
                    NULL as SKid
                    from whitebillfh a
                    left outer join whitebilldetailfh b
                    on a.id=b.fid
                    left outer join plan c
                    on b.planid=c.id
                    where DATE_FORMAT(a.datafh,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                    and a.client=m_Clent

                    union ALL

                #已经收款
                    select DATE_FORMAT(billdate,'%Y-%m-%d') as billdate,client,
                    billtype,null as qty,0 ps,0 price,- ifnull(money,0) as money,collectiontype,billmemo,
                    id as SKid
                    from receivables
                    where DATE_FORMAT(billdate,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                    and client=m_Clent
                )p
                ORDER BY p.billdate;
        end if;

    ELSEIF m_Value=1 THEN

        if m_Clent='全部' THEN
            SELECT r.client,round(ifnull(sum(r.money),0),2) money FROM (
            select p.*
            from
                (
                    /*select m_Sdate as billdate,'全部' as client,'期初结余' as billtype,null as qty,null as price,
                           round(sum(ifnull(m.money,0))-sum(ifnull(n.money,0)),2) as money,
                           null as collectiontype,
                           null as billmemo,
                           0 as SKid
                    from
                        (
                            select a.client,sum(a.money)as money
                            from
                                (
                                    select a.client,ROUND(sum(b.qty*ifnull(a.price,0)),2)as money
                                    from whitebillfh a
                                             left outer join whitebilldetailfh b
                                                             on a.id=b.fid
                                    where DATE(a.datafh) < DATE(m_Sdate)
                                          #and a.client=m_Clent
                                    GROUP BY a.client
                                    union all
                                    select m_Clent as client,0 as money
                                )a GROUP BY a.client
                        )m

                            left outer join
                        (
                            select client,sum(money) as money
                            from receivables
                            where DATE(billdate) < DATE(m_Sdate)
                            GROUP BY client
                        )n on m.client=n.client

                    union all*/


                    #发货明细
                    select DATE_FORMAT(a.datafh,'%Y-%m-%d') as billdate,a.client,
                           CONCAT_WS(' ',c.order_code,c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as billtype,
                           b.qty,a.price,
                           ROUND(b.qty*ifnull(a.price,0),2)as money,
                           a.checkout as collectiontype,
                           a.memo as billmemo,
                           0 as SKid
                    from whitebillfh a
                             left outer join whitebilldetailfh b
                                             on a.id=b.fid
                             left outer join plan c
                                             on b.planid=c.id
                    # where DATE_FORMAT(a.datafh,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                          #and a.client=m_Clent

                    union ALL

                    #已经收款
                    select DATE_FORMAT(billdate,'%Y-%m-%d') as billdate,client,
                           billtype,null as qty,null as price,-money as money,collectiontype,billmemo,
                           id as SKid
                    from receivables
                   # where DATE_FORMAT(billdate,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                    #and client=m_Clent
                )p
            ORDER BY p.billdate
                ) r
            GROUP BY r.client;
        else
            SELECT
            r.client, round(ifnull(sum(r.money),0),2) money
            FROM (
            select p.*
            from
                (
                    select m_Sdate as billdate,m.client as client,'期初结余' as billtype,null as qty,null as price,ifnull(m.money,0)-ifnull(n.money,0) as money,
                           null as collectiontype,
                           null as billmemo,
                           0 as SKid
                    from
                        (
                            select a.client,sum(a.money)as money
                            from
                                (
                                    select a.client,ROUND(sum(b.qty*ifnull(a.price,0)),2)as money
                                    from whitebillfh a
                                             left outer join whitebilldetailfh b
                                                             on a.id=b.fid
                                    where DATE_FORMAT(a.datafh,'%Y-%m-%d') < m_Sdate
                                      and a.client=m_Clent
                                    GROUP BY a.client
                                    union all
                                    select m_Clent as client,0 as money
                                )a GROUP BY a.client
                        )m

                            left outer join
                        (
                            select client,sum(money)as money
                            from receivables
                            where DATE_FORMAT(billdate,'%Y-%m-%d') < m_Sdate
                              and client=m_Clent
                            GROUP BY client
                        )n on m.client=n.client


                    union all


                    #发货明细
                    select DATE_FORMAT(a.datafh,'%Y-%m-%d') as billdate,a.client,
                           CONCAT_WS(' ',c.order_code,c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as billtype,
                           b.qty,a.price,
                           ROUND(b.qty*ifnull(a.price,0),2)as money,
                           a.checkout as collectiontype,
                           a.memo as billmemo,
                           0 as SKid
                    from whitebillfh a
                             left outer join whitebilldetailfh b
                                             on a.id=b.fid
                             left outer join plan c
                                             on b.planid=c.id
                    where DATE_FORMAT(a.datafh,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                      and a.client=m_Clent

                    union ALL

                    #已经收款
                    select DATE_FORMAT(billdate,'%Y-%m-%d') as billdate,client,
                           billtype,null as qty,null as price,-money as money,collectiontype,billmemo,
                           id as SKid
                    from receivables
                    where DATE_FORMAT(billdate,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                      and client=m_Clent
                )p
            ORDER BY p.billdate) r
            GROUP BY r.client;
        end if;

    end if;

end
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
