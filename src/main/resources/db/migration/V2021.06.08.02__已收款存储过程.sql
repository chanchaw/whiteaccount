/*
 Navicat Premium Data Transfer



 Date: 08/06/2021 12:52:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for getCollectionOld
-- ----------------------------
DROP PROCEDURE IF EXISTS `getCollectionOld`;
delimiter ;;
CREATE PROCEDURE `getCollectionOld`(m_SDate varchar(100),
m_EDate varchar(100),
m_Client varchar(100),
m_Values varchar(100))
BEGIN

IF m_Values=0 THEN

		IF m_Client='全部' THEN

			select id,DATE_FORMAT(billdate,'%Y-%m-%d') as billdate,client,
			billtype,money,collectiontype,billmemo
			from receivables
			where DATE_FORMAT(billdate,'%Y-%m-%d') BETWEEN m_SDate and m_EDate
			and billtype='收款';
			
		ELSE
		
			select id,DATE_FORMAT(billdate,'%Y-%m-%d') as billdate,client,
			billtype,money,collectiontype,billmemo
			from receivables
			where DATE_FORMAT(billdate,'%Y-%m-%d') BETWEEN m_SDate and m_EDate
			and billtype='收款'
			and client=m_Client;


		end if;


ELSEIF m_Values=1 THEN

		IF m_Client='全部' THEN
		
			select 0 as id,null as billdate,client,
			null as billtype,sum(money)as money,null as collectiontype,null as billmemo
			from receivables
			where DATE_FORMAT(billdate,'%Y-%m-%d') BETWEEN m_SDate and m_EDate
			and billtype='收款'
			GROUP BY client;
		
		ELSE
		
		select 0 as id,null as billdate,client,
			null as billtype,sum(money)as money,null as collectiontype,null as billmemo
			from receivables
			where DATE_FORMAT(billdate,'%Y-%m-%d') BETWEEN m_SDate and m_EDate
			and billtype='收款'
			and client=m_Client
			GROUP BY client;
			
		end if;
	
end if;


end
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
