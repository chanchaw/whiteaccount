DROP PROCEDURE IF EXISTS `p_greyfabric_input`;
DELIMITER ;;
CREATE PROCEDURE `p_greyfabric_input`(IN `start` DATE, IN `end` DATE)
BEGIN
    /**
      2021年6月9日 白坯入库明细
     */
    SELECT *
    FROM (SELECT a.id,
                 c.company_name                                                              client,
                 b.plan_code                                                                 planCode,
                 a.cn                                                                        computerName,
                 CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.m_height, b.greyfabric_specification, b.material_specification) specification,
                 ifnull(sum(a.ps), 0)                                                        ps,
                 ifnull(sum(a.qty), 0)                                                       qty,
                 a.createdate,
                 a.class_name                                                                className,
                 a.jth                                                                       machineType,
                 a.aorb                                                                      aorb
          FROM jrkbillsum a
                   LEFT JOIN plan b ON a.planid = b.id
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
          WHERE DATE(a.createdate) BETWEEN DATE(`start`) AND DATE(`end`)
          GROUP BY a.createdate,a.aorb

          UNION ALL

          SELECT a.id,
                 c.company_name                                                              client,
                 b.plan_code                                                                 planCode,
                 a.bill_type                                                                 computerName,
                 CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.m_height, b.greyfabric_specification, b.material_specification) specification,
                 ifnull(a.pairs, 0)                                                          ps,
                 ifnull(a.qty, 0)                                                            qty,
                 a.jrkbill_date                                                              createdate,
                 NULL                                                                        className,
                 NULL                                                                        machineType,
                 NULL                                                                        aorb
          FROM jrkbill_adjust a
                   LEFT JOIN plan b ON a.plan_id = b.id
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
          WHERE bill_type = '手动入库'
            AND DATE(jrkbill_date) BETWEEN DATE(`start`) AND DATE(`end`)) rep
    ORDER BY rep.createdate DESC;
END;;
DELIMITER ;