-- ----------------------------
-- Procedure structure for getreceivablesDetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `getReceivablesDetail`;
delimiter ;;
CREATE PROCEDURE `getReceivablesDetail`(m_Sdate varchar(100),
                                        m_Edate varchar(100),
                                        m_Clent varchar(100),
                                        m_Value VARCHAR(100))
BEGIN

    IF m_Value=0 THEN

        if m_Clent='全部' THEN
            select p.*
            from
                (
                    select m_Sdate as billdate,'全部' as client,'期初结余' as billtype,null as qty,0 ps,null as price,
                           round(sum(ifnull(m.money,0))-sum(ifnull(n.money,0)),2) as money,
                           null as collectiontype,
                           null as billmemo,
                           0 as SKid
                    from
                        (
                            select a.client,sum(a.money)as money
                            from
                                (
                                    select a.client,ROUND(sum(b.qty*ifnull(a.price,0)),2)as money
                                    from whitebillfh a
                                             left outer join whitebilldetailfh b
                                                             on a.id=b.fid
                                    where DATE(a.datafh) < DATE(m_Sdate)
                                    GROUP BY a.client
                                    union all
                                    select m_Clent as client,0 as money
                                )a GROUP BY a.client
                        )m

                            left outer join
                        (
                            select client,sum(money) as money
                            from receivables
                            where DATE(billdate) < DATE(m_Sdate)
                            GROUP BY client
                        )n on m.client=n.client

                    union all


                    #发货明细
                    select DATE_FORMAT(a.datafh,'%Y-%m-%d') as billdate,a.client,
                           CONCAT_WS(' ',c.order_code,c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as billtype,
                           b.qty,b.ps,a.price,
                           ROUND(b.qty*ifnull(a.price,0),2)as money,
                           a.checkout as collectiontype,
                           a.memo as billmemo,
                           0 as SKid
                    from whitebillfh a
                             left outer join whitebilldetailfh b
                                             on a.id=b.fid
                             left outer join plan c
                                             on b.planid=c.id
                    where DATE_FORMAT(a.datafh,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                          #and a.client=m_Clent
                          AND  a.checkout <> '现金'

                    union ALL

                    #已经收款
                    select DATE_FORMAT(billdate,'%Y-%m-%d') as billdate,client,
                           billtype,null as qty,0 ps,null as price,-money as money,collectiontype,billmemo,
                           id as SKid
                    from receivables
                    where DATE_FORMAT(billdate,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                    #and client=m_Clent
                )p
            ORDER BY p.billdate;
        else
            select p.*
            from
                (
                    select m_Sdate as billdate,m.client as client,'期初结余' as billtype,null as qty,0 ps,null as price,ifnull(m.money,0)-ifnull(n.money,0) as money,
                           null as collectiontype,
                           null as billmemo,
                           NULL as SKid
                    from
                        (
                            select a.client,sum(a.money)as money
                            from
                                (
                                    select a.client,ROUND(sum(b.qty*ifnull(a.price,0)),2)as money
                                    from whitebillfh a
                                             left outer join whitebilldetailfh b
                                                             on a.id=b.fid
                                    where DATE_FORMAT(a.datafh,'%Y-%m-%d') < m_Sdate
                                      and a.client=m_Clent
                                    GROUP BY a.client
                                    union all
                                    select m_Clent as client,0 as money
                                )a GROUP BY a.client
                        )m

                            left outer join
                        (
                            select client,sum(money)as money
                            from receivables
                            where DATE_FORMAT(billdate,'%Y-%m-%d') < m_Sdate
                              and client=m_Clent
                            GROUP BY client
                        )n on m.client=n.client


                    union all


                    #发货明细
                    select DATE_FORMAT(a.datafh,'%Y-%m-%d') as billdate,a.client,
                           CONCAT_WS(' ',c.order_code,c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as billtype,
                           b.qty,b.ps,a.price,
                           ROUND(b.qty*ifnull(a.price,0),2)as money,
                           a.checkout as collectiontype,
                           a.memo as billmemo,
                           NULL as SKid
                    from whitebillfh a
                             left outer join whitebilldetailfh b
                                             on a.id=b.fid
                             left outer join plan c
                                             on b.planid=c.id
                    where DATE_FORMAT(a.datafh,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                      and a.client=m_Clent AND a.checkout <> '现金'

                    union ALL

                    #已经收款
                    select DATE_FORMAT(billdate,'%Y-%m-%d') as billdate,client,
                           billtype,null as qty,0 ps,0 price,- ifnull(money,0) as money,collectiontype,billmemo,
                           id as SKid
                    from receivables
                    where DATE_FORMAT(billdate,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                      and client=m_Clent
                )p
            ORDER BY p.billdate;
        end if;

    ELSEIF m_Value=1 THEN

        if m_Clent='全部' THEN
            SELECT r.client,round(ifnull(sum(r.money),0),2) money FROM (
                                                                           select p.*
                                                                           from
                                                                               (
                                                                                   /*select m_Sdate as billdate,'全部' as client,'期初结余' as billtype,null as qty,null as price,
                                                                                          round(sum(ifnull(m.money,0))-sum(ifnull(n.money,0)),2) as money,
                                                                                          null as collectiontype,
                                                                                          null as billmemo,
                                                                                          0 as SKid
                                                                                   from
                                                                                       (
                                                                                           select a.client,sum(a.money)as money
                                                                                           from
                                                                                               (
                                                                                                   select a.client,ROUND(sum(b.qty*ifnull(a.price,0)),2)as money
                                                                                                   from whitebillfh a
                                                                                                            left outer join whitebilldetailfh b
                                                                                                                            on a.id=b.fid
                                                                                                   where DATE(a.datafh) < DATE(m_Sdate)
                                                                                                         #and a.client=m_Clent
                                                                                                   GROUP BY a.client
                                                                                                   union all
                                                                                                   select m_Clent as client,0 as money
                                                                                               )a GROUP BY a.client
                                                                                       )m

                                                                                           left outer join
                                                                                       (
                                                                                           select client,sum(money) as money
                                                                                           from receivables
                                                                                           where DATE(billdate) < DATE(m_Sdate)
                                                                                           GROUP BY client
                                                                                       )n on m.client=n.client

                                                                                   union all*/


                                                                                   #发货明细
                                                                                   select DATE_FORMAT(a.datafh,'%Y-%m-%d') as billdate,a.client,
                                                                                          CONCAT_WS(' ',c.order_code,c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as billtype,
                                                                                          b.qty,a.price,
                                                                                          ROUND(b.qty*ifnull(a.price,0),2)as money,
                                                                                          a.checkout as collectiontype,
                                                                                          a.memo as billmemo,
                                                                                          0 as SKid
                                                                                   from whitebillfh a
                                                                                            left outer join whitebilldetailfh b
                                                                                                            on a.id=b.fid
                                                                                            left outer join plan c
                                                                                                            on b.planid=c.id
                                                                                         WHERE a.checkout <> '现金'
                                                                                        # where DATE_FORMAT(a.datafh,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                                                                                        #and a.client=m_Clent

                                                                                   union ALL

                                                                                   #已经收款
                                                                                   select DATE_FORMAT(billdate,'%Y-%m-%d') as billdate,client,
                                                                                          billtype,null as qty,null as price,-money as money,collectiontype,billmemo,
                                                                                          id as SKid
                                                                                   from receivables
                                                                                   # where DATE_FORMAT(billdate,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                                                                                   #and client=m_Clent
                                                                               )p
                                                                           ORDER BY p.billdate
                                                                       ) r
            GROUP BY r.client;
        else
            SELECT
                r.client, round(ifnull(sum(r.money),0),2) money
            FROM (
                     select p.*
                     from
                         (
                             select m_Sdate as billdate,m.client as client,'期初结余' as billtype,null as qty,null as price,ifnull(m.money,0)-ifnull(n.money,0) as money,
                                    null as collectiontype,
                                    null as billmemo,
                                    0 as SKid
                             from
                                 (
                                     select a.client,sum(a.money)as money
                                     from
                                         (
                                             select a.client,ROUND(sum(b.qty*ifnull(a.price,0)),2)as money
                                             from whitebillfh a
                                                      left outer join whitebilldetailfh b
                                                                      on a.id=b.fid
                                             where DATE_FORMAT(a.datafh,'%Y-%m-%d') < m_Sdate
                                               and a.client=m_Clent AND a.checkout <> '现金'
                                             GROUP BY a.client
                                             union all
                                             select m_Clent as client,0 as money
                                         )a GROUP BY a.client
                                 )m

                                     left outer join
                                 (
                                     select client,sum(money)as money
                                     from receivables
                                     where DATE_FORMAT(billdate,'%Y-%m-%d') < m_Sdate
                                       and client=m_Clent
                                     GROUP BY client
                                 )n on m.client=n.client


                             union all


                             #发货明细
                             select DATE_FORMAT(a.datafh,'%Y-%m-%d') as billdate,a.client,
                                    CONCAT_WS(' ',c.order_code,c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as billtype,
                                    b.qty,a.price,
                                    ROUND(b.qty*ifnull(a.price,0),2)as money,
                                    a.checkout as collectiontype,
                                    a.memo as billmemo,
                                    0 as SKid
                             from whitebillfh a
                                      left outer join whitebilldetailfh b
                                                      on a.id=b.fid
                                      left outer join plan c
                                                      on b.planid=c.id
                             where DATE_FORMAT(a.datafh,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                               and a.client=m_Clent AND a.checkout <> '现金'

                             union ALL

                             #已经收款
                             select DATE_FORMAT(billdate,'%Y-%m-%d') as billdate,client,
                                    billtype,null as qty,null as price,-money as money,collectiontype,billmemo,
                                    id as SKid
                             from receivables
                             where DATE_FORMAT(billdate,'%Y-%m-%d') BETWEEN m_Sdate and m_Edate
                               and client=m_Clent
                         )p
                     ORDER BY p.billdate) r
            GROUP BY r.client;
        end if;

    end if;

end
;;
delimiter ;