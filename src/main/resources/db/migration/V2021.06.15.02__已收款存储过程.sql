/*
 Navicat Premium Data Transfer

 Date: 08/06/2021 12:52:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `getCollectionOld`;
DELIMITER ;;
CREATE PROCEDURE `getCollectionOld`(m_SDate VARCHAR(100),
                                    m_EDate VARCHAR(100),
                                    m_Client VARCHAR(100),
                                    m_Values VARCHAR(100))
BEGIN

    IF m_Values = 0 THEN

        IF m_Client = '全部' THEN

            SELECT id, DATE_FORMAT(billdate, '%Y-%m-%d') AS billdate, client, billtype, money, collectiontype, billmemo
            FROM receivables
            WHERE DATE_FORMAT(billdate, '%Y-%m-%d') BETWEEN m_SDate AND m_EDate
              AND billtype = '收款'
            UNION ALL
            SELECT a.id,
                   DATE(a.datafh)                       billdate,
                   a.client,
                   '发货'                                 billtype,
                   round(ifnull(a.price, 0) * b.qty, 2) money,
                   a.checkout                           collectiontype,
                   a.memo                               billmemo
            FROM whitebillfh a
                     LEFT JOIN whitebilldetailfh b ON a.id = b.fid
            WHERE a.checkout = '现金'
              AND DATE(a.datafh) BETWEEN DATE(`m_SDate`) AND DATE(`m_EDate`);

        ELSE

            SELECT id, DATE_FORMAT(billdate, '%Y-%m-%d') AS billdate, client, billtype, money, collectiontype, billmemo
            FROM receivables
            WHERE DATE_FORMAT(billdate, '%Y-%m-%d') BETWEEN m_SDate AND m_EDate
              AND billtype = '收款'
              AND client = m_Client
            UNION ALL
            SELECT a.id,
                   DATE(a.datafh)                       billdate,
                   a.client,
                   '发货'                                 billtype,
                   round(ifnull(a.price, 0) * b.qty, 2) money,
                   a.checkout                           collectiontype,
                   a.memo                               billmemo
            FROM whitebillfh a
                     LEFT JOIN whitebilldetailfh b ON a.id = b.fid
            WHERE a.checkout = '现金'
              AND a.client = `m_Client`
              AND DATE(a.datafh) BETWEEN DATE(`m_SDate`) AND DATE(`m_EDate`);

        END IF;


    ELSEIF m_Values = 1 THEN

        IF m_Client = '全部' THEN

            SELECT 0          AS id,
                   NULL       AS billdate,
                   client,
                   NULL       AS billtype,
                   sum(money) AS money,
                   NULL       AS collectiontype,
                   NULL       AS billmemo
            FROM receivables
            WHERE DATE_FORMAT(billdate, '%Y-%m-%d') BETWEEN m_SDate AND m_EDate
              AND billtype = '收款'
            GROUP BY client
            UNION ALL
            SELECT a.id,
                   DATE(a.datafh)                 billdate,
                   a.client,
                   '发货'                           billtype,
                   sum(round(a.price * b.qty, 2)) money,
                   a.checkout                     collectiontype,
                   a.memo                         billmemo
            FROM whitebillfh a
                     LEFT JOIN whitebilldetailfh b ON a.id = b.fid
            WHERE a.checkout = '现金'
              AND DATE(a.datafh) BETWEEN DATE(`m_SDate`) AND DATE(`m_EDate`)
            GROUP BY a.client;

        ELSE

            SELECT 0          AS id,
                   NULL       AS billdate,
                   client,
                   NULL       AS billtype,
                   sum(money) AS money,
                   NULL       AS collectiontype,
                   NULL       AS billmemo
            FROM receivables
            WHERE DATE_FORMAT(billdate, '%Y-%m-%d') BETWEEN m_SDate AND m_EDate
              AND billtype = '收款'
              AND client = m_Client
            GROUP BY client
            UNION ALL
            SELECT a.id,
                   DATE(a.datafh)                            billdate,
                   a.client,
                   '发货'                                      billtype,
                   sum(round(ifnull(a.price, 0) * b.qty, 2)) money,
                   a.checkout                                collectiontype,
                   a.memo                                    billmemo
            FROM whitebillfh a
                     LEFT JOIN whitebilldetailfh b ON a.id = b.fid
            WHERE a.checkout = '现金'
              AND a.client = `m_Client`
              AND DATE(a.datafh) BETWEEN DATE(`m_SDate`) AND DATE(`m_EDate`)
            GROUP BY a.client;
        END IF;
    END IF;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;