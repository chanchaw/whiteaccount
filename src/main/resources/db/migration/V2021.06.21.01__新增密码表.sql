/**
  新增密码表
 */
create table sys_password
(
    sys_field VARCHAR(50) not null comment '密码类型',
    sys_password VARCHAR(100) null comment '密码',
    constraint sys_password_pk
        primary key (sys_field)
)
    comment '密码';