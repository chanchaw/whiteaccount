/**
  加工类型 基础资料
  2021年6月23日
 */
create table process_type
(
    id int auto_increment comment '自增主键',
    type_name VARCHAR(100) null comment '加工类型名称',
    constraint process_type_pk
        primary key (id)
)
    comment '加工类型';

create unique index process_type_type_name_uindex
    on process_type (type_name);
