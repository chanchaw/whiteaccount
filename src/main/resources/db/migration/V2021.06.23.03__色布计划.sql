/**
    2021年6月23日 色布计划
 */
create table dye_fabric_plan
(
    id BIGINT auto_increment comment '自增主键',
    contact_company_id VARCHAR(50) not null comment '外键,往来单位',
    contract_code VARCHAR(100) null comment '合同号',
    plan_code VARCHAR(100) null comment '计划单号',
    plan_date DATE null comment '计划日期',
    order_code VARCHAR(100) null comment '订单号',
    product_name VARCHAR(100) null comment '品名',
    process_type VARCHAR(100) not null comment '类型',
    material_specification VARCHAR(100) null comment '规格',
    color_no VARCHAR(100) null comment '色号/花号',
    color VARCHAR(100) null comment '颜色/花型',
    net_diff decimal(30,1) comment '毛净差',
    rate decimal(10,1) comment '米克系数',
    meter_counter INT null default 0 comment '计米',
    plan_kilo DECIMAL(60,1) null comment '计划重量',
    material_price DECIMAL(60,2) null comment '价格',
    plan_employee_id VARCHAR(50) null comment '跟单人员',
    remarks VARCHAR(300) null comment '备注',
    submit_date DATE null comment '交期',
    create_user_id int null comment '外键,创建人',
    create_time DATETIME default CURRENT_TIMESTAMP null comment '创建日期，DB自动填充',
    upload int default 0 null comment '上传字段(0:未上传;1:已上传;)',
    state INT default 1 null comment '状态(0:作废;1:正常;)',
    constraint dye_fabric_plan_pk
        primary key (id),
    constraint dye_fabric_plan_contact_company_id_fk
        foreign key (contact_company_id) references contact_company (id)
)
    comment '色布计划';

create unique index dye_fabric_plan_plan_code_uindex
    on dye_fabric_plan (plan_code);