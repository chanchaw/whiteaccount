/*
 色布计划单编号生成存储过程
 Date: 18/05/2021 16:10:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP FUNCTION IF EXISTS `f_dye_plan_codegen`;
DELIMITER ;;
CREATE FUNCTION `f_dye_plan_codegen`(prefix VARCHAR(50)) RETURNS VARCHAR(100) CHARSET utf8
BEGIN
    DECLARE currentDate VARCHAR(50); -- 日期
    DECLARE curNum BIGINT; -- 当前记数
    DECLARE result VARCHAR(100); -- 结果
    DECLARE curNumCount INT; -- 记数位数
    DECLARE currentDateCount INT; -- 日期位数
    DECLARE currentDateStart INT; -- 日期截取位置(0:左侧截取;1:右侧截取;)
    DECLARE currentDateLen INT; -- 日期截取位置(0:左侧截取;1:右侧截取;)
    SELECT ifnull(config_value, 4) INTO curNumCount FROM sys_config_int WHERE config_field = '计划单号记数位数';
    SELECT ifnull(config_value, 8) INTO currentDateCount FROM sys_config_int WHERE config_field = '计划单号日期位数';
    SELECT ifnull(config_value, 0) INTO currentDateStart FROM sys_config_int WHERE config_field = '计划单号日期截取起始位置';
    SELECT ifnull(config_value, 0) INTO currentDateLen FROM sys_config_int WHERE config_field = '计划单号日期截取长度';
    IF ifnull(curNumCount, 0) = 0 THEN SET curNumCount = 4; END IF;
    IF ifnull(currentDateCount, 0) = 0 THEN SET currentDateCount = 8; END IF;
    IF ifnull(currentDateStart, 0) = 0 THEN SET currentDateStart = 3; END IF;
    IF ifnull(currentDateLen, 0) = 0 THEN SET currentDateLen = 2; END IF;
    SELECT substr(concat(DATE_FORMAT(NOW(), '%Y'), DATE_FORMAT(NOW(), '%m'), DATE_FORMAT(NOW(), '%d')),
                  currentDateStart, currentDateLen)
    INTO currentDate;

    -- 创建临时表
    DROP TEMPORARY TABLE IF EXISTS t_num,t_num_tmp;
    CREATE TEMPORARY TABLE t_num
    (
        pk_id       BIGINT AUTO_INCREMENT PRIMARY KEY,
        count_value BIGINT
    );
    CREATE TEMPORARY TABLE t_num_tmp
    (
        pk_id       BIGINT AUTO_INCREMENT PRIMARY KEY,
        count_value BIGINT
    );
    INSERT INTO t_num (count_value)
    SELECT cast(substring(plan_code, length(`prefix`) + length(`currentDate`) + 1, curNumCount) AS DECIMAL(24)) num
    FROM dye_fabric_plan
    WHERE plan_code LIKE concat(`prefix`, `currentDate`, '%');

    INSERT INTO t_num_tmp (count_value)
    SELECT cast(substring(plan_code, length(`prefix`) + length(`currentDate`) + 1, curNumCount) AS DECIMAL(24)) num
    FROM dye_fabric_plan
    WHERE plan_code LIKE concat(`prefix`, `currentDate`, '%');

    SELECT ifnull(a.count_value + 1, 1)
    INTO `curNum`
    FROM t_num a
    WHERE a.count_value + 1 NOT IN (SELECT count_value FROM t_num_tmp)
    ORDER BY a.pk_id
    LIMIT 1;

    SELECT concat(`prefix`, substring(currentDate, 1), lpad(ifnull(curNum, 1), curNumCount, '0')) INTO result;
    RETURN result;

END
;;
SET FOREIGN_KEY_CHECKS = 1;
