DROP PROCEDURE IF EXISTS `p_dye_fabric_plan_report`;
DELIMITER ;;
CREATE PROCEDURE p_dye_fabric_plan_report(IN `start` DATE, IN `end` DATE)
BEGIN
    /**
      2021年6月24日 色布计划序时表
     */
    SELECT a.id                   id,
           DATE(a.plan_date)      plan_date,
           a.contact_company_id,
           b.company_name,
           a.contract_code,
           a.plan_code,
           a.order_code,
           a.product_name,
           a.process_type,
           a.material_specification,
           a.color_no,
           a.color,
           ifnull(a.net_diff, 0)  net_diff,
           ifnull(a.plan_kilo, 0) plan_kilo,
           ifnull(a.rate, 0)      rate,
           a.material_price,
           a.plan_employee_id,
           c.emp_name             employee_name,
           DATE(a.submit_date)    submit_date,
           a.create_user_id       create_user_id,
           d.user_name            create_user_name,
           a.remarks              remarks,
           0                      input_pairs,
           0                      delivery_pairs,
           0                      remain_pairs,
           0                      input_kilo,
           0                      delivery_kilo,
           0                      remain_kilo
    FROM dye_fabric_plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN employee c ON a.plan_employee_id = c.id
             LEFT JOIN sys_user d ON a.create_user_id = d.id
    WHERE a.state = 1
      AND DATE(a.plan_date) BETWEEN DATE(`start`) AND DATE(`end`);
END ;;
DELIMITER ;