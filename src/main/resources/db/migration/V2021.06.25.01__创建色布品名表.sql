/**
  2021年6月25日 创建色布品名表
 */
create table dye_product_info
(
    id int auto_increment comment '自增主键',
    product_name VARCHAR(100) not null comment '品名',
    product_code VARCHAR(100) null comment '编号',
    remarks VARCHAR(100) null comment '备注',
    constraint dye_product_info_pk
        primary key (id)
)
    comment '色布品名';

create unique index dye_product_info_product_name_uindex
    on dye_product_info (product_name);