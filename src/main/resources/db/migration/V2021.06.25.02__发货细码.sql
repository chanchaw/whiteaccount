/*
 发货细码
 Date: 25/06/2021 09:23:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `cp_jrkbillfh`;
CREATE TABLE `cp_jrkbillfh`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qty` decimal(50, 2) NULL DEFAULT NULL COMMENT '发货时每匹公斤数',
  `isorder` int(255) NULL DEFAULT NULL COMMENT '发货序号',
  `computername` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '计算机名',
  `isfh` int(255) NULL DEFAULT NULL COMMENT '标记是否发货 1表示发货',
  `isdelete` int(255) NULL DEFAULT NULL COMMENT '标记当前行数据是否删除 1表示已经删除',
  `planid` int(11) NULL DEFAULT NULL COMMENT '对应订单的主键(即计划主键)',
  `fid` int(11) NULL DEFAULT NULL COMMENT '生成发货单对应发货明细表主键',
  `upload` int(11) NULL DEFAULT NULL COMMENT '是否上传（1上传，0空否）',
  `price` decimal(10, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
