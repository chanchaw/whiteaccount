/*

 发货单表
 Date: 25/06/2021 09:23:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `cp_billfh`;
CREATE TABLE `cp_billfh`
(
    `id`        INT(11)                                                 NOT NULL AUTO_INCREMENT COMMENT '发货主表主键（色布发货）',
    `planid`    INT(10)                                                 NULL DEFAULT NULL COMMENT '计划单主键',
    `billcode`  VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据编号',
    `client`    VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户',
    `datafh`    DATETIME(0)                                             NULL DEFAULT NULL COMMENT '发货时间',
    `ps`        VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '匹数',
    `qty`       VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重量',
    countable   INT                                                     NULL COMMENT '件数',
    `price`     DECIMAL(50, 2)                                          NULL DEFAULT NULL COMMENT '单价',
    `updatefh`  INT(11)                                                 NULL DEFAULT NULL COMMENT '是否修改 （1：修改，0/空：未修改）',
    `upload`    INT(11)                                                 NULL DEFAULT NULL COMMENT '是否上传 (1：已上传)',
    `checkout`  VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记账',
    `memo`      VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发货备注',
    `manager`   VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '经手人',
    `carnumber` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '车牌号',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
