/**
  2021年6月25日 增加完工状态
 */
alter table dye_fabric_plan
    add mark_finished TINYINT(1) default 0 null comment '完工状态(0:未完工;1:已完工;)' after submit_date;