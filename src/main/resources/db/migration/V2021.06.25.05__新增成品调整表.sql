/**
  2021年6月25日 新增成品调整表
 */
CREATE TABLE cp_adjust
(
    id             INT AUTO_INCREMENT COMMENT '自增主键' PRIMARY KEY,
    plan_id        BIGINT                                   NULL COMMENT '外键,对应dye_fabric_plan.id',
    bill_date   DATETIME                                 NULL COMMENT '日期',
    pairs          INT            DEFAULT 0                 NULL COMMENT '匹数',
    qty            DECIMAL(20, 1)                           NULL COMMENT '公斤',
    countable      INT                                      NULL COMMENT '件数',
    bill_type      VARCHAR(50)                              NULL COMMENT '调整类型',
    create_time    DATETIME       DEFAULT CURRENT_TIMESTAMP NULL COMMENT '创建时间',
    create_user_id INT                                      NULL COMMENT '创建人编号',
    remarks        VARCHAR(255)                             NULL COMMENT '备注',
    modify_user_id INT                                      NULL COMMENT '修改人编号',
    modify_time    DATETIME                                 NULL COMMENT '修改时间',
    cancel_user_id INT                                      NULL COMMENT '作废人编号',
    cancel_time    DATETIME                                 NULL COMMENT '作废时间',
    state          INT            DEFAULT 1                 NULL COMMENT '状态(0:作废;1:正常;)',
    upload         INT                                      NULL COMMENT '1：已上传，0：待上传',
    client_name    VARCHAR(100)                             NULL COMMENT '客户名称',
    price          DECIMAL(60, 2) DEFAULT 0.00              NULL COMMENT '单价'
) COMMENT '打卷调整表';
CREATE INDEX cp_adjust_cancel_user_id_index ON cp_adjust (cancel_user_id);
CREATE INDEX cp_adjust_create_user_id_index ON cp_adjust (create_user_id);
CREATE INDEX cp_adjust_modify_user_id_index ON cp_adjust (modify_user_id);
CREATE INDEX cp_adjust_plan_id_index ON cp_adjust (plan_id);