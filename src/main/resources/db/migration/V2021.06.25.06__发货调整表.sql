SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `cp_receivables`;
CREATE TABLE `cp_receivables` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '应收款表 主键',
  `billtype` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据类型（收款/收款调整）',
  `client` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  `billdate` datetime(0) NULL DEFAULT NULL COMMENT '单据时间',
  `collectiontype` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收款类型（转账/承兑/现金）',
  `billmemo` varchar(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `upload` int(11) NULL DEFAULT NULL COMMENT '是否上传云端',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic COMMENT = '发货调整收款表';

SET FOREIGN_KEY_CHECKS = 1;
