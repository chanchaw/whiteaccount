DROP PROCEDURE IF EXISTS `p_dye_fabric_delivery`;
DELIMITER ;;
CREATE PROCEDURE p_dye_fabric_delivery(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100),
                                       IN `checkout` VARCHAR(100))
BEGIN
    /**
     2021年6月25日 色布发货序时表
     */
    IF (isnull(`checkout`) OR length(`checkout`) <= 0) THEN
        IF (isnull(`clientName`) OR length(`clientName`) <= 0) THEN
            SELECT a.client                   client,
                   concat_ws(',', b.contract_code, b.plan_code, b.product_name, b.material_specification, b.color_no,
                             b.color)         specification,
                   ifnull(a.ps, 0)            ps,
                   ifnull(a.qty, 0)           qty,
                   ifnull(a.countable, 0)     countable,
                   ifnull(a.price, 0)         price,
                   ifnull(a.price, 0) * a.qty money,
                   a.checkout                 checkout,
                   a.billcode                 billcode,
                   DATE(a.datafh)             datafh,
                   a.carnumber                carnumber,
                   a.memo                     memo
            FROM cp_billfh a
                     LEFT JOIN dye_fabric_plan b ON a.planid = b.id
            WHERE DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`);
        ELSE
            SELECT a.client                   client,
                   concat_ws(',', b.contract_code, b.plan_code, b.product_name, b.material_specification, b.color_no,
                             b.color)         specification,
                   ifnull(a.ps, 0)            ps,
                   ifnull(a.qty, 0)           qty,
                   ifnull(a.countable, 0)     countable,
                   ifnull(a.price, 0)         price,
                   ifnull(a.price, 0) * a.qty money,
                   a.checkout                 checkout,
                   a.billcode                 billcode,
                   DATE(a.datafh)             datafh,
                   a.carnumber                carnumber,
                   a.memo                     memo
            FROM cp_billfh a
                     LEFT JOIN dye_fabric_plan b ON a.planid = b.id
            WHERE a.client = `clientName`
              AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`);
        END IF;
    ELSE
        IF (isnull(`clientName`) OR length(`clientName`) <= 0) THEN
            SELECT a.client                   client,
                   concat_ws(',', b.contract_code, b.plan_code, b.product_name, b.material_specification, b.color_no,
                             b.color)         specification,
                   ifnull(a.ps, 0)            ps,
                   ifnull(a.qty, 0)           qty,
                   ifnull(a.countable, 0)     countable,
                   ifnull(a.price, 0)         price,
                   ifnull(a.price, 0) * a.qty money,
                   a.checkout                 checkout,
                   a.billcode                 billcode,
                   DATE(a.datafh)             datafh,
                   a.carnumber                carnumber,
                   a.memo                     memo
            FROM cp_billfh a
                     LEFT JOIN dye_fabric_plan b ON a.planid = b.id
            WHERE ifnull(a.checkout, '') = `checkout`
              AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`);
        ELSE
            SELECT a.client                   client,
                   concat_ws(',', b.contract_code, b.plan_code, b.product_name, b.material_specification, b.color_no,
                             b.color)         specification,
                   ifnull(a.ps, 0)            ps,
                   ifnull(a.qty, 0)           qty,
                   ifnull(a.countable, 0)     countable,
                   ifnull(a.price, 0)         price,
                   ifnull(a.price, 0) * a.qty money,
                   a.checkout                 checkout,
                   a.billcode                 billcode,
                   DATE(a.datafh)             datafh,
                   a.carnumber                carnumber,
                   a.memo                     memo
            FROM cp_billfh a
                     LEFT JOIN dye_fabric_plan b ON a.planid = b.id
            WHERE a.client = `clientName`
              AND ifnull(a.checkout, '') = `checkout`
              AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`);
        END IF;
    END IF;
END ;;
DELIMITER ;