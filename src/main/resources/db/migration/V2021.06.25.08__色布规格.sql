/**
  2021年6月25日 色布规格
 */
create table dye_specification
(
    id int auto_increment comment '自增主键',
    spec_name VARCHAR(100) null comment '规格名称',
    constraint dye_specification_pk
        primary key (id)
)
    comment '色布规格';

create unique index dye_specification_spec_name_uindex
    on dye_specification (spec_name);