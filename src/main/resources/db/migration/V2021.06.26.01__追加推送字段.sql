/**
  2021年6月26日 追加推送字段
 */
alter table cp_adjust
    add is_push TINYINT(1) default 0 null comment '是否已经推送(0：未推送；1：已推送；)';
alter table jrkbill_adjust
    add is_push TINYINT(1) default 0 null comment '是否已经推送(0：未推送；1：已推送；)';
alter table cp_billfh
    add is_push TINYINT(1) default 0 null comment '是否已经推送(0：未推送；1：已推送；)';
alter table whitebilldetailfh
    add is_push TINYINT(1) default 0 null comment '是否已经推送(0：未推送；1：已推送；)';