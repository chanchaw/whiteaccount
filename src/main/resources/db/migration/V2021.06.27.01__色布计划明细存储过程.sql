DROP PROCEDURE IF EXISTS `p_dye_plan_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_dye_plan_dtl`(IN `p_planId` BIGINT)
BEGIN
    /**
     2021年6月27日  色布计划明细
     */
    SELECT a.id                                                                                         id,
           b.company_name                                                                               company_name,
           concat_ws(' ', ifnull(a.contract_code, ''), ifnull(a.plan_code, ''), ifnull(a.product_name, ''),
                     ifnull(a.material_specification, ''), ifnull(a.color_no, ''), ifnull(a.color, '')) product_name,
           ifnull(a.plan_kilo, 0)                                                                       plan_kilo,
           ifnull(c.pairs, 0)                                                                           input_pairs,
           ifnull(c.qty, 0)                                                                             input_kilo,
           ifnull(d.qty, 0) + ifnull(e.qty, 0)                                                          send_kilo,
           ifnull(d.ps, 0) + ifnull(e.pairs, 0)                                                         send_pairs,
           ifnull(round(ifnull(a.plan_kilo, 0) - ifnull(d.qty, 0) - ifnull(e.qty, 0), 1),
                  0)                                                                                    plan_remain_kilo,
           ifnull(round(ifnull(c.qty, 0) - ifnull(d.qty, 0) - ifnull(e.qty, 0), 1), 0)                  remain_kilo,
           ifnull(round(ifnull(c.pairs, 0) - ifnull(d.ps, 0) - ifnull(e.pairs, 0), 1), 0)               remain_pairs,
           ifnull(a.material_price, 0)                                                                  price
    FROM dye_fabric_plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN (
        #         入库
        SELECT sum(pairs) pairs, sum(qty) qty, plan_id
        FROM cp_adjust
        WHERE plan_id = `p_planId`
          AND bill_type IN ('手动入库', '盘点')
        GROUP BY plan_id) c ON c.plan_id = a.id
             LEFT JOIN (
        #         出库
        SELECT sum(ps) ps, sum(qty) qty, planid FROM cp_billfh WHERE planid = `p_planId` GROUP BY planId) d
                       ON d.planId = a.id
             LEFT JOIN (
        #         出库调整
        SELECT sum(pairs) pairs, sum(qty) qty, plan_id
        FROM cp_adjust
        WHERE plan_id = `p_planId`
          AND bill_type = '手动出库'
        GROUP BY plan_id) e ON e.plan_id = a.id
    WHERE a.id = `p_planId`;
END ;;
DELIMITER ;