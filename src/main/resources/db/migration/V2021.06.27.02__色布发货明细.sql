DROP PROCEDURE IF EXISTS `p_dye_fh_dtl`;
DELIMITER ;;
CREATE PROCEDURE p_dye_fh_dtl(IN id BIGINT)
BEGIN
    /**
       2021年6月27日 色布发货明细
     */
    SELECT r.*
    FROM ((SELECT a.client                             client_name,
                  a.billcode                           billcode,
                  ifnull(a.countable, 0)               countable,
                  ifnull(sum(a.ps), 0)                 send_pairs,
                  sum(round(a.qty, 1))                 send_kilo,
                  d.material_specification             material_specification,
                  date_format(a.datafh, '%m-%d %H:%i') send_date,
                  a.memo                               remarks,
                  a.datafh                             order_date
           FROM cp_billfh a
                    LEFT JOIN dye_fabric_plan d ON a.planid = d.id
           WHERE a.planid = `id`
             AND a.id IS NOT NULL
           GROUP BY a.id)
          UNION ALL
          (SELECT c.company_name                          client_name,
                  b.bill_type                             billcode,
                  ifnull(b.countable, 0)                  countable,
                  ifnull(b.pairs, 0)                      send_pairs,
                  round(b.qty, 1)                         send_kilo,
                  a.material_specification                material_specification,
                  date_format(b.bill_date, '%m-%d %H:%i') send_date,
                  b.remarks                               remarks,
                  b.bill_date                             order_date
           FROM dye_fabric_plan a
                    LEFT JOIN cp_adjust b ON a.id = b.plan_id
                    LEFT JOIN contact_company c ON a.contact_company_id = c.id
           WHERE b.plan_id = `id`
             AND b.bill_type = '手动出库'
             AND b.id IS NOT NULL)) r
    ORDER BY r.order_date DESC;
END;;
DELIMITER ;