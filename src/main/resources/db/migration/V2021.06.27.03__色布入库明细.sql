DROP PROCEDURE IF EXISTS `p_dye_inbound_dtl`;
DELIMITER ;;
CREATE PROCEDURE p_dye_inbound_dtl(IN id BIGINT)
BEGIN
    /**
       2021年6月27日 色布入库明细
     */
    SELECT *
    FROM ((SELECT b.id                                    id,
                  ifnull(b.pairs, 0)                      input_pairs,
                  ifnull(round(b.qty, 1), 0)              input_kilo,
                  NULL                                    aorb,
                  a.material_specification                material_specification,
                  b.bill_type                             computer_name,
                  NULL                                    machine_type,
                  date_format(b.bill_date, '%m-%d %H:%i') createdate,
                  b.remarks                               remarks,
                  b.bill_type                             bill_type,
                  b.bill_date                             order_date
           FROM dye_fabric_plan a
                    LEFT JOIN cp_adjust b ON a.id = b.plan_id
           WHERE a.id = `id`
             AND b.bill_type in ('手动入库','盘点')
             AND b.id IS NOT NULL)) r
    ORDER BY r.order_date DESC;
END;;
DELIMITER ;