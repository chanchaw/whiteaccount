DROP PROCEDURE IF EXISTS `p_dye_plan_storage_modify`;
DELIMITER ;;
CREATE PROCEDURE p_dye_plan_storage_modify(IN id BIGINT)
BEGIN
    /**
      2021年6月27日  库存调整存储过程
     */
    SELECT *
    FROM (SELECT report.*,
                 @storage_ps := ifnull(report.pairs, 0) + @storage_ps  remain_pairs,
                 @storage_qty := ifnull(report.kilo, 0) + @storage_qty remain_kilo
          FROM (
                   --          手动调整
                   (SELECT b.id                                                              id,
                           a.id                                                              plan_id,
                           date_format(b.bill_date, '%m-%d %H:%i')                           jrkbill_date,
                           ifnull(b.bill_type, '')                                           bill_type,
                           if(b.bill_type = '手动出库', -ifnull(b.pairs, 0), ifnull(b.pairs, 0)) pairs,
                           if(b.bill_type = '手动出库', -ifnull(b.qty, 0), ifnull(b.qty, 0))     kilo,
                           b.remarks                                                         remarks,
                           b.client_name                                                     client_name,
                           b.countable                                                       countable,
                           b.price                                                           price,
                           b.bill_date                                                       order_date
                    FROM dye_fabric_plan a
                             LEFT JOIN cp_adjust b ON a.id = b.plan_id AND b.state = 1
                    WHERE a.id = `id`
                      AND b.id IS NOT NULL)

                   UNION ALL
                   --          发货
                   (SELECT NULL                                 id,
                           a.id                                 plan_id,
                           date_format(b.datafh, '%m-%d %H:%i') jrkbill_date,
                           '发货'                                 bill_type,
                           -ifnull(b.ps, 0)                     pairs,
                           -ifnull(b.qty, 0)                    kilo,
                           b.memo                               remarks,
                           b.client                             client_name,
                           b.countable                          countable,
                           b.price                              price,
                           b.datafh                             order_date
                    FROM dye_fabric_plan a
                             LEFT JOIN cp_billfh b ON a.id = b.planid
                    WHERE a.id = `id`
                      AND b.id IS NOT NULL)) report,
               (SELECT @storage_ps := 0) p,
               (SELECT @storage_qty := 0) q
          ORDER BY report.order_date) r
    ORDER BY r.order_date DESC;
END;;
DELIMITER ;