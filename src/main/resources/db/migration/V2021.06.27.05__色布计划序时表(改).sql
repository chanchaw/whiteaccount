DROP PROCEDURE IF EXISTS `p_dye_fabric_plan_report`;
DELIMITER ;;
CREATE PROCEDURE p_dye_fabric_plan_report(IN `start` DATE, IN `end` DATE, IN `markFinished` TINYINT(1))
BEGIN
    /**
      2021年6月24日 色布计划序时表
      2021年6月27日 按照日期到排序
      2021年6月28日 追加入库出库数量
     */
    SELECT a.id                                                      id,
           DATE(a.plan_date)                                         plan_date,
           a.contact_company_id,
           b.company_name,
           a.contract_code,
           a.plan_code,
           a.order_code,
           a.product_name,
           a.process_type,
           a.material_specification,
           a.color_no,
           a.color,
           ifnull(a.net_diff, 0)                                     net_diff,
           ifnull(a.plan_kilo, 0)                                    plan_kilo,
           ifnull(a.rate, 0)                                         rate,
           a.material_price,
           a.plan_employee_id,
           c.emp_name                                                employee_name,
           DATE(a.submit_date)                                       submit_date,
           a.create_user_id                                          create_user_id,
           d.user_name                                               create_user_name,
           a.remarks                                                 remarks,
           ifnull(e.pairs, 0)                                        input_pairs,
           ifnull(f.ps, 0) + ifnull(g.pairs, 0)                      delivery_pairs,
           ifnull(e.pairs, 0) - ifnull(f.ps, 0) - ifnull(g.pairs, 0) remain_pairs,
           ifnull(e.qty, 0)                                          input_kilo,
           ifnull(f.qty, 0) + ifnull(g.qty, 0)                       delivery_kilo,
           ifnull(e.qty, 0) - ifnull(f.qty, 0) - ifnull(g.qty, 0)    remain_kilo,
           ifnull(a.mark_finished, 0)                                mark_finished
    FROM dye_fabric_plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN employee c ON a.plan_employee_id = c.id
             LEFT JOIN sys_user d ON a.create_user_id = d.id
             LEFT JOIN (
        #         入库
        SELECT sum(pairs) pairs, sum(qty) qty, plan_id
        FROM cp_adjust
        WHERE bill_type IN ('手动入库', '盘点')
        GROUP BY plan_id) e ON e.plan_id = a.id
             LEFT JOIN (
        #         出库
        SELECT sum(ps) ps, sum(qty) qty, planid FROM cp_billfh GROUP BY planId) f ON f.planId = a.id
             LEFT JOIN (
        #         出库调整
        SELECT sum(pairs) pairs, sum(qty) qty, plan_id FROM cp_adjust WHERE bill_type = '手动出库' GROUP BY plan_id) g
                       ON g.plan_id = a.id
    WHERE a.state = 1
      AND if(isnull(`markFinished`), 1 = 1, ifnull(a.mark_finished, 0) = `markFinished`)
      AND DATE(a.plan_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.id DESC;
END ;;
DELIMITER ;