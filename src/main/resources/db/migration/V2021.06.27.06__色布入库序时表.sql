DROP PROCEDURE IF EXISTS `p_dye_inbound_report`;
DELIMITER ;;
CREATE PROCEDURE `p_dye_inbound_report`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100))
BEGIN
    /**
      2021年6月27日 色布入库序时表
     */
    IF ((isnull(`clientName`) OR length(`clientName`) <= 0) OR `clientName` = '全部') THEN
        SELECT c.company_name     client_name,
               b.plan_code,
               b.product_name,
               b.material_specification,
               b.color_no,
               b.color,
               ifnull(a.pairs, 0) ps,
               ifnull(a.qty, 0)   qty,
               a.client_name      supplier,
               a.bill_date,
               a.remarks
        FROM cp_adjust a
                 LEFT JOIN dye_fabric_plan b ON a.plan_id = b.id
                 LEFT JOIN contact_company c ON b.contact_company_id = c.id
        WHERE a.bill_type IN ('手动入库', '盘点')
          AND DATE(a.bill_date) BETWEEN DATE(`start`) AND DATE(`end`);
    ELSE
        SELECT c.company_name     client_name,
               b.plan_code,
               b.product_name,
               b.material_specification,
               b.color_no,
               b.color,
               ifnull(a.pairs, 0) ps,
               ifnull(a.qty, 0)   qty,
               a.client_name      supplier,
               a.bill_date,
               a.remarks
        FROM cp_adjust a
                 LEFT JOIN dye_fabric_plan b ON a.plan_id = b.id
                 LEFT JOIN contact_company c ON b.contact_company_id = c.id
        WHERE a.bill_type IN ('手动入库', '盘点')
          AND c.company_name = `clientName`
          AND DATE(a.bill_date) BETWEEN DATE(`start`) AND DATE(`end`);
    END IF;
END;;
DELIMITER ;