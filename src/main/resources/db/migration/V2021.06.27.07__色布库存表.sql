DROP PROCEDURE IF EXISTS `p_dye_storage_report`;
DELIMITER ;;
CREATE PROCEDURE p_dye_storage_report(IN `clientName` VARCHAR(100), IN `value` INT)
BEGIN
    /**
      2021年6月27日 色布库存表
     */
    IF (ifnull(`value`, 0) = 1) THEN
        #         汇总
        SELECT rep.*
        FROM (SELECT b.company_name                                                        client_name,
                     sum(ifnull(c.pairs, 0) - ifnull(d.ps, 0) - ifnull(e.pairs, 0))        ps,
                     sum(round(ifnull(c.qty, 0) - ifnull(d.qty, 0) - ifnull(e.qty, 0), 1)) qty
              FROM dye_fabric_plan a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
                       LEFT JOIN (
                  #         入库
                  SELECT sum(pairs) pairs, sum(qty) qty, plan_id
                  FROM cp_adjust
                  WHERE bill_type IN ('手动入库', '盘点')
                  GROUP BY plan_id) c ON c.plan_id = a.id
                       LEFT JOIN (
                  #         出库
                  SELECT sum(ps) ps, sum(qty) qty, planid FROM cp_billfh WHERE planid = `planid` GROUP BY planId) d
                                 ON d.planId = a.id
                       LEFT JOIN (
                  #         出库调整
                  SELECT sum(pairs) pairs, sum(qty) qty, plan_id
                  FROM cp_adjust
                  WHERE bill_type = '手动出库'
                  GROUP BY plan_id) e ON e.plan_id = a.id
              WHERE a.state = 1
                AND if(ifnull(`clientName`, '') IN ('', '全部'), 1 = 1, b.company_name = ifnull(`clientName`, ''))
              GROUP BY a.contact_company_id) rep
        WHERE abs(ifnull(rep.ps, 0)) > 0;
    ELSE
        #         明细
        SELECT b.company_name                                                   client_name,
               a.plan_code,
               a.product_name,
               a.material_specification,
               a.color_no,
               a.color,
               ifnull(c.pairs, 0) - ifnull(d.ps, 0) - ifnull(e.pairs, 0)        ps,
               round(ifnull(c.qty, 0) - ifnull(d.qty, 0) - ifnull(e.qty, 0), 1) qty
        FROM dye_fabric_plan a
                 LEFT JOIN contact_company b ON a.contact_company_id = b.id
                 LEFT JOIN (
            #         入库
            SELECT sum(pairs) pairs, sum(qty) qty, plan_id
            FROM cp_adjust
            WHERE bill_type IN ('手动入库', '盘点')
            GROUP BY plan_id) c ON c.plan_id = a.id
                 LEFT JOIN (
            #         出库
            SELECT sum(ps) ps, sum(qty) qty, planid FROM cp_billfh WHERE planid = `planid` GROUP BY planId) d
                           ON d.planId = a.id
                 LEFT JOIN (
            #         出库调整
            SELECT sum(pairs) pairs, sum(qty) qty, plan_id FROM cp_adjust WHERE bill_type = '手动出库' GROUP BY plan_id) e
                           ON e.plan_id = a.id
        WHERE a.state = 1
          AND abs(ifnull(c.pairs, 0) - ifnull(d.ps, 0) - ifnull(e.pairs, 0)) > 0
          AND if(ifnull(`clientName`, '') IN ('', '全部'), 1 = 1, b.company_name = ifnull(`clientName`, ''));
    END IF;
END ;;
DELIMITER ;