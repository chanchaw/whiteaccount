SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `wx_getworder`;
delimiter ;;
CREATE PROCEDURE `wx_getworder`(m_clientname varchar(100))
BEGIN

#微信端 已排计划
select p.* 
from 
(
		select a.id,b.company_name as clientname,a.plan_code,
		DATE_FORMAT(plan_date,'%Y-%m-%d') as plan_date,
		a.greyfabric_price as price, #单价
		CONCAT_WS(' ',a.product_name,CONCAT_WS('*',a.greyfabric_width,a.greyfabric_weight),IF(a.m_height>0,a.m_height,''),a.greyfabric_specification) as pmgg,


		round(a.plan_kilo,1) as planqty,
		c.rkps,c.rkqty,
		d.fhps,d.fhqty,
		e.pdps,e.pdqty,
		ifnull(c.rkps,0)-ifnull(d.fhps,0)+ifnull(e.pdps,0) as kcps,
		ifnull(c.rkqty,0)-ifnull(d.fhqty,0)+ifnull(e.pdqty,0) as kcqty,
		
		d.fhmoney,

		IF(ifnull(a.mark_finished,0)=0,2,1) as finished  #已完工的等于1  未完工等于2  用来倒排序  未完工在前面

		from plan a
		left outer join contact_company b
		on a.contact_company_id=b.id

		left outer join 
		(
		#入库数据
			
			select p.planid,sum(p.rkps)as rkps,sum(p.rkqty)as rkqty
			from 
			(
				select a.planid,sum(a.ps)as rkps,sum(a.qty)as rkqty
				from jrkbillsum a
				left outer join plan b
				on a.planid=b.id
				where  ifnull(b.state,0)=1
				GROUP BY a.planid
											
				union all
											
				select plan_id as planid,sum(pairs)as rkps,sum(qty) as rkqty
				from jrkbill_adjust
				where  bill_type='手动入库'
				group by plan_id
			)p
			GROUP BY p.planid	

		) c on a.id=c.planid

		left outer join 
		(
		#发货数据
		 select p.id,sum(p.fhps)as fhps,sum(p.fhqty)as fhqty,sum(p.fhmoney)as fhmoney
		 from 
		 (
				select c.id,
				b.ps as fhps,
				round(b.qty,1)as fhqty,
				a.price*b.qty as fhmoney
				
				from whitebillfh a
				left outer join whitebilldetailfh b
				on a.id=b.fid
				left outer join plan c
				on b.planid=c.id
				where ifnull(c.state,0)=1
				GROUP BY c.id
					
				union all
				
				select plan_id as id,pairs as fhps,qty as fhqty,price*qty as fhmoney
				from jrkbill_adjust
				where  bill_type='手动出库'
			
			)p GROUP BY p.id		
			
		) d on a.id=d.id

		left outer join 
		(
		#盘点
					select plan_id as planid,sum(ifnull(pairs,0)) as pdps,sum(ifnull(qty,0))as pdqty
					from jrkbill_adjust
					where  bill_type='盘点'
					GROUP BY plan_id
		)e on a.id=e.planid

		where  ifnull(a.state,0)=1
					and b.company_name=m_clientname
)p					
ORDER BY p.finished,p.plan_date DESC;
  

end
;;
delimiter ;

DROP PROCEDURE IF EXISTS `wx_getwplan`;
delimiter ;;
CREATE PROCEDURE `wx_getwplan`()
BEGIN

#微信端 已排计划

select a.id,b.company_name as clientname,a.plan_code,
DATE_FORMAT(plan_date,'%Y-%m-%d') as plan_date,
a.greyfabric_price as price, #单价
CONCAT_WS(' ',a.product_name,CONCAT_WS('*',a.greyfabric_width,a.greyfabric_weight),IF(a.m_height>0,a.m_height,''),a.greyfabric_specification) as pmgg,


round(a.plan_kilo,1) as planqty,
c.rkps,c.rkqty,
d.fhps,d.fhqty,
e.pdps,e.pdqty,
ifnull(c.rkps,0)-ifnull(d.fhps,0)+ifnull(e.pdps,0) as kcps,
ifnull(c.rkqty,0)-ifnull(d.fhqty,0)+ifnull(e.pdqty,0) as kcqty

from plan a
left outer join contact_company b
on a.contact_company_id=b.id

left outer join 
(
#入库数据
	
  select p.planid,sum(p.rkps)as rkps,sum(p.rkqty)as rkqty
	from 
	(
		select a.planid,sum(a.ps)as rkps,sum(a.qty)as rkqty
		from jrkbillsum a
		left outer join plan b
		on a.planid=b.id
		where ifnull(b.mark_finished,0)=0
		and ifnull(b.state,0)=1
		GROUP BY a.planid
									
		union all
									
		select plan_id as planid,sum(pairs)as rkps,sum(qty) as rkqty
		from jrkbill_adjust
		where  bill_type='手动入库'
		group by plan_id
	)p
	GROUP BY p.planid	

) c on a.id=c.planid

left outer join 
(
#发货数据
 select p.id,sum(p.fhps)as fhps,sum(p.fhqty)as fhqty
 from 
 (
		select c.id,
		b.ps as fhps,
		round(b.qty,1)as fhqty
		
		from whitebillfh a
		left outer join whitebilldetailfh b
		on a.id=b.fid
		left outer join plan c
		on b.planid=c.id
		where ifnull(c.mark_finished,0)=0
			and ifnull(c.state,0)=1
		GROUP BY c.id
			
		union all
		
		select plan_id as id,pairs as fhps,qty as fhqty
		from jrkbill_adjust
		where  bill_type='手动出库'
  
	)p GROUP BY p.id		
	
) d on a.id=d.id

left outer join 
(
#盘点
      select plan_id as planid,sum(ifnull(pairs,0)) as pdps,sum(ifnull(qty,0))as pdqty
			from jrkbill_adjust
			where  bill_type='盘点'
			GROUP BY plan_id
)e on a.id=e.planid

where ifnull(a.mark_finished,0)=0
			and ifnull(a.state,0)=1;


end
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
