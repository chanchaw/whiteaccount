/**
  2021年6月29日 代发客户
 */
create table delivery_company
(
    id int auto_increment comment '自增主键',
    company_name VARCHAR(100) null,
    company_address VARCHAR(100) null comment '地址',
    remarks VARCHAR(200) null,
    constraint delivery_company_pk
        primary key (id)
)
    comment '代发客户';

create unique index delivery_company_company_name_uindex
    on delivery_company (company_name);