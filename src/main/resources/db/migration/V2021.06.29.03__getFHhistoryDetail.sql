SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `getFHhistoryDetail`;
delimiter ;;
CREATE PROCEDURE `getFHhistoryDetail`(m_SDate varchar(100),
m_EDate varchar(100),
m_Client varchar(100),
m_CheckType varchar(100))
BEGIN


set @m_Date=CONCAT(" and DATE_FORMAT(a.datafh,'%Y-%m-%d') BETWEEN '",m_SDate,"' and '",m_EDate,"'");
set @m_Client=IF(m_Client="全部",' ',CONCAT(' and a.client=',"'",m_Client,"'"));
set @m_CheckType=IF(m_CheckType="全部",'  ',CONCAT(' and a.checkout=',"'",m_CheckType,"'"));




SET @SQL="select a.id,a.client,a.address,
CONCAT_WS(' ',c.order_code,c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as pmgg,
b.ps,
round(b.qty,1)as qty,
round(a.price,2)as price,
ROUND(round(a.price,2) * round(b.qty,1),2) as money,
a.checkout,
a.billcode,
DATE_FORMAT(a.datafh,'%m-%d %H:%i') as b_data

from whitebillfh a
left outer join whitebilldetailfh b
on a.id=b.fid
left outer join plan c
on b.planid=c.id 
where 1=1 ";


SET @SQL = CONCAT(@SQL,@m_Date,@m_Client,@m_CheckType, " ORDER BY a.id DESC");
		

#SET @SQL = CONCAT(@SQL,);

#select  @SQL;

prepare stmt from @SQL;
execute stmt ; 
deallocate prepare stmt; 

END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
