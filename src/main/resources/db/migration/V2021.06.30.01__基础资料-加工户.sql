/**
    2021年6月30日 基础资料-加工户
 */
create table process_company
(
    id int auto_increment comment '自增主键',
    company_name VARCHAR(200) null comment '加工户名称',
    constraint process_company_pk
        primary key (id)
)
    comment '加工户';

create unique index process_company_company_name_uindex
    on process_company (company_name);