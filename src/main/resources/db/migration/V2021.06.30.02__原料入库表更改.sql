/**
  2021年6月30日 原料入库表更改
 */
alter table material_order modify account_type varchar(50) null comment '付款类型(现金,欠账)';

alter table material_order
    add input_kilo DECIMAL(30,1) null comment '入库重量' after input_amount;