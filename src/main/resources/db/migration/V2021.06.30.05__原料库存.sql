DROP PROCEDURE IF EXISTS `p_material_storage`;
DELIMITER ;;
CREATE PROCEDURE `p_material_storage`()
BEGIN
    /**
      2021年6月30日 原料库存
     */
    SELECT a.material_spec, a.batch_num, sum(b.access_mode * ifnull(a.input_kilo, 0)) input_kilo
    FROM material_order a
             LEFT JOIN bill_type b ON a.bill_type = b.id
             LEFT JOIN supplier c ON a.supplier_id = c.id
    GROUP BY a.supplier_id, a.material_spec, a.batch_num;
END ;;