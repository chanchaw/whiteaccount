/**
  2021年6月30日 原料单据追加加工户与经手人
 */
alter table material_order
    add process_company VARCHAR(100) null comment '加工户' after remarks;

alter table material_order
    add handler_name VARCHAR(100) null comment '经手人' after process_company;