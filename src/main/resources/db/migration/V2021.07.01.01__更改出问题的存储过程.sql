SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_greyfabric_input
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_greyfabric_input`;
DELIMITER ;;
CREATE PROCEDURE `p_greyfabric_input`(IN `start` DATE, IN `end` DATE)
BEGIN
    /**
      2021年6月9日 白坯入库明细
     */
    SELECT rep.*
    FROM (SELECT any_value(a.id)                                                             id,
                 c.company_name                                                              client,
                 b.plan_code                                                                 planCode,
                 any_value(a.cn)                                                             computerName,
                 CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.m_height, b.greyfabric_specification, b.material_specification) specification,
                 ifnull(sum(a.ps), 0)                                                        ps,
                 ifnull(sum(a.qty), 0)                                                       qty,
                 a.createdate,
                 any_value(a.class_name)                                                     className,
                 any_value(a.jth)                                                            machineType,
                 a.aorb                                                                      aorb
          FROM jrkbillsum a
                   LEFT JOIN plan b ON a.planid = b.id
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
          WHERE DATE(a.createdate) BETWEEN DATE(`start`) AND DATE(`end`)
          GROUP BY a.createdate, a.aorb

          UNION ALL

          SELECT a.id,
                 c.company_name                                                              client,
                 b.plan_code                                                                 planCode,
                 a.bill_type                                                                 computerName,
                 CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.m_height, b.greyfabric_specification, b.material_specification) specification,
                 ifnull(a.pairs, 0)                                                          ps,
                 ifnull(a.qty, 0)                                                            qty,
                 a.jrkbill_date                                                              createdate,
                 NULL                                                                        className,
                 NULL                                                                        machineType,
                 NULL                                                                        aorb
          FROM jrkbill_adjust a
                   LEFT JOIN plan b ON a.plan_id = b.id
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
          WHERE bill_type = '手动入库'
            AND DATE(jrkbill_date) BETWEEN DATE(`start`) AND DATE(`end`)) rep
    ORDER BY rep.createdate DESC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_jrkbill_dtl
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_jrkbill_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_jrkbill_dtl`(IN `id` BIGINT)
BEGIN
    /**
       2021年5月22日 打卷入库明细
       2021年6月6日  拼接单据类型到机台号
     */
    SELECT *
    FROM ((SELECT any_value(b.id)                          id,
                  ifnull(sum(b.ps), 0)                     input_pairs,
                  ifnull(sum(round(b.qty, 1)), 0)          input_kilo,
                  any_value(b.aorb)                        aorb,
                  any_value(a.greyfabric_specification)    greyfabric_specification,
                  any_value(a.material_specification)      material_specification,
                  any_value(b.cn)                          computer_name,
                  any_value(b.jth)                         machine_type,
                  date_format(b.createdate, '%m-%d %H:%i') createdate,
                  NULL                                     remarks,
                  '入库'                                     bill_type,
                  b.createdate                             order_date
           FROM plan a
                    LEFT JOIN jrkbillsum b ON a.id = b.planid
           WHERE a.id = `id`
             AND b.id IS NOT NULL
           GROUP BY b.createdate)
          UNION ALL
          (SELECT b.id                                       id,
                  ifnull(b.pairs, 0)                         input_pairs,
                  ifnull(round(b.qty, 1), 0)                 input_kilo,
                  NULL                                       aorb,
                  a.greyfabric_specification                 greyfabric_specification,
                  a.material_specification                   material_specification,
                  b.bill_type                                computer_name,
                  NULL                                       machine_type,
                  date_format(b.jrkbill_date, '%m-%d %H:%i') createdate,
                  b.remarks                                  remarks,
                  b.bill_type                                bill_type,
                  b.jrkbill_date                             order_date
           FROM plan a
                    LEFT JOIN jrkbill_adjust b ON a.id = b.plan_id
           WHERE a.id = `id`
             AND b.bill_type = '手动入库'
             AND b.id IS NOT NULL)) r
    ORDER BY r.order_date DESC;

END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
