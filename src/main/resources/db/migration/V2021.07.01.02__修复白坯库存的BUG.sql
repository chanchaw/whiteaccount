SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `p_greyfabric_storage`;
delimiter ;;
CREATE PROCEDURE `p_greyfabric_storage`(IN mode INT)
BEGIN
    /**
     2021年6月9日 白坯库存
     */
    SET @baseSql = if(mode=1, concat("
    select re.* from (
    SELECT rep.clientName client,
       -- rep.planCode,
       -- rep.productName,
       sum(rep.ps) ps,
       sum(rep.qty) qty,
       -- avg(rep.price) price,
       sum(rep.money) money
       -- rep.specification
FROM ((SELECT c.company_name                                                              clientName,
              b.plan_code                                                                 planCode,
              b.product_name                                                              productName,
              ifnull(a.ps, 0)                                                             ps,
              ifnull(a.qty, 0)                                                            qty,
              ifnull(b.greyfabric_price, 0)                                               price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)                  money,
              CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification) specification
       FROM jrkbillsum a
                LEFT JOIN plan b ON a.planid = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

      UNION ALL

      #   收款调整
      (SELECT c.company_name                                                                    clientName,
              b.plan_code                                                                       planCode,
              b.product_name                                                                    productName,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)                              ps,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)                                qty,
              ifnull(a.price, 0)                                                                price,
              round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2) money,
              CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification)       specification
       FROM jrkbill_adjust a
                LEFT JOIN plan b ON a.plan_id = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)
        union all
        # 发货管理
        (SELECT a.client                                                       clientName,
               c.plan_code                                                     planCode,
               c.product_name                                                  productName,
               -ifnull(b.ps, 0)                                                ps,
               -ifnull(b.qty, 0)                                               qty,
               ifnull(a.price, 0)                                              price,
               round(ifnull(a.price, 0) * -ifnull(b.qty, 0), 2)                 money,
               CONCAT_WS(' ', c.order_code, c.product_name, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                         c.greyfabric_specification, c.material_specification) specification
        FROM whitebillfh a
                 LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON b.planid = c.id)
                ) rep
        left join contact_company co on rep.clientName = co.company_name
GROUP BY rep.clientName order by co.id ) re where abs(ifnull(re.ps,0))>0; "),
                      concat("
    select re.* from (
    SELECT any_value(rep.clientName) client,
       rep.planCode,
       any_value(rep.productName) productName,
       sum(rep.ps) ps,
       sum(rep.qty) qty,
       avg(rep.price) price,
       sum(rep.money) money,
       any_value(rep.specification) specification
FROM ((SELECT c.company_name                                                              clientName,
              b.plan_code                                                                 planCode,
              b.product_name                                                              productName,
              ifnull(a.ps, 0)                                                             ps,
              ifnull(a.qty, 0)                                                            qty,
              ifnull(b.greyfabric_price, 0)                                               price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)                  money,
              CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification) specification
       FROM jrkbillsum a
                LEFT JOIN plan b ON a.planid = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

      UNION ALL

      #   收款调整
      (SELECT c.company_name                                                                    clientName,
              b.plan_code                                                                       planCode,
              b.product_name                                                                    productName,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)                              ps,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)                                qty,
              ifnull(a.price, 0)                                                                price,
              round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2) money,
              CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification)       specification
       FROM jrkbill_adjust a
                LEFT JOIN plan b ON a.plan_id = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)
       union all
       # 发货管理
        (SELECT a.client                                                        clientName,
               c.plan_code                                                     planCode,
               c.product_name                                                  productName,
               -ifnull(b.ps, 0)                                                ps,
               -ifnull(b.qty, 0)                                               qty,
               ifnull(a.price, 0)                                              price,
               round(ifnull(a.price, 0) * -ifnull(b.qty, 0), 2)                 money,
               CONCAT_WS(' ', c.order_code, c.product_name, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                         c.greyfabric_specification, c.material_specification) specification
        FROM whitebillfh a
                 LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON b.planid = c.id)
                ) rep
        left join contact_company co on rep.clientName = co.company_name
GROUP BY rep.planCode order by co.id ) re where abs(ifnull(re.ps,0))>0; ")
        );
    PREPARE stat FROM @baseSql;
    EXECUTE stat ;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
