DROP PROCEDURE IF EXISTS `getReceivablesDetail`;
DELIMITER ;;
CREATE PROCEDURE `getReceivablesDetail`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100), IN `isGather` INT)
BEGIN
    /**
        2021年7月1日  白坯应收款
     */
    IF (`isGather` = 1) THEN
        #      汇总
        SELECT rep.client client, round(sum(rep.money), 2) money
        FROM (SELECT a.client, round(sum(a.price * b.qty), 2) money
              FROM whitebillfh a
                       LEFT JOIN whitebilldetailfh b ON a.id = b.fid
              WHERE a.checkout <> '现金'
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, a.client = `clientName`)
              GROUP BY a.client
              UNION ALL
              SELECT client, round(sum(-money), 2) money
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
              GROUP BY client) rep
        GROUP BY rep.client;
    ELSE
        #         明细
        SELECT rep.*
        FROM (SELECT `start`                                                                                  billdate,
                     if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', '全部', `clientName`) client,
                     '期初结余'                                                                                   billtype,
                     0                                                                                        qty,
                     0                                                                                        ps,
                     0                                                                                        price,
                     sum(a.money)                                                                             money,
                     ''                                                                                       collectiontype,
                     ''                                                                                       billmemo,
                     0                                                                                        SKid
              FROM (SELECT a.client, round(sum(a.price * b.qty), 2) money
                    FROM whitebillfh a
                             LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                    WHERE DATE(a.datafh) < DATE(`start`)
                      AND a.checkout <> '现金'
                      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                             a.client = `clientName`)
                    GROUP BY a.client
                    UNION ALL
                    SELECT client, round(sum(-money), 2) money
                    FROM receivables
                    WHERE DATE(billdate) < DATE(`start`)
                      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                             client = `clientName`)
                    GROUP BY client) a
              UNION ALL
              #      发货明细
              SELECT DATE(a.datafh)                                                          billdate,
                     a.client                                                                client,
                     CONCAT_WS(' ', c.order_code, c.product_name,
                               CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                               c.greyfabric_specification, a.aorb, c.material_specification) billtype,
                     ifnull(b.qty, 0)                                                        qty,
                     ifnull(b.ps, 0)                                                         ps,
                     ifnull(a.price, 0)                                                      price,
                     ROUND(b.qty * ifnull(a.price, 0), 2)                                    money,
                     a.checkout                                                              collectiontype,
                     a.memo                                                                  billmemo,
                     0                                                                       SKid
              FROM whitebillfh a
                       LEFT OUTER JOIN whitebilldetailfh b ON a.id = b.fid
                       LEFT OUTER JOIN plan c ON b.planid = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
                AND a.checkout <> '现金'
                AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`)
              UNION ALL
              #     收款明细
              SELECT DATE_FORMAT(billdate, '%Y-%m-%d') billdate,
                     client                            client,
                     billtype                          billtype,
                     0                                 qty,
                     0                                 ps,
                     0                                 price,
                     -ifnull(money, 0)                 money,
                     collectiontype                    collectiontype,
                     billmemo                          billmemo,
                     id                                SKid
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
                AND DATE(billdate) BETWEEN DATE(`start`) AND DATE(`end`)) rep
        ORDER BY rep.billdate;
    END IF;
END ;;
DELIMITER ;