SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP FUNCTION IF EXISTS `f_material_codegen`;
DELIMITER ;;
CREATE FUNCTION `f_material_codegen`(prefix VARCHAR(50)) RETURNS VARCHAR(100) CHARSET utf8
BEGIN
    DECLARE currentDate VARCHAR(50); -- 日期
    DECLARE curNum BIGINT; -- 当前记数
    DECLARE result VARCHAR(100); -- 结果
    DECLARE curNumCount INT; -- 记数位数
    DECLARE currentDateStart INT; -- 日期截取起始位置
    DECLARE currentDateLen INT; -- 日期截取长度
    SELECT ifnull(config_value, 4) INTO curNumCount FROM sys_config_int WHERE config_field = '原料编号记数位数';
    SELECT ifnull(config_value, 3) INTO currentDateStart FROM sys_config_int WHERE config_field = '原料编号日期截取起始位置';
    SELECT ifnull(config_value, 4) INTO currentDateLen FROM sys_config_int WHERE config_field = '原料编号日期截取长度';
    IF ifnull(curNumCount, 0) = 0 THEN SET curNumCount = 4; END IF;
    IF ifnull(currentDateStart, 0) = 0 THEN SET currentDateStart = 3; END IF;
    IF ifnull(currentDateLen, 0) = 0 THEN SET currentDateLen = 2; END IF;
    SELECT substr(concat(DATE_FORMAT(NOW(), '%Y'), DATE_FORMAT(NOW(), '%m'), DATE_FORMAT(NOW(), '%d')),
                  currentDateStart, currentDateLen)
    INTO currentDate;

    -- 创建临时表
    DROP TEMPORARY TABLE IF EXISTS t_num,t_num_tmp;
    CREATE TEMPORARY TABLE t_num
    (
        pk_id       BIGINT AUTO_INCREMENT PRIMARY KEY,
        count_value BIGINT
    );
    CREATE TEMPORARY TABLE t_num_tmp
    (
        pk_id       BIGINT AUTO_INCREMENT PRIMARY KEY,
        count_value BIGINT
    );
    INSERT INTO t_num (count_value)
    SELECT cast(substring(order_code, length(`prefix`) + length(`currentDate`) + 1, curNumCount) AS DECIMAL(24)) num
    FROM material_order
    WHERE order_code LIKE concat(`prefix`, `currentDate`, '%');

    INSERT INTO t_num_tmp (count_value)
    SELECT cast(substring(order_code, length(`prefix`) + length(`currentDate`) + 1, curNumCount) AS DECIMAL(24)) num
    FROM material_order
    WHERE order_code LIKE concat(`prefix`, `currentDate`, '%');

    SELECT ifnull(a.count_value + 1, 1)
    INTO `curNum`
    FROM t_num a
    WHERE a.count_value + 1 NOT IN (SELECT count_value FROM t_num_tmp)
    ORDER BY a.pk_id
    LIMIT 1;

    SELECT concat(`prefix`, substring(currentDate, 1), lpad(ifnull(curNum, 1), curNumCount, '0')) INTO result;
    RETURN result;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_material_order_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_material_order_report`;
DELIMITER ;;
CREATE PROCEDURE `p_material_order_report`(IN `start` DATE, IN `end` DATE)
BEGIN
    /**
      2021年5月25日  原料入库序时表
     */
    SELECT a.id,                                     -- 主键
           DATE(a.order_date)      order_date,       -- 制单日期
           a.order_code            order_code,       -- 单据编号
           a.account_type,                           -- 付款方式
           b.supplier_name,                          -- 供应商名称
           a.material_spec,                          -- 原料规格
           a.batch_num,                              -- 批次号
           a.input_money,                            -- 入库金额
           ifnull(a.box_amount, 0) box_amount,       -- 件数
           a.input_kilo,                             -- 入库重量
           a.price,                                  -- 单价
           a.input_money,                            -- 金额
           a.remarks,                                -- 备注
           a.supplier_id,                            -- 供应商编号
           c.user_name             create_user_name, -- 创建人名称
           a.create_time,                            -- 创建时间
           a.account_type,                           -- 类型
           d.bill_type_name                          -- 单据类型名称
    FROM material_order a
             LEFT JOIN supplier b ON a.supplier_id = b.id
             LEFT JOIN sys_user c ON a.create_user_id = c.id
             LEFT JOIN bill_type d ON a.bill_type = d.id
    WHERE a.state = 1
      AND d.access_mode > 0
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.id DESC;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;