SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `sys_config_int`;
CREATE TABLE `sys_config_int`  (
  `config_field` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '属性名',
  `config_value` int(11) NULL DEFAULT NULL COMMENT '属性值',
  `config_desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性描述',
  PRIMARY KEY (`config_field`) USING BTREE,
  UNIQUE INDEX `sys_config_int_config_field_uindex`(`config_field`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '配置表';

BEGIN;
INSERT INTO `sys_config_int` VALUES ('原料编号日期截取起始位置', 3, '最小值1'), ('原料编号日期截取长度', 4, '最大值8'), ('原料编号记数位数', 4, '原料编号记数位数'), ('计划单号日期截取起始位置', 3, '最小值1'), ('计划单号日期截取长度', 2, '最大值8'), ('计划单号记数位数', 4, '原料编号记数位数'), ('计划单完工按钮校验是否开启', 1, '1:开启;0:关闭;关闭后完工计划不会校验是否有库存');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
