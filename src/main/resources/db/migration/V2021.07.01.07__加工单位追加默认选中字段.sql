/**
  加工单位追加默认选中字段
 */
alter table process_company
    add is_selected TINYINT(1) default 0 null comment '是否默认选中(0:默认不选;1:默认选中;)';