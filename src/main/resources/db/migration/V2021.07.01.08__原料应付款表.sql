/**
  2021年7月1日 原料应付款表
 */
create table material_account_payable
(
    id int auto_increment comment '自增主键',
    bill_date DATETIME null comment '单据日期',
    billtype VARCHAR(100) null comment '单据类型',
    client_name VARCHAR(100) not null comment '客户名称',
    price DECIMAL(30,2) null comment '单价',
    money DECIMAL(30,2) null comment '金额',
    payable_type VARCHAR(100) null comment '类型(预付)',
    box_amount INT null comment '件数',
    billmemo VARCHAR(255) null comment '备注',
    upload INT default 0 null comment '是否已经上传(0:未上传;1:已上传;)',
    is_push TINYINT(1) default 0 null comment '是否推送字段(0:未推送;1:已推送;)',
    constraint material_account_payable_pk
        primary key (id)
)
    comment '原料应付款';

create index material_account_payable_client_name_index
    on material_account_payable (client_name);

create index material_account_payable_payable_type_index
    on material_account_payable (payable_type);