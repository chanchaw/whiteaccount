DROP PROCEDURE IF EXISTS `p_account_payment`;
DELIMITER ;;
CREATE PROCEDURE `p_account_payment`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100), IN `isGather` INT)
BEGIN
    /**
      2021年7月2日 已付款存储过程
     */
    IF (`isGather` = 1) THEN
        #             汇总
        SELECT client_name, sum(money) money
        FROM material_account_payable
        WHERE if(ifnull(`client_name`, '') = '' OR ifnull(`client_name`, '') = '全部', 1 = 1, client_name = `client_name`)
          AND DATE(bill_date) BETWEEN DATE(`start`) AND DATE(`end`)
        GROUP BY client_name;
    ELSE
        #             明细
        SELECT client_name,
               billtype,
               DATE(bill_date)       bill_date,
               billmemo,
               payable_type,
               ifnull(money, 0)      money,
               ifnull(box_amount, 0) box_amount
        FROM material_account_payable
        WHERE if(ifnull(`client_name`, '') = '' OR ifnull(`client_name`, '') = '全部', 1 = 1, client_name = `client_name`)
          AND DATE(bill_date) BETWEEN DATE(`start`) AND DATE(`end`);
    END IF;
END ;;
DELIMITER ;