/**
  追加单据类型默认选中标志
 */
alter table bill_type
    add is_selected TINYINT(1) default 0 null comment '是否默认选中';