create table product_outward_order
(
    id BIGINT auto_increment comment '自增主键',
    order_date DATE null comment '单据日期',
    order_code VARCHAR(100) null comment '单据编号',
    process_company VARCHAR(100) null comment '加工户',
    batch_num VARCHAR(100) null comment '批次号',
    supplier_name VARCHAR(100) null comment '供应商',
    plan_code VARCHAR(100) not null comment '外键,对应白坯计划计划单号',
    material_specification VARCHAR(100) null comment '品名规格',
    box_amount DECIMAL(30) null comment '件数',
    input_pairs DECIMAL(30,1) null comment '匹数',
    input_kilo DECIMAL(30,1) null comment '重量',
    processing_cost DECIMAL(60,2) null comment '加工费',
    money DECIMAL(60,2) null comment '金额',
    handler_name VARCHAR(60) null comment '经手人',
    create_user_id int null comment '外键,制单人,对应sys_user.id',
    remarks VARCHAR(255) null comment '备注',
    create_time DATETIME default current_timestamp null comment '创建时间',
    is_finished_product TINYINT(1) DEFAULT 0 NULL COMMENT '是否成品(0:半成品;1:卷布;)',
    state INT default 0 null comment '状态(0:作废;1:正常;)',
    constraint product_outward_order_pk
        primary key (id)
)
    comment '外发入库单据';

create unique index product_outward_order_order_code_uindex
    on product_outward_order (order_code);

