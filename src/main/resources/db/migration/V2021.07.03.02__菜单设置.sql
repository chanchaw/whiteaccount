/*
    更改菜单表
*/
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE `sys_menu`;
INSERT INTO `sys_menu`
VALUES (100, NULL, '计划管理', '/', 'eject-aside-icon-gur-project-08'),
       (101, 100, '白坯计划', 'jh_plan_report', ''),
       (102, 100, '成品计划', 'jh_dye_fabric_plan_report', ''),
       (200, NULL, '发货管理', '/', 'eject-aside-icon-gur-project-01'),
       (201, 200, '白坯发货', 'fh_delivery_report', ''),
       (202, 200, '成品发货', 'fh_dye_delivery_report', ''),
       (300, NULL, '收货管理', '/', 'eject-aside-icon-gur-project-36'),
       (301, 300, '原料收货', 'yf_material_order_report', ''),
       (400, NULL, '基础资料', '/', 'eject-aside-icon-gur-project-34'),
       (401, 400, '客户', 'jc_contactcompany', ''),
       (402, 400, '供应商', 'jc_supplier', NULL),
       (403, 400, '品名', 'jc_product_info', ''),
       (404, 400, '原料规格', 'jc_specification', NULL),
       (405, 400, '白坯克重', 'jc_weight_of_fabric', NULL),
       (406, 400, '白坯门幅', 'jc_width_of_fabric', ''),
       (407, 400, '机台号', 'jc_machine_type', ''),
       (408, 400, '部门员工', 'jc_employee', ''),
       (409, 400, '计划模板', 'jc_plan_template', ''),
       (410, 400, '收货单位', 'jc_take_delivery', ''),
       (411, 400, '单据类型', 'jc_bill_type', ''),
       (412, 400, '头份', 'jc_first_copies', ''),
       (413, 400, '加工类型', 'jc_process_type', ''),
       (414, 400, '色布计划模板', 'jc_dye_fabric_plan_template', ''),
       (415, 400, '色布品名', 'jc_dye_product_info', ''),
       (416, 400, '色布规格', 'jc_dye_specification', ''),
       (417, 400, '代发客户', 'jc_delivery_company', ''),
       (418, 400, '加工户', 'jc_process_company', ''),
       (500, NULL, '系统管理', '/', 'eject-aside-icon-gur-project-31'),
       (501, 500, '权限表', 'jc_permission', NULL),
       (502, 500, '用户表', 'jc_user', NULL),
       (503, 500, '角色组', 'sz_role_group', NULL),
       (504, 500, '配置信息', 'sz_config', NULL),
       (505, 500, '重置账套', 'sz_reset_account', ''),
       (600, NULL, '外发管理', '', 'eject-aside-icon-gur-project-35'),
       (601, 600, '原料外发', 'wf_material_send_report', ''),
       (700, NULL, '财务管理', '', 'eject-aside-icon-gur-project-18'),
       (701, 700, '应收账款', 'ysyf_receivable_report', ''),
       (702, 700, '应付账款', 'ysyf_payable_report', ''),
       (703, 700, '外发对账', 'ysyf_outward_report', '');

TRUNCATE TABLE sys_permission;
INSERT INTO sys_permission (sys_menu_id, sys_role_id)
SELECT m.id, b.id
FROM sys_menu m
         JOIN sys_role b;
SET FOREIGN_KEY_CHECKS = 1;