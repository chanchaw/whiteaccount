/**
  2021年7月3日 白坯发货主表追加字段
 */
#     追加表字段
SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `whitebillfh` ADD COLUMN `planid` int(11) NULL DEFAULT NULL AFTER `id`;
ALTER TABLE `whitebillfh` ADD COLUMN `ps` int(11) NULL DEFAULT NULL AFTER `datafh`;
ALTER TABLE `whitebillfh` ADD COLUMN `qty` decimal(60, 1) NULL DEFAULT NULL AFTER `ps`;
ALTER TABLE `whitebillfh` ADD COLUMN `is_push` tinyint(1) NULL DEFAULT 0;
ALTER TABLE `whitebillfh` ADD COLUMN `signstr` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `carnumber`;
SET FOREIGN_KEY_CHECKS=1;

#     复制明细数据
SET FOREIGN_KEY_CHECKS = 0;
UPDATE whitebillfh a LEFT JOIN (SELECT fid, any_value(ps) ps, any_value(qty) qty, any_value(planid) planid
                                FROM whitebilldetailfh
                                GROUP BY fid) b ON a.id = b.fid
SET a.ps      = b.ps,
    a.qty     = b.qty,
    a.planid  = b.planid,
    a.is_push = 1
WHERE b.fid IS NOT NULL;
SET FOREIGN_KEY_CHECKS = 1;

#      删除明细表
SET FOREIGN_KEY_CHECKS =0;
DROP TABLE whitebilldetailfh;
SET FOREIGN_KEY_CHECKS =1;