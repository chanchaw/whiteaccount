SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for getCollectionOld
-- ----------------------------
DROP PROCEDURE IF EXISTS `getCollectionOld`;
delimiter ;;
CREATE PROCEDURE `getCollectionOld`(m_SDate VARCHAR(100),
                                    m_EDate VARCHAR(100),
                                    m_Client VARCHAR(100),
                                    m_Values VARCHAR(100))
BEGIN

    IF m_Values = 0 THEN

        IF m_Client = '全部' THEN

            SELECT id, DATE_FORMAT(billdate, '%Y-%m-%d') AS billdate, client, billtype, money, collectiontype, billmemo
            FROM receivables
            WHERE DATE_FORMAT(billdate, '%Y-%m-%d') BETWEEN m_SDate AND m_EDate
              AND billtype = '收款'
            UNION ALL
            SELECT a.id,
                   DATE(a.datafh)                       billdate,
                   a.client,
                   '发货'                                 billtype,
                   round(ifnull(a.price, 0) * a.qty, 2) money,
                   a.checkout                           collectiontype,
                   a.memo                               billmemo
            FROM whitebillfh a
#                      LEFT JOIN whitebilldetailfh b ON a.id = b.fid
            WHERE a.checkout = '现金'
              AND DATE(a.datafh) BETWEEN DATE(`m_SDate`) AND DATE(`m_EDate`);

        ELSE

            SELECT id, DATE_FORMAT(billdate, '%Y-%m-%d') AS billdate, client, billtype, money, collectiontype, billmemo
            FROM receivables
            WHERE DATE_FORMAT(billdate, '%Y-%m-%d') BETWEEN m_SDate AND m_EDate
              AND billtype = '收款'
              AND client = m_Client
            UNION ALL
            SELECT a.id,
                   DATE(a.datafh)                       billdate,
                   a.client,
                   '发货'                                 billtype,
                   round(ifnull(a.price, 0) * a.qty, 2) money,
                   a.checkout                           collectiontype,
                   a.memo                               billmemo
            FROM whitebillfh a
#                      LEFT JOIN whitebilldetailfh b ON a.id = b.fid
            WHERE a.checkout = '现金'
              AND a.client = `m_Client`
              AND DATE(a.datafh) BETWEEN DATE(`m_SDate`) AND DATE(`m_EDate`);

        END IF;


    ELSEIF m_Values = 1 THEN

        IF m_Client = '全部' THEN

            SELECT 0          AS id,
                   NULL       AS billdate,
                   client,
                   NULL       AS billtype,
                   sum(money) AS money,
                   NULL       AS collectiontype,
                   NULL       AS billmemo
            FROM receivables
            WHERE DATE_FORMAT(billdate, '%Y-%m-%d') BETWEEN m_SDate AND m_EDate
              AND billtype = '收款'
            GROUP BY client
            UNION ALL
            SELECT a.id,
                   DATE(a.datafh)                 billdate,
                   a.client,
                   '发货'                           billtype,
                   sum(round(a.price * a.qty, 2)) money,
                   a.checkout                     collectiontype,
                   a.memo                         billmemo
            FROM whitebillfh a
#                      LEFT JOIN whitebilldetailfh b ON a.id = b.fid
            WHERE a.checkout = '现金'
              AND DATE(a.datafh) BETWEEN DATE(`m_SDate`) AND DATE(`m_EDate`)
            GROUP BY a.client;

        ELSE

            SELECT 0          AS id,
                   NULL       AS billdate,
                   client,
                   NULL       AS billtype,
                   sum(money) AS money,
                   NULL       AS collectiontype,
                   NULL       AS billmemo
            FROM receivables
            WHERE DATE_FORMAT(billdate, '%Y-%m-%d') BETWEEN m_SDate AND m_EDate
              AND billtype = '收款'
              AND client = m_Client
            GROUP BY client
            UNION ALL
            SELECT a.id,
                   DATE(a.datafh)                            billdate,
                   a.client,
                   '发货'                                      billtype,
                   sum(round(ifnull(a.price, 0) * a.qty, 2)) money,
                   a.checkout                                collectiontype,
                   a.memo                                    billmemo
            FROM whitebillfh a
#                      LEFT JOIN whitebilldetailfh b ON a.id = b.fid
            WHERE a.checkout = '现金'
              AND a.client = `m_Client`
              AND DATE(a.datafh) BETWEEN DATE(`m_SDate`) AND DATE(`m_EDate`)
            GROUP BY a.client;
        END IF;
    END IF;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getFHDetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `getFHDetail`;
delimiter ;;
CREATE PROCEDURE `getFHDetail`(m_fhid varchar(100))
BEGIN

select a.id,a.billcode,a.client,a.address,a.aorb,a.price,a.checkout,a.memo,a.manager,a.carnumber,
a.ps,a.qty,a.planid,
c.order_code,c.plan_code,c.material_specification,
CONCAT_WS(' ',c.order_code,product_name,CONCAT_WS('*',greyfabric_width,greyfabric_weight),IF(m_height>0,m_height,''),greyfabric_specification) as PMGG
from whitebillfh a
# left outer join whitebilldetailfh b on a.id=b.fid
left outer join plan c
on a.planid=c.id
where a.id=m_fhid
;

end
;;
delimiter ;

-- ----------------------------
-- Procedure structure for GetFHDetailNow
-- ----------------------------
DROP PROCEDURE IF EXISTS `GetFHDetailNow`;
delimiter ;;
CREATE PROCEDURE `GetFHDetailNow`()
BEGIN

select a.id,a.client,a.address,
CONCAT_WS(' ',c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as pmgg,
a.ps,
round(a.qty,1)as qty,
round(a.price,1)as price,
ROUND(a.price*a.qty,1) as money,
a.checkout,
DATE_FORMAT(a.datafh,'%m-%d %H:%i') as b_data

from whitebillfh a
# left outer join whitebilldetailfh b on a.id=b.fid
left outer join plan c
on a.planid=c.id

 where DATE_FORMAT(a.datafh,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d')
ORDER BY a.id DESC
;
 
 
end
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getFHhistoryDetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `getFHhistoryDetail`;
delimiter ;;
CREATE PROCEDURE `getFHhistoryDetail`(m_SDate varchar(100),
m_EDate varchar(100),
m_Client varchar(100),
m_CheckType varchar(100))
BEGIN


set @m_Date=CONCAT(" and DATE_FORMAT(a.datafh,'%Y-%m-%d') BETWEEN '",m_SDate,"' and '",m_EDate,"'");
set @m_Client=IF(m_Client="全部",' ',CONCAT(' and a.client=',"'",m_Client,"'"));
set @m_CheckType=IF(m_CheckType="全部",'  ',CONCAT(' and a.checkout=',"'",m_CheckType,"'"));




SET @SQL="select a.id,a.client,a.address,
CONCAT_WS(' ',c.order_code,c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as pmgg,
a.ps,
round(a.qty,1)as qty,
round(a.price,2)as price,
ROUND(round(a.price,2) * round(a.qty,1),2) as money,
a.checkout,
a.billcode,
DATE_FORMAT(a.datafh,'%m-%d %H:%i') as b_data

from whitebillfh a
# left outer join whitebilldetailfh b on a.id=b.fid
left outer join plan c on a.planid=c.id
where 1=1 ";


SET @SQL = CONCAT(@SQL,@m_Date,@m_Client,@m_CheckType, " ORDER BY a.id DESC");
		

#SET @SQL = CONCAT(@SQL,);

#select  @SQL;

prepare stmt from @SQL;
execute stmt ; 
deallocate prepare stmt; 

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getReceivablesAllSum
-- ----------------------------
DROP PROCEDURE IF EXISTS `getReceivablesAllSum`;
delimiter ;;
CREATE PROCEDURE `getReceivablesAllSum`()
BEGIN


select p.*,null as str1
from
(
	select m.client,ifnull(m.money,0)-ifnull(n.money,0) as money
	from
	(
	select a.client,ROUND(sum(a.qty*ifnull(a.price,0)),2)as money
	from whitebillfh a
# 	left outer join whitebilldetailfh b on a.id=b.fid
	GROUP BY a.client
	)m

	left outer join
	(
	select client,sum(money)as money
	from receivables
	GROUP BY client
	)n on m.client=n.client

)p where p.money>0;

end
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getReceivablesDetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `getReceivablesDetail`;
delimiter ;;
CREATE PROCEDURE `getReceivablesDetail`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100), IN `isGather` INT)
BEGIN
    /**
        2021年7月1日  白坯应收款
     */
    IF (`isGather` = 1) THEN
        #      汇总
        SELECT rep.client client, round(sum(rep.money), 2) money
        FROM (SELECT a.client, round(sum(a.price * a.qty), 2) money
              FROM whitebillfh a
#                        LEFT JOIN whitebilldetailfh b ON a.id = b.fid
              WHERE a.checkout <> '现金'
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, a.client = `clientName`)
              GROUP BY a.client
              UNION ALL
              SELECT client, round(sum(-money), 2) money
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
              GROUP BY client) rep
        GROUP BY rep.client;
    ELSE
        #         明细
        SELECT rep.*
        FROM (SELECT `start`                                                                                  billdate,
                     if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', '全部', `clientName`) client,
                     '期初结余'                                                                                   billtype,
                     0                                                                                        qty,
                     0                                                                                        ps,
                     0                                                                                        price,
                     sum(a.money)                                                                             money,
                     ''                                                                                       collectiontype,
                     ''                                                                                       billmemo,
                     0                                                                                        SKid
              FROM (SELECT a.client, round(sum(a.price * a.qty), 2) money
                    FROM whitebillfh a
#                              LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                    WHERE DATE(a.datafh) < DATE(`start`)
                      AND a.checkout <> '现金'
                      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                             a.client = `clientName`)
                    GROUP BY a.client
                    UNION ALL
                    SELECT client, round(sum(-money), 2) money
                    FROM receivables
                    WHERE DATE(billdate) < DATE(`start`)
                      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                             client = `clientName`)
                    GROUP BY client) a
              UNION ALL
              #      发货明细
              SELECT DATE(a.datafh)                                                          billdate,
                     a.client                                                                client,
                     CONCAT_WS(' ', c.order_code, c.product_name,
                               CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                               c.greyfabric_specification, a.aorb, c.material_specification) billtype,
                     ifnull(a.qty, 0)                                                        qty,
                     ifnull(a.ps, 0)                                                         ps,
                     ifnull(a.price, 0)                                                      price,
                     ROUND(a.qty * ifnull(a.price, 0), 2)                                    money,
                     a.checkout                                                              collectiontype,
                     a.memo                                                                  billmemo,
                     0                                                                       SKid
              FROM whitebillfh a
#                        LEFT OUTER JOIN whitebilldetailfh b ON a.id = b.fid
                       LEFT OUTER JOIN plan c ON a.planid = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
                AND a.checkout <> '现金'
                AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`)
              UNION ALL
              #     收款明细
              SELECT DATE_FORMAT(billdate, '%Y-%m-%d') billdate,
                     client                            client,
                     billtype                          billtype,
                     0                                 qty,
                     0                                 ps,
                     0                                 price,
                     -ifnull(money, 0)                 money,
                     collectiontype                    collectiontype,
                     billmemo                          billmemo,
                     id                                SKid
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
                AND DATE(billdate) BETWEEN DATE(`start`) AND DATE(`end`)) rep
        ORDER BY rep.billdate;
    END IF;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_greyfabric_storage
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_greyfabric_storage`;
delimiter ;;
CREATE PROCEDURE `p_greyfabric_storage`(IN mode INT)
BEGIN
    /**
     2021年6月9日 白坯库存
     */
    SET @baseSql = if(mode=1, concat("
    select re.* from (
    SELECT rep.clientName client,
       -- rep.planCode,
       -- rep.productName,
       sum(rep.ps) ps,
       sum(rep.qty) qty,
       -- avg(rep.price) price,
       sum(rep.money) money
       -- rep.specification
FROM ((SELECT c.company_name                                                              clientName,
              b.plan_code                                                                 planCode,
              b.product_name                                                              productName,
              ifnull(a.ps, 0)                                                             ps,
              ifnull(a.qty, 0)                                                            qty,
              ifnull(b.greyfabric_price, 0)                                               price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)                  money,
              CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification) specification
       FROM jrkbillsum a
                LEFT JOIN plan b ON a.planid = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

      UNION ALL

      #   收款调整
      (SELECT c.company_name                                                                    clientName,
              b.plan_code                                                                       planCode,
              b.product_name                                                                    productName,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)                              ps,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)                                qty,
              ifnull(a.price, 0)                                                                price,
              round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2) money,
              CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification)       specification
       FROM jrkbill_adjust a
                LEFT JOIN plan b ON a.plan_id = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)
        union all
        # 发货管理
        (SELECT a.client                                                       clientName,
               c.plan_code                                                     planCode,
               c.product_name                                                  productName,
               -ifnull(a.ps, 0)                                                ps,
               -ifnull(a.qty, 0)                                               qty,
               ifnull(a.price, 0)                                              price,
               round(ifnull(a.price, 0) * -ifnull(a.qty, 0), 2)                 money,
               CONCAT_WS(' ', c.order_code, c.product_name, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                         c.greyfabric_specification, c.material_specification) specification
        FROM whitebillfh a
#                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON a.planid = c.id)
                ) rep
        left join contact_company co on rep.clientName = co.company_name
GROUP BY rep.clientName order by co.id ) re where abs(ifnull(re.ps,0))>0; "),
                      concat("
    select re.* from (
    SELECT any_value(rep.clientName) client,
       rep.planCode,
       any_value(rep.productName) productName,
       sum(rep.ps) ps,
       sum(rep.qty) qty,
       avg(rep.price) price,
       sum(rep.money) money,
       any_value(rep.specification) specification
FROM ((SELECT c.company_name                                                              clientName,
              b.plan_code                                                                 planCode,
              b.product_name                                                              productName,
              ifnull(a.ps, 0)                                                             ps,
              ifnull(a.qty, 0)                                                            qty,
              ifnull(b.greyfabric_price, 0)                                               price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)                  money,
              CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification) specification
       FROM jrkbillsum a
                LEFT JOIN plan b ON a.planid = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

      UNION ALL

      #   收款调整
      (SELECT c.company_name                                                                    clientName,
              b.plan_code                                                                       planCode,
              b.product_name                                                                    productName,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)                              ps,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)                                qty,
              ifnull(a.price, 0)                                                                price,
              round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2) money,
              CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification)       specification
       FROM jrkbill_adjust a
                LEFT JOIN plan b ON a.plan_id = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)
       union all
       # 发货管理
        (SELECT a.client                                                        clientName,
               c.plan_code                                                     planCode,
               c.product_name                                                  productName,
               -ifnull(a.ps, 0)                                                ps,
               -ifnull(a.qty, 0)                                               qty,
               ifnull(a.price, 0)                                              price,
               round(ifnull(a.price, 0) * -ifnull(a.qty, 0), 2)                 money,
               CONCAT_WS(' ', c.order_code, c.product_name, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                         c.greyfabric_specification, c.material_specification) specification
        FROM whitebillfh a
#                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON a.planid = c.id)
                ) rep
        left join contact_company co on rep.clientName = co.company_name
GROUP BY rep.planCode order by co.id ) re where abs(ifnull(re.ps,0))>0; ")
        );
    PREPARE stat FROM @baseSql;
    EXECUTE stat ;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_jrkbillfh_dtl
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_jrkbillfh_dtl`;
delimiter ;;
CREATE PROCEDURE `p_jrkbillfh_dtl`(IN `id` BIGINT)
BEGIN
    /**
       2021年5月22日 发货明细
       2021年6月6日  拼接字段,追加类型到单据列
     */
    SELECT r.*
    FROM ((SELECT a.client                             client_name,
                  a.billcode                           billcode,
                  a.address                            address,
                  ifnull(sum(a.ps), 0)                 send_pairs,
                  sum(round(a.qty, 1))                 send_kilo,
                  d.greyfabric_specification           greyfabric_specification,
                  d.material_specification             material_specification,
                  date_format(a.datafh, '%m-%d %H:%i') send_date,
                  a.memo                               remarks,
                  a.datafh                             order_date
           FROM whitebillfh a
#                     LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                    LEFT JOIN plan d ON a.planid = d.id
           WHERE a.planid = `id`
             AND a.id IS NOT NULL
           GROUP BY a.id)
          UNION ALL
          (SELECT c.company_name                             client_name,
                  b.bill_type                                billcode,
                  b.address                                  address,
                  ifnull(b.pairs, 0)                         send_pairs,
                  round(b.qty, 1)                            send_kilo,
                  a.greyfabric_specification                 greyfabric_specification,
                  a.material_specification                   material_specification,
                  date_format(b.jrkbill_date, '%m-%d %H:%i') send_date,
                  b.remarks                                  remarks,
                  b.jrkbill_date                             order_date
           FROM plan a
                    LEFT JOIN jrkbill_adjust b ON a.id = b.plan_id
                    LEFT JOIN contact_company c ON a.contact_company_id = c.id
           WHERE b.plan_id = `id`
             AND b.bill_type = '手动出库'
             AND b.id IS NOT NULL)) r
    ORDER BY r.order_date DESC;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_plan_dtl
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_plan_dtl`;
delimiter ;;
CREATE PROCEDURE `p_plan_dtl`(IN `id` BIGINT)
BEGIN
    /**
      2021年5月22日 计划单明细查看
     */
    SELECT b.company_name                                                                      company_name,
           concat(ifnull(a.plan_code, ''), ' ', ifnull(a.product_name, ''), ' ', a.greyfabric_weight, '*',
                  a.greyfabric_width, ' ', ifnull(a.m_height, 0), ' ', ifnull(a.greyfabric_specification, ''), ' ',
                  ifnull(a.material_specification, ''))                                        product_name,
           ifnull(a.plan_kilo, 0)                                                              plan_kilo,
           ifnull(c.pairs, 0) + ifnull(f.pairs, 0)                                             input_pairs,
           ifnull(c.kilo, 0) + ifnull(f.kilo, 0)                                               input_kilo,
           ifnull(d.kilo, 0) + ifnull(e.kilo, 0)                                               send_kilo,
           ifnull(d.pairs, 0) + ifnull(e.pairs, 0)                                             send_pairs,
           ifnull(round(ifnull(a.plan_kilo, 0) - ifnull(d.kilo, 0) - ifnull(e.kilo, 0), 1), 0) plan_remain_kilo,
           ifnull(round(ifnull(c.kilo, 0) + ifnull(f.kilo, 0) - ifnull(d.kilo, 0) - ifnull(e.kilo, 0) +
                        ifnull(g.kilo, 0), 1), 0)                                              remain_kilo,
           ifnull(round(ifnull(c.pairs, 0) + ifnull(f.pairs, 0) - ifnull(d.pairs, 0) - ifnull(e.pairs, 0) +
                        ifnull(g.pairs, 0), 0), 0)                                             remain_pairs,
           ifnull(a.greyfabric_price, 0)                                                       greyfabric_price
    FROM plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN (
        # 总入库公斤
        SELECT a.id, ifnull(b.pairs, 0) pairs, ifnull(b.kilo, 0) kilo
        FROM plan a
                 LEFT JOIN (SELECT planid, sum(ps) pairs, sum(qty) kilo FROM jrkbillsum GROUP BY planid) b
                           ON a.id = b.planid) c ON c.id = a.id
             LEFT JOIN (
        # 已发公斤
        SELECT a.planid id, ifnull(sum(round(a.qty, 1)), 0) kilo, ifnull(sum(a.ps), 0) pairs
        FROM whitebillfh a
#                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
        GROUP BY a.planid) d ON d.id = a.id
             LEFT JOIN (
        # 手动出库
        SELECT ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo, plan_id
        FROM jrkbill_adjust
        WHERE state = 1
          AND bill_type = '手动出库'
        GROUP BY plan_id) e ON e.plan_id = a.id
             LEFT JOIN (
        #             手动入库
        SELECT ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo, plan_id
        FROM jrkbill_adjust
        WHERE state = 1
          AND bill_type IN ('手动入库')
        GROUP BY plan_id) f ON f.plan_id = a.id
             LEFT JOIN (
        #            盘点
        SELECT ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo, plan_id
        FROM jrkbill_adjust
        WHERE state = 1
          AND bill_type IN ('盘点')
        GROUP BY plan_id) g ON g.plan_id = a.id
    WHERE a.id = `id`;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_plan_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_plan_report`;
delimiter ;;
CREATE PROCEDURE `p_plan_report`(IN `start` DATE, IN `end` DATE, IN `markFinished` TINYINT(1))
BEGIN
    /**
        2021年5月18日 计划单序时表
        2021年5月26日 追加是否完工字段
     */
    SET @baseSql = concat("
SELECT a.id,
       a.contact_company_id,
       b.company_name,
       a.contract_code,
       a.plan_code,
       a.plan_date,
       a.order_code,
       a.product_name,
       a.material_specification,
       a.material_lot_no,
       a.material_supplier_id,
       d.supplier_name                         material_supplier_name,
       a.greyfabric_weight,
       a.greyfabric_width,
       a.greyfabric_specification,
       a.greyfabric_price,
       a.m_height,
       a.plan_kilo,
       a.greyfabric_date,
       a.submit_date,
       a.material_price,
       a.processing_cost,
       a.middle_specification,
       a.middle_supplier_id,
       e.supplier_name                         middle_supplier_name,
       a.bottom_specification,
       a.bottom_supplier_id,
       c.supplier_name                         bottom_supplier_name,
       a.machine_type,
       a.needle_amount,
       a.low_weight,
       a.low_width,
       a.plan_employee_id,
       h.emp_name                              plan_employee_name,
       a.product_employee_id,
       i.emp_name                              product_employee_name,
       a.check_employee_id,
       g.emp_name                              check_employee_name,
       a.mark_company_name,
       a.fabric_requirement,
       a.remarks,
       a.create_user_id,
       ifnull(a.mark_finished, 0)              mark_finished,
       j.user_name                             create_user_name,
       a.create_time,
       a.state,
       ifnull(k.pairs, 0)                                                           input_pairs,
       ifnull(k.kilo, 0)                                                            input_kilo,
       ifnull(l.kilo, 0)+ifnull(n.kilo,0)                                           delivery_kilo,
       ifnull(l.pairs, 0)+ifnull(n.pairs,0)                                         delivery_pairs,
       ifnull(k.pairs, 0)-ifnull(l.pairs, 0)-ifnull(n.pairs,0)+ifnull(m.pairs,0)    remain_pairs,
       ifnull(k.kilo, 0)-ifnull(l.kilo, 0)-ifnull(n.kilo,0)+ifnull(m.kilo,0)        remain_kilo,
       a.notice_date                           notice_date
FROM plan a
         LEFT JOIN contact_company b ON a.contact_company_id = b.id
         LEFT JOIN supplier c ON a.bottom_supplier_id = c.id
         LEFT JOIN supplier d ON a.material_supplier_id = d.id
         LEFT JOIN supplier e ON a.middle_supplier_id = e.id
         LEFT JOIN employee g ON a.check_employee_id = g.id
         LEFT JOIN employee h ON a.plan_employee_id = h.id
         LEFT JOIN employee i ON a.product_employee_id = i.id
         LEFT JOIN sys_user j ON a.create_user_id = j.id
         LEFT JOIN (
    /* 打卷数据 */
     SELECT
                a.id,
                ifnull(b.pairs,0)+ifnull(c.pairs,0) pairs,
                 ifnull(b.kilo,0)+ifnull(c.kilo,0) kilo
                FROM plan a
                 LEFT JOIN (SELECT planid,sum(ps) pairs,sum(qty) kilo FROM jrkbillsum GROUP BY planid) b ON a.id = b.planid
                 LEFT JOIN (SELECT plan_id,sum(ifnull(pairs,0)) pairs,sum(ifnull(qty,0)) kilo FROM jrkbill_adjust WHERE state = 1 and bill_type='手动入库' GROUP BY plan_id) c ON a.id = c.plan_id
     ) k ON k.id = a.id
         LEFT JOIN (
    /* 发货数据 */
    SELECT ifnull(sum(a.ps), 0) pairs, ifnull(sum(a.qty), 0) kilo, a.planid
    FROM whitebillfh a
#              LEFT JOIN whitebilldetailfh b ON a.id = b.fid
    GROUP BY a.planid) l ON l.planid = a.id
    left join (
    /* 盘点 */
        SELECT plan_id, ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo
            FROM jrkbill_adjust
            WHERE bill_type = '盘点'
            GROUP BY plan_id
    ) m on m.plan_id = a.id
     left join (
     /* 出库 */
        SELECT plan_id, ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo
            FROM jrkbill_adjust
            WHERE bill_type = '手动出库'
            GROUP BY plan_id
    ) n on n.plan_id = a.id
    WHERE a.state = 1 ", if(isnull(`markFinished`), '', concat(" AND ifnull(a.mark_finished, 0)=", `markFinished`)),
                          if(isnull(`markFinished`),
                             concat("AND DATE(a.plan_date) BETWEEN DATE('", `start`, "') AND DATE('", `end`, "')"), ''),
                          " order by ifnull(a.mark_finished, 0),a.create_time desc");
    PREPARE statement FROM @baseSql;
    EXECUTE statement;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_plan_storage_modify
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_plan_storage_modify`;
delimiter ;;
CREATE PROCEDURE `p_plan_storage_modify`(IN `id` BIGINT)
BEGIN
    /**
      2021年6月4日  库存调整存储过程
      2021年6月6日  追加字段,客户，收货单位，供应商
     */
    SELECT *
    FROM (SELECT report.*,
                 @storage_ps := ifnull(report.pairs, 0) + @storage_ps  remain_pairs,
                 @storage_qty := ifnull(report.kilo, 0) + @storage_qty remain_kilo
          FROM (
                   --           库存
                   (SELECT NULL                                     id,
                           a.id                                     plan_id,
                           date_format(b.createdate, '%m-%d %H:%i') jrkbill_date,
                           '入库'                                     bill_type,
                           ifnull(sum(b.ps), 0)                     pairs,
                           ifnull(sum(b.qty), 0)                    kilo,
                           ''                                       remarks,
                           c.company_name                           client_name,
                           NULL                                     address,
                           NULL                                     price,
                           b.createdate                             order_date
                    FROM plan a
                             LEFT JOIN jrkbillsum b ON a.id = b.planid
                             LEFT JOIN contact_company c ON a.contact_company_id = c.id
                    WHERE a.id = `id`
                      AND b.id IS NOT NULL
                    GROUP BY b.createdate)

                   UNION ALL
                   --          手动调整
                   (SELECT b.id                                                              id,
                           a.id                                                              plan_id,
                           date_format(b.jrkbill_date, '%m-%d %H:%i')                        jrkbill_date,
                           ifnull(b.bill_type, '')                                           bill_type,
                           if(b.bill_type = '手动出库', -ifnull(b.pairs, 0), ifnull(b.pairs, 0)) pairs,
                           if(b.bill_type = '手动出库', -ifnull(b.qty, 0), ifnull(b.qty, 0))     kilo,
                           b.remarks                                                         remarks,
                           b.client_name                                                     client_name,
                           b.address                                                         address,
                           b.price                                                           price,
                           b.jrkbill_date                                                    order_date
                    FROM plan a
                             LEFT JOIN jrkbill_adjust b ON a.id = b.plan_id AND b.state = 1
                    WHERE a.id = `id`
                      AND b.id IS NOT NULL)

                   UNION ALL
                   --          发货
                   (SELECT NULL                                 id,
                           a.id                                 plan_id,
                           date_format(c.datafh, '%m-%d %H:%i') jrkbill_date,
                           '发货'                                 bill_type,
                           -ifnull(c.ps, 0)                     pairs,
                           -ifnull(c.qty, 0)                    kilo,
                           c.memo                               remarks,
                           c.client                             client_name,
                           c.address                            address,
                           c.price                              price,
                           c.datafh                             order_date
                    FROM plan a
#                              LEFT JOIN whitebilldetailfh b ON a.id = b.planid
                             LEFT JOIN whitebillfh c ON a.id = c.planid
                    WHERE a.id = `id`)) report,
               (SELECT @storage_ps := 0) p,
               (SELECT @storage_qty := 0) q
          ORDER BY report.order_date) r
    ORDER BY r.order_date DESC;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_wechat_push_list
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_wechat_push_list`;
delimiter ;;
CREATE PROCEDURE `p_wechat_push_list`(IN `moduleName` VARCHAR(100))
BEGIN
    /**
        2021年6月26日  微信推送信息生成
        2021年6月27日  客户列更改
     */
    IF (`moduleName` = 'GREY_FABRIC') THEN
        SELECT '你有一个订单已经发货'                                                 a,
               concat(ifnull(a.client, ''), '(', a.address, ')')       b,
               concat(a.billcode, '(白坯)')                                   c,
               concat(ifnull(c.plan_code, ''), ' ', ifnull(c.product_name, ''), ' ', c.greyfabric_weight, '*',
                      c.greyfabric_width, ' ', ifnull(c.m_height, 0), ' ', ifnull(c.greyfabric_specification, ''), ' ',
                      ifnull(c.material_specification, ''))                 d,
               concat(ifnull(a.ps, 0), '匹,', ifnull(a.qty, 0), 'KG,单价', ifnull(a.price, 0), '元,金额',
                      round(ifnull(a.qty, 0) * ifnull(a.price, 0), 2), '元') e,
               date_format(a.datafh, '%Y年%m月%d日')                           f,
               '如有质量问题，请在发货后7天内通知我厂，请勿开裁！'                                  g,
               ''                                                           h,
               -1                                                           accessMode,
               a.id                                                         id
        FROM whitebillfh a
#                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON a.planid = c.id
                 LEFT JOIN contact_company d ON c.contact_company_id = d.id
        WHERE a.is_push = 0;
    ELSEIF (`moduleName` = 'DYE_FABRIC') THEN
        SELECT '你有一个订单已经发货'                                                                               a,
               concat(ifnull(c.company_name, ''), '(', a.client, ')')                                     b,
               concat(a.billcode, '(色布)')                                                                 c,
               concat_ws(' ', b.plan_code, b.product_name, b.material_specification, b.color_no, b.color) d,
               concat(ifnull(a.ps, 0), '匹,', ifnull(a.qty, 0), 'KG,单价', ifnull(a.price, 0), '元,金额',
                      round(ifnull(a.qty, 0) * ifnull(a.price, 0), 2), '元')                               e,
               date_format(a.datafh, '%Y年%m月%d日')                                                         f,
               '如有质量问题，请在发货后7天内通知我厂，请勿开裁！'                                                                g,
               ''                                                                                         h,
               -1                                                                                         accessMode,
               a.id                                                                                       id
        FROM cp_billfh a
                 LEFT JOIN dye_fabric_plan b ON a.planid = b.id
                 LEFT JOIN contact_company c ON b.contact_company_id = c.id
        WHERE a.is_push = 0;
    END IF;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;