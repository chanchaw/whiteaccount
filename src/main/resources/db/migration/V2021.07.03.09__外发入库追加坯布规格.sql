/**
  2021年7月5日 外发入库追加坯布规格
 */
alter table product_outward_order modify material_specification varchar(100) null comment '原料规格';

alter table product_outward_order
    add greyfabric_specification VARCHAR(100) null comment '坯布规格' after material_specification;