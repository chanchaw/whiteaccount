/**
  2021年7月5日 追加收货单位
 */
alter table product_outward_order
    add consignee VARCHAR(200) null comment '收货单位' after is_finished_product;
alter table product_outward_order
	add ab_surface VARCHAR(10) null comment 'AB面' after is_finished_product;
alter table product_outward_order
	add is_send_directly TINYINT(1) default 0 null comment '是否直发(0:否;1:是;)';
