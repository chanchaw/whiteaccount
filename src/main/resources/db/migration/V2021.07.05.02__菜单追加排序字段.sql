/**
  2021年7月5日 菜单追加排序字段
 */
alter table sys_menu
    add serial_no int null comment '排序字段';