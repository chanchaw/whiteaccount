/**
  报表起始日期距离当前天数
 */
REPLACE INTO sys_config_int (config_field, config_value, config_desc)
VALUES ('报表起始日期距离当前天数', 30, '报表起始日期距离当前天数,只能大于0,小于0可能会不显示数据');