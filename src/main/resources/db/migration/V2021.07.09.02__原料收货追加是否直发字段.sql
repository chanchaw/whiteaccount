/**
  2021年7月9日 原料收货追加是否直发字段
 */
alter table material_order
    add is_send_directly TINYINT(1) default 0 null comment '是否直发(0:否;1:是;)';