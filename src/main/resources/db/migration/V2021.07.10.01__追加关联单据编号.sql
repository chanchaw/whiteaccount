/**
  2021年7月10日 追加关联单据编号
 */
alter table material_order
    add linked_order_code VARCHAR(100) null comment '关联单据编号';

create index material_order_linked_order_code_index
    on material_order (linked_order_code);