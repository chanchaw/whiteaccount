/**
  2021年7月11日 追加对应的单据编号
 */
alter table product_outward_order
    add linked_order_code VARCHAR(100) null comment '对应的单据编号';
create index product_outward_order_linked_order_code_index
    on product_outward_order (linked_order_code);