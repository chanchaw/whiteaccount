DROP PROCEDURE IF EXISTS `p_unfinished_outward`;
DELIMITER ;;
CREATE PROCEDURE p_unfinished_outward(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100))
BEGIN
    /**
      2021年7月12日 未剖幅报表
     */
    SELECT a.id,
           d.company_name                                  client,
           a.order_date                                    order_date,
           a.process_company                               process_company,
           a.order_code                                    order_code,
           a.plan_code                                     plan_code,
           a.material_specification                        material_specification,
           a.greyfabric_specification                      greyfabric_specification,
           ifnull(a.box_amount, 0)                         box_amount,
           ifnull(a.input_kilo, 0)                         input_kilo,
           a.consignee                                     consignee,
           a.handler_name                                  handler_name,
           b.user_name                                     create_user_name,
           if(ifnull(a.is_send_directly, 0) = 0, '否', '是') is_send_directly,
           a.remarks                                       remarks
    FROM product_outward_order a
             LEFT JOIN sys_user b ON a.create_user_id = b.id
             LEFT JOIN plan c ON a.plan_code = c.plan_code
             LEFT JOIN contact_company d ON c.contact_company_id = d.id
    WHERE ifnull(a.state, 0) = 1
      AND ifnull(a.is_finished_product, 0) = 0
      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, d.company_name = `clientName`)
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.create_time DESC;
END ;;