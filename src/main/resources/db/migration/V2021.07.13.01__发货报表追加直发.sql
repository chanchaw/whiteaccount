SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `p_jrkbillfh_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_jrkbillfh_dtl`(IN `id` BIGINT)
BEGIN
    /**
       2021年5月22日 发货明细
       2021年6月6日  拼接字段,追加类型到单据列
       2021年7月13日 追加直发
     */
    SELECT r.*
    FROM ((SELECT a.client                             client_name,
                  a.billcode                           billcode,
                  a.address                            address,
                  ifnull(sum(a.ps), 0)                 send_pairs,
                  sum(round(a.qty, 1))                 send_kilo,
                  d.greyfabric_specification           greyfabric_specification,
                  d.material_specification             material_specification,
                  date_format(a.datafh, '%m-%d %H:%i') send_date,
                  a.memo                               remarks,
                  a.datafh                             order_date
           FROM whitebillfh a
                    #                     LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                    LEFT JOIN plan d ON a.planid = d.id
           WHERE a.planid = `id`
             AND a.id IS NOT NULL
           GROUP BY a.id)
          UNION ALL
          (SELECT c.company_name                             client_name,
                  b.bill_type                                billcode,
                  b.address                                  address,
                  ifnull(b.pairs, 0)                         send_pairs,
                  round(b.qty, 1)                            send_kilo,
                  a.greyfabric_specification                 greyfabric_specification,
                  a.material_specification                   material_specification,
                  date_format(b.jrkbill_date, '%m-%d %H:%i') send_date,
                  b.remarks                                  remarks,
                  b.jrkbill_date                             order_date
           FROM plan a
                    LEFT JOIN jrkbill_adjust b ON a.id = b.plan_id
                    LEFT JOIN contact_company c ON a.contact_company_id = c.id
           WHERE b.plan_id = `id`
             AND b.bill_type = '手动出库'
             AND b.id IS NOT NULL)
          UNION ALL
          #     直发
          (SELECT c.company_name                            client_name,
                  b.order_code                              billcode,
                  b.process_company                         address,
                  ifnull(b.input_pairs, 0)                  send_pairs,
                  round(b.input_kilo, 1)                    send_kilo,
                  a.greyfabric_specification                greyfabric_specification,
                  a.material_specification                  material_specification,
                  date_format(b.create_time, '%m-%d %H:%i') send_date,
                  b.remarks                                 remarks,
                  b.create_time                             order_date
           FROM plan a
                    LEFT JOIN product_outward_order b ON a.plan_code = b.plan_code AND b.state = 1
                    LEFT JOIN contact_company c ON a.contact_company_id = c.id
           WHERE a.id = `id`
             AND ifnull(b.is_finished_product, 0) = 1
             AND ifnull(b.is_send_directly, 0) = 1
             AND b.id IS NOT NULL)) r
    ORDER BY r.order_date DESC;

END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
