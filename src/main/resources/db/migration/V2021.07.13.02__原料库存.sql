SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `p_material_storage`;
DELIMITER ;;
CREATE PROCEDURE `p_material_storage`(IN `supplierName` VARCHAR(100))
BEGIN
    /**
      2021年6月30日 原料库存
      2021年7月1日  修改BUG
     */
    SELECT rep.*
    FROM (SELECT a.material_spec,
                 a.batch_num,
                 sum(b.access_mode * ifnull(a.input_kilo, 0)) input_kilo,
                 ifnull(sum(b.access_mode * a.box_amount), 0) box_amount,
                 c.supplier_name,
                 a.supplier_id
          FROM material_order a
                   LEFT JOIN bill_type b ON a.bill_type = b.id
                   LEFT JOIN supplier c ON a.supplier_id = c.id
          WHERE a.state = 1
            AND ifnull(a.is_send_directly, 0) = 0
            AND if(ifnull(`supplierName`, '') = '' OR ifnull(`supplierName`, '') = '全部', 1 = 1,
                   c.supplier_name = `supplierName`)
          GROUP BY a.supplier_id, a.material_spec, a.batch_num) rep
    WHERE abs(ifnull(rep.input_kilo, 0)) > 0;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;