/**
  2021年7月13日 追加原料退回单据类型
 */
REPLACE INTO bill_type (id, bill_type_name, access_mode, allow_manual, storage_id, inout_type, is_selected)
VALUES ('YLTH', '原料退回', 1, 1, '1', 1, 0);
