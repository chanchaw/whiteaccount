SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`
(
    `id`        INT(11)                                                NOT NULL AUTO_INCREMENT COMMENT '自增主键',
    `parent_id` INT(11)                                                NULL DEFAULT NULL COMMENT '父节点',
    `menu_name` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名',
    `menu_path` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单相对路径',
    `menu_icon` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
    `serial_no` INT(11)                                                NULL DEFAULT NULL COMMENT '排序字段',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `sys_menu_parent_id_index` (`parent_id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 801
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '菜单表';

BEGIN;
INSERT INTO `sys_menu`
VALUES (100, NULL, '计划管理', '/', 'eject-aside-icon-gur-project-08', 1),
       (101, 100, '白坯计划', 'jh_plan_report', '', NULL),
       (102, 100, '成品计划', 'jh_dye_fabric_plan_report', '', NULL),
       (200, NULL, '白坯管理', '/', 'eject-aside-icon-gur-project-01', 2),
       (201, 200, '白坯管理', 'fh_delivery_report', '', NULL),
       (300, NULL, '原料管理', '/', 'eject-aside-icon-gur-project-36', 4),
       (301, 300, '原料管理', 'yf_material_order_report', '', NULL),
       (400, NULL, '基础资料', '/', 'eject-aside-icon-gur-project-34', 7),
       (401, 400, '客户', 'jc_contactcompany', '', NULL),
       (402, 400, '供应商', 'jc_supplier', NULL, NULL),
       (403, 400, '品名', 'jc_product_info', '', NULL),
       (404, 400, '原料规格', 'jc_specification', NULL, NULL),
       (405, 400, '白坯克重', 'jc_weight_of_fabric', NULL, NULL),
       (406, 400, '白坯门幅', 'jc_width_of_fabric', '', NULL),
       (407, 400, '机台号', 'jc_machine_type', '', NULL),
       (408, 400, '部门员工', 'jc_employee', '', NULL),
       (409, 400, '计划模板', 'jc_plan_template', '', NULL),
       (410, 400, '收货单位', 'jc_take_delivery', '', NULL),
       (411, 400, '单据类型', 'jc_bill_type', '', NULL),
       (412, 400, '头份', 'jc_first_copies', '', NULL),
       (413, 400, '加工类型', 'jc_process_type', '', NULL),
       (414, 400, '色布计划模板', 'jc_dye_fabric_plan_template', '', NULL),
       (415, 400, '色布品名', 'jc_dye_product_info', '', NULL),
       (416, 400, '色布规格', 'jc_dye_specification', '', NULL),
       (417, 400, '代发客户', 'jc_delivery_company', '', NULL),
       (418, 400, '加工户', 'jc_process_company', '', NULL),
       (500, NULL, '系统管理', '/', 'eject-aside-icon-gur-project-31', 8),
       (501, 500, '权限表', 'jc_permission', NULL, NULL),
       (502, 500, '用户表', 'jc_user', NULL, NULL),
       (503, 500, '角色组', 'sz_role_group', NULL, NULL),
       (504, 500, '配置信息', 'sz_config', NULL, NULL),
       (505, 500, '重置账套', 'sz_reset_account', '', NULL),
       (600, NULL, '外发管理', '', 'eject-aside-icon-gur-project-35', 5),
       (601, 600, '外发管理', 'wf_material_send_report', '', NULL),
       (700, NULL, '财务管理', '', 'eject-aside-icon-gur-project-18', 6),
       (701, 700, '应收账款', 'ysyf_receivable_report', '', NULL),
       (702, 700, '应付账款', 'ysyf_payable_report', '', NULL),
       (703, 700, '外发对账', 'ysyf_outward_report', '', NULL),
       (800, NULL, '成品管理', '', 'eject-aside-icon-gur-project-30', 3),
       (801, 800, '成品管理', 'fh_dye_delivery_report', '', NULL);
COMMIT;
TRUNCATE TABLE sys_permission;
INSERT INTO sys_permission (sys_menu_id, sys_role_id)
SELECT m.id, b.id
FROM sys_menu m
         JOIN sys_role b;

SET FOREIGN_KEY_CHECKS = 1;