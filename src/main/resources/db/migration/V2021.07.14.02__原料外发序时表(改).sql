SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `p_material_outward`;
DELIMITER ;;
CREATE PROCEDURE `p_material_outward`(IN `start` DATE, IN `end` DATE, IN `processName` VARCHAR(100))
BEGIN
    /**
        2021年7月3日 外发序时表
        2021年7月10日 追加字段(是否直发)
    */
    SELECT a.id                                               id,
           a.order_code                                       order_code,
           a.process_company                                  process_company,
           a.material_spec                                    material_spec,
           a.batch_num                                        batch_num,
           ifnull(a.input_kilo, 0) * if(c.id = 'YLTH', -1, 1) input_kilo,
           ifnull(a.box_amount, 0) * if(c.id = 'YLTH', -1, 1) box_amount,
           b.supplier_name                                    supplier_name,
           DATE(a.order_date)                                 order_date,
           a.create_time                                      create_time,
           if(ifnull(a.is_send_directly, 0), '是', '否')        is_send_directly,
           a.handler_name                                     handler_name,
           a.remarks                                          remarks,
           c.bill_type_name                                   bill_type_name,
           if(c.id = 'YLTH', 1, 0)                            is_returning
    FROM material_order a
             LEFT JOIN supplier b ON a.supplier_id = b.id
             LEFT JOIN bill_type c ON a.bill_type = c.id
             LEFT JOIN process_company d ON a.process_company = d.company_name
    WHERE a.state = 1
      AND ((c.access_mode < 0 AND ifnull(d.is_selected, 0) <= 0) OR c.id = 'YLTH')
      AND if(ifnull(`processName`, '') = '' OR ifnull(`processName`, '') = '全部', 1 = 1,
             a.process_company = `processName`)
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.id DESC;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;