DROP PROCEDURE IF EXISTS `p_wechat_push_list`;
DELIMITER ;;
CREATE PROCEDURE `p_wechat_push_list`(IN `moduleName` VARCHAR(100))
BEGIN
    /**
        2021年6月26日  微信推送信息生成
        2021年6月27日  客户列更改
     */
    IF (`moduleName` = 'GREY_FABRIC') THEN
        SELECT if(ifnull(a.updatefh, 0) > 0, '发货单修改通知', '你有一个订单已经发货')       a,
               concat(ifnull(a.client, ''), '(', a.address, ')')            b,
               concat(a.billcode, '(白坯)')                                   c,
               concat(ifnull(c.plan_code, ''), ' ', ifnull(c.product_name, ''), ' ', c.greyfabric_weight, '*',
                      c.greyfabric_width, ' ', ifnull(c.m_height, 0), ' ', ifnull(c.greyfabric_specification, ''), ' ',
                      ifnull(c.material_specification, ''))                 d,
               concat(ifnull(b.ps, 0), '匹,', ifnull(b.qty, 0), 'KG,单价', ifnull(a.price, 0), '元,金额',
                      round(ifnull(b.qty, 0) * ifnull(a.price, 0), 2), '元') e,
               date_format(a.datafh, '%Y年%m月%d日')                           f,
               '如有质量问题，请在发货后7天内通知我厂，请勿开裁！'                                  g,
               ''                                                           h,
               -1                                                           accessMode,
               a.id                                                         id
        FROM whitebillfh a
                 LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON b.planid = c.id
                 LEFT JOIN contact_company d ON c.contact_company_id = d.id
        WHERE b.is_push = 0;
    ELSEIF (`moduleName` = 'DYE_FABRIC') THEN
        SELECT if(ifnull(a.updatefh, 0) > 0, '发货单修改通知', '你有一个订单已经发货')                                     a,
               concat(ifnull(c.company_name, ''), '(', a.client, ')')                                     b,
               concat(a.billcode, '(色布)')                                                                 c,
               concat_ws(' ', b.plan_code, b.product_name, b.material_specification, b.color_no, b.color) d,
               concat(ifnull(a.ps, 0), '匹,', ifnull(a.qty, 0), 'KG,单价', ifnull(a.price, 0), '元,金额',
                      round(ifnull(a.qty, 0) * ifnull(a.price, 0), 2), '元')                               e,
               date_format(a.datafh, '%Y年%m月%d日')                                                         f,
               '如有质量问题，请在发货后7天内通知我厂，请勿开裁！'                                                                g,
               ''                                                                                         h,
               -1                                                                                         accessMode,
               a.id                                                                                       id
        FROM cp_billfh a
                 LEFT JOIN dye_fabric_plan b ON a.planid = b.id
                 LEFT JOIN contact_company c ON b.contact_company_id = c.id
        WHERE a.is_push = 0;
    END IF;
END;;
DELIMITER ;