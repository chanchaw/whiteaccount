DROP PROCEDURE IF EXISTS `p_processcompany_account`;
DELIMITER ;;
CREATE PROCEDURE `p_processcompany_account`(IN `start` DATE, IN `end` DATE, IN `processcompany` VARCHAR(100),
                                            IN `mode` INT)
BEGIN
    /**
     2021年7月15日  加工户对账表
     */
    IF (ifnull(`mode`, 0) = 1) THEN
        #         汇总
        (SELECT a.process_company process_company,
                NULL              order_date,
                a.material_spec   material_spec,
                b.supplier_name   supplier_name,
                a.batch_num       batch_num,
                sum(a.box_amount) box_amount,
                sum(a.input_kilo) material_input_kilo,
                NULL              product_name,
                NULL              plan_code,
                NULL              greyfabric_spec,
                NULL              material_spec2,
                NULL              amount,
                NULL              greyfabric_input_kilo,
                NULL              process_cost,
                0                 process_money,
                0                 pay,
                NULL              consignee
         FROM material_order a
                  LEFT JOIN supplier b ON a.supplier_id = b.id
         WHERE a.state = 1
           AND if(ifnull(`processcompany`, '') = '' OR ifnull(`processcompany`, '') = '全部', 1 = 1,
                  a.process_company = `processcompany`)
           AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
         GROUP BY a.process_company, a.material_spec, b.supplier_name, a.batch_num)

        UNION ALL

        (SELECT a.process_company                                                                     process_company,
                NULL                                                                                  order_date,
                NULL                                                                                  material_spec,
                NULL                                                                                  supplier_name,
                NULL                                                                                  batch_num,
                NULL                                                                                  box_amount,
                NULL                                                                                  material_input_kilo,
                b.product_name                                                                        product_name,
                NULL                                                                                  plan_code,
                a.greyfabric_specification                                                            greyfabric_spec,
                a.material_specification                                                              material_spec2,
                sum(if(a.is_finished_product = 0, ifnull(a.box_amount, 0), ifnull(a.input_pairs, 0))) amount,
                sum(ifnull(a.input_kilo, 0))                                                          greyfabric_input_kilo,
                NULL                                                                                  process_cost,
                ifnull(sum(a.money), 0)                                                               process_money,
                NULL                                                                                  pay,
                NULL                                                                                  consignee
         FROM product_outward_order a
                  LEFT JOIN plan b ON a.plan_code = b.plan_code
         WHERE a.state = 1
           AND if(ifnull(`processcompany`, '') = '' OR ifnull(`processcompany`, '') = '全部', 1 = 1,
                  a.process_company = `processcompany`)
           AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
         GROUP BY a.process_company, b.product_name, a.greyfabric_specification, a.material_specification)
        UNION ALL
        (SELECT a.client_name process_company,
                a.bill_date   order_date,
                NULL          material_spec,
                NULL          supplier_name,
                NULL          batch_num,
                NULL          box_amount,
                NULL          material_input_kilo,
                NULL          product_name,
                NULL          plan_code,
                NULL          greyfabric_spec,
                NULL          material_spec2,
                0             amount,
                0             greyfabric_input_kilo,
                0             process_cost,
                0             process_money,
                sum(a.money)  pay,
                a.client_name consignee
         FROM material_account_payable a
         WHERE if(ifnull(`processcompany`, '') = '' OR ifnull(`processcompany`, '') = '全部', 1 = 1,
                  a.client_name = `processcompany`)
           AND DATE(a.bill_date) BETWEEN DATE(`start`) AND DATE(`end`)
         GROUP BY a.client_name);
    ELSE
        #         明细
        SELECT r.* FROM (
        (SELECT a.create_time     create_time,
                a.process_company process_company,
                a.order_date      order_date,
                a.material_spec,
                b.supplier_name,
                a.batch_num,
                a.box_amount,
                a.input_kilo      material_input_kilo,
                NULL              product_name,
                NULL              plan_code,
                NULL              greyfabric_spec,
                NULL              material_spec2,
                NULL              amount,
                NULL              greyfabric_input_kilo,
                NULL              process_cost,
                NULL              process_money,
                NULL              id,
                NULL              pay,
                NULL              consignee
         FROM material_order a
                  LEFT JOIN supplier b ON a.supplier_id = b.id
         WHERE a.state = 1
           AND if(ifnull(`processcompany`, '') = '' OR ifnull(`processcompany`, '') = '全部', 1 = 1,
                  a.process_company = `processcompany`)
           AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`))

        UNION ALL

        (SELECT a.create_time                                                                    create_time,
                a.process_company                                                                process_company,
                a.order_date                                                                     order_date,
                NULL                                                                             material_spec,
                NULL                                                                             supplier_name,
                NULL                                                                             batch_num,
                NULL                                                                             box_amount,
                NULL                                                                             material_input_kilo,
                b.product_name                                                                   product_name,
                a.plan_code                                                                      plan_code,
                a.greyfabric_specification                                                       greyfabric_spec,
                a.material_specification                                                         material_spec2,
                if(a.is_finished_product = 0, ifnull(a.box_amount, 0), ifnull(a.input_pairs, 0)) amount,
                ifnull(a.input_kilo, 0)                                                          greyfabric_input_kilo,
                a.processing_cost                                                                process_cost,
                ifnull(a.money, 0)                                                               process_money,
                NULL                                                                             id,
                NULL                                                                             pay,
                a.consignee                                                                      consignee
         FROM product_outward_order a
                  LEFT JOIN plan b ON a.plan_code = b.plan_code
         WHERE a.state = 1
           AND if(ifnull(`processcompany`, '') = '' OR ifnull(`processcompany`, '') = '全部', 1 = 1,
                  a.process_company = `processcompany`)
           AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`))
        UNION ALL
        (SELECT a.bill_date        create_time,
                a.client_name      process_company,
                a.bill_date        order_date,
                NULL               material_spec,
                NULL               supplier_name,
                NULL               batch_num,
                NULL               box_amount,
                NULL               material_input_kilo,
                NULL               product_name,
                NULL               plan_code,
                NULL               greyfabric_spec,
                NULL               material_spec2,
                0                  amount,
                0                  greyfabric_input_kilo,
                0                  process_cost,
                0                  process_money,
                a.id               id,
                ifnull(a.money, 0) pay,
                a.client_name      consignee
         FROM material_account_payable a
         WHERE if(ifnull(`processcompany`, '') = '' OR ifnull(`processcompany`, '') = '全部', 1 = 1,
                  a.client_name = `processcompany`)
           AND DATE(a.bill_date) BETWEEN DATE(`start`) AND DATE(`end`))) r
        ORDER BY r.create_time DESC;
    END IF;
END ;;
DELIMITER ;