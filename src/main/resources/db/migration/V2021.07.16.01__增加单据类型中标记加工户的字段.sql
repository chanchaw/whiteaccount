/**
  增加单据类型中标记加工户的字段
 */
alter table bill_type
    add company int default 0 null comment '0:原料商;1:加工户;';
alter table bill_type
	add is_return TINYINT(1) default 0 null comment '是否退回的类型(0:否;1:是;)' after is_selected;
alter table bill_type
	add is_check TINYINT(1) default 0 null comment '是否盘点的类型(0:否;1:是;)' after is_selected;