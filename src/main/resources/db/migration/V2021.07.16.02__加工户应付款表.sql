/**
  加工户应付款表
 */
CREATE TABLE process_account_payable
(
    id           INT AUTO_INCREMENT COMMENT '自增主键' PRIMARY KEY,
    bill_date    DATETIME             NULL COMMENT '单据日期',
    billtype     VARCHAR(100)         NULL COMMENT '单据类型',
    process_name VARCHAR(100)         NOT NULL COMMENT '外加工商',
    price        DECIMAL(30, 2)       NULL COMMENT '单价',
    money        DECIMAL(30, 2)       NULL COMMENT '金额',
    payable_type VARCHAR(100)         NULL COMMENT '类型(预付)',
    box_amount   INT                  NULL COMMENT '件数',
    billmemo     VARCHAR(255)         NULL COMMENT '备注',
    upload       INT        DEFAULT 0 NULL COMMENT '是否已经上传(0:未上传;1:已上传;)',
    is_push      TINYINT(1) DEFAULT 0 NULL COMMENT '是否推送字段(0:未推送;1:已推送;)'
) COMMENT '外加工应付款';
CREATE INDEX process_account_payable_process_name_index ON process_account_payable (process_name);
CREATE INDEX process_account_payable_payable_type_index ON process_account_payable (payable_type);
