SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `bill_type`;
CREATE TABLE `bill_type`
(
    `id`             VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '单据类型编号',
    `bill_type_name` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '单据类型名称',
    `access_mode`    INT(11)                                                NULL DEFAULT NULL COMMENT '数据影响(0,1,-1)',
    `allow_manual`   TINYINT(1)                                             NULL DEFAULT 1 COMMENT '是否允许手动生成',
    `storage_id`     VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属仓库编号',
    `inout_type`     INT(11)                                                NULL DEFAULT 1 COMMENT '扣库类型(本期入库:1;本期出库:-1)',
    `is_selected`    TINYINT(1)                                             NULL DEFAULT 0 COMMENT '是否默认选中',
    `is_check`       TINYINT(1)                                             NULL DEFAULT 0 COMMENT '是否盘点的类型(0:否;1:是;)',
    `is_return`      TINYINT(1)                                             NULL DEFAULT 0 COMMENT '是否退回的类型(0:否;1:是;)',
    `company`        INT(11)                                                NULL DEFAULT 0 COMMENT '0:原料商;1:加工户;',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `bill_type_bill_type_name_uindex` (`bill_type_name`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '单据类型';
#  插入数据
BEGIN;
INSERT INTO `bill_type`
VALUES ('LLCK', '领料出库', -1, 1, '1', -1, 1, 0, 0, 1),
       ('LLTH', '领料退回', 1, 1, '1', -1, 0, 0, 1, 1),
       ('QTRK', '其他入库', 1, 1, '1', 1, 0, 0, 0, 0),
       ('YLPD', '原料盘点', 1, 1, '1', 1, 0, 1, 0, 1),
       ('YLRK', '原料入库', 1, 0, '1', 1, 1, 0, 0, 0),
       ('YLTH', '原料退回', -1, 1, '1', 1, 0, 0, 1, 0);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;