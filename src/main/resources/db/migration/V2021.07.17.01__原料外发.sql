SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `p_material_outbound`;
DELIMITER ;;
CREATE PROCEDURE `p_material_outbound`(IN `start` DATE, IN `end` DATE, IN `processCompany` VARCHAR(100))
BEGIN
    /**
      2021年6月30日 原料领料序时表存储过程
      2021年7月15日 追加原料退回的单据类型
      2021年7月16日 追加单据类型
     */
    SELECT a.id                                            id,
           a.order_code                                    order_code,
           a.process_company                               process_company,
           a.material_spec                                 material_spec,
           a.batch_num                                     batch_num,
           ifnull(a.input_kilo, 0) * c.access_mode * -1    input_kilo,
           ifnull(a.box_amount, 0) * c.access_mode * -1    box_amount,
           b.supplier_name                                 supplier_name,
           DATE(a.order_date)                              order_date,
           a.create_time                                   create_time,
           a.handler_name                                  handler_name,
           a.remarks                                       remarks,
           c.access_mode                                   access_mode,
           if(ifnull(a.is_send_directly, 0) = 1, '是', '否') is_send_directly,
           c.bill_type_name                                bill_type_name,
           e.user_name                                     create_user_name
    FROM material_order a
             LEFT JOIN supplier b ON a.supplier_id = b.id
             LEFT JOIN bill_type c ON a.bill_type = c.id
             LEFT JOIN process_company d ON a.process_company = d.company_name
             LEFT JOIN sys_user e ON a.create_user_id = e.id
    WHERE a.state = 1
      AND ifnull(c.company, 0) = 1
      AND if(ifnull(`processCompany`, '') = '' OR ifnull(`processCompany`, '') = '全部', 1 = 1,
             a.process_company = `processCompany`)
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.id DESC;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
