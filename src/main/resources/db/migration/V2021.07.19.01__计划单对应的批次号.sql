/**
  2021年7月19日 计划单对应的批次号
 */
create table plan_batchnum
(
    id BIGINT auto_increment comment '自增主键',
    plan_code VARCHAR(100) not null comment '计划单号',
    batch_num VARCHAR(100) null comment '机台号',
    serial_num int null comment '排序字段',
    remarks VARCHAR(255) null comment '备注',
    create_time DATETIME default CURRENT_TIMESTAMP null comment '创建时间',
    constraint plan_batchnum_pk
        primary key (id)
)
    comment '白坯计划与批次号的对应关系';

create index plan_batchnum_batch_num_index
    on plan_batchnum (batch_num);

create index plan_batchnum_plan_code_index
    on plan_batchnum (plan_code);