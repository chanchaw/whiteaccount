/**
  增加喷码字段
 */
alter table plan
    add is_marking TINYINT(1) default 0 null comment '是否喷码(0:否;1:是;)';