SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `getFHhistoryDetail`;
DELIMITER ;;
CREATE PROCEDURE `getFHhistoryDetail`(m_SDate VARCHAR(100),
                                      m_EDate VARCHAR(100),
                                      m_Client VARCHAR(100),
                                      m_CheckType VARCHAR(100))
BEGIN


    SET @m_Date = CONCAT(" and DATE_FORMAT(a.datafh,'%Y-%m-%d') BETWEEN '", m_SDate, "' and '", m_EDate, "'");
    SET @m_Client = IF(m_Client = "全部", ' ', CONCAT(' and a.client=', "'", m_Client, "'"));
    SET @m_CheckType = IF(m_CheckType = "全部", '  ', CONCAT(' and a.checkout=', "'", m_CheckType, "'"));


    SET @SQL = "select a.id,a.client,a.address,
CONCAT_WS(' ',c.order_code,c.product_name,CONCAT_WS('*',c.greyfabric_width,c.greyfabric_weight),c.m_height,c.greyfabric_specification,a.aorb,c.material_specification)as pmgg,
a.ps,
round(a.qty,1)as qty,
round(a.price,2)as price,
ROUND(round(a.price,2) * round(a.qty,1),2) as money,
a.checkout,
a.billcode,
c.material_lot_no batch_num,
DATE_FORMAT(a.datafh,'%m-%d %H:%i') as b_data

from whitebillfh a
# left outer join whitebilldetailfh b on a.id=b.fid
left outer join plan c on a.planid=c.id
where 1=1 ";


    SET @SQL = CONCAT(@SQL, @m_Date, @m_Client, @m_CheckType, " ORDER BY a.id DESC");


    #SET @SQL = CONCAT(@SQL,);

    #select  @SQL;

    PREPARE stmt FROM @SQL;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_greyfabric_input
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_greyfabric_input`;
DELIMITER ;;
CREATE PROCEDURE `p_greyfabric_input`(IN `start` DATE, IN `end` DATE)
BEGIN
    /**
      2021年6月9日  白坯入库明细
      2021年7月12日 追加白坯
      2021年7月17日 追加直发
     */
    SELECT rep.*
    FROM (SELECT any_value(a.id)                                id,
                 any_value(c.company_name)                      client,
                 any_value(b.plan_code)                         planCode,
                 any_value(a.cn)                                computerName,
                 CONCAT_WS(' ', any_value(b.order_code), any_value(b.product_name),
                           CONCAT_WS('*', any_value(b.greyfabric_width), any_value(b.greyfabric_weight)),
                           any_value(b.m_height), any_value(b.greyfabric_specification),
                           any_value(b.material_specification)) specification,
                 ifnull(sum(a.ps), 0)                           ps,
                 ifnull(sum(a.qty), 0)                          qty,
                 a.createdate,
                 any_value(a.class_name)                        className,
                 any_value(a.jth)                               machineType,
                 a.aorb                                         aorb,
                 a.batch_num                                    batchNum,
                 NULL                                           isSendDirectly
          FROM jrkbillsum a
                   LEFT JOIN plan b ON a.planid = b.id
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
          WHERE DATE(a.createdate) BETWEEN DATE(`start`) AND DATE(`end`)
          GROUP BY a.createdate, a.aorb

          UNION ALL

          SELECT a.id,
                 c.company_name                                                              client,
                 b.plan_code                                                                 planCode,
                 a.bill_type                                                                 computerName,
                 CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.m_height, b.greyfabric_specification, b.material_specification) specification,
                 ifnull(a.pairs, 0)                                                          ps,
                 ifnull(a.qty, 0)                                                            qty,
                 a.jrkbill_date                                                              createdate,
                 NULL                                                                        className,
                 NULL                                                                        machineType,
                 NULL                                                                        aorb,
                 NULL                                                                        batchNum,
                 NULL                                                                        isSendDirectly
          FROM jrkbill_adjust a
                   LEFT JOIN plan b ON a.plan_id = b.id
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
          WHERE bill_type = '手动入库'
            AND DATE(jrkbill_date) BETWEEN DATE(`start`) AND DATE(`end`)
          UNION ALL
          #         白坯入库
          SELECT a.id,
                 c.company_name                                                              client,
                 b.plan_code                                                                 planCode,
                 a.process_company                                                           computerName,
                 CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.m_height, b.greyfabric_specification, a.material_specification) specification,
                 ifnull(a.input_pairs, 0)                                                    ps,
                 ifnull(a.input_kilo, 0)                                                     qty,
                 a.create_time                                                               createdate,
                 NULL                                                                        className,
                 NULL                                                                        machineType,
                 a.ab_surface                                                                aorb,
                 a.batch_num                                                                 batchNum,
                 if(ifnull(a.is_send_directly, 0) = 1, '是', '否')                             isSendDirectly
          FROM product_outward_order a
                   LEFT JOIN plan b ON a.plan_code = b.plan_code
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
          WHERE a.state = 1
            AND ifnull(a.is_finished_product, 0) = 1
            AND DATE(a.create_time) BETWEEN DATE(`start`) AND DATE(`end`)) rep
    ORDER BY rep.createdate DESC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_greyfabric_storage
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_greyfabric_storage`;
DELIMITER ;;
CREATE PROCEDURE `p_greyfabric_storage`(IN mode INT)
BEGIN
    /**
     2021年6月9日 白坯库存
     */
    SET @baseSql = if(mode = 1, concat("
    select re.* from (
    SELECT rep.clientName client,
       -- rep.planCode,
       -- rep.productName,
       sum(rep.ps) ps,
       sum(rep.qty) qty,
       -- avg(rep.price) price,
       p.material_lot_no  batchNum,
       sum(rep.money) money
       -- rep.specification
FROM ((SELECT c.company_name                                                              clientName,
              b.plan_code                                                                 planCode,
              b.product_name                                                              productName,
              ifnull(a.ps, 0)                                                             ps,
              ifnull(a.qty, 0)                                                            qty,
              ifnull(b.greyfabric_price, 0)                                               price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)                  money,
              CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification) specification,
              a.batch_num
       FROM jrkbillsum a
                LEFT JOIN plan b ON a.planid = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

      UNION ALL

      #   收款调整
      (SELECT c.company_name                                                                    clientName,
              b.plan_code                                                                       planCode,
              b.product_name                                                                    productName,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)                              ps,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)                                qty,
              ifnull(a.price, 0)                                                                price,
              round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2) money,
              CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification)       specification,
              NULL                                                                              batch_num
       FROM jrkbill_adjust a
                LEFT JOIN plan b ON a.plan_id = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

        union all
        #  白坯入库
        (
            SELECT c.company_name                                           clientName,
                   a.plan_code                                              planCode,
                   b.product_name                                           productName,
                   ifnull(a.input_pairs, 0)                                 ps,
                   ifnull(a.input_kilo, 0)                                  qty,
                   ifnull(a.processing_cost, 0)                             price,
                   round(a.money, 2)                                        money,
                   CONCAT_WS(' ', a.order_code, a.greyfabric_specification) specification,
                   a.batch_num                                              batch_num
            FROM product_outward_order a
                     LEFT JOIN plan b ON a.plan_code = b.plan_code
                     LEFT JOIN contact_company c ON b.contact_company_id = c.id
            WHERE a.state = 1
              AND ifnull(a.is_send_directly, 0) = 0
              AND ifnull(a.is_finished_product, 0) = 1
        )
        union all
        # 发货管理
        (SELECT a.client                                                       clientName,
               c.plan_code                                                     planCode,
               c.product_name                                                  productName,
               -ifnull(a.ps, 0)                                                ps,
               -ifnull(a.qty, 0)                                               qty,
               ifnull(a.price, 0)                                              price,
               round(ifnull(a.price, 0) * -ifnull(a.qty, 0), 2)                 money,
               CONCAT_WS(' ', c.order_code, c.product_name, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                         c.greyfabric_specification, c.material_specification) specification,
               c.material_lot_no                                               batch_num
        FROM whitebillfh a
#                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON a.planid = c.id)
                ) rep
        left join contact_company co on rep.clientName = co.company_name
        left join plan p on rep.planCode = p.plan_code
GROUP BY rep.clientName,co.id order by co.id ) re where abs(ifnull(re.ps,0))>0; "), concat("
    select re.*,p.material_lot_no batchNum  from (
    SELECT any_value(rep.clientName) client,
       rep.planCode,
       any_value(rep.productName) productName,
       sum(rep.ps) ps,
       sum(rep.qty) qty,
       avg(rep.price) price,
       sum(rep.money) money,
       any_value(rep.specification) specification
FROM ((SELECT c.company_name                                                              clientName,
              b.plan_code                                                                 planCode,
              b.product_name                                                              productName,
              ifnull(a.ps, 0)                                                             ps,
              ifnull(a.qty, 0)                                                            qty,
              ifnull(b.greyfabric_price, 0)                                               price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)                  money,
              CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification) specification,
              a.batch_num
       FROM jrkbillsum a
                LEFT JOIN plan b ON a.planid = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

      UNION ALL

      #   收款调整
      (SELECT c.company_name                                                                    clientName,
              b.plan_code                                                                       planCode,
              b.product_name                                                                    productName,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)                              ps,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)                                qty,
              ifnull(a.price, 0)                                                                price,
              round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2) money,
              CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification)       specification,
              null      batch_num
       FROM jrkbill_adjust a
                LEFT JOIN plan b ON a.plan_id = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)
       union all
        (
            SELECT c.company_name                                           clientName,
                   a.plan_code                                              planCode,
                   b.product_name                                           productName,
                   ifnull(a.input_pairs, 0)                                 ps,
                   ifnull(a.input_kilo, 0)                                  qty,
                   ifnull(a.processing_cost, 0)                             price,
                   round(a.money, 2)                                        money,
                   CONCAT_WS('' '', a.order_code, a.greyfabric_specification) specification,
                   a.batch_num
            FROM product_outward_order a
                     LEFT JOIN plan b ON a.plan_code = b.plan_code
                     LEFT JOIN contact_company c ON b.contact_company_id = c.id
            WHERE a.state = 1
              AND ifnull(a.is_send_directly, 0) = 0
              AND ifnull(a.is_finished_product, 0) = 1
        )
       union all
       # 发货管理
        (SELECT a.client                                                        clientName,
               c.plan_code                                                     planCode,
               c.product_name                                                  productName,
               -ifnull(a.ps, 0)                                                ps,
               -ifnull(a.qty, 0)                                               qty,
               ifnull(a.price, 0)                                              price,
               round(ifnull(a.price, 0) * -ifnull(a.qty, 0), 2)                 money,
               CONCAT_WS(' ', c.order_code, c.product_name, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                         c.greyfabric_specification, c.material_specification) specification,
               c.material_lot_no                                               batch_num
        FROM whitebillfh a
#                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON a.planid = c.id)
                ) rep
        left join contact_company co on rep.clientName = co.company_name
GROUP BY rep.planCode ) re
left join plan p on re.planCode = p.plan_code
where abs(ifnull(re.ps,0))>0; "));
    PREPARE stat FROM @baseSql;
    EXECUTE stat;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_unfinished_outward
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_unfinished_outward`;
DELIMITER ;;
CREATE PROCEDURE `p_unfinished_outward`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100))
BEGIN
    /**
      2021年7月12日 未剖幅报表
     */
    SELECT a.id,
           d.company_name                                  client,
           a.order_date                                    order_date,
           a.process_company                               process_company,
           a.order_code                                    order_code,
           a.plan_code                                     plan_code,
           a.material_specification                        material_specification,
           a.greyfabric_specification                      greyfabric_specification,
           ifnull(a.box_amount, 0)                         box_amount,
           ifnull(a.input_kilo, 0)                         input_kilo,
           a.consignee                                     consignee,
           a.handler_name                                  handler_name,
           b.user_name                                     create_user_name,
           if(ifnull(a.is_send_directly, 0) = 0, '否', '是') is_send_directly,
           a.batch_num                                     batch_num,
           a.remarks                                       remarks
    FROM product_outward_order a
             LEFT JOIN sys_user b ON a.create_user_id = b.id
             LEFT JOIN plan c ON a.plan_code = c.plan_code
             LEFT JOIN contact_company d ON c.contact_company_id = d.id
    WHERE ifnull(a.state, 0) = 1
      AND ifnull(a.is_finished_product, 0) = 0
      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, d.company_name = `clientName`)
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.create_time DESC;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
