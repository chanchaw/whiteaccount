SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `p_product_inbound_report`;
DELIMITER ;;
CREATE PROCEDURE `p_product_inbound_report`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100))
BEGIN
    /**
     2021年7月2日  原料外发入库序时表
     2021年7月17日 追加字段
     2021年7月20日 按主键倒排序
     */
    SELECT a.id                                        id,
           if(a.is_finished_product = 0, '未剖幅', '已剖幅') is_finished_product,
           a.order_code                                order_code,
           a.process_company                           process_company,
           a.material_specification                    material_spec,
           ifnull(a.input_kilo, 0)                     input_kilo,
           ifnull(a.input_pairs, 0)                    input_pairs,
           ifnull(a.box_amount, 0)                     box_amount,
           a.supplier_name                             supplier_name,
           a.order_date                                order_date,
           a.create_time                               create_time,
           a.handler_name                              handler_name,    -- 经手人
           c.user_name                                 create_user_name,
           round(ifnull(a.processing_cost, 0), 2)      processing_cost, -- 加工费
           round(ifnull(a.money, 0), 2)                money,           -- 金额
           a.remarks                                   remarks
    FROM product_outward_order a
             LEFT JOIN plan b ON a.plan_code = b.plan_code
             LEFT JOIN sys_user c ON a.create_user_id = c.id
    WHERE a.state = 1
      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, a.supplier_name = `clientName`)
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.id DESC;
END
;;
DELIMITER ;
SET FOREIGN_KEY_CHECKS = 1;