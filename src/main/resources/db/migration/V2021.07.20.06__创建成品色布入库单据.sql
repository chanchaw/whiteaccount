/**
  2021年7月20日 创建成品色布入库单据
 */
create table dye_fabric_inbound
(
    id BIGINT auto_increment comment '自增主键',
    order_date DATETIME null comment '单据日期',
    order_code VARCHAR(100) not null comment '单据编号',
    dye_company VARCHAR(100) null comment '染厂',
    dye_plan_code VARCHAR(100) null comment '计划单号',
    process_type VARCHAR(100) null comment '加工类型',
    contact_company_id VARCHAR(50) null comment '外键,往来单位',
    net_diff DECIMAL(30,1) null comment '毛净差',
    rate DECIMAL(30,1) null comment '米克系数',
    is_send_directly TINYINT(1) default 0 null comment '是否直发(0:否;1:是;)',
    specification VARCHAR(255) null comment '色布规格',
    input_pairs DECIMAL(60,1) null comment '匹数',
    input_kilo DECIMAL(60,1) null comment '毛重',
    net_kilo DECIMAL(60,1) null comment '净重',
    input_meter DECIMAL(60,1) null comment '米数',
    handler_name VARCHAR(100) null comment '经手人',
    price DECIMAL(60,2) null comment '单价',
    money DECIMAL(60,2) null comment '金额',
    create_user_id int null comment '创建人编号',
    upload int default 0 null comment '上传标记',
    is_push TINYINT(1) default 0 null comment '是否推送(0:是;1:否;)',
    remarks VARCHAR(255) null comment '备注',
    create_time DATETIME default CURRENT_TIMESTAMP null comment '创建日期',
    state int default 1 null comment '单据状态(0:作废;1:正常;)',
    constraint dye_fabric_inbound_pk
        primary key (id)
)
    comment '色布入库单据';

create index dye_fabric_inbound_contact_company_id_index
    on dye_fabric_inbound (contact_company_id);

create index dye_fabric_inbound_create_user_id_index
    on dye_fabric_inbound (create_user_id);

create index dye_fabric_inbound_dye_plan_code_index
    on dye_fabric_inbound (dye_plan_code);

create index dye_fabric_inbound_order_code_index
    on dye_fabric_inbound (order_code);

create unique index dye_fabric_inbound_order_code_uindex
    on dye_fabric_inbound (order_code);