/**
  2021年7月21日 关联单据编号
 */
alter table dye_fabric_inbound
    add linked_order_code VARCHAR(100) null comment '关联单据编号' after create_time;