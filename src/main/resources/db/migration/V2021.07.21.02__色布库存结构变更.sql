SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_dye_fabric_plan_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_dye_fabric_plan_report`;
DELIMITER ;;
CREATE PROCEDURE `p_dye_fabric_plan_report`(IN `start` DATE, IN `end` DATE, IN `markFinished` TINYINT(1))
BEGIN
    /**
      2021年6月24日 色布计划序时表
      2021年6月27日 按照日期到排序
      2021年6月28日 追加入库出库数量
      2021年7月21日 更改逻辑
     */
    SELECT a.id                                                            id,
           DATE(a.plan_date)                                               plan_date,
           a.contact_company_id                                            contact_company_id,
           b.company_name                                                  company_name,
           a.contract_code                                                 contract_code,
           a.plan_code                                                     plan_code,
           a.order_code                                                    order_code,
           a.product_name                                                  product_name,
           a.process_type                                                  process_type,
           a.material_specification                                        material_specification,
           a.color_no                                                      material_specification,
           a.color                                                         color,
           ifnull(a.net_diff, 0)                                           net_diff,
           ifnull(a.plan_kilo, 0)                                          plan_kilo,
           ifnull(a.rate, 0)                                               rate,
           ifnull(a.material_price, 0)                                     material_price,
           a.plan_employee_id                                              plan_employee_id,
           c.emp_name                                                      employee_name,
           DATE(a.submit_date)                                             submit_date,
           a.create_user_id                                                create_user_id,
           d.user_name                                                     create_user_name,
           a.remarks                                                       remarks,
           ifnull(e.pairs, 0) + ifnull(x.input_pairs, 0)                   input_pairs,
           ifnull(f.ps, 0) + ifnull(g.pairs, 0) + ifnull(y.input_pairs, 0) delivery_pairs,
           ifnull(e.pairs, 0) + ifnull(x.input_pairs, 0) - ifnull(y.input_pairs, 0) - ifnull(f.ps, 0) -
           ifnull(g.pairs, 0)                                              remain_pairs,
           ifnull(e.qty, 0) + ifnull(x.input_kilo, 0)                      input_kilo,
           ifnull(f.qty, 0) + ifnull(g.qty, 0) + ifnull(y.input_kilo, 0)   delivery_kilo,
           ifnull(e.qty, 0) + ifnull(x.input_kilo, 0) - ifnull(y.input_kilo, 0) - ifnull(f.qty, 0) -
           ifnull(g.qty, 0)                                                remain_kilo,
           ifnull(a.mark_finished, 0)                                      mark_finished
    FROM dye_fabric_plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN employee c ON a.plan_employee_id = c.id
             LEFT JOIN sys_user d ON a.create_user_id = d.id
             LEFT JOIN (
        #         调整入库
        SELECT sum(pairs) pairs, sum(qty) qty, plan_id
        FROM cp_adjust
        WHERE bill_type IN ('手动入库', '盘点')
        GROUP BY plan_id) e ON e.plan_id = a.id
             LEFT JOIN (
        #      色布入库
        SELECT dye_plan_code, sum(input_kilo) input_kilo, sum(input_pairs) input_pairs, sum(input_meter) input_meter
        FROM dye_fabric_inbound
        WHERE state = 1
          AND isnull(linked_order_code)
        GROUP BY dye_plan_code) x ON x.dye_plan_code = a.plan_code
             LEFT JOIN (
        #         出库
        SELECT sum(ps) ps, sum(qty) qty, planid FROM cp_billfh GROUP BY planId) f ON f.planId = a.id
             LEFT JOIN (
        #             色布出库
        SELECT dye_plan_code, sum(input_kilo) input_kilo, sum(input_pairs) input_pairs, sum(input_meter) input_meter
        FROM dye_fabric_inbound
        WHERE state = 1
          AND length(ifnull(linked_order_code, '')) > 0
        GROUP BY dye_plan_code) y ON y.dye_plan_code = a.plan_code
             LEFT JOIN (
        #         出库调整
        SELECT sum(pairs) pairs, sum(qty) qty, plan_id FROM cp_adjust WHERE bill_type = '手动出库' GROUP BY plan_id) g
                       ON g.plan_id = a.id
    WHERE a.state = 1
      AND if(isnull(`markFinished`), 1 = 1, ifnull(a.mark_finished, 0) = `markFinished`)
      AND DATE(a.plan_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.id DESC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_dye_inbound_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_dye_inbound_report`;
DELIMITER ;;
CREATE PROCEDURE `p_dye_inbound_report`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100))
BEGIN
    /**
      2021年6月27日 色布入库序时表
     */
    IF ((isnull(`clientName`) OR length(`clientName`) <= 0) OR `clientName` = '全部') THEN
        SELECT NULL               id,
               c.company_name     client_name,
               b.plan_code,
               b.product_name,
               b.material_specification,
               b.color_no,
               b.color,
               ifnull(a.pairs, 0) ps,
               ifnull(a.qty, 0)   qty,
               a.client_name      supplier,
               a.bill_date,
               a.remarks,
               NULL               is_send_directly
        FROM cp_adjust a
                 LEFT JOIN dye_fabric_plan b ON a.plan_id = b.id
                 LEFT JOIN contact_company c ON b.contact_company_id = c.id
        WHERE a.bill_type IN ('手动入库', '盘点')
          AND DATE(a.bill_date) BETWEEN DATE(`start`) AND DATE(`end`)
        UNION ALL
        #      色布入库
        SELECT a.id                                 id,
               b.company_name                       client_name,
               a.dye_plan_code                      plan_code,
               c.product_name,
               c.material_specification,
               c.color_no,
               c.color,
               ifnull(a.input_pairs, 0)             ps,
               ifnull(a.input_kilo, 0)              qty,
               NULL                                 supplier,
               a.order_date,
               a.remarks,
               if(a.is_send_directly = 1, '是', '否') is_send_directly
        FROM dye_fabric_inbound a
                 LEFT JOIN contact_company b ON a.contact_company_id = b.id
                 LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
        WHERE a.state = 1
          AND isnull(a.linked_order_code)
          AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`);
    ELSE
        SELECT NULL               id,
               c.company_name     client_name,
               b.plan_code,
               b.product_name,
               b.material_specification,
               b.color_no,
               b.color,
               ifnull(a.pairs, 0) ps,
               ifnull(a.qty, 0)   qty,
               a.client_name      supplier,
               a.bill_date,
               a.remarks,
               NULL               is_send_directly
        FROM cp_adjust a
                 LEFT JOIN dye_fabric_plan b ON a.plan_id = b.id
                 LEFT JOIN contact_company c ON b.contact_company_id = c.id
        WHERE a.bill_type IN ('手动入库', '盘点')
          AND c.company_name = `clientName`
          AND DATE(a.bill_date) BETWEEN DATE(`start`) AND DATE(`end`)
        UNION ALL
        #      色布入库
        SELECT a.id                                 id,
               b.company_name                       client_name,
               a.dye_plan_code                      plan_code,
               c.product_name,
               c.material_specification,
               c.color_no,
               c.color,
               ifnull(a.input_pairs, 0)             ps,
               ifnull(a.input_kilo, 0)              qty,
               NULL                                 supplier,
               a.order_date,
               a.remarks,
               if(a.is_send_directly = 1, '是', '否') is_send_directly
        FROM dye_fabric_inbound a
                 LEFT JOIN contact_company b ON a.contact_company_id = b.id
                 LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
        WHERE a.state = 1
          AND isnull(a.linked_order_code)
          AND b.company_name = `clientName`
          AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`);
    END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_dye_storage_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_dye_storage_report`;
DELIMITER ;;
CREATE PROCEDURE `p_dye_storage_report`(IN `clientName` VARCHAR(100), IN `value` INT)
BEGIN
    /**
      2021年6月27日 色布库存表
     */
    IF (ifnull(`value`, 0) = 1) THEN
        #         汇总
        SELECT rep.*
        FROM (SELECT b.company_name                  client_name,
                     sum(ifnull(c.pairs, 0) + ifnull(x.input_pairs, 0) - ifnull(y.input_pairs, 0) - ifnull(d.ps, 0) -
                         ifnull(e.pairs, 0))         ps,
                     sum(round(ifnull(c.qty, 0) + ifnull(x.input_kilo, 0) - ifnull(y.input_kilo, 0) - ifnull(d.qty, 0) -
                               ifnull(e.qty, 0), 1)) qty
              FROM dye_fabric_plan a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
                       LEFT JOIN (
                  #         入库
                  SELECT sum(pairs) pairs, sum(qty) qty, plan_id
                  FROM cp_adjust
                  WHERE bill_type IN ('手动入库', '盘点')
                  GROUP BY plan_id) c ON c.plan_id = a.id
                       LEFT JOIN (
                  #         出库
                  SELECT sum(ps) ps, sum(qty) qty, planid FROM cp_billfh WHERE planid = `planid` GROUP BY planId) d
                                 ON d.planId = a.id
                       LEFT JOIN (
                  #         出库调整
                  SELECT sum(pairs) pairs, sum(qty) qty, plan_id
                  FROM cp_adjust
                  WHERE bill_type = '手动出库'
                  GROUP BY plan_id) e ON e.plan_id = a.id
                       LEFT JOIN (
                  #      色布入库(非直发)
                  SELECT dye_plan_code,
                         sum(input_kilo)  input_kilo,
                         sum(input_pairs) input_pairs,
                         sum(input_meter) input_meter
                  FROM dye_fabric_inbound
                  WHERE state = 1
                    AND isnull(linked_order_code)
                  GROUP BY dye_plan_code) x ON x.dye_plan_code = a.plan_code
                       LEFT JOIN (
                  #             色布出库
                  SELECT dye_plan_code,
                         sum(input_kilo)  input_kilo,
                         sum(input_pairs) input_pairs,
                         sum(input_meter) input_meter
                  FROM dye_fabric_inbound
                  WHERE state = 1
                    AND length(ifnull(linked_order_code, '')) > 0
                  GROUP BY dye_plan_code) y ON y.dye_plan_code = a.plan_code
              WHERE a.state = 1
                AND if(ifnull(`clientName`, '') IN ('', '全部'), 1 = 1, b.company_name = ifnull(`clientName`, ''))
              GROUP BY a.contact_company_id) rep
        WHERE abs(ifnull(rep.ps, 0)) > 0;
    ELSE
        #         明细
        SELECT b.company_name             client_name,
               a.plan_code,
               a.product_name,
               a.material_specification,
               a.color_no,
               a.color,
               ifnull(c.pairs, 0) + ifnull(x.input_pairs, 0) - ifnull(y.input_pairs, 0) - ifnull(d.ps, 0) -
               ifnull(e.pairs, 0)         ps,
               round(ifnull(c.qty, 0) + ifnull(x.input_kilo, 0) - ifnull(y.input_kilo, 0) - ifnull(d.qty, 0) -
                     ifnull(e.qty, 0), 1) qty
        FROM dye_fabric_plan a
                 LEFT JOIN contact_company b ON a.contact_company_id = b.id
                 LEFT JOIN (
            #         入库
            SELECT sum(pairs) pairs, sum(qty) qty, plan_id
            FROM cp_adjust
            WHERE bill_type IN ('手动入库', '盘点')
            GROUP BY plan_id) c ON c.plan_id = a.id
                 LEFT JOIN (
            #         出库
            SELECT sum(ps) ps, sum(qty) qty, planid FROM cp_billfh WHERE planid = `planid` GROUP BY planId) d
                           ON d.planId = a.id
                 LEFT JOIN (
            #         出库调整
            SELECT sum(pairs) pairs, sum(qty) qty, plan_id FROM cp_adjust WHERE bill_type = '手动出库' GROUP BY plan_id) e
                           ON e.plan_id = a.id
                 LEFT JOIN (
            #      色布入库(非直发)
            SELECT dye_plan_code, sum(input_kilo) input_kilo, sum(input_pairs) input_pairs, sum(input_meter) input_meter
            FROM dye_fabric_inbound
            WHERE state = 1
              AND isnull(linked_order_code)
            GROUP BY dye_plan_code) x ON x.dye_plan_code = a.plan_code
                 LEFT JOIN (
            #             色布出库
            SELECT dye_plan_code, sum(input_kilo) input_kilo, sum(input_pairs) input_pairs, sum(input_meter) input_meter
            FROM dye_fabric_inbound
            WHERE state = 1
              AND length(ifnull(linked_order_code, '')) > 0
            GROUP BY dye_plan_code) y ON y.dye_plan_code = a.plan_code
        WHERE a.state = 1
          AND abs(ifnull(c.pairs, 0) + ifnull(x.input_pairs, 0) - ifnull(y.input_pairs, 0) - ifnull(d.ps, 0) -
                  ifnull(e.pairs, 0)) > 0
          AND if(ifnull(`clientName`, '') IN ('', '全部'), 1 = 1, b.company_name = ifnull(`clientName`, ''));
    END IF;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;