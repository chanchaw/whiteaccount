SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_dye_fh_dtl
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_dye_fh_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_dye_fh_dtl`(IN id BIGINT)
BEGIN
    /**
       2021年6月27日 色布发货明细
     */
    SELECT r.*
    FROM ((SELECT a.client                             client_name,
                  a.billcode                           billcode,
                  ifnull(a.countable, 0)               countable,
                  ifnull(sum(a.ps), 0)                 send_pairs,
                  sum(round(a.qty, 1))                 send_kilo,
                  d.material_specification             material_specification,
                  date_format(a.datafh, '%m-%d %H:%i') send_date,
                  a.memo                               remarks,
                  a.datafh                             order_date
           FROM cp_billfh a
                    LEFT JOIN dye_fabric_plan d ON a.planid = d.id
           WHERE a.planid = `id`
             AND a.id IS NOT NULL
           GROUP BY a.id)
          UNION ALL
          (SELECT c.company_name                          client_name,
                  b.bill_type                             billcode,
                  ifnull(b.countable, 0)                  countable,
                  ifnull(b.pairs, 0)                      send_pairs,
                  round(b.qty, 1)                         send_kilo,
                  a.material_specification                material_specification,
                  date_format(b.bill_date, '%m-%d %H:%i') send_date,
                  b.remarks                               remarks,
                  b.bill_date                             order_date
           FROM dye_fabric_plan a
                    LEFT JOIN cp_adjust b ON a.id = b.plan_id
                    LEFT JOIN contact_company c ON a.contact_company_id = c.id
           WHERE b.plan_id = `id`
             AND b.bill_type = '手动出库'
             AND b.id IS NOT NULL)
          UNION ALL
          ( #             色布出库
              SELECT b.company_name                           client_name,
                     a.order_code                             billcode,
                     NULL                                     countable,
                     ifnull(a.input_pairs, 0)                 send_pairs,
                     round(a.input_kilo, 1)                   send_kilo,
                     a.specification                          material_specification,
                     date_format(a.order_date, '%m-%d %H:%i') send_date,
                     b.remarks                                remarks,
                     a.order_date                             order_date
              FROM dye_fabric_inbound a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
                       LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
              WHERE a.state = 1
                AND c.id = `id`
                AND length(ifnull(a.linked_order_code, '')) > 0)) r
    ORDER BY r.order_date DESC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_dye_inbound_dtl
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_dye_inbound_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_dye_inbound_dtl`(IN id BIGINT)
BEGIN
    /**
       2021年6月27日 色布入库明细
     */
    SELECT *
    FROM ((SELECT b.id                                    id,
                  ifnull(b.pairs, 0)                      input_pairs,
                  ifnull(round(b.qty, 1), 0)              input_kilo,
                  NULL                                    aorb,
                  a.material_specification                material_specification,
                  b.bill_type                             computer_name,
                  NULL                                    machine_type,
                  ifnull(b.countable, 0)                  countable,
                  date_format(b.bill_date, '%m-%d %H:%i') createdate,
                  b.remarks                               remarks,
                  b.bill_type                             bill_type,
                  b.bill_date                             order_date
           FROM dye_fabric_plan a
                    LEFT JOIN cp_adjust b ON a.id = b.plan_id
           WHERE a.id = `id`
             AND b.bill_type IN ('手动入库', '盘点')
             AND b.id IS NOT NULL)
          UNION ALL
          ( #             色布入库
              SELECT NULL                                     id,
                     ifnull(a.input_pairs, 0)                 input_pairs,
                     ifnull(round(a.input_kilo, 1), 0)        input_kilo,
                     NULL                                     aorb,
                     a.specification                          material_specification,
                     '色布入库'                                   computer_name,
                     NULL                                     machine_type,
                     NULL                                     countable,
                     date_format(a.order_date, '%m-%d %H:%i') createdate,
                     b.remarks                                remarks,
                     '色布入库'                                   bill_type,
                     a.order_date                             order_date
              FROM dye_fabric_inbound a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
                       LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
              WHERE a.state = 1
                AND c.id = `id`
                AND length(ifnull(a.linked_order_code, '')) <= 0)) r
    ORDER BY r.order_date DESC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_dye_plan_dtl
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_dye_plan_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_dye_plan_dtl`(IN `p_planId` BIGINT)
BEGIN
    /**
     2021年6月27日  色布计划明细
     */
    SELECT a.id                                                                                         id,
           b.company_name                                                                               company_name,
           concat_ws(' ', ifnull(a.contract_code, ''), ifnull(a.plan_code, ''), ifnull(a.product_name, ''),
                     ifnull(a.material_specification, ''), ifnull(a.color_no, ''), ifnull(a.color, '')) product_name,
           ifnull(a.plan_kilo, 0)                                                                       plan_kilo,
           ifnull(c.pairs, 0) + ifnull(x.input_pairs, 0)                                                input_pairs,
           ifnull(c.qty, 0) + ifnull(x.input_kilo, 0)                                                   input_kilo,
           ifnull(d.qty, 0) + ifnull(e.qty, 0) + ifnull(y.input_kilo, 0)                                send_kilo,
           ifnull(d.ps, 0) + ifnull(e.pairs, 0) + ifnull(y.input_pairs, 0)                              send_pairs,
           ifnull(round(ifnull(a.plan_kilo, 0) - ifnull(d.qty, 0) - ifnull(e.qty, 0) - ifnull(y.input_kilo, 0), 1),
                  0)                                                                                    plan_remain_kilo,
           ifnull(round(ifnull(c.qty, 0) + ifnull(x.input_kilo, 0) - ifnull(d.qty, 0) - ifnull(e.qty, 0), 1) -
                  ifnull(y.input_kilo, 0), 0)                                                           remain_kilo,
           ifnull(round(ifnull(c.pairs, 0) + ifnull(x.input_pairs, 0) - ifnull(d.ps, 0) - ifnull(e.pairs, 0) -
                        ifnull(y.input_pairs, 0), 1), 0)                                                remain_pairs,
           ifnull(a.material_price, 0)                                                                  price
    FROM dye_fabric_plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN (
        #         入库
        SELECT sum(pairs) pairs, sum(qty) qty, plan_id
        FROM cp_adjust
        WHERE plan_id = `p_planId`
          AND bill_type IN ('手动入库', '盘点')
        GROUP BY plan_id) c ON c.plan_id = a.id
             LEFT JOIN (
        #         出库
        SELECT sum(ps) ps, sum(qty) qty, planid FROM cp_billfh WHERE planid = `p_planId` GROUP BY planId) d
                       ON d.planId = a.id
             LEFT JOIN (
        #         出库调整
        SELECT sum(pairs) pairs, sum(qty) qty, plan_id
        FROM cp_adjust
        WHERE plan_id = `p_planId`
          AND bill_type = '手动出库'
        GROUP BY plan_id) e ON e.plan_id = a.id
             LEFT JOIN (
        #      色布入库
        SELECT a.dye_plan_code,
               sum(a.input_kilo)  input_kilo,
               sum(a.input_pairs) input_pairs,
               sum(a.input_meter) input_meter
        FROM dye_fabric_inbound a
                 LEFT JOIN dye_fabric_plan b ON a.dye_plan_code = b.plan_code
        WHERE a.state = 1
          AND isnull(a.linked_order_code)
          AND b.id = `p_planId`
        GROUP BY a.dye_plan_code) x ON x.dye_plan_code = a.plan_code
             LEFT JOIN (
        #             色布出库
        SELECT a.dye_plan_code,
               sum(a.input_kilo)  input_kilo,
               sum(a.input_pairs) input_pairs,
               sum(a.input_meter) input_meter
        FROM dye_fabric_inbound a
                 LEFT JOIN dye_fabric_plan b ON a.dye_plan_code = b.plan_code
        WHERE a.state = 1
          AND b.id = `p_planId`
          AND length(ifnull(a.linked_order_code, '')) > 0
        GROUP BY a.dye_plan_code) y ON y.dye_plan_code = a.plan_code
    WHERE a.id = `p_planId`;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_dye_plan_storage_modify
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_dye_plan_storage_modify`;
DELIMITER ;;
CREATE PROCEDURE `p_dye_plan_storage_modify`(IN id BIGINT)
BEGIN
    /**
      2021年6月27日  库存调整存储过程
     */
    SELECT *
    FROM (SELECT report.*,
                 @storage_ps := ifnull(report.pairs, 0) + @storage_ps  remain_pairs,
                 @storage_qty := ifnull(report.kilo, 0) + @storage_qty remain_kilo
          FROM (
                   --          手动调整
                   (SELECT b.id                                                              id,
                           a.id                                                              plan_id,
                           date_format(b.bill_date, '%m-%d %H:%i')                           jrkbill_date,
                           ifnull(b.bill_type, '')                                           bill_type,
                           if(b.bill_type = '手动出库', -ifnull(b.pairs, 0), ifnull(b.pairs, 0)) pairs,
                           if(b.bill_type = '手动出库', -ifnull(b.qty, 0), ifnull(b.qty, 0))     kilo,
                           b.remarks                                                         remarks,
                           b.client_name                                                     client_name,
                           b.countable                                                       countable,
                           b.price                                                           price,
                           b.bill_date                                                       order_date
                    FROM dye_fabric_plan a
                             LEFT JOIN cp_adjust b ON a.id = b.plan_id AND b.state = 1
                    WHERE a.id = `id`
                      AND b.id IS NOT NULL)

                   UNION ALL
                   --          发货
                   (SELECT NULL                                 id,
                           a.id                                 plan_id,
                           date_format(b.datafh, '%m-%d %H:%i') jrkbill_date,
                           '发货'                                 bill_type,
                           -ifnull(b.ps, 0)                     pairs,
                           -ifnull(b.qty, 0)                    kilo,
                           b.memo                               remarks,
                           b.client                             client_name,
                           b.countable                          countable,
                           b.price                              price,
                           b.datafh                             order_date
                    FROM dye_fabric_plan a
                             LEFT JOIN cp_billfh b ON a.id = b.planid
                    WHERE a.id = `id`
                      AND b.id IS NOT NULL)
                   UNION ALL
                   (
                       --     色布入库
                       SELECT NULL                                     id,
                              b.id                                     plan_id,
                              date_format(a.order_date, '%m-%d %H:%i') jrkbill_date,
                              '色布入库'                                   bill_type,
                              ifnull(a.input_pairs, 0)                 pairs,
                              ifnull(a.input_kilo, 0)                  kilo,
                              a.remarks                                remarks,
                              c.company_name                           client_name,
                              NULL                                     countable,
                              a.price                                  price,
                              a.order_date                             order_date
                       FROM dye_fabric_inbound a
                                LEFT JOIN dye_fabric_plan b ON a.dye_plan_code = b.plan_code
                                LEFT JOIN contact_company c ON b.contact_company_id = c.id
                       WHERE a.state = 1
                         AND b.id = `id`
                         AND isnull(a.linked_order_code))
                   UNION ALL
                   (
                       --           色布出库
                       SELECT NULL                                     id,
                              b.id                                     plan_id,
                              date_format(a.order_date, '%m-%d %H:%i') jrkbill_date,
                              '色布出库'                                   bill_type,
                              -ifnull(a.input_pairs, 0)                pairs,
                              -ifnull(a.input_kilo, 0)                 kilo,
                              a.remarks                                remarks,
                              c.company_name                           client_name,
                              NULL                                     countable,
                              a.price                                  price,
                              a.order_date                             order_date
                       FROM dye_fabric_inbound a
                                LEFT JOIN dye_fabric_plan b ON a.dye_plan_code = b.plan_code
                                LEFT JOIN contact_company c ON b.contact_company_id = c.id
                       WHERE a.state = 1
                         AND b.id = `id`
                         AND length(ifnull(a.linked_order_code, '')) > 0)) report,
               (SELECT @storage_ps := 0) p,
               (SELECT @storage_qty := 0) q
          ORDER BY report.order_date) r
    ORDER BY r.order_date DESC;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
