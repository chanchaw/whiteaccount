SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_dye_fabric_delivery
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_dye_fabric_delivery`;
DELIMITER ;;
CREATE PROCEDURE `p_dye_fabric_delivery`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100),
                                         IN `checkout` VARCHAR(100))
BEGIN
    /**
        2021年6月25日 色布发货序时表
        2021年7月21日 追加色布出库
     */
    (SELECT c.company_name           client,
            concat_ws(' ', b.contract_code, b.plan_code, b.product_name, b.material_specification, b.color_no,
                      b.color)       specification,
            ifnull(a.input_pairs, 0) ps,
            ifnull(a.input_kilo, 0)  qty,
            NULL                     countable,
            ifnull(a.price, 0)       price,
            a.money                  money,
            a.dye_company            checkout,
            a.order_code             billcode,
            DATE(a.order_date)       datafh,
            NULL                     carnumber,
            a.remarks                memo
     FROM dye_fabric_inbound a
              LEFT JOIN dye_fabric_plan b ON a.dye_plan_code = b.plan_code
              LEFT JOIN contact_company c ON b.contact_company_id = c.id
     WHERE a.state = 1
       AND IF(isnull(`clientName`) OR length(`clientName`) <= 0, TRUE, c.company_name = `clientName`)
       AND IF(isnull(`checkout`) OR length(`checkout`) <= 0, TRUE, ifnull(a.dye_company, '') = `checkout`)
       AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
       AND length(ifnull(a.linked_order_code, '')) > 0)
    UNION ALL
    (SELECT a.client                   client,
            concat_ws(' ', b.contract_code, b.plan_code, b.product_name, b.material_specification, b.color_no,
                      b.color)         specification,
            ifnull(a.ps, 0)            ps,
            ifnull(a.qty, 0)           qty,
            ifnull(a.countable, 0)     countable,
            ifnull(a.price, 0)         price,
            ifnull(a.price, 0) * a.qty money,
            a.checkout                 checkout,
            a.billcode                 billcode,
            DATE(a.datafh)             datafh,
            a.carnumber                carnumber,
            a.memo                     memo
     FROM cp_billfh a
              LEFT JOIN dye_fabric_plan b ON a.planid = b.id
     WHERE IF(isnull(`clientName`) OR length(`clientName`) <= 0, TRUE, a.client = `clientName`)
       AND IF(isnull(`checkout`) OR length(`checkout`) <= 0, TRUE, ifnull(a.checkout, '') = `checkout`)
       AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`));
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
