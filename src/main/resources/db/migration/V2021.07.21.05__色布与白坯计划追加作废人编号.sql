/**
  2021年7月21日 色布与白坯计划追加作废人编号
 */
alter table plan
    add cancel_user_id int null comment '作废人.编号' after create_user_id;
create index plan_cancel_user_id_index
    on plan (cancel_user_id);
alter table dye_fabric_plan
    add cancel_user_id int null comment '作废人.编号';
create index dye_fabric_plan_cancel_user_id_index
    on dye_fabric_plan (cancel_user_id);