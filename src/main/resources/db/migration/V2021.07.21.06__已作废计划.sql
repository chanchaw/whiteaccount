DROP PROCEDURE IF EXISTS `p_canceled_plan`;
DELIMITER ;;
CREATE PROCEDURE `p_canceled_plan`()
BEGIN
    SELECT a.id                                         id,
           a.plan_code                                  planCode,
           b.company_name                               companyName,
           concat(ifnull(a.product_name, ''), ' ', a.greyfabric_weight, '*', a.greyfabric_width, ' ',
                  ifnull(a.m_height, 0), ' ', ifnull(a.greyfabric_specification, ''), ' ',
                  ifnull(a.material_specification, '')) materialSpecification,
           a.plan_kilo                                  planKilo,
           c.user_name                                  cancelUserName
    FROM plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN sys_user c ON a.cancel_user_id = c.id
    WHERE a.state = 0;
END ;;
DELIMITER ;

DROP PROCEDURE IF EXISTS `p_canceled_dye_fabric_plan`;
DELIMITER ;;
CREATE PROCEDURE `p_canceled_dye_fabric_plan`()
BEGIN
    SELECT a.id                     id,
           a.plan_code              planCode,
           b.company_name           companyName,
           a.material_specification materialSpecification,
           a.plan_kilo              planKilo,
           c.user_name              cancelUserName
    FROM dye_fabric_plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN sys_user c ON a.cancel_user_id = c.id
    WHERE a.state = 0;
END ;;
DELIMITER ;