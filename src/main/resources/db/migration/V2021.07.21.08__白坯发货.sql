SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for getFHhistoryDetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `getFHhistoryDetail`;
DELIMITER ;;
CREATE PROCEDURE `getFHhistoryDetail`(m_SDate DATE,
                                      m_EDate DATE,
                                      m_Client VARCHAR(100),
                                      m_CheckType VARCHAR(100))
BEGIN
    SELECT NULL                                                                                   id,
           a.client,
           a.address,
           CONCAT_WS(' ', c.order_code, c.product_name, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight),
                     c.m_height, c.greyfabric_specification, a.aorb, c.material_specification) AS pmgg,
           a.ps,
           round(a.qty, 1)                                                                     AS qty,
           round(a.price, 2)                                                                   AS price,
           ROUND(round(a.price, 2) * round(a.qty, 1), 2)                                       AS money,
           a.checkout                                                                             checkout,
           a.billcode                                                                             billcode,
           c.material_lot_no                                                                      batch_num,
           NULL                                                                                   isSendDirectly,
           a.datafh                                                                            AS b_data,
           a.datafh                                                                               createTime,
           memo                                                                                   remarks,
           NULL                                                                                   createUserName,
           NULL                                                                                   handlerName
    FROM whitebillfh a
             LEFT OUTER JOIN plan c ON a.planid = c.id
    WHERE if(ifnull(m_Client, '') = '' OR ifnull(m_Client, '') = '全部', TRUE, a.client = `m_Client`)
      AND if(ifnull(m_CheckType, '') = '' OR ifnull(m_CheckType, '') = '全部', TRUE, a.checkout = `m_CheckType`)
      AND DATE_FORMAT(a.datafh, '%Y-%m-%d') BETWEEN DATE(m_SDate) AND DATE(m_EDate)

    UNION ALL

    SELECT a.id,
           c.company_name                                                                               client,
           a.consignee                                                                                  address,
           CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                     b.m_height, b.greyfabric_specification, a.ab_surface, b.material_specification) AS pmgg,
           ifnull(a.input_pairs, 0)                                                                     ps,
           round(a.input_kilo, 1)                                                                    AS qty,
           round(a.processing_cost, 2)                                                               AS price,
           round(a.money, 2)                                                                         AS money,
           if(a.is_finished_product = 1, '已剖幅', '未剖幅')                                                  checkout,
           a.order_code                                                                                 billcode,
           a.batch_num                                                                                  batch_num,
           if(a.is_send_directly = 1, '是', '否')                                                         isSendDirectly,
           a.order_date                                                                              AS b_data,
           a.create_time                                                                                createTime,
           a.remarks                                                                                    remarks,
           d.user_name                                                                                  createUserName,
           a.handler_name                                                                               handlerName
    FROM product_outward_order a
             LEFT JOIN plan b ON a.plan_code = b.plan_code
             LEFT JOIN contact_company c ON b.contact_company_id = c.id
             LEFT JOIN sys_user d ON a.create_user_id = d.id
    WHERE if(ifnull(m_Client, '') = '' OR ifnull(m_Client, '') = '全部', TRUE, c.company_name = `m_Client`)
      AND length(ifnull(a.linked_order_code, '')) > 0
      AND DATE_FORMAT(a.order_date, '%Y-%m-%d') BETWEEN DATE(`m_SDate`) AND DATE(`m_EDate`);
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
