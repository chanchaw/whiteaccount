/**
  2021年7月22日 白坯原料成品追加审核与审核人字段
 */
alter table dye_fabric_inbound
    add is_audit TINYINT(1) default 0 null comment '是否审核(0:否;1:是;)';
alter table dye_fabric_inbound
    add audit_user_id int null comment '审核人编号';
create index dye_fabric_inbound_audit_user_id_index
    on dye_fabric_inbound (audit_user_id);

alter table dye_fabric_plan
    add is_audit TINYINT(1) default 0 null comment '是否审核(0:否;1:是;)';
alter table dye_fabric_plan
    add audit_user_id int null comment '审核人编号';
create index dye_fabric_plan_audit_user_id_index
    on dye_fabric_plan (audit_user_id);

alter table material_order
    add is_audit TINYINT(1) default 0 null comment '是否审核(0:否;1:是;)';
alter table material_order
    add audit_user_id int null comment '审核人编号';
create index material_order_audit_user_id_index
    on material_order (audit_user_id);

alter table plan
    add is_audit TINYINT(1) default 0 null comment '是否审核(0:否;1:是;)';
alter table plan
    add audit_user_id int null comment '审核人编号';
create index plan_audit_user_id_index
    on plan (audit_user_id);

alter table product_outward_order
    add is_audit TINYINT(1) default 0 null comment '是否审核(0:否;1:是;)';
alter table product_outward_order
    add audit_user_id int null comment '审核人编号';
create index product_outward_order_audit_user_id_index
    on product_outward_order (audit_user_id);