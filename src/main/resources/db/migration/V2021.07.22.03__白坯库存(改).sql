SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_greyfabric_storage
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_greyfabric_storage`;
delimiter ;;
CREATE PROCEDURE `p_greyfabric_storage`(IN mode INT)
BEGIN
    /**
     2021年6月9日 白坯库存
     */
    SET @baseSql = if(mode = 1, concat("
    select re.* from (
    SELECT rep.clientName client,
       -- rep.planCode,
       -- rep.productName,
       sum(rep.ps) ps,
       sum(rep.qty) qty,
       -- avg(rep.price) price,
       p.material_lot_no  batchNum,
       sum(rep.money) money
       -- rep.specification
FROM ((SELECT c.company_name                                                              clientName,
              b.plan_code                                                                 planCode,
              b.product_name                                                              productName,
              ifnull(a.ps, 0)                                                             ps,
              ifnull(a.qty, 0)                                                            qty,
              ifnull(b.greyfabric_price, 0)                                               price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)                  money,
              CONCAT_WS(' ', b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification) specification,
              a.batch_num
       FROM jrkbillsum a
                LEFT JOIN plan b ON a.planid = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

      UNION ALL

      #   收款调整
      (SELECT c.company_name                                                                    clientName,
              b.plan_code                                                                       planCode,
              b.product_name                                                                    productName,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)                              ps,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)                                qty,
              ifnull(a.price, 0)                                                                price,
              round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2) money,
              CONCAT_WS(' ', b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification)       specification,
              NULL                                                                              batch_num
       FROM jrkbill_adjust a
                LEFT JOIN plan b ON a.plan_id = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

        union all
        #  白坯入库
        (
            SELECT c.company_name                                           clientName,
                   a.plan_code                                              planCode,
                   b.product_name                                           productName,
                   ifnull(a.input_pairs, 0)                                 ps,
                   ifnull(a.input_kilo, 0)                                  qty,
                   ifnull(a.processing_cost, 0)                             price,
                   round(a.money, 2)                                        money,
                   CONCAT_WS(' ', a.greyfabric_specification) specification,
                   a.batch_num                                              batch_num
            FROM product_outward_order a
                     LEFT JOIN plan b ON a.plan_code = b.plan_code
                     LEFT JOIN contact_company c ON b.contact_company_id = c.id
            WHERE a.state = 1
              AND ifnull(a.is_send_directly, 0) = 0
              AND ifnull(a.is_finished_product, 0) = 1
        )
        union all
        # 发货管理
        (SELECT a.client                                                       clientName,
               c.plan_code                                                     planCode,
               c.product_name                                                  productName,
               -ifnull(a.ps, 0)                                                ps,
               -ifnull(a.qty, 0)                                               qty,
               ifnull(a.price, 0)                                              price,
               round(ifnull(a.price, 0) * -ifnull(a.qty, 0), 2)                 money,
               CONCAT_WS(' ', c.product_name, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                         c.greyfabric_specification, c.material_specification) specification,
               c.material_lot_no                                               batch_num
        FROM whitebillfh a
#                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON a.planid = c.id)
                ) rep
        left join contact_company co on rep.clientName = co.company_name
        left join plan p on rep.planCode = p.plan_code
GROUP BY rep.clientName,co.id order by co.id ) re where abs(ifnull(re.ps,0))>0; "), concat("
    select re.*,p.material_lot_no batchNum  from (
    SELECT any_value(rep.clientName) client,
       rep.planCode,
       any_value(rep.productName) productName,
       sum(rep.ps) ps,
       sum(rep.qty) qty,
       avg(rep.price) price,
       sum(rep.money) money,
       any_value(rep.specification) specification
FROM ((SELECT c.company_name                                                              clientName,
              b.plan_code                                                                 planCode,
              b.product_name                                                              productName,
              ifnull(a.ps, 0)                                                             ps,
              ifnull(a.qty, 0)                                                            qty,
              ifnull(b.greyfabric_price, 0)                                               price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)                  money,
              CONCAT_WS(' ', b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification) specification,
              a.batch_num
       FROM jrkbillsum a
                LEFT JOIN plan b ON a.planid = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

      UNION ALL

      #   收款调整
      (SELECT c.company_name                                                                    clientName,
              b.plan_code                                                                       planCode,
              b.product_name                                                                    productName,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)                              ps,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)                                qty,
              ifnull(a.price, 0)                                                                price,
              round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2) money,
              CONCAT_WS(' ', b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification)       specification,
              null      batch_num
       FROM jrkbill_adjust a
                LEFT JOIN plan b ON a.plan_id = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)
       union all
        (
            SELECT c.company_name                                           clientName,
                   a.plan_code                                              planCode,
                   b.product_name                                           productName,
                   ifnull(a.input_pairs, 0)                                 ps,
                   ifnull(a.input_kilo, 0)                                  qty,
                   ifnull(a.processing_cost, 0)                             price,
                   round(a.money, 2)                                        money,
                   CONCAT_WS(' ', a.greyfabric_specification) specification,
                   a.batch_num
            FROM product_outward_order a
                     LEFT JOIN plan b ON a.plan_code = b.plan_code
                     LEFT JOIN contact_company c ON b.contact_company_id = c.id
            WHERE a.state = 1
              AND ifnull(a.is_send_directly, 0) = 0
              AND ifnull(a.is_finished_product, 0) = 1
        )
       union all
       # 发货管理
        (SELECT a.client                                                        clientName,
               c.plan_code                                                     planCode,
               c.product_name                                                  productName,
               -ifnull(a.ps, 0)                                                ps,
               -ifnull(a.qty, 0)                                               qty,
               ifnull(a.price, 0)                                              price,
               round(ifnull(a.price, 0) * -ifnull(a.qty, 0), 2)                 money,
               CONCAT_WS(' ', c.product_name, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                         c.greyfabric_specification, c.material_specification) specification,
               c.material_lot_no                                               batch_num
        FROM whitebillfh a
#                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON a.planid = c.id)
                ) rep
        left join contact_company co on rep.clientName = co.company_name
GROUP BY rep.planCode ) re
left join plan p on re.planCode = p.plan_code
where abs(ifnull(re.ps,0))>0; "));
    PREPARE stat FROM @baseSql;
    EXECUTE stat;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
