SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `p_dye_fabric_delivery`;
delimiter ;;
CREATE PROCEDURE `p_dye_fabric_delivery`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100),
                                         IN `checkout` VARCHAR(100))
BEGIN
    /**
        2021年6月25日 色布发货序时表
        2021年7月21日 追加色布出库
        2021年7月22日 追加审核字段
     */
    (SELECT c.company_name           client,
            concat_ws(' ', b.contract_code, b.plan_code, b.product_name, b.material_specification, b.color_no,
                      b.color)       specification,
            ifnull(a.input_pairs, 0) ps,
            ifnull(a.input_kilo, 0)  qty,
            NULL                     countable,
            ifnull(a.price, 0)       price,
            a.money                  money,
            a.dye_company            checkout,
            a.order_code             billcode,
            DATE(a.order_date)       datafh,
            NULL                     carnumber,
            a.remarks                memo,
            a.is_audit               is_audit
     FROM dye_fabric_inbound a
              LEFT JOIN dye_fabric_plan b ON a.dye_plan_code = b.plan_code
              LEFT JOIN contact_company c ON b.contact_company_id = c.id
     WHERE a.state = 1
       AND IF(isnull(`clientName`) OR length(`clientName`) <= 0, TRUE, c.company_name = `clientName`)
       AND IF(isnull(`checkout`) OR length(`checkout`) <= 0, TRUE, ifnull(a.dye_company, '') = `checkout`)
       AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
       AND length(ifnull(a.linked_order_code, '')) > 0)
    UNION ALL
    (SELECT a.client                   client,
            concat_ws(' ', b.contract_code, b.plan_code, b.product_name, b.material_specification, b.color_no,
                      b.color)         specification,
            ifnull(a.ps, 0)            ps,
            ifnull(a.qty, 0)           qty,
            ifnull(a.countable, 0)     countable,
            ifnull(a.price, 0)         price,
            ifnull(a.price, 0) * a.qty money,
            a.checkout                 checkout,
            a.billcode                 billcode,
            DATE(a.datafh)             datafh,
            a.carnumber                carnumber,
            a.memo                     memo,
            NULL                       is_audit
     FROM cp_billfh a
              LEFT JOIN dye_fabric_plan b ON a.planid = b.id
     WHERE IF(isnull(`clientName`) OR length(`clientName`) <= 0, TRUE, a.client = `clientName`)
       AND IF(isnull(`checkout`) OR length(`checkout`) <= 0, TRUE, ifnull(a.checkout, '') = `checkout`)
       AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`));
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_dye_fabric_plan_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_dye_fabric_plan_report`;
delimiter ;;
CREATE PROCEDURE `p_dye_fabric_plan_report`(IN `start` DATE, IN `end` DATE, IN `markFinished` TINYINT(1))
BEGIN
    /**
      2021年6月24日 色布计划序时表
      2021年6月27日 按照日期到排序
      2021年6月28日 追加入库出库数量
      2021年7月21日 更改逻辑
     */
    SELECT a.id                                                            id,
           DATE(a.plan_date)                                               plan_date,
           a.contact_company_id                                            contact_company_id,
           b.company_name                                                  company_name,
           a.contract_code                                                 contract_code,
           a.plan_code                                                     plan_code,
           a.order_code                                                    order_code,
           a.product_name                                                  product_name,
           a.process_type                                                  process_type,
           a.material_specification                                        material_specification,
           a.color_no                                                      material_specification,
           a.color                                                         color,
           ifnull(a.net_diff, 0)                                           net_diff,
           ifnull(a.plan_kilo, 0)                                          plan_kilo,
           ifnull(a.rate, 0)                                               rate,
           ifnull(a.material_price, 0)                                     material_price,
           a.plan_employee_id                                              plan_employee_id,
           c.emp_name                                                      employee_name,
           DATE(a.submit_date)                                             submit_date,
           a.create_user_id                                                create_user_id,
           d.user_name                                                     create_user_name,
           a.remarks                                                       remarks,
           ifnull(e.pairs, 0) + ifnull(x.input_pairs, 0)                   input_pairs,
           ifnull(f.ps, 0) + ifnull(g.pairs, 0) + ifnull(y.input_pairs, 0) delivery_pairs,
           ifnull(e.pairs, 0) + ifnull(x.input_pairs, 0) - ifnull(y.input_pairs, 0) - ifnull(f.ps, 0) -
           ifnull(g.pairs, 0)                                              remain_pairs,
           ifnull(e.qty, 0) + ifnull(x.input_kilo, 0)                      input_kilo,
           ifnull(f.qty, 0) + ifnull(g.qty, 0) + ifnull(y.input_kilo, 0)   delivery_kilo,
           ifnull(e.qty, 0) + ifnull(x.input_kilo, 0) - ifnull(y.input_kilo, 0) - ifnull(f.qty, 0) -
           ifnull(g.qty, 0)                                                remain_kilo,
           ifnull(a.mark_finished, 0)                                      mark_finished,
           a.is_audit                                                      is_audit
    FROM dye_fabric_plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN employee c ON a.plan_employee_id = c.id
             LEFT JOIN sys_user d ON a.create_user_id = d.id
             LEFT JOIN (
        #         调整入库
        SELECT sum(pairs) pairs, sum(qty) qty, plan_id
        FROM cp_adjust
        WHERE bill_type IN ('手动入库', '盘点')
        GROUP BY plan_id) e ON e.plan_id = a.id
             LEFT JOIN (
        #      色布入库
        SELECT dye_plan_code, sum(input_kilo) input_kilo, sum(input_pairs) input_pairs, sum(input_meter) input_meter
        FROM dye_fabric_inbound
        WHERE state = 1
          AND isnull(linked_order_code)
        GROUP BY dye_plan_code) x ON x.dye_plan_code = a.plan_code
             LEFT JOIN (
        #         出库
        SELECT sum(ps) ps, sum(qty) qty, planid FROM cp_billfh GROUP BY planId) f ON f.planId = a.id
             LEFT JOIN (
        #             色布出库
        SELECT dye_plan_code, sum(input_kilo) input_kilo, sum(input_pairs) input_pairs, sum(input_meter) input_meter
        FROM dye_fabric_inbound
        WHERE state = 1
          AND length(ifnull(linked_order_code, '')) > 0
        GROUP BY dye_plan_code) y ON y.dye_plan_code = a.plan_code
             LEFT JOIN (
        #         出库调整
        SELECT sum(pairs) pairs, sum(qty) qty, plan_id FROM cp_adjust WHERE bill_type = '手动出库' GROUP BY plan_id) g
                       ON g.plan_id = a.id
    WHERE a.state = 1
      AND if(isnull(`markFinished`), 1 = 1, ifnull(a.mark_finished, 0) = `markFinished`)
      AND DATE(a.plan_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.id DESC;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_dye_inbound_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_dye_inbound_report`;
delimiter ;;
CREATE PROCEDURE `p_dye_inbound_report`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100))
BEGIN
    /**
      2021年6月27日 色布入库序时表
     */
    IF ((isnull(`clientName`) OR length(`clientName`) <= 0) OR `clientName` = '全部') THEN
        SELECT NULL               id,
               c.company_name     client_name,
               b.plan_code,
               b.product_name,
               b.material_specification,
               b.color_no,
               b.color,
               ifnull(a.pairs, 0) ps,
               ifnull(a.qty, 0)   qty,
               a.client_name      supplier,
               a.bill_date,
               a.remarks,
               NULL               is_send_directly,
               NULL               is_audit
        FROM cp_adjust a
                 LEFT JOIN dye_fabric_plan b ON a.plan_id = b.id
                 LEFT JOIN contact_company c ON b.contact_company_id = c.id
        WHERE a.bill_type IN ('手动入库', '盘点')
          AND DATE(a.bill_date) BETWEEN DATE(`start`) AND DATE(`end`)
        UNION ALL
        #      色布入库
        SELECT a.id                                 id,
               b.company_name                       client_name,
               a.dye_plan_code                      plan_code,
               c.product_name,
               c.material_specification,
               c.color_no,
               c.color,
               ifnull(a.input_pairs, 0)             ps,
               ifnull(a.input_kilo, 0)              qty,
               NULL                                 supplier,
               a.order_date,
               a.remarks,
               if(a.is_send_directly = 1, '是', '否') is_send_directly,
               a.is_audit                           is_audit
        FROM dye_fabric_inbound a
                 LEFT JOIN contact_company b ON a.contact_company_id = b.id
                 LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
        WHERE a.state = 1
          AND isnull(a.linked_order_code)
          AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`);
    ELSE
        SELECT NULL               id,
               c.company_name     client_name,
               b.plan_code,
               b.product_name,
               b.material_specification,
               b.color_no,
               b.color,
               ifnull(a.pairs, 0) ps,
               ifnull(a.qty, 0)   qty,
               a.client_name      supplier,
               a.bill_date,
               a.remarks,
               NULL               is_send_directly,
               NULL               is_audit
        FROM cp_adjust a
                 LEFT JOIN dye_fabric_plan b ON a.plan_id = b.id
                 LEFT JOIN contact_company c ON b.contact_company_id = c.id
        WHERE a.bill_type IN ('手动入库', '盘点')
          AND c.company_name = `clientName`
          AND DATE(a.bill_date) BETWEEN DATE(`start`) AND DATE(`end`)
        UNION ALL
        #      色布入库
        SELECT a.id                                 id,
               b.company_name                       client_name,
               a.dye_plan_code                      plan_code,
               c.product_name,
               c.material_specification,
               c.color_no,
               c.color,
               ifnull(a.input_pairs, 0)             ps,
               ifnull(a.input_kilo, 0)              qty,
               NULL                                 supplier,
               a.order_date,
               a.remarks,
               if(a.is_send_directly = 1, '是', '否') is_send_directly,
               a.is_audit                           is_audit
        FROM dye_fabric_inbound a
                 LEFT JOIN contact_company b ON a.contact_company_id = b.id
                 LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
        WHERE a.state = 1
          AND isnull(a.linked_order_code)
          AND b.company_name = `clientName`
          AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`);
    END IF;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_material_order_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_material_order_report`;
delimiter ;;
CREATE PROCEDURE `p_material_order_report`(IN `start` DATE, IN `end` DATE, IN `supplierName` VARCHAR(100))
BEGIN
    /**
      2021年5月25日  原料入库序时表
      2021年7月15日  原料入库排除原料退回的单据类型
      2021年7月16日 更改单据类型
     */
    SELECT a.id,                                                             -- 主键
           DATE(a.order_date)                              order_date,       -- 制单日期
           a.order_code                                    order_code,       -- 单据编号
           a.account_type,                                                   -- 付款方式
           b.supplier_name,                                                  -- 供应商名称
           a.material_spec,                                                  -- 原料规格
           a.batch_num,                                                      -- 批次号
           a.input_money * d.access_mode                   input_money,      -- 入库金额
           ifnull(a.box_amount, 0) * d.access_mode         box_amount,       -- 件数
           a.input_kilo * d.access_mode                    input_kilo,       -- 入库重量
           a.price,                                                          -- 单价
           a.remarks,                                                        -- 备注
           a.supplier_id,                                                    -- 供应商编号
           c.user_name                                     create_user_name, -- 创建人名称
           a.create_time,                                                    -- 创建时间
           a.account_type,                                                   -- 类型
           d.bill_type_name,                                                 -- 单据类型名称
           if(ifnull(a.is_send_directly, 0) = 1, '是', '否') is_send_directly, -- 是否直发
           a.is_audit                                       is_audit
    FROM material_order a
             LEFT JOIN supplier b ON a.supplier_id = b.id
             LEFT JOIN sys_user c ON a.create_user_id = c.id
             LEFT JOIN bill_type d ON a.bill_type = d.id
    WHERE a.state = 1
      AND ifnull(d.company, 0) = 0
      AND if(ifnull(`supplierName`, '') = '' OR ifnull(`supplierName`, '') = '全部', 1 = 1,
             b.supplier_name = `supplierName`)
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.id DESC;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_material_outbound
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_material_outbound`;
delimiter ;;
CREATE PROCEDURE `p_material_outbound`(IN `start` DATE, IN `end` DATE, IN `processCompany` VARCHAR(100))
BEGIN
    /**
      2021年6月30日 原料领料序时表存储过程
      2021年7月15日 追加原料退回的单据类型
      2021年7月16日 追加单据类型
     */
    SELECT a.id                                            id,
           a.order_code                                    order_code,
           a.process_company                               process_company,
           a.material_spec                                 material_spec,
           a.batch_num                                     batch_num,
           ifnull(a.input_kilo, 0) * c.access_mode * -1    input_kilo,
           ifnull(a.box_amount, 0) * c.access_mode * -1    box_amount,
           b.supplier_name                                 supplier_name,
           DATE(a.order_date)                              order_date,
           a.create_time                                   create_time,
           a.handler_name                                  handler_name,
           a.remarks                                       remarks,
           c.access_mode                                   access_mode,
           if(ifnull(a.is_send_directly, 0) = 1, '是', '否') is_send_directly,
           c.bill_type_name                                bill_type_name,
           e.user_name                                     create_user_name,
           a.is_audit                                      is_audit
    FROM material_order a
             LEFT JOIN supplier b ON a.supplier_id = b.id
             LEFT JOIN bill_type c ON a.bill_type = c.id
             LEFT JOIN process_company d ON a.process_company = d.company_name
             LEFT JOIN sys_user e ON a.create_user_id = e.id
    WHERE a.state = 1
      AND ifnull(c.company, 0) = 1
      AND if(ifnull(`processCompany`, '') = '' OR ifnull(`processCompany`, '') = '全部', 1 = 1,
             a.process_company = `processCompany`)
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.id DESC;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_plan_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_plan_report`;
delimiter ;;
CREATE PROCEDURE `p_plan_report`(IN `start` DATE, IN `end` DATE, IN `markFinished` TINYINT(1))
BEGIN
    /**
        2021年5月18日 计划单序时表
        2021年5月26日 追加是否完工字段
        2021年7月20日 机台号排序
     */

    SELECT a.id,
           a.contact_company_id,
           b.company_name,
           a.contract_code,
           a.plan_code,
           a.plan_date,
           a.order_code,
           a.product_name,
           a.material_specification,
           a.material_lot_no,
           a.material_supplier_id,
           d.supplier_name                                                                   material_supplier_name,
           a.greyfabric_weight,
           a.greyfabric_width,
           a.greyfabric_specification,
           a.greyfabric_price,
           a.m_height,
           a.plan_kilo,
           a.greyfabric_date,
           a.submit_date,
           a.material_price,
           a.processing_cost,
           a.middle_specification,
           a.middle_supplier_id,
           e.supplier_name                                                                   middle_supplier_name,
           a.bottom_specification,
           a.bottom_supplier_id,
           c.supplier_name                                                                   bottom_supplier_name,
           a.machine_type,
           a.needle_amount,
           a.low_weight,
           a.low_width,
           a.plan_employee_id,
           h.emp_name                                                                        plan_employee_name,
           a.product_employee_id,
           i.emp_name                                                                        product_employee_name,
           a.check_employee_id,
           g.emp_name                                                                        check_employee_name,
           a.mark_company_name,
           a.fabric_requirement,
           a.remarks,
           a.create_user_id,
           ifnull(a.mark_finished, 0)                                                        mark_finished,
           j.user_name                                                                       create_user_name,
           a.create_time,
           a.state,
           ifnull(k.pairs, 0)                                                                input_pairs,
           ifnull(k.kilo, 0)                                                                 input_kilo,
           ifnull(l.kilo, 0) + ifnull(n.kilo, 0)                                             delivery_kilo,
           ifnull(l.pairs, 0) + ifnull(n.pairs, 0)                                           delivery_pairs,
           ifnull(k.pairs, 0) - ifnull(l.pairs, 0) - ifnull(n.pairs, 0) + ifnull(m.pairs, 0) remain_pairs,
           ifnull(k.kilo, 0) - ifnull(l.kilo, 0) - ifnull(n.kilo, 0) + ifnull(m.kilo, 0)     remain_kilo,
           a.notice_date                                                                     notice_date,
           o.jth                                                                             jth,
           a.is_audit                                                                        is_audit
    FROM plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN supplier c ON a.bottom_supplier_id = c.id
             LEFT JOIN supplier d ON a.material_supplier_id = d.id
             LEFT JOIN supplier e ON a.middle_supplier_id = e.id
             LEFT JOIN employee g ON a.check_employee_id = g.id
             LEFT JOIN employee h ON a.plan_employee_id = h.id
             LEFT JOIN employee i ON a.product_employee_id = i.id
             LEFT JOIN sys_user j ON a.create_user_id = j.id
             LEFT JOIN (
        /* 打卷数据 */
        SELECT a.id, ifnull(b.pairs, 0) + ifnull(c.pairs, 0) pairs, ifnull(b.kilo, 0) + ifnull(c.kilo, 0) kilo
        FROM plan a
                 LEFT JOIN (SELECT planid, sum(ps) pairs, sum(qty) kilo FROM jrkbillsum GROUP BY planid) b
                           ON a.id = b.planid
                 LEFT JOIN (SELECT plan_id, sum(ifnull(pairs, 0)) pairs, sum(ifnull(qty, 0)) kilo
                            FROM jrkbill_adjust
                            WHERE state = 1
                              AND bill_type = '手动入库'
                            GROUP BY plan_id) c ON a.id = c.plan_id) k ON k.id = a.id
             LEFT JOIN (
        /* 发货数据 */
        SELECT ifnull(sum(a.ps), 0) pairs, ifnull(sum(a.qty), 0) kilo, a.planid
        FROM whitebillfh a
             #              LEFT JOIN whitebilldetailfh b ON a.id = b.fid
        GROUP BY a.planid) l ON l.planid = a.id
             LEFT JOIN (
        /* 盘点 */
        SELECT plan_id, ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo
        FROM jrkbill_adjust
        WHERE bill_type = '盘点'
        GROUP BY plan_id) m ON m.plan_id = a.id
             LEFT JOIN (
        /* 出库 */
        SELECT plan_id, ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo
        FROM jrkbill_adjust
        WHERE bill_type = '手动出库'
        GROUP BY plan_id) n ON n.plan_id = a.id
        /* 机台 */
             LEFT JOIN (SELECT planid, group_concat(DISTINCT ifnull(jth, '') ORDER BY cast(ifnull(jth, '') AS DECIMAL) SEPARATOR ',') jth FROM plan_jth GROUP BY planid) o
                       ON o.planid = a.id
    WHERE a.state = 1
      AND if(isnull(`markFinished`), 1 = 1, ifnull(a.mark_finished, 0) = `markFinished`)
      AND if(isnull(`markFinished`),DATE(a.plan_date) BETWEEN DATE(`start`) AND DATE(`end`), 1 = 1)
    ORDER BY ifnull(a.mark_finished, 0), a.create_time DESC;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;