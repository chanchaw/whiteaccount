/**
  2021年7月23日 创建仓库表
 */
CREATE TABLE storage
(
    id           INT          NOT NULL COMMENT '主键',
    storage_name VARCHAR(100) NOT NULL COMMENT '仓库名称',
    CONSTRAINT storage_pk PRIMARY KEY (id)
) COMMENT '仓库';

CREATE UNIQUE INDEX storage_storage_name_uindex ON storage (storage_name);

INSERT INTO storage (id, storage_name)
VALUES (1, '白坯仓库'),
       (2, '废布仓库');