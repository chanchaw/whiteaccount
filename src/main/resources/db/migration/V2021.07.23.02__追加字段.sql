/**
  2021年7月23日 追加字段
 */
alter table product_outward_order
    add storage_id INT null comment '仓库编号';
create index product_outward_order_storage_id_index
    on product_outward_order (storage_id);