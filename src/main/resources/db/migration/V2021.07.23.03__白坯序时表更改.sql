SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `getFHhistoryDetail`;
DELIMITER ;;
CREATE PROCEDURE `getFHhistoryDetail`(m_SDate DATE,
                                      m_EDate DATE,
                                      m_Client VARCHAR(100),
                                      m_CheckType VARCHAR(100))
BEGIN
    SELECT NULL                                                                                   id,
           a.client,
           a.address,
           CONCAT_WS(' ', c.order_code, c.product_name, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight),
                     c.m_height, c.greyfabric_specification, a.aorb, c.material_specification) AS pmgg,
           a.ps,
           round(a.qty, 1)                                                                     AS qty,
           round(a.price, 2)                                                                   AS price,
           ROUND(round(a.price, 2) * round(a.qty, 1), 2)                                       AS money,
           a.checkout                                                                             checkout,
           a.billcode                                                                             billcode,
           c.material_lot_no                                                                      batch_num,
           NULL                                                                                   isSendDirectly,
           a.datafh                                                                            AS b_data,
           a.datafh                                                                               createTime,
           memo                                                                                   remarks,
           NULL                                                                                   createUserName,
           NULL                                                                                   handlerName,
           NULL                                                                                   is_audit
    FROM whitebillfh a
             LEFT OUTER JOIN plan c ON a.planid = c.id
    WHERE if(ifnull(m_Client, '') = '' OR ifnull(m_Client, '') = '全部', TRUE, a.client = `m_Client`)
      AND if(ifnull(m_CheckType, '') = '' OR ifnull(m_CheckType, '') = '全部', TRUE, a.checkout = `m_CheckType`)
      AND DATE_FORMAT(a.datafh, '%Y-%m-%d') BETWEEN DATE(m_SDate) AND DATE(m_EDate)

    UNION ALL

    SELECT a.id,
           c.company_name                                                                               client,
           a.consignee                                                                                  address,
           CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                     b.m_height, b.greyfabric_specification, a.ab_surface, b.material_specification) AS pmgg,
           ifnull(a.input_pairs, 0) * if(ifnull(a.storage_id, 0) = 1, -1, 1)                            ps,
           round(a.input_kilo, 1) * if(ifnull(a.storage_id, 0) = 1, -1, 1)                           AS qty,
           round(a.processing_cost, 2)                                                               AS price,
           round(a.money, 2) * if(ifnull(a.storage_id, 0) = 1, -1, 1)                                AS money,
           if(a.is_finished_product = 1, '已剖幅', '未剖幅')                                                  checkout,
           a.order_code                                                                                 billcode,
           a.batch_num                                                                                  batch_num,
           if(a.is_send_directly = 1, '是', '否')                                                         isSendDirectly,
           a.order_date                                                                              AS b_data,
           a.create_time                                                                                createTime,
           a.remarks                                                                                    remarks,
           d.user_name                                                                                  createUserName,
           a.handler_name                                                                               handlerName,
           a.is_audit                                                                                   is_audit
    FROM product_outward_order a
             LEFT JOIN plan b ON a.plan_code = b.plan_code
             LEFT JOIN contact_company c ON b.contact_company_id = c.id
             LEFT JOIN sys_user d ON a.create_user_id = d.id
    WHERE if(ifnull(m_Client, '') = '' OR ifnull(m_Client, '') = '全部', TRUE, c.company_name = `m_Client`)
      AND (length(ifnull(a.linked_order_code, '')) > 0 AND ifnull(a.storage_id, 1) = 1)
      AND ifnull(a.is_finished_product, 0) = 1
      AND DATE_FORMAT(a.order_date, '%Y-%m-%d') BETWEEN DATE(`m_SDate`) AND DATE(`m_EDate`);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for getReceivablesDetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `getReceivablesDetail`;
DELIMITER ;;
CREATE PROCEDURE `getReceivablesDetail`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100), IN `isGather` INT)
BEGIN
    /**
        2021年7月1日  白坯应收款
     */
    IF (`isGather` = 1) THEN
        #      汇总
        SELECT rep.client client, round(sum(rep.money), 2) money
        FROM (SELECT a.client, round(sum(a.price * a.qty), 2) money
              FROM whitebillfh a
                   #                        LEFT JOIN whitebilldetailfh b ON a.id = b.fid
              WHERE a.checkout <> '现金'
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, a.client = `clientName`)
              GROUP BY a.client
              UNION ALL
              #             色布入库
              SELECT a.client, round(sum(ifnull(a.price, 0) * ifnull(a.qty, 0)), 2) money
              FROM cp_billfh a
              WHERE a.checkout <> '现金'
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, a.client = `clientName`)
              GROUP BY a.client
              UNION ALL
              #             白坯退货
              SELECT c.company_name client, -round(sum(ifnull(a.processing_cost, 0) * ifnull(a.input_kilo, 0)), 2) money
              FROM product_outward_order a
                       LEFT JOIN plan b ON a.plan_code = b.plan_code
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                       c.company_name = `clientName`)
                AND ifnull(a.is_finished_product, 0) = 1
                AND ifnull(a.storage_id, 0) >0
              UNION ALL
              SELECT client, round(sum(-money), 2) money
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
              GROUP BY client) rep
        GROUP BY rep.client;
    ELSE
        #         明细
        SELECT rep.*
        FROM (SELECT `start`                                                                                  billdate,
                     if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', '全部', `clientName`) client,
                     NULL                                                                                     billcode,
                     '期初结余'                                                                                   billtype,
                     0                                                                                        qty,
                     0                                                                                        ps,
                     0                                                                                        price,
                     sum(a.money)                                                                             money,
                     ''                                                                                       collectiontype,
                     ''                                                                                       billmemo,
                     0                                                                                        SKid
              FROM (SELECT a.client, round(sum(a.price * a.qty), 2) money
                    FROM whitebillfh a
                    WHERE DATE(a.datafh) < DATE(`start`)
                      AND a.checkout <> '现金'
                      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                             a.client = `clientName`)
                    GROUP BY a.client
                    UNION ALL
                    SELECT client, round(sum(-money), 2) money
                    FROM receivables
                    WHERE DATE(billdate) < DATE(`start`)
                      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                             client = `clientName`)
                    GROUP BY client) a
              UNION ALL
              #      发货明细
              SELECT DATE(a.datafh)                                                          billdate,
                     a.client                                                                client,
                     a.billcode                                                              billcode,
                     CONCAT_WS(' ', c.plan_code, c.order_code, c.product_name,
                               CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                               c.greyfabric_specification, a.aorb, c.material_specification) billtype,
                     ifnull(a.qty, 0)                                                        qty,
                     ifnull(a.ps, 0)                                                         ps,
                     ifnull(a.price, 0)                                                      price,
                     ROUND(a.qty * ifnull(a.price, 0), 2)                                    money,
                     a.checkout                                                              collectiontype,
                     a.memo                                                                  billmemo,
                     0                                                                       SKid
              FROM whitebillfh a
                       LEFT OUTER JOIN plan c ON a.planid = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
                AND a.checkout <> '现金'
                AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL
              #              色布发货
              SELECT DATE(a.datafh)                                                                 billdate,
                     a.client                                                                       client,
                     a.billcode                                                                     billcode,
                     CONCAT_WS(' ', '色布发货', c.order_code, c.product_name, c.material_specification) billtype,
                     ifnull(a.qty, 0)                                                               qty,
                     ifnull(a.ps, 0)                                                                ps,
                     ifnull(a.price, 0)                                                             price,
                     ROUND(a.qty * ifnull(a.price, 0), 2)                                           money,
                     a.checkout                                                                     collectiontype,
                     a.memo                                                                         billmemo,
                     0                                                                              SKid
              FROM cp_billfh a
                       LEFT OUTER JOIN dye_fabric_plan c ON a.planid = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, a.client = `clientName`)
                AND a.checkout <> '现金'
                AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL
              #     收款明细
              SELECT DATE_FORMAT(billdate, '%Y-%m-%d') billdate,
                     client                            client,
                     NULL                              billcode,
                     billtype                          billtype,
                     0                                 qty,
                     0                                 ps,
                     0                                 price,
                     -ifnull(money, 0)                 money,
                     collectiontype                    collectiontype,
                     billmemo                          billmemo,
                     id                                SKid
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
                AND DATE(billdate) BETWEEN DATE(`start`) AND DATE(`end`)
              UNION ALL
              #           白坯出库
              SELECT DATE_FORMAT(billdate, '%Y-%m-%d') billdate,
                     client                            client,
                     NULL                              billcode,
                     billtype                          billtype,
                     0                                 qty,
                     0                                 ps,
                     0                                 price,
                     -ifnull(money, 0)                 money,
                     collectiontype                    collectiontype,
                     billmemo                          billmemo,
                     id                                SKid
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
                AND DATE(billdate) BETWEEN DATE(`start`) AND DATE(`end`)
              UNION ALL
              #            白坯退货
              SELECT DATE_FORMAT(a.order_date, '%Y-%m-%d') billdate,
                     c.company_name                        client,
                     a.order_code                          billcode,
                     '白坯退货'                                billtype,
                     -ifnull(a.input_kilo, 0)              qty,
                     -ifnull(a.input_pairs, 0)             ps,
                     a.processing_cost                     price,
                     -ifnull(a.money, 0)                   money,
                     NULL                                  collectiontype,
                     a.remarks                             billmemo,
                     NULL                                  SKid
              FROM product_outward_order a
                       LEFT JOIN plan b ON a.plan_code = b.plan_code
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
                AND ifnull(a.storage_id,0)>0
                AND ifnull(a.is_finished_product, 0) = 1
                AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)) rep
        ORDER BY rep.billdate;
    END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_greyfabric_input
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_greyfabric_input`;
DELIMITER ;;
CREATE PROCEDURE `p_greyfabric_input`(IN `start` DATE, IN `end` DATE)
BEGIN
    /**
      2021年6月9日  白坯入库明细
      2021年7月12日 追加白坯
      2021年7月17日 追加直发
      2021年7月22日 追加制单人，制单日期，备注，经手人
     */
    SELECT rep.*
    FROM (SELECT any_value(a.id)                                id,
                 any_value(c.company_name)                      client,
                 any_value(b.plan_code)                         planCode,
                 any_value(a.cn)                                computerName,
                 CONCAT_WS(' ', any_value(b.order_code), any_value(b.product_name),
                           CONCAT_WS('*', any_value(b.greyfabric_width), any_value(b.greyfabric_weight)),
                           any_value(b.m_height), any_value(b.greyfabric_specification),
                           any_value(b.material_specification)) specification,
                 ifnull(sum(a.ps), 0)                           ps,
                 ifnull(sum(a.qty), 0)                          qty,
                 a.createdate                                   createdate,
                 any_value(a.class_name)                        className,
                 any_value(a.jth)                               machineType,
                 any_value(a.aorb)                              aorb,
                 any_value(a.batch_num)                         batchNum,
                 NULL                                           isSendDirectly,
                 a.createdate                                   createTime,
                 NULL                                           remarks,
                 NULL                                           createUserName,
                 NULL                                           handlerName,
                 NULL                                           is_audit
          FROM jrkbillsum a
                   LEFT JOIN plan b ON a.planid = b.id
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
          WHERE DATE(a.createdate) BETWEEN DATE(`start`) AND DATE(`end`)
          GROUP BY a.createdate, a.aorb

          UNION ALL

          SELECT a.id,
                 c.company_name                                                              client,
                 b.plan_code                                                                 planCode,
                 a.bill_type                                                                 computerName,
                 CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.m_height, b.greyfabric_specification, b.material_specification) specification,
                 ifnull(a.pairs, 0)                                                          ps,
                 ifnull(a.qty, 0)                                                            qty,
                 a.jrkbill_date                                                              createdate,
                 NULL                                                                        className,
                 NULL                                                                        machineType,
                 NULL                                                                        aorb,
                 NULL                                                                        batchNum,
                 NULL                                                                        isSendDirectly,
                 a.create_time                                                               createTime,
                 a.remarks                                                                   remarks,
                 d.user_name                                                                 createUserName,
                 NULL                                                                        handlerName,
                 NULL                                                                        is_audit
          FROM jrkbill_adjust a
                   LEFT JOIN plan b ON a.plan_id = b.id
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
                   LEFT JOIN sys_user d ON a.create_user_id = d.id
          WHERE bill_type = '手动入库'
            AND DATE(jrkbill_date) BETWEEN DATE(`start`) AND DATE(`end`)
          UNION ALL
          #         白坯入库
          SELECT a.id,
                 c.company_name                                                              client,
                 b.plan_code                                                                 planCode,
                 a.process_company                                                           computerName,
                 CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.m_height, b.greyfabric_specification, a.material_specification) specification,
                 ifnull(a.input_pairs, 0)                                                    ps,
                 ifnull(a.input_kilo, 0)                                                     qty,
                 a.create_time                                                               createdate,
                 NULL                                                                        className,
                 NULL                                                                        machineType,
                 a.ab_surface                                                                aorb,
                 a.batch_num                                                                 batchNum,
                 if(ifnull(a.is_send_directly, 0) = 1, '是', '否')                             isSendDirectly,
                 a.create_time                                                               createTime,
                 a.remarks                                                                   remarks,
                 d.user_name                                                                 createUserName,
                 a.handler_name                                                              handlerName,
                 a.is_audit                                                                  is_audit
          FROM product_outward_order a
                   LEFT JOIN plan b ON a.plan_code = b.plan_code
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
                   LEFT JOIN sys_user d ON a.create_user_id = d.id
          WHERE a.state = 1
            AND (length(ifnull(a.linked_order_code, '')) <= 0 OR ifnull(a.storage_id, 0) = 1)
            AND ifnull(a.is_finished_product, 0) = 1
            AND DATE(a.create_time) BETWEEN DATE(`start`) AND DATE(`end`)) rep
    ORDER BY rep.createdate DESC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_greyfabric_storage
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_greyfabric_storage`;
DELIMITER ;;
CREATE PROCEDURE `p_greyfabric_storage`(IN mode INT)
BEGIN
    /**
     2021年6月9日 白坯库存
     */
    SET @baseSql = if(mode = 1, concat("
    select re.* from (
    SELECT rep.clientName client,
       -- rep.planCode,
       -- rep.productName,
       sum(rep.ps) ps,
       sum(rep.qty) qty,
       -- avg(rep.price) price,
       p.material_lot_no  batchNum,
       sum(rep.money) money
       -- rep.specification
FROM ((SELECT c.company_name                                                              clientName,
              b.plan_code                                                                 planCode,
              b.product_name                                                              productName,
              ifnull(a.ps, 0)                                                             ps,
              ifnull(a.qty, 0)                                                            qty,
              ifnull(b.greyfabric_price, 0)                                               price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)                  money,
              CONCAT_WS(' ', b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification) specification,
              a.batch_num
       FROM jrkbillsum a
                LEFT JOIN plan b ON a.planid = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

      UNION ALL

      #   收款调整
      (SELECT c.company_name                                                                    clientName,
              b.plan_code                                                                       planCode,
              b.product_name                                                                    productName,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)                              ps,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)                                qty,
              ifnull(a.price, 0)                                                                price,
              round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2) money,
              CONCAT_WS(' ', b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification)       specification,
              NULL                                                                              batch_num
       FROM jrkbill_adjust a
                LEFT JOIN plan b ON a.plan_id = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

        union all
        #  白坯入库
        (
            SELECT c.company_name                                           clientName,
                   a.plan_code                                              planCode,
                   b.product_name                                           productName,
                   ifnull(a.input_pairs, 0)                                 ps,
                   ifnull(a.input_kilo, 0)                                  qty,
                   ifnull(a.processing_cost, 0)                             price,
                   round(a.money, 2)                                        money,
                   CONCAT_WS(' ', a.greyfabric_specification) specification,
                   a.batch_num                                              batch_num
            FROM product_outward_order a
                     LEFT JOIN plan b ON a.plan_code = b.plan_code
                     LEFT JOIN contact_company c ON b.contact_company_id = c.id
            WHERE a.state = 1
              AND (ifnull(a.is_send_directly, 0) = 0 or ifnull(a.storage_id,0)=1)
              AND ifnull(a.is_finished_product, 0) = 1
        )
        union all
        # 发货管理
        (SELECT a.client                                                       clientName,
               c.plan_code                                                     planCode,
               c.product_name                                                  productName,
               -ifnull(a.ps, 0)                                                ps,
               -ifnull(a.qty, 0)                                               qty,
               ifnull(a.price, 0)                                              price,
               round(ifnull(a.price, 0) * -ifnull(a.qty, 0), 2)                 money,
               CONCAT_WS(' ', c.product_name, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                         c.greyfabric_specification, c.material_specification) specification,
               c.material_lot_no                                               batch_num
        FROM whitebillfh a
#                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON a.planid = c.id)
                ) rep
        left join contact_company co on rep.clientName = co.company_name
        left join plan p on rep.planCode = p.plan_code
GROUP BY rep.clientName,co.id order by co.id ) re where abs(ifnull(re.ps,0))>0; "), concat("
    select re.*,p.material_lot_no batchNum  from (
    SELECT any_value(rep.clientName) client,
       rep.planCode,
       any_value(rep.productName) productName,
       sum(rep.ps) ps,
       sum(rep.qty) qty,
       avg(rep.price) price,
       sum(rep.money) money,
       any_value(rep.specification) specification
FROM ((SELECT c.company_name                                                              clientName,
              b.plan_code                                                                 planCode,
              b.product_name                                                              productName,
              ifnull(a.ps, 0)                                                             ps,
              ifnull(a.qty, 0)                                                            qty,
              ifnull(b.greyfabric_price, 0)                                               price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)                  money,
              CONCAT_WS(' ', b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification) specification,
              a.batch_num
       FROM jrkbillsum a
                LEFT JOIN plan b ON a.planid = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

      UNION ALL

      #   收款调整
      (SELECT c.company_name                                                                    clientName,
              b.plan_code                                                                       planCode,
              b.product_name                                                                    productName,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)                              ps,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)                                qty,
              ifnull(a.price, 0)                                                                price,
              round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2) money,
              CONCAT_WS(' ', b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                        b.m_height, b.greyfabric_specification, b.material_specification)       specification,
              null      batch_num
       FROM jrkbill_adjust a
                LEFT JOIN plan b ON a.plan_id = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)
       union all
        (
            SELECT c.company_name                                           clientName,
                   a.plan_code                                              planCode,
                   b.product_name                                           productName,
                   ifnull(a.input_pairs, 0)                                 ps,
                   ifnull(a.input_kilo, 0)                                  qty,
                   ifnull(a.processing_cost, 0)                             price,
                   round(a.money, 2)                                        money,
                   CONCAT_WS(' ', a.greyfabric_specification) specification,
                   a.batch_num
            FROM product_outward_order a
                     LEFT JOIN plan b ON a.plan_code = b.plan_code
                     LEFT JOIN contact_company c ON b.contact_company_id = c.id
            WHERE a.state = 1
              AND (ifnull(a.is_send_directly, 0) = 0 or ifnull(a.storage_id,0)=1)
              AND ifnull(a.is_finished_product, 0) = 1
        )
       union all
       # 发货管理
        (SELECT a.client                                                        clientName,
               c.plan_code                                                     planCode,
               c.product_name                                                  productName,
               -ifnull(a.ps, 0)                                                ps,
               -ifnull(a.qty, 0)                                               qty,
               ifnull(a.price, 0)                                              price,
               round(ifnull(a.price, 0) * -ifnull(a.qty, 0), 2)                 money,
               CONCAT_WS(' ', c.product_name, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                         c.greyfabric_specification, c.material_specification) specification,
               c.material_lot_no                                               batch_num
        FROM whitebillfh a
#                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON a.planid = c.id)
                ) rep
        left join contact_company co on rep.clientName = co.company_name
GROUP BY rep.planCode ) re
left join plan p on re.planCode = p.plan_code
where abs(ifnull(re.ps,0))>0; "));
    PREPARE stat FROM @baseSql;
    EXECUTE stat;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_plan_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_plan_report`;
DELIMITER ;;
CREATE PROCEDURE `p_plan_report`(IN `start` DATE, IN `end` DATE, IN `markFinished` TINYINT(1))
BEGIN
    /**
        2021年5月18日 计划单序时表
        2021年5月26日 追加是否完工字段
        2021年7月20日 机台号排序
     */

    SELECT a.id,
           a.contact_company_id,
           b.company_name,
           a.contract_code,
           a.plan_code,
           a.plan_date,
           a.order_code,
           a.product_name,
           a.material_specification,
           a.material_lot_no,
           a.material_supplier_id,
           d.supplier_name                                                                   material_supplier_name,
           a.greyfabric_weight,
           a.greyfabric_width,
           a.greyfabric_specification,
           a.greyfabric_price,
           a.m_height,
           a.plan_kilo,
           a.greyfabric_date,
           a.submit_date,
           a.material_price,
           a.processing_cost,
           a.middle_specification,
           a.middle_supplier_id,
           e.supplier_name                                                                   middle_supplier_name,
           a.bottom_specification,
           a.bottom_supplier_id,
           c.supplier_name                                                                   bottom_supplier_name,
           a.machine_type,
           a.needle_amount,
           a.low_weight,
           a.low_width,
           a.plan_employee_id,
           h.emp_name                                                                        plan_employee_name,
           a.product_employee_id,
           i.emp_name                                                                        product_employee_name,
           a.check_employee_id,
           g.emp_name                                                                        check_employee_name,
           a.mark_company_name,
           a.fabric_requirement,
           a.remarks,
           a.create_user_id,
           ifnull(a.mark_finished, 0)                                                        mark_finished,
           j.user_name                                                                       create_user_name,
           a.create_time,
           a.state,
           ifnull(k.pairs, 0)                                                                input_pairs,
           ifnull(k.kilo, 0)                                                                 input_kilo,
           ifnull(l.kilo, 0) + ifnull(n.kilo, 0)                                             delivery_kilo,
           ifnull(l.pairs, 0) + ifnull(n.pairs, 0)                                           delivery_pairs,
           ifnull(k.pairs, 0) + ifnull(p.input_pairs,0) - ifnull(l.pairs, 0) - ifnull(n.pairs, 0) + ifnull(m.pairs, 0) remain_pairs,
           ifnull(k.kilo, 0) + ifnull(p.input_kilo,0)- ifnull(l.kilo, 0) - ifnull(n.kilo, 0) + ifnull(m.kilo, 0)     remain_kilo,
           a.notice_date                                                                     notice_date,
           o.jth                                                                             jth,
           a.is_audit                                                                        is_audit
    FROM plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN supplier c ON a.bottom_supplier_id = c.id
             LEFT JOIN supplier d ON a.material_supplier_id = d.id
             LEFT JOIN supplier e ON a.middle_supplier_id = e.id
             LEFT JOIN employee g ON a.check_employee_id = g.id
             LEFT JOIN employee h ON a.plan_employee_id = h.id
             LEFT JOIN employee i ON a.product_employee_id = i.id
             LEFT JOIN sys_user j ON a.create_user_id = j.id
             LEFT JOIN (
        /* 打卷数据 */
        SELECT a.id, ifnull(b.pairs, 0) + ifnull(c.pairs, 0) pairs, ifnull(b.kilo, 0) + ifnull(c.kilo, 0) kilo
        FROM plan a
                 LEFT JOIN (SELECT planid, sum(ps) pairs, sum(qty) kilo FROM jrkbillsum GROUP BY planid) b
                           ON a.id = b.planid
                 LEFT JOIN (SELECT plan_id, sum(ifnull(pairs, 0)) pairs, sum(ifnull(qty, 0)) kilo
                            FROM jrkbill_adjust
                            WHERE state = 1
                              AND bill_type = '手动入库'
                            GROUP BY plan_id) c ON a.id = c.plan_id) k ON k.id = a.id
             LEFT JOIN (
        /* 发货数据 */
        SELECT ifnull(sum(a.ps), 0) pairs, ifnull(sum(a.qty), 0) kilo, a.planid
        FROM whitebillfh a
             #              LEFT JOIN whitebilldetailfh b ON a.id = b.fid
        GROUP BY a.planid) l ON l.planid = a.id
             LEFT JOIN (
        /* 盘点 */
        SELECT plan_id, ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo
        FROM jrkbill_adjust
        WHERE bill_type = '盘点'
        GROUP BY plan_id) m ON m.plan_id = a.id
             LEFT JOIN (
        /* 出库 */
        SELECT plan_id, ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo
        FROM jrkbill_adjust
        WHERE bill_type = '手动出库'
        GROUP BY plan_id) n ON n.plan_id = a.id
        /* 机台 */
             LEFT JOIN (SELECT planid,
                               group_concat(DISTINCT ifnull(jth, '') ORDER BY cast(ifnull(jth, '') AS DECIMAL) SEPARATOR
                                            ',') jth
                        FROM plan_jth
                        GROUP BY planid) o ON o.planid = a.id
             LEFT JOIN (
        #    白坯退货+白坯入库
        SELECT
            sum(a.input_pairs) input_pairs,
            sum(a.input_kilo) input_kilo,
            a.plan_code
        FROM product_outward_order a
        WHERE a.state = 1
          AND (ifnull(a.is_send_directly,0)=0 OR ifnull(a.storage_id,0)=1)
          AND ifnull(a.is_finished_product,0)=1
        GROUP BY a.plan_code
    ) p ON p.plan_code = a.plan_code
    WHERE a.state = 1
      AND if(isnull(`markFinished`), TRUE, ifnull(a.mark_finished, 0) = `markFinished`)
      AND if(isnull(`markFinished`), DATE(a.plan_date) BETWEEN DATE(`start`) AND DATE(`end`), TRUE)
    ORDER BY ifnull(a.mark_finished, 0), a.create_time DESC;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
