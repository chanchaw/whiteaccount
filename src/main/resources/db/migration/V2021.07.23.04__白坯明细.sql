SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


DROP PROCEDURE IF EXISTS `p_jrkbill_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_jrkbill_dtl`(IN `id` BIGINT)
BEGIN
    /**
       2021年5月22日 打卷入库明细
       2021年6月6日  拼接单据类型到机台号
       2021年7月12日 追加白坯入库
     */
    SELECT *
    FROM ((SELECT any_value(b.id)                          id,
                  ifnull(sum(b.ps), 0)                     input_pairs,
                  ifnull(sum(round(b.qty, 1)), 0)          input_kilo,
                  any_value(b.aorb)                        aorb,
                  any_value(a.greyfabric_specification)    greyfabric_specification,
                  any_value(a.material_specification)      material_specification,
                  any_value(b.cn)                          computer_name,
                  any_value(b.jth)                         machine_type,
                  date_format(b.createdate, '%m-%d %H:%i') createdate,
                  NULL                                     remarks,
                  '入库'                                     bill_type,
                  b.createdate                             order_date
           FROM plan a
                    LEFT JOIN jrkbillsum b ON a.id = b.planid
           WHERE a.id = `id`
             AND b.id IS NOT NULL
           GROUP BY b.createdate)
          UNION ALL
          (SELECT b.id                                       id,
                  ifnull(b.pairs, 0)                         input_pairs,
                  ifnull(round(b.qty, 1), 0)                 input_kilo,
                  NULL                                       aorb,
                  a.greyfabric_specification                 greyfabric_specification,
                  a.material_specification                   material_specification,
                  b.bill_type                                computer_name,
                  NULL                                       machine_type,
                  date_format(b.jrkbill_date, '%m-%d %H:%i') createdate,
                  b.remarks                                  remarks,
                  b.bill_type                                bill_type,
                  b.jrkbill_date                             order_date
           FROM plan a
                    LEFT JOIN jrkbill_adjust b ON a.id = b.plan_id
           WHERE a.id = `id`
             AND b.bill_type = '手动入库'
             AND b.id IS NOT NULL)
          UNION ALL
          #     白坯入库
          (SELECT a.id                                          id,
                  ifnull(a.input_pairs, 0)                      input_pairs,
                  ifnull(round(a.input_kilo, 1), 0)             input_kilo,
                  a.ab_surface                                  aorb,
                  a.greyfabric_specification                    greyfabric_specification,
                  a.material_specification                      material_specification,
                  a.process_company                             computer_name,
                  if(a.is_send_directly = 1, '直发', '')          machine_type,
                  date_format(a.create_time, '%m-%d %H:%i')     createdate,
                  b.remarks                                     remarks,
                  if(ifnull(a.storage_id, 0) = 1, '白坯退货', NULL) bill_type,
                  a.order_date                                  order_date
           FROM product_outward_order a
                    LEFT JOIN plan b ON a.plan_code = b.plan_code
           WHERE a.state = 1
             AND (length(ifnull(a.linked_order_code, '')) <= 0 OR ifnull(a.storage_id, 0) = 1)
             AND ifnull(a.is_finished_product, 0) = 1
             AND b.id = `id`)) r
    ORDER BY r.order_date DESC;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_jrkbillfh_dtl
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_jrkbillfh_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_jrkbillfh_dtl`(IN `id` BIGINT)
BEGIN
    /**
       2021年5月22日 发货明细
       2021年6月6日  拼接字段,追加类型到单据列
       2021年7月13日 追加直发
     */
    SELECT r.*
    FROM ((SELECT a.client                             client_name,
                  a.billcode                           billcode,
                  a.address                            address,
                  ifnull(sum(a.ps), 0)                 send_pairs,
                  sum(round(a.qty, 1))                 send_kilo,
                  d.greyfabric_specification           greyfabric_specification,
                  d.material_specification             material_specification,
                  date_format(a.datafh, '%m-%d %H:%i') send_date,
                  a.memo                               remarks,
                  a.datafh                             order_date
           FROM whitebillfh a
                    #                     LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                    LEFT JOIN plan d ON a.planid = d.id
           WHERE a.planid = `id`
             AND a.id IS NOT NULL
           GROUP BY a.id)
          UNION ALL
          (SELECT c.company_name                             client_name,
                  b.bill_type                                billcode,
                  b.address                                  address,
                  ifnull(b.pairs, 0)                         send_pairs,
                  round(b.qty, 1)                            send_kilo,
                  a.greyfabric_specification                 greyfabric_specification,
                  a.material_specification                   material_specification,
                  date_format(b.jrkbill_date, '%m-%d %H:%i') send_date,
                  b.remarks                                  remarks,
                  b.jrkbill_date                             order_date
           FROM plan a
                    LEFT JOIN jrkbill_adjust b ON a.id = b.plan_id
                    LEFT JOIN contact_company c ON a.contact_company_id = c.id
           WHERE b.plan_id = `id`
             AND b.bill_type = '手动出库'
             AND b.id IS NOT NULL)
          UNION ALL
          #     直发
          (SELECT c.company_name                            client_name,
                  b.order_code                              billcode,
                  b.process_company                         address,
                  ifnull(b.input_pairs, 0)                  send_pairs,
                  round(b.input_kilo, 1)                    send_kilo,
                  a.greyfabric_specification                greyfabric_specification,
                  a.material_specification                  material_specification,
                  date_format(b.create_time, '%m-%d %H:%i') send_date,
                  b.remarks                                 remarks,
                  b.create_time                             order_date
           FROM plan a
                    LEFT JOIN product_outward_order b ON a.plan_code = b.plan_code AND b.state = 1
                    LEFT JOIN contact_company c ON a.contact_company_id = c.id
           WHERE a.id = `id`
             AND ifnull(b.is_finished_product, 0) = 1
             AND (ifnull(b.is_send_directly, 0) = 1)
             AND b.id IS NOT NULL)) r
    ORDER BY r.order_date DESC;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_plan_dtl
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_plan_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_plan_dtl`(IN `id` BIGINT)
BEGIN
    /**
      2021年5月22日 计划单明细查看
     */
    SELECT b.company_name                                                                      company_name,
           concat(ifnull(a.plan_code, ''), ' ', ifnull(a.product_name, ''), ' ', a.greyfabric_weight, '*',
                  a.greyfabric_width, ' ', ifnull(a.m_height, 0), ' ', ifnull(a.greyfabric_specification, ''), ' ',
                  ifnull(a.material_specification, ''))                                        product_name,
           ifnull(a.plan_kilo, 0)                                                              plan_kilo,
           ifnull(c.pairs, 0) + ifnull(f.pairs, 0) + ifnull(h.pairs, 0) + ifnull(i.pairs, 0)   input_pairs,
           ifnull(c.kilo, 0) + ifnull(f.kilo, 0) + ifnull(h.kilo, 0) + ifnull(i.kilo, 0)       input_kilo,
           ifnull(d.kilo, 0) + ifnull(e.kilo, 0) + ifnull(i.kilo, 0)                           send_kilo,
           ifnull(d.pairs, 0) + ifnull(e.pairs, 0) + ifnull(i.pairs, 0)                        send_pairs,
           ifnull(round(ifnull(a.plan_kilo, 0) - ifnull(d.kilo, 0) - ifnull(e.kilo, 0), 1), 0) plan_remain_kilo,
           ifnull(round(ifnull(c.kilo, 0) + ifnull(f.kilo, 0) - ifnull(d.kilo, 0) - ifnull(e.kilo, 0) +
                        ifnull(g.kilo, 0) + ifnull(h.kilo, 0), 1), 0)                          remain_kilo,
           ifnull(round(ifnull(c.pairs, 0) + ifnull(f.pairs, 0) - ifnull(d.pairs, 0) - ifnull(e.pairs, 0) +
                        ifnull(g.pairs, 0) + ifnull(h.pairs, 0), 0), 0)                        remain_pairs,
           ifnull(a.greyfabric_price, 0)                                                       greyfabric_price
    FROM plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN (
        # 总入库公斤
        SELECT a.id, ifnull(b.pairs, 0) pairs, ifnull(b.kilo, 0) kilo
        FROM plan a
                 LEFT JOIN (SELECT planid, sum(ps) pairs, sum(qty) kilo FROM jrkbillsum GROUP BY planid) b
                           ON a.id = b.planid) c ON c.id = a.id
             LEFT JOIN (
        # 已发公斤
        SELECT a.planid id, ifnull(sum(round(a.qty, 1)), 0) kilo, ifnull(sum(a.ps), 0) pairs
        FROM whitebillfh a
             #                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
        GROUP BY a.planid) d ON d.id = a.id
             LEFT JOIN (
        # 手动出库
        SELECT ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo, plan_id
        FROM jrkbill_adjust
        WHERE state = 1
          AND bill_type = '手动出库'
        GROUP BY plan_id) e ON e.plan_id = a.id
             LEFT JOIN (
        #             手动入库
        SELECT ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo, plan_id
        FROM jrkbill_adjust
        WHERE state = 1
          AND bill_type IN ('手动入库')
        GROUP BY plan_id) f ON f.plan_id = a.id
             LEFT JOIN (
        #            盘点
        SELECT ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo, plan_id
        FROM jrkbill_adjust
        WHERE state = 1
          AND bill_type IN ('盘点')
        GROUP BY plan_id) g ON g.plan_id = a.id
        #   白坯入库
             LEFT JOIN (SELECT ifnull(sum(input_pairs), 0) pairs, ifnull(sum(input_kilo), 0) kilo, plan_code
                        FROM product_outward_order
                        WHERE state = 1
                          AND ifnull(is_finished_product, 0) = 1
                          AND ifnull(is_send_directly, 0) = 0
                          AND ifnull(storage_id, 0) <= 0
                        GROUP BY plan_code) h ON h.plan_code = a.plan_code
        #     白坯发货
             LEFT JOIN (SELECT ifnull(sum(if(ifnull(storage_id, 0) = 1, -1, 1) * input_pairs), 0) pairs,
                               ifnull(sum(input_kilo), 0)                                         kilo,
                               plan_code
                        FROM product_outward_order
                        WHERE state = 1
                          AND ifnull(is_finished_product, 0) = 1
                          AND ((ifnull(is_send_directly, 0) = 1 AND length(ifnull(linked_order_code, '')) > 0) OR
                               ifnull(storage_id, 0) = 1)
                        GROUP BY plan_code) i ON i.plan_code = a.plan_code
    WHERE a.id = `id`;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_plan_storage_modify
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_plan_storage_modify`;
DELIMITER ;;
CREATE PROCEDURE `p_plan_storage_modify`(IN `id` BIGINT)
BEGIN
    /**
      2021年6月4日  库存调整存储过程
      2021年6月6日  追加字段,客户，收货单位，供应商
     */
    SELECT *
    FROM (SELECT report.*,
                 @storage_ps := ifnull(report.pairs, 0) + @storage_ps  remain_pairs,
                 @storage_qty := ifnull(report.kilo, 0) + @storage_qty remain_kilo
          FROM (
                   --           库存
                   (SELECT NULL                                     id,
                           a.id                                     plan_id,
                           date_format(b.createdate, '%m-%d %H:%i') jrkbill_date,
                           '入库'                                     bill_type,
                           ifnull(sum(b.ps), 0)                     pairs,
                           ifnull(sum(b.qty), 0)                    kilo,
                           ''                                       remarks,
                           c.company_name                           client_name,
                           NULL                                     address,
                           NULL                                     price,
                           b.createdate                             order_date
                    FROM plan a
                             LEFT JOIN jrkbillsum b ON a.id = b.planid
                             LEFT JOIN contact_company c ON a.contact_company_id = c.id
                    WHERE a.id = `id`
                      AND b.id IS NOT NULL
                    GROUP BY b.createdate)

                   UNION ALL
                   --          手动调整
                   (SELECT b.id                                                              id,
                           a.id                                                              plan_id,
                           date_format(b.jrkbill_date, '%m-%d %H:%i')                        jrkbill_date,
                           ifnull(b.bill_type, '')                                           bill_type,
                           if(b.bill_type = '手动出库', -ifnull(b.pairs, 0), ifnull(b.pairs, 0)) pairs,
                           if(b.bill_type = '手动出库', -ifnull(b.qty, 0), ifnull(b.qty, 0))     kilo,
                           b.remarks                                                         remarks,
                           b.client_name                                                     client_name,
                           b.address                                                         address,
                           b.price                                                           price,
                           b.jrkbill_date                                                    order_date
                    FROM plan a
                             LEFT JOIN jrkbill_adjust b ON a.id = b.plan_id AND b.state = 1
                    WHERE a.id = `id`
                      AND b.id IS NOT NULL)

                   UNION ALL
                   --          发货
                   (SELECT NULL                                 id,
                           a.id                                 plan_id,
                           date_format(c.datafh, '%m-%d %H:%i') jrkbill_date,
                           '发货'                                 bill_type,
                           -ifnull(c.ps, 0)                     pairs,
                           -ifnull(c.qty, 0)                    kilo,
                           c.memo                               remarks,
                           c.client                             client_name,
                           c.address                            address,
                           c.price                              price,
                           c.datafh                             order_date
                    FROM plan a
                             #                              LEFT JOIN whitebilldetailfh b ON a.id = b.planid
                             LEFT JOIN whitebillfh c ON a.id = c.planid
                    WHERE a.id = `id`
                      AND c.id IS NOT NULL)
                   UNION ALL
                   #   白坯入库
                   (SELECT NULL                                            id,
                           b.id                                            plan_id,
                           date_format(a.create_time, '%m-%d %H:%i')       jrkbill_date,
                           if(ifnull(a.storage_id, 0) > 0, '白坯退货', '加工入库') bill_type,
                           ifnull(a.input_pairs, 0)                        pairs,
                           ifnull(a.input_kilo, 0)                         kilo,
                           a.remarks                                       remarks,
                           c.company_name                                  client_name,
                           c.address                                       address,
                           a.processing_cost                               price,
                           a.create_time                                   order_date
                    FROM product_outward_order a
                             LEFT JOIN plan b ON a.plan_code = b.plan_code
                             LEFT JOIN contact_company c ON b.contact_company_id = c.id
                    WHERE a.state = 1
                      AND b.id = `id`
                      AND ((ifnull(a.is_finished_product, 0) = 1 AND length(ifnull(a.linked_order_code, '')) <= 0) OR
                           ifnull(a.storage_id, 0) = 1))
                   #     白坯发货
                   UNION ALL
                   (SELECT NULL                                      id,
                           b.id                                      plan_id,
                           date_format(a.create_time, '%m-%d %H:%i') jrkbill_date,
                           '直发出库'                                    bill_type,
                           -ifnull(a.input_pairs, 0)                 pairs,
                           -ifnull(a.input_kilo, 0)                  kilo,
                           a.remarks                                 remarks,
                           c.company_name                            client_name,
                           c.address                                 address,
                           a.processing_cost                         price,
                           a.create_time                             order_date
                    FROM product_outward_order a
                             LEFT JOIN plan b ON a.plan_code = b.plan_code
                             LEFT JOIN contact_company c ON b.contact_company_id = c.id
                    WHERE a.state = 1
                      AND b.id = `id`
                      AND ((ifnull(a.is_finished_product, 0) = 1 AND length(ifnull(a.linked_order_code, '')) > 0) AND
                           ifnull(a.storage_id, 0) <= 0))) report,
               (SELECT @storage_ps := 0) p,
               (SELECT @storage_qty := 0) q
          ORDER BY report.order_date) r
    ORDER BY r.order_date DESC;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;