DROP PROCEDURE IF EXISTS `p_product_waste`;
DELIMITER ;;
CREATE PROCEDURE `p_product_waste`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100))
BEGIN
    /**
      2021年7月24日 废布入库
     */
    SELECT a.id,
           a.linked_order_code                          linked_order_code,
           a.plan_code                                  plan_code,
           a.order_code                                 order_code,
           d.company_name                               client_name,
           concat(ifnull(b.product_name, ''), ' ', b.greyfabric_weight, '*', b.greyfabric_width, ' ',
                  ifnull(b.m_height, 0), ' ', ifnull(a.greyfabric_specification, ''), ' ',
                  ifnull(a.material_specification, '')) material_specification,
           a.input_pairs                                input_pairs,
           a.input_kilo                                 input_kilo,
           a.create_time                                create_time,
           a.handler_name                               handler_name,
           c.user_name                                  create_user_name,
           a.is_audit                                   is_audit,
           a.consignee                                  consignee
    FROM product_outward_order a
             LEFT JOIN plan b ON a.plan_code = b.plan_code
             LEFT JOIN sys_user c ON a.create_user_id = c.id
             LEFT JOIN contact_company d ON b.contact_company_id = d.id
    WHERE a.state = 1
      AND ifnull(a.storage_id, 0) = 2
      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, d.company_name = `clientName`)
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`);
END ;;
DELIMITER ;