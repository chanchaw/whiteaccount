/**
  2021年7月24日 追加自动审核配置
 */
REPLACE INTO sys_config_int(config_field, config_value, config_desc)
VALUES ('每日自动审核时间', 0, '可用值:0-24;0为关闭自动审核;1-24为当天小时');