SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_greyfabric_input
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_greyfabric_input`;
DELIMITER ;;
CREATE PROCEDURE `p_greyfabric_input`(IN `start` DATE, IN `end` DATE)
BEGIN
    /**
      2021年6月9日  白坯入库明细
      2021年7月12日 追加白坯
      2021年7月17日 追加直发
      2021年7月22日 追加制单人，制单日期，备注，经手人
     */
    SELECT rep.*
    FROM (SELECT NULL                                           id,
                 any_value(c.company_name)                      client,
                 any_value(b.plan_code)                         planCode,
                 any_value(a.cn)                                computerName,
                 CONCAT_WS(' ', any_value(b.order_code), any_value(b.product_name),
                           CONCAT_WS('*', any_value(b.greyfabric_width), any_value(b.greyfabric_weight)),
                           any_value(b.m_height), any_value(b.greyfabric_specification),
                           any_value(b.material_specification)) specification,
                 ifnull(sum(a.ps), 0)                           ps,
                 ifnull(sum(a.qty), 0)                          qty,
                 a.createdate                                   createdate,
                 any_value(a.class_name)                        className,
                 any_value(a.jth)                               machineType,
                 any_value(a.aorb)                              aorb,
                 any_value(a.batch_num)                         batchNum,
                 NULL                                           isSendDirectly,
                 a.createdate                                   createTime,
                 NULL                                           remarks,
                 NULL                                           createUserName,
                 NULL                                           handlerName,
                 NULL                                           is_audit
          FROM jrkbillsum a
                   LEFT JOIN plan b ON a.planid = b.id
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
          WHERE DATE(a.createdate) BETWEEN DATE(`start`) AND DATE(`end`)
          GROUP BY a.createdate, a.aorb

          UNION ALL

          SELECT NULL                                                                        id,
                 c.company_name                                                              client,
                 b.plan_code                                                                 planCode,
                 a.bill_type                                                                 computerName,
                 CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.m_height, b.greyfabric_specification, b.material_specification) specification,
                 ifnull(a.pairs, 0)                                                          ps,
                 ifnull(a.qty, 0)                                                            qty,
                 a.jrkbill_date                                                              createdate,
                 NULL                                                                        className,
                 NULL                                                                        machineType,
                 NULL                                                                        aorb,
                 NULL                                                                        batchNum,
                 NULL                                                                        isSendDirectly,
                 a.create_time                                                               createTime,
                 a.remarks                                                                   remarks,
                 d.user_name                                                                 createUserName,
                 NULL                                                                        handlerName,
                 NULL                                                                        is_audit
          FROM jrkbill_adjust a
                   LEFT JOIN plan b ON a.plan_id = b.id
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
                   LEFT JOIN sys_user d ON a.create_user_id = d.id
          WHERE bill_type = '手动入库'
            AND DATE(jrkbill_date) BETWEEN DATE(`start`) AND DATE(`end`)
          UNION ALL
          #         白坯入库
          SELECT a.id,
                 c.company_name                                                              client,
                 b.plan_code                                                                 planCode,
                 a.process_company                                                           computerName,
                 CONCAT_WS(' ', b.order_code, b.product_name, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.m_height, b.greyfabric_specification, a.material_specification) specification,
                 ifnull(a.input_pairs, 0)                                                    ps,
                 ifnull(a.input_kilo, 0)                                                     qty,
                 a.create_time                                                               createdate,
                 NULL                                                                        className,
                 NULL                                                                        machineType,
                 a.ab_surface                                                                aorb,
                 a.batch_num                                                                 batchNum,
                 if(ifnull(a.is_send_directly, 0) = 1, '是', '否')                             isSendDirectly,
                 a.create_time                                                               createTime,
                 a.remarks                                                                   remarks,
                 d.user_name                                                                 createUserName,
                 a.handler_name                                                              handlerName,
                 a.is_audit                                                                  is_audit
          FROM product_outward_order a
                   LEFT JOIN plan b ON a.plan_code = b.plan_code
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
                   LEFT JOIN sys_user d ON a.create_user_id = d.id
          WHERE a.state = 1
            AND (length(ifnull(a.linked_order_code, '')) <= 0 OR ifnull(a.storage_id, 0) = 1)
            AND ifnull(a.is_finished_product, 0) = 1
            AND DATE(a.create_time) BETWEEN DATE(`start`) AND DATE(`end`)) rep
    ORDER BY rep.createdate DESC;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
