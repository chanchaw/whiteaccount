/**
  2021年7月24日 默认配置
 */
REPLACE INTO sys_config_int (config_field, config_value, config_desc)
VALUES ('外发入库编号记数位数', 4, '外发入库编号记数位数'),
       ('外发入库编号日期截取起始位置', 3, '外发入库编号日期截取起始位置'),
       ('外发入库编号日期截取长度', 4, '外发入库编号日期截取长度');