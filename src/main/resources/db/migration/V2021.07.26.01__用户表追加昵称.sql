/**
  2021年7月26日 用户表追加 昵称
 */
alter table sys_user
    add nick_name VARCHAR(100) null comment '昵称' after user_name;