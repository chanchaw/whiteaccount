/**
  2021年7月26日 是否默认选中
 */
alter table sys_role_group
    add is_initial_select TINYINT(1) default 0 null comment '是否默认选中';