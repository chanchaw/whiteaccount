SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for getReceivablesDetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `getReceivablesDetail`;
DELIMITER ;;
CREATE PROCEDURE `getReceivablesDetail`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100), IN `isGather` INT)
BEGIN
    /**
        2021年7月1日  白坯应收款
     */
    IF (`isGather` = 1) THEN
        #      汇总
        SELECT rep.client client, round(sum(rep.money), 2) money
        FROM (SELECT a.client, round(sum(a.price * a.qty), 2) money
              FROM whitebillfh a
                   #                        LEFT JOIN whitebilldetailfh b ON a.id = b.fid
              WHERE a.checkout <> '现金'
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, a.client = `clientName`)
              GROUP BY a.client
              UNION ALL
              #             色布入库
              SELECT a.client, round(sum(ifnull(a.price, 0) * ifnull(a.qty, 0)), 2) money
              FROM cp_billfh a
              WHERE a.checkout <> '现金'
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, a.client = `clientName`)
              GROUP BY a.client
              UNION ALL
              #             白坯退货
              SELECT c.company_name client, -round(sum(ifnull(a.processing_cost, 0) * ifnull(a.input_kilo, 0)), 2) money
              FROM product_outward_order a
                       LEFT JOIN plan b ON a.plan_code = b.plan_code
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                       c.company_name = `clientName`)
                AND ifnull(a.is_finished_product, 0) = 1
                AND ifnull(a.storage_id, 0) > 0
              GROUP BY c.company_name
              UNION ALL
              SELECT client, round(sum(-money), 2) money
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
              GROUP BY client) rep
        GROUP BY rep.client;
    ELSE
        #         明细
        SELECT rep.*,
               if(trim(ifnull(`clientName`, '')) = '' OR ifnull(`clientName`, '') = '全部', 0,
                  @money_today := ifnull(rep.money, 0) + @money_today) accoumlatedtoday
        FROM (SELECT `start`                                                                                  billdate,
                     if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', '全部', `clientName`) client,
                     NULL                                                                                     billcode,
                     '期初结余'                                                                                   billtype,
                     0                                                                                        qty,
                     0                                                                                        ps,
                     0                                                                                        price,
                     sum(a.money)                                                                             money,
                     ''                                                                                       collectiontype,
                     ''                                                                                       billmemo,
                     0                                                                                        SKid,
                     NULL                                                                                     planCode,
                     '期初结余'                                                                                   type
              FROM (SELECT a.client, round(sum(a.price * a.qty), 2) money
                    FROM whitebillfh a
                    WHERE DATE(a.datafh) < DATE(`start`)
                      AND a.checkout <> '现金'
                      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                             a.client = `clientName`)
                    GROUP BY a.client
                    UNION ALL
                    SELECT client, round(sum(-money), 2) money
                    FROM receivables
                    WHERE DATE(billdate) < DATE(`start`)
                      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                             client = `clientName`)
                    GROUP BY client) a
              UNION ALL
              #      发货明细
              SELECT DATE(a.datafh)                                                          billdate,
                     a.client                                                                client,
                     a.billcode                                                              billcode,
                     CONCAT_WS(' ', c.plan_code, c.order_code, c.product_name,
                               CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.m_height,
                               c.greyfabric_specification, a.aorb, c.material_specification) billtype, -- 项目
                     ifnull(a.qty, 0)                                                        qty,
                     ifnull(a.ps, 0)                                                         ps,
                     ifnull(a.price, 0)                                                      price,
                     ROUND(a.qty * ifnull(a.price, 0), 2)                                    money,
                     a.checkout                                                              collectiontype,
                     a.memo                                                                  billmemo,
                     0                                                                       SKid,
                     c.plan_code                                                             planCode,
                     '白坯发货'                                                                  type      -- 单据类型
              FROM whitebillfh a
                       LEFT OUTER JOIN plan c ON a.planid = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
                AND a.checkout <> '现金'
                AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL
              #              色布发货
              SELECT DATE(a.datafh)                                                                 billdate,
                     a.client                                                                       client,
                     a.billcode                                                                     billcode,
                     CONCAT_WS(' ', '色布发货', c.order_code, c.product_name, c.material_specification) billtype,
                     ifnull(a.qty, 0)                                                               qty,
                     ifnull(a.ps, 0)                                                                ps,
                     ifnull(a.price, 0)                                                             price,
                     ROUND(a.qty * ifnull(a.price, 0), 2)                                           money,
                     a.checkout                                                                     collectiontype,
                     a.memo                                                                         billmemo,
                     0                                                                              SKid,
                     c.plan_code                                                                    planCode,
                     '色布发货'                                                                         type -- 单据类型
              FROM cp_billfh a
                       LEFT OUTER JOIN dye_fabric_plan c ON a.planid = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, a.client = `clientName`)
                AND a.checkout <> '现金'
                AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL

              #           白坯出库
              SELECT DATE_FORMAT(billdate, '%Y-%m-%d') billdate,
                     client                            client,
                     NULL                              billcode,
                     billtype                          billtype,
                     0                                 qty,
                     0                                 ps,
                     0                                 price,
                     -ifnull(money, 0)                 money,
                     collectiontype                    collectiontype,
                     billmemo                          billmemo,
                     id                                SKid,
                     NULL                              planCode,
                     billtype                          type -- 单据类型
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
                AND DATE(billdate) BETWEEN DATE(`start`) AND DATE(`end`)
              UNION ALL
              #            白坯退货
              SELECT DATE_FORMAT(a.order_date, '%Y-%m-%d')                             billdate,
                     c.company_name                                                    client,
                     a.order_code                                                      billcode,
                     '白坯退货'                                                            billtype,
                     -ifnull(a.input_kilo, 0)                                          qty,
                     -ifnull(a.input_pairs, 0)                                         ps,
                     a.processing_cost                                                 price,
                     -round(ifnull(a.processing_cost * ifnull(a.input_kilo, 0), 0), 2) money,
                     NULL                                                              collectiontype,
                     a.remarks                                                         billmemo,
                     NULL                                                              SKid,
                     b.plan_code                                                       planCode,
                     '白坯退回'                                                            type -- 单据类型
              FROM product_outward_order a
                       LEFT JOIN plan b ON a.plan_code = b.plan_code
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                       c.company_name = `clientName`)
                AND ifnull(a.storage_id, 0) > 0
                AND ifnull(a.is_finished_product, 0) = 1
                AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)) rep,
             (SELECT @money_today := 0) t
        ORDER BY rep.billdate;
    END IF;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
