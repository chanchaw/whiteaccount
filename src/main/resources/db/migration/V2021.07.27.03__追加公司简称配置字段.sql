/**
  追加公司简称配置字段
 */
REPLACE INTO sys_config_varchar (config_field, config_value, config_desc)
VALUES ('公司简称', '', '手机公众号用户名称');