SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_dye_fabric_delivery
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_dye_fabric_delivery`;
DELIMITER ;;
CREATE PROCEDURE `p_dye_fabric_delivery`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100),
                                         IN `checkout` VARCHAR(100))
BEGIN
    /**
        2021年6月25日 色布发货序时表
        2021年7月21日 追加色布出库
        2021年7月22日 追加审核字段
       2021年7月27日 按照日期到排序
     */
    SELECT report.*
    FROM ((SELECT c.company_name           client,
                  concat_ws(' ', b.contract_code, b.plan_code, b.product_name, b.material_specification, b.color_no,
                            b.color)       specification,
                  ifnull(a.input_pairs, 0) ps,
                  ifnull(a.input_kilo, 0)  qty,
                  NULL                     countable,
                  ifnull(a.price, 0)       price,
                  a.money                  money,
                  a.dye_company            checkout,
                  a.order_code             billcode,
                  a.create_time            datafh,
                  NULL                     carnumber,
                  a.remarks                memo,
                  a.is_audit               is_audit
           FROM dye_fabric_inbound a
                    LEFT JOIN dye_fabric_plan b ON a.dye_plan_code = b.plan_code
                    LEFT JOIN contact_company c ON b.contact_company_id = c.id
           WHERE a.state = 1
             AND IF(isnull(`clientName`) OR length(`clientName`) <= 0, TRUE, c.company_name = `clientName`)
             AND IF(isnull(`checkout`) OR length(`checkout`) <= 0, TRUE, ifnull(a.dye_company, '') = `checkout`)
             AND DATE(a.create_time) BETWEEN DATE(`start`) AND DATE(`end`)
             AND length(ifnull(a.linked_order_code, '')) > 0)
          UNION ALL
          (SELECT a.client                   client,
                  concat_ws(' ', b.contract_code, b.plan_code, b.product_name, b.material_specification, b.color_no,
                            b.color)         specification,
                  ifnull(a.ps, 0)            ps,
                  ifnull(a.qty, 0)           qty,
                  ifnull(a.countable, 0)     countable,
                  ifnull(a.price, 0)         price,
                  ifnull(a.price, 0) * a.qty money,
                  a.checkout                 checkout,
                  a.billcode                 billcode,
                  a.datafh                   datafh,
                  a.carnumber                carnumber,
                  a.memo                     memo,
                  NULL                       is_audit
           FROM cp_billfh a
                    LEFT JOIN dye_fabric_plan b ON a.planid = b.id
           WHERE IF(isnull(`clientName`) OR length(`clientName`) <= 0, TRUE, a.client = `clientName`)
             AND IF(isnull(`checkout`) OR length(`checkout`) <= 0, TRUE, ifnull(a.checkout, '') = `checkout`)
             AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`))) report
    ORDER BY report.datafh DESC;
END
;;
DELIMITER ;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_dye_inbound_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_dye_inbound_report`;
DELIMITER ;;
CREATE PROCEDURE `p_dye_inbound_report`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100))
BEGIN
    /**
      2021年6月27日 色布入库序时表
     */
    IF ((isnull(`clientName`) OR length(`clientName`) <= 0) OR `clientName` = '全部') THEN
        SELECT report.*
        FROM (SELECT NULL               id,
                     c.company_name     client_name,
                     b.plan_code,
                     b.product_name,
                     b.material_specification,
                     b.color_no,
                     b.color,
                     ifnull(a.pairs, 0) ps,
                     ifnull(a.qty, 0)   qty,
                     a.client_name      supplier,
                     a.bill_date        bill_date,
                     a.remarks          remarks,
                     NULL               is_send_directly,
                     NULL               is_audit
              FROM cp_adjust a
                       LEFT JOIN dye_fabric_plan b ON a.plan_id = b.id
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE a.bill_type IN ('手动入库', '盘点')
                AND DATE(a.bill_date) BETWEEN DATE(`start`) AND DATE(`end`)
              UNION ALL
              #      色布入库
              SELECT a.id                                 id,
                     b.company_name                       client_name,
                     a.dye_plan_code                      plan_code,
                     c.product_name                       product_name,
                     c.material_specification             material_specification,
                     c.color_no                           color_no,
                     c.color                              color,
                     ifnull(a.input_pairs, 0)             ps,
                     ifnull(a.input_kilo, 0)              qty,
                     a.dye_company                        supplier,
                     a.order_date                         order_date,
                     a.remarks                            remarks,
                     if(a.is_send_directly = 1, '是', '否') is_send_directly,
                     a.is_audit                           is_audit
              FROM dye_fabric_inbound a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
                       LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
              WHERE a.state = 1
                AND isnull(a.linked_order_code)
                AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)) report
        ORDER BY report.bill_date DESC;
    ELSE
        SELECT report.*
        FROM (SELECT NULL               id,
                     c.company_name     client_name,
                     b.plan_code,
                     b.product_name,
                     b.material_specification,
                     b.color_no,
                     b.color,
                     ifnull(a.pairs, 0) ps,
                     ifnull(a.qty, 0)   qty,
                     a.client_name      supplier,
                     a.bill_date,
                     a.remarks,
                     NULL               is_send_directly,
                     NULL               is_audit
              FROM cp_adjust a
                       LEFT JOIN dye_fabric_plan b ON a.plan_id = b.id
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE a.bill_type IN ('手动入库', '盘点')
                AND c.company_name = `clientName`
                AND DATE(a.bill_date) BETWEEN DATE(`start`) AND DATE(`end`)
              UNION ALL
              #      色布入库
              SELECT a.id                                 id,
                     b.company_name                       client_name,
                     a.dye_plan_code                      plan_code,
                     c.product_name,
                     c.material_specification,
                     c.color_no,
                     c.color,
                     ifnull(a.input_pairs, 0)             ps,
                     ifnull(a.input_kilo, 0)              qty,
                     a.dye_company                        supplier,
                     a.order_date,
                     a.remarks,
                     if(a.is_send_directly = 1, '是', '否') is_send_directly,
                     a.is_audit                           is_audit
              FROM dye_fabric_inbound a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
                       LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
              WHERE a.state = 1
                AND isnull(a.linked_order_code)
                AND b.company_name = `clientName`
                AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)) report
        ORDER BY report.bill_date DESC;
    END IF;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;