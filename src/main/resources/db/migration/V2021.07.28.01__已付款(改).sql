SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


DROP PROCEDURE IF EXISTS `p_account_payment`;
DELIMITER ;;
CREATE PROCEDURE `p_account_payment`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100), IN `isGather` INT)
BEGIN
    /**
      2021年7月2日 已付款存储过程
      2021年7月28日 追加客户过滤，排序
     */
    IF (`isGather` = 1) THEN
        #             汇总
        SELECT client_name, sum(money) money
        FROM material_account_payable
        WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, `clientName` = `client_name`)
          AND billtype = '付款'
          AND DATE(bill_date) BETWEEN DATE(`start`) AND DATE(`end`)
        GROUP BY client_name;
    ELSE
        #             明细
        SELECT client_name           client_name,
               billtype              billtype,
               DATE(bill_date)       bill_date,
               billmemo              billmemo,
               payable_type          payable_type,
               ifnull(money, 0)      money,
               ifnull(box_amount, 0) box_amount
        FROM material_account_payable
        WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, `clientName` = `client_name`)
          AND billtype = '付款'
          AND DATE(bill_date) BETWEEN DATE(`start`) AND DATE(`end`)
        ORDER BY bill_date;
    END IF;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
