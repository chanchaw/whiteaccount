DROP PROCEDURE IF EXISTS `test_p_insert_menu_103`;
DELIMITER ;;
CREATE PROCEDURE test_p_insert_menu_103()
BEGIN
    /***
      新增菜单 白坯产量表
     */
    INSERT INTO sys_menu (id, parent_id, menu_name, menu_path, menu_icon, serial_no)
    VALUES (202, 200, '白坯产量表', 'fh_machine_report', '', 0);
    INSERT INTO sys_permission (sys_menu_id, sys_role_id)
    SELECT m.id, b.id
    FROM sys_menu m
             JOIN sys_role b
    WHERE m.id = 202;
END ;;
DELIMITER ;
CALL test_p_insert_menu_103();
DROP PROCEDURE IF EXISTS `test_p_insert_menu_103`;