/**
  2021年7月30日 追加对账单价
 */
alter table whitebillfh
    add account_price DECIMAL(50,2) null comment '对账单价' after price;