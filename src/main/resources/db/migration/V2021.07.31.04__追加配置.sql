/**
   2021年7月31日 追加配置
 */
REPLACE INTO sys_config_int (config_field, config_value, config_desc)
VALUES ('应收账款是否允许修改本订单的所有单价', 0, '0:不允许;1:允许;');