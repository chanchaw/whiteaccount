/**
  用户常用访问表
 */
create table sys_user_visit_list
(
    id int auto_increment comment '自增主键',
    sys_user_id int null comment '用户编号',
    sys_menu_id int null comment '菜单编号',
    serial_num int null comment '排序字段',
    constraint sys_user_visit_list_pk
        primary key (id)
)
    comment '用户常用访问表';

create index sys_user_visit_list_sys_user_id_index
	on sys_user_visit_list (sys_user_id);
create index sys_user_visit_list_sys_menu_id_index
	on sys_user_visit_list (sys_menu_id);
