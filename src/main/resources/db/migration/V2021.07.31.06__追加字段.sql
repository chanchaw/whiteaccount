alter table sys_user_visit_list
    add sys_menu_parent int null comment '菜单父节点' after sys_menu_id;

alter table sys_user_visit_list
    add create_time DATETIME default CURRENT_TIMESTAMP null comment '创建时间';

create index sys_user_visit_list_sys_menu_parent_index
    on sys_user_visit_list (sys_menu_parent);