SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_material_order_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_material_order_report`;
DELIMITER ;;
CREATE PROCEDURE `p_material_order_report`(IN `start` DATE, IN `end` DATE, IN `supplierName` VARCHAR(100))
BEGIN
    /**
      2021年5月25日  原料入库序时表
      2021年7月15日  原料入库排除原料退回的单据类型
      2021年7月16日 更改单据类型
     */
    SELECT a.id,                                                                          -- 主键
           DATE(a.order_date)                                           order_date,       -- 制单日期
           a.order_code                                                 order_code,       -- 单据编号
           a.account_type,                                                                -- 付款方式
           b.supplier_name,                                                               -- 供应商名称
           a.material_spec,                                                               -- 原料规格
           a.batch_num,                                                                   -- 批次号
           if(ifnull(a.price, 0) > 0, a.input_money * d.access_mode, 0) input_money,      -- 入库金额
           ifnull(a.box_amount, 0) * d.access_mode                      box_amount,       -- 件数
           a.input_kilo * d.access_mode                                 input_kilo,       -- 入库重量
           ifnull(a.price, 0)                                           price,            -- 单价
           a.remarks,                                                                     -- 备注
           a.supplier_id,                                                                 -- 供应商编号
           c.user_name                                                  create_user_name, -- 创建人名称
           a.create_time,                                                                 -- 创建时间
           a.account_type,                                                                -- 类型
           d.bill_type_name,                                                              -- 单据类型名称
           if(ifnull(a.is_send_directly, 0) = 1, '是', '否')              is_send_directly, -- 是否直发
           a.is_audit                                                   is_audit
    FROM material_order a
             LEFT JOIN supplier b ON a.supplier_id = b.id
             LEFT JOIN sys_user c ON a.create_user_id = c.id
             LEFT JOIN bill_type d ON a.bill_type = d.id
    WHERE a.state = 1
      AND ifnull(d.company, 0) = 0
      AND if(ifnull(`supplierName`, '') = '' OR ifnull(`supplierName`, '') = '全部', 1 = 1,
             b.supplier_name = `supplierName`)
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.id DESC;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;