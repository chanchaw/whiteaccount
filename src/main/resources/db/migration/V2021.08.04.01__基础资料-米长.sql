/**
  2021年8月4日 米长
 */
create table meter_length
(
    id int auto_increment comment '自增主键',
    meter_length varchar(100) null comment '米长',
    constraint meter_length_pk
        primary key (id)
)
    comment '米长';