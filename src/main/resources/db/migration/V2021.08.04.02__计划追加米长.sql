/**
    2021年8月4日 计划追加米长
 */
alter table plan
    add meter_length VARCHAR(100) null comment '米长' after m_height;
