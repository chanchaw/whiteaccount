SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


DROP PROCEDURE IF EXISTS `getFHhistoryDetail`;
DELIMITER ;;
CREATE PROCEDURE `getFHhistoryDetail`(m_SDate DATE,
                                      m_EDate DATE,
                                      m_Client VARCHAR(100),
                                      m_CheckType VARCHAR(100))
BEGIN
    SELECT *
    FROM (SELECT NULL                                                                               id,
                 a.client                                                                           client,
                 a.address                                                                          address,
                 a.planid                                                                           planId,
                 c.plan_code                                                                        planCode,
                 CONCAT_WS(' ', c.m_height, c.meter_length, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight),
                           c.greyfabric_specification, c.material_specification, c.product_name) AS pmgg,
                 a.ps,
                 round(a.qty, 1)                                                                 AS qty,
                 round(a.price, 2)                                                               AS price,
                 if(ifnull(a.account_price, 0) > 0, round(a.account_price), round(a.price))         account_price,
                 ROUND(if(ifnull(a.account_price, 0) > 0, round(a.account_price), round(a.price)) * round(a.qty, 1),
                       2)                                                                        AS money,
                 a.checkout                                                                         checkout,
                 a.billcode                                                                         billcode,
                 c.material_lot_no                                                                  batch_num,
                 NULL                                                                               isSendDirectly,
                 a.datafh                                                                        AS b_data,
                 a.datafh                                                                           createTime,
                 memo                                                                               remarks,
                 NULL                                                                               createUserName,
                 NULL                                                                               handlerName,
                 NULL                                                                               is_audit,
                 '白坯发货'                                                                             billType
          FROM whitebillfh a
                   LEFT OUTER JOIN plan c ON a.planid = c.id
          WHERE if(ifnull(m_Client, '') = '' OR ifnull(m_Client, '') = '全部', TRUE, a.client = `m_Client`)
            AND if(ifnull(m_CheckType, '') = '' OR ifnull(m_CheckType, '') = '全部', TRUE, a.checkout = `m_CheckType`)
            AND DATE_FORMAT(a.datafh, '%Y-%m-%d') BETWEEN DATE(m_SDate) AND DATE(m_EDate)

          UNION ALL

          SELECT a.id,
                 c.company_name                                                                     client,
                 a.consignee                                                                        address,
                 b.id                                                                               planId,
                 b.plan_code                                                                        planCode,
                 CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.greyfabric_specification, b.material_specification, b.product_name) AS pmgg,
                 ifnull(a.input_pairs, 0) * if(ifnull(a.storage_id, 0) > 0, -1, 1)                  ps,
                 round(a.input_kilo, 1) * if(ifnull(a.storage_id, 0) > 0, -1, 1)                 AS qty,
                 round(a.processing_cost, 2)                                                     AS price,
                 round(a.processing_cost, 2)                                                        account_price,
                 round(a.money, 2) * if(ifnull(a.storage_id, 0) > 0, -1, 1)                      AS money,
                 if(a.is_finished_product = 1, '已剖幅', '未剖幅')                                        checkout,
                 a.order_code                                                                       billcode,
                 a.batch_num                                                                        batch_num,
                 if(a.is_send_directly = 1, '是', '否')                                               isSendDirectly,
                 a.order_date                                                                    AS b_data,
                 a.create_time                                                                      createTime,
                 a.remarks                                                                          remarks,
                 d.user_name                                                                        createUserName,
                 a.handler_name                                                                     handlerName,
                 a.is_audit                                                                         is_audit,
                 if(length(ifnull(a.linked_order_code, '')) > 0 AND ifnull(a.storage_id, 0) <= 0, '白坯出库',
                    '白坯退回')                                                                         billType
          FROM product_outward_order a
                   LEFT JOIN plan b ON a.plan_code = b.plan_code
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
                   LEFT JOIN sys_user d ON a.create_user_id = d.id
          WHERE a.state = 1
            AND if(ifnull(m_Client, '') = '' OR ifnull(m_Client, '') = '全部', TRUE, c.company_name = `m_Client`)
            AND (length(ifnull(a.linked_order_code, '')) > 0 OR ifnull(a.storage_id, 0) > 0)
            AND ifnull(a.is_finished_product, 0) = 1
            AND DATE_FORMAT(a.order_date, '%Y-%m-%d') BETWEEN DATE(`m_SDate`) AND DATE(`m_EDate`)) rep
    ORDER BY rep.b_data DESC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_greyfabric_input
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_greyfabric_input`;
DELIMITER ;;
CREATE PROCEDURE `p_greyfabric_input`(IN `start` DATE, IN `end` DATE)
BEGIN
    /**
      2021年6月9日  白坯入库明细
      2021年7月12日 追加白坯
      2021年7月17日 追加直发
      2021年7月22日 追加制单人，制单日期，备注，经手人
     */
    SELECT rep.*
    FROM (SELECT NULL                                 id,
                 any_value(c.company_name)            client,
                 any_value(b.plan_code)               planCode,
                 any_value(a.cn)                      computerName,
                 CONCAT_WS(' ', any_value(b.m_height), any_value(b.meter_length),
                           CONCAT_WS('*', any_value(b.greyfabric_width), any_value(b.greyfabric_weight)),
                           any_value(b.greyfabric_specification), any_value(b.material_specification),
                           any_value(b.product_name)) specification,
                 ifnull(sum(a.ps), 0)                 ps,
                 ifnull(sum(a.qty), 0)                qty,
                 a.createdate                         createdate,
                 any_value(a.class_name)              className,
                 any_value(a.jth)                     machineType,
                 any_value(a.aorb)                    aorb,
                 any_value(a.batch_num)               batchNum,
                 NULL                                 isSendDirectly,
                 a.createdate                         createTime,
                 NULL                                 remarks,
                 NULL                                 createUserName,
                 NULL                                 handlerName,
                 NULL                                 is_audit
          FROM jrkbillsum a
                   LEFT JOIN plan b ON a.planid = b.id
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
          WHERE DATE(a.createdate) BETWEEN DATE(`start`) AND DATE(`end`)
          GROUP BY a.createdate, a.aorb

          UNION ALL

          SELECT NULL                                                                            id,
                 c.company_name                                                                  client,
                 b.plan_code                                                                     planCode,
                 a.bill_type                                                                     computerName,
                 CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.greyfabric_specification, b.material_specification, b.product_name) specification,
                 ifnull(a.pairs, 0)                                                              ps,
                 ifnull(a.qty, 0)                                                                qty,
                 a.jrkbill_date                                                                  createdate,
                 NULL                                                                            className,
                 NULL                                                                            machineType,
                 NULL                                                                            aorb,
                 NULL                                                                            batchNum,
                 NULL                                                                            isSendDirectly,
                 a.create_time                                                                   createTime,
                 a.remarks                                                                       remarks,
                 d.user_name                                                                     createUserName,
                 NULL                                                                            handlerName,
                 NULL                                                                            is_audit
          FROM jrkbill_adjust a
                   LEFT JOIN plan b ON a.plan_id = b.id
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
                   LEFT JOIN sys_user d ON a.create_user_id = d.id
          WHERE bill_type = '手动入库'
            AND DATE(jrkbill_date) BETWEEN DATE(`start`) AND DATE(`end`)
          UNION ALL
          #         白坯入库
          SELECT a.id,
                 c.company_name                                                                  client,
                 b.plan_code                                                                     planCode,
                 a.process_company                                                               computerName,
                 CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.greyfabric_specification, b.material_specification, b.product_name) specification,
                 ifnull(a.input_pairs, 0)                                                        ps,
                 ifnull(a.input_kilo, 0)                                                         qty,
                 a.create_time                                                                   createdate,
                 NULL                                                                            className,
                 NULL                                                                            machineType,
                 a.ab_surface                                                                    aorb,
                 a.batch_num                                                                     batchNum,
                 if(ifnull(a.is_send_directly, 0) = 1, '是', '否')                                 isSendDirectly,
                 a.create_time                                                                   createTime,
                 a.remarks                                                                       remarks,
                 d.user_name                                                                     createUserName,
                 a.handler_name                                                                  handlerName,
                 a.is_audit                                                                      is_audit
          FROM product_outward_order a
                   LEFT JOIN plan b ON a.plan_code = b.plan_code
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
                   LEFT JOIN sys_user d ON a.create_user_id = d.id
          WHERE a.state = 1
            AND (length(ifnull(a.linked_order_code, '')) <= 0 OR ifnull(a.storage_id, 0) = 1)
            AND ifnull(a.is_finished_product, 0) = 1
            AND DATE(a.create_time) BETWEEN DATE(`start`) AND DATE(`end`)) rep
    ORDER BY rep.createdate DESC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_greyfabric_storage
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_greyfabric_storage`;
DELIMITER ;;
CREATE PROCEDURE `p_greyfabric_storage`(IN mode INT)
BEGIN
    /**
     2021年6月9日 白坯库存
     */
    SET @baseSql = if(mode = 1, concat("
    select re.* from (
    SELECT rep.clientName client,
       -- rep.planCode,
       -- rep.productName,
       sum(rep.ps) ps,
       sum(rep.qty) qty,
       -- avg(rep.price) price,
       p.material_lot_no  batchNum,
       sum(rep.money) money
       -- rep.specification
FROM ((SELECT c.company_name                                                              clientName,
              b.plan_code                                                                 planCode,
              b.product_name                                                              productName,
              ifnull(a.ps, 0)                                                             ps,
              ifnull(a.qty, 0)                                                            qty,
              ifnull(b.greyfabric_price, 0)                                               price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)                  money,
                CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.greyfabric_specification, b.material_specification,b.product_name)
             specification,
              a.batch_num
       FROM jrkbillsum a
                LEFT JOIN plan b ON a.planid = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

      UNION ALL

      #   收款调整
      (SELECT c.company_name                                                                    clientName,
              b.plan_code                                                                       planCode,
              b.product_name                                                                    productName,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)                              ps,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)                                qty,
              ifnull(a.price, 0)                                                                price,
              round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2) money,
              CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.greyfabric_specification, b.material_specification,b.product_name)       specification,
              NULL                                                                              batch_num
       FROM jrkbill_adjust a
                LEFT JOIN plan b ON a.plan_id = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

        union all
        #  白坯入库
        (
            SELECT c.company_name                                           clientName,
                   a.plan_code                                              planCode,
                   b.product_name                                           productName,
                   ifnull(a.input_pairs, 0)                                 ps,
                   ifnull(a.input_kilo, 0)                                  qty,
                   ifnull(a.processing_cost, 0)                             price,
                   round(a.money, 2)                                        money,
                   CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.greyfabric_specification, b.material_specification,b.product_name)               specification,
                   a.batch_num                                              batch_num
            FROM product_outward_order a
                     LEFT JOIN plan b ON a.plan_code = b.plan_code
                     LEFT JOIN contact_company c ON b.contact_company_id = c.id
            WHERE a.state = 1
              AND (ifnull(a.is_send_directly, 0) = 0 or ifnull(a.storage_id,0)=1)
              AND ifnull(a.is_finished_product, 0) = 1
        )
        union all
        # 发货管理
        (SELECT a.client                                                       clientName,
               c.plan_code                                                     planCode,
               c.product_name                                                  productName,
               -ifnull(a.ps, 0)                                                ps,
               -ifnull(a.qty, 0)                                               qty,
               ifnull(a.price, 0)                                              price,
               round(ifnull(a.price, 0) * -ifnull(a.qty, 0), 2)                 money,
               CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.greyfabric_specification, b.material_specification,b.product_name) specification,
               c.material_lot_no                                               batch_num
        FROM whitebillfh a
#                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON a.planid = c.id)
                ) rep
        left join contact_company co on rep.clientName = co.company_name
        left join plan p on rep.planCode = p.plan_code
GROUP BY rep.clientName,co.id order by co.id ) re where abs(ifnull(re.ps,0))>0; "), concat("
    select re.*,p.material_lot_no batchNum  from (
    SELECT any_value(rep.clientName) client,
       rep.planCode,
       any_value(rep.productName) productName,
       sum(rep.ps) ps,
       sum(rep.qty) qty,
       avg(rep.price) price,
       sum(rep.money) money,
       any_value(rep.specification) specification
FROM ((SELECT c.company_name                                                              clientName,
              b.plan_code                                                                 planCode,
              b.product_name                                                              productName,
              ifnull(a.ps, 0)                                                             ps,
              ifnull(a.qty, 0)                                                            qty,
              ifnull(b.greyfabric_price, 0)                                               price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)                  money,
              CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.greyfabric_specification, b.material_specification,b.product_name) specification,
              a.batch_num
       FROM jrkbillsum a
                LEFT JOIN plan b ON a.planid = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)

      UNION ALL

      #   收款调整
      (SELECT c.company_name                                                                    clientName,
              b.plan_code                                                                       planCode,
              b.product_name                                                                    productName,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)                              ps,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)                                qty,
              ifnull(a.price, 0)                                                                price,
              round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2) money,
              CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.greyfabric_specification, b.material_specification,b.product_name)       specification,
              null      batch_num
       FROM jrkbill_adjust a
                LEFT JOIN plan b ON a.plan_id = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id)
       union all
        (
            SELECT c.company_name                                           clientName,
                   a.plan_code                                              planCode,
                   b.product_name                                           productName,
                   ifnull(a.input_pairs, 0)                                 ps,
                   ifnull(a.input_kilo, 0)                                  qty,
                   ifnull(a.processing_cost, 0)                             price,
                   round(a.money, 2)                                        money,
                   CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.greyfabric_specification, b.material_specification,b.product_name) specification,
                   a.batch_num
            FROM product_outward_order a
                     LEFT JOIN plan b ON a.plan_code = b.plan_code
                     LEFT JOIN contact_company c ON b.contact_company_id = c.id
            WHERE a.state = 1
              AND (ifnull(a.is_send_directly, 0) = 0 or ifnull(a.storage_id,0)=1)
              AND ifnull(a.is_finished_product, 0) = 1
        )
       union all
       # 发货管理
        (SELECT a.client                                                        clientName,
               c.plan_code                                                     planCode,
               c.product_name                                                  productName,
               -ifnull(a.ps, 0)                                                ps,
               -ifnull(a.qty, 0)                                               qty,
               ifnull(a.price, 0)                                              price,
               round(ifnull(a.price, 0) * -ifnull(a.qty, 0), 2)                 money,
               CONCAT_WS(' ', c.m_height, c.meter_length, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight),
                           c.greyfabric_specification, c.material_specification,c.product_name) specification,
               c.material_lot_no                                               batch_num
        FROM whitebillfh a
#                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
                 LEFT JOIN plan c ON a.planid = c.id)
                ) rep
        left join contact_company co on rep.clientName = co.company_name
GROUP BY rep.planCode ) re
left join plan p on re.planCode = p.plan_code
where abs(ifnull(re.ps,0))>0; "));
    PREPARE stat FROM @baseSql;
    EXECUTE stat;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for p_product_waste
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_product_waste`;
DELIMITER ;;
CREATE PROCEDURE `p_product_waste`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100))
BEGIN
    /**
      2021年7月24日 废布入库
     */
    SELECT a.id,
           a.linked_order_code                                                             linked_order_code,
           a.plan_code                                                                     plan_code,
           a.order_code                                                                    order_code,
           d.company_name                                                                  client_name,
           CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                     b.greyfabric_specification, b.material_specification, b.product_name) material_specification,
           a.input_pairs                                                                   input_pairs,
           a.input_kilo                                                                    input_kilo,
           a.create_time                                                                   create_time,
           a.handler_name                                                                  handler_name,
           c.user_name                                                                     create_user_name,
           a.is_audit                                                                      is_audit,
           a.consignee                                                                     consignee
    FROM product_outward_order a
             LEFT JOIN plan b ON a.plan_code = b.plan_code
             LEFT JOIN sys_user c ON a.create_user_id = c.id
             LEFT JOIN contact_company d ON b.contact_company_id = d.id
    WHERE a.state = 1
      AND ifnull(a.storage_id, 0) = 2
      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, d.company_name = `clientName`)
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`);
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;