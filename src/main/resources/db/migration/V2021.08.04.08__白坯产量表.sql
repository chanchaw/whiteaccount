SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_greyfabric_jrkbill
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_greyfabric_jrkbill`;
DELIMITER ;;
CREATE PROCEDURE `p_greyfabric_jrkbill`(IN `start` DATE, IN `end` DATE, IN `machineNum` VARCHAR(100))
BEGIN
    /**
      2021年7月28日 白坯产量
      2021年8月4日 更改规格拼接字段
     */
    DROP TEMPORARY TABLE IF EXISTS t_tmp;
    DROP TEMPORARY TABLE IF EXISTS t_tmp_sub;
    CREATE TEMPORARY TABLE t_tmp
    (
        pk_id         BIGINT AUTO_INCREMENT PRIMARY KEY,
        jth           VARCHAR(100),
        createdate    DATETIME,
        specification VARCHAR(200),
        qty           DECIMAL(60, 1),
        sumqty        DECIMAL(60, 1),
        serialNum     INT
    );
    CREATE TEMPORARY TABLE t_tmp_sub
    (
        pk_id         BIGINT AUTO_INCREMENT PRIMARY KEY,
        jth           VARCHAR(100),
        createdate    DATETIME,
        specification VARCHAR(200),
        qty           DECIMAL(60, 1),
        sumqty        DECIMAL(60, 1),
        serialNum     INT
    );
    IF length(ifnull(`machineNum`, '')) > 0 THEN
        -- 有机台号参数
        INSERT INTO t_tmp (jth, createdate, specification, qty, sumqty, serialNum)
        SELECT z.jth, z.createdate createdate, z.specification, z.qty, z.sumqty, z.serialNum
        FROM (SELECT y.*,
                     if(@spec = y.specification, @amount := @amount + ifnull(y.qty, 0),
                        @amount := ifnull(y.qty, 0))                           sumqty,
                     if(@spec = y.specification, 0, 1)                         is_last_one,
                     if(@spec = y.specification, @num := @num + 1, @num := 1)  serialNum,
                     if(@spec = y.specification, '', @spec := y.specification) currspec
              FROM (SELECT x.jth, x.specification, sum(x.qty) qty, any_value(x.createdate) createdate
                    FROM (SELECT a.jth                                 jth,
                                 a.createdate                          createdate,
                                 CONCAT_WS(' ', b.m_height, b.meter_length,
                                           CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                                           b.greyfabric_specification) specification,
                                 ifnull(a.qty, 0)                      qty
                          FROM jrkbillsum a
                                   LEFT JOIN plan b ON a.planid = b.id
                          WHERE ifnull(a.jth, '') = `machineNum`
                            AND DATE(a.createdate) BETWEEN DATE(`start`) AND DATE(`end`)) x
                    GROUP BY DATE(x.createdate), x.specification, x.jth ASC) y,
                   (SELECT @amount := 0) num,
                   (SELECT @spec := '') spec,
                   (SELECT @num := 0) serialNum
              ORDER BY y.specification, y.createdate) z
        ORDER BY z.createdate, z.serialNum;
        INSERT INTO t_tmp_sub (jth, createdate, specification, qty, sumqty, serialNum)
        SELECT jth, createdate, specification, qty, sumqty, serialNum
        FROM t_tmp;
        -- 查询出记录
        SELECT a.*, if(a.serialNum < b.serialNum, 0, 1) is_last_one, b.serialNum ccc, a.serialNum ddd
        FROM t_tmp a
                 LEFT JOIN (SELECT max(serialNum) serialNum, any_value(DATE(createdate)) createdate, specification, jth
                            FROM t_tmp_sub
                            GROUP BY specification, jth) b ON a.specification = b.specification AND a.jth = b.jth;
    ELSE
        -- 没有机台号参数
        INSERT INTO t_tmp (jth, createdate, specification, qty, sumqty, serialNum)
        SELECT z.jth, NULL createdate, z.specification, z.qty, z.sumqty, z.serialNum
        FROM (SELECT y.*,
                     if(@spec = y.specification, @amount := @amount + ifnull(y.qty, 0),
                        @amount := ifnull(y.qty, 0))                           sumqty,
                     if(@spec = y.specification, 0, 1)                         is_last_one,
                     if(@spec = y.specification, @num := @num + 1, @num := 1)  serialNum,
                     if(@spec = y.specification, '', @spec := y.specification) currspec
              FROM (SELECT x.jth, x.specification, sum(x.qty) qty
                    FROM (SELECT a.jth                                 jth,
                                 #                a.createdate                                                                          createdate,
                                 CONCAT_WS(' ', b.m_height, b.meter_length,
                                           CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                                           b.greyfabric_specification) specification,
                                 ifnull(a.qty, 0)                      qty
                          FROM jrkbillsum a
                                   LEFT JOIN plan b ON a.planid = b.id
                          WHERE DATE(a.createdate) BETWEEN DATE(`start`) AND DATE(`end`)) x
                    GROUP BY x.specification, x.jth ASC) y,
                   (SELECT @amount := 0) num,
                   (SELECT @spec := '') spec,
                   (SELECT @num := 0) serialNum
              ORDER BY y.specification, y.jth) z
                 LEFT JOIN machine_type m ON z.jth = m.machine_name
        ORDER BY m.serial_num, z.serialNum;
        INSERT INTO t_tmp_sub (jth, createdate, specification, qty, sumqty, serialNum)
        SELECT jth, createdate, specification, qty, sumqty, serialNum
        FROM t_tmp;
        -- 查询出记录
        SELECT a.*, if(a.serialNum < b.serialNum, 0, 1) is_last_one
        FROM t_tmp a
                 LEFT JOIN (SELECT max(serialNum) serialNum, specification FROM t_tmp_sub GROUP BY specification) b
                           ON a.specification = b.specification;
    END IF;
    -- 执行完成后删除临时表
    DROP TEMPORARY TABLE IF EXISTS t_tmp;
    DROP TEMPORARY TABLE IF EXISTS t_tmp_sub;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;