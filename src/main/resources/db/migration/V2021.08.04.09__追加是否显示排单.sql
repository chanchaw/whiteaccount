/**
  2021年8月4日 追加是否显示排单
 */
alter table plan
    add is_show_order TINYINT(1) default 0 null comment '是否显示排单(0:否;1:是;)';