SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_plan_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_plan_report`;
DELIMITER ;;
CREATE PROCEDURE `p_plan_report`(IN `start` DATE, IN `end` DATE, IN `markFinished` TINYINT(1))
BEGIN
    /**
        2021年5月18日 计划单序时表
        2021年5月26日 追加是否完工字段
        2021年7月20日 机台号排序
     */

    select config_value INTO @value from sys_config_int WHERE config_field = '白坯计划排序规则';

    set @baseSql = concat("
        SELECT a.id,
           a.contact_company_id,
           b.company_name,
           a.contract_code,
           a.plan_code,
           a.plan_date,
           a.order_code,
           a.product_name,
           a.material_specification,
           a.material_lot_no,
           a.material_supplier_id,
           d.supplier_name                         material_supplier_name,
           a.greyfabric_weight,
           a.greyfabric_width,
           a.greyfabric_specification,
           a.greyfabric_price,
           a.m_height,
           a.plan_kilo,
           a.greyfabric_date,
           a.meter_length,
           a.submit_date,
           a.material_price,
           a.processing_cost,
           a.middle_specification,
           a.middle_supplier_id,
           e.supplier_name                         middle_supplier_name,
           a.bottom_specification,
           a.bottom_supplier_id,
           c.supplier_name                         bottom_supplier_name,
           a.machine_type,
           if(ifnull(a.is_show_order,0)=1,'是','否')                         is_show_order,
           a.needle_amount,
           a.low_weight,
           a.low_width,
           a.plan_employee_id,
           h.emp_name                              plan_employee_name,
           a.product_employee_id,
           i.emp_name                              product_employee_name,
           a.check_employee_id,
           g.emp_name                              check_employee_name,
           a.mark_company_name,
           a.fabric_requirement,
           a.remarks,
           a.create_user_id,
           ifnull(a.mark_finished, 0)              mark_finished,
           j.user_name                             create_user_name,
           a.create_time,
           a.state,
           ifnull(k.pairs, 0)                      input_pairs,
           ifnull(k.kilo, 0)                       input_kilo,
           ifnull(l.kilo, 0) + ifnull(n.kilo, 0)   delivery_kilo,
           ifnull(l.pairs, 0) + ifnull(n.pairs, 0) delivery_pairs,
           ifnull(k.pairs, 0) + ifnull(p.input_pairs, 0) - ifnull(l.pairs, 0) - ifnull(n.pairs, 0) +
           ifnull(m.pairs, 0)                      remain_pairs,
           ifnull(k.kilo, 0) + ifnull(p.input_kilo, 0) - ifnull(l.kilo, 0) - ifnull(n.kilo, 0) +
           ifnull(m.kilo, 0)                       remain_kilo,
           a.notice_date                           notice_date,
           o.jth                                   jth,
           a.is_audit                              is_audit
    FROM plan a
        LEFT JOIN contact_company b ON a.contact_company_id = b.id
        LEFT JOIN supplier c ON a.bottom_supplier_id = c.id
        LEFT JOIN supplier d ON a.material_supplier_id = d.id
        LEFT JOIN supplier e ON a.middle_supplier_id = e.id
        LEFT JOIN employee g ON a.check_employee_id = g.id
        LEFT JOIN employee h ON a.plan_employee_id = h.id
        LEFT JOIN employee i ON a.product_employee_id = i.id
        LEFT JOIN sys_user j ON a.create_user_id = j.id
        LEFT JOIN (
        /* 打卷数据 */
        SELECT a.id, ifnull(b.pairs, 0) + ifnull(c.pairs, 0) pairs, ifnull(b.kilo, 0) + ifnull(c.kilo, 0) kilo
        FROM plan a
        LEFT JOIN (SELECT planid, sum(ps) pairs, sum(qty) kilo FROM jrkbillsum GROUP BY planid) b
        ON a.id = b.planid
        LEFT JOIN (SELECT plan_id, sum(ifnull(pairs, 0)) pairs, sum(ifnull(qty, 0)) kilo
        FROM jrkbill_adjust
        WHERE state = 1
        AND bill_type = '手动入库'
        GROUP BY plan_id) c ON a.id = c.plan_id) k ON k.id = a.id
        LEFT JOIN (
        /* 发货数据 */
        SELECT ifnull(sum(a.ps), 0) pairs, ifnull(sum(a.qty), 0) kilo, a.planid
        FROM whitebillfh a
        #              LEFT JOIN whitebilldetailfh b ON a.id = b.fid
        GROUP BY a.planid) l ON l.planid = a.id
        LEFT JOIN (
        /* 盘点 */
        SELECT plan_id, ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo
        FROM jrkbill_adjust
        WHERE bill_type = '盘点'
        GROUP BY plan_id) m ON m.plan_id = a.id
        LEFT JOIN (
        /* 出库 */
        SELECT plan_id, ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo
        FROM jrkbill_adjust
        WHERE bill_type = '手动出库'
        GROUP BY plan_id) n ON n.plan_id = a.id
        /* 机台 */
        LEFT JOIN (SELECT planid,
        group_concat(DISTINCT ifnull(jth, '') ORDER BY cast(ifnull(jth, '') AS DECIMAL) SEPARATOR
        ',') jth
        FROM plan_jth
        GROUP BY planid) o ON o.planid = a.id
        LEFT JOIN (
        #    白坯退货+白坯入库
        SELECT sum(a.input_pairs) input_pairs, sum(a.input_kilo) input_kilo, a.plan_code
        FROM product_outward_order a
        WHERE a.state = 1
        AND (ifnull(a.is_send_directly, 0) = 0 OR ifnull(a.storage_id, 0) = 1)
        AND ifnull(a.is_finished_product, 0) = 1
        GROUP BY a.plan_code) p ON p.plan_code = a.plan_code
    WHERE a.state = 1
      AND if(length(",ifnull(`markFinished`,'""'),")<=0, TRUE, ifnull(a.mark_finished, 0) = ",ifnull(`markFinished`,0),")
        AND if(length(",ifnull(`markFinished`,'""'),")<=0, DATE(a.plan_date) BETWEEN DATE('",`start`,"') AND DATE('",`end`,"'), TRUE)
        ORDER BY ",if(ifnull( @value,0)=1,
                      " ifnull(a.mark_finished, 0), a.meter_length, a.m_height",
                      " ifnull(a.mark_finished, 0), a.create_time "),"

");
    prepare statement from @baseSql;
    execute statement;

END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
