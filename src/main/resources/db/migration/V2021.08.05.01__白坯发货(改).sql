SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `getFHhistoryDetail`;
delimiter ;;
CREATE PROCEDURE `getFHhistoryDetail`(m_SDate DATE,
                                      m_EDate DATE,
                                      m_Client VARCHAR(100),
                                      m_CheckType VARCHAR(100))
BEGIN
    SELECT *
    FROM (SELECT NULL                                                                               id,
                 a.client                                                                           client,
                 a.address                                                                          address,
                 a.planid                                                                           planId,
                 c.plan_code                                                                        planCode,
                 CONCAT_WS(' ', c.m_height, c.meter_length, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight),
                           c.greyfabric_specification, c.material_specification, c.product_name) AS pmgg,
                 a.ps,
                 round(a.qty, 1)                                                                 AS qty,
                 round(a.price, 2)                                                               AS price,
                 if(ifnull(a.account_price, 0) > 0, round(a.account_price,2), round(a.price,2))         account_price,
                 ROUND(if(ifnull(a.account_price, 0) > 0, round(a.account_price,2), round(a.price,2)) * round(a.qty, 1),
                       2)                                                                        AS money,
                 a.checkout                                                                         checkout,
                 a.billcode                                                                         billcode,
                 c.material_lot_no                                                                  batch_num,
                 NULL                                                                               isSendDirectly,
                 a.datafh                                                                        AS b_data,
                 a.datafh                                                                           createTime,
                 memo                                                                               remarks,
                 NULL                                                                               createUserName,
                 NULL                                                                               handlerName,
                 NULL                                                                               is_audit,
                 '白坯发货'                                                                             billType
          FROM whitebillfh a
                   LEFT OUTER JOIN plan c ON a.planid = c.id
          WHERE if(ifnull(m_Client, '') = '' OR ifnull(m_Client, '') = '全部', TRUE, a.client = `m_Client`)
            AND if(ifnull(m_CheckType, '') = '' OR ifnull(m_CheckType, '') = '全部', TRUE, a.checkout = `m_CheckType`)
            AND DATE_FORMAT(a.datafh, '%Y-%m-%d') BETWEEN DATE(m_SDate) AND DATE(m_EDate)

          UNION ALL

          SELECT a.id,
                 c.company_name                                                                     client,
                 a.consignee                                                                        address,
                 b.id                                                                               planId,
                 b.plan_code                                                                        planCode,
                 CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.greyfabric_specification, b.material_specification, b.product_name) AS pmgg,
                 ifnull(a.input_pairs, 0) * if(ifnull(a.storage_id, 0) > 0, -1, 1)                  ps,
                 round(a.input_kilo, 1) * if(ifnull(a.storage_id, 0) > 0, -1, 1)                 AS qty,
                 round(a.processing_cost, 2)                                                     AS price,
                 round(a.processing_cost, 2)                                                        account_price,
                 round(a.money, 2) * if(ifnull(a.storage_id, 0) > 0, -1, 1)                      AS money,
                 if(a.is_finished_product = 1, '已剖幅', '未剖幅')                                        checkout,
                 a.order_code                                                                       billcode,
                 a.batch_num                                                                        batch_num,
                 if(a.is_send_directly = 1, '是', '否')                                               isSendDirectly,
                 a.order_date                                                                    AS b_data,
                 a.create_time                                                                      createTime,
                 a.remarks                                                                          remarks,
                 d.user_name                                                                        createUserName,
                 a.handler_name                                                                     handlerName,
                 a.is_audit                                                                         is_audit,
                 if(length(ifnull(a.linked_order_code, '')) > 0 AND ifnull(a.storage_id, 0) <= 0, '白坯出库',
                    '白坯退回')                                                                         billType
          FROM product_outward_order a
                   LEFT JOIN plan b ON a.plan_code = b.plan_code
                   LEFT JOIN contact_company c ON b.contact_company_id = c.id
                   LEFT JOIN sys_user d ON a.create_user_id = d.id
          WHERE a.state = 1
            AND if(ifnull(m_Client, '') = '' OR ifnull(m_Client, '') = '全部', TRUE, c.company_name = `m_Client`)
            AND (length(ifnull(a.linked_order_code, '')) > 0 OR ifnull(a.storage_id, 0) > 0)
            AND ifnull(a.is_finished_product, 0) = 1
            AND DATE_FORMAT(a.order_date, '%Y-%m-%d') BETWEEN DATE(`m_SDate`) AND DATE(`m_EDate`)) rep
    ORDER BY rep.b_data DESC;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;