/**
  2021年8月5日 更改配置说明
 */
UPDATE sys_config_int
SET config_desc = '0:按照日期;1:按照米长毛高;'
WHERE config_field = '白坯计划排序规则';
DELETE FROM sys_config_int WHERE config_field = '计划单排单显示';