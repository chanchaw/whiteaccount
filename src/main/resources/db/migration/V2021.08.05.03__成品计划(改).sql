SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Procedure structure for p_dye_fabric_plan_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_dye_fabric_plan_report`;
delimiter ;;
CREATE PROCEDURE `p_dye_fabric_plan_report`(IN `start` DATE, IN `end` DATE, IN `markFinished` TINYINT(1))
BEGIN
    /**
      2021年6月24日 色布计划序时表
      2021年6月27日 按照日期到排序
      2021年6月28日 追加入库出库数量
      2021年7月21日 更改逻辑
     */
    SELECT a.id                                                            id,
           DATE(a.plan_date)                                               plan_date,
           a.contact_company_id                                            contact_company_id,
           b.company_name                                                  company_name,
           a.contract_code                                                 contract_code,
           a.plan_code                                                     plan_code,
           a.order_code                                                    order_code,
           a.product_name                                                  product_name,
           a.process_type                                                  process_type,
           a.material_specification                                        material_specification,
           a.color_no                                                      material_specification,
           a.color                                                         color,
           ifnull(a.net_diff, 0)                                           net_diff,
           ifnull(a.plan_kilo, 0)                                          plan_kilo,
           ifnull(a.rate, 0)                                               rate,
           ifnull(a.material_price, 0)                                     material_price,
           a.plan_employee_id                                              plan_employee_id,
           c.emp_name                                                      plan_employee_name,
           DATE(a.submit_date)                                             submit_date,
           a.create_user_id                                                create_user_id,
           d.user_name                                                     create_user_name,
           a.remarks                                                       remarks,
           ifnull(e.pairs, 0) + ifnull(x.input_pairs, 0)                   input_pairs,
           ifnull(f.ps, 0) + ifnull(g.pairs, 0) + ifnull(y.input_pairs, 0) delivery_pairs,
           ifnull(e.pairs, 0) + ifnull(x.input_pairs, 0) - ifnull(y.input_pairs, 0) - ifnull(f.ps, 0) -
           ifnull(g.pairs, 0)                                              remain_pairs,
           ifnull(e.qty, 0) + ifnull(x.input_kilo, 0)                      input_kilo,
           ifnull(f.qty, 0) + ifnull(g.qty, 0) + ifnull(y.input_kilo, 0)   delivery_kilo,
           ifnull(e.qty, 0) + ifnull(x.input_kilo, 0) - ifnull(y.input_kilo, 0) - ifnull(f.qty, 0) -
           ifnull(g.qty, 0)                                                remain_kilo,
           ifnull(a.mark_finished, 0)                                      mark_finished,
           a.is_audit                                                      is_audit
    FROM dye_fabric_plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN employee c ON a.plan_employee_id = c.id
             LEFT JOIN sys_user d ON a.create_user_id = d.id
             LEFT JOIN (
        #         调整入库
        SELECT sum(pairs) pairs, sum(qty) qty, plan_id
        FROM cp_adjust
        WHERE bill_type IN ('手动入库', '盘点')
        GROUP BY plan_id) e ON e.plan_id = a.id
             LEFT JOIN (
        #      色布入库
        SELECT dye_plan_code, sum(input_kilo) input_kilo, sum(input_pairs) input_pairs, sum(input_meter) input_meter
        FROM dye_fabric_inbound
        WHERE state = 1
          AND isnull(linked_order_code)
        GROUP BY dye_plan_code) x ON x.dye_plan_code = a.plan_code
             LEFT JOIN (
        #         出库
        SELECT sum(ps) ps, sum(qty) qty, planid FROM cp_billfh GROUP BY planId) f ON f.planId = a.id
             LEFT JOIN (
        #             色布出库
        SELECT dye_plan_code, sum(input_kilo) input_kilo, sum(input_pairs) input_pairs, sum(input_meter) input_meter
        FROM dye_fabric_inbound
        WHERE state = 1
          AND length(ifnull(linked_order_code, '')) > 0
        GROUP BY dye_plan_code) y ON y.dye_plan_code = a.plan_code
             LEFT JOIN (
        #         出库调整
        SELECT sum(pairs) pairs, sum(qty) qty, plan_id FROM cp_adjust WHERE bill_type = '手动出库' GROUP BY plan_id) g
                       ON g.plan_id = a.id
    WHERE a.state = 1
      AND if(isnull(`markFinished`), 1 = 1, ifnull(a.mark_finished, 0) = `markFinished`)
      AND DATE(a.plan_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.id DESC;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
