/**
  禁止保存计划单上面多个要素完全相同的单据
 */
REPLACE INTO sys_config_int (config_field, config_value, config_desc)
VALUES ('禁止保存计划单上面多个要素完全相同的单据', 1, '0表示关闭;1表示开启');