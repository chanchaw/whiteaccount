DROP PROCEDURE IF EXISTS p_plan_report_dtl;
CREATE PROCEDURE p_plan_report_dtl(IN `p_state` INT, IN `markFinished` TINYINT(1))
BEGIN
    /**
     2021年8月7日 白坯计划存储过程
     */
    SELECT id                       id,
           contact_company_id       contactCompanyId,
           contract_code            contractCode,
           plan_code                planCode,
           plan_date                planDate,
           notice_date              noticeDate,
           order_code               orderCode,
           product_name             productName,
           material_specification   materialSpecification,
           material_lot_no          materialLotNo,
           material_supplier_id     materialSupplierId,
           greyfabric_weight        greyfabricWeight,
           greyfabric_width         greyfabricWidth,
           greyfabric_specification greyfabricSpecification,
           greyfabric_price         greyfabricPrice,
           m_height                 mHeight,
           meter_length             meterLength,
           plan_kilo                planKilo,
           greyfabric_date          greyfabricDate,
           submit_date              submitDate,
           material_price           materialPrice,
           processing_cost          processingCost,
           middle_specification     middleSpecification,
           middle_supplier_id       middleSupplierId,
           middle_tf                middleTf,
           bottom_specification     bottomSpecification,
           bottom_supplier_id       bottomSupplierId,
           bottom_tf                bottomTf,
           machine_type             machineType,
           needle_amount            needleAmount,
           low_weight               lowWeight,
           low_width                lowWidth,
           plan_employee_id         planEmployeeId,
           product_employee_id      productEmployeeId,
           check_employee_id        checkEmployeeId,
           mark_company_name        markCompanyName,
           fabric_requirement       fabricRequirement,
           remarks                  remarks,
           create_user_id           createUserId,
           cancel_user_id           cancelUserId,
           create_time              createTime,
           mark_finished            markFinished,
           state                    state,
           upload                   upload,
           is_marking               isMarking,
           is_audit                 isAudit,
           audit_user_id            auditUserId,
           is_show_order            isShowOrder
    FROM plan
    WHERE state = `p_state`
      AND mark_finished = `markFinished`
    ORDER BY ifnull(mark_finished, 0), CAST(m_height AS DECIMAL(10, 2)), m_height, meter_length;
END;