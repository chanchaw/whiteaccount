/**
     2021年8月7日   追加字段
 */
alter table dye_fabric_plan
    add plan_pairs DECIMAL(60,1) default 0 null comment '计划匹数' after meter_counter;

alter table dye_fabric_plan
    add plan_use_pairs DECIMAL(60,1) default 0 null comment '投坯匹数' after plan_kilo;

alter table dye_fabric_plan
    add plan_use_kilo DECIMAL(60,1) default 0 null comment '投坯公斤' after plan_use_pairs;