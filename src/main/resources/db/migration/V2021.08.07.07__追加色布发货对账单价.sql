/**
  2021年8月7日 追加色布发货对账单价
 */
alter table dye_fabric_inbound
    add account_price DECIMAL(60,2) default 0 null comment '对账单价';