/**
  2021年8月7日 追加对账单价
 */
alter table cp_billfh
    add account_price DECIMAL(60,2) default 0 null comment '对账单价' after qty;