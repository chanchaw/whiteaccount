SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `getReceivablesDetail`;
DELIMITER ;;
CREATE PROCEDURE `getReceivablesDetail`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100), IN `isGather` INT)
BEGIN
    /**
        2021年7月1日  白坯应收款
        2021年7月30日 追加ID
     */
    IF (`isGather` = 1) THEN
        #      汇总
        SELECT rep.client client, round(sum(rep.money), 2) money
        FROM (SELECT a.client, round(sum(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price) * a.qty), 2) money
              FROM whitebillfh a
                   #                        LEFT JOIN whitebilldetailfh b ON a.id = b.fid
              WHERE a.checkout <> '现金'
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, a.client = `clientName`)
              GROUP BY a.client
              UNION ALL
              #             色布入库
              SELECT a.client,
                     round(sum(if(ifnull(a.account_price, 0) > 0, ifnull(a.account_price, 0), ifnull(a.price, 0)) *
                               a.qty), 2) money
              FROM cp_billfh a
              WHERE a.checkout <> '现金'
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, a.client = `clientName`)
              GROUP BY a.client
              UNION ALL
              #             色布出库
              SELECT b.company_name                client,
                     - round(sum(if(ifnull(a.account_price, 0) > 0, ifnull(a.account_price, 0), ifnull(a.price, 0)) *
                                 a.input_kilo), 2) money
              FROM dye_fabric_inbound a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
              WHERE a.state = 1
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                       b.company_name = `clientName`)
                AND length(ifnull(a.linked_order_code, '')) > 0
              GROUP BY a.contact_company_id
              UNION ALL
              #             白坯退货
              SELECT c.company_name client, -round(sum(ifnull(a.processing_cost, 0) * ifnull(a.input_kilo, 0)), 2) money
              FROM product_outward_order a
                       LEFT JOIN plan b ON a.plan_code = b.plan_code
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                       c.company_name = `clientName`)
                AND ifnull(a.is_finished_product, 0) = 1
                AND ifnull(a.storage_id, 0) > 0
              GROUP BY c.company_name
              UNION ALL
              SELECT client, round(sum(-money), 2) money
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, client = `clientName`)
              GROUP BY client) rep
        GROUP BY rep.client;
    ELSE
        #         明细
        SELECT rep.*,
               if(trim(ifnull(`clientName`, '')) = '' OR ifnull(`clientName`, '') = '全部', 0,
                  @money_today := ifnull(rep.money, 0) + @money_today) accoumlatedtoday
        FROM (SELECT `start`                                                                                  billdate,
                     if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', '全部', `clientName`) client,
                     NULL                                                                                     billcode,
                     '期初结余'                                                                                   billtype,
                     0                                                                                        qty,
                     0                                                                                        ps,
                     0                                                                                        price,
                     0                                                                                        account_price,
                     sum(a.money)                                                                             money,
                     ''                                                                                       collectiontype,
                     ''                                                                                       billmemo,
                     0                                                                                        SKid,
                     0                                                                                        whitebillfhid,
                     0                                                                                        dye_fabric_inbound_id,
                     0                                                                                        cp_billfh_id,
                     NULL                                                                                     planCode,
                     NULL                                                                                     planid,
                     '期初结余'                                                                                   type
              FROM (SELECT a.client,
                           round(sum(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price) * a.qty), 2) money
                    FROM whitebillfh a
                    WHERE DATE(a.datafh) < DATE(`start`)
                      AND a.checkout <> '现金'
                      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                             a.client = `clientName`)
                    GROUP BY a.client
                    UNION ALL
                    SELECT client, round(sum(-money), 2) money
                    FROM receivables
                    WHERE DATE(billdate) < DATE(`start`)
                      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                             client = `clientName`)
                    GROUP BY client) a
              UNION ALL
              #      发货明细
              SELECT DATE(a.datafh)                                                                            billdate,
                     a.client                                                                                  client,
                     a.billcode                                                                                billcode,
                     CONCAT_WS(' ', c.plan_code, c.order_code, c.m_height, c.meter_length,
                               CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.greyfabric_specification,
                               c.material_specification,
                               c.product_name)                                                                 billtype, -- 项目
                     ifnull(a.qty, 0)                                                                          qty,
                     ifnull(a.ps, 0)                                                                           ps,
                     ifnull(a.price, 0)                                                                        price,
                     if(ifnull(a.account_price, 0) > 0, a.account_price, a.price)                              account_price,
                     ROUND(a.qty * ifnull(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0), 2) money,
                     a.checkout                                                                                collectiontype,
                     a.memo                                                                                    billmemo,
                     0                                                                                         SKid,
                     a.id                                                                                      whitebillfhid,
                     0                                                                                         dye_fabric_inbound_id,
                     0                                                                                         cp_billfh_id,
                     c.plan_code                                                                               planCode,
                     a.planid                                                                                  planid,
                     '白坯发货'                                                                                    type      -- 单据类型
              FROM whitebillfh a
                       LEFT OUTER JOIN plan c ON a.planid = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, client = `clientName`)
                AND a.checkout <> '现金'
                AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL
              #              色布发货
              SELECT DATE(a.datafh)                                                                            billdate,
                     a.client                                                                                  client,
                     a.billcode                                                                                billcode,
                     CONCAT_WS(' ', '色布发货', c.order_code, c.product_name, c.material_specification)            billtype,
                     ifnull(a.qty, 0)                                                                          qty,
                     ifnull(a.ps, 0)                                                                           ps,
                     ifnull(a.price, 0)                                                                        price,
                     if(ifnull(a.account_price, 0) > 0, a.account_price, a.price)                              account_price,
                     ROUND(a.qty * ifnull(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0), 2) money,
                     a.checkout                                                                                collectiontype,
                     a.memo                                                                                    billmemo,
                     0                                                                                         SKid,
                     0                                                                                         whitebillfhid,
                     0                                                                                         dye_fabric_inbound_id,
                     a.id                                                                                      cp_billfh_id,
                     c.plan_code                                                                               planCode,
                     a.planid                                                                                  planid,
                     '色布发货'                                                                                    type -- 单据类型
              FROM cp_billfh a
                       LEFT OUTER JOIN dye_fabric_plan c ON a.planid = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, a.client = `clientName`)
                AND a.checkout <> '现金'
                AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL

              #             色布出库
              SELECT DATE(a.order_date)                                           billdate,
                     b.company_name                                               client,
                     a.order_code                                                 billcode,
                     concat_ws(' ', ifnull(c.contract_code, ''), ifnull(c.plan_code, ''), ifnull(c.product_name, ''),
                               ifnull(c.material_specification, ''), ifnull(c.color_no, ''),
                               ifnull(c.color, ''))                               billtype,
                     ifnull(a.input_kilo, 0)                                      qty,
                     ifnull(a.input_pairs, 0)                                     ps,
                     ifnull(a.price, 0)                                           price,
                     if(ifnull(a.account_price, 0) > 0, a.account_price, a.price) account_price,
                     ROUND(a.input_kilo * ifnull(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0),
                           2)                                                     money,
                     '欠账'                                                         collectiontype,
                     a.remarks                                                    billmemo,
                     0                                                            SKid,
                     0                                                            whitebillfhid,
                     a.id                                                         dye_fabric_inbound_id,
                     0                                                            cp_billfh_id,
                     c.plan_code                                                  planCode,
                     c.id                                                         planid,
                     '色布发货'                                                       type -- 单据类型
              FROM dye_fabric_inbound a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
                       LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
              WHERE a.state = 1
                AND length(ifnull(a.linked_order_code, '')) > 0
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                       b.company_name = `clientName`)
                AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL

              #           白坯出库
              SELECT DATE_FORMAT(billdate, '%Y-%m-%d') billdate,
                     client                            client,
                     NULL                              billcode,
                     billtype                          billtype,
                     0                                 qty,
                     0                                 ps,
                     0                                 price,
                     0                                 account_price,
                     -ifnull(money, 0)                 money,
                     collectiontype                    collectiontype,
                     billmemo                          billmemo,
                     id                                SKid,
                     0                                 whitebillfhid,
                     0                                 dye_fabric_inbound_id,
                     0                                 cp_billfh_id,
                     NULL                              planCode,
                     NULL                              planid,
                     billtype                          type -- 单据类型
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, client = `clientName`)
                AND DATE(billdate) BETWEEN DATE(`start`) AND DATE(`end`)
              UNION ALL
              #            白坯退货
              SELECT DATE_FORMAT(a.order_date, '%Y-%m-%d')                             billdate,
                     c.company_name                                                    client,
                     a.order_code                                                      billcode,
                     '白坯退货'                                                            billtype,
                     -ifnull(a.input_kilo, 0)                                          qty,
                     -ifnull(a.input_pairs, 0)                                         ps,
                     a.processing_cost                                                 price,
                     a.processing_cost                                                 account_price,
                     -round(ifnull(a.processing_cost * ifnull(a.input_kilo, 0), 0), 2) money,
                     NULL                                                              collectiontype,
                     a.remarks                                                         billmemo,
                     NULL                                                              SKid,
                     0                                                                 whitebillfhid,
                     0                                                                 dye_fabric_inbound_id,
                     0                                                                 cp_billfh_id,
                     b.plan_code                                                       planCode,
                     b.id                                                              planid,
                     '白坯退回'                                                            type -- 单据类型
              FROM product_outward_order a
                       LEFT JOIN plan b ON a.plan_code = b.plan_code
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                       c.company_name = `clientName`)
                AND ifnull(a.storage_id, 0) > 0
                AND ifnull(a.is_finished_product, 0) = 1
                AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)) rep,
             (SELECT @money_today := 0) t
        ORDER BY rep.billdate;
    END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for getReceivablesDetail4print
-- ----------------------------
DROP PROCEDURE IF EXISTS `getReceivablesDetail4print`;
DELIMITER ;;
CREATE PROCEDURE `getReceivablesDetail4print`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100),
                                              IN `isGather` INT)
BEGIN
    /**
        2021年7月1日  白坯应收款
     */
    IF (`isGather` = 1) THEN
        #      汇总
        SELECT rep.client client, round(sum(rep.money), 2) money
        FROM (SELECT a.client,
                     round(sum(if(ifnull(a.account_price, 0) > 0, ifnull(a.account_price, 0), ifnull(a.price, 0)) *
                               a.qty), 2) money
              FROM whitebillfh a
                   #                        LEFT JOIN whitebilldetailfh b ON a.id = b.fid
              WHERE a.checkout <> '现金'
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, a.client = `clientName`)
              GROUP BY a.client
              UNION ALL
              #             色布出库
              SELECT b.company_name                client,
                     - round(sum(if(ifnull(a.account_price, 0) > 0, ifnull(a.account_price, 0), ifnull(a.price, 0)) *
                                 a.input_kilo), 2) money
              FROM dye_fabric_inbound a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
              WHERE a.state = 1
                AND length(ifnull(a.linked_order_code, '')) > 0
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                       b.company_name = `clientName`)
              GROUP BY a.contact_company_id
              UNION ALL
              #             色布入库
              SELECT a.client,
                     round(sum(if(ifnull(a.account_price, 0) > 0, ifnull(a.account_price, 0), ifnull(a.price, 0)) *
                               a.qty), 2) money
              FROM cp_billfh a
              WHERE a.checkout <> '现金'
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, a.client = `clientName`)
              GROUP BY a.client
              UNION ALL
              #             白坯退货
              SELECT c.company_name client, -round(sum(ifnull(a.processing_cost, 0) * ifnull(a.input_kilo, 0)), 2) money
              FROM product_outward_order a
                       LEFT JOIN plan b ON a.plan_code = b.plan_code
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                       c.company_name = `clientName`)
                AND ifnull(a.is_finished_product, 0) = 1
                AND ifnull(a.storage_id, 0) > 0
              GROUP BY c.company_name
              UNION ALL
              SELECT client, round(sum(-money), 2) money
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
              GROUP BY client) rep
        GROUP BY rep.client;
    ELSE
        #         明细
        SELECT rep.*,
               if(trim(ifnull(`clientName`, '')) = '' OR ifnull(`clientName`, '') = '全部', 0,
                  @money_today := ifnull(rep.money, 0) + @money_today) accoumlatedtoday
        FROM (SELECT `start`                                                                                  billdate,
                     if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', '全部', `clientName`) client,
                     NULL                                                                                     billcode,
                     '期初结余'                                                                                   billtype,
                     0                                                                                        qty,
                     0                                                                                        ps,
                     0                                                                                        price,
                     sum(a.money)                                                                             money,
                     ''                                                                                       collectiontype,
                     ''                                                                                       billmemo,
                     0                                                                                        SKid,
                     NULL                                                                                     planCode,
                     '期初结余'                                                                                   type
              FROM (SELECT a.client, round(sum(a.price * a.qty), 2) money
                    FROM whitebillfh a
                    WHERE DATE(a.datafh) < DATE(`start`)
                      AND a.checkout <> '现金'
                      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                             a.client = `clientName`)
                    GROUP BY a.client
                    UNION ALL
                    SELECT client, round(sum(-money), 2) money
                    FROM receivables
                    WHERE DATE(billdate) < DATE(`start`)
                      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                             client = `clientName`)
                    GROUP BY client) a
              UNION ALL
              #      发货明细
              SELECT DATE(a.datafh)                                                                     billdate,
                     a.client                                                                           client,
                     a.billcode                                                                         billcode,
                     CONCAT_WS(' ', c.plan_code, c.order_code, c.m_height, c.meter_length,
                               CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.greyfabric_specification,
                               c.material_specification, c.product_name)                                billtype, -- 项目
                     ifnull(a.qty, 0)                                                                   qty,
                     ifnull(a.ps, 0)                                                                    ps,
                     if(ifnull(a.account_price, 0) > 0, ifnull(a.account_price, 0), ifnull(a.price, 0)) price,
                     ROUND(a.qty * if(ifnull(a.account_price, 0) > 0, ifnull(a.account_price, 0), ifnull(a.price, 0)),
                           2)                                                                           money,
                     a.checkout                                                                         collectiontype,
                     a.memo                                                                             billmemo,
                     0                                                                                  SKid,
                     c.plan_code                                                                        planCode,
                     '白坯发货'                                                                             type      -- 单据类型
              FROM whitebillfh a
                       LEFT OUTER JOIN plan c ON a.planid = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
                AND a.checkout <> '现金'
                AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL
              #              色布发货
              SELECT DATE(a.datafh)                                                                     billdate,
                     a.client                                                                           client,
                     a.billcode                                                                         billcode,
                     CONCAT_WS(' ', '色布发货', c.product_name, c.material_specification)                   billtype,
                     ifnull(a.qty, 0)                                                                   qty,
                     ifnull(a.ps, 0)                                                                    ps,
                     if(ifnull(a.account_price, 0) > 0, ifnull(a.account_price, 0), ifnull(a.price, 0)) price,
                     ROUND(a.qty * if(ifnull(a.account_price, 0) > 0, ifnull(a.account_price, 0), ifnull(a.price, 0)),
                           2)                                                                           money,
                     a.checkout                                                                         collectiontype,
                     a.memo                                                                             billmemo,
                     0                                                                                  SKid,
                     c.plan_code                                                                        planCode,
                     '色布发货'                                                                             type -- 单据类型
              FROM cp_billfh a
                       LEFT OUTER JOIN dye_fabric_plan c ON a.planid = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, a.client = `clientName`)
                AND a.checkout <> '现金'
                AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL

              #             色布出库
              SELECT DATE(a.order_date)                                                                           billdate,
                     b.company_name                                                                               client,
                     a.order_code                                                                                 billcode,
                     concat_ws(' ', ifnull(c.contract_code, ''), ifnull(c.plan_code, ''), ifnull(c.product_name, ''),
                               ifnull(c.material_specification, ''), ifnull(c.color_no, ''),
                               ifnull(c.color, ''))                                                               billtype,
                     ifnull(a.input_kilo, 0)                                                                      qty,
                     ifnull(a.input_pairs, 0)                                                                     ps,
                     if(ifnull(a.account_price, 0) > 0, ifnull(a.account_price, 0), ifnull(a.price, 0))           price,
                     ROUND(a.input_kilo *
                           if(ifnull(a.account_price, 0) > 0, ifnull(a.account_price, 0), ifnull(a.price, 0)), 2) money,
                     '欠账'                                                                                         collectiontype,
                     a.remarks                                                                                    billmemo,
                     0                                                                                            SKid,
                     c.plan_code                                                                                  planCode,
                     '色布发货'                                                                                       type -- 单据类型
              FROM dye_fabric_inbound a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
                       LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
              WHERE a.state = 1
                AND length(ifnull(a.linked_order_code, '')) > 0
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                       b.company_name = `clientName`)
                AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL

              #           白坯出库
              SELECT DATE_FORMAT(billdate, '%Y-%m-%d') billdate,
                     client                            client,
                     NULL                              billcode,
                     billtype                          billtype,
                     0                                 qty,
                     0                                 ps,
                     0                                 price,
                     -ifnull(money, 0)                 money,
                     collectiontype                    collectiontype,
                     billmemo                          billmemo,
                     id                                SKid,
                     NULL                              planCode,
                     billtype                          type -- 单据类型
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1, client = `clientName`)
                AND DATE(billdate) BETWEEN DATE(`start`) AND DATE(`end`)
              UNION ALL
              #            白坯退货
              SELECT DATE_FORMAT(a.order_date, '%Y-%m-%d')                             billdate,
                     c.company_name                                                    client,
                     a.order_code                                                      billcode,
                     '白坯退货'                                                            billtype,
                     -ifnull(a.input_kilo, 0)                                          qty,
                     -ifnull(a.input_pairs, 0)                                         ps,
                     a.processing_cost                                                 price,
                     -round(ifnull(a.processing_cost * ifnull(a.input_kilo, 0), 0), 2) money,
                     NULL                                                              collectiontype,
                     a.remarks                                                         billmemo,
                     NULL                                                              SKid,
                     b.plan_code                                                       planCode,
                     '白坯退回'                                                            type -- 单据类型
              FROM product_outward_order a
                       LEFT JOIN plan b ON a.plan_code = b.plan_code
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                       c.company_name = `clientName`)
                AND ifnull(a.storage_id, 0) > 0
                AND ifnull(a.is_finished_product, 0) = 1
                AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)) rep,
             (SELECT @money_today := 0) t
        ORDER BY rep.billdate;
    END IF;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
