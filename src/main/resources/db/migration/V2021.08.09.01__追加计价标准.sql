/**
  2021年8月9日 追加计价标准
 */
alter table dye_fabric_plan
    add price_standard int default 1 null comment '计价标准(1:毛重;2:净重;3:公式计米;4:实际计米;)' after rate;