/**
  2021年8月9日 色布入库追加计价标准
 */
alter table dye_fabric_inbound
    add price_standard int default 1 null comment '计价标准(1:毛重;2:净重;3:公式计米;4:实际计米;)';