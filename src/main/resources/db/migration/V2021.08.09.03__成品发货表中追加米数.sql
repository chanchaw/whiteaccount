/**
  2021年8月9日 成品发货表中追加米数
 */
alter table cp_billfh
    add meter_number DECIMAL(60,1) default 0 null comment '米数' after qty;