SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


DROP PROCEDURE IF EXISTS `p_dye_inbound_report`;
DELIMITER ;;
CREATE PROCEDURE `p_dye_inbound_report`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100))
BEGIN
    /**
      2021年6月27日 色布入库序时表
      2021年8月9日 追加单价金额计价标准
     */
    IF ((isnull(`clientName`) OR length(`clientName`) <= 0) OR `clientName` = '全部') THEN
        SELECT report.*
        FROM (SELECT NULL                                                          id,
                     c.company_name                                                client_name,
                     b.plan_code,
                     b.product_name,
                     b.material_specification,
                     b.color_no,
                     b.color,
                     ifnull(a.pairs, 0)                                            ps,
                     ifnull(a.qty, 0)                                              qty,
                     ifnull(a.qty, 0) - ifnull(a.pairs, 0) * ifnull(b.net_diff, 0) net_qty,
                     0                                                             ms,
                     ifnull(a.price, 0)                                            price,
                     CASE b.price_standard
                         WHEN 1 THEN ifnull(a.qty, 0)
                         WHEN 2 THEN ifnull(a.qty, 0) - ifnull(a.pairs, 0) * ifnull(b.net_diff, 0)
                         WHEN 3 THEN ifnull(a.qty, 0)
                         WHEN 4 THEN 0
                         ELSE ifnull(a.qty, 0) END * ifnull(a.price,0)             money,
                     CASE b.price_standard
                         WHEN 1 THEN '毛重'
                         WHEN 2 THEN '净重'
                         WHEN 3 THEN '公式计米'
                         WHEN 4 THEN '实际计米'
                         ELSE '' END                                               price_standard_name,
                     a.client_name                                                 supplier,
                     a.bill_date                                                   bill_date,
                     a.remarks                                                     remarks,
                     NULL                                                          is_send_directly,
                     NULL                                                          is_audit
              FROM cp_adjust a
                       LEFT JOIN dye_fabric_plan b ON a.plan_id = b.id
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE a.bill_type IN ('手动入库', '盘点')
                AND DATE(a.bill_date) BETWEEN DATE(`start`) AND DATE(`end`)
              UNION ALL
              #      色布入库
              SELECT a.id                                 id,
                     b.company_name                       client_name,
                     a.dye_plan_code                      plan_code,
                     c.product_name                       product_name,
                     c.material_specification             material_specification,
                     c.color_no                           color_no,
                     c.color                              color,
                     ifnull(a.input_pairs, 0)             ps,
                     ifnull(a.input_kilo, 0)              qty,
                     ifnull(a.net_kilo, 0)                net_qty,
                     ifnull(a.input_meter, 0)             ms,
                     ifnull(a.price, 0)                   price,
                     ifnull(a.money, 0)                   money,
                     CASE a.price_standard
                         WHEN 1 THEN '毛重'
                         WHEN 2 THEN '净重'
                         WHEN 3 THEN '公式计米'
                         WHEN 4 THEN '实际计米'
                         ELSE '' END                      price_standard_name,
                     a.dye_company                        supplier,
                     a.order_date                         order_date,
                     a.remarks                            remarks,
                     if(a.is_send_directly = 1, '是', '否') is_send_directly,
                     a.is_audit                           is_audit
              FROM dye_fabric_inbound a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
                       LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
              WHERE a.state = 1
                AND isnull(a.linked_order_code)
                AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)) report
        ORDER BY report.bill_date DESC;
    ELSE
        SELECT report.*
        FROM (SELECT NULL                                                          id,
                     c.company_name                                                client_name,
                     b.plan_code,
                     b.product_name,
                     b.material_specification,
                     b.color_no,
                     b.color,
                     ifnull(a.pairs, 0)                                            ps,
                     ifnull(a.qty, 0)                                              qty,
                     ifnull(a.qty, 0) - ifnull(a.pairs, 0) * ifnull(b.net_diff, 0) net_qty,
                     0                                                             ms,
                     ifnull(a.price, 0)                                            price,
                     CASE b.price_standard
                         WHEN 1 THEN ifnull(a.qty, 0)
                         WHEN 2 THEN ifnull(a.qty, 0) - ifnull(a.pairs, 0) * ifnull(b.net_diff, 0)
                         WHEN 3 THEN ifnull(a.qty, 0)
                         WHEN 4 THEN 0
                         ELSE ifnull(a.qty, 0) END * ifnull(a.price,0)             money,
                     CASE b.price_standard
                         WHEN 1 THEN '毛重'
                         WHEN 2 THEN '净重'
                         WHEN 3 THEN '公式计米'
                         WHEN 4 THEN '实际计米'
                         ELSE '' END                                               price_standard_name,
                     a.client_name                                                 supplier,
                     a.bill_date,
                     a.remarks,
                     NULL                                                          is_send_directly,
                     NULL                                                          is_audit
              FROM cp_adjust a
                       LEFT JOIN dye_fabric_plan b ON a.plan_id = b.id
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE a.bill_type IN ('手动入库', '盘点')
                AND c.company_name = `clientName`
                AND DATE(a.bill_date) BETWEEN DATE(`start`) AND DATE(`end`)
              UNION ALL
              #      色布入库
              SELECT a.id                                 id,
                     b.company_name                       client_name,
                     a.dye_plan_code                      plan_code,
                     c.product_name,
                     c.material_specification,
                     c.color_no,
                     c.color,
                     ifnull(a.input_pairs, 0)             ps,
                     ifnull(a.input_kilo, 0)              qty,
                     ifnull(a.net_kilo, 0)                net_qty,
                     ifnull(a.input_meter, 0)             ms,
                     ifnull(a.price, 0)                   price,
                     ifnull(a.money, 0)                   money,
                     CASE a.price_standard
                         WHEN 1 THEN '毛重'
                         WHEN 2 THEN '净重'
                         WHEN 3 THEN '公式计米'
                         WHEN 4 THEN '实际计米'
                         ELSE '' END                      price_standard_name,
                     a.dye_company                        supplier,
                     a.order_date,
                     a.remarks,
                     if(a.is_send_directly = 1, '是', '否') is_send_directly,
                     a.is_audit                           is_audit
              FROM dye_fabric_inbound a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
                       LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
              WHERE a.state = 1
                AND isnull(a.linked_order_code)
                AND b.company_name = `clientName`
                AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)) report
        ORDER BY report.bill_date DESC;
    END IF;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;