DROP PROCEDURE IF EXISTS `p_dye_storage_report`;;
DELIMITER ;;
create procedure p_dye_storage_report(IN clientName varchar(100), IN value int)
BEGIN
    /**
      2021年6月27日 色布库存表
     */
    IF (ifnull(`value`, 0) = 1) THEN
        #         汇总
        SELECT rep.*
        FROM (
                 SELECT b.company_name                  client_name,
                        sum(ifnull(c.pairs, 0) + ifnull(x.input_pairs, 0) - ifnull(y.input_pairs, 0) - ifnull(d.ps, 0) -
                            ifnull(e.pairs, 0))         ps,
                        sum(round(ifnull(c.qty, 0) + ifnull(x.input_kilo, 0) - ifnull(y.input_kilo, 0) - ifnull(d.qty, 0) -
                                  ifnull(e.qty, 0), 1)) qty
                 FROM dye_fabric_plan a
                          LEFT JOIN contact_company b ON a.contact_company_id = b.id
                          LEFT JOIN (
                     #         入库
                     SELECT sum(pairs) pairs, sum(qty) qty, plan_id
                     FROM cp_adjust
                     WHERE bill_type IN ('手动入库', '盘点')
                     GROUP BY plan_id
                 ) c ON c.plan_id = a.id

                          LEFT JOIN (
                     #         出库
                     SELECT sum(ps) ps, sum(qty) qty, planid FROM cp_billfh WHERE planid = `planid` GROUP BY planId
                 ) d ON d.planId = a.id

                          LEFT JOIN (
                     #         出库调整
                     SELECT sum(pairs) pairs, sum(qty) qty, plan_id
                     FROM cp_adjust
                     WHERE bill_type = '手动出库'
                     GROUP BY plan_id
                 ) e ON e.plan_id = a.id

                          LEFT JOIN (
                     #      色布入库(非直发)
                     SELECT dye_plan_code,
                            sum(input_kilo)  input_kilo,
                            sum(input_pairs) input_pairs,
                            sum(input_meter) input_meter
                     FROM dye_fabric_inbound
                     WHERE state = 1
                       AND isnull(linked_order_code)
                     GROUP BY dye_plan_code
                 ) x ON x.dye_plan_code = a.plan_code

                          LEFT JOIN (
                     #             色布出库
                     SELECT dye_plan_code,
                            sum(input_kilo)  input_kilo,
                            sum(input_pairs) input_pairs,
                            sum(input_meter) input_meter
                     FROM dye_fabric_inbound
                     WHERE state = 1
                       AND length(ifnull(linked_order_code, '')) > 0
                     GROUP BY dye_plan_code
                 ) y ON y.dye_plan_code = a.plan_code

                 WHERE a.state = 1
                   AND if(ifnull(`clientName`, '') IN ('', '全部'), 1 = 1, b.company_name = ifnull(`clientName`, ''))
                 GROUP BY a.contact_company_id) rep
        WHERE abs(ifnull(rep.ps, 0)) > 0;
    ELSE
        SELECT b.company_name             client_name,
               a.plan_code,
               a.product_name,
               a.material_specification,
               a.color_no,
               a.color,

               CASE a.price_standard
                   WHEN 1 THEN '毛重'
                   WHEN 2 THEN '净重'
                   WHEN 3 THEN '公式计米'
                   WHEN 4 THEN '实际计米'
                   ELSE '' END  price_standard_name,# 计价标准

               ifnull(c.pairs, 0) + ifnull(x.input_pairs, 0) - ifnull(y.input_pairs, 0) - ifnull(d.ps, 0) -
               ifnull(e.pairs, 0)         ps, # 匹数
               round(ifnull(c.qty, 0) + ifnull(x.input_kilo, 0) - ifnull(y.input_kilo, 0) - ifnull(d.qty, 0) - ifnull(e.qty, 0), 1) as qty, # 毛重
               round(ifnull(c.net_qty, 0) + ifnull(x.net_qty, 0) - ifnull(y.net_qty, 0) - ifnull(d.net_qty, 0) - ifnull(e.net_qty, 0), 1) as net_qty,# 净重
               round(ifnull(c.meter_number, 0) + ifnull(x.meter_number, 0) - ifnull(y.meter_number, 0) - ifnull(d.meter_number, 0) - ifnull(e.meter_number, 0), 1) as meter_number # 米数

        FROM dye_fabric_plan a
                 LEFT JOIN contact_company b ON a.contact_company_id = b.id
                 LEFT JOIN (
            #         入库
            SELECT c1.plan_id,sum(ifnull(c1.pairs,0)) pairs, sum(ifnull(c1.qty,0)) qty,
                   sum(ifnull(c1.qty,0) - ifnull(c1.pairs,0) * ifnull(c2.net_diff,0)) as net_qty,
                   0 as meter_number # 米数
            FROM cp_adjust c1
                     left join dye_fabric_plan c2 on c1.plan_id = c2.id
            WHERE bill_type IN ('手动入库', '盘点')
            GROUP BY plan_id
        ) c ON c.plan_id = a.id

                 LEFT JOIN (
            #         出库
            SELECT d1.planid,sum(ifnull(d1.ps,0)) as ps, sum(ifnull(d1.qty,0)) as qty,
                   sum(ifnull(d1.qty,0) - ifnull(d1.ps,0) * ifnull(d2.net_diff,0)) as net_qty,
                   CASE d2.price_standard
                       WHEN 3 THEN sum(ROUND(d1.qty*d2.rate, 2))
                       WHEN 4 THEN sum(ROUND(d1.meter_number, 2)) END as meter_number
                   #sum(ifnull(d1.meter_number,0)) as meter_number
            FROM cp_billfh d1
                     left join dye_fabric_plan d2 on d1.planid = d2.id
            WHERE planid = `planid` GROUP BY planId
        ) d ON d.planId = a.id

            # 调整出库
                 LEFT JOIN (
            SELECT e1.plan_id,sum(ifnull(e1.pairs,0)) pairs, sum(ifnull(e1.qty,0)) qty,
                   sum(ifnull(e1.qty,0) - ifnull(e1.pairs,0) * ifnull(e2.net_diff,0)) as net_qty,
                   0 as meter_number
            FROM cp_adjust as e1
                     left join dye_fabric_plan e2 on e1.plan_id = e2.id
            WHERE bill_type = '手动出库'
            GROUP BY plan_id
        ) e ON e.plan_id = a.id

                 LEFT JOIN (
            #      色布入库(非直发)
            SELECT x1.dye_plan_code, sum(ifnull(x1.input_pairs,0)) input_pairs,sum(ifnull(x1.input_kilo,0)) input_kilo,
                   sum(ifnull(x1.input_kilo,0) - ifnull(x1.input_pairs,0) * ifnull(x2.net_diff,0)) as net_qty,
                   sum(ifnull(x1.input_meter,0)) as meter_number
            FROM dye_fabric_inbound x1
                     left join dye_fabric_plan x2 on x1.dye_plan_code = x2.plan_code
            WHERE x1.state = 1
              AND isnull(linked_order_code)
            GROUP BY dye_plan_code
        ) x ON x.dye_plan_code = a.plan_code

                 LEFT JOIN (
            #             色布出库
            SELECT y1.dye_plan_code, sum(ifnull(y1.input_pairs,0)) input_pairs,sum(ifnull(y1.input_kilo,0)) input_kilo,
                   sum(ifnull(y1.input_kilo,0) - ifnull(y1.input_pairs,0) * ifnull(y2.net_diff,0)) as net_qty,
                   sum(ifnull(y1.input_meter,0)) as meter_number
            FROM dye_fabric_inbound as y1
                     left join dye_fabric_plan as y2 on y1.dye_plan_code = y2.plan_code
            WHERE y1.state = 1
              AND length(ifnull(linked_order_code, '')) > 0
            GROUP BY dye_plan_code
        ) y ON y.dye_plan_code = a.plan_code

        WHERE a.state = 1
          AND abs(ifnull(c.pairs, 0) + ifnull(x.input_pairs, 0) - ifnull(y.input_pairs, 0) - ifnull(d.ps, 0) -
                  ifnull(e.pairs, 0)) > 0
          AND if(ifnull(`clientName`, '') IN ('', '全部'), 1 = 1, b.company_name = ifnull(`clientName`, ''));
    END IF;
END;;
DELIMITER ;

