SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
DROP PROCEDURE IF EXISTS `getReceivablesDetail4print`;
DELIMITER ;;
-- 报表中使用的数据源
CREATE PROCEDURE `getReceivablesDetail4print`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100),
                                              IN `isGather` INT)
BEGIN
    /**
        2021年7月1日  白坯应收款
        2021年7月30日 追加ID
        2021年8月11日 修复BUG
        2021年8月12日 追加开票金额
     */
    IF (`isGather` = 1) THEN
        #      汇总
        SELECT rep.client client, round(sum(rep.money), 2) money, round(sum(rep.ticket_money), 2) ticket_money
        FROM (SELECT a.client
                   , round(sum(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price) * a.qty), 2) money
                   , 0                                                                                   ticket_money
              FROM whitebillfh a
              WHERE a.checkout <> '现金'
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, a.client = `clientName`)
              GROUP BY a.client
              UNION ALL
              #             色布入库
              SELECT a.client,
                     round(sum(if(ifnull(a.account_price, 0) > 0, ifnull(a.account_price, 0), ifnull(a.price, 0)) *
                               a.qty), 2) money,
                     0                    ticket_money
              FROM cp_billfh a
              WHERE a.checkout <> '现金'
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, a.client = `clientName`)
              GROUP BY a.client
              UNION ALL
              #             色布出库
              SELECT b.company_name              client,
                     round(sum(if(ifnull(a.account_price, 0) > 0, ifnull(a.account_price, 0), ifnull(a.price, 0)) *
                               a.input_kilo), 2) money,
                     0                           ticket_money
              FROM dye_fabric_inbound a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
              WHERE a.state = 1
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                       b.company_name = `clientName`)
                AND length(ifnull(a.linked_order_code, '')) > 0
              GROUP BY a.contact_company_id
              UNION ALL
              #             白坯退货
              SELECT c.company_name                                                         client,
                     -round(sum(ifnull(a.processing_cost, 0) * ifnull(a.input_kilo, 0)), 2) money,
                     0                                                                      ticket_money
              FROM product_outward_order a
                       LEFT JOIN plan b ON a.plan_code = b.plan_code
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE a.state = 1
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                       c.company_name = `clientName`)
                AND ifnull(a.is_finished_product, 0) = 1
                AND ifnull(a.storage_id, 0) > 0
              GROUP BY c.company_name
              UNION ALL
              SELECT client, round(sum(-money), 2) money, 0 ticket_money
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, client = `clientName`)
                AND billtype <> '开票'
              GROUP BY client
              UNION ALL
              -- 开票金额
              SELECT client, 0 money, sum(money) ticket_money
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, client = `clientName`)
                AND billtype = '开票'
              GROUP BY client) rep
        GROUP BY rep.client;
    ELSE
        #         明细
        SELECT rep.*,
               if(trim(ifnull(`clientName`, '')) = '' OR ifnull(`clientName`, '') = '全部', 0,
                  @money_today := ifnull(rep.money, 0) + @money_today)                                                                              accoumlatedtoday, -- 本日累计
               CASE
                   WHEN trim(ifnull(`clientName`, '')) = '' OR ifnull(`clientName`, '') = '全部' THEN 0
                   ELSE if(rep.billtype = '收款', @money_sell := @money_sell + 0, CASE
                                                                                    WHEN rep.billtype = '期初结余' THEN
                                                                                        @money_sell :=
                                                                                                ifnull(rep.money, 0) + ifnull(x.money, 0) + @money_sell
                                                                                    ELSE @money_sell := ifnull(rep.money, 0) + @money_sell END) END accoumlatedselltoday
        FROM (SELECT `start`                                                                                  billdate,
                     if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', '全部', `clientName`) client,
                     NULL                                                                                     billcode,
                     '期初结余'                                                                                   billtype,
                     0                                                                                        qty,
                     0    AS                                                                                  qty_jz,
                     0    AS                                                                                  meter_number,
                     0                                                                                        ps,
                     0                                                                                        price,
                     0                                                                                        account_price,
                     sum(a.money)                                                                             money,
                     sum(a.ticket_money)                                                                      ticket_money,
                     '毛重' AS                                                                                  price_standard,
                     ''                                                                                       collectiontype,
                     ''                                                                                       billmemo,
                     0                                                                                        SKid,
                     0                                                                                        whitebillfhid,
                     0                                                                                        dye_fabric_inbound_id,
                     0                                                                                        cp_billfh_id,
                     NULL                                                                                     planCode,
                     NULL                                                                                     planid,
                     '期初结余'                                                                                   type
              FROM (
                       -- 白坯发货应收款
                       SELECT a.client,
                              round(sum(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price) * a.qty), 2) money,
                              0                                                                                   ticket_money
                       FROM whitebillfh a
                       WHERE DATE(a.datafh) < DATE(`start`)
                         AND a.checkout <> '现金'
                         AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                                a.client = `clientName`)
                       GROUP BY a.client
                       UNION ALL

                       -- 成品发货应收款
                       SELECT a.client,                                -- 客户
                              sum(CASE b.price_standard
                                      WHEN 1 THEN ROUND(a.qty * ifnull(
                                              if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0), 2)
                                      WHEN 2 THEN ROUND((a.qty - (a.ps * b.net_diff)) * ifnull(
                                              if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0), 2)
                                      WHEN 3 THEN ROUND(a.qty * b.rate * ifnull(
                                              if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0), 2)
                                      WHEN 4 THEN ROUND(a.meter_number * ifnull(
                                              if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0),
                                                        2) END) money, -- 金额
                              0                                 ticket_money
                       FROM cp_billfh a
                                LEFT JOIN dye_fabric_plan b ON a.planid = b.id
                       WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                                a.client = `clientName`)
                         AND a.checkout <> '现金'
                         AND DATE(a.datafh) < DATE(`start`)
                       GROUP BY a.client
                       UNION ALL

                       -- 色布出库
                       SELECT b.company_name                                                                    client,
                              ROUND(a.input_kilo *
                                    ifnull(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0), 2) money,
                              0                                                                                 ticket_money
                       FROM dye_fabric_inbound a
                                LEFT JOIN contact_company b ON a.contact_company_id = b.id
                                LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
                       WHERE a.state = 1
                         AND length(ifnull(a.linked_order_code, '')) > 0
                         AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                                b.company_name = `clientName`)
                         AND DATE(a.order_date) < DATE(`start`)
                       GROUP BY b.company_name

                       UNION ALL

                       --     白坯退货
                       SELECT c.company_name                                                    client,
                              -round(ifnull(a.processing_cost * ifnull(a.input_kilo, 0), 0), 2) money,
                              0                                                                 ticket_money
                       FROM product_outward_order a
                                LEFT JOIN plan b ON a.plan_code = b.plan_code
                                LEFT JOIN contact_company c ON b.contact_company_id = c.id
                       WHERE a.state = 1
                         AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                                c.company_name = `clientName`)
                         AND ifnull(a.storage_id, 0) > 0
                         AND ifnull(a.is_finished_product, 0) = 1
                         AND DATE(a.order_date) < DATE(`start`)
                       GROUP BY c.company_name

                       UNION ALL

                       -- 已收款
                       SELECT client, round(sum(-money), 2) money, 0 ticket_money
                       FROM receivables
                       WHERE DATE(billdate) < DATE(`start`)
                         AND billtype <> '开票'
                         AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                                client = `clientName`)
                       GROUP BY client

                       UNION ALL

                       -- 开票金额
                       SELECT client, 0 money, round(sum(money), 2) ticket_money
                       FROM receivables
                       WHERE DATE(billdate) < DATE(`start`)
                         AND billtype = '开票'
                         AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                                client = `clientName`)
                       GROUP BY client) a
              UNION ALL
              #      发货明细
              SELECT DATE(a.datafh)                                                                            billdate,
                     a.client                                                                                  client,
                     a.billcode                                                                                billcode,
                     CONCAT_WS(' ', a.billcode, c.m_height, c.meter_length,
                               CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight), c.greyfabric_specification,
                               c.material_specification,
                               c.product_name)                                                                 billtype, -- 项目
                     ifnull(a.qty, 0)                                                                          qty,
                     0    AS                                                                                   qty_jz,
                     0    AS                                                                                   meter_number,
                     ifnull(a.ps, 0)                                                                           ps,
                     ifnull(a.price, 0)                                                                        price,
                     if(ifnull(a.account_price, 0) > 0, a.account_price, a.price)                              account_price,
                     ROUND(a.qty * ifnull(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0), 2) money,
                     0                                                                                         ticket_money,
                     '毛重' AS                                                                                   price_standard,
                     a.checkout                                                                                collectiontype,
                     a.memo                                                                                    billmemo,
                     0                                                                                         SKid,
                     a.id                                                                                      whitebillfhid,
                     0                                                                                         dye_fabric_inbound_id,
                     0                                                                                         cp_billfh_id,
                     c.plan_code                                                                               planCode,
                     a.planid                                                                                  planid,
                     '白坯发货'                                                                                    type      -- 单据类型
              FROM whitebillfh a
                       LEFT OUTER JOIN plan c ON a.planid = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, client = `clientName`)
                AND a.checkout <> '现金'
                AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL
              #              色布发货
              SELECT DATE(a.datafh)                                                               billdate,
                     a.client                                                                     client,
                     a.billcode                                                                   billcode,
                     CONCAT_WS(' ', '色布发货', a.billcode, c.product_name, c.material_specification) billtype,
                     ifnull(a.qty, 0)                                                             qty,
                     ROUND(ifnull(a.qty, 0) - (a.ps * ifnull(c.net_diff, 0)), 2) AS               qty_jz,       #净重
                     CASE c.price_standard
                         WHEN 3 THEN ROUND(a.qty * c.rate, 2)
                         WHEN 4 THEN ROUND(a.meter_number, 2) END                AS               meter_number, #米数
                     ifnull(a.ps, 0)                                                              ps,
                     ifnull(a.price, 0)                                                           price,
                     if(ifnull(a.account_price, 0) > 0, a.account_price, a.price)                 account_price,
                     CASE c.price_standard
                         WHEN 1 THEN ROUND(
                                     a.qty * ifnull(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0), 2)
                         WHEN 2 THEN ROUND((a.qty - (a.ps * c.net_diff)) *
                                           ifnull(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0), 2)
                         WHEN 3 THEN ROUND(a.qty * c.rate *
                                           ifnull(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0), 2)
                         WHEN 4 THEN ROUND(a.meter_number *
                                           ifnull(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0),
                                           2) END                                AS               money,
                     0                                                                            ticket_money,
                     #计价方式
                     CASE c.price_standard
                         WHEN 1 THEN '毛重'
                         WHEN 2 THEN '净重'
                         WHEN 3 THEN '公式计米'
                         WHEN 4 THEN '实际计米' END                                  AS               price_standard,
                     a.checkout                                                                   collectiontype,
                     a.memo                                                                       billmemo,
                     0                                                                            SKid,
                     0                                                                            whitebillfhid,
                     0                                                                            dye_fabric_inbound_id,
                     a.id                                                                         cp_billfh_id,
                     c.plan_code                                                                  planCode,
                     a.planid                                                                     planid,
                     '色布发货'                                                                       type          -- 单据类型
              FROM cp_billfh a
                       LEFT OUTER JOIN dye_fabric_plan c ON a.planid = c.id
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, a.client = `clientName`)
                AND a.checkout <> '现金'
                AND DATE(a.datafh) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL

              #             色布出库
              SELECT DATE(a.order_date)                                           billdate,
                     b.company_name                                               client,
                     a.order_code                                                 billcode,
                     concat_ws(' ', ifnull(a.order_code, ''), ifnull(c.product_name, ''),
                               ifnull(c.material_specification, ''), ifnull(c.color_no, ''),
                               ifnull(c.color, ''))                               billtype,
                     ifnull(a.input_kilo, 0)                                      qty,
                     0    AS                                                      qty_jz,
                     0    AS                                                      meter_number,
                     ifnull(a.input_pairs, 0)                                     ps,
                     ifnull(a.price, 0)                                           price,
                     if(ifnull(a.account_price, 0) > 0, a.account_price, a.price) account_price,
                     ROUND(a.input_kilo * ifnull(if(ifnull(a.account_price, 0) > 0, a.account_price, a.price), 0),
                           2)                                                     money,
                     0                                                            ticket_money,
                     '毛重' AS                                                      price_standard,
                     '欠账'                                                         collectiontype,
                     a.remarks                                                    billmemo,
                     0                                                            SKid,
                     0                                                            whitebillfhid,
                     a.id                                                         dye_fabric_inbound_id,
                     0                                                            cp_billfh_id,
                     c.plan_code                                                  planCode,
                     c.id                                                         planid,
                     '色布发货'                                                       type -- 单据类型
              FROM dye_fabric_inbound a
                       LEFT JOIN contact_company b ON a.contact_company_id = b.id
                       LEFT JOIN dye_fabric_plan c ON a.dye_plan_code = c.plan_code
              WHERE a.state = 1
                AND length(ifnull(a.linked_order_code, '')) > 0
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                       b.company_name = `clientName`)
                AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL

              #           白坯出库
              SELECT DATE_FORMAT(billdate, '%Y-%m-%d') billdate,
                     client                            client,
                     NULL                              billcode,
                     billtype                          billtype,
                     0                                 qty,
                     0    AS                           qty_jz,
                     0    AS                           meter_number,
                     0                                 meter_number,
                     0                                 price,
                     0                                 account_price,
                     -ifnull(money, 0)                 money,
                     0                                 ticket_money,
                     '毛重' AS                           price_standard,
                     collectiontype                    collectiontype,
                     billmemo                          billmemo,
                     id                                SKid,
                     0                                 whitebillfhid,
                     0                                 dye_fabric_inbound_id,
                     0                                 cp_billfh_id,
                     NULL                              planCode,
                     NULL                              planid,
                     billtype                          type -- 单据类型
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, client = `clientName`)
                AND billtype <> '开票'
                AND DATE(billdate) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL
              #           开票
              SELECT DATE_FORMAT(billdate, '%Y-%m-%d') billdate,
                     client                            client,
                     NULL                              billcode,
                     billtype                          billtype,
                     0                                 qty,
                     0    AS                           qty_jz,
                     0    AS                           meter_number,
                     0                                 meter_number,
                     0                                 price,
                     0                                 account_price,
                     0                                 money,
                     ifnull(money, 0)                  ticket_money,
                     '毛重' AS                           price_standard,
                     collectiontype                    collectiontype,
                     billmemo                          billmemo,
                     id                                SKid,
                     0                                 whitebillfhid,
                     0                                 dye_fabric_inbound_id,
                     0                                 cp_billfh_id,
                     NULL                              planCode,
                     NULL                              planid,
                     billtype                          type -- 单据类型
              FROM receivables
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, client = `clientName`)
                AND billtype = '开票'
                AND DATE(billdate) BETWEEN DATE(`start`) AND DATE(`end`)

              UNION ALL
              #            白坯退货
              SELECT DATE_FORMAT(a.order_date, '%Y-%m-%d')                             billdate,
                     c.company_name                                                    client,
                     a.order_code                                                      billcode,
                     '白坯退货'                                                            billtype,
                     -ifnull(a.input_kilo, 0)                                          qty,
                     0    AS                                                           qty_jz,
                     0    AS                                                           meter_number,
                     -ifnull(a.input_pairs, 0)                                         ps,
                     a.processing_cost                                                 price,
                     a.processing_cost                                                 account_price,
                     -round(ifnull(a.processing_cost * ifnull(a.input_kilo, 0), 0), 2) money,
                     0                                                                 ticket_money,
                     '毛重' AS                                                           price_standard,
                     NULL                                                              collectiontype,
                     a.remarks                                                         billmemo,
                     NULL                                                              SKid,
                     0                                                                 whitebillfhid,
                     0                                                                 dye_fabric_inbound_id,
                     0                                                                 cp_billfh_id,
                     b.plan_code                                                       planCode,
                     b.id                                                              planid,
                     '白坯退回'                                                            type -- 单据类型
              FROM product_outward_order a
                       LEFT JOIN plan b ON a.plan_code = b.plan_code
                       LEFT JOIN contact_company c ON b.contact_company_id = c.id
              WHERE a.state = 1
                AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                       c.company_name = `clientName`)
                AND ifnull(a.storage_id, 0) > 0
                AND ifnull(a.is_finished_product, 0) = 1
                AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)) rep
                 LEFT JOIN (SELECT client, round(sum(money), 2) money, 0 ticket_money, '期初结余' type
                            FROM receivables
                            WHERE DATE(billdate) < DATE(`start`)
                              AND billtype <> '开票'
                              AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                                     client = `clientName`)
                            ) x ON x.type = rep.billtype
                 JOIN (SELECT @money_today := 0) t,
             (SELECT @money_sell := 0) f
        ORDER BY rep.billdate;
    END IF;
END
;;
DELIMITER ;
SET FOREIGN_KEY_CHECKS = 1;