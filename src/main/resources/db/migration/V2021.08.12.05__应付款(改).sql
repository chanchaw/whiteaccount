SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `p_account_payable`;
DELIMITER ;;
CREATE PROCEDURE `p_account_payable`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100), IN `isGather` INT)
BEGIN
    /**
      2021年7月2日 应付款
      2021年7月24日 追加批次号，加工户，是否直发字段
      2021年7月27日 追加本日累计
      2021年8月12日 追加发票金额,追加销售累计
     */
    IF (`isGather` = 1) THEN
        #         汇总
        SELECT rep.supplier_name, sum(rep.money) money, sum(rep.ticket_money) ticket_money
        FROM (
                 -- 发票金额
                 SELECT client_name supplier_name, 0 money, sum(money) ticket_money
                 FROM material_account_payable
                 WHERE billtype = '开票'
                   AND IF(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                          client_name = `clientName`)
                 GROUP BY client_name
                 UNION ALL
                 SELECT c.supplier_name supplier_name, sum(a.input_money) money, 0 ticket_money
                 FROM material_order a
                          LEFT JOIN bill_type b ON a.bill_type = b.id
                          LEFT JOIN supplier c ON a.supplier_id = c.id
                 WHERE a.state = 1
                   AND b.access_mode > 0
                   AND a.account_type <> '现金'
                   AND IF(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                          c.supplier_name = `clientName`)
                 GROUP BY a.supplier_id
                 UNION ALL
                 SELECT client_name supplier_name, sum(-money) money, 0 ticket_money
                 FROM material_account_payable
                 WHERE IF(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                          client_name = `clientName`)
                 GROUP BY client_name) rep
        GROUP BY rep.supplier_name;
    ELSE
        #         明细
        SELECT report.*,
               if(trim(ifnull(`clientName`, '')) = '' OR ifnull(`clientName`, '') = '全部', 0,
                  @money_today := ifnull(report.money, 0) + @money_today)                                                 accoumlatedtoday,
               if(trim(ifnull(`clientName`, '')) = '' OR ifnull(`clientName`, '') = '全部', 0,
                  if(report.material_spec = '付款', @money_sell, if(report.material_spec = '期初数据', @money_sell :=
                              ifnull(report.money, 0) + ifnull(x.money, 0) + @money_sell,
                                                                  @money_sell := ifnull(report.money, 0) + @money_sell))) accoumlatedselltoday
        FROM (SELECT NULL                           id,
                     any_value(DATE(prev.billdate)) billdate,
                     any_value(prev.supplier_name)  supplier_name,
                     any_value(prev.billtype)       material_spec,
                     any_value(prev.account_type)   account_type,
                     any_value(prev.box_amount)     box_amount,
                     any_value(prev.input_kilo)     input_kilo,
                     any_value(prev.price)          price,
                     sum(prev.money)                money,
                     sum(prev.ticket_money)         ticket_money,
                     any_value(prev.remarks)        remarks,
                     NULL                           is_send_directly,
                     NULL                           process_company,
                     NULL                           batch_num,
                     '期初数据'                         type
              FROM (
                       #         起初数据
                       SELECT `start`               billdate,
                              if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', '全部',
                                 `clientName`)      supplier_name,
                              any_value(a.billtype) billtype,
                              NULL                  account_type,
                              0                     box_amount,
                              0                     input_kilo,
                              0                     price,
                              sum(a.money)          money,
                              sum(a.ticket_money)   ticket_money,
                              NULL                  remarks
                       FROM (SELECT c.supplier_name, sum(a.input_money) money, 0 ticket_money, '期初数据' billtype
                             FROM material_order a
                                      LEFT JOIN bill_type b ON a.bill_type = b.id
                                      LEFT JOIN supplier c ON a.supplier_id = c.id
                             WHERE a.state = 1
                               AND b.access_mode > 0
                               AND a.account_type <> '现金'
                               AND IF(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                                      c.supplier_name = `clientName`)
                               AND DATE(a.order_date) < DATE(`start`)
                             GROUP BY a.supplier_id
                             UNION ALL
                             SELECT client_name supplier_name, sum(-money) money, 0 ticket_money, '期初数据' billtype
                             FROM material_account_payable
                             WHERE IF(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                                      client_name = `clientName`)
                               AND billtype <> '开票'
                               AND DATE(bill_date) < DATE(`start`)
                             GROUP BY client_name
                             UNION ALL
                             -- 发票金额
                             SELECT client_name supplier_name, 0 money, sum(money) ticket_money, '期初数据' billtype
                             FROM material_account_payable
                             WHERE IF(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                                      client_name = `clientName`)
                               AND billtype = '开票'
                               AND DATE(bill_date) < DATE(`start`)
                             GROUP BY client_name
                             UNION ALL
                             SELECT x.supplier_name, x.money, 0 ticket_money, x.billtype
                             FROM (SELECT IF(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', '全部',
                                             `clientName`) supplier_name,
                                          0                money,
                                          '期初数据'           billtype) x) a
                       GROUP BY a.supplier_name) prev
              GROUP BY prev.supplier_name
              UNION ALL
              SELECT NULL                                 id,
                     DATE(a.order_date)                   billdate,
                     c.supplier_name,
                     a.material_spec,
                     a.account_type                       account_type,
                     ifnull(a.box_amount, 0)              box_amount,
                     ifnull(a.input_kilo, 0)              input_kilo,
                     ifnull(a.price, 0)                   price,
                     ifnull(a.input_money, 0)             money,
                     0                                    ticket_money,
                     a.remarks                            remarks,
                     if(a.is_send_directly = 1, '是', '否') is_send_directly,
                     a.process_company                    process_company,
                     a.batch_num                          batch_num,
                     b.bill_type_name                     type
              FROM material_order a
                       LEFT JOIN bill_type b ON a.bill_type = b.id
                       LEFT JOIN supplier c ON a.supplier_id = c.id
              WHERE a.state = 1
                AND a.account_type <> '现金'
                AND b.access_mode > 0
                AND IF(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                       c.supplier_name = `clientName`)
                AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
              UNION ALL
              SELECT id,
                     DATE(bill_date)       billdate,
                     client_name           supplier_name,
                     billtype              material_spec,
                     payable_type          account_type,
                     ifnull(box_amount, 0) box_amount,
                     0                     input_kilo,
                     ifnull(price, 0)      price,
                     -ifnull(money, 0)     money,
                     0                     ticket_money,
                     billmemo              remarks,
                     NULL                  is_send_directly,
                     NULL                  process_company,
                     NULL                  batch_num,
                     billtype              type
              FROM material_account_payable
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                       client_name = `clientName`)
                AND billtype <> '开票'
                AND DATE(bill_date) BETWEEN DATE(`start`) AND DATE(`end`)
              UNION ALL
              -- 发票金额
              SELECT id,
                     DATE(bill_date)       billdate,
                     client_name           supplier_name,
                     billtype              material_spec,
                     payable_type          account_type,
                     ifnull(box_amount, 0) box_amount,
                     0                     input_kilo,
                     ifnull(price, 0)      price,
                     0                     money,
                     ifnull(money, 0)      ticket_money,
                     billmemo              remarks,
                     NULL                  is_send_directly,
                     NULL                  process_company,
                     NULL                  batch_num,
                     billtype              type
              FROM material_account_payable
              WHERE if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE,
                       client_name = `clientName`)
                AND billtype = '开票'
                AND DATE(bill_date) BETWEEN DATE(`start`) AND DATE(`end`)) report
                 LEFT JOIN (
            -- 期初数据
            SELECT client_name supplier_name, sum(money) money, 0 ticket_money, '期初数据' billtype
            FROM material_account_payable
            WHERE IF(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', 1 = 1,
                     client_name = `clientName`)
              AND billtype <> '开票'
              AND DATE(bill_date) < DATE(`start`)) x ON x.billtype = report.material_spec
                 JOIN (SELECT @money_today := 0) t,
             (SELECT @money_sell := 0) p
        ORDER BY report.billdate;
    END IF;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;