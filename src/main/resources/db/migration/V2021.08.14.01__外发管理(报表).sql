SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP PROCEDURE IF EXISTS `p_material_outward`;
delimiter ;;
CREATE PROCEDURE `p_material_outward`(IN `start` DATE, IN `end` DATE, IN `processName` VARCHAR(100))
BEGIN
    /**
        2021年7月3日 外发序时表
        2021年7月10日 追加字段(是否直发)
    */
    SELECT a.id                                                        id,
           a.order_code                                                order_code,
           a.process_company                                           process_company,
           a.material_spec                                             material_spec,
           a.batch_num                                                 batch_num,
           ifnull(a.input_kilo, 0) * if(ifnull(c.is_return, 0), -1, 1) input_kilo,
           ifnull(a.box_amount, 0) * if(ifnull(c.is_return, 0), -1, 1) box_amount,
           b.supplier_name                                             supplier_name,
           DATE(a.order_date)                                          order_date,
           a.create_time                                               create_time,
           if(ifnull(a.is_send_directly, 0), '是', '否')                 is_send_directly,
           a.handler_name                                              handler_name,
           a.remarks                                                   remarks,
           c.bill_type_name                                            bill_type_name,
           ifnull(c.is_return, 0)                                      is_returning
    FROM material_order a
             LEFT JOIN supplier b ON a.supplier_id = b.id
             LEFT JOIN bill_type c ON a.bill_type = c.id
             LEFT JOIN process_company d ON a.process_company = d.company_name
    WHERE a.state = 1
      AND ifnull(c.company, 0) = 1
      AND ((c.access_mode < 0 AND ifnull(d.is_selected, 0) <= 0) OR ifnull(c.is_return, 0) = 1)
      AND if(ifnull(`processName`, '') = '' OR ifnull(`processName`, '') = '全部', TRUE,
             a.process_company = `processName`)
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.id DESC;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for p_product_inbound_report
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_product_inbound_report`;
delimiter ;;
CREATE PROCEDURE `p_product_inbound_report`(IN `start` DATE, IN `end` DATE, IN `clientName` VARCHAR(100))
BEGIN
    /**
     2021年7月2日  原料外发入库序时表
     2021年7月17日 追加字段
     2021年7月20日 按主键倒排序
     2021年8月4日 更改规格拼接顺序
     2021年8月14日 将客户条件改成加工户，按照加工户筛选
     */
    SELECT a.id                                                                            id,
           if(a.is_finished_product = 0, '未剖幅', '已剖幅')                                     is_finished_product,
           a.order_code                                                                    order_code,
           a.process_company                                                               process_company,
           CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                     b.greyfabric_specification, b.material_specification, b.product_name) material_spec,
           ifnull(a.input_kilo, 0)                                                         input_kilo,
           ifnull(a.input_pairs, 0)                                                        input_pairs,
           ifnull(a.box_amount, 0)                                                         box_amount,
           a.supplier_name                                                                 supplier_name,
           a.order_date                                                                    order_date,
           a.create_time                                                                   create_time,
           a.handler_name                                                                  handler_name,    -- 经手人
           c.user_name                                                                     create_user_name,
           round(ifnull(a.processing_cost, 0), 2)                                          processing_cost, -- 加工费
           round(ifnull(a.money, 0), 2)                                                    money,           -- 金额
           if(a.is_send_directly = 1, '是', '否')                                            is_send_directly,-- 是否直发
           a.consignee                                                                     consignee,
           a.remarks                                                                       remarks
    FROM product_outward_order a
             LEFT JOIN plan b ON a.plan_code = b.plan_code
             LEFT JOIN sys_user c ON a.create_user_id = c.id
             LEFT JOIN process_company d ON a.process_company = d.company_name
    WHERE a.state = 1
      AND ifnull(d.is_selected, 0) = 0
      AND length(ifnull(a.linked_order_code, '')) <= 0
      AND if(ifnull(`clientName`, '') = '' OR ifnull(`clientName`, '') = '全部', TRUE, a.process_company = `clientName`)
      AND DATE(a.order_date) BETWEEN DATE(`start`) AND DATE(`end`)
    ORDER BY a.id DESC;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
