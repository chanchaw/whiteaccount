/**
  2021年9月4日 13:42:59 基础资料 - 机台号 - 追加所属加工户
 */
alter table machine_type add process_compname varchar(200) default '' null comment '加工户名称';

/**
  2021年9月4日 13:43:28 上传来的打卷汇总表追加已导入标记字段
 */
alter table jrkbillsum add imported int default 0 null comment '已导入外加工入库,默认0表示还没有导入';

/**
  2021年9月4日 13:43:57 关联到导入时生成的数据主键
 */
alter table jrkbillsum add outwardid bigint default 0 null comment '=product_outward_order.id,标记关联的自动导入的数据';

/**
  2021年9月4日 13:44:46 外发入库的数据关联到计划单
 */
alter table product_outward_order add planid bigint default 0 null comment '=plan.id,做外发入库回写jrkbillsum时补充';

/**
  2021年9月4日 13:45:13 导入生成的数据关联到对应的打卷数据主键 - 一一对应
 */
alter table product_outward_order add jrkid int default 0 null comment '=jrkbillsum.id,标记当前自动生成的外发入库对应的打卷的主键';

/**
  移库操作生成的两笔数据通过该字段关联
 */
alter table jrkbill_adjust add opbatch varchar(100) default '' null comment '移库操作的多条数据填充同一个UUID';