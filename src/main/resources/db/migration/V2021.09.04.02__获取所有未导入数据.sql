create
    definer = root@`%` procedure usp_getUnImportedJRK(IN processCompname varchar(200))
BEGIN
    # 2021年8月15日 15:08:41 chanchaw
    # 查询获取指定加工户的所有未导入过的打卷数据
    # - 加工户和机台号的关联在表 machine_type 中
    # 下面的逻辑代码拷贝自 p_greyfabric_input
    # 追加条件：指定加工户、只显示未导入过的，按照打卷时间倒排序
    SELECT a.id as id,
    any_value(c.company_name)            client,
    any_value(b.plan_code)               planCode,
    any_value(a.cn)                      computerName,
    CONCAT_WS(' ', any_value(b.m_height), any_value(b.meter_length),
           CONCAT_WS('*', any_value(b.greyfabric_width), any_value(b.greyfabric_weight)),
           any_value(b.greyfabric_specification), any_value(b.material_specification),
           any_value(b.product_name)) specification,
    ifnull(a.ps, 0)                 ps,
    ifnull(a.qty, 0)                qty,
    a.createdate                         createdate,
    any_value(a.class_name)              className,
    any_value(a.jth)                     machineType,
    any_value(a.aorb)                    aorb,
    any_value(a.batch_num)               batchNum,
    NULL                                 isSendDirectly,
    a.createdate                         createTime,
    NULL                                 remarks,
    NULL                                 createUserName,
    NULL                                 handlerName,
    NULL                                 is_audit
    FROM jrkbillsum a
    LEFT JOIN plan b ON a.planid = b.id
    LEFT JOIN contact_company c ON b.contact_company_id = c.id
    WHERE a.jth in (select machine_name from machine_type
        where process_compname collate utf8_general_ci = processCompname collate utf8_general_ci)
    and a.imported = 0
    ORDER BY a.createdate DESC;
END;

