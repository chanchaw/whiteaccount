SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
DROP PROCEDURE IF EXISTS `p_greyfabric_storage`;
DELIMITER ;;
create procedure p_greyfabric_storage(IN mode int, IN m_EDate varchar(100))
BEGIN
    /**
     2021年6月9日 白坯库存
     */
 if mode = 1 THEN

    select re.*
		from (
       SELECT rep.clientName client,sum(rep.ps) ps,sum(rep.qty) qty,p.material_lot_no  batchNum,sum(rep.money) money

       FROM (
			       #白坯打卷入库
						 SELECT c.company_name as clientName,b.plan_code as planCode,b.product_name as productName,
              ifnull(a.ps, 0)as ps,
              ifnull(a.qty, 0)as qty,
              ifnull(b.greyfabric_price, 0)as price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)as money

						FROM jrkbillsum a
            LEFT JOIN plan b ON a.planid = b.id
            LEFT JOIN contact_company c ON b.contact_company_id = c.id
					  where ifnull(b.state,0)=1
						and DATE_FORMAT(a.createdate,'%Y-%m-%d')<=DATE_FORMAT(m_EDate,'%Y-%m-%d')

      UNION ALL

      #   收款调整
          (
			       SELECT c.company_name as clientName,b.plan_code as planCode,b.product_name as productName,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)as ps,
              if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)as qty,
              ifnull(a.price, 0)as price,
              round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2)as money

								FROM jrkbill_adjust a
                LEFT JOIN plan b ON a.plan_id = b.id
                LEFT JOIN contact_company c ON b.contact_company_id = c.id
								where ifnull(b.state,0)=1
								 and DATE_FORMAT(a.create_time,'%Y-%m-%d')<=DATE_FORMAT(m_EDate,'%Y-%m-%d')  #使用修改日期
				   )

        union all

        #  白坯入库(退货)
        (
            SELECT c.company_name as clientName,a.plan_code as planCode,b.product_name as productName,
                   ifnull(a.input_pairs, 0)as ps,ifnull(a.input_kilo, 0)as qty,
									 ifnull(a.processing_cost, 0)as  price,round(a.money, 2)as money

            FROM product_outward_order a
            LEFT JOIN plan b ON a.plan_code = b.plan_code
            LEFT JOIN contact_company c ON b.contact_company_id = c.id
            WHERE a.state = 1
              AND (ifnull(a.is_send_directly, 0) = 0 or ifnull(a.storage_id,0)=1)
              AND ifnull(a.is_finished_product, 0) = 1
							and ifnull(b.state,0)=1
							and DATE_FORMAT(a.order_date,'%Y-%m-%d')<=DATE_FORMAT(m_EDate,'%Y-%m-%d')
        )

        union all


        # 发货管理
        (
							SELECT a.client as clientName,
               c.plan_code as planCode,
               c.product_name as productName,
               -ifnull(a.ps, 0)as ps,
               -ifnull(a.qty, 0)as qty,
               ifnull(a.price, 0)as price,
               round(ifnull(a.price, 0) * -ifnull(a.qty, 0), 2)as money

							FROM whitebillfh a
							LEFT JOIN plan c ON a.planid = c.id
							where ifnull(c.state,0)=1
							and DATE_FORMAT(a.datafh,'%Y-%m-%d')<=DATE_FORMAT(m_EDate,'%Y-%m-%d')

				)
                ) rep
        left join contact_company co on rep.clientName = co.company_name
        left join plan p on rep.planCode = p.plan_code
GROUP BY rep.clientName,co.id order by co.id ) re where abs(ifnull(re.ps,0))>0;


else


select re.*,p.material_lot_no batchNum
from
	(
      SELECT any_value(rep.clientName) client,
       rep.planCode,
       any_value(rep.productName) productName,
       sum(rep.ps) ps,
       sum(rep.qty) qty,
       avg(rep.price) price,
       sum(rep.money) money,
       any_value(rep.specification) specification

		FROM (

			   #白坯打卷入库
					SELECT c.company_name as clientName,b.plan_code as planCode,b.product_name as productName,
              ifnull(a.ps, 0)as ps,ifnull(a.qty, 0)as qty,ifnull(b.greyfabric_price, 0)as price,
              round(ifnull(b.greyfabric_price, 0) * ifnull(a.qty, 0), 2)as money,
              CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.greyfabric_specification, b.material_specification,b.product_name) specification,
							a.batch_num

				 FROM jrkbillsum a
				 LEFT JOIN plan b ON a.planid = b.id
				 LEFT JOIN contact_company c ON b.contact_company_id = c.id
				 where ifnull(b.state,0)=1
				 and DATE_FORMAT(a.createdate,'%Y-%m-%d')<=DATE_FORMAT(m_EDate,'%Y-%m-%d')


      UNION ALL

				#   收款调整
					SELECT c.company_name as clientName,b.plan_code as planCode,b.product_name as productName,
								if(a.bill_type = '手动出库', -1, 1) * ifnull(a.pairs, 0)as ps,
								if(a.bill_type = '手动出库', -1, 1) * ifnull(a.qty, 0)as qty,
								ifnull(a.price, 0)as price,
								round(ifnull(a.price, 0) * ifnull(a.qty, 0) * if(a.bill_type = '手动出库', -1, 1), 2) money,
								CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
														 b.greyfabric_specification, b.material_specification,b.product_name)       specification,
								null      batch_num

					 FROM jrkbill_adjust a
					 LEFT JOIN plan b ON a.plan_id = b.id
					 LEFT JOIN contact_company c ON b.contact_company_id = c.id
					 where ifnull(b.state,0)=1
					 and DATE_FORMAT(a.create_time,'%Y-%m-%d')<=DATE_FORMAT(m_EDate,'%Y-%m-%d')  #使用修改日期


       union all

				     #白坯退回
            SELECT c.company_name as clientName,a.plan_code as planCode,b.product_name as productName,
                   ifnull(a.input_pairs, 0)as ps,ifnull(a.input_kilo, 0)as qty,
                   ifnull(a.processing_cost, 0)as price,round(a.money, 2)as money,
                   CONCAT_WS(' ', b.m_height, b.meter_length, CONCAT_WS('*', b.greyfabric_width, b.greyfabric_weight),
                           b.greyfabric_specification, b.material_specification,b.product_name) specification,
                   a.batch_num
            FROM product_outward_order a
            LEFT JOIN plan b ON a.plan_code = b.plan_code
            LEFT JOIN contact_company c ON b.contact_company_id = c.id
            WHERE a.state = 1
              AND (ifnull(a.is_send_directly, 0) = 0 or ifnull(a.storage_id,0)=1)
              AND ifnull(a.is_finished_product, 0) = 1
							and ifnull(b.state,0)=1
							and DATE_FORMAT(a.order_date,'%Y-%m-%d')<=DATE_FORMAT(m_EDate,'%Y-%m-%d')

					union all

					 # 发货管理
						 SELECT a.client as clientName,c.plan_code as planCode,c.product_name as productName,
									 -ifnull(a.ps, 0)as ps,-ifnull(a.qty, 0)as qty,ifnull(a.price, 0)as price,
									 round(ifnull(a.price, 0) * -ifnull(a.qty, 0), 2)as money,
									 CONCAT_WS(' ', c.m_height, c.meter_length, CONCAT_WS('*', c.greyfabric_width, c.greyfabric_weight),
															 c.greyfabric_specification, c.material_specification,c.product_name) specification,
									 c.material_lot_no                                               batch_num
						FROM whitebillfh a
						LEFT JOIN plan c ON a.planid = c.id
						where ifnull(c.state,0)=1
						and DATE_FORMAT(a.datafh,'%Y-%m-%d')<=DATE_FORMAT(m_EDate,'%Y-%m-%d')


      ) rep
        left join contact_company co
				on rep.clientName = co.company_name

GROUP BY rep.planCode

) re
left join plan p
on re.planCode = p.plan_code
where abs(ifnull(re.ps,0))>0

order by cast(re.specification as decimal(10,2)),re.specification ;

end IF;

END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;