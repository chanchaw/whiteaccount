SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
DROP PROCEDURE IF EXISTS `p_plan_dtl`;
DELIMITER ;;
CREATE PROCEDURE `p_plan_dtl`(IN id bigint)
BEGIN
    /**
      2021年5月22日 计划单明细查看
     */
    SELECT b.company_name                                                                      company_name,
           concat(ifnull(a.plan_code, ''), ' ', ifnull(a.product_name, ''), ' ', a.greyfabric_weight, '*',
                  a.greyfabric_width, ' ', ifnull(a.m_height, 0), ' ', ifnull(a.greyfabric_specification, ''), ' ',
                  ifnull(a.material_specification, ''))                                        product_name,
           ifnull(a.plan_kilo, 0)                                                              plan_kilo,
           ifnull(c.pairs, 0) + ifnull(f.pairs, 0) + ifnull(h.pairs, 0) + ifnull(i.pairs, 0)   input_pairs,
           ifnull(c.kilo, 0) + ifnull(f.kilo, 0) + ifnull(h.kilo, 0) + ifnull(i.kilo, 0)       input_kilo,
           ifnull(d.kilo, 0) + ifnull(e.kilo, 0) + ifnull(i.kilo, 0)                           send_kilo,
           ifnull(d.pairs, 0) + ifnull(e.pairs, 0) + ifnull(i.pairs, 0)                        send_pairs,
           ifnull(round(ifnull(a.plan_kilo, 0) - ifnull(d.kilo, 0) - ifnull(e.kilo, 0), 1), 0) plan_remain_kilo,
           ifnull(round(ifnull(c.kilo, 0) + ifnull(f.kilo, 0) - ifnull(d.kilo, 0) - ifnull(e.kilo, 0) +
                        ifnull(g.kilo, 0) + ifnull(h.kilo, 0), 1), 0)                          remain_kilo,
           ifnull(round(ifnull(c.pairs, 0) + ifnull(f.pairs, 0) - ifnull(d.pairs, 0) - ifnull(e.pairs, 0) +
                        ifnull(g.pairs, 0) + ifnull(h.pairs, 0), 0), 0)                        remain_pairs,
           ifnull(a.greyfabric_price, 0)                                                       greyfabric_price
    FROM plan a
             LEFT JOIN contact_company b ON a.contact_company_id = b.id
             LEFT JOIN (
        # 总入库公斤
        SELECT a.id, ifnull(b.pairs, 0) pairs, ifnull(b.kilo, 0) kilo
        FROM plan a
                 LEFT JOIN (SELECT planid, sum(ps) pairs, sum(qty) kilo FROM jrkbillsum GROUP BY planid) b
                           ON a.id = b.planid) c ON c.id = a.id
             LEFT JOIN (
        # 已发公斤
        SELECT a.planid id, ifnull(sum(round(a.qty, 1)), 0) kilo, ifnull(sum(a.ps), 0) pairs
        FROM whitebillfh a
             #                  LEFT JOIN whitebilldetailfh b ON a.id = b.fid
        GROUP BY a.planid) d ON d.id = a.id
             LEFT JOIN (
        # 手动出库
        SELECT ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo, plan_id
        FROM jrkbill_adjust
        WHERE state = 1
          AND bill_type in ('手动出库')
        GROUP BY plan_id) e ON e.plan_id = a.id
             LEFT JOIN (
        #             手动入库
        SELECT ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo, plan_id
        FROM jrkbill_adjust
        WHERE state = 1
          AND bill_type IN ('手动入库','移库')
        GROUP BY plan_id) f ON f.plan_id = a.id
             LEFT JOIN (
        #            盘点
        SELECT ifnull(sum(pairs), 0) pairs, ifnull(sum(qty), 0) kilo, plan_id
        FROM jrkbill_adjust
        WHERE state = 1
          AND bill_type IN ('盘点')
        GROUP BY plan_id) g ON g.plan_id = a.id
        #   白坯入库
             LEFT JOIN (SELECT ifnull(sum(input_pairs), 0) pairs, ifnull(sum(input_kilo), 0) kilo, plan_code
                        FROM product_outward_order
                        WHERE state = 1
                          AND ifnull(is_finished_product, 0) = 1
                          AND ifnull(is_send_directly, 0) = 0
                          AND ifnull(storage_id, 0) <= 0
                        GROUP BY plan_code) h ON h.plan_code = a.plan_code
        #     白坯发货
             LEFT JOIN (SELECT ifnull(sum(if(ifnull(storage_id, 0) = 1, -1, 1) * input_pairs), 0) pairs,
                               ifnull(sum(input_kilo), 0)                                         kilo,
                               plan_code
                        FROM product_outward_order
                        WHERE state = 1
                          AND ifnull(is_finished_product, 0) = 1
                          AND ((ifnull(is_send_directly, 0) = 1 AND length(ifnull(linked_order_code, '')) > 0) OR
                               ifnull(storage_id, 0) = 1)
                        GROUP BY plan_code) i ON i.plan_code = a.plan_code
    WHERE a.id = `id`;
END
;;
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;