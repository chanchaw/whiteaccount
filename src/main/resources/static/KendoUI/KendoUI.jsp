<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<link rel="stylesheet" href="Source/KendoUI/styles/kendo.common.min.css" />
<link rel="stylesheet" href="Source/KendoUI/styles/kendo.default.min.css" />
<link rel="stylesheet" href="Source/KendoUI/styles/kendo.default.mobile.min.css" />
<link rel="stylesheet" href="Source/KendoUI/styles/kendo.rtl.min.css" />
<script type="text/javascript" src="Source/KendoUI/js/jszip.min.js"></script>
<script type="text/javascript" src="Source/KendoUI/js/kendo.all.min.js"></script>
<script type="text/javascript" src="Source/KendoUI/js/messages/kendo.messages.zh-CN.min.js"></script>
<script type="text/javascript" src="Source/KendoUI/mergeRows.js"></script>