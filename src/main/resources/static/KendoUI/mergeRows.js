/**
 *  kendouiGrid合并单元格
 */
function kendoRowSpan(id, colorOrderNumber) {
    var table_firstitem = null
    var table_firsttd = ''
    var table_number = 0
    for (let i of colorOrderNumber) {
        var element = $('#' + id).find('td:nth-child(' + i + ')')
        if (!element) continue;
        element.each(function (index, item) {
            let text = $(item).html()
            if (table_number == 0) {
                table_firstitem = text
                table_number++
                table_firsttd = $(item)
            } else {
                if (text != table_firstitem) {
                    table_firsttd.attr('rowSpan', table_number)
                    table_firsttd.css({'borderBottom': '1px solid rgb(213,213,213)'})
                    table_firsttd = $(item)
                    table_number = 1
                    table_firstitem = text
                } else {
                    $(item).css('display', 'none')
                    table_number++
                }
            }
        })
        if (table_number != 0) {
            table_firsttd.attr('rowSpan', table_number)
            table_firsttd.css({'borderBottom': '1px solid rgb(213,213,213)'})
            table_number = 0
            table_firsttd = ''
            table_firstitem = null
        }
    }
}

/**
 * 获取当前(kendouigrid)网格组件合并列的序号
 * @param gridId
 * @param menuId
 */
function getGridColumnArray(gridId, menuId) {
    let grid = $('#' + gridId).data('kendoGrid')
    if (grid == undefined) return []
    let columns = grid.getOptions().columns
    let localColumns = sessionStorage.getItem(gridId) ? JSON.parse(sessionStorage.getItem(gridId)) : []
    let arr = []
    if (localColumns.length <= 0) {
        $.ajax({
            url: 'deploy/getMergeCol',
            data: {
                id: menuId
            },
            type: 'post',
            dataType: 'json',
            success: function (data) {
                if (!data) return;
                sessionStorage[gridId] = JSON.stringify(data)
                for (let i = 0; i < columns.length; i++) {
                    let item = columns[i]
                    if (item != undefined) {
                        for (let n of localColumns) {
                            if (n['fieldname'] == item['field']) {
                                arr.push(i + 1)
                                break
                            }
                        }
                    }
                }

            },
        })
    } else {
        for (let i = 0; i < columns.length; i++) {
            let item = columns[i]
            if (item != undefined) {
                for (let n of localColumns) {
                    if (n['fieldname'] == item['field']) {
                        arr.push(i + 1)
                        break
                    }
                }
            }
        }
    }
    return arr
}