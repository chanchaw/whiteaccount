/**
 * 定义已格全局ajax事件处理，用来监听请求
 */
$(document).ajaxComplete(function (event, xhr, options) {
    if (xhr.responseJSON) {
        if (xhr.responseJSON.state == 5008) {
            if (top.location != self.location) {
                window.parent.location.reload();
            } else {
                window.location.reload();
            }
        }
    }
});
$(document).ajaxError(function (event, xhr, options) {

});