/**
 * Easyui 网格单元格编辑
 */
$.extend($.fn.datagrid.methods, {
    editCell: function (jq, param) {
        return jq.each(function () {
            var opts = $(this).datagrid('options');
            var fields = $(this).datagrid('getColumnFields', true).concat($(this).datagrid('getColumnFields'));
            for (var i = 0; i < fields.length; i++) {
                var col = $(this).datagrid('getColumnOption', fields[i]);
                col.editor1 = col.editor;
                if (fields[i] != param.field) {
                    col.editor = null;
                }
            }
            $(this).datagrid('beginEdit', param.index);
            var ed = $(this).datagrid('getEditor', param);
            if (ed) {
                if ($(ed.target).hasClass('textbox-f')) {
                    $(ed.target).textbox('textbox').focus();
                } else {
                    $(ed.target).focus();
                }
            }
            for (var i = 0; i < fields.length; i++) {
                var col = $(this).datagrid('getColumnOption', fields[i]);
                col.editor = col.editor1;
            }
        });
    },
    enableCellEditing: function (jq) {
        return jq.each(function () {
            var dg = $(this);
            var opts = dg.datagrid('options');
            opts.oldOnClickCell = opts.onClickCell;
            opts.onClickCell = function (index, field) {
                if (opts.editIndex != undefined) {
                    if (dg.datagrid('validateRow', opts.editIndex)) {
                        dg.datagrid('endEdit', opts.editIndex);
                        opts.editIndex = undefined;
                    } else {
                        return;
                    }
                }
                dg.datagrid('selectRow', index).datagrid('editCell', {
                    index: index,
                    field: field
                });
                opts.editIndex = index;
                opts.oldOnClickCell.call(this, index, field);
            }
        });
    }
});

/**
 * Easyui拓展获取格式化列
 */
$.extend($.fn.datagrid.methods, {
    getFormatRows: function (jq, param) {
        function getRows(target) {
            let state = $(target).data('datagrid');
            if (state['filterSource']) {
                return state.filterSource['originalRows'];
            } else if (state.data['originalRows']) {
                return state.data.originalRows;
            } else {
                return state.data.rows;
            }
        }

        function getFormatRows(target) {
            let rows = JSON.parse(JSON.stringify(getRows($(target)))) || [];
            let formatters = $(target).edatagrid('options').columns[0];
            let formatter = {};
            formatters.map(item => {
                formatter[item['field']] = item['formatter'] || null;
            });
            return rows.map(item => {
                for (let i in item) {
                    let fn = formatter[i];
                    if (fn) {
                        item[i] = fn(item[i], item);
                    }
                }
                return item;
            })
        }

        return getFormatRows($(jq));
    }
})

/**
 *拓展datagrid的弹框编辑器
 *
 */
$.extend($.fn.datagrid.methods, {
    initEditableDialog: function (jq, param) {
        var datagrid = $(jq);
        var row = datagrid.datagrid('getSelected');
        if (!row) {
            $.messager.alert("提示", "请选择行!", "warning");
            return;
        }
        //新增div
        var body = document.body;
        var div = document.createElement("div");
        div.setAttribute("id", "datagrid_edit_dialog");
        body.appendChild(div);
        //生成dialog,form以及元素
        var dialog_editing = $("#datagrid_edit_dialog");
        var width = 320;
        var height = 500;
        dialog_editing.append("<form class='easyui-form' method='post'></form>");
        if ('width' in param) {
            width = param.width;
        }
        if ('height' in param) {
            height = param.height;
            if (param.height === 'auto') {
                let opts = datagrid.datagrid('options').columns;
                var index = 0;
                for (let i of opts[0]) {
                    if (!!i['input']) index++;
                }
                height = index * 33.5 + 76
            }
        }
        dialog_editing.dialog({
            width: width,
            height: height,
            modal: true,
            closed: false,
            border: false,
            title: '编辑页面',
            resizable: true,
            onBeforeClose: function () {
                var that = this;
                $.messager.confirm("提示", "关闭编辑框将不会保存输入的内容,确定?", function (r) {
                    if (r) {
                        $(that).dialog("destroy");
                    }
                });
                return false;
            },
            buttons: [
                {text: '保存', iconCls: 'icon-save', handler: save},
                {text: '取消', iconCls: 'icon-cancel', handler: cancel}]
        });
        //生成编辑框
        var options = datagrid.datagrid('options');
        var title = "";
        var type = "";
        var field_id = ""
        var dataoptions = undefined;
        for (i = 0; i < options.columns[0].length; i++) {
            var field = options.columns[0][i];
            title = field.title;
            field_id = field.field;
            if ('input' in field) {
                if ('type' in field.input) {
                    type = field.input.type;
                } else {
                    continue;
                }
                if ('dataoptions' in field.input) {
                    dataoptions = field.input.dataoptions;
                }
            } else {
                continue;
            }
            dialog_editing.find("form").append("<div style='width:100%;height:25px;margin-top:8px;margin-buttom:8px;'>" +
                "<label style='width:90px;text-align:right;'>" + title + ":"
                + "</label>" + "<input style='height:25px;' name='" + field_id + "' id='" + field_id + "' style='float:left;width:calc(95% - 90px);' class='" + type + "'>" + "</div>");
            $('#' + field_id).attr('data-options', dataoptions);
        }
        $.parser.parse(dialog_editing);
        dialog_editing.window("center");
        dialog_editing.find("form").form('load', row);
        dialog_editing.dialog('open');

        function extend(target, source) {
            for (var obj in source) {
                target[obj] = source[obj];
            }
            return target;
        }

        function save() {
            dialog_editing.find("form").form('submit', {
                url: param.url,
                onSubmit: function (p) {
                    p = extend(p, param.extra);
                    if ($(this).form('validate'))
                        $.messager.progress({title: '提示', msg: '正在保存中，请稍候......', interval: 1000});
                    return $(this).form('validate');
                },
                success: function (data) {
                    try {
                        data = eval('(' + data + ')');
                    } catch (e) {
                        $.messager.progress('close');
                    }
                    if (data.state == 200) {
                        $.messager.show({
                            title: '成功',
                            msg: data.message
                        });
                        datagrid.datagrid("reload");
                        dialog_editing.dialog("destroy");
                    } else {
                        $.messager.show({
                            title: data.message,
                            msg: data.message
                        });
                    }
                    $.messager.progress('close');
                },
                onLoadError: function () {
                    $.messager.show({
                        title: "<p style='color:red;'>错误</p>",
                        msg: "无法连接服务器,请检查网络或者重新登录!"
                    });
                    $.messager.progress('close');
                }
            });
        }

        //取消
        function cancel() {
            $.messager.confirm("提示", "关闭编辑框将不会保存输入的内容,确定?", function (r) {
                if (r) {
                    dialog_editing.dialog("destroy");
                }
            });
        }

    },
    initAddDialog: function (jq, param) {
        //width,height,url,extra
        var datagrid = $(jq);
        //新增div
        var body = document.body;
        var div = document.createElement("div");
        div.setAttribute("id", "datagrid_edit_dialog");
        body.appendChild(div);
        //生成dialog,form以及元素
        var dialog_editing = $("#datagrid_edit_dialog");
        var width = 320;
        var height = 500;
        dialog_editing.append("<form class='easyui-form' method='post'></form>");
        if ('width' in param) {
            width = param.width;
        }
        if ('height' in param) {
            height = param.height;
            if (param.height == 'auto') {
                let opts = datagrid.datagrid('options').columns;
                var index = 0;
                for (let i of opts[0]) {
                    if (!!i['input']) index++;
                }
                height = index * 33.5 + 76
            }
        }
        dialog_editing.dialog({
            width: width,
            height: height,
            border: false,
            modal: true,
            closed: false,
            title: '编辑页面',
            resizable: true,
            onBeforeClose: function () {
                var that = this;
                $.messager.confirm("提示", "关闭编辑框将不会保存输入的内容,确定?", function (r) {
                    if (r) {
                        $(that).dialog("destroy");
                    }
                });
                return false;
            },
            buttons: [
                {text: '保存', iconCls: 'icon-save', handler: save},
                {text: '取消', iconCls: 'icon-cancel', handler: cancel}]
        });
        //生成编辑框
        var options = datagrid.datagrid('options');
        var title = "";
        var type = "";
        var field_id = ""
        var dataoptions = "";
        for (i = 0; i < options.columns[0].length; i++) {
            var field = options.columns[0][i];
            title = field.title;
            field_id = field.field;
            if ('input' in field) {
                if ('type' in field.input) {
                    type = field.input.type;
                } else {
                    continue;
                }
                if ('dataoptions' in field.input) {
                    dataoptions = field.input.dataoptions;
                }
            } else {
                continue;
            }
            dialog_editing.find("form").append("<div style='width:100%;height:25px;margin-top:8px;margin-buttom:8px;'>" +
                "<label style='width:90px;text-align:right;'>" + title + ":"
                + "</label>" + "<input style='height:25px;' name='" + field_id + "' id='" + field_id + "' style='float:left;width:calc(95% - 90px);' class='" + type + "'>" + "</div>");
            $('#' + field_id).attr('data-options', dataoptions);
        }
        $.parser.parse(dialog_editing);
        dialog_editing.window("center");
        dialog_editing.dialog('open');

        function extend(target, source) {
            for (var obj in source) {
                target[obj] = source[obj];
            }
            return target;
        }

        //保存按钮
        function save() {
            dialog_editing.find("form").form('submit', {
                url: param.url,
                onSubmit: function (p) {
                    p = extend(p, param.extra);
                    if ($(this).form('validate'))
                        $.messager.progress({title: '提示', msg: '正在保存中，请稍候......', interval: 1000});
                    return $(this).form('validate');
                },
                success: function (data) {
                    try {
                        data = eval('(' + data + ')');
                    } catch (e) {
                        $.messager.progress('close');
                    }
                    if (data.state == 200) {
                        $.messager.show({
                            title: '成功',
                            msg: data.message
                        });
                        datagrid.datagrid("reload");
                        dialog_editing.dialog("destroy");
                    } else {
                        $.messager.show({
                            title: data.message,
                            msg: data.message
                        });
                    }
                    $.messager.progress('close');
                },
                onLoadError: function () {
                    $.messager.show({
                        title: "<p style='color:red;'>错误</p>",
                        msg: "无法连接服务器,请检查网络或者重新登录!"
                    });
                    $.messager.progress('close');
                }
            });
        }

        //取消
        function cancel() {
            $.messager.confirm("提示", "关闭编辑框将不会保存输入的内容,确定?", function (r) {
                if (r) {
                    dialog_editing.dialog("destroy");
                }
            });
        }

    },
    getEditingRowIndexs: function (jq) {
        var rows = $.data(jq[0], "datagrid").panel.find('.datagrid-row-editing');
        var indexs = [];
        rows.each(function (i, row) {
            var index = row.sectionRowIndex;
            if (indexs.indexOf(index) == -1) {
                indexs.push(index);
            }
        });
        return indexs;
    }
});

/**
 * 为tree拓展的弹框编辑节点
 */
$.extend($.fn.tree.methods, {
    extendEdit: function (jq, param) {
        /*
         * param:state,id,text,pId,url
         */
        var body = document.body;
        var tree = $(jq);
        var options = $(jq).tree('options');
        var selected = tree.tree("getSelected");
        var parent = null;
        if (!selected) {
            parent = tree.tree('getParent', selected.target).id;
        }
        if (param.state == 'edit') {
            if (!selected) {
                $.messager.alert("提示", "未选中节点！", "warning");
                return;
            }
        }
        if (typeof param == 'string') {

        } else {
            var div = document.createElement("div");
            div.setAttribute("id", "tree_edit_dialog")
            body.appendChild(div);
            var title = "";
            var field_id = "";
            var type = "";
            var dataoptions = "";
            $("#tree_edit_dialog").append("<form class='easyui-form' method='post'></form>");
            for (var i = 0; i < 3; i++) {
                if (i == 0) {
                    title = "编号";
                    field_id = param.id;
                    type = "easyui-textbox";
                    dataoptions = "required:true";
                } else if (i == 1) {
                    title = "名称";
                    field_id = param.text;
                    type = "easyui-textbox";
                    dataoptions = "required:true";
                } else {
                    title = "上级";
                    field_id = param.pId;
                    type = "easyui-combotree";
                    dataoptions = "panelWidth:140,panelHeight:200,required:false,url:\'" + options.url + "\',onClick:function(node){var tree=$(this).combotree(\'tree\').tree(\'getSelected\');" +
                        "$(this).combotree(\'setValue\',tree.id)" +
                        "}";
                }
                $("#tree_edit_dialog").find("form").append("<div style='width:100%;height:25px;margin-top:8px;margin-buttom:8px;'>" +
                    "<label style='width:90px;text-align:right;'>" + title + ":"
                    + "</label>" + "<input style='height:25px;' name='" + field_id +
                    "' style='float:left;width:calc(95% - 90px);' class='" +
                    type + "' data-options=\"" + dataoptions + "\">" + "</div>");
            }
            $("#tree_edit_dialog").dialog({
                title: "编辑",
                onBeforeClose: function () {
                    var that = this;
                    $.messager.confirm("提示", "关闭编辑框将不会保存输入的内容,确定?", function (r) {
                        if (r) {
                            $(that).dialog("destroy");
                        }
                    });
                    return false;
                },
                modal: true,
                width: 300,
                height: 190,
                buttons: [{text: '保存', iconCls: 'icon-save', handler: save},
                    {text: '取消', iconCls: 'icon-cancel', handler: cancel}]
            });
            $.parser.parse($("#tree_edit_dialog"));
            if (param.state == 'edit') {
                var row1 = {};
                row1[param.id] = selected.id;
                row1[param.text] = selected.text;
                row1[param.pId] = parent;
                $("#tree_edit_dialog").find("form").form('load', row1);
            }
            $("#tree_edit_dialog").window("center");

            //保存按钮
            function save() {
                $("#tree_edit_dialog").find("form").form('submit', {
                    url: param.url,
                    onSubmit: function (p) {
                        p[param.pId] == p[param.pId] === "" ? null : p[param.pId];
                        if ($(this).form('validate'))
                            $.messager.progress({title: '提示', msg: '正在保存中，请稍候......', interval: 1000});
                        return $(this).form('validate');
                    },
                    success: function (data) {
                        try {
                            data = eval('(' + data + ')');
                        } catch (e) {
                            $.messager.progress('close');
                        }
                        if (data.state == 200) {
                            $.messager.show({
                                title: '成功',
                                msg: data.message
                            });
                            tree.tree("reload");
                            $("#tree_edit_dialog").dialog("destroy");
                        } else {
                            $.messager.show({
                                title: data.message,
                                msg: data.message
                            });
                        }
                        $.messager.progress('close');
                    },
                    onLoadError: function () {
                        $.messager.show({
                            title: "<p style='color:red;'>错误</p>",
                            msg: "无法连接服务器,请检查网络或者重新登录!"
                        });
                        $.messager.progress('close');
                    }
                });
            }

            //取消按钮
            function cancel() {
                $.messager.confirm("提示", "关闭编辑框将不会保存输入的内容,确定?", function (r) {
                    if (r) {
                        $("#tree_edit_dialog").dialog("destroy");
                    }
                });
            }
        }
    }
});

//表单转json
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

/***
 * 自定义验证
 */
// 时间格式规范
var regex_dateTime = /\d{4}\-\d{2}\-\d{2} \d{2}\:\d{2}:\d{2}/;//@author ren
$.extend($.fn.validatebox.defaults.rules, {
    // 字符最大长度（param传参）
    maxLength: {
        validator: function (value, param) {
            return value.length <= param[0];
        },
        message: '您输入的字数太长了,最多{0}个字'
    },

    //最大值判断
    max: {
        validator: function (value, param) {
            return value <= param[0]
        },
        message: '请输入一个最大为{0}的值'
    },

    //最小值判断
    min: {
        validator: function (value, param) {
            return value >= param[0]
        },
        message: '请输入一个最小为{0}的值'
    },

    // 验证姓名，可以是中文或英文

    name: {
        validator: function (value) {
            return /^[\u0391-\uFFE5]{1,20}$/i.test(value) | /^\w+[\w\s]+\w+$/i.test(value);
        },
        message: '姓名字数过多或应为中文或者英文'
    },

    // 验证身份证
    idcard: {
        validator: function (value) {
            return /^\d{15}(\d{2}[Xx0-9])?$/i.test(value);
        },
        message: '身份证应为15位或者18位'
    },

    // 验证IP地址
    ip: {
        validator: function (value) {
            return /\d+\.\d+\.\d+\.\d+/.test(value);
        },
        message: 'IP地址格式不正确'
    },

    //年龄验证
    age: {
        validator: function (value) {
            return /^[0-9]{1,2}$/i.test(value);//0-99
        },
        message: '您输入的年龄不合法'
    },

    // 验证电话号码
    phone: {
        validator: function (value) {
            return /^1\d{10}$/i.test(value) || /^0\d{2,3}-?\d{7,8}$/i.test(value);
        },
        message: '电话号码正确格式:15288888888或020(0310)-88888888'
    },

    // 验证数字,整数或小数
    number: {
        validator: function (value) {
            return /^\d{1,10}(\.\d{0,4})?$/i.test(value);
        },
        message: '请输入正确的金额'
    },

    // 验证数字,只能为整数
    integer: {
        validator: function (value) {
            return /^\d{1,12}$/i.test(value);
        },
        message: '请输入一个整数'
    },


    // 时间验证
    //@author ren
    /* start */
    endToStart: {
        validator: function (value, param) {
            return value > $("#" + param[0] + " input[name='" + param[1] + "']").val();//结束时间>开始时间
        },
        message: '结束时间应晚于起始时间'
    },

    startToEnd: {
        validator: function (value, param) {
            return value > $("#" + param[0]).datetimebox('getValue');//结束时间>开始时间
        },
        message: '结束时间应晚于起始时间'
    },

    datetimeValidate: {
        validator: function (value, param) {
            return regex_dateTime.test(value);//验证时间格式是否规范
        },
        message: '时间格式应为 2015-01-01 12:00:00'
    }
    /* end */
});

/**
 * 2020年12月3日
 * 拓展插件--日期输入框
 * 将日期输入框默认显示为____-__-__的形式,时间输入框默认显示为____-__-__ __:__:__
 * 并且只能输入数字
 */
$(function () {
    const PLACEHOLDER = '_';
    const DATE_SEPARATOR = '-';
    const TIME_SEPARATOR = ':';
    const DEFAULT_DATE = '____-__-__';
    const DEFAULT_TIME = '____-__-__ __:__:__';

    //  为每个组件添加cls
    function init() {
        $('.easyui-datebox').each(function () {
            if ($(this).datebox().length > 0) {
                $(this).datebox({
                    cls: 'custum-date-input'
                })
            }
        })
        $('.easyui-datetimebox').each(function () {
            if ($(this).datetimebox().length > 0) {
                $(this).datetimebox({
                    cls: 'custum-datetime-input'
                })
            }
        })
    }

    //  判断当前浏览器是否IE
    function isIE() {
        return !!window.ActiveXObject || 'ActiveXObject' in window;
    }

    //  设置光标位置
    function setCursor(element, index) {
        if (!isIE()) {
            element.selectionStart = index;
            element.selectionEnd = index;
        } else {
            let range = element.createTextRange();
            range.moveStart('character', -element.value.length);
            range.move('character', index);
            range.select();
        }
    }

    //  获取光标起始索引
    function getCursorIndex(element) {
        if (!isIE()) {
            return element.selectionStart;
        } else {
            let range = element.createTextRange();
            range.moveStart('character', -element.value.length);
            return range.text.length;
        }
    }

    //  获取选中的字符串
    function getSelections(element) {
        if (!isIE()) {
            let _value = element.value;
            return _value.substring(element.selectionStart, element.selectionEnd);
        } else {
            return document.selection.createRange().text;
        }
    }

    //  替换选中位置
    function replaceSelectionArea(element, selectionStart, selectionEnd, defaultValue = DEFAULT_DATE) {
        selectionEnd = selectionEnd > defaultValue.length ? defaultValue.length : selectionEnd;
        let currentValueArr = element.value.split('');
        currentValueArr.splice(selectionStart, 0, defaultValue.substring(selectionStart, selectionEnd));
        element.value = currentValueArr.join('');
    }

    //  创建光标选中区域
    function setSelections(element, selectionStart, selectionEnd) {
        if (!isIE()) {
            element.selectionStart = selectionStart;
            element.selectionEnd = selectionEnd;
        } else {
            let range = element.createRange();
            range.move('character', -element.value.length);
            range.moveEnd('character', selectionEnd);
            range.moveStart('character', selectionStart);
            range.select();
        }
    }

    //  重置光标,将光标放入正确位置
    function resetCursor(element, defaultValue = DEFAULT_DATE) {
        let _value = element.value;
        let _length = _value.length;
        //  多选删除则重置
        if (_length !== defaultValue.length) {
            element.value = defaultValue;
            setCursor(element, 0);
            return;
        }
        let index = _value.search(PLACEHOLDER);
        setCursor(element, index !== -1 ? index : _value.length);
    }

    init();
    let dateboxInputs = $('.custum-date-input > input[type!=hidden]');
    let datetimeboxInputs = $('.custum-datetime-input > input[type!=hidden]');
    dateboxInputs.each(function (index, item) {
        $(item).on('keydown', function (e) {
            let currentInputElement = this;
            let keyCode = e.keyCode;
            let cursorIndex = getCursorIndex(currentInputElement);
            if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || keyCode == 229) {
                //  输入数字键
                let reg = /(\d)_/
                setTimeout(function () {
                    let _value = currentInputElement.value;
                    //  判断当前是否为最后一个字符
                    if (cursorIndex >= DEFAULT_DATE.length) {
                        let arr = _value.split('');
                        arr.splice(DEFAULT_DATE.length - 1, 1);
                        currentInputElement.value = arr.join('').substring(0, DEFAULT_DATE.length);
                        setCursor(currentInputElement, DEFAULT_DATE.length);
                        return;
                    }
                    let cursorEnd = _value.substring(cursorIndex);
                    currentInputElement.value = _value.substr(0, cursorIndex) + cursorEnd.replace(reg, '$1');
                    resetCursor(currentInputElement);
                }, 10)
                return true;
            } else if (keyCode == 8) {
                //  按退格键
                let _value = this.value;
                if (!cursorIndex && !getSelections(currentInputElement).length) return false;
                //  选中清空
                if (getSelections(currentInputElement).length > 1) {
                    let selectStart = currentInputElement.selectionStart;
                    let selectionEnd = currentInputElement.selectionEnd;
                    setTimeout(function () {
                        let _value = currentInputElement.value;
                        if (_value == '') {
                            currentInputElement.value = DEFAULT_DATE;
                            resetCursor(currentInputElement);
                            return;
                        }
                        replaceSelectionArea(currentInputElement, selectStart, selectionEnd, DEFAULT_DATE);
                        resetCursor(currentInputElement);
                    }, 10);
                    return true;
                }
                if (_value.charAt(cursorIndex - 1) == DATE_SEPARATOR) {
                    let arrVal = _value.split('');
                    arrVal.splice(cursorIndex - 2, 1, PLACEHOLDER);
                    this.value = arrVal.join('');
                    resetCursor(currentInputElement);
                    return false;
                }
                setTimeout(function () {
                    //  值为空时初始化
                    if (currentInputElement.value == '') {
                        currentInputElement.value = DEFAULT_DATE;
                        resetCursor(currentInputElement);
                        return;
                    }
                    //  值不为空时将删除的填充为_
                    let _value = currentInputElement.value;
                    let arrVal = _value.split('');
                    arrVal.splice(cursorIndex - 1, 0, PLACEHOLDER);
                    currentInputElement.value = arrVal.join('');
                    resetCursor(currentInputElement);
                }, 10);
                return true;
            } else if (keyCode == 37) {
                //  左箭头，左移一格
                if (cursorIndex > 0) {
                    setCursor(currentInputElement, this.value.charAt(cursorIndex - 1) == DATE_SEPARATOR ? cursorIndex - 2 : cursorIndex - 1);
                }
            } else if (keyCode == 39) {
                //  右箭头
                if (this.value.length > cursorIndex) {
                    setCursor(currentInputElement, this.value.charAt(cursorIndex + 1) == DATE_SEPARATOR ? cursorIndex + 2 : cursorIndex + 1);
                }
            } else if (keyCode == 38) {
                //  上箭头
                setCursor(currentInputElement, 0);
            } else if (keyCode == 40) {
                //  下箭头
                if (this.value != '') {
                    setCursor(currentInputElement, currentInputElement.value.length);
                }
            }
            return false;
        });
        //  失去焦点事件
        $(item).on('blur', function () {
            if (this.value == DEFAULT_DATE) {
                this.value = '';
            }
        })
        //  获得焦点事件
        $(item).on('focus', function () {
            if (this.value === '') {
                this.value = DEFAULT_DATE;
                setCursor(this, 0);
            }
        })
    });
    datetimeboxInputs.each(function (index, item) {
        $(item).on('keydown', function (e) {
            let currentInputElement = this;
            let keyCode = e.keyCode;
            let cursorIndex = getCursorIndex(currentInputElement);
            if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || keyCode == 229) {
                //  输入数字键
                let reg = /(\d)_/
                setTimeout(function () {
                    let _value = currentInputElement.value;
                    //  判断当前是否为最后一个字符
                    if (cursorIndex >= DEFAULT_TIME.length) {
                        let arr = _value.split('');
                        arr.splice(DEFAULT_TIME.length - 1, 1);
                        currentInputElement.value = arr.join('').substring(0, DEFAULT_TIME.length);
                        setCursor(currentInputElement, DEFAULT_TIME.length);
                        return;
                    }
                    let cursorEnd = _value.substring(cursorIndex);
                    currentInputElement.value = (cursorIndex == 0 ? '' : _value.substr(0, cursorIndex)) + cursorEnd.replace(reg, '$1');
                    resetCursor(currentInputElement, DEFAULT_TIME);
                }, 10)
                return true;
            } else if (keyCode == 8) {
                //  按退格键
                let _value = this.value;
                if (!cursorIndex && !getSelections(currentInputElement).length) return false;
                //  选中清空
                if (getSelections(currentInputElement).length > 1) {
                    let selectStart = currentInputElement.selectionStart;
                    let selectionEnd = currentInputElement.selectionEnd;
                    setTimeout(function () {
                        let _value = currentInputElement.value;
                        if (_value == '') {
                            currentInputElement.value = DEFAULT_TIME;
                            resetCursor(currentInputElement);
                            return;
                        }
                        replaceSelectionArea(currentInputElement, selectStart, selectionEnd, DEFAULT_TIME);
                        resetCursor(currentInputElement, DEFAULT_TIME);
                    }, 10);
                    return true;
                }
                let charCode = _value.charAt(cursorIndex - 1)
                if (charCode == DATE_SEPARATOR || charCode == TIME_SEPARATOR || charCode == ' ') {
                    let arrVal = _value.split('');
                    arrVal.splice(cursorIndex - 2, 1, PLACEHOLDER);
                    this.value = arrVal.join('');
                    resetCursor(currentInputElement, DEFAULT_TIME);
                    return false;
                }
                setTimeout(function () {
                    //  值为空时初始化
                    if (currentInputElement.value == '') {
                        currentInputElement.value = DEFAULT_TIME;
                        resetCursor(currentInputElement, DEFAULT_TIME);
                        return;
                    }
                    //  值不为空时将删除的填充为_
                    let _value = currentInputElement.value;
                    let arrVal = _value.split('');
                    arrVal.splice(cursorIndex - 1, 0, PLACEHOLDER);
                    currentInputElement.value = arrVal.join('');
                    resetCursor(currentInputElement, DEFAULT_TIME);
                }, 10);
                return true;
            } else if (keyCode == 37) {
                //  左箭头，左移一格
                if (cursorIndex > 0) {
                    let charCode = this.value.charAt(cursorIndex - 1);
                    setCursor(currentInputElement, charCode == DATE_SEPARATOR || charCode == TIME_SEPARATOR ? cursorIndex - 2 : cursorIndex - 1);
                }
            } else if (keyCode == 39) {
                //  右箭头
                if (this.value.length > cursorIndex) {
                    let charCode = this.value.charAt(cursorIndex + 1);
                    setCursor(currentInputElement, charCode == DATE_SEPARATOR || charCode == TIME_SEPARATOR ? cursorIndex + 2 : cursorIndex + 1);
                }
            } else if (keyCode == 38) {
                //  上箭头
                setCursor(currentInputElement, 0);
            } else if (keyCode == 40) {
                //  下箭头
                if (this.value != '') {
                    setCursor(currentInputElement, currentInputElement.value.length);
                }
            }
            return false;
        });
        //  失去焦点事件
        $(item).on('blur', function () {
            if (this.value == DEFAULT_TIME) {
                this.value = '';
            }
        })
        //  获得焦点事件
        $(item).on('focus', function () {
            if (this.value === '') {
                this.value = DEFAULT_TIME;
                setCursor(this, 0);
            }
        })
    });

})