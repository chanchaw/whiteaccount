$(function () {
    $('#formSubmit input[name]').each(function (index, item) {
        $(this).on({
            'keyup': function (e) {
                if (e.keyCode == 13) {
                    if (index == 0) {
                        if ($(e.currentTarget).val() != '') {
                            $('input[name]').eq(index + 1).select();
                        }
                    } else {
                        submit();
                    }
                }
            },
            'focus': function (e) {
                $('#msg').fadeOut();
            }
        })
    })
    $('.btn').on('click', function (e) {
        submit();
    })

    $('form label').click(function () {
        if ($('input[type="checkbox"]').is(':checked')) {
            $('input[type="checkbox"]').removeAttr('checked');
        } else {
            $('input[type="checkbox"]').attr('checked', true);
        }
    })

    if (top != self) {
        top.location = self.location;
    }
})

//  提交表单事件
function submit() {
    let formData = {};
    $('#formSubmit').serializeArray().map(item => {
        return formData[item['name']] = item['value'];
    })
    setInputDisabled(true);
    $.ajax({
        url: 'system/login',
        type: 'POST',
        data: formData,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                window.location.reload();
            } else {
                $('#msg').fadeIn();
                $('#msg span').eq(0).html(data.message);
                $('input[name]').eq(0).focus();
            }
        },
        complete: function () {
            setInputDisabled(false);
        }
    })
}

/**
 * 设置输入框状态
 * @param flag
 */
function setInputDisabled(flag) {
    if (flag) {
        $('input').attr('disabled', flag).addClass('disabled-input');
        $('#btn').val('正在登录...')
    } else {
        $('input').attr('disabled', flag).removeClass('disabled-input');
        $('#btn').val('登录')
    }
}
