//  基础资料类别编号
const BASIC_PARENT_MENU_ID = 400;
$(function () {
    initDataList();
    initMenuIcon();
})

/**
 * 初始化
 */
function initMenuIcon() {
    $.ajax({
        url: 'sysUserVisitList/querybyUserId',
        type: 'GET',
        dataType: 'JSON',
        data: {},
        success: function (data) {
            if (data instanceof Array && data.length > 0) {
                let gridList = [];
                for (let item of data) {
                    if (item['parentMenuId'] != BASIC_PARENT_MENU_ID)
                        $('#layout_center').append(getButton(item.url, item.iconClass, item.menuName));
                    else
                        gridList.push(item);
                }
                $('#nav_datalist').datalist('loadData', gridList);
                $.parser.parse($('#layout_center'));
            }
        }
    })
}

/**
 * 初始化列表
 */
function initDataList() {
    $('#nav_datalist').datalist({
        line: true,
        fit: true,
        checkbox: false,
        align: 'center',
        textField: 'menuName',
        valueField: 'url',
        textFormatter: function (v, r, i) {
            return `<span style="cursor:pointer;"> ● ${v}</span>`;
        },
        data: [],
        onSelect: function (index, row) {
            let url = row['url'];
            let menuName = row['menuName'];
            if (!url) {
                $.messager.alert('提示', '出现错误,无法打开标签页!', 'error')
                return;
            }
            window.parent.addTab(menuName, url);
        },
    });
}

/**
 * 获取按钮模板
 * @param url
 * @param icon
 * @param text
 * @returns {string}
 */
function getButton(url, icon, text) {
    return `<div class="pull-left content-item" >
            <a url="${url}" class="easyui-linkbutton" 
               data-options="onClick:onClick,size:'large',plain:true,width:120,height:150,iconAlign:'top',iconCls:'${icon}'">
               <span style="font-size: 15px;">${text}</span>
            </a>
            </div>`;
}

/**
 * 点击事件
 */
function onClick() {
    let text = $(this).text();
    let url = $(this).attr('url');
    if (!url) {
        $.messager.alert('提示', '出现错误,无法打开标签页!', 'error')
        return;
    }
    window.parent.addTab(text, url);
}
