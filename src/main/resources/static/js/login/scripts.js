var timeout, interval;
jQuery(document).ready(function () {
    if (top != self) {
        top.location = self.location;
    }
    getQrCode();
});
function getQrCode() {
    msg(true);
    $.get('system/qrcode', {}, function (data) {
        if (data.success) {
            $('#qrcode').attr('src', data.data['qrCodeUrl']);
            sessionStorage.setItem("token", data.data['token']);
            if (timeout) window.clearTimeout(timeout);
            if (interval) window.clearInterval(interval);
            let intv = data.data['timeout'] || 120;
            $('#timeout').html(intv);
            interval = window.setInterval(() => {
                $('#timeout').html(--intv);
                //  定时器
                $.post('system/login', {qrcode: data.data['token']}, data => {
                    if (data.success) {
                        if (timeout)
                            window.clearTimeout(timeout);
                        window.location.reload();
                    }
                }, 'json');
            }, 1000);
            timeout = window.setTimeout(() => {
                window.clearInterval(interval);
                msg(false);
            }, (data.data['timeout'] || 120) * 1000);
        }
    }, 'json');
}
function msg(f) {
    if (f) {
        $('.qrcode-message').css('display', 'inline');
        $('.btn-qrcode').css('display', 'none');
    } else {
        $('.qrcode-message').css('display', 'none');
        $('.btn-qrcode').css('display', 'inline');
    }
}