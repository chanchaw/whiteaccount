$(function () {
    let buttonGroup = [
        {
            id: 'btn_add',
            size: 'small',
            text: '白坯入库',
            iconCls: 'icon-add',
            plain: true,
            onClick: btn_add
        },
        {
            id: 'btn_material_returining',
            size: 'small',
            text: '白坯退货',
            iconCls: 'icon-add',
            onClick: btn_fabric_returining,
            plain: true
        },
        {
            id: 'btn_export',
            size: 'small',
            text: '导出Excel',
            iconCls: 'icon-page_excel',
            plain: true,
            onClick: btn_export_excel
        },
        {
            id: 'btn_print',
            size: 'small',
            text: '打印',
            iconCls: 'icon-print',
            plain: true,
            onClick: btn_print
        },
        {
            id: 'btn_grid_columns',
            size: 'small',
            text: '列',
            iconCls: 'icon-text_columns',
            plain: true,
            onClick: btn_open_column_list
        },
        {
            id: 'btn_cancel_filter',
            size: 'small',
            text: '清除过滤',
            iconCls: 'icon-clear',
            plain: true,
            onClick: btn_cancel_filter
        },
        {
            id: 'btn_cancel_audit',
            size: 'small',
            disabled: true,
            text: '消审',
            iconCls: 'icon-exclamation',
            onClick: btn_cancel_audit,
            plain: true
        },
        {
            id: 'btn_close', size: 'small', text: '关闭', iconCls: 'icon-no', onClick: btn_close,
            plain: true
        },
    ];
    buttonGen($('#north'), buttonGroup);
    bindEnterEvents();
    let kGrid = $('#v_grid').data('kendoGrid');
    var options = kGrid.getOptions();
    if (options.columns.length > 0)
        initCombobox(options)
})

function setBtnState(b) {
    if (b) {
        $('#btn_add').linkbutton('enable');
    } else {
        $('#btn_add').linkbutton('disable');
    }
}

//  选中事件
function onSelect(title, index) {
    function state(f) {
        if (!f && index == 3) {
            $('#contactCompanyId').combobox('clear');
            $('#contactCompanyId').combobox('disable');
            $('#start').datebox('disable');
            $('#end').datebox('disable');
        } else {
            $('#start').datebox('enable');
            $('#end').datebox('enable');
            $('#contactCompanyId').combobox('enable');
        }
        btn_search();
    }

    switch (index) {
        case 0:
            if ($('#v_grid').data('kendoGrid') == undefined) {
                grid_1();
            }
            break;
        case 1: // 白坯管理 - 第二个标签页 - 白坯入库
            if ($('#v_grid_1').data('kendoGrid') == undefined) {
                grid_2();
            }
            break;
        case 2:// 白坯管理 - 第三个标签页 - 白坯库存
            if ($('#v_grid_2').data('kendoGrid') == undefined) {
                grid_3();
            }
            break;
        case 3:
            if ($('#v_grid_3').data('kendoGrid') == undefined) {
                grid_4();
            }
            break;
        case 4:
            if ($('#v_grid_4').data('kendoGrid') == undefined) {
                grid_5();
            }
            break;
    }
    setTimeout(function () {
        if (index == 1) {
            setBtnState(true);
        } else {
            setBtnState(false);
        }
    }, 0);
    if (index >= 2) {
        $('#state').switchbutton('enable');
    } else {
        $('#state').switchbutton('disable');
    }
    if (index == 0) {
        $('#btn_material_returining').linkbutton('enable');
    } else {
        $('#btn_material_returining').linkbutton('disable');
    }
    state($('#state').switchbutton('options').checked);
    btn_search();
}

/**
 * 2021年8月14日 16:27:40 chanchaw
 * 白坯管理 - 第一个标签页 - 白坯发货
 */
function grid_1() {
    let SUFFIX = "_a";
    let client = new kendo.data.DataSource({data: []});
    let address = new kendo.data.DataSource({data: []});
    let specification = new kendo.data.DataSource({data: []});
    let billcode = new kendo.data.DataSource({data: []});
    let checkout = new kendo.data.DataSource({data: []});

    let planCode = new kendo.data.DataSource({data: []});
    let remarks = new kendo.data.DataSource({data: []});
    let isSendDirectly = new kendo.data.DataSource({data: []});
    let pmgg = new kendo.data.DataSource({data: []});
    let batch_num = new kendo.data.DataSource({data: []});
    let createUserName = new kendo.data.DataSource({data: []});
    let handlerName = new kendo.data.DataSource({data: []});
    let billType = new kendo.data.DataSource({data: []});


    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "delivery/report/fhdtl",
                dataType: "json",
                type: "GET",
                data: {
                    start: $('#start').datebox('getValue'),
                    end: $('#end').datebox('getValue'),
                    clientName: $('#contactCompanyId').combobox('getValue'),
                    checkType: $('#type').combobox('getValue'),
                    // textField: $('#column').combobox('getValue'),
                    // valueField: $('#value').textbox('getValue'),
                }
            }
        },
        schema: {
            model: {
                id: 'id',
                fields: {
                    ps: {type: "number"},
                    qty: {type: "number"},
                    account_price: {type: "number"},
                    money: {type: "number"},
                    createTime: {type: "date"},
                    b_data: {type: "date"},
                }
            }
        },
        aggregate: [
            {field: "ps", aggregate: "sum"},
            {field: "qty", aggregate: "sum"},
            {field: "money", aggregate: "sum"},
        ],
        change: function (e) {
            let data = this.data();
            client.data(unique(data, 'client'));
            address.data(unique(data, 'address'));
            specification.data(unique(data, 'specification'));
            billcode.data(unique(data, 'billcode'));
            checkout.data(unique(data, 'checkout'));

            planCode.data(unique(data, 'planCode'));
            remarks.data(unique(data, 'remarks'));
            isSendDirectly.data(unique(data, 'isSendDirectly'));
            pmgg.data(unique(data, 'pmgg'));
            batch_num.data(unique(data, 'batch_num'));
            createUserName.data(unique(data, 'createUserName'));
            handlerName.data(unique(data, 'handlerName'));
            billType.data(unique(data, 'billType'));


            $('#btn_cancel_audit').linkbutton('disable')
            //获取过滤条件
            var filtrer = this.filter();
            //获取所有数据
            var allData = this.data();
            //进行过滤条件的筛选
            var query = new kendo.data.Query(allData);
            //获取过过滤后的数据
            var dat = query.filter(filtrer).data;
            // grid_1(query.filter().data);
        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })
        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })
        }
    });
    let columns = [
        {
            field: "no",
            title: "#",
            filterable: false,
            template: "<span class='row_number'></span>",
            width: 40
        },
        {
            field: "id",
            title: '编号',
            width: 93,
            hidden: true,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
        },
        {
            field: "client",
            title: '客户',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: client}
        },
        {
            field: "planId",
            title: '计划主键',
            width: 93,
            hidden: true,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "planCode",
            title: '计划单号',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: planCode}
        },
        {
            field: "address",
            title: '收货单位',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: address}
        },
        {
            field: "remarks",
            title: '备注',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: remarks}
        },
        {
            field: "isSendDirectly",
            title: '直发',
            template: item => item['isSendDirectly'] == '是' ? `<input type="checkbox" checked onclick="return false;">` : '',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: isSendDirectly}
        },
        {
            field: "pmgg",
            title: '品名规格',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: pmgg}
        },
        // {
        //     field: "meterLength",
        //     title: '米长',
        //     width: 93,
        //     attributes: {'class': 'kendo-custum-char'},
        //     headerAttributes: {'style': 'text-align:center;'},
        //     footerAttributes: {'style': 'color: red;text-align:center;'},
        //     filterable: false
        // },
        {
            field: "batch_num",
            title: '批次号',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: batch_num}
        },
        {
            field: "ps",
            title: '匹数',
            width: 93,
            template: item => item['ps'] ? parseFloat(item['ps']).toFixed(0) : '',
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "qty",
            title: '重量',
            width: 93,
            template: item => item['qty'] ? parseFloat(item['qty']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "price",
            title: '单价',
            width: 93,
            template: item => item['price'] ? parseFloat(item['price']).toFixed(2) : '',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "account_price",
            title: '对账单价',
            width: 93,
            template: item => item['account_price'] ? parseFloat(item['account_price']).toFixed(2) : '',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "money",
            title: '金额',
            width: 93,
            template: item => item['money'] ? parseFloat(item['money']).toFixed(2) : '',
            footerAttributes: {'style': 'text-align:right;'},
            footerTemplate: '#=kendo.format("{0:n2}",sum)#',
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "checkout",
            title: '结算',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: checkout}
        },
        {
            field: "billcode",
            title: '发货单号',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: billcode}
        },
        {
            field: "createUserName",
            title: '创建人',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: createUserName}
        },
        {
            field: "handlerName",
            title: '经手人',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: handlerName}
        },
        {
            field: "billType",
            title: '单据类型',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: billType}
        },
        {
            field: "b_data",
            title: '发货日期',
            width: 85,
            format: "{0: MM-dd}",
            headerAttributes: {'style': 'text-align:center;'},
            attributes: {'class': 'kendo-custum-char'},
            filterable: false
        },
        {
            field: "createTime",
            title: '制单时间',
            format: "{0: MM-dd HH:mm}",
            width: 85,
            headerAttributes: {'style': 'text-align:center;'},
            attributes: {'class': 'kendo-custum-char'},
            filterable: false
        },
    ];
    let grid = $("#v_grid").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "白坯发货.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            let kGrid = $('#v_grid').data('kendoGrid');
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
                let dataItem = kGrid.dataItem($(this));
                if (dataItem['is_audit'] != undefined && !dataItem['is_audit']) {
                    $(this).addClass('kendo-custum-non-checked');
                }
            });
        },
        columnHide: function (e) {
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
        },
        columnShow: function (e) {
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null, SUFFIX);
        },
        columnResize: function (e) {
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null, SUFFIX);
            }
        },
        columnReorder: function (e) {
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field'] + SUFFIX
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid.on('dblclick', 'tr', function (e) {
        let dataItem = $('#v_grid').data('kendoGrid').dataItem(e.currentTarget);
        if (dataItem != undefined && dataItem.id) {
            let id = dataItem.id;
            $.get('productOutwardOrder/queryByPk/' + id, {}, function (data) {
                if (data != undefined) {
                    if (data.storageId) {
                        btn_fabric_returining_modify(data);
                    } else {
                        btn_modify(data);
                    }
                }
            }, 'json');
        }
    });
    grid.on('click', 'tr', function (e) {
        let dataItem = $('#v_grid').data('kendoGrid').dataItem(e.currentTarget);
        if (dataItem['is_audit']) {
            $('#btn_cancel_audit').linkbutton('enable')
        } else {
            $('#btn_cancel_audit').linkbutton('disable')
        }
    })
    grid01.setOptions({
        columns: columns
    });
}

/**
 * 2021年8月14日 16:27:40 chanchaw
 * 白坯管理 - 第二个标签页 - 白坯入库
 */
function grid_2() {
    let SUFFIX = "_b";
    let client = new kendo.data.DataSource({data: []});
    let address = new kendo.data.DataSource({data: []});
    let machineType = new kendo.data.DataSource({data: []});
    let batchNum = new kendo.data.DataSource({data: []});
    let planCode = new kendo.data.DataSource({data: []});
    let computerName = new kendo.data.DataSource({data: []});
    let isSendDirectly = new kendo.data.DataSource({data: []});
    let specification = new kendo.data.DataSource({data: []});
    let className = new kendo.data.DataSource({data: []});
    let aorb = new kendo.data.DataSource({data: []});

    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "delivery/report/greyfabric/input",
                dataType: "json",
                type: "GET",
                data: {
                    start: $('#start').datebox('getValue'),
                    end: $('#end').datebox('getValue'),
                    clientName: $('#contactCompanyId').combobox('getValue'),
                    // textField: $('#column').combobox('getValue'),
                    // valueField: $('#value').textbox('getValue'),
                }
            }
        },
        schema: {
            model: {
                // id: 'id',
                fields: {
                    createdate: {type: "date"},
                    ps: {type: "number"},
                    qty: {type: "number"},
                }
            }
        },
        aggregate: [
            {field: "ps", aggregate: "sum"},
            {field: "qty", aggregate: "sum"},
        ],
        change: function (e) {
            let data = this.data();
            client.data(unique(data, 'client'));
            address.data(unique(data, 'address'));
            machineType.data(unique(data, 'machineType'));

            batchNum.data(unique(data, 'batchNum'));
            planCode.data(unique(data, 'planCode'));
            computerName.data(unique(data, 'computerName'));
            isSendDirectly.data(unique(data, 'isSendDirectly'));
            specification.data(unique(data, 'specification'));
            className.data(unique(data, 'className'));
            aorb.data(unique(data, 'aorb'));

            $('#btn_cancel_audit').linkbutton('disable')
            //获取过滤条件
            var filtrer = this.filter();
            //获取所有数据
            var allData = this.data();
            //进行过滤条件的筛选
            var query = new kendo.data.Query(allData);
            //获取过过滤后的数据
            var dat = query.filter(filtrer).data;
            // grid_1(query.filter().data);
        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })

        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })

        }
    });
    let columns = [
        {
            field: "no",
            title: "#",
            filterable: false,
            template: "<span class='row_number'></span>", width: 40},
        {
            field: "id",
            title: '编号',
            width: 93,
            hidden: true,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
        },
        {
            field: "client",
            title: '客户',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: {multi: true, search: true, dataSource: client}
        },
        // {
        //     field: "client",
        //     title: '客户',
        //     width: 93,
        //     attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
        //     headerAttributes: {'style': 'text-align:center;'},
        //     footerAttributes: {'style': 'color: red;text-align:center;'},
        //     filterable: false
        // },
        {
            field: "batchNum",
            title: '批次号',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: batchNum}
        },

        {
            field: "planCode",
            title: '计划单号',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: planCode}
        },
        // {
        //     field: "meterLength",
        //     title: '米长',
        //     width: 93,
        //     attributes: {'class': 'kendo-custum-char'},
        //     headerAttributes: {'style': 'text-align:center;'},
        //     footerAttributes: {'style': 'color: red;text-align:center;'},
        //     filterable: false
        // },
        {
            field: "computerName",
            title: '机号',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: computerName}
        },
        {
            field: "isSendDirectly",
            title: '直发',
            template: item => item['isSendDirectly'] == '是' ? `<input type="checkbox" checked onclick="return false;">` : '',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: isSendDirectly}
        },
        {
            field: "machineType",
            title: '机台号',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: machineType}
        },
        {
            field: "specification",
            title: '规格',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: specification}
        },
        {
            field: "ps",
            title: '匹数',
            width: 93,
            template: item => item['ps'] ? parseFloat(item['ps']).toFixed(0) : '',
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
            // filterable: {multi: true, search: true, dataSource: ps}
        },
        {
            field: "qty",
            title: '重量',
            width: 93,
            template: item => item['qty'] ? parseFloat(item['qty']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "createdate",
            title: '入库时间',
            width: 85,
            format: "{0: MM-dd HH:mm}",
            headerAttributes: {'style': 'text-align:center;'},
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            filterable: false
        },
        {
            field: "className",
            title: '班次',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: className}
        },
        {
            field: "aorb",
            title: 'AB面',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: aorb}
        },
    ];
    let grid = $("#v_grid_1").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "原料序时表.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            let kGrid = $('#v_grid_1').data('kendoGrid');
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
                let dataItem = kGrid.dataItem($(this));
                if (dataItem['is_audit'] != undefined && !dataItem['is_audit']) {
                    $(this).addClass('kendo-custum-non-checked');
                }
            });
        },
        columnHide: function (e) {
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
        },
        columnShow: function (e) {
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null, SUFFIX);
        },
        columnResize: function (e) {
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null, SUFFIX);
            }
        },
        columnReorder: function (e) {
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field'] + SUFFIX
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid_1').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid01.setOptions({
        columns: columns
    });
    // 白坯管理 - 白坯入库 - 表格双击事件
    grid.on('dblclick', 'tr', function (e) {
        let dataItem = $('#v_grid_1').data('kendoGrid').dataItem(e.currentTarget);
        if (dataItem != undefined) {
            let id = dataItem.id;
            if (id == undefined) return;
            $.get('productOutwardOrder/queryByPk/' + id, {}, function (data) {
                if (data != undefined) {
                    btn_modify(data);
                }
            }, 'json');
        }
    });
    grid.on('click', 'tr', function (e) {
        let dataItem = $('#v_grid_1').data('kendoGrid').dataItem(e.currentTarget);
        if (dataItem['is_audit']) {
            $('#btn_cancel_audit').linkbutton('enable')
        } else {
            $('#btn_cancel_audit').linkbutton('disable')
        }
    })
}

/**
 * 2021年8月18日 17:22:19chanchaw
 * 白坯管理 - 第三个标签页 - 白坯库存
 */
function grid_3() {
    let SUFFIX = "_c";
    let client = new kendo.data.DataSource({data: []});
    let batchNum = new kendo.data.DataSource({data: []});
    let planCode = new kendo.data.DataSource({data: []});
    let specification = new kendo.data.DataSource({data: []});
    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "delivery/report/greyfabric/storage",
                dataType: "json",
                type: "GET",
                data: {
                    start: $('#start').datebox('getValue'),
                    end: $('#end').datebox('getValue'),
                    clientName: $('#contactCompanyId').combobox('getValue') || '',
                    value: $('#state').switchbutton('options').checked ? 0 : 1
                    // textField: $('#column').combobox('getValue'),
                    // valueField: $('#value').textbox('getValue'),
                }
            }
        },
        schema: {
            model: {
                // id: 'id',
                fields: {
                    ps: {type: "number"},
                    qty: {type: "number"},
                    money: {type: "number"},
                }
            }
        },
        aggregate: [
            {field: "ps", aggregate: "sum"},
            {field: "qty", aggregate: "sum"},
            {field: "money", aggregate: "sum"},
        ],
        change: function (e) {
            let data = this.data();
            client.data(unique(data, 'client'));
            batchNum.data(unique(data, 'batchNum'));
            planCode.data(unique(data, 'planCode'));
            specification.data(unique(data, 'specification'));

            $('#btn_cancel_audit').linkbutton('disable')
            //获取过滤条件
            var filtrer = this.filter();
            //获取所有数据
            var allData = this.data();
            //进行过滤条件的筛选
            var query = new kendo.data.Query(allData);
            //获取过过滤后的数据
            var dat = query.filter(filtrer).data;
            // grid_1(query.filter().data);
        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })

        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })

        }
    });
    let columns = [
        {field: "no", title: "#", filterable: false, template: "<span class='row_number'></span>", width: 40},
        {
            field: "id",
            title: '编号',
            width: 93,
            hidden: true,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
        },
        {
            field: "client",
            title: '客户',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: client}
        },
        {
            field: "batchNum",
            title: '批次号',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: batchNum}
        },
        // {
        //     field: "meterLength",
        //     title: '米长',
        //     width: 93,
        //     attributes: {'class': 'kendo-custum-char'},
        //     headerAttributes: {'style': 'text-align:center;'},
        //     footerAttributes: {'style': 'color: red;text-align:center;'},
        //     filterable: false
        // },
        {
            field: "planCode",
            title: '计划单号',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: planCode}
        },
        /*{
            field: "productName",
            title: '品名',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },*/
        {
            field: "specification",
            title: '规格',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:left;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            // filterable: false
            filterable: {multi: true, search: true, dataSource: specification}
        },
        {
            field: "ps",
            title: '匹数',
            width: 93,
            template: item => item['ps'] ? parseFloat(item['ps']).toFixed(0) : '',
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {'style': 'text-align:right;text-align:center;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "qty",
            title: '重量',
            width: 93,
            template: item => item['qty'] ? parseFloat(item['qty']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        /*{
            field: "price",
            title: '单价',
            width: 93,
            template: item => item['price'] ? parseFloat(item['price']).toFixed(2) : '',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "money",
            title: '金额',
            width: 93,
            template: item => item['money'] ? parseFloat(item['money']).toFixed(2) : '',
            footerAttributes: {'style': 'text-align:right;'},
            footerTemplate: '#=kendo.format("{0:n2}",sum)#',
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        }*/
    ];
    let grid = $("#v_grid_2").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "白坯库存.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
            });
        },
        columnHide: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
        },
        columnShow: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null, SUFFIX);
        },
        columnResize: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null, SUFFIX);
            }
        },
        columnReorder: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field'] + SUFFIX
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid_2').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid01.setOptions({
        columns: columns
    });
}

/**
 * 2021年8月14日 16:27:40 chanchaw
 * 白坯管理 - 第四个标签页 - 未剖幅入库
 */
function grid_4() {
    let SUFFIX = "_d";
    let client = new kendo.data.DataSource({data: []});
    let process_company = new kendo.data.DataSource({data: []});
    let batchNum = new kendo.data.DataSource({data: []});
    let order_code = new kendo.data.DataSource({data: []});
    let plan_code = new kendo.data.DataSource({data: []});
    let material_specification = new kendo.data.DataSource({data: []});
    let greyfabric_specification = new kendo.data.DataSource({data: []});
    let consignee = new kendo.data.DataSource({data: []});
    let is_send_directly = new kendo.data.DataSource({data: []});
    let remarks = new kendo.data.DataSource({data: []});

    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "delivery/report/unfinishedOutward",
                dataType: "json",
                type: "GET",
                data: {
                    start: $('#start').datebox('getValue'),
                    end: $('#end').datebox('getValue'),
                    clientName: $('#contactCompanyId').combobox('getValue') || ''
                    // textField: $('#column').combobox('getValue'),
                    // valueField: $('#value').textbox('getValue'),
                }
            }
        },
        schema: {
            model: {
                id: 'id',
                fields: {
                    box_amount: {type: "number"},
                    input_kilo: {type: "number"},
                }
            }
        },
        aggregate: [
            {field: "box_amount", aggregate: "sum"},
            {field: "input_kilo", aggregate: "sum"},
        ],
        change: function (e) {
            let data = this.data();
            client.data(unique(data, 'client'));
            process_company.data(unique(data, 'process_company'));
            batchNum.data(unique(data, 'batchNum'));
            order_code.data(unique(data, 'order_code'));
            plan_code.data(unique(data, 'plan_code'));
            material_specification.data(unique(data, 'material_specification'));
            greyfabric_specification.data(unique(data, 'greyfabric_specification'));
            consignee.data(unique(data, 'consignee'));
            is_send_directly.data(unique(data, 'is_send_directly'));
            remarks.data(unique(data, 'remarks'));
        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })
        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })
        }
    });
    let columns = [
        {field: "no", title: "#", filterable: false, template: "<span class='row_number'></span>", width: 40},
        {
            field: "id",
            title: '编号',
            width: 93,
            hidden: true,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
        },
        {
            field: "client",
            title: '客户',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: client}
        }
        ,
        {
            field: "process_company",
            title: '加工户',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: process_company}
        },
        {
            field: "batchNum",
            title: '批次号',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: batchNum}
        },
        // {
        //     field: "meterLength",
        //     title: '米长',
        //     width: 93,
        //     attributes: {'class': 'kendo-custum-char'},
        //     headerAttributes: {'style': 'text-align:center;'},
        //     footerAttributes: {'style': 'color: red;text-align:center;'},
        //     filterable: false
        // },
        {
            field: "order_code",
            title: '单据编号',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: order_code}
        },
        {
            field: "plan_code",
            title: '计划单号',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: plan_code}
        },
        {
            field: "material_specification",
            title: '原料规格',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: material_specification}
        },
        {
            field: "greyfabric_specification",
            title: '白坯规格',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: greyfabric_specification}
        },
        {
            field: "box_amount",
            title: '件数',
            width: 93,
            template: item => item['box_amount'] ? parseFloat(item['box_amount']).toFixed(0) : '',
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "input_kilo",
            title: '公斤',
            width: 93,
            template: item => item['input_kilo'] ? parseFloat(item['input_kilo']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "consignee",
            title: '收货单位',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: consignee}
        },
        {
            field: "handler_name",
            title: '经手人',
            width: 93,
            attributes: {'style': 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "create_user_name",
            title: '创建人',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "is_send_directly",
            title: '是否直发',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: is_send_directly}
        },
        {
            field: "remarks",
            title: '备注',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: remarks}
        },
    ];
    let grid = $("#v_grid_3").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "未剖幅入库.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
            });
        },
        columnHide: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
        },
        columnShow: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null, SUFFIX);
        },
        columnResize: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null, SUFFIX);
            }
        },
        columnReorder: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field'] + SUFFIX
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid_3').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid01.setOptions({
        columns: columns
    });
    grid.on('dblclick', 'tr', function (e) {
        let dataItem = $('#v_grid_3').data('kendoGrid').dataItem(e.currentTarget);
        if (dataItem != undefined) {
            let id = dataItem.id;
            $.get('productOutwardOrder/queryByPk/' + id, {}, function (data) {
                if (data != undefined) {
                    btn_modify(data);
                }
            }, 'json');
        }
    })
}

/**
 * 2021年8月14日 16:27:40 chanchaw
 * 白坯管理 - 第五个标签页 - 废布入库
 */
function grid_5() {
    let SUFFIX = "_e";
    let plan_code = new kendo.data.DataSource({data: []});
    let linked_order_code = new kendo.data.DataSource({data: []});
    let client_name = new kendo.data.DataSource({data: []});
    let material_specification = new kendo.data.DataSource({data: []});
    let order_code = new kendo.data.DataSource({data: []});
    let consignee = new kendo.data.DataSource({data: []});
    let remarks = new kendo.data.DataSource({data: []});
    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "delivery/report/waste",
                dataType: "json",
                type: "GET",
                data: {
                    start: $('#start').datebox('getValue'),
                    end: $('#end').datebox('getValue'),
                    clientName: $('#contactCompanyId').combobox('getValue') || ''
                    // textField: $('#column').combobox('getValue'),
                    // valueField: $('#value').textbox('getValue'),
                }
            }
        },
        schema: {
            model: {
                id: 'id',
                fields: {
                    input_pairs: {type: "number"},
                    input_kilo: {type: "number"},
                    create_time: {type: "date"},
                }
            }
        },
        aggregate: [
            {field: "input_pairs", aggregate: "sum"},
            {field: "input_kilo", aggregate: "sum"},
        ],
        change: function (e) {
            let data = this.data();
            plan_code.data(unique(data, 'plan_code'));
            linked_order_code.data(unique(data, 'linked_order_code'));
            client_name.data(unique(data, 'client_name'));
            material_specification.data(unique(data, 'material_specification'));
            order_code.data(unique(data, 'order_code'));
            consignee.data(unique(data, 'consignee'));
            remarks.data(unique(data, 'remarks'));
            
        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })
        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })
        }
    });
    let columns = [
        {field: "no", title: "#", filterable: false, template: "<span class='row_number'></span>", width: 40},
        {
            field: "id",
            title: '编号',
            width: 93,
            hidden: true,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
        },
        {
            field: "plan_code",
            title: '计划单号',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
           //filterable: false
            filterable: {multi: true, search: true, dataSource: plan_code}
        },
        {
            field: "linked_order_code",
            title: '源发货单号',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: linked_order_code}
        },
        // {
        //     field: "meterLength",
        //     title: '米长',
        //     width: 93,
        //     attributes: {'class': 'kendo-custum-char'},
        //     headerAttributes: {'style': 'text-align:center;'},
        //     footerAttributes: {'style': 'color: red;text-align:center;'},
        //     filterable: false
        // },
        {
            field: "client_name",
            title: '客户',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: client_name}
        },
        {
            field: "material_specification",
            title: '规格',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: material_specification}
        },
        {
            field: "order_code",
            title: '单据编号',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: order_code}
        },
        {
            field: "input_pairs",
            title: '公斤',
            width: 93,
            template: item => item['input_pairs'] ? parseFloat(item['input_pairs']).toFixed(0) : '',
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "input_kilo",
            title: '重量',
            width: 93,
            template: item => item['input_kilo'] ? parseFloat(item['input_kilo']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "consignee",
            title: '退货单位',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: consignee}
        },
        {
            field: "handler_name",
            title: '经手人',
            width: 93,
            attributes: {'style': 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "create_user_name",
            title: '创建人',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "create_time",
            title: '入库时间',
            width: 93,
            format: "{0: MM-dd HH:mm}",
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "remarks",
            title: '备注',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: false
            filterable: {multi: true, search: true, dataSource: remarks}
        },
    ];
    let grid = $("#v_grid_4").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "废布入库.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            let kGrid = $('#v_grid_4').data('kendoGrid');
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
                let dataItem = kGrid.dataItem($(this));
                if (dataItem['is_audit'] != undefined && !dataItem['is_audit']) {
                    $(this).addClass('kendo-custum-non-checked');
                }
            });
        },
        columnHide: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
        },
        columnShow: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null, SUFFIX);
        },
        columnResize: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null, SUFFIX);
            }
        },
        columnReorder: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field'] + SUFFIX
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid_4').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid01.setOptions({
        columns: columns
    });
    /*grid.on('dblclick', 'tr', function (e) {
        let dataItem = $('#v_grid_4').data('kendoGrid').dataItem(e.currentTarget);
        if (dataItem != undefined) {
            let id = dataItem.id;
            $.get('productOutwardOrder/queryByPk/' + id, {}, function (data) {
                if (data != undefined) {
                    btn_modify(data);
                }
            }, 'json');
        }
    });*/
}

function btn_cancel_filter() {
    switch (getCurrentIndex()) {
        case 0:
            $('#v_grid').data('kendoGrid').dataSource.filter({});
            break;
        case 1:
            $('#v_grid_1').data('kendoGrid').dataSource.filter({});
            break;
        case 2:
            $('#v_grid_2').data('kendoGrid').dataSource.filter({});
            break;
        case 3:
            $('#v_grid_3').data('kendoGrid').dataSource.filter({});
            break;
        case 4:
            $('#v_grid_4').data('kendoGrid').dataSource.filter({});
            break;
    }
    $('#v_grid').data('kendoGrid').dataSource.filter({});
}

//  查询
function btn_search() {
    function setCol(ele, show = []) {
        let kGrid = ele.data('kendoGrid');
        let columns = kGrid.columns;
        let checked = $('#state').switchbutton('options').checked;
        if (!checked)
            for (let col of columns) {
                if (col['field'] && show.indexOf(col['field']) == -1) {
                    kGrid.hideColumn(col);
                } else if (col['field']) {
                    kGrid.showColumn(col);
                }
            }
        else {
            for (let col of columns) {
                if (col['title'] && col['title'].indexOf('编号') > -1)
                    kGrid.hideColumn(col);
                else
                    kGrid.showColumn(col);
            }
        }
    }

    switch (getCurrentIndex()) {
        case 0:
            $('#v_grid').data('kendoGrid').dataSource.read({
                start: $('#start').datebox('getValue'),
                end: $('#end').datebox('getValue'),
                clientName: $('#contactCompanyId').combobox('getValue'),
                checkType: $('#type').combobox('getValue'),
                // textField: $('#column').combobox('getValue'),
                // valueField: $('#value').textbox('getValue'),
            });
            break;
        case 1:
            $('#v_grid_1').data('kendoGrid').dataSource.read({
                start: $('#start').datebox('getValue'),
                end: $('#end').datebox('getValue'),
                clientName: $('#contactCompanyId').combobox('getValue'),
                // textField: $('#column').combobox('getValue'),
                // valueField: $('#value').textbox('getValue'),
            });
            break;
        case 2:
            $('#v_grid_2').data('kendoGrid').dataSource.read({
                start: $('#start').datebox('getValue'),
                end: $('#end').datebox('getValue'),
                clientName: $('#contactCompanyId').combobox('getValue'),
                value: $('#state').switchbutton('options').checked ? 0 : 1
                // textField: $('#column').combobox('getValue'),
                // valueField: $('#value').textbox('getValue'),
            });
            setCol($('#v_grid_2'), ['no', 'client', 'money', 'ps', 'qty']);
            break;
        case 3:
            $('#v_grid_3').data('kendoGrid').dataSource.read({
                start: $('#start').datebox('getValue'),
                end: $('#end').datebox('getValue'),
                clientName: $('#contactCompanyId').combobox('getValue'),
                checkType: $('#type').combobox('getValue'),
                value: $('#state').switchbutton('options').checked ? 0 : 1
                // textField: $('#column').combobox('getValue'),
                // valueField: $('#value').textbox('getValue'),
            });
            setCol($('#v_grid_3'), ['no', 'client', 'money']);
            break;
        case 4:
            $('#v_grid_4').data('kendoGrid').dataSource.read({
                start: $('#start').datebox('getValue'),
                end: $('#end').datebox('getValue'),
                clientName: $('#contactCompanyId').combobox('getValue'),
                // textField: $('#column').combobox('getValue'),
                // valueField: $('#value').textbox('getValue'),
            });
            setCol($('#v_grid_4'), ['no', 'client', 'money']);
            break;
    }
}

// 2021年8月14日 16:06:56 chanchaw
// 工具栏按钮 - 白坯入库
function btn_add() {
    function load(ele, data) {
        $(ele).form('load', data);
    }

    let loadData = {
        orderDate: getCurrentDate(),
        // planCode: dataItem['plan_code'],
        // materialSpecification: dataItem['material_specification'],
        // processingCost: dataItem['processing_cost'] || 0,
        // supplierName: dataItem['material_supplier_name'],
    }
    let columnA = [
        {
            title: '主键',
            field: 'id',
            hidden: true,
        },
        {
            title: '单据日期',
            field: 'orderDate',
            editor: {type: 'datebox', options: {required: true, value: getCurrentDate()}}
        },
        {
            title: '单据编号',
            field: 'orderCode',
            editor: {
                type: 'textbox',
                options: {
                    readonly: true
                }
            }
        },
        {
            title: '加工户', field: 'processCompany', id: 'processCompanyInput', editor: {
                type: 'combobox', options: {
                    required: true,
                    url: 'processCompany/listQuery',
                    method: 'GET',
                    textField: 'companyName',
                    valueField: 'companyName',
                    onLoadSuccess: function () {
                    }
                }
            }
        },
        {
            title: '加工类型',
            field: 'isFinishedProduct',
            editor: {
                type: 'combobox',
                options: {
                    data: [{id: 0, text: '未剖幅'}, {id: 1, text: '已剖幅'}],
                    limitToList: true,
                    value: 1,
                    required: true,
                    textField: 'text',
                    valueField: 'id',
                    panelHeight: 'auto',
                    onChange: function (newVal, oldVal) {
                        if (newVal) {
                            $('#boxAmountField').numberbox('clear');
                            $('#boxAmountField').numberbox({required: false, readonly: true});
                            $('#inputPairsField').numberbox({readonly: false, required: true});
                            $('#abSurfaceId').combobox('readonly', false);
                            $('#fieldIsSendDirectly').checkbox('enable');
                        } else {
                            $('#abSurfaceId').combobox('clear');
                            $('#abSurfaceId').combobox('readonly', true);
                            $('#consigneeId').combobox('clear');
                            $('#fieldIsSendDirectly').checkbox('disable');
                            $('#inputPairsField').numberbox('clear');
                            $('#inputPairsField').numberbox({required: false, readonly: true});
                            $('#boxAmountField').numberbox({readonly: false, required: true});
                        }
                    }
                }
            }
        },
        {
            title: '计划单号',
            field: 'planCode',
            id: 'planCodeInput',
            editor: {
                type: 'textbox',
                options: {
                    icons: [{
                        iconCls: 'icon-remove', handler: function (e) {
                            $(e.data.target).textbox('clear');
                        }
                    }, {
                        iconCls: 'icon-add', handler: function (e) {
                            let div = $(`<div>
                                        <div id="t_dialog" 
                                             data-options="title:'白坯计划',width:900,height:500,resizable:true,
                                                           modal:true,iconCls:'icon-search',
                                                           onClose:function(){$(this).dialog('destroy')}" 
                                             class="easyui-dialog">
                                            <table id="t_plancode"></table>
                                        </div>
                                    </div>`);
                            $.parser.parse(div);
                            $('#t_plancode').datagrid({
                                url: 'plan/queryByExample',
                                queryParams: {state: 1, markFinished: false},
                                method: 'GET',
                                striped: true,
                                fit: true,
                                sortable: true,
                                columns: [[
                                    {
                                        title: '计划日期',
                                        hidden: true,
                                        field: 'planDate',
                                        width: 120,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '计划单号', field: 'planCode', width: 100, align: 'center', halign: 'center'},
                                    {title: '订单号', field: 'orderCode', width: 120, align: 'center', halign: 'center'},
                                    {title: '毛高', field: 'mHeight', width: 80, align: 'center', halign: 'center'},
                                    {title: '米长', field: 'meterLength', width: 50, align: 'center', halign: 'center'},
                                    {
                                        title: '门幅',
                                        field: 'greyfabricWidth',
                                        width: 70,
                                        align: 'center',
                                        halign: 'center'
                                    },

                                    {title: '克重', field: 'greyfabricWeight', width: 70, align: 'center', halign: 'center'},
                                    // {title: '克重', field: 'lowWeight', width: 60, align: 'center', halign: 'center'},
                                    {
                                        title: '头份',
                                        field: 'greyfabricSpecification',
                                        width: 80,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '品名', field: 'productName', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '原料规格',
                                        field: 'materialSpecification',
                                        width: 100,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                ]],
                                rownumbers: true,
                                remoteSort: false,
                                showFooter: true,
                                singleSelect: true,
                                onBeforeLoad: function () {
                                },
                                onLoadSuccess: function () {
                                },
                                onDblClickRow: function (index, row) {
                                    delete row['createUserId'];
                                    let specification = [
                                        row['orderCode'],
                                        row['productName'],
                                        row['greyfabricWidth'] + '*' + row['greyfabricWeight'],
                                        row['mHeight'],
                                        row['greyfabricSpecification'],
                                        row['materialSpecification'],
                                    ];
                                    row['greyfabricSpecification'] = specification.join(' ');
                                    row['greyfabricSpecification'] = specification.join(' ');
                                    load($('.form-dialog-edit form').eq(0), row);
                                    $('#batchNumInput').combobox('options').queryParams = {planCode: row['planCode']};
                                    $('#batchNumInput').combobox('reload');
                                    $.ajax({
                                        url: 'contactCompany/queryByPk/' + row['contactCompanyId'],
                                        type: 'GET',
                                        dataType: 'json',
                                        async: false,
                                        success: function (data) {
                                            if (data != undefined)
                                                $('#supplierNameInput').textbox('setValue', data['companyName'] || '')
                                        }
                                    })
                                    $('#t_dialog').dialog('close');
                                }
                            });
                        }
                    }],
                    required: true,
                }
            }
        },
        {
            title: '客户', id: 'supplierNameInput', field: 'clientName', editor: {
                type: 'textbox', options: {}
            }
        },
        {
            title: '批次号',
            field: 'batchNum',
            id: 'batchNumInput',
            editor: {
                type: 'combobox',
                options: {
                    url: 'planBatchnum/listQuery/planCode',
                    method: 'GET',
                    panelHeight: 300,
                    textField: 'batchNum',
                    valueField: 'batchNum',
                    onLoadSuccess: function (data) {
                        let fieldValue = $(this).combobox('options').valueField;
                        if (data instanceof Array && data.length > 0 && !$(this).combobox('getValue')) {
                            $(this).combobox('setValue', data[0][fieldValue]);
                        }
                    }
                }
            }
        },
        /*{
            title: '原料规格',
            field: 'materialSpecification',
            editor: {
                type: 'textbox',
                options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    cls: 'input-blue',
                    panelHeight: 200,
                    required: true,
                }
            }
        },*/
        {
            title: '件数',
            field: 'boxAmount',
            id: 'boxAmountField',
            editor: {type: 'numberbox', options: {precision: 0, required: false, readonly: true}}
        },
        {
            title: '坯布规格',
            field: 'greyfabricSpecification',
            editor: {
                type: 'textbox',
                options: {
                    cls: 'input-blue',
                    width: 514,
                    required: true,
                }
            }
        },
        {
            title: '匹数',
            field: 'inputPairs',
            id: 'inputPairsField',
            editor: {type: 'numberbox', options: {precision: 0, required: true}}
        },
        {
            title: '公斤',
            field: 'inputKilo',
            id: 'inputKiloId',
            editor: {type: 'numberbox', options: {precision: 1, required: true}}
        },
        {
            title: '单价',
            field: 'processingCost',
            id: 'processingCostId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: '金额',
            field: 'money',
            id: 'moneyId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: 'A/B',
            field: 'abSurface',
            id: 'abSurfaceId',
            editor: {
                type: 'combobox', options: {
                    buttonIcon: 'icon-add',
                    onClickButton: function () {
                        let _value = $('#abSurfaceId').combobox('getValue');
                        if (_value) {
                            switch (_value) {
                                case 'A':
                                    $('#abSurfaceId').combobox('setValue', 'B');
                                    break;
                                case 'B':
                                    $('#abSurfaceId').combobox('clear');
                                    break;
                            }
                        } else {
                            $('#abSurfaceId').combobox('setValue', 'A');
                        }
                    },
                    data: [{id: 'A'}, {id: 'B'}],
                    panelHeight: 'auto',
                    textField: 'id',
                    valueField: 'id',
                    required: false,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '收货单位',
            field: 'consignee',
            id: 'consigneeId',
            editor: {
                type: 'combobox', options: {
                    width: 120, cls: 'input-red', readonly: true,
                    url: 'takeDeliveryAddress/listQuery',
                    method: 'GET',
                    textField: 'company',
                    valueField: 'company',
                    panelWidth: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '直发',
            width: '40px',
            field: 'isSendDirectly',
            id: 'fieldIsSendDirectly',
            editor: {
                type: 'checkbox', options: {
                    cls: 'input-red', value: true, onChange: function (checked) {
                        if (checked) {
                            let _value = $('#consigneeId').combobox('getValue');
                            $('#consigneeId').combobox({readonly: false, required: true});
                            $('#consigneeId').combobox('setValue', _value);
                        } else {
                            $('#consigneeId').combobox('clear');
                            $('#consigneeId').combobox({readonly: true, required: false});
                        }
                    }
                }
            }
        },
        {
            title: '经手人', field: 'handlerName', editor: {
                type: 'combobox', options: {
                    required: false,
                    method: 'GET',
                    url: 'employee/listQuery',
                    queryParams: {state: 1},
                    textField: 'empName',
                    valueField: 'empName'
                }
            }
        },
        {
            title: '制单人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    cls: 'input-blue',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {width: 514,}}},
    ];
    $.formDialog.open({
        mode: '新增',
        columns: columnA,
        loadData: loadData,
        width: 656,
        height: 460,
        onOpen: function () {
            $('#inputKiloId').numberbox('textbox').bind('keyup', function (e) {
                let price = $('#processingCostId').numberbox('getValue') || 0;
                let _value = $(this).val();
                $('#moneyId').numberbox('setValue', price * _value);
            })
            $('#processingCostId').numberbox('textbox').bind('keyup', function (e) {
                let qty = $('#inputKiloId').numberbox('getValue') || 0;
                let _value = $(this).val();
                $('#moneyId').numberbox('setValue', qty * _value);
            });
            // $('.form-dialog-edit').find('.form-row:visible').eq(0).css({'margin-top': '10px'});
        },
        buttons: [
            {
                text: '作废',
                iconCls: 'icon-cancel',
                handler: e => {
                    let param = $.formDialog.getForm().serializeObject();
                    if (param.id) {
                        cancelByPk(param.id, function (data) {
                            if (data.success) {
                                $.formDialog.close();
                                btn_search();
                            } else {
                                messagerAlert(data.message);
                            }
                        })
                    }
                }
            }, {
                text: '保存',
                iconCls: 'icon-save',
                handler: e => {
                    let dialog = $('.form-dialog-edit');
                    if (!$(dialog.find('form')).form('validate')) {
                        return;
                    }
                    let param = $(dialog.find('form')).serializeObject();
                    let url = 'productOutwardOrder/insertSelective';
                    //  保存
                    $.messager.progress({
                        title: '',
                        msg: '请稍等...',
                        interval: 5000
                    });
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        data: param,
                        success: function (data) {
                            if (data.success) {
                                $.formDialog.close();
                                btn_search();
                                $.messager.show({title: '提示', msg: data.message});
                            } else {
                                $.messager.alert('提示', data.message, 'warning');
                            }
                        },
                        complete: function () {
                            $.messager.progress('close');
                        }
                    })
                }
            }, {
                text: '退出',
                iconCls: 'icon-no',
                handler: e => {
                    $.formDialog.close();
                }
            }],
    });
    $.formDialog.disableBtn(['作废']);
}


// 白坯退货 模态窗
function btn_fabric_returining() {
    let kGrid = $('#v_grid').data('kendoGrid');
    let dataItem = kGrid.dataItem(kGrid.select());
    if (dataItem == undefined) {
        messagerAlert('未选中入库数据！')
        return;
    }
    let loadData = {
        orderDate: getCurrentDate(),
    }
    if (!dataItem.id) {
        $.ajax({
            url: `plan/queryByPk/${dataItem.planId}`,
            type: 'GET',
            dataType: 'json',
            data: {},
            async: false
        }).then(function (data) {
            loadData.linkedOrderCode = dataItem['billcode'];
            loadData.planCode = dataItem['planCode'];
            loadData.processCompany = '车间';
            loadData.isFinishedProduct = true;
            loadData.isSendDirectly = false;
            loadData.consignee = dataItem['client'];
            loadData.processingCost = dataItem['price'] || 0;
            loadData.money = dataItem['money'] || 0;
            loadData.batchNum = dataItem['batch_num'];
            loadData.id = null;
            let columnA = [
                {
                    title: '主键',
                    field: 'id',
                    hidden: true,
                },
                {
                    title: '关联单据编号',
                    field: 'linkedOrderCode',
                    hidden: true,
                },
                {
                    title: '单据日期',
                    field: 'orderDate',
                    editor: {type: 'datebox', options: {required: true, value: getCurrentDate()}}
                },
                {
                    title: '单据编号',
                    field: 'orderCode',
                    hidden: true
                },
                {
                    title: '加工户', field: 'processCompany', id: 'processCompanyInput', hidden: true
                },
                {
                    title: '加工类型',
                    field: 'isFinishedProduct',
                    hidden: true
                },
                {
                    title: '计划单号',
                    field: 'planCode',
                    id: 'planCodeInput',
                    hidden: true,
                },
                {
                    title: '退回仓库', field: 'storageId', editor: {
                        type: 'combobox', options: {
                            required: true,
                            url: 'storage/listQuery',
                            method: 'GET',
                            textField: 'storageName',
                            valueField: 'id',
                            value: 1,
                            panelHeight: 'auto',
                            onLoadSuccess: function () {
                                $(this).combobox('options').limitToList = true;
                            }
                        }
                    }
                },
                {
                    title: '客户', id: 'supplierNameInput', field: 'clientName', hidden: true
                },
                {
                    title: '批次号',
                    field: 'batchNum',
                    id: 'batchNumInput',
                    hidden: true
                },
                {
                    title: '件数',
                    field: 'boxAmount',
                    id: 'boxAmountField',
                    hidden: true
                },
                {
                    title: '坯布规格',
                    field: 'greyfabricSpecification',
                    hidden: true
                },
                {
                    title: '匹数',
                    field: 'inputPairs',
                    id: 'inputPairsField',
                    editor: {type: 'numberbox', options: {precision: 0, required: true}}
                },
                {
                    title: '公斤',
                    field: 'inputKilo',
                    id: 'inputKiloId',
                    editor: {type: 'numberbox', options: {precision: 1, required: true}}
                },
                {
                    title: '单价',
                    field: 'processingCost',
                    id: 'processingCostId',
                    hidden: true
                },
                {
                    title: '金额',
                    field: 'money',
                    id: 'moneyId',
                    hidden: true
                },
                {
                    title: 'A/B',
                    field: 'abSurface',
                    id: 'abSurfaceId',
                    hidden: true
                },
                {
                    title: '收货单位',
                    field: 'consignee',
                    id: 'consigneeId',
                    hidden: true
                },
                {
                    title: '直发',
                    width: '40px',
                    field: 'isSendDirectly',
                    id: 'fieldIsSendDirectly',
                    hidden: true
                },
                {
                    title: '经手人', field: 'handlerName', editor: {
                        type: 'combobox', options: {
                            required: false,
                            method: 'GET',
                            url: 'employee/listQuery',
                            queryParams: {state: 1},
                            textField: 'empName',
                            valueField: 'empName'
                        }
                    }
                },
                {
                    title: '制单人',
                    field: 'createUserId',
                    editor: {
                        type: 'combobox',
                        options: {
                            readonly: true,
                            url: 'sysUser/query',
                            method: 'GET',
                            textField: 'userName',
                            cls: 'input-blue',
                            valueField: 'id',
                            onLoadSuccess: function () {
                                $(this).combobox('options').limitToList = true;
                            }
                        }
                    }
                },
                {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {width: 200,}}},
            ];

            $.formDialog.open({
                mode: '白坯退货',
                columns: columnA,
                loadData: loadData,
                width: 350,
                height: 350,
                onOpen: function () {
                },
                buttons: [
                    {
                        text: '作废',
                        iconCls: 'icon-cancel',
                        handler: e => {
                            let param = $.formDialog.getForm().serializeObject();
                            if (param.id) {
                                cancelByPk(param.id, function (data) {
                                    if (data.success) {
                                        $.formDialog.close();
                                        btn_search();
                                    } else {
                                        messagerAlert(data.message);
                                    }
                                })
                            }
                        }
                    }, {
                        text: '保存',
                        iconCls: 'icon-save',
                        handler: e => {
                            let dialog = $('.form-dialog-edit');
                            if (!$(dialog.find('form')).form('validate')) {
                                return;
                            }
                            let param = $(dialog.find('form')).serializeObject();
                            let url = 'productOutwardOrder/insertSelective';
                            //  保存
                            $.messager.progress({
                                title: '',
                                msg: '请稍等...',
                                interval: 5000
                            });
                            $.ajax({
                                url: url,
                                type: 'POST',
                                dataType: 'json',
                                data: param,
                                success: function (data) {
                                    if (data.success) {
                                        $.formDialog.close();
                                        btn_search();
                                        $.messager.show({title: '提示', msg: data.message});
                                    } else {
                                        $.messager.alert('提示', data.message, 'warning');
                                    }
                                },
                                complete: function () {
                                    $.messager.progress('close');
                                }
                            })
                        }
                    }, {
                        text: '退出',
                        iconCls: 'icon-no',
                        handler: e => {
                            $.formDialog.close();
                        }
                    }],
            });
        });
    } else {
        $.ajax({
            url: `productOutwardOrder/queryByPk/${dataItem.id}`,
            type: 'GET',
            dataType: 'json',
            data: {},
            async: false
        }).then(function (data) {
            loadData = data;
            loadData.linkedOrderCode = data.orderCode;
            loadData.id = null;
            let columnA = [
                {
                    title: '主键',
                    field: 'id',
                    hidden: true,
                },
                {
                    title: '关联单据编号',
                    field: 'linkedOrderCode',
                    hidden: true,
                },
                {
                    title: '单据日期',
                    field: 'orderDate',
                    editor: {type: 'datebox', options: {required: true, value: getCurrentDate()}}
                },
                {
                    title: '单据编号',
                    field: 'orderCode',
                    hidden: true
                },
                {
                    title: '加工户', field: 'processCompany', id: 'processCompanyInput', hidden: true
                },
                {
                    title: '加工类型',
                    field: 'isFinishedProduct',
                    hidden: true
                },
                {
                    title: '计划单号',
                    field: 'planCode',
                    id: 'planCodeInput',
                    hidden: true,
                },
                {
                    title: '退回仓库', field: 'storageId', editor: {
                        type: 'combobox', options: {
                            required: true,
                            url: 'storage/listQuery',
                            method: 'GET',
                            textField: 'storageName',
                            valueField: 'id',
                            value: 1,
                            panelHeight: 'auto',
                            onLoadSuccess: function () {
                                $(this).combobox('options').limitToList = true;
                            }
                        }
                    }
                },
                {
                    title: '客户', id: 'supplierNameInput', field: 'clientName', hidden: true
                },
                {
                    title: '批次号',
                    field: 'batchNum',
                    id: 'batchNumInput',
                    hidden: true
                },
                {
                    title: '件数',
                    field: 'boxAmount',
                    id: 'boxAmountField',
                    hidden: true
                },
                {
                    title: '坯布规格',
                    field: 'greyfabricSpecification',
                    hidden: true
                },
                {
                    title: '匹数',
                    field: 'inputPairs',
                    id: 'inputPairsField',
                    editor: {type: 'numberbox', options: {precision: 0, required: true}}
                },
                {
                    title: '公斤',
                    field: 'inputKilo',
                    id: 'inputKiloId',
                    editor: {type: 'numberbox', options: {precision: 1, required: true}}
                },
                {
                    title: '单价',
                    field: 'processingCost',
                    id: 'processingCostId',
                    hidden: true
                },
                {
                    title: '金额',
                    field: 'money',
                    id: 'moneyId',
                    hidden: true
                },
                {
                    title: 'A/B',
                    field: 'abSurface',
                    id: 'abSurfaceId',
                    hidden: true
                },
                {
                    title: '收货单位',
                    field: 'consignee',
                    id: 'consigneeId',
                    hidden: true
                },
                {
                    title: '直发',
                    width: '40px',
                    field: 'isSendDirectly',
                    id: 'fieldIsSendDirectly',
                    hidden: true
                },
                {
                    title: '经手人', field: 'handlerName', editor: {
                        type: 'combobox', options: {
                            required: false,
                            method: 'GET',
                            url: 'employee/listQuery',
                            queryParams: {state: 1},
                            textField: 'empName',
                            valueField: 'empName'
                        }
                    }
                },
                {
                    title: '制单人',
                    field: 'createUserId',
                    editor: {
                        type: 'combobox',
                        options: {
                            readonly: true,
                            url: 'sysUser/query',
                            method: 'GET',
                            textField: 'userName',
                            cls: 'input-blue',
                            valueField: 'id',
                            onLoadSuccess: function () {
                                $(this).combobox('options').limitToList = true;
                            }
                        }
                    }
                },
                {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {width: 200,}}},
            ];
            $.formDialog.open({
                mode: '白坯退货',
                columns: columnA,
                loadData: loadData,
                width: 350,
                height: 350,
                onOpen: function () {
                },
                buttons: [{
                    text: '作废',
                    iconCls: 'icon-cancel',
                    handler: e => {
                        let param = $.formDialog.getForm().serializeObject();
                        if (param.id) {
                            cancelByPk(param.id, function (data) {
                                if (data.success) {
                                    $.formDialog.close();
                                    btn_search();
                                } else {
                                    messagerAlert(data.message);
                                }
                            })
                        }
                    }
                }, {
                    text: '保存',
                    iconCls: 'icon-save',
                    handler: e => {
                        let dialog = $('.form-dialog-edit');
                        if (!$(dialog.find('form')).form('validate')) {
                            return;
                        }
                        let param = $(dialog.find('form')).serializeObject();
                        let url = 'productOutwardOrder/insertSelective';
                        //  保存
                        $.messager.progress({
                            title: '',
                            msg: '请稍等...',
                            interval: 5000
                        });
                        $.ajax({
                            url: url,
                            type: 'POST',
                            dataType: 'json',
                            data: param,
                            success: function (data) {
                                if (data.success) {
                                    $.formDialog.close();
                                    btn_search();
                                    $.messager.show({title: '提示', msg: data.message});
                                } else {
                                    $.messager.alert('提示', data.message, 'warning');
                                }
                            },
                            complete: function () {
                                $.messager.progress('close');
                            }
                        })
                    }
                }, {
                    text: '退出',
                    iconCls: 'icon-no',
                    handler: e => {
                        $.formDialog.close();
                    }
                }],
            });
            $.formDialog.disableBtn(['作废']);
        });
    }
}

function btn_fabric_returining_modify(data) {
    if (data == undefined) {
        messagerAlert('未选中入库数据！')
        return;
    }
    let loadData = data;
    let columnA = [
        {
            title: '主键',
            field: 'id',
            hidden: true,
        },
        {
            title: '关联单据编号',
            field: 'linkedOrderCode',
            hidden: true,
        },
        {
            title: '单据日期',
            field: 'orderDate',
            editor: {type: 'datebox', options: {required: true, value: getCurrentDate()}}
        },
        {
            title: '单据编号',
            field: 'orderCode',
            hidden: true
        },
        {
            title: '加工户', field: 'processCompany', id: 'processCompanyInput', hidden: true
        },
        {
            title: '加工类型',
            field: 'isFinishedProduct',
            hidden: true
        },
        {
            title: '计划单号',
            field: 'planCode',
            id: 'planCodeInput',
            hidden: true,
        },
        {
            title: '退回仓库', field: 'storageId', editor: {
                type: 'combobox', options: {
                    required: true,
                    url: 'storage/listQuery',
                    method: 'GET',
                    textField: 'storageName',
                    valueField: 'id',
                    panelHeight: 'auto',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '客户', id: 'supplierNameInput', field: 'clientName', hidden: true
        },
        {
            title: '批次号',
            field: 'batchNum',
            id: 'batchNumInput',
            hidden: true
        },
        {
            title: '件数',
            field: 'boxAmount',
            id: 'boxAmountField',
            hidden: true
        },
        {
            title: '坯布规格',
            field: 'greyfabricSpecification',
            hidden: true
        },
        {
            title: '匹数',
            field: 'inputPairs',
            id: 'inputPairsField',
            editor: {type: 'numberbox', options: {precision: 0, required: true}}
        },
        {
            title: '公斤',
            field: 'inputKilo',
            id: 'inputKiloId',
            editor: {type: 'numberbox', options: {precision: 1, required: true}}
        },
        {
            title: '单价',
            field: 'processingCost',
            id: 'processingCostId',
            hidden: true
        },
        {
            title: '金额',
            field: 'money',
            id: 'moneyId',
            hidden: true
        },
        {
            title: 'A/B',
            field: 'abSurface',
            id: 'abSurfaceId',
            hidden: true
        },
        {
            title: '收货单位',
            field: 'consignee',
            id: 'consigneeId',
            hidden: true
        },
        {
            title: '直发',
            width: '40px',
            field: 'isSendDirectly',
            id: 'fieldIsSendDirectly',
            hidden: true
        },
        {
            title: '经手人', field: 'handlerName', editor: {
                type: 'combobox', options: {
                    required: false,
                    method: 'GET',
                    url: 'employee/listQuery',
                    queryParams: {state: 1},
                    textField: 'empName',
                    valueField: 'empName'
                }
            }
        },
        {
            title: '制单人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    cls: 'input-blue',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {width: 200,}}},
    ];
    $.formDialog.open({
        mode: '白坯退货',
        columns: columnA,
        loadData: loadData,
        width: 350,
        height: 350,
        onOpen: function () {
        },
        buttons: [{
            text: '作废',
            iconCls: 'icon-cancel',
            handler: e => {
                let param = $.formDialog.getForm().serializeObject();
                if (param.id) {
                    cancelByPk(param.id, function (data) {
                        if (data.success) {
                            $.formDialog.close();
                            btn_search();
                        } else {
                            messagerAlert(data.message);
                        }
                    })
                }
            }
        }, {
            text: '保存',
            iconCls: 'icon-save',
            handler: e => {
                let dialog = $('.form-dialog-edit');
                if (!$(dialog.find('form')).form('validate')) {
                    return;
                }
                let param = $(dialog.find('form')).serializeObject();
                let url = 'productOutwardOrder/updateSelective';
                //  保存
                $.messager.progress({
                    title: '',
                    msg: '请稍等...',
                    interval: 5000
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: param,
                    success: function (data) {
                        if (data.success) {
                            $.formDialog.close();
                            btn_search();
                            $.messager.show({title: '提示', msg: data.message});
                        } else {
                            $.messager.alert('提示', data.message, 'warning');
                        }
                    },
                    complete: function () {
                        $.messager.progress('close');
                    }
                })
            }
        }, {
            text: '退出',
            iconCls: 'icon-no',
            handler: e => {
                $.formDialog.close();
            }
        }],
    });
}

function btn_add_outbound() {
    function load(ele, data) {
        $(ele).form('load', data);
    }

    let loadData = {
        orderDate: getCurrentDate(),
        // planCode: dataItem['plan_code'],
        // materialSpecification: dataItem['material_specification'],
        // processingCost: dataItem['processing_cost'] || 0,
        // supplierName: dataItem['material_supplier_name'],
    }
    let columnA = [
        {
            title: '主键',
            field: 'id',
            hidden: true,
        },
        {
            title: '单据日期',
            field: 'orderDate',
            editor: {type: 'datebox', options: {required: true, value: getCurrentDate()}}
        },
        {
            title: '单据编号',
            field: 'orderCode',
            editor: {
                type: 'textbox',
                options: {
                    readonly: true
                }
            }
        },
        {
            title: '加工户', field: 'processCompany', id: 'processCompanyInput', editor: {
                type: 'combobox', options: {
                    required: true,
                    url: 'processCompany/listQuery',
                    method: 'GET',
                    textField: 'companyName',
                    valueField: 'companyName',
                    onLoadSuccess: function () {
                    }
                }
            }
        },
        {
            title: '加工类型',
            field: 'isFinishedProduct',
            editor: {
                type: 'combobox',
                options: {
                    data: [{id: 0, text: '未剖幅'}, {id: 1, text: '已剖幅'}],
                    limitToList: true,
                    value: 1,
                    required: true,
                    textField: 'text',
                    valueField: 'id',
                    panelHeight: 'auto',
                    onChange: function (newVal, oldVal) {
                        if (newVal) {
                            $('#boxAmountField').numberbox('clear');
                            $('#boxAmountField').numberbox({required: false, readonly: true});
                            $('#inputPairsField').numberbox({readonly: false, required: true});
                            $('#abSurfaceId').combobox('readonly', false);
                            $('#fieldIsSendDirectly').checkbox('enable');
                        } else {
                            $('#abSurfaceId').combobox('clear');
                            $('#abSurfaceId').combobox('readonly', true);
                            $('#consigneeId').combobox('clear');
                            $('#fieldIsSendDirectly').checkbox('disable');
                            $('#inputPairsField').numberbox('clear');
                            $('#inputPairsField').numberbox({required: false, readonly: true});
                            $('#boxAmountField').numberbox({readonly: false, required: true});
                        }
                    }
                }
            }
        },
        {
            title: '计划单号',
            field: 'planCode',
            id: 'planCodeInput',
            editor: {
                type: 'textbox',
                options: {
                    icons: [{
                        iconCls: 'icon-remove', handler: function (e) {
                            $(e.data.target).textbox('clear');
                        }
                    }, {
                        iconCls: 'icon-add', handler: function (e) {
                            let div = $(`<div>
                                        <div id="t_dialog" 
                                             data-options="title:'白坯计划',width:730,height:500,resizable:true,
                                                           modal:true,iconCls:'icon-search',
                                                           onClose:function(){$(this).dialog('destroy')},
                                                           /*
                                                           buttons:[{text:'取消',iconCls:'icon-cancel',handler:function(){
                                                                        $('#t_dialog').dialog('close');
                                                                    }},
                                                                    {text:'保存',iconCls:'icon-save',handler:function(){
                                                                        let _selected = $('#t_plancode').datagrid('getSelected');
                                                                        if(_selected != undefined){
                                                                               delete _selected['createUserId'];
                                                                                 let specification = [
                                                                                    _selected['orderCode'],
                                                                                    _selected['productName'],
                                                                                    _selected['greyfabricWidth'] + '*' + _selected['greyfabricWeight'],
                                                                                    _selected['mHeight'],
                                                                                    _selected['greyfabricSpecification'],
                                                                                    _selected['materialSpecification'],
                                                                                ];
                                                                                _selected['greyfabricSpecification'] = specification.join(' ');
                                                                               $('.form-dialog-edit form').eq(0).form('load',_selected);
                                                                               $('#batchNumInput').combobox('options').queryParams =  {planCode: _selected['planCode']};
                                                                               $('#batchNumInput').combobox('reload');
                                                                               $('#t_dialog').dialog('close');
                                                                        } else {
                                                                          $.messager.alert('提示','未选中!','warning')  
                                                                        }
                                                                    }}]
                                                                    */
                                                           " 
                                             class="easyui-dialog">
                                            <table id="t_plancode"></table>
                                        </div>
                                    </div>`);
                            $.parser.parse(div);
                            $('#t_plancode').datagrid({
                                url: 'plan/listQuery',
                                queryParams: {state: 1, markFinished: false},
                                method: 'GET',
                                striped: true,
                                fit: true,
                                sortable: true,
                                columns: [[
                                    {
                                        title: '计划日期',
                                        hidden: true,
                                        field: 'planDate',
                                        width: 120,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '计划单号', field: 'planCode', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '门幅',
                                        field: 'greyfabricWidth',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '克重', field: 'lowWeight', width: 60, align: 'center', halign: 'center'},
                                    {
                                        title: '头份',
                                        field: 'greyfabricSpecification',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '毛高', field: 'mHeight', width: 120, align: 'center', halign: 'center'},
                                    {title: '品名', field: 'productName', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '原料规格',
                                        field: 'materialSpecification',
                                        width: 100,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                ]],
                                rownumbers: true,
                                remoteSort: false,
                                showFooter: true,
                                singleSelect: true,
                                onBeforeLoad: function () {
                                },
                                onLoadSuccess: function () {
                                },
                                onDblClickRow: function (index, row) {
                                    delete row['createUserId'];
                                    let specification = [
                                        row['orderCode'],
                                        row['productName'],
                                        row['greyfabricWidth'] + '*' + row['greyfabricWeight'],
                                        row['mHeight'],
                                        row['greyfabricSpecification'],
                                        row['materialSpecification'],
                                    ];
                                    row['greyfabricSpecification'] = specification.join(' ');
                                    row['greyfabricSpecification'] = specification.join(' ');
                                    load($('.form-dialog-edit form').eq(0), row);
                                    $('#batchNumInput').combobox('options').queryParams = {planCode: row['planCode']};
                                    $('#batchNumInput').combobox('reload');
                                    $.ajax({
                                        url: 'contactCompany/queryByPk/' + row['contactCompanyId'],
                                        type: 'GET',
                                        dataType: 'json',
                                        async: false,
                                        success: function (data) {
                                            if (data != undefined)
                                                $('#supplierNameInput').textbox('setValue', data['companyName'] || '')
                                        }
                                    })
                                    $('#t_dialog').dialog('close');
                                }
                            });
                        }
                    }],
                    required: true,
                }
            }
        },
        {
            title: '客户', id: 'supplierNameInput', field: 'clientName', editor: {
                type: 'textbox', options: {}
            }
        },
        {
            title: '批次号',
            field: 'batchNum',
            id: 'batchNumInput',
            editor: {
                type: 'combobox',
                options: {
                    url: 'planBatchnum/listQuery/planCode',
                    method: 'GET',
                    panelHeight: 300,
                    textField: 'batchNum',
                    valueField: 'batchNum',
                    onLoadSuccess: function (data) {
                        let fieldValue = $(this).combobox('options').valueField;
                        if (data instanceof Array && data.length > 0 && !$(this).combobox('getValue')) {
                            $(this).combobox('setValue', data[0][fieldValue]);
                        }
                    }
                }
            }
        },
        /*{
            title: '原料规格',
            field: 'materialSpecification',
            editor: {
                type: 'textbox',
                options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    cls: 'input-blue',
                    panelHeight: 200,
                    required: true,
                }
            }
        },*/
        {
            title: '件数',
            field: 'boxAmount',
            id: 'boxAmountField',
            editor: {type: 'numberbox', options: {precision: 0, required: false, readonly: true}}
        },
        {
            title: '坯布规格',
            field: 'greyfabricSpecification',
            editor: {
                type: 'textbox',
                options: {
                    cls: 'input-blue',
                    width: 514,
                    required: true,
                }
            }
        },
        {
            title: '匹数',
            field: 'inputPairs',
            id: 'inputPairsField',
            editor: {type: 'numberbox', options: {precision: 0, required: true}}
        },
        {
            title: '公斤',
            field: 'inputKilo',
            id: 'inputKiloId',
            editor: {type: 'numberbox', options: {precision: 1, required: true}}
        },
        {
            title: '单价',
            field: 'processingCost',
            id: 'processingCostId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: '金额',
            field: 'money',
            id: 'moneyId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: 'A/B',
            field: 'abSurface',
            id: 'abSurfaceId',
            editor: {
                type: 'combobox', options: {
                    buttonIcon: 'icon-add',
                    onClickButton: function () {
                        let _value = $('#abSurfaceId').combobox('getValue');
                        if (_value) {
                            switch (_value) {
                                case 'A':
                                    $('#abSurfaceId').combobox('setValue', 'B');
                                    break;
                                case 'B':
                                    $('#abSurfaceId').combobox('clear');
                                    break;
                            }
                        } else {
                            $('#abSurfaceId').combobox('setValue', 'A');
                        }
                    },
                    data: [{id: 'A'}, {id: 'B'}],
                    panelHeight: 'auto',
                    textField: 'id',
                    valueField: 'id',
                    required: false,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '收货单位',
            field: 'consignee',
            id: 'consigneeId',
            editor: {
                type: 'combobox', options: {
                    width: 120, cls: 'input-red', readonly: true,
                    url: 'takeDeliveryAddress/listQuery',
                    method: 'GET',
                    textField: 'company',
                    valueField: 'company',
                    panelWidth: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '直发',
            width: '40px',
            field: 'isSendDirectly',
            id: 'fieldIsSendDirectly',
            editor: {
                type: 'checkbox', options: {
                    cls: 'input-red', value: true, onChange: function (checked) {
                        if (checked) {
                            let _value = $('#consigneeId').combobox('getValue');
                            $('#consigneeId').combobox({readonly: false, required: true});
                            $('#consigneeId').combobox('setValue', _value);
                        } else {
                            $('#consigneeId').combobox('clear');
                            $('#consigneeId').combobox({readonly: true, required: false});
                        }
                    }
                }
            }
        },
        {
            title: '经手人', field: 'handlerName', editor: {
                type: 'combobox', options: {
                    required: false,
                    method: 'GET',
                    url: 'employee/listQuery',
                    queryParams: {state: 1},
                    textField: 'empName',
                    valueField: 'empName'
                }
            }
        },
        {
            title: '制单人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    cls: 'input-blue',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {width: 514,}}},
    ];
    $.formDialog.open({
        mode: '新增',
        columns: columnA,
        loadData: loadData,
        width: 656,
        height: 460,
        onOpen: function () {
            $('#inputKiloId').numberbox('textbox').bind('keyup', function (e) {
                let price = $('#processingCostId').numberbox('getValue') || 0;
                let _value = $(this).val();
                $('#moneyId').numberbox('setValue', price * _value);
            })
            $('#processingCostId').numberbox('textbox').bind('keyup', function (e) {
                let qty = $('#inputKiloId').numberbox('getValue') || 0;
                let _value = $(this).val();
                $('#moneyId').numberbox('setValue', qty * _value);
            });
            // $('.form-dialog-edit').find('.form-row:visible').eq(0).css({'margin-top': '10px'});
        },
        buttons: [{
            text: '作废',
            iconCls: 'icon-cancel',
            handler: e => {
                let param = $.formDialog.getForm().serializeObject();
                if (param.id) {
                    cancelByPk(param.id, function (data) {
                        if (data.success) {
                            $.formDialog.close();
                            btn_search();
                        } else {
                            messagerAlert(data.message);
                        }
                    })
                }
            }
        }, {
            text: '保存',
            iconCls: 'icon-save',
            handler: e => {
                let dialog = $('.form-dialog-edit');
                if (!$(dialog.find('form')).form('validate')) {
                    return;
                }
                let param = $(dialog.find('form')).serializeObject();
                let url = 'productOutwardOrder/insertSelective';
                //  保存
                $.messager.progress({
                    title: '',
                    msg: '请稍等...',
                    interval: 5000
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: param,
                    success: function (data) {
                        if (data.success) {
                            $.formDialog.close();
                            btn_search();
                            $.messager.show({title: '提示', msg: data.message});
                        } else {
                            $.messager.alert('提示', data.message, 'warning');
                        }
                    },
                    complete: function () {
                        $.messager.progress('close');
                    }
                })
            }
        }, {
            text: '退出',
            iconCls: 'icon-no',
            handler: e => {
                $.formDialog.close();
            }
        }],
    });
}

function btn_process_returining() {
    function load(ele, data) {
        $(ele).form('load', data);
    }

    let loadData = {
        orderDate: getCurrentDate(),
        // planCode: dataItem['plan_code'],
        // materialSpecification: dataItem['material_specification'],
        // processingCost: dataItem['processing_cost'] || 0,
        // supplierName: dataItem['material_supplier_name'],
    }
    let columnA = [
        {
            title: '主键',
            field: 'id',
            hidden: true,
        },
        {
            title: '单据日期',
            field: 'orderDate',
            editor: {type: 'datebox', options: {required: true, value: getCurrentDate()}}
        },
        {
            title: '单据编号',
            field: 'orderCode',
            editor: {
                type: 'textbox',
                options: {
                    readonly: true
                }
            }
        },
        {
            title: '加工户', field: 'processCompany', id: 'processCompanyInput', editor: {
                type: 'combobox', options: {
                    required: true,
                    url: 'processCompany/listQuery',
                    method: 'GET',
                    textField: 'companyName',
                    valueField: 'companyName',
                    onLoadSuccess: function () {
                    }
                }
            }
        },
        {
            title: '加工类型',
            field: 'isFinishedProduct',
            editor: {
                type: 'combobox',
                options: {
                    data: [{id: 0, text: '未剖幅'}, {id: 1, text: '已剖幅'}],
                    limitToList: true,
                    value: 1,
                    required: true,
                    textField: 'text',
                    valueField: 'id',
                    panelHeight: 'auto',
                    onChange: function (newVal, oldVal) {
                        if (newVal) {
                            $('#boxAmountField').numberbox('clear');
                            $('#boxAmountField').numberbox({required: false, readonly: true});
                            $('#inputPairsField').numberbox({readonly: false, required: true});
                            $('#abSurfaceId').combobox('readonly', false);
                            $('#fieldIsSendDirectly').checkbox('enable');
                        } else {
                            $('#abSurfaceId').combobox('clear');
                            $('#abSurfaceId').combobox('readonly', true);
                            $('#consigneeId').combobox('clear');
                            $('#fieldIsSendDirectly').checkbox('disable');
                            $('#inputPairsField').numberbox('clear');
                            $('#inputPairsField').numberbox({required: false, readonly: true});
                            $('#boxAmountField').numberbox({readonly: false, required: true});
                        }
                    }
                }
            }
        },
        {
            title: '计划单号',
            field: 'planCode',
            id: 'planCodeInput',
            editor: {
                type: 'textbox',
                options: {
                    icons: [{
                        iconCls: 'icon-remove', handler: function (e) {
                            $(e.data.target).textbox('clear');
                        }
                    }, {
                        iconCls: 'icon-add', handler: function (e) {
                            let div = $(`<div>
                                        <div id="t_dialog" 
                                             data-options="title:'白坯计划',width:730,height:500,resizable:true,
                                                           modal:true,iconCls:'icon-search',
                                                           onClose:function(){$(this).dialog('destroy')},
                                                           /*
                                                           buttons:[{text:'取消',iconCls:'icon-cancel',handler:function(){
                                                                        $('#t_dialog').dialog('close');
                                                                    }},
                                                                    {text:'保存',iconCls:'icon-save',handler:function(){
                                                                        let _selected = $('#t_plancode').datagrid('getSelected');
                                                                        if(_selected != undefined){
                                                                               delete _selected['createUserId'];
                                                                                 let specification = [
                                                                                    _selected['orderCode'],
                                                                                    _selected['productName'],
                                                                                    _selected['greyfabricWidth'] + '*' + _selected['greyfabricWeight'],
                                                                                    _selected['mHeight'],
                                                                                    _selected['greyfabricSpecification'],
                                                                                    _selected['materialSpecification'],
                                                                                ];
                                                                                _selected['greyfabricSpecification'] = specification.join(' ');
                                                                               $('.form-dialog-edit form').eq(0).form('load',_selected);
                                                                               $('#batchNumInput').combobox('options').queryParams =  {planCode: _selected['planCode']};
                                                                               $('#batchNumInput').combobox('reload');
                                                                               $('#t_dialog').dialog('close');
                                                                        } else {
                                                                          $.messager.alert('提示','未选中!','warning')  
                                                                        }
                                                                    }}]
                                                                    */
                                                           " 
                                             class="easyui-dialog">
                                            <table id="t_plancode"></table>
                                        </div>
                                    </div>`);
                            $.parser.parse(div);
                            $('#t_plancode').datagrid({
                                url: 'plan/listQuery',
                                queryParams: {state: 1, markFinished: false},
                                method: 'GET',
                                striped: true,
                                fit: true,
                                sortable: true,
                                columns: [[
                                    {
                                        title: '计划日期',
                                        hidden: true,
                                        field: 'planDate',
                                        width: 120,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '计划单号', field: 'planCode', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '门幅',
                                        field: 'greyfabricWidth',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '克重', field: 'lowWeight', width: 60, align: 'center', halign: 'center'},
                                    {
                                        title: '头份',
                                        field: 'greyfabricSpecification',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '毛高', field: 'mHeight', width: 120, align: 'center', halign: 'center'},
                                    {title: '品名', field: 'productName', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '原料规格',
                                        field: 'materialSpecification',
                                        width: 100,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                ]],
                                rownumbers: true,
                                remoteSort: false,
                                showFooter: true,
                                singleSelect: true,
                                onBeforeLoad: function () {
                                },
                                onLoadSuccess: function () {
                                },
                                onDblClickRow: function (index, row) {
                                    delete row['createUserId'];
                                    let specification = [
                                        row['orderCode'],
                                        row['productName'],
                                        row['greyfabricWidth'] + '*' + row['greyfabricWeight'],
                                        row['mHeight'],
                                        row['greyfabricSpecification'],
                                        row['materialSpecification'],
                                    ];
                                    row['greyfabricSpecification'] = specification.join(' ');
                                    row['greyfabricSpecification'] = specification.join(' ');
                                    load($('.form-dialog-edit form').eq(0), row);
                                    $('#batchNumInput').combobox('options').queryParams = {planCode: row['planCode']};
                                    $('#batchNumInput').combobox('reload');
                                    $.ajax({
                                        url: 'contactCompany/queryByPk/' + row['contactCompanyId'],
                                        type: 'GET',
                                        dataType: 'json',
                                        async: false,
                                        success: function (data) {
                                            if (data != undefined)
                                                $('#supplierNameInput').textbox('setValue', data['companyName'] || '')
                                        }
                                    })
                                    $('#t_dialog').dialog('close');
                                }
                            });
                        }
                    }],
                    required: true,
                }
            }
        },
        {
            title: '客户', id: 'supplierNameInput', field: 'clientName', editor: {
                type: 'textbox', options: {}
            }
        },
        {
            title: '批次号',
            field: 'batchNum',
            id: 'batchNumInput',
            editor: {
                type: 'combobox',
                options: {
                    url: 'planBatchnum/listQuery/planCode',
                    method: 'GET',
                    panelHeight: 300,
                    textField: 'batchNum',
                    valueField: 'batchNum',
                    onLoadSuccess: function (data) {
                        let fieldValue = $(this).combobox('options').valueField;
                        if (data instanceof Array && data.length > 0 && !$(this).combobox('getValue')) {
                            $(this).combobox('setValue', data[0][fieldValue]);
                        }
                    }
                }
            }
        },
        /*{
            title: '原料规格',
            field: 'materialSpecification',
            editor: {
                type: 'textbox',
                options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    cls: 'input-blue',
                    panelHeight: 200,
                    required: true,
                }
            }
        },*/
        {
            title: '件数',
            field: 'boxAmount',
            id: 'boxAmountField',
            editor: {type: 'numberbox', options: {precision: 0, required: false, readonly: true}}
        },
        {
            title: '坯布规格',
            field: 'greyfabricSpecification',
            editor: {
                type: 'textbox',
                options: {
                    cls: 'input-blue',
                    width: 514,
                    required: true,
                }
            }
        },
        {
            title: '匹数',
            field: 'inputPairs',
            id: 'inputPairsField',
            editor: {type: 'numberbox', options: {precision: 0, required: true}}
        },
        {
            title: '公斤',
            field: 'inputKilo',
            id: 'inputKiloId',
            editor: {type: 'numberbox', options: {precision: 1, required: true}}
        },
        {
            title: '单价',
            field: 'processingCost',
            id: 'processingCostId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: '金额',
            field: 'money',
            id: 'moneyId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: 'A/B',
            field: 'abSurface',
            id: 'abSurfaceId',
            editor: {
                type: 'combobox', options: {
                    buttonIcon: 'icon-add',
                    onClickButton: function () {
                        let _value = $('#abSurfaceId').combobox('getValue');
                        if (_value) {
                            switch (_value) {
                                case 'A':
                                    $('#abSurfaceId').combobox('setValue', 'B');
                                    break;
                                case 'B':
                                    $('#abSurfaceId').combobox('clear');
                                    break;
                            }
                        } else {
                            $('#abSurfaceId').combobox('setValue', 'A');
                        }
                    },
                    data: [{id: 'A'}, {id: 'B'}],
                    panelHeight: 'auto',
                    textField: 'id',
                    valueField: 'id',
                    required: false,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '收货单位',
            field: 'consignee',
            id: 'consigneeId',
            editor: {
                type: 'combobox', options: {
                    width: 120, cls: 'input-red', readonly: true,
                    url: 'takeDeliveryAddress/listQuery',
                    method: 'GET',
                    textField: 'company',
                    valueField: 'company',
                    panelWidth: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '直发',
            width: '40px',
            field: 'isSendDirectly',
            id: 'fieldIsSendDirectly',
            editor: {
                type: 'checkbox', options: {
                    cls: 'input-red', value: true, onChange: function (checked) {
                        if (checked) {
                            let _value = $('#consigneeId').combobox('getValue');
                            $('#consigneeId').combobox({readonly: false, required: true});
                            $('#consigneeId').combobox('setValue', _value);
                        } else {
                            $('#consigneeId').combobox('clear');
                            $('#consigneeId').combobox({readonly: true, required: false});
                        }
                    }
                }
            }
        },
        {
            title: '经手人', field: 'handlerName', editor: {
                type: 'combobox', options: {
                    required: false,
                    method: 'GET',
                    url: 'employee/listQuery',
                    queryParams: {state: 1},
                    textField: 'empName',
                    valueField: 'empName'
                }
            }
        },
        {
            title: '制单人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    cls: 'input-blue',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {width: 514,}}},
    ];
    $.formDialog.open({
        mode: '新增',
        columns: columnA,
        loadData: loadData,
        width: 656,
        height: 460,
        onOpen: function () {
            $('#inputKiloId').numberbox('textbox').bind('keyup', function (e) {
                let price = $('#processingCostId').numberbox('getValue') || 0;
                let _value = $(this).val();
                $('#moneyId').numberbox('setValue', price * _value);
            })
            $('#processingCostId').numberbox('textbox').bind('keyup', function (e) {
                let qty = $('#inputKiloId').numberbox('getValue') || 0;
                let _value = $(this).val();
                $('#moneyId').numberbox('setValue', qty * _value);
            });
            // $('.form-dialog-edit').find('.form-row:visible').eq(0).css({'margin-top': '10px'});
        },
        buttons: [{
            text: '作废',
            iconCls: 'icon-cancel',
            handler: e => {
                let param = $.formDialog.getForm().serializeObject();
                if (param.id) {
                    cancelByPk(param.id, function (data) {
                        if (data.success) {
                            $.formDialog.close();
                            btn_search();
                        } else {
                            messagerAlert(data.message);
                        }
                    })
                }
            }
        }, {
            text: '保存',
            iconCls: 'icon-save',
            handler: e => {
                let dialog = $('.form-dialog-edit');
                if (!$(dialog.find('form')).form('validate')) {
                    return;
                }
                let param = $(dialog.find('form')).serializeObject();
                let url = 'productOutwardOrder/insertSelective';
                //  保存
                $.messager.progress({
                    title: '',
                    msg: '请稍等...',
                    interval: 5000
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: param,
                    success: function (data) {
                        if (data.success) {
                            $.formDialog.close();
                            btn_search();
                            $.messager.show({title: '提示', msg: data.message});
                        } else {
                            $.messager.alert('提示', data.message, 'warning');
                        }
                    },
                    complete: function () {
                        $.messager.progress('close');
                    }
                })
            }
        }, {
            text: '退出',
            iconCls: 'icon-no',
            handler: e => {
                $.formDialog.close();
            }
        }],
    });
}

function btn_fabric_check() {
    function load(ele, data) {
        $(ele).form('load', data);
    }

    let loadData = {
        orderDate: getCurrentDate(),
        // planCode: dataItem['plan_code'],
        // materialSpecification: dataItem['material_specification'],
        // processingCost: dataItem['processing_cost'] || 0,
        // supplierName: dataItem['material_supplier_name'],
    }
    let columnA = [
        {
            title: '主键',
            field: 'id',
            hidden: true,
        },
        {
            title: '单据日期',
            field: 'orderDate',
            editor: {type: 'datebox', options: {required: true, value: getCurrentDate()}}
        },
        {
            title: '单据编号',
            field: 'orderCode',
            editor: {
                type: 'textbox',
                options: {
                    readonly: true
                }
            }
        },
        {
            title: '加工户', field: 'processCompany', id: 'processCompanyInput', editor: {
                type: 'combobox', options: {
                    required: true,
                    url: 'processCompany/listQuery',
                    method: 'GET',
                    textField: 'companyName',
                    valueField: 'companyName',
                    onLoadSuccess: function () {
                    }
                }
            }
        },
        {
            title: '加工类型',
            field: 'isFinishedProduct',
            editor: {
                type: 'combobox',
                options: {
                    data: [{id: 0, text: '未剖幅'}, {id: 1, text: '已剖幅'}],
                    limitToList: true,
                    value: 1,
                    required: true,
                    textField: 'text',
                    valueField: 'id',
                    panelHeight: 'auto',
                    onChange: function (newVal, oldVal) {
                        if (newVal) {
                            $('#boxAmountField').numberbox('clear');
                            $('#boxAmountField').numberbox({required: false, readonly: true});
                            $('#inputPairsField').numberbox({readonly: false, required: true});
                            $('#abSurfaceId').combobox('readonly', false);
                            $('#fieldIsSendDirectly').checkbox('enable');
                        } else {
                            $('#abSurfaceId').combobox('clear');
                            $('#abSurfaceId').combobox('readonly', true);
                            $('#consigneeId').combobox('clear');
                            $('#fieldIsSendDirectly').checkbox('disable');
                            $('#inputPairsField').numberbox('clear');
                            $('#inputPairsField').numberbox({required: false, readonly: true});
                            $('#boxAmountField').numberbox({readonly: false, required: true});
                        }
                    }
                }
            }
        },
        {
            title: '计划单号',
            field: 'planCode',
            id: 'planCodeInput',
            editor: {
                type: 'textbox',
                options: {
                    icons: [{
                        iconCls: 'icon-remove', handler: function (e) {
                            $(e.data.target).textbox('clear');
                        }
                    }, {
                        iconCls: 'icon-add', handler: function (e) {
                            let div = $(`<div>
                                        <div id="t_dialog" 
                                             data-options="title:'白坯计划',width:730,height:500,resizable:true,
                                                           modal:true,iconCls:'icon-search',
                                                           onClose:function(){$(this).dialog('destroy')},
                                                           /*
                                                           buttons:[{text:'取消',iconCls:'icon-cancel',handler:function(){
                                                                        $('#t_dialog').dialog('close');
                                                                    }},
                                                                    {text:'保存',iconCls:'icon-save',handler:function(){
                                                                        let _selected = $('#t_plancode').datagrid('getSelected');
                                                                        if(_selected != undefined){
                                                                               delete _selected['createUserId'];
                                                                                 let specification = [
                                                                                    _selected['orderCode'],
                                                                                    _selected['productName'],
                                                                                    _selected['greyfabricWidth'] + '*' + _selected['greyfabricWeight'],
                                                                                    _selected['mHeight'],
                                                                                    _selected['greyfabricSpecification'],
                                                                                    _selected['materialSpecification'],
                                                                                ];
                                                                                _selected['greyfabricSpecification'] = specification.join(' ');
                                                                               $('.form-dialog-edit form').eq(0).form('load',_selected);
                                                                               $('#batchNumInput').combobox('options').queryParams =  {planCode: _selected['planCode']};
                                                                               $('#batchNumInput').combobox('reload');
                                                                               $('#t_dialog').dialog('close');
                                                                        } else {
                                                                          $.messager.alert('提示','未选中!','warning')  
                                                                        }
                                                                    }}]
                                                                    */
                                                           " 
                                             class="easyui-dialog">
                                            <table id="t_plancode"></table>
                                        </div>
                                    </div>`);
                            $.parser.parse(div);
                            $('#t_plancode').datagrid({
                                url: 'plan/listQuery',
                                queryParams: {state: 1, markFinished: false},
                                method: 'GET',
                                striped: true,
                                fit: true,
                                sortable: true,
                                columns: [[
                                    {
                                        title: '计划日期',
                                        hidden: true,
                                        field: 'planDate',
                                        width: 120,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '计划单号', field: 'planCode', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '门幅',
                                        field: 'greyfabricWidth',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '克重', field: 'lowWeight', width: 60, align: 'center', halign: 'center'},
                                    {
                                        title: '头份',
                                        field: 'greyfabricSpecification',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '毛高', field: 'mHeight', width: 120, align: 'center', halign: 'center'},
                                    {title: '品名', field: 'productName', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '原料规格',
                                        field: 'materialSpecification',
                                        width: 100,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                ]],
                                rownumbers: true,
                                remoteSort: false,
                                showFooter: true,
                                singleSelect: true,
                                onBeforeLoad: function () {
                                },
                                onLoadSuccess: function () {
                                },
                                onDblClickRow: function (index, row) {
                                    delete row['createUserId'];
                                    let specification = [
                                        row['orderCode'],
                                        row['productName'],
                                        row['greyfabricWidth'] + '*' + row['greyfabricWeight'],
                                        row['mHeight'],
                                        row['greyfabricSpecification'],
                                        row['materialSpecification'],
                                    ];
                                    row['greyfabricSpecification'] = specification.join(' ');
                                    row['greyfabricSpecification'] = specification.join(' ');
                                    load($('.form-dialog-edit form').eq(0), row);
                                    $('#batchNumInput').combobox('options').queryParams = {planCode: row['planCode']};
                                    $('#batchNumInput').combobox('reload');
                                    $.ajax({
                                        url: 'contactCompany/queryByPk/' + row['contactCompanyId'],
                                        type: 'GET',
                                        dataType: 'json',
                                        async: false,
                                        success: function (data) {
                                            if (data != undefined)
                                                $('#supplierNameInput').textbox('setValue', data['companyName'] || '')
                                        }
                                    })
                                    $('#t_dialog').dialog('close');
                                }
                            });
                        }
                    }],
                    required: true,
                }
            }
        },
        {
            title: '客户', id: 'supplierNameInput', field: 'clientName', editor: {
                type: 'textbox', options: {}
            }
        },
        {
            title: '批次号',
            field: 'batchNum',
            id: 'batchNumInput',
            editor: {
                type: 'combobox',
                options: {
                    url: 'planBatchnum/listQuery/planCode',
                    method: 'GET',
                    panelHeight: 300,
                    textField: 'batchNum',
                    valueField: 'batchNum',
                    onLoadSuccess: function (data) {
                        let fieldValue = $(this).combobox('options').valueField;
                        if (data instanceof Array && data.length > 0 && !$(this).combobox('getValue')) {
                            $(this).combobox('setValue', data[0][fieldValue]);
                        }
                    }
                }
            }
        },
        /*{
            title: '原料规格',
            field: 'materialSpecification',
            editor: {
                type: 'textbox',
                options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    cls: 'input-blue',
                    panelHeight: 200,
                    required: true,
                }
            }
        },*/
        {
            title: '件数',
            field: 'boxAmount',
            id: 'boxAmountField',
            editor: {type: 'numberbox', options: {precision: 0, required: false, readonly: true}}
        },
        {
            title: '坯布规格',
            field: 'greyfabricSpecification',
            editor: {
                type: 'textbox',
                options: {
                    cls: 'input-blue',
                    width: 514,
                    required: true,
                }
            }
        },
        {
            title: '匹数',
            field: 'inputPairs',
            id: 'inputPairsField',
            editor: {type: 'numberbox', options: {precision: 0, required: true}}
        },
        {
            title: '公斤',
            field: 'inputKilo',
            id: 'inputKiloId',
            editor: {type: 'numberbox', options: {precision: 1, required: true}}
        },
        {
            title: '单价',
            field: 'processingCost',
            id: 'processingCostId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: '金额',
            field: 'money',
            id: 'moneyId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: 'A/B',
            field: 'abSurface',
            id: 'abSurfaceId',
            editor: {
                type: 'combobox', options: {
                    buttonIcon: 'icon-add',
                    onClickButton: function () {
                        let _value = $('#abSurfaceId').combobox('getValue');
                        if (_value) {
                            switch (_value) {
                                case 'A':
                                    $('#abSurfaceId').combobox('setValue', 'B');
                                    break;
                                case 'B':
                                    $('#abSurfaceId').combobox('clear');
                                    break;
                            }
                        } else {
                            $('#abSurfaceId').combobox('setValue', 'A');
                        }
                    },
                    data: [{id: 'A'}, {id: 'B'}],
                    panelHeight: 'auto',
                    textField: 'id',
                    valueField: 'id',
                    required: false,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '收货单位',
            field: 'consignee',
            id: 'consigneeId',
            editor: {
                type: 'combobox', options: {
                    width: 120, cls: 'input-red', readonly: true,
                    url: 'takeDeliveryAddress/listQuery',
                    method: 'GET',
                    textField: 'company',
                    valueField: 'company',
                    panelWidth: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '直发',
            width: '40px',
            field: 'isSendDirectly',
            id: 'fieldIsSendDirectly',
            editor: {
                type: 'checkbox', options: {
                    cls: 'input-red', value: true, onChange: function (checked) {
                        if (checked) {
                            let _value = $('#consigneeId').combobox('getValue');
                            $('#consigneeId').combobox({readonly: false, required: true});
                            $('#consigneeId').combobox('setValue', _value);
                        } else {
                            $('#consigneeId').combobox('clear');
                            $('#consigneeId').combobox({readonly: true, required: false});
                        }
                    }
                }
            }
        },
        {
            title: '经手人', field: 'handlerName', editor: {
                type: 'combobox', options: {
                    required: false,
                    method: 'GET',
                    url: 'employee/listQuery',
                    queryParams: {state: 1},
                    textField: 'empName',
                    valueField: 'empName'
                }
            }
        },
        {
            title: '制单人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    cls: 'input-blue',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {width: 514,}}},
    ];
    $.formDialog.open({
        mode: '新增',
        columns: columnA,
        loadData: loadData,
        width: 656,
        height: 460,
        onOpen: function () {
            $('#inputKiloId').numberbox('textbox').bind('keyup', function (e) {
                let price = $('#processingCostId').numberbox('getValue') || 0;
                let _value = $(this).val();
                $('#moneyId').numberbox('setValue', price * _value);
            })
            $('#processingCostId').numberbox('textbox').bind('keyup', function (e) {
                let qty = $('#inputKiloId').numberbox('getValue') || 0;
                let _value = $(this).val();
                $('#moneyId').numberbox('setValue', qty * _value);
            });
            // $('.form-dialog-edit').find('.form-row:visible').eq(0).css({'margin-top': '10px'});
        },
        buttons: [{
            text: '作废',
            iconCls: 'icon-cancel',
            handler: e => {
                let param = $.formDialog.getForm().serializeObject();
                if (param.id) {
                    cancelByPk(param.id, function (data) {
                        if (data.success) {
                            $.formDialog.close();
                            btn_search();
                        } else {
                            messagerAlert(data.message);
                        }
                    })
                }
            }
        }, {
            text: '保存',
            iconCls: 'icon-save',
            handler: e => {
                let dialog = $('.form-dialog-edit');
                if (!$(dialog.find('form')).form('validate')) {
                    return;
                }
                let param = $(dialog.find('form')).serializeObject();
                let url = 'productOutwardOrder/insertSelective';
                //  保存
                $.messager.progress({
                    title: '',
                    msg: '请稍等...',
                    interval: 5000
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: param,
                    success: function (data) {
                        if (data.success) {
                            $.formDialog.close();
                            btn_search();
                            $.messager.show({title: '提示', msg: data.message});
                        } else {
                            $.messager.alert('提示', data.message, 'warning');
                        }
                    },
                    complete: function () {
                        $.messager.progress('close');
                    }
                })
            }
        }, {
            text: '退出',
            iconCls: 'icon-no',
            handler: e => {
                $.formDialog.close();
            }
        }],
    });
}

function btn_modify(loadData) {
    function load(ele, data) {
        $(ele).form('load', data);
    }

    let columnB = [
        {
            title: '主键',
            field: 'id',
            hidden: true,
        },
        {
            title: '单据日期',
            field: 'orderDate',
            editor: {type: 'datebox', options: {required: true, value: getCurrentDate()}}
        },
        {
            title: '单据编号',
            field: 'orderCode',
            editor: {
                type: 'textbox',
                options: {
                    readonly: true
                }
            }
        },
        {
            title: '加工户', field: 'processCompany', id: 'processCompanyInput', editor: {
                type: 'combobox', options: {
                    required: true,
                    url: 'processCompany/listQuery',
                    method: 'GET',
                    textField: 'companyName',
                    valueField: 'companyName',
                    onLoadSuccess: function () {
                    }
                }
            }
        },
        {
            title: '加工类型',
            field: 'isFinishedProduct',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    data: [{id: 0, text: '未剖幅'}, {id: 1, text: '已剖幅'}],
                    limitToList: true,
                    value: 1,
                    required: true,
                    textField: 'text',
                    valueField: 'id',
                    panelHeight: 'auto',
                    onChange: function (newVal, oldVal) {
                        if (newVal) {
                            $('#boxAmountField').numberbox('clear');
                            $('#boxAmountField').numberbox({required: false, readonly: true});
                            $('#inputPairsField').numberbox({readonly: false, required: true});
                            $('#abSurfaceId').combobox('readonly', false);
                            $('#fieldIsSendDirectly').checkbox('enable');
                        } else {
                            $('#abSurfaceId').combobox('clear');
                            $('#abSurfaceId').combobox('readonly', true);
                            $('#consigneeId').combobox('clear');
                            $('#fieldIsSendDirectly').checkbox('disable');
                            $('#inputPairsField').numberbox('clear');
                            $('#inputPairsField').numberbox({required: false, readonly: true});
                            $('#boxAmountField').numberbox({readonly: false, required: true});
                        }
                    }
                }
            }
        },
        {
            title: '批次号',
            field: 'batchNum',
            id: 'batchNumInput',
            editor: {
                type: 'combobox',
                options: {
                    url: 'planBatchnum/listQuery/planCode',
                    method: 'GET',
                    panelHeight: 300,
                    textField: 'batchNum',
                    valueField: 'batchNum',
                    onLoadSuccess: function (data) {
                        let fieldValue = $(this).combobox('options').valueField;
                        if (data instanceof Array && data.length > 0 && !$(this).combobox('getValue')) {
                            $(this).combobox('setValue', data[0][fieldValue]);
                        }
                    }
                }
            }
        },
        {
            title: '客户', id: 'supplierNameInput', field: 'clientName', editor: {
                type: 'textbox', options: {}
            }
        },
        /*{
            title: '供应商', field: 'supplierName', editor: {
                type: 'combobox', options: {
                    url: 'supplier/listQuery',
                    method: 'GET',
                    textField: 'supplierName',
                    valueField: 'supplierName',
                    cls: 'input-blue',
                    panelHeight: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },*/
        {
            title: '计划单号',
            field: 'planCode',
            id: 'planCodeInput',
            editor: {
                type: 'textbox',
                options: {
                    icons: [{
                        iconCls: 'icon-remove', handler: function (e) {
                            $(e.data.target).textbox('clear');
                        }
                    }, {
                        iconCls: 'icon-add', handler: function (e) {
                            let div = $(`<div>
                                        <div id="t_dialog" 
                                             data-options="title:'白坯计划',width:730,height:500,resizable:true,
                                                           modal:true,iconCls:'icon-search',
                                                           onClose:function(){$(this).dialog('destroy')},
                                                           /*
                                                           buttons:[{text:'取消',iconCls:'icon-cancel',handler:function(){
                                                                        $('#t_dialog').dialog('close');
                                                                    }},
                                                                    {text:'保存',iconCls:'icon-save',handler:function(){
                                                                        let _selected = $('#t_plancode').datagrid('getSelected');
                                                                        if(_selected != undefined){
                                                                               delete _selected['createUserId'];
                                                                                 let specification = [
                                                                                    _selected['orderCode'],
                                                                                    _selected['productName'],
                                                                                    _selected['greyfabricWidth'] + '*' + _selected['greyfabricWeight'],
                                                                                    _selected['mHeight'],
                                                                                    _selected['greyfabricSpecification'],
                                                                                    _selected['materialSpecification'],
                                                                                ];
                                                                                _selected['greyfabricSpecification'] = specification.join(' ');
                                                                               $('.form-dialog-edit form').eq(0).form('load',_selected);
                                                                               $('#batchNumInput').combobox('options').queryParams =  {planCode: _selected['planCode']};
                                                                               $('#batchNumInput').combobox('reload');
                                                                               $('#t_dialog').dialog('close');
                                                                        } else {
                                                                          $.messager.alert('提示','未选中!','warning')  
                                                                        }
                                                                    }}]
                                                                    */
                                                           " 
                                             class="easyui-dialog">
                                            <table id="t_plancode"></table>
                                        </div>
                                    </div>`);
                            $.parser.parse(div);
                            $('#t_plancode').datagrid({
                                url: 'plan/listQuery',
                                queryParams: {state: 1, markFinished: false},
                                method: 'GET',
                                striped: true,
                                fit: true,
                                sortable: true,
                                columns: [[
                                    {
                                        title: '计划日期',
                                        hidden: true,
                                        field: 'planDate',
                                        width: 120,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '计划单号', field: 'planCode', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '门幅',
                                        field: 'greyfabricWidth',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '克重', field: 'lowWeight', width: 60, align: 'center', halign: 'center'},
                                    {
                                        title: '头份',
                                        field: 'greyfabricSpecification',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '毛高', field: 'mHeight', width: 120, align: 'center', halign: 'center'},
                                    {title: '品名', field: 'productName', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '原料规格',
                                        field: 'materialSpecification',
                                        width: 100,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                ]],
                                rownumbers: true,
                                remoteSort: false,
                                showFooter: true,
                                singleSelect: true,
                                onBeforeLoad: function () {
                                },
                                onLoadSuccess: function () {
                                },
                                onDblClickRow: function (index, row) {
                                    delete row['createUserId'];
                                    let specification = [
                                        row['orderCode'],
                                        row['productName'],
                                        row['greyfabricWidth'] + '*' + row['greyfabricWeight'],
                                        row['mHeight'],
                                        row['greyfabricSpecification'],
                                        row['materialSpecification'],
                                    ];
                                    row['greyfabricSpecification'] = specification.join(' ');
                                    row['greyfabricSpecification'] = specification.join(' ');
                                    load($('.form-dialog-edit form').eq(0), row);
                                    $('#batchNumInput').combobox('options').queryParams = {planCode: row['planCode']};
                                    $('#batchNumInput').combobox('reload');
                                    $('#t_dialog').dialog('close');
                                }
                            });
                        }
                    }],
                    required: true,
                }
            }
        },
        /*{
            title: '原料规格',
            field: 'materialSpecification',
            editor: {
                type: 'textbox',
                options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    cls: 'input-blue',
                    panelHeight: 200,
                    required: true,
                }
            }
        },*/
        {
            title: '件数',
            field: 'boxAmount',
            id: 'boxAmountField',
            editor: {type: 'numberbox', options: {precision: 0, required: false, readonly: true}}
        },
        {
            title: '坯布规格',
            field: 'greyfabricSpecification',
            editor: {
                type: 'textbox',
                options: {
                    cls: 'input-blue',
                    width: 514,
                    required: true,
                }
            }
        },
        {
            title: '匹数',
            field: 'inputPairs',
            id: 'inputPairsField',
            editor: {type: 'numberbox', options: {precision: 0, required: true}}
        },
        {
            title: '公斤',
            field: 'inputKilo',
            id: 'inputKiloId',
            editor: {type: 'numberbox', options: {precision: 1, required: true}}
        },
        {
            title: '单价',
            field: 'processingCost',
            id: 'processingCostId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: '金额',
            field: 'money',
            id: 'moneyId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: 'A/B',
            field: 'abSurface',
            id: 'abSurfaceId',
            editor: {
                type: 'combobox', options: {
                    buttonIcon: 'icon-add',
                    onClickButton: function () {
                        let _value = $('#abSurfaceId').combobox('getValue');
                        if (_value) {
                            switch (_value) {
                                case 'A':
                                    $('#abSurfaceId').combobox('setValue', 'B');
                                    break;
                                case 'B':
                                    $('#abSurfaceId').combobox('clear');
                                    break;
                            }
                        } else {
                            $('#abSurfaceId').combobox('setValue', 'A');
                        }
                    },
                    data: [{id: 'A'}, {id: 'B'}],
                    panelHeight: 'auto',
                    textField: 'id',
                    valueField: 'id',
                    required: false,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '收货单位',
            field: 'consignee',
            id: 'consigneeId',
            editor: {
                type: 'combobox', options: {
                    width: 120, cls: 'input-red', readonly: true,
                    url: 'takeDeliveryAddress/listQuery',
                    method: 'GET',
                    textField: 'company',
                    valueField: 'company',
                    panelWidth: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '直发',
            width: '40px',
            field: 'isSendDirectly',
            id: 'fieldIsSendDirectly',
            editor: {
                type: 'checkbox', options: {
                    cls: 'input-red', value: true, onChange: function (checked) {
                        if (checked) {
                            let _value = $('#consigneeId').combobox('getValue');
                            $('#consigneeId').combobox({readonly: false, required: true});
                            $('#consigneeId').combobox('setValue', _value);
                        } else {
                            $('#consigneeId').combobox('clear');
                            $('#consigneeId').combobox({readonly: true, required: false});
                        }
                    }
                }
            }
        },
        {
            title: '经手人', field: 'handlerName', editor: {
                type: 'combobox', options: {
                    required: false,
                    method: 'GET',
                    url: 'employee/listQuery',
                    queryParams: {state: 1},
                    textField: 'empName',
                    valueField: 'empName'
                }
            }
        },
        {
            title: '制单人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    cls: 'input-blue',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {width: 514,}}},
    ];
    $.formDialog.open({
        mode: '修改',
        columns: columnB,
        loadData: loadData,
        width: 656,
        height: 460,
        onOpen: function () {
            $('#inputKiloId').numberbox('textbox').bind('keyup', function (e) {
                let price = $('#processingCostId').numberbox('getValue') || 0;
                let _value = $(this).val();
                $('#moneyId').numberbox('setValue', price * _value);
            })
            $('#processingCostId').numberbox('textbox').bind('keyup', function (e) {
                let qty = $('#inputKiloId').numberbox('getValue') || 0;
                let _value = $(this).val();
                $('#moneyId').numberbox('setValue', qty * _value);
            });
            // $('.form-dialog-edit').find('.form-row:visible').eq(0).css({'margin-top': '10px'});
        },
        buttons: [{
            text: '作废',
            iconCls: 'icon-cancel',
            handler: e => {
                let param = $.formDialog.getForm().serializeObject();
                if (param.id) {
                    cancelByPk(param.id, function (data) {
                        if (data.success) {
                            $.formDialog.close();
                            btn_search();
                        } else {
                            messagerAlert(data.message);
                        }
                    })
                }
            }
        }, {
            text: '保存',
            iconCls: 'icon-save',
            handler: e => {
                let dialog = $('.form-dialog-edit');
                if (!$(dialog.find('form')).form('validate')) {
                    return;
                }
                let param = $(dialog.find('form')).serializeObject();
                let url = 'productOutwardOrder/updateSelective';
                //  保存
                $.messager.progress({
                    title: '',
                    msg: '请稍等...',
                    interval: 5000
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: param,
                    success: function (data) {
                        if (data.success) {
                            $.formDialog.close();
                            btn_search();
                            $.messager.show({title: '提示', msg: data.message});
                        } else {
                            $.messager.alert('提示', data.message, 'warning');
                        }
                    },
                    complete: function () {
                        $.messager.progress('close');
                    }
                })
            }
        }, {
            text: '退出',
            iconCls: 'icon-no',
            handler: e => {
                $.formDialog.close();
            }
        }],
    });
}

function openDialog(opts) {
    $.formDialog.open(opts);
}

function getCurrentIndex() {
    let tab = $('#tabs').tabs('getSelected');
    let index = $('#tabs').tabs('getTabIndex', tab);
    return index;
}

function btn_print() {
    let reportlets = [];
    $.get('customize/getDatabase', {}, data => {
        if (data.success) {
            switch (getCurrentIndex()) {
                case 0:
                    reportlets.push({
                        reportlet: `${data.data}/发货序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 1:
                    reportlets.push({
                        reportlet: `${data.data}/白坯入库序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 2:
                    reportlets.push({
                        reportlet: `${data.data}/白坯库存序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 3:
                    if (!$('#contactCompanyId').combobox('getValue')) {
                        $.messager.alert('提示', '请选择客户再打印！', 'warning');
                        return;
                    }
                    reportlets.push({
                        reportlet: `${data.data}/应收款序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 4:
                    if (!$('#contactCompanyId').combobox('getValue')) {
                        $.messager.alert('提示', '请选择客户再打印！', 'warning');
                        return;
                    }
                    reportlets.push({
                        reportlet: `${data.data}/已收款序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
            }
        }
    }, 'json');
}

function btn_open_column_list() {
    let kendoGridID = '';
    switch (getCurrentIndex()) {
        case 0:
            kendoGridID = '#v_grid';
            break;
        case 1:
            kendoGridID = '#v_grid_1';
            break;
        case 2:
            kendoGridID = '#v_grid_2';
            break;
        case 3:
            kendoGridID = '#v_grid_3';
            break;
        case 4:
            kendoGridID = '#v_grid_4';
            break;
    }
    let options = $(kendoGridID).data('kendoGrid').getOptions().columns;
    if ($('#btn_open_column_list').length <= 0) {
        let div = $(`<div data-options="resizable:true,iconCls:'icon-text_columns',width:200,height:300,modal:true,title:'列',
                                         onClose:function(){$(this).dialog('destroy')}   " 
                          class="easyui-dialog" id="btn_open_column_list">
                   </div>`);
        let form = $('<form style="width: 100%;height: 100%;"></form>')
        for (let i of options) {
            if (i.field) {
                let checkbox = $(`<div style="width: 100%;height: auto;padding: 5px;">
                <input class="easyui-checkbox" name="${i.field}" data-options="label:'${i.title}',
                labelWidth:120,value:true,name:'${i.field}'">
                </div>`);
                form.append(checkbox);
            }
        }
        let container = $(`<div></div>`);
        form.appendTo(div);
        div.appendTo(container);
        $('body').append(container);
        $.parser.parse($('#btn_open_column_list').parent());
        let formData = {};
        options.map(item => {
            formData[item['field']] = item['hidden'] == undefined ? true : !item['hidden'];
        })
        $('#btn_open_column_list form').eq(0).form('load', formData);
        $('#btn_open_column_list form .easyui-checkbox').each(function () {
            //  绑定事件
            $(this).checkbox({
                onChange: function (checked) {
                    let opts = $(this).checkbox('options');
                    if (checked) {
                        $(kendoGridID).data('kendoGrid').showColumn(opts.name);
                    } else {
                        $(kendoGridID).data('kendoGrid').hideColumn(opts.name);
                    }
                }
            })
        })
    } else {
        $('#btn_open_column_list').dialog('open');
    }
}

function btn_export_excel() {
    switch (getCurrentIndex()) {
        case 0:
            $('#v_grid').data('kendoGrid').saveAsExcel();
            break;
        case 1:
            $('#v_grid_1').data('kendoGrid').saveAsExcel();
            break;
        case 2:
            $('#v_grid_2').data('kendoGrid').saveAsExcel();
            break;
        case 3:
            $('#v_grid_3').data('kendoGrid').saveAsExcel();
            break;
        case 4:
            $('#v_grid_4').data('kendoGrid').saveAsExcel();
            break;
    }
}

function stateChange(checked) {
    if (!checked && getCurrentIndex() == 3) {
        $('#contactCompanyId').combobox('clear');
        $('#contactCompanyId').combobox('disable');
        $('#start').datebox('disable');
        $('#end').datebox('disable');
    } else {
        $('#start').datebox('enable');
        $('#end').datebox('enable');
        $('#contactCompanyId').combobox('enable');
    }
    btn_search();
}

//  消审
function btn_cancel_audit() {
    let kGrid = getCurrentSelectedGrid();
    if (kGrid == undefined) return;
    let selected = kGrid.select();
    if (selected == undefined) {
        messagerAlert('请选中！');
        return;
    }
    let dataItems = [];
    selected.each(function () {
        let dataItem = kGrid.dataItem(this);
        if (dataItem.id) {
            dataItems.push(dataItem.id);
        }
    });
    if (dataItems.length <= 0) {
        messagerAlert('请选中！');
        return;
    }
    messagerConfirm('确定消审？', r => {
        if (r) {
            $.ajax({
                url: 'productOutwardOrder/cancelAudit',
                data: {id: dataItems},
                type: 'POST',
                dataType: 'json',
                traditional: true,
            }).then(function (data) {
                if (data.success) {
                    btn_search();
                } else {
                    messagerAlert(data.message);
                }
            })
        }
    })
}

function getCurrentSelectedGrid() {
    switch (getCurrentIndex()) {
        case 0:
            return $('#v_grid').data('kendoGrid')
        case 1:
            return $('#v_grid_1').data('kendoGrid');
    }
}

function cancelByPk(id, fn) {
    messagerConfirm('是否作废,作废l后不可恢复?', r => {
        if (r) {
            $.ajax({
                url: 'productOutwardOrder/cancelByPk',
                data: {id: id},
                type: 'POST',
                dataType: 'json',
                success: fn
            })
        }
    })
}

function btn_close() {
    window.parent.closeTab();
}

//选中事件
function initCombobox(options) {
    var columns = options.columns;
    columns.splice(0, 1);
    $('#column').combobox({
        valueField: 'field',
        textField: 'title',
        data: columns,
        editable: false
    })
}