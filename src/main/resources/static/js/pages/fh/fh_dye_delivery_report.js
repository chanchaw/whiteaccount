$(function () {
    let buttonGroup = [
        {
            id: 'btn_add',
            size: 'small',
            text: '色布入库',
            iconCls: 'icon-add',
            plain: true,
            disabled: true,
            onClick: btn_add
        },
        {
            id: 'btn_export',
            size: 'small',
            text: '导出Excel',
            iconCls: 'icon-page_excel',
            plain: true,
            onClick: btn_export_excel
        },
        {
            id: 'btn_print',
            size: 'small',
            text: '打印',
            iconCls: 'icon-print',
            plain: true,
            onClick: btn_print
        },
        {
            id: 'btn_grid_columns',
            size: 'small',
            text: '列',
            iconCls: 'icon-text_columns',
            plain: true,
            onClick: btn_open_column_list
        },
        {
            id: 'btn_cancel_audit',
            size: 'small',
            disabled: true,
            text: '消审',
            iconCls: 'icon-exclamation',
            onClick: btn_cancel_audit,
            plain: true
        },
        {
            id: 'btn_close', size: 'small', text: '关闭', iconCls: 'icon-no', onClick: btn_close,
            plain: true
        },
    ];
    buttonGen($('#north'), buttonGroup);
    bindEnterEvents();
    setBtnState(false);
    let kGrid = $('#v_grid').data('kendoGrid');
    var options = kGrid.getOptions();
    if (options.columns.length > 0)
        initCombobox(options)
})

function setBtnState(b) {
    if (b) {
        $('#btn_add').linkbutton('enable');
        $('#btn_modify').linkbutton('enable');
    } else {
        $('#btn_add').linkbutton('disable');
        $('#btn_modify').linkbutton('disable');
    }
}

//  选中事件
function onSelect(title, index) {
    function state(f) {
        if (!f && index == 3) {
            $('#contactCompanyId').combobox('clear');
            $('#contactCompanyId').combobox('disable');
            $('#start').datebox('disable');
            $('#end').datebox('disable');
        } else {
            $('#start').datebox('enable');
            $('#end').datebox('enable');
            $('#contactCompanyId').combobox('enable');
        }
    }

    switch (index) {
        case 0:
            if ($('#v_grid').data('kendoGrid') == undefined) {
                grid_1();
            }
            break;
        case 1:
            if ($('#v_grid_1').data('kendoGrid') == undefined) {
                grid_2();
            }
            break;
        case 2:
            if ($('#v_grid_2').data('kendoGrid') == undefined) {
                grid_3();
            }
            break;
        case 3:
            if ($('#v_grid_3').data('kendoGrid') == undefined) {
                grid_4();
            }
            break;
        case 4:
            if ($('#v_grid_4').data('kendoGrid') == undefined) {
                grid_5();
            }
            break;
    }
    if (index == 1) {
        setBtnState(true);
    } else {
        setBtnState(false);
    }
    if (index >= 2) {
        $('#state').switchbutton('enable');
    } else {
        $('#state').switchbutton('disable');
    }
    state($('#state').switchbutton('options').checked)
}

/**
 * 2021年8月11日 11:06:32 chanchaw
 * 成品管理 - 第一个标签页 - 色布发货
 */
function grid_1() {
    let SUFFIX = "_a";
    let client = new kendo.data.DataSource({data: []});
    let address = new kendo.data.DataSource({data: []});
    let specification = new kendo.data.DataSource({data: []});
    let billcode = new kendo.data.DataSource({data: []});
    let checkout = new kendo.data.DataSource({data: []});
    let is_send_directly = new kendo.data.DataSource({data: []});
    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "dyeFabricDelivery/report/fhdtl",
                dataType: "json",
                type: "GET",
                data: {
                    start: $('#start').datebox('getValue'),
                    end: $('#end').datebox('getValue'),
                    clientName: $('#contactCompanyId').combobox('getValue'),
                    // checkType: $('#type').combobox('getValue')
                    // textField: $('#column').combobox('getValue'),
                    // valueField: $('#value').textbox('getValue'),
                }
            }
        },
        schema: {
            model: {
                id: 'id',
                fields: {
                    ps: {type: "number"},
                    qty: {type: "number"},
                    money: {type: "number"},
                    countable: {type: "number"},
                    net_qty: {type: "number"},
                    ms: {type: "number"},
                    datafh: {type: "date"},
                }
            }
        },
        aggregate: [
            {field: "ps", aggregate: "sum"},
            {field: "qty", aggregate: "sum"},
            {field: "money", aggregate: "sum"},
            {field: "countable", aggregate: "sum"},
            {field: "ms", aggregate: "sum"},
            {field: "net_qty", aggregate: "sum"},
        ],
        change: function (e) {
            let data = this.data();
            client.data(unique(data, 'client'));
            address.data(unique(data, 'address'));
            specification.data(unique(data, 'specification'));
            billcode.data(unique(data, 'billcode'));
            checkout.data(unique(data, 'checkout'));
            is_send_directly.data(unique(data, 'is_send_directly'));

            $('#btn_cancel_audit').linkbutton('disable')
        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })
        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })
        }
    });
    let columns = [
        {field: "no", title: "#", filterable: false, template: "<span class='row_number'></span>", width: 40},
        {
            field: "id",
            title: '编号',
            width: 93,
            hidden: true,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
        },
        {
            field: "client",
            title: '客户',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "specification",
            title: '品名规格',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "ps",
            title: '匹数',
            width: 93,
            template: item => item['ps'] ? parseFloat(item['ps']).toFixed(0) : '',
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "price_standard_name",
            title: '计价标准',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
        },
        {
            field: "qty",
            title: '毛重',
            width: 93,
            template: item => item['qty'] ? parseFloat(item['qty']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "net_qty",
            title: '净重',
            width: 93,
            template: item => item['net_qty'] ? parseFloat(item['net_qty']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "ms",
            title: '米数',
            width: 93,
            template: item => item['ms'] ? parseFloat(item['ms']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "countable",
            title: '件数',
            width: 93,
            template: item => item['countable'] ? parseFloat(item['countable']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "price",
            title: '单价',
            width: 93,
            template: item => item['price'] ? parseFloat(item['price']).toFixed(2) : '',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "money",
            title: '金额',
            width: 93,
            template: item => item['money'] ? parseFloat(item['money']).toFixed(2) : '',
            footerAttributes: {'style': 'text-align:right;'},
            footerTemplate: '#=kendo.format("{0:n2}",sum)#',
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "is_send_directly",
            title: '直发',
            width: 93,
            template: item => item['is_send_directly'] == '是' ? `<input type="checkbox" checked onclick="return false;">` : '',
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "checkout",
            title: '结算',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "billcode",
            title: '发货单号',
            width: 93,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "datafh",
            title: '发货时间',
            width: 85,
            format: "{0: MM-dd HH:mm}",
            headerAttributes: {'style': 'text-align:center;'},
            attributes: {'class': 'kendo-custum-char'},
            filterable: false
        },
    ];
    let grid = $("#v_grid").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "色布发货序时表.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            let kGrid = $('#v_grid').data('kendoGrid');
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
                let dataItem = kGrid.dataItem($(this));
                if (dataItem['is_audit'] != undefined && !dataItem['is_audit']) {
                    $(this).addClass('kendo-custum-non-checked');
                }
            });
        },
        columnHide: function (e) {
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
        },
        columnShow: function (e) {
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null, SUFFIX);
        },
        columnResize: function (e) {
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null, SUFFIX);
            }
        },
        columnReorder: function (e) {
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field'] + SUFFIX
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid01.setOptions({
        columns: columns
    });
    grid.on('dblclick', 'tr', function (e) {
        //  修改单据
        btn_modify();
    })
    grid.on('click', 'tr', function (e) {
        let kGrid = $("#v_grid").data("kendoGrid");
        let dataItem = kGrid.dataItem(e.currentTarget);
        if (dataItem['is_audit']) {
            $('#btn_cancel_audit').linkbutton('enable')
        } else {
            $('#btn_cancel_audit').linkbutton('disable')
        }
    })
}

/**
 * 2021年8月11日 11:06:32 chanchaw
 * 成品管理 - 第二个标签页 - 色布入库
 */
function grid_2() {
    let SUFFIX = "_b";
    let client_name = new kendo.data.DataSource({data: []});
    let plan_code = new kendo.data.DataSource({data: []});
    let product_name = new kendo.data.DataSource({data: []});
    let material_specification = new kendo.data.DataSource({data: []});
    let color_no = new kendo.data.DataSource({data: []});
    let color = new kendo.data.DataSource({data: []});
    let supplier = new kendo.data.DataSource({data: []});
    let price_standard_name = new kendo.data.DataSource({data: []});
    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "dyeFabricDelivery/report/dyeFabric/input",
                dataType: "json",
                type: "GET",
                data: {
                    start: $('#start').datebox('getValue'),
                    end: $('#end').datebox('getValue'),
                    clientName: $('#contactCompanyId').combobox('getValue'),
                    // textField: $('#column').combobox('getValue'),
                    // valueField: $('#value').textbox('getValue'),
                }
            }
        },
        schema: {
            model: {
                fields: {
                    bill_date: {type: "date"},
                    ps: {type: "number"},
                    qty: {type: "number"},
                    net_qty: {type: "number"},
                    ms: {type: "number"},
                    price: {type: "number"},
                    money: {type: "number"},
                }
            }
        },
        aggregate: [
            {field: "ps", aggregate: "sum"},
            {field: "qty", aggregate: "sum"},
            {field: "net_qty", aggregate: "sum"},
            {field: "ms", aggregate: "sum"},
            {field: "money", aggregate: "sum"},
        ],
        change: function (e) {
            let data = this.data();
            client_name.data(unique(data, 'client_name'));
            plan_code.data(unique(data, 'plan_code'));
            product_name.data(unique(data, 'product_name'));
            material_specification.data(unique(data, 'material_specification'));
            color_no.data(unique(data, 'color_no'));
            color.data(unique(data, 'color'));
            supplier.data(unique(data, 'supplier'));
            price_standard_name.data(unique(data, 'price_standard_name'));

            $('#btn_cancel_audit').linkbutton('disable')
        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })

        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })

        }
    });
    let columns = [
        {field: "no", title: "#", filterable: false, template: "<span class='row_number'></span>", width: 40},
        {
            field: "id",
            title: '编号',
            width: 93,
            hidden: true,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
        },
        {
            field: "client_name",
            title: '客户',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: {checkAll: true, multi: true, search: true, dataSource: client_name}
        },
        {
            field: "plan_code",
            title: '计划单号',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: {checkAll: true, multi: true, search: true, dataSource: plan_code}
        },
        {
            field: "product_name",
            title: '品名',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: {checkAll: true, multi: true, search: true, dataSource: product_name}
        },
        {
            field: "material_specification",
            title: '规格',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: {checkAll: true, multi: true, search: true, dataSource: material_specification}
        },
        {
            field: "color_no",
            title: '色号',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: {checkAll: true, multi: true, search: true, dataSource: color_no}
        },
        {
            field: "color",
            title: '颜色',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: {checkAll: true, multi: true, search: true, dataSource: color}
        },
        {
            field: "ps",
            title: '匹数',
            width: 93,
            template: item => item['ps'] ? parseFloat(item['ps']).toFixed(0) : '',
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "qty",
            title: '毛重',
            width: 93,
            template: item => item['qty'] ? parseFloat(item['qty']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "net_qty",
            title: '净重',
            width: 93,
            template: item => item['net_qty'] ? parseFloat(item['net_qty']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "ms",
            title: '米数',
            width: 93,
            template: item => item['ms'] ? parseFloat(item['ms']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "price",
            title: '单价',
            width: 93,
            template: item => item['price'] ? parseFloat(item['price']).toFixed(2) : '',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "money",
            title: '金额',
            width: 93,
            template: item => item['money'] ? parseFloat(item['money']).toFixed(2) : '',
            footerTemplate: '#=kendo.format("{0:n2}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },

        {
            field: "bill_date",
            title: '入库时间',
            width: 85,
            format: "{0: MM-dd HH:mm}",
            headerAttributes: {'style': 'text-align:center;'},
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            filterable: false
        },
        {
            field: "supplier",
            title: '供货商',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: {checkAll: true, multi: true, search: true, dataSource: supplier}
        },
        {
            field: "price_standard_name",
            title: '计价标准',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: {checkAll: true, multi: true, search: true, dataSource: price_standard_name}
        },
        {
            field: "remarks",
            title: '备注',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "is_send_directly",
            title: '直发',
            width: 93,
            template: item => item['is_send_directly'] == '是' ? `<input type="checkbox" checked onclick="return false;">` : '',
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
    ];
    let grid = $("#v_grid_1").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "色布入库序时表.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            let kGrid = $('#v_grid_1').data('kendoGrid');
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
                let dataItem = kGrid.dataItem($(this));
                if (dataItem['is_audit'] != undefined && !dataItem['is_audit']) {
                    $(this).addClass('kendo-custum-non-checked');
                }
            });
        },
        columnHide: function (e) {
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
        },
        columnShow: function (e) {
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null, SUFFIX);
        },
        columnResize: function (e) {
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null, SUFFIX);
            }
        },
        columnReorder: function (e) {
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field'] + SUFFIX
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid_1').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid01.setOptions({
        columns: columns
    });
    grid.on('dblclick', 'tr', function (e) {
        //  修改单据
        btn_modify();
    })
    grid.on('click', 'tr', function (e) {
        let kGrid = $("#v_grid_1").data("kendoGrid");
        let dataItem = kGrid.dataItem(e.currentTarget);
        if (dataItem['is_audit']) {
            $('#btn_cancel_audit').linkbutton('enable')
        } else {
            $('#btn_cancel_audit').linkbutton('disable')
        }
    })
}

/**
 * 2021年8月11日 11:06:32 chanchaw
 * 成品管理 - 第三个标签页 - 色布库存
 */
function grid_3() {
    let SUFFIX = "_c";
    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "dyeFabricDelivery/report/dyeFabric/storage",
                dataType: "json",
                type: "GET",
                data: {
                    start: $('#start').datebox('getValue'),
                    end: $('#end').datebox('getValue'),
                    clientName: $('#contactCompanyId').combobox('getValue') || '',
                    value: $('#state').switchbutton('options').checked ? 0 : 1
                }
            }
        },
        schema: {
            model: {
                // id: 'id',
                fields: {
                    ps: {type: "number"},
                    qty: {type: "number"},
                }
            }
        },
        aggregate: [
            {field: "ps", aggregate: "sum"},
            {field: "qty", aggregate: "sum"},
            {field: "net_qty", aggregate: "sum"},
            {field: "meter_number", aggregate: "sum"},
        ],
        change: function (e) {
            // 打印表格数据
            // let ret = [];
            // let kGrid = $("#v_grid_2").data("kendoGrid");
            // let rows = $('#v_grid_2').data('kendoGrid').dataSource.data();
            //
            // console.log(rows);

            // for (let i = 0; i < rows.length; i++) {
            //     ret.push(kGrid.dataItem($(this)));
            // }
            // console.log('色布库存表格数据：',ret);

            // selected.each(function () {
            //     let dataItem = kGrid.dataItem(this);
            //     if (dataItem.id) {
            //         dataItems.push(dataItem.id);
            //     }
            // });


            let data = this.data();
        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })

        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })

        }
    });
    // 第三个标签页 - 色布库存表格列定义
    let columns = [
        {field: "no", title: "#", filterable: false, template: "<span class='row_number'></span>", width: 40},
        {
            field: "client_name",
            title: '客户',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "plan_code",
            title: '计划单号',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "product_name",
            title: '品名',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "material_specification",
            title: '规格',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "color_no",
            title: '色号',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "color",
            title: '颜色',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {   // price_standard_name
            field: "price_standard_name",
            title: '计价标准',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            //filterable: {checkAll: true, multi: true, search: true, dataSource: price_standard_name}
        },
        {
            field: "ps",
            title: '匹数',
            width: 93,
            template: item => item['ps'] ? parseFloat(item['ps']).toFixed(0) : '',
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "qty",
            title: '毛重',
            width: 93,
            template: item => item['qty'] ? parseFloat(item['qty']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "net_qty",
            title: '净重',
            width: 93,
            template: item => item['net_qty'] ? parseFloat(item['net_qty']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        },
        {
            field: "meter_number",
            title: '米数',
            width: 93,
            template: item => item['meter_number'] ? parseFloat(item['meter_number']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {'style': 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {'style': 'text-align:center;'},
            filterable: false
        }
    ];
    let grid = $("#v_grid_2").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "色布库存.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
            });
        },
        columnHide: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
        },
        columnShow: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null, SUFFIX);
        },
        columnResize: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null, SUFFIX);
            }
        },
        columnReorder: function (e) {
            if (!$('#state').switchbutton('options').checked) return;
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field'] + SUFFIX
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid_2').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid01.setOptions({
        columns: columns
    });
}

function btn_cancel_filter() {
    switch (getCurrentIndex()) {
        case 0:
            $('#v_grid').data('kendoGrid').dataSource.filter({});
            break;
        case 1:
            $('#v_grid_1').data('kendoGrid').dataSource.filter({});
            break;
        case 2:
            $('#v_grid_2').data('kendoGrid').dataSource.filter({});
            break;
        case 3:
            $('#v_grid_3').data('kendoGrid').dataSource.filter({});
            break;
        case 4:
            $('#v_grid_4').data('kendoGrid').dataSource.filter({});
            break;
    }
    $('#v_grid').data('kendoGrid').dataSource.filter({});
}

function btn_search() {
    function setCol(ele, show = []) {
        let kGrid = ele.data('kendoGrid');
        let columns = kGrid.columns;
        let checked = $('#state').switchbutton('options').checked;
        if (!checked)
            for (let col of columns) {
                if (col['field'] && show.indexOf(col['field']) == -1) {
                    kGrid.hideColumn(col);
                } else if (col['field']) {
                    kGrid.showColumn(col);
                }
            }
        else {
            for (let col of columns) {
                if (col['title'] && col['title'].indexOf('编号') > -1)
                    kGrid.hideColumn(col);
                else
                    kGrid.showColumn(col);
            }
        }
    }

    switch (getCurrentIndex()) {
        case 0:
            $('#v_grid').data('kendoGrid').dataSource.read({
                start: $('#start').datebox('getValue'),
                end: $('#end').datebox('getValue'),
                clientName: $('#contactCompanyId').combobox('getValue'),
                // checkType: $('#type').combobox('getValue')
                // textField: $('#column').combobox('getValue'),
                // valueField: $('#value').textbox('getValue'),
            });
            break;
        case 1:
            $('#v_grid_1').data('kendoGrid').dataSource.read({
                start: $('#start').datebox('getValue'),
                end: $('#end').datebox('getValue'),
                clientName: $('#contactCompanyId').combobox('getValue'),
                // textField: $('#column').combobox('getValue'),
                // valueField: $('#value').textbox('getValue'),
            });
            break;
        case 2:
            $('#v_grid_2').data('kendoGrid').dataSource.read({
                start: $('#start').datebox('getValue'),
                end: $('#end').datebox('getValue'),
                clientName: $('#contactCompanyId').combobox('getValue'),
                value: $('#state').switchbutton('options').checked ? 0 : 1
            });
            setCol($('#v_grid_2'), ['no', 'client_name', 'ps', 'qty']);
            break;
        case 3:
            $('#v_grid_3').data('kendoGrid').dataSource.read({
                start: $('#start').datebox('getValue'),
                end: $('#end').datebox('getValue'),
                clientName: $('#contactCompanyId').combobox('getValue'),
                // checkType: $('#type').combobox('getValue'),
                value: $('#state').switchbutton('options').checked ? 0 : 1
            });
            setCol($('#v_grid_3'), ['no', 'client', 'money']);
            break;
        case 4:
            $('#v_grid_4').data('kendoGrid').dataSource.read({
                start: $('#start').datebox('getValue'),
                end: $('#end').datebox('getValue'),
                clientName: $('#contactCompanyId').combobox('getValue'),
                value: $('#state').switchbutton('options').checked ? 0 : 1
            });
            setCol($('#v_grid_4'), ['no', 'client', 'money']);
            break;
    }
}

function btn_add() {
    let columns = [
        {field: 'id', title: '主键', hidden: true,},
        {
            field: 'orderDate',
            title: '单据日期',
            editor: {type: 'datebox', options: {required: true, value: getCurrentDate()}}
        },
        {
            field: 'orderCode',
            title: '单据编号',
            editor: {type: 'textbox', options: {required: false, readonly: true}}
        },
        {
            field: 'dyeCompany',
            title: '供货单位',
            editor: {
                type: 'combobox', options: {
                    url: 'takeDeliveryAddress/listQuery',
                    method: 'GET', queryParams: {state: 1},
                    textField: 'company', valueField: 'company',
                    panelHeight: 200,
                    required: true,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                        if (!$(this).combobox('getValue')) {
                            let loadData = $(this).combobox('getData') || [];
                            let opts = $(this).combobox('options');
                            if (loadData.length > 0) {
                                $(this).combobox('setValue', loadData[0][opts.valueField]);
                            }
                        }
                    }
                }
            }
        },
        {
            field: 'dyePlanCode',
            title: '计划单号',
            editor: {
                type: 'textbox', options: {
                    icons: [{
                        iconCls: 'icon-remove', handler: function (e) {
                            $(e.data.target).textbox('clear');
                        }
                    }, {
                        iconCls: 'icon-add', handler: function (e) {
                            let div = $(`<div>
                                        <div id="t_dialog" 
                                             data-options="title:'色布计划',width:730,height:500,resizable:true,
                                                           modal:true,iconCls:'icon-search',
                                                           onClose:function(){$(this).dialog('destroy')}" 
                                             class="easyui-dialog">
                                            <table id="t_plancode"></table>
                                        </div>
                                    </div>`);
                            $.parser.parse(div);
                            $('#t_plancode').datagrid({
                                url: 'dyeFabricPlan/listQuery',
                                queryParams: {state: 1, markFinished: false},
                                method: 'GET',
                                striped: true,
                                fit: true,
                                sortable: true,
                                columns: [[
                                    {
                                        title: '计划日期',
                                        hidden: true,
                                        field: 'planDate',
                                        width: 120,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '计划单号', field: 'planCode', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '订单号',
                                        field: 'orderCode',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '品名', field: 'productName', width: 60, align: 'center', halign: 'center'},
                                    {
                                        title: '类型',
                                        field: 'processType',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {
                                        title: '规格',
                                        field: 'materialSpecification',
                                        width: 120,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '色号/花号', field: 'colorNo', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '颜色/花型',
                                        field: 'color',
                                        width: 100,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                ]],
                                rownumbers: true,
                                remoteSort: false,
                                showFooter: true,
                                singleSelect: true,
                                onBeforeLoad: function () {
                                },
                                onLoadSuccess: function () {
                                },
                                onDblClickRow: function (index, row) {
                                    let specification = [
                                        row['productName'],
                                        row['materialSpecification'],
                                        row['color'],
                                        row['colorNo'],
                                    ];
                                    $('.form-dialog-edit form').eq(0).form('load', {
                                        specification: specification.join(','),
                                        dyePlanCode: row['planCode'],
                                        processType: row['processType'],
                                        contactCompanyId: row['contactCompanyId'],
                                        priceStandard: row['priceStandard'],
                                        netDiff: row['netDiff'],
                                        rate: row['rate'],
                                        price: row['materialPrice'] || 0,
                                    })
                                    $('#t_dialog').dialog('close');
                                }
                            });
                        }
                    }],
                    required: true,
                    editable: false
                }
            }
        },
        {
            field: 'processType',
            title: '加工类型',
            editor: {
                type: 'combobox', options: {
                    url: 'processType/listQuery',
                    method: 'GET',
                    textField: 'typeName',
                    valueField: 'typeName',
                    cls: 'input-blue',
                    panelHeight: 200,
                    required: true,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                        if (!$(this).combobox('getValue')) {
                            $(this).combobox('setValue', '染色');
                        }
                    }
                }
            }
        },
        {
            field: 'contactCompanyId',
            title: '客户',
            editor: {
                type: 'combobox', options: {
                    width: 115,
                    url: 'contactCompany/listQuery',
                    method: 'GET', queryParams: {state: 1},
                    textField: 'companyName', valueField: 'id',
                    panelHeight: 200,
                    required: true,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                        if (!$(this).combobox('getValue')) {
                            let loadData = $(this).combobox('getData') || [];
                            let opts = $(this).combobox('options');
                            if (loadData.length > 0) {
                                $(this).combobox('setValue', loadData[0][opts.valueField]);
                            }
                        }
                    }
                }
            }
        },
        {
            title: '直发',
            width: '50px',
            field: 'isSendDirectly',
            editor: {
                type: 'checkbox', options: {
                    cls: 'input-red', value: true
                }
            }
        },
        {
            title: '毛&nbsp;&nbsp;净&nbsp;&nbsp;差',
            field: 'netDiff',
            id: 'netDiffInput',
            editor: {type: 'numberbox', options: {cls: 'input-red', precision: 1, readonly: true}}
        },
        {
            title: '米克系数',
            field: 'rate',
            id: 'rateInput',
            editor: {
                type: 'numberbox',
                options: {cls: 'input-red', precision: 1, value: 1, readonly: true}
            }
        },
        {
            title: '色布规格',
            field: 'specification',
            editor: {type: 'textbox', options: {cls: 'input-blue', required: false, readonly: true, width: 355}}
        },
        {
            title: '计价',
            width: '85px',
            field: 'priceStandard',
            editor: {
                type: 'combobox',
                options: {
                    width: 85,
                    url: 'dyeFabricPlan/list/pricestandard',
                    method: 'GET',
                    textField: 'key',
                    valueField: 'value',
                    panelHeight: 'auto',
                    readonly: true,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                        let opts = $(this).combobox('options');
                        let allData = $(this).combobox('getData') || [];
                        if (allData.length > 0) {
                            $(this).combobox('setValue', allData[0][opts.valueField]);
                        }
                    },
                    onChange: function (newVal, oldVal) {
                        if (!newVal) return;
                        modifyMoneyGenerateLogic(newVal,
                            $('#netDiffInput'), $('#rateInput'), $('#inputPairsInput'),
                            $('#inputKiloInput'), $('#netKiloInput'), $('#inputMeterInput'),
                            $('#priceInput'), $('#moneyInput'));
                        switch (newVal) {
                            case 1:
                            case 2:
                            case 4:
                                $('#rateInput').numberbox('clear');
                                $('#rateInput').numberbox({editable: false, required: false});
                                $('#meterCounterEditor').val(0);
                                break;
                            case 3:
                                $('#rateInput').numberbox({editable: true, required: true});
                                $('#meterCounterEditor').val(1);
                                break;
                        }
                    }
                }
            }
        },
        {
            field: 'inputPairs',
            id: 'inputPairsInput',
            title: '匹&nbsp;&nbsp;&nbsp;&nbsp;数',
            editor: {type: 'numberbox', options: {precision: 0, required: true,}}
        },
        {
            field: 'inputKilo',
            id: 'inputKiloInput',
            title: '毛&nbsp;&nbsp;&nbsp;&nbsp;重',
            editor: {type: 'numberbox', options: {precision: 1, required: true,}}
        },
        {
            field: 'netKilo',
            id: 'netKiloInput',
            title: '净&nbsp;&nbsp;&nbsp;&nbsp;重',
            editor: {type: 'numberbox', options: {precision: 1, required: false,}}
        },
        {
            field: 'inputMeter',
            id: 'inputMeterInput',
            title: '米&nbsp;&nbsp;&nbsp;&nbsp;数',
            editor: {type: 'numberbox', options: {precision: 1, required: false,}}
        },
        {
            field: 'price',
            id: 'priceInput',
            title: '单&nbsp;&nbsp;&nbsp;&nbsp;价',
            editor: {type: 'numberbox', options: {precision: 2, required: false,}}
        },
        {
            field: 'money',
            id: 'moneyInput',
            title: '金&nbsp;&nbsp;&nbsp;&nbsp;额',
            editor: {type: 'numberbox', options: {precision: 2, required: false,}}
        },
        {
            field: 'handlerName',
            title: '经&nbsp;&nbsp;手&nbsp;&nbsp;人',
            editor: {type: 'textbox', options: {}}
        },
        {
            title: '制&nbsp;&nbsp;单&nbsp;&nbsp;人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    cls: 'input-blue',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            field: 'remarks',
            title: '备&nbsp;&nbsp;&nbsp;&nbsp;注',
            editor: {type: 'textbox', options: {width: 514}}
        }
    ];
    let loadData = {};
    openDialog({
        loadData: loadData,
        columns: columns,
        height: 460,
        width: 667,
        mode: '色布入库',
        onOpen: function () {
        },
        buttons: [
            {
                text: '作废', iconCls: 'icon-cancel', handler: e => {
                    btn_cancel();
                }
            }, {
                text: '保存', iconCls: 'icon-save', handler: e => {
                    let dialog = $('.form-dialog-edit');
                    if (!$(dialog.find('form')).form('validate')) {
                        return;
                    }
                    let param = $(dialog.find('form')).serializeObject();
                    let url = 'dyeFabricInbound/insertSelective';
                    $.messager.progress({
                        title: '',
                        msg: '请稍等...',
                        interval: 5000
                    });
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        data: param,
                        success: function (data) {
                            if (data.success) {
                                btn_search();
                                $.formDialog.close();
                                $.messager.show({title: '提示', msg: data.message});
                            } else {
                                $.messager.alert('提示', data.message, 'warning');
                            }
                        },
                        complete: function () {
                            $.messager.progress('close');
                        }
                    })
                }
            }, {
                text: '取消', iconCls: 'icon-cancel', handler: e => {
                    $.formDialog.close();
                }
            }]
    });
    $.formDialog.disableBtn(['作废'])
}

function btn_modify() {
    let kGrid = getSelectedGrid();
    let dataItem = kGrid.dataItem(kGrid.select());
    if (dataItem == undefined || !dataItem['id']) {
        $.messager.alert('提示', '请选中色布入库', 'warning');
        return;
    }
    let columns = [
        {field: 'id', title: '主键', hidden: true,},
        {
            field: 'orderDate',
            title: '单据日期',
            editor: {type: 'datebox', options: {required: true, value: getCurrentDate()}}
        },
        {
            field: 'orderCode',
            title: '单据编号',
            editor: {type: 'textbox', options: {required: false, readonly: true}}
        },
        {
            field: 'dyeCompany',
            title: '收货单位',
            editor: {
                type: 'combobox', options: {
                    url: 'takeDeliveryAddress/listQuery',
                    method: 'GET', queryParams: {state: 1},
                    textField: 'company', valueField: 'company',
                    panelHeight: 200,
                    required: true,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                        if (!$(this).combobox('getValue')) {
                            let loadData = $(this).combobox('getData') || [];
                            let opts = $(this).combobox('options');
                            if (loadData.length > 0) {
                                $(this).combobox('setValue', loadData[0][opts.valueField]);
                            }
                        }
                    }
                }
            }
        },
        {
            field: 'dyePlanCode',
            title: '计划单号',
            editor: {
                type: 'textbox', options: {
                    icons: [{
                        iconCls: 'icon-remove', handler: function (e) {
                            $(e.data.target).textbox('clear');
                        }
                    }, {
                        iconCls: 'icon-add', handler: function (e) {
                            let div = $(`<div>
                                        <div id="t_dialog" 
                                             data-options="title:'色布计划',width:730,height:500,resizable:true,
                                                           modal:true,iconCls:'icon-search',
                                                           onClose:function(){$(this).dialog('destroy')}" 
                                             class="easyui-dialog">
                                            <table id="t_plancode"></table>
                                        </div>
                                    </div>`);
                            $.parser.parse(div);
                            $('#t_plancode').datagrid({
                                url: 'dyeFabricPlan/listQuery',
                                queryParams: {state: 1, markFinished: false},
                                method: 'GET',
                                striped: true,
                                fit: true,
                                sortable: true,
                                columns: [[
                                    {
                                        title: '计划日期',
                                        hidden: true,
                                        field: 'planDate',
                                        width: 120,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '计划单号', field: 'planCode', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '订单号',
                                        field: 'orderCode',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '品名', field: 'productName', width: 60, align: 'center', halign: 'center'},
                                    {
                                        title: '类型',
                                        field: 'processType',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {
                                        title: '规格',
                                        field: 'materialSpecification',
                                        width: 120,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '色号/花号', field: 'colorNo', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '颜色/花型',
                                        field: 'color',
                                        width: 100,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                ]],
                                rownumbers: true,
                                remoteSort: false,
                                showFooter: true,
                                singleSelect: true,
                                onBeforeLoad: function () {
                                },
                                onLoadSuccess: function () {
                                },
                                onDblClickRow: function (index, row) {
                                    let specification = [
                                        row['productName'],
                                        row['materialSpecification'],
                                        row['color'],
                                        row['colorNo'],
                                    ];
                                    $('.form-dialog-edit form').eq(0).form('load', {
                                        specification: specification.join(','),
                                        dyePlanCode: row['planCode'],
                                        processType: row['processType'],
                                        contactCompanyId: row['contactCompanyId'],
                                        priceStandard: row['priceStandard'],
                                        netDiff: row['netDiff'],
                                        rate: row['rate'],
                                        price: row['materialPrice'] || 0,
                                    })
                                    $('#t_dialog').dialog('close');
                                }
                            });
                        }
                    }],
                    required: true,
                    editable: false
                }
            }
        },
        {
            field: 'processType',
            title: '加工类型',
            editor: {
                type: 'combobox', options: {
                    url: 'processType/listQuery',
                    method: 'GET',
                    textField: 'typeName',
                    valueField: 'typeName',
                    cls: 'input-blue',
                    panelHeight: 200,
                    required: true,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                        if (!$(this).combobox('getValue')) {
                            $(this).combobox('setValue', '染色');
                        }
                    }
                }
            }
        },
        {
            field: 'contactCompanyId',
            title: '客户',
            editor: {
                type: 'combobox', options: {
                    width: 115,
                    url: 'contactCompany/listQuery',
                    method: 'GET', queryParams: {state: 1},
                    textField: 'companyName', valueField: 'id',
                    panelHeight: 200,
                    required: true,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                        if (!$(this).combobox('getValue')) {
                            let loadData = $(this).combobox('getData') || [];
                            let opts = $(this).combobox('options');
                            if (loadData.length > 0) {
                                $(this).combobox('setValue', loadData[0][opts.valueField]);
                            }
                        }
                    }
                }
            }
        },
        {
            title: '直发',
            width: '50px',
            field: 'isSendDirectly',
            editor: {
                type: 'checkbox', options: {
                    cls: 'input-red', value: true
                }
            }
        },
        {
            title: '毛&nbsp;&nbsp;净&nbsp;&nbsp;差',
            field: 'netDiff',
            id: 'netDiffInput',
            editor: {type: 'numberbox', options: {cls: 'input-red', precision: 1, readonly: true}}
        },
        {
            title: '米克系数',
            field: 'rate',
            id: 'rateInput',
            editor: {
                type: 'numberbox',
                options: {cls: 'input-red', precision: 1, value: 1, editable: false}
            }
        },
        {
            title: '色布规格',
            field: 'specification',
            editor: {
                type: 'textbox',
                options: {cls: 'input-blue', required: false, readonly: true, width: 355}
            }
        },
        {
            title: '计价',
            width: '85px',
            field: 'priceStandard',
            editor: {
                type: 'combobox',
                options: {
                    width: 85,
                    url: 'dyeFabricPlan/list/pricestandard',
                    method: 'GET',
                    value: 1,
                    textField: 'key',
                    valueField: 'value',
                    panelHeight: 'auto',
                    readonly: true,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    },
                    onChange: function (newVal, oldVal) {
                        if (!newVal) return;
                        modifyMoneyGenerateLogic(newVal,
                            $('#netDiffInput'), $('#rateInput'), $('#inputPairsInput'),
                            $('#inputKiloInput'), $('#netKiloInput'), $('#inputMeterInput'),
                            $('#priceInput'), $('#moneyInput'));
                        switch (newVal) {
                            case 1:
                            case 2:
                            case 4:
                                $('#rateInput').numberbox('clear');
                                $('#rateInput').numberbox({editable: false, required: false});
                                $('#meterCounterEditor').val(0);
                                break;
                            case 3:
                                $('#rateInput').numberbox({editable: true, required: true});
                                $('#meterCounterEditor').val(1);
                                break;
                        }
                    }
                }
            }
        },
        {
            field: 'inputPairs',
            id: 'inputPairsInput',
            title: '匹&nbsp;&nbsp;&nbsp;&nbsp;数',
            editor: {type: 'numberbox', options: {precision: 0, required: true,}}
        },
        {
            field: 'inputKilo',
            id: 'inputKiloInput',
            title: '毛&nbsp;&nbsp;&nbsp;&nbsp;重',
            editor: {type: 'numberbox', options: {precision: 1, required: true,}}
        },
        {
            field: 'netKilo',
            id: 'netKiloInput',
            title: '净&nbsp;&nbsp;&nbsp;&nbsp;重',
            editor: {type: 'numberbox', options: {precision: 1, required: false,}}
        },
        {
            field: 'inputMeter',
            id: 'inputMeterInput',
            title: '米&nbsp;&nbsp;&nbsp;&nbsp;数',
            editor: {type: 'numberbox', options: {precision: 1, required: false,}}
        },
        {
            field: 'price',
            id: 'priceInput',
            title: '单&nbsp;&nbsp;&nbsp;&nbsp;价',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            field: 'money',
            id: 'moneyInput',
            title: '金&nbsp;&nbsp;&nbsp;&nbsp;额',
            editor: {type: 'numberbox', options: {precision: 2, required: false,}}
        },
        {
            field: 'handlerName',
            title: '经&nbsp;&nbsp;手&nbsp;&nbsp;人',
            editor: {type: 'textbox', options: {}}
        },
        {
            title: '制&nbsp;&nbsp;单&nbsp;&nbsp;人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    cls: 'input-blue',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            field: 'remarks',
            title: '备&nbsp;&nbsp;&nbsp;&nbsp;注',
            editor: {type: 'textbox', options: {width: 514}}
        }
    ];
    $.ajax({
        url: 'dyeFabricInbound/queryByPk/' + dataItem.id,
        data: {},
        type: 'GET',
        dataType: 'json',
    }).then(function (data) {
        if (!data['state']) {
            $.messager.alert('提示', '已作废！', 'warning');
            return;
        }
        openDialog({
            loadData: data,
            columns: columns,
            height: 460,
            width: 667,
            onOpen: function () {
                //  毛重
                $('#inputKiloInput').textbox('textbox').bind('keyup', function (e) {
                    let _value = $(e.currentTarget).val() || 0;
                    let rate = $('#rateInput').numberbox('getValue') || 0;
                    let netDiff = $('#netDiffInput').numberbox('getValue') || 0;
                    let pairs = $('#inputPairsInput').numberbox('getValue') || 0;
                    let price = $('#priceInput').numberbox('getValue') || 0;
                    $('#inputMeterInput').numberbox('setValue', _value * rate);
                    $('#netKiloInput').numberbox('setValue', _value - (pairs * netDiff));
                    $('#moneyInput').numberbox('setValue', $('#netKiloInput').numberbox('getValue') * price);
                });
                //  匹数
                $('#inputPairsInput').textbox('textbox').bind('keyup', function (e) {
                    let _value = $(e.currentTarget).val() || 0;
                    let inputKilo = $('#inputKiloInput').numberbox('getValue') || 0;
                    let netDiff = $('#netDiffInput').numberbox('getValue') || 0;
                    $('#netKiloInput').numberbox('setValue', inputKilo - (_value * netDiff));
                });
                //  米克系数
                $('#rateInput').textbox('textbox').bind('keyup', function (e) {
                    let keyCode = e.keyCode;
                    let _value = $(e.currentTarget).val() || 0;
                    let inputKilo = $('#inputKiloInput').numberbox('getValue') || 0;
                    $('#inputMeterInput').numberbox('setValue', inputKilo * _value);
                });
                //  毛净差
                $('#netDiffInput').textbox('textbox').bind('keyup', function (e) {
                    let _value = $(e.currentTarget).val() || 0;
                    let inputKilo = $('#inputKiloInput').numberbox('getValue') || 0;
                    let inputPairs = $('#inputPairsInput').numberbox('getValue') || 0;
                    let price = $('#priceInput').numberbox('getValue') || 0;
                    $('#netKiloInput').numberbox('setValue', inputKilo - (inputPairs * _value));
                    $('#moneyInput').numberbox('setValue', $('#netKiloInput').numberbox('getValue') * price);
                });
                //  单价
                $('#priceInput').textbox('textbox').bind('keyup', function (e) {
                    let _value = $(e.currentTarget).val() || 0;
                    let netKilo = $('#netKiloInput').numberbox('getValue') || 0;
                    $('#moneyInput').numberbox('setValue', netKilo * _value);
                });
            },
            mode: '色布入库修改',
            buttons: [{
                text: '作废', iconCls: 'icon-cancel', handler: e => {
                    btn_cancel();
                }
            }, {
                text: '保存', iconCls: 'icon-save', handler: e => {
                    let dialog = $('.form-dialog-edit');
                    if (!$(dialog.find('form')).form('validate')) {
                        return;
                    }
                    let param = $(dialog.find('form')).serializeObject();
                    let url = 'dyeFabricInbound/updateSelective';
                    //  修改
                    $.messager.progress({
                        title: '',
                        msg: '请稍等...',
                        interval: 5000
                    });
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        data: param,
                        success: function (data) {
                            if (data.success) {
                                btn_search();
                                $.formDialog.close();
                                $.messager.show({title: '提示', msg: data.message});
                            } else {
                                $.messager.alert('提示', data.message, 'warning');
                            }
                        },
                        complete: function () {
                            $.messager.progress('close');
                        }
                    })
                }
            }, {
                text: '取消', iconCls: 'icon-no', handler: e => {
                    $.formDialog.close();
                }
            }]
        });
    }, function () {
    })
}

/**
 *
 * @param priceStandard         计价标准
 * @param netDiffEle            毛净差
 * @param rateEle               米克系数
 * @param inputPairsEle         匹数
 * @param inputKiloEle          公斤(毛重)
 * @param netKiloEle            净重
 * @param inputMeterEle         米数
 * @param priceEle              单价
 * @param moneyEle              金额
 */
function modifyMoneyGenerateLogic(priceStandard, netDiffEle, rateEle, inputPairsEle, inputKiloEle, netKiloEle, inputMeterEle, priceEle, moneyEle) {
    if (priceStandard) {
        netDiffEle.textbox('textbox').unbind('keyup');
        rateEle.textbox('textbox').unbind('keyup');
        inputPairsEle.textbox('textbox').unbind('keyup');
        inputKiloEle.textbox('textbox').unbind('keyup');
        netKiloEle.textbox('textbox').unbind('keyup');
        inputMeterEle.textbox('textbox').unbind('keyup');
        priceEle.textbox('textbox').unbind('keyup');
        moneyEle.textbox('textbox').unbind('keyup');
    }
    switch (priceStandard) {
        case 1:
        case 3:
            //  毛重
            inputKiloEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let rate = rateEle.numberbox('getValue') || 0;
                let netDiff = netDiffEle.numberbox('getValue') || 0;
                let pairs = inputPairsEle.numberbox('getValue') || 0;
                let price = priceEle.numberbox('getValue') || 0;
                inputMeterEle.numberbox('setValue', _value * rate);
                netKiloEle.numberbox('setValue', _value - (pairs * netDiff));
                moneyEle.numberbox('setValue', _value * price);
            });
            //  匹数
            inputPairsEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let inputKilo = inputKiloEle.numberbox('getValue') || 0;
                let netDiff = netDiffEle.numberbox('getValue') || 0;
                netKiloEle.numberbox('setValue', inputKilo - (_value * netDiff));
            });
            //  毛净差
            netDiffEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let inputKilo = inputKiloEle.numberbox('getValue') || 0;
                let inputPairs = inputPairsEle.numberbox('getValue') || 0;
                netKiloEle.numberbox('setValue', inputKilo - (inputPairs * _value));
            });
            //  单价
            priceEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let kilo = inputKiloEle.numberbox('getValue') || 0;
                moneyEle.numberbox('setValue', kilo * _value);
            });
            moneyEle.numberbox('setValue', (inputKiloEle.numberbox('getValue') || 0) * (priceEle.numberbox('getValue') || 0));
            break;
        case 2:
            //  毛重
            inputKiloEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let rate = rateEle.numberbox('getValue') || 0;
                let netDiff = netDiffEle.numberbox('getValue') || 0;
                let pairs = inputPairsEle.numberbox('getValue') || 0;
                let price = priceEle.numberbox('getValue') || 0;
                inputMeterEle.numberbox('setValue', _value * rate);
                netKiloEle.numberbox('setValue', _value - (pairs * netDiff));
                moneyEle.numberbox('setValue', netKiloEle.numberbox('getValue') * price);
            });
            //  匹数
            inputPairsEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let inputKilo = inputKiloEle.numberbox('getValue') || 0;
                let netDiff = netDiffEle.numberbox('getValue') || 0;
                netKiloEle.numberbox('setValue', inputKilo - (_value * netDiff));
            });
            //  毛净差
            netDiffEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let inputKilo = inputKiloEle.numberbox('getValue') || 0;
                let inputPairs = inputPairsEle.numberbox('getValue') || 0;
                let price = priceEle.numberbox('getValue') || 0;
                netKiloEle.numberbox('setValue', inputKilo - (inputPairs * _value));
                moneyEle.numberbox('setValue', netKiloEle.numberbox('getValue') * price);
            });
            //  单价
            priceEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let netKilo = netKiloEle.numberbox('getValue') || 0;
                moneyEle.numberbox('setValue', netKilo * _value);
            });
            //  净重
            netKiloEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let price = priceEle.numberbox('getValue') || 0;
                moneyEle.numberbox('setValue', price * _value);
            });
            moneyEle.numberbox('setValue', (netKiloEle.numberbox('getValue') || 0) * (priceEle.numberbox('getValue') || 0));
            break;
        case 4:
            //  毛重
            inputKiloEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let rate = rateEle.numberbox('getValue') || 0;
                let netDiff = netDiffEle.numberbox('getValue') || 0;
                let pairs = inputPairsEle.numberbox('getValue') || 0;
                let price = priceEle.numberbox('getValue') || 0;
                inputMeterEle.numberbox('setValue', _value * rate);
                netKiloEle.numberbox('setValue', _value - (pairs * netDiff));
                moneyEle.numberbox('setValue', _value * rate * price);
            });
            //  匹数
            inputPairsEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let inputKilo = inputKiloEle.numberbox('getValue') || 0;
                let netDiff = netDiffEle.numberbox('getValue') || 0;
                netKiloEle.numberbox('setValue', inputKilo - (_value * netDiff));
            });
            //  毛净差
            netDiffEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let inputKilo = inputKiloEle.numberbox('getValue') || 0;
                let inputPairs = inputPairsEle.numberbox('getValue') || 0;
                netKiloEle.numberbox('setValue', inputKilo - (inputPairs * _value));
            });
            //  米数
            inputMeterEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let price = priceEle.numberbox('getValue') || 0;
                moneyEle.numberbox('setValue', price * _value);
            });
            //  单价
            priceEle.textbox('textbox').bind('keyup', function (e) {
                let _value = $(e.currentTarget).val() || 0;
                let meter = inputMeterEle.numberbox('getValue') || 0;
                moneyEle.numberbox('setValue', meter * _value);
            });
            moneyEle.numberbox('setValue', (inputMeterEle.numberbox('getValue') || 0) * (priceEle.numberbox('getValue') || 0));
            break;
    }
}

function btn_cancel() {
    //  本操作只能在第一个或者第二个标签页中使用
    if (getCurrentIndex() > 1) {
        return;
    }
    let selected = getSelectedGrid().dataItem(getSelectedGrid().select());
    if (selected == undefined || !selected.id) {
        messagerAlert('请选中色布入库单据');
        return;
    }
    messagerConfirm('是否作废？', r => {
        if (r) {
            $.post('dyeFabricInbound/cancelByPk/' + selected.id, {}, function (data) {
                if (data.success) {
                    btn_search();
                    $.formDialog.close();
                } else {
                    messagerAlert(data.message);
                }
            }, 'json')
        }
    })
}

function openDialog(opts) {
    $.formDialog.open(opts);
}

function getCurrentIndex() {
    let tab = $('#tabs').tabs('getSelected');
    let index = $('#tabs').tabs('getTabIndex', tab);
    return index;
}

function getSelectedGrid() {
    let kendoGridID = '';
    switch (getCurrentIndex()) {
        case 0:
            kendoGridID = '#v_grid';
            break;
        case 1:
            kendoGridID = '#v_grid_1';
            break;
        case 2:
            kendoGridID = '#v_grid_2';
            break;
    }
    return $(kendoGridID).data('kendoGrid');
}

function btn_print() {
    let reportlets = [];
    $.get('customize/getDatabase', {}, data => {
        if (data.success) {
            switch (getCurrentIndex()) {
                case 0:
                    reportlets.push({
                        reportlet: `${data.data}/色布发货序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 1:
                    reportlets.push({
                        reportlet: `${data.data}/色布库存序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 2:
                    reportlets.push({
                        reportlet: `${data.data}/色布库存序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
            }
        }
    }, 'json');
}

function btn_open_column_list() {
    let kendoGridID = '';
    switch (getCurrentIndex()) {
        case 0:
            kendoGridID = '#v_grid';
            break;
        case 1:
            kendoGridID = '#v_grid_1';
            break;
        case 2:
            kendoGridID = '#v_grid_2';
            break;
        case 3:
            kendoGridID = '#v_grid_3';
            break;
        case 4:
            kendoGridID = '#v_grid_4';
            break;
    }
    let options = $(kendoGridID).data('kendoGrid').getOptions().columns;
    if ($('#btn_open_column_list').length <= 0) {
        let div = $(`<div data-options="resizable:true,iconCls:'icon-text_columns',width:200,height:300,modal:true,title:'列',
                                         onClose:function(){$(this).dialog('destroy')}   " 
                          class="easyui-dialog" id="btn_open_column_list">
                   </div>`);
        let form = $('<form style="width: 100%;height: 100%;"></form>')
        for (let i of options) {
            if (i.field) {
                let checkbox = $(`<div style="width: 100%;height: auto;padding: 5px;">
                <input class="easyui-checkbox" name="${i.field}" data-options="label:'${i.title}',
                labelWidth:120,value:true,name:'${i.field}'">
                </div>`);
                form.append(checkbox);
            }
        }
        let container = $(`<div></div>`);
        form.appendTo(div);
        div.appendTo(container);
        $('body').append(container);
        $.parser.parse($('#btn_open_column_list').parent());
        let formData = {};
        options.map(item => {
            formData[item['field']] = item['hidden'] == undefined ? true : !item['hidden'];
        })
        $('#btn_open_column_list form').eq(0).form('load', formData);
        $('#btn_open_column_list form .easyui-checkbox').each(function () {
            //  绑定事件
            $(this).checkbox({
                onChange: function (checked) {
                    let opts = $(this).checkbox('options');
                    if (checked) {
                        $(kendoGridID).data('kendoGrid').showColumn(opts.name);
                    } else {
                        $(kendoGridID).data('kendoGrid').hideColumn(opts.name);
                    }
                }
            })
        })
    } else {
        $('#btn_open_column_list').dialog('open');
    }
}

function btn_export_excel() {
    switch (getCurrentIndex()) {
        case 0:
            $('#v_grid').data('kendoGrid').saveAsExcel();
            break;
        case 1:
            $('#v_grid_1').data('kendoGrid').saveAsExcel();
            break;
        case 2:
            $('#v_grid_2').data('kendoGrid').saveAsExcel();
            break;
        case 3:
            $('#v_grid_3').data('kendoGrid').saveAsExcel();
            break;
        case 4:
            $('#v_grid_4').data('kendoGrid').saveAsExcel();
            break;
    }
}

function stateChange(checked) {
    if (!checked && getCurrentIndex() == 3) {
        $('#contactCompanyId').combobox('clear');
        $('#contactCompanyId').combobox('disable');
        $('#start').datebox('disable');
        $('#end').datebox('disable');
    } else {
        $('#start').datebox('enable');
        $('#end').datebox('enable');
        $('#contactCompanyId').combobox('enable');
    }
    btn_search();
}

//  消审
function btn_cancel_audit() {
    let kGrid = getCurrentSelectedGrid();
    let selected = kGrid.select();
    if (selected == undefined) {
        messagerAlert('请选中！');
        return;
    }
    let dataItems = [];
    selected.each(function () {
        let dataItem = kGrid.dataItem(this);
        if (dataItem.id) {
            dataItems.push(dataItem.id);
        }
    });
    if (dataItems.length <= 0) {
        messagerAlert('请选中！');
        return;
    }
    messagerConfirm('确定消审？', r => {
        if (r) {
            $.ajax({
                url: 'dyeFabricInbound/cancelAudit',
                data: {id: dataItems},
                type: 'POST',
                dataType: 'json',
                traditional: true,
            }).then(function (data) {
                if (data.success) {
                    btn_search();
                } else {
                    messagerAlert(data.message);
                }
            })
        }
    })
}

function getCurrentSelectedGrid() {
    switch (getCurrentIndex()) {
        case 0:
            return $('#v_grid').data('kendoGrid')
        case 1:
            return $('#v_grid_1').data('kendoGrid');
    }
}

function btn_close() {
    window.parent.closeTab();
}

// 打印色布库存表格数据源
function getRawData() {
    let ret = [];
    let kGrid = $("#v_grid_2").data("kendoGrid");
    let rows = $('#v_grid_2').data('kendoGrid').dataSource.data();

    $(rows).each(function () {
        ret.push(kGrid.dataItem($(this)));
    })
    console.log('色布库存表格数据：', ret);
}

//选中事件
function initCombobox(options) {
    var columns = options.columns;
    columns.splice(0, 1);
    $('#column').combobox({
        valueField: 'field',
        textField: 'title',
        data: columns,
        editable: false
    })
}