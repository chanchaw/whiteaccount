/**
 * 自动生成按钮
 * @param parentEle
 */
function generateButton(parentEle, arr = []) {
    let permissions = window.parent.getPermissions() || [];
    let menuId = $('body').data('menu_id');
    let buttonOpts = [
        {
            id: 'btn_search',
            text: '查询',
            iconCls: `icon-search`,
            onClick: btn_search
        },
        {
            id: 'btn_add',
            text: '新增',
            iconCls: `icon-add`,
            onClick: btn_add,
            disabled: permissions.indexOf(menuId + ':ADD') == -1
        },
        {
            id: 'btn_modify',
            text: '修改',
            iconCls: `icon-edit`,
            onClick: btn_modify,
            disabled: permissions.indexOf(menuId + ':MODIFY') == -1
        },
        {
            id: 'btn_save',
            text: '保存',
            iconCls: `icon-save`,
            onClick: btn_save
        },
        {
            id: 'btn_delete',
            text: '删除',
            iconCls: `icon-cancel`,
            onClick: btn_delete,
            disabled: permissions.indexOf(menuId + ':DELETE') == -1
        },
        {id: 'btn_import', text: '导入', iconCls: `icon-page_white_excel`, onClick: btn_import},
        {
            id: 'btn_export',
            text: '导出',
            iconCls: `icon-page_excel`,
            onClick: btn_export,
            disabled: permissions.indexOf(menuId + ':EXPORT') == -1
        },
        {
            id: 'btn_close', text: '关闭', iconCls: `icon-no`, onClick: function () {
                window.parent.closeTab();
            }
        },
    ];
    let newButtons = buttonOpts.concat(arr);
    for (let i of newButtons) {
        $(parentEle).append(`<a id="${i.id}" class="easyui-linkbutton" style="margin: 5px 1px 5px 5px"/>`);
        $(`#${i.id}`).linkbutton({
            text: i.text || '',
            iconCls: i.iconCls,
            onClick: i.onClick,
            plain: true,
            disabled: i['disabled'] == undefined ? false : i['disabled']
        })
    }
}

/**
 * 自动生成树按钮
 * @param parentEle
 */
function generateTreeButton(parentEle) {
    let permissions = window.parent.getPermissions() || [];
    let menuId = $('body').data('menu_id');
    let buttonOpts = [
        {
            id: 'btn_tree_add',
            text: '新增',
            iconCls: `icon-add`,
            onClick: btn_tree_add,
            disabled: permissions.indexOf(menuId + ':ADD') == -1
        },
        {
            id: 'btn_tree_edit',
            text: '修改',
            iconCls: `icon-edit`,
            onClick: btn_tree_edit,
            disabled: permissions.indexOf(menuId + ':MODIFY') == -1
        },
        {
            id: 'btn_tree_delete',
            text: '删除',
            iconCls: `icon-cancel`,
            onClick: btn_tree_delete,
            disabled: permissions.indexOf(menuId + ':DELETE') == -1
        },
    ];
    for (let i of buttonOpts) {
        $(parentEle).append(`<a id="${i.id}" style="margin: 5px 1px 5px 5px"/>`);
        $(`#${i.id}`).linkbutton({
            iconCls: i.iconCls,
            onClick: i.onClick,
            plain: true,
            disabled: i['disabled'] == undefined ? false : i['disabled']
        })
    }
}

//分页过滤
function pageFilter(data) {
    if (typeof data.length == 'number' && typeof data.splice == 'function') {
        data = {
            total: data.length,
            rows: data,
        }
    }
    let dg = $(this);
    let opts = dg.datagrid('options');
    let pager = dg.datagrid('getPager');
    pager.pagination({
        onSelectPage: function (pageNumber, pageSize) {
            opts.pageNumber = pageNumber
            opts.pageSize = pageSize
            pager.pagination('refresh', {
                pageNumber: pageNumber,
                pageSize: pageSize
            })
            dg.datagrid('loadData', data)
        }
    })
    if (!data.originalRows) {
        data.originalRows = data.rows;
    }
    let start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
    let end = start + parseInt(opts.pageSize);
    data.rows = (data.originalRows.slice(start, end));
    return data;
}