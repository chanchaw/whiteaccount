$(function () {
    //  按钮生成器
    generateButton($('#north'));
    let columns = [[
        {
            field: 'id',
            title: '编号',
            width: 100,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: true}}
        },
        {
            field: 'billTypeName',
            title: '名称',
            width: 200,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: true}}
        },
        {
            field: 'accessMode',
            title: '库存影响',
            width: 200,
            align: 'center',
            halign: 'center',
            sortable: true,
            formatter: (v, r, i) => {
                if (!v) return '';
                switch (v) {
                    case 0:
                        return '不影响';
                    case 1:
                        return '增加库存';
                    case -1:
                        return '减少库存';
                }
                return '';
            },
            editor: {
                type: 'combobox',
                options: {
                    required: true,
                    limitToList: true,
                    panelHeight: 'auto',
                    textField: 'text',
                    valueField: 'id',
                    data: [{id: 1, text: '增加库存'}, {id: -1, text: '减少库存'}, {id: 0, text: '不影响'}]
                }
            }
        },
        {
            field: 'allowManual',
            title: '是否可以自动生成',
            width: 150,
            align: 'center',
            halign: 'center',
            formatter: (v, r, i) => {
                if (v == undefined || v == '') return '可以';
                return !v ? '可以' : '不可以';
            },
            sortable: true,
            editor: {type: 'checkbox', options: {required: true, on: 0, off: 1, panelHeight: 'auto',}}
        },
        {
            field: 'storageId',
            title: '所属仓库 ',
            width: 150,
            align: 'center',
            halign: 'center',
            sortable: true,
            formatter: (v, r, i) => {
                if (!v) return '';
                switch (v) {
                    case '1':
                        return '材料仓库';
                    case '2':
                        return '成品仓库';
                    default:
                        return '';
                }
            },
            editor: {
                type: 'combobox', options: {
                    required: true,
                    data: [{id: '1', storeName: '材料仓库'}, {id: '2', storeName: '成品仓库'}],
                    panelHeight: 'auto',
                    textField: 'storeName',
                    valueField: 'id',
                    limitToList: true
                }
            }
        },
        {
            field: 'company',
            title: '类别',
            width: 100,
            align: 'center',
            halign: 'center',
            sortable: true,
            formatter: (v, r, i) => {
                if (v == undefined) return '';
                switch (v) {
                    case 1:
                        return '加工户';
                    case 0:
                        return '原料商';
                }
                return '';
            },
            editor: {
                type: 'combobox',
                options: {
                    data: [{id: -1, text: '无'}, {id: 0, text: '原料商'}, {id: 1, text: '加工户'}],
                    panelHeight: 'auto',
                    textField: 'text',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true
                    }
                }
            }
        },
        {
            field: 'inoutType',
            title: '扣库类型',
            width: 100,
            align: 'center',
            halign: 'center',
            sortable: true,
            formatter: (v, r, i) => {
                if (!v) return '';
                switch (v) {
                    case -1:
                        return '本期出库';
                    case 1:
                        return '本期入库';
                }
                return '';
            },
            editor: {
                type: 'combobox',
                options: {
                    required: true,
                    limitToList: true,
                    panelHeight: 'auto',
                    textField: 'text',
                    valueField: 'id',
                    data: [{id: 1, text: '本期入库'}, {id: -1, text: '本期出库'}]
                }
            }
        },
        {
            field: 'isSelected',
            title: '是否默认选中',
            width: 200,
            align: 'center',
            halign: 'center',
            sortable: true,
            formatter: (v, r, i) => {
                return v == undefined ? '' : v ? '是' : '否';
            },
            editor: {
                type: 'combobox', options: {
                    required: true, data: [{id: false, text: '否'}, {id: true, text: '是'}],
                    textField: 'text', valueField: 'id',
                    value: false,
                    panelHeight: 'auto',
                }
            }
        },
        {
            field: 'isCheck',
            title: '是否盘点类型',
            width: 200,
            align: 'center',
            halign: 'center',
            sortable: true,
            formatter: (v, r, i) => {
                return v == undefined ? '' : v ? '是' : '否';
            },
            editor: {
                type: 'combobox', options: {
                    required: true, data: [{id: false, text: '否'}, {id: true, text: '是'}],
                    textField: 'text', valueField: 'id',
                    value: false,
                    panelHeight: 'auto',
                }
            }
        },
        {
            field: 'isReturn',
            title: '是否退回类型',
            width: 200,
            align: 'center',
            halign: 'center',
            sortable: true,
            formatter: (v, r, i) => {
                return v == undefined ? '' : v ? '是' : '否';
            },
            editor: {
                type: 'combobox', options: {
                    required: true, data: [{id: false, text: '否'}, {id: true, text: '是'}],
                    textField: 'text', valueField: 'id',
                    value: false,
                    panelHeight: 'auto',
                }
            }
        },
    ]];
    setDatagridColumnOpts($('body').data('menu_id'), $('#e_grid'), columns, edatagrid);
    bindEnterEvents()
    $.uploadComponent({
        url: 'billType/upload',
        method: 'POST',
        fields: columns[0],
        onSuccess: function (data) {
            if (data.success) {
                $.uploadComponent('close');
                $('#e_grid').edatagrid('reload');
                $.messager.show({title: '提示', msg: data.message});
            } else {
                $(this).edatagrid('cancelRow');
                $(this).edatagrid('editRow', index);
                $.messager.alert('提示', '上传失败!' + data.message, 'error');
            }
        }
    })
})

function edatagrid(columns) {
    $('#e_grid').edatagrid({
        method: 'GET',
        param: {},
        url: 'billType/listQuery',
        saveUrl: 'billType/insertSelective',
        updateUrl: 'billType/updateSelective',
        destroyUrl: 'billType/deleteByPk',
        idField: 'id',
        destroyMsg: {
            norecord: {
                title: '警告',
                msg: '未选中任何数据'
            },
            confirm: {
                title: '确认',
                msg: '确定删除吗?删除后不可恢复!'
            }
        },
        loadFilter: pageFilter,
        pagination: true,
        // pagePosition: 'top',
        pageSize: 200,
        pageNumber: 1,
        pageList: [200, 400, 600, 800, 1000],
        striped: true,
        fit: true,
        sortable: true,
        rownumbers: true,
        remoteSort: false,
        showFooter: true,
        singleSelect: true,
        columns: columns,
        onLoadSuccess: function () {
            $(this).datagrid('enableFilter');
            $(this).data('edatagrid').options.editIndex = -1;
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
        },
        onBeforeLoad: function (param) {
            $(this).datagrid('destroyFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('disable');
                }
            })
            return true;
        },
        onSuccess: function (index, row) {
            $(this).edatagrid('reload');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
            } else {
                $(this).edatagrid('enableEditing');
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onDestroy: function (index, row) {
            $(this).edatagrid('reload');
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
            } else {
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onError: function (index, row) {
            $.messager.show({title: '提示', msg: row.message});
        },
        onResizeColumn: function (f, w) {
            columnResizeEvent($('body').data('menu_id'), f, w);
        },
    })
}

function btn_search() {
    $('#e_grid').edatagrid('reload');
}

function btn_add() {
    $('#e_grid').edatagrid('addRow');
}

function btn_modify() {
    let selected = $('#e_grid').edatagrid('getSelected');
    if (selected != undefined) {
        let index = $('#e_grid').edatagrid('getRowIndex', selected);
        if (index != -1) {
            $('#e_grid').edatagrid('editRow', index);
        }
    } else {
        $.messager.alert('提示', '点击鼠标左键选中行后再执行本功能', 'warning');
    }
}

function btn_delete() {
    let selected = $('#e_grid').edatagrid('getSelected');
    if (selected != undefined) {
        let index = $('#e_grid').edatagrid('getRowIndex', selected);
        if (index != -1) {
            $('#e_grid').edatagrid('destroyRow', index);
        }
    } else {
        $.messager.alert('提示', '点击鼠标左键选中行后再执行本功能', 'warning');
    }
}

function btn_import() {
    $.uploadComponent('open');
}

function btn_save() {
    $('#e_grid').edatagrid('saveRow');
}

function btn_export() {
    exportExcel('#e_grid', '单据类型');
}