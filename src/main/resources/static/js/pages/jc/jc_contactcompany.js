$(function () {
    //  按钮生成器
    generateButton($('#north'));
    let columns = [[
        {
            field: 'id', title: '编号', width: 60, align: 'center', halign: 'center', sortable: true, editor: {
                type: 'textbox', options: {required: true}
            }
        },
        {
            field: 'companyName', width: 100, title: '名称', align: 'center', halign: 'center', sortable: true, editor: {
                type: 'textbox', options: {required: true}
            }
        },
        {
            field: 'companyCategories', width: 50, title: '类别', align: 'center', halign: 'center', sortable: true, editor: {
                type: 'combobox', options: {
                    data: [{id: '全部', text: '全部'}, {id: '白坯', text: '白坯'}, {id: '色布', text: '色布'}],
                    value: 1,
                    panelHeight: 'auto',
                    textField: 'text',
                    valueField: 'id',
                    editable: false,
                    required: true
                }
            }
        },
        {
            field: 'companyCode', width: 100, title: '编码', align: 'center', halign: 'center', sortable: true, editor: {
                type: 'textbox', options: {required: false}
            }
        },
        {
            field: 'settlementClient',
            width: 100,
            title: '结算客户',
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {
                type: 'textbox', options: {required: false}
            }
        },
        {
            field: 'businessManager',
            width: 100,
            title: '业务员',
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {
                type: 'textbox', options: {required: false}
            }
        },
        {
            field: 'phoneNumber',
            width: 100,
            title: '电话号码',
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {
                type: 'textbox', options: {required: false}
            }
        },
        {
            field: 'area', width: 100, title: '地区', align: 'center', halign: 'center', sortable: true, editor: {
                type: 'textbox', options: {required: false}
            }
        },
        {
            field: 'address', width: 100, title: '地址', align: 'center', halign: 'center', sortable: true, editor: {
                type: 'textbox', options: {required: false}
            }
        },
        {
            field: 'postCode', width: 100, title: '邮编', align: 'center', halign: 'center', sortable: true, editor: {
                type: 'textbox', options: {required: false}
            }
        },
        {
            field: 'bankOfDeposit',
            width: 100,
            title: '开户行',
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {
                type: 'textbox', options: {required: false}
            }
        },
        {
            field: 'bankAccount',
            width: 100,
            title: '收款账号',
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {
                type: 'textbox', options: {required: false}
            }
        },
        {
            field: 'dutyParagraph',
            width: 100,
            title: '税号',
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {
                type: 'textbox', options: {required: false}
            }
        },
        {
            field: 'remarks', width: 200, title: '备注', align: 'center', halign: 'center', sortable: true, editor: {
                type: 'textbox', options: {}
            }
        },
        {
            field: 'state', width: 50, title: '状态', align: 'center', halign: 'center', sortable: true, editor: {
                type: 'combobox', options: {
                    data: [{id: 1, text: '正常'}, {id: 0, text: '停用'}],
                    value: 1,
                    panelHeight: 'auto',
                    textField: 'text',
                    valueField: 'id',
                    editable: false,
                    required: true
                }
            },
            formatter: (v, r, i) => {
                return v == 1 ? '正常' : '停用';
            }
        },
    ]];
    setDatagridColumnOpts($('body').data('menu_id'), $('#e_grid'), columns, edatagrid);
    bindEnterEvents();

    $.uploadComponent({
        url: 'contactCompany/upload',
        method: 'POST',
        fields: columns[0],
        onSuccess: function (data) {
            if (data.success) {
                $.uploadComponent('close');
                $('#e_grid').edatagrid('reload');
                $.messager.show({title: '提示', msg: data.message});
            } else {
                $(this).edatagrid('cancelRow');
                $(this).edatagrid('editRow', index);
                $.messager.alert('提示', '上传失败!' + data.message, 'error');
            }
        }
    })
})

function edatagrid(columns) {
    $('#e_grid').edatagrid({
        method: 'GET',
        param: {},
        url: 'contactCompany/listQuery',
        saveUrl: 'contactCompany/insertSelective',
        updateUrl: 'contactCompany/updateSelective',
        destroyUrl: 'contactCompany/deleteByPk',
        idField: 'id',
        destroyMsg: {
            norecord: {
                title: '警告',
                msg: '未选中任何数据'
            },
            confirm: {
                title: '确认',
                msg: '确定删除吗?删除后不可恢复!'
            }
        },
        loadFilter: pageFilter,
        pagination: true,
        columns: columns,
        // pagePosition: 'top',
        pageSize: 200,
        pageNumber: 1,
        pageList: [200, 400, 600, 800, 1000],
        striped: true,
        fit: true,
        sortable: true,
        rownumbers: true,
        remoteSort: false,
        showFooter: true,
        singleSelect: true,
        onLoadSuccess: function () {
            $(this).datagrid('enableFilter');
            $(this).data('edatagrid').options.editIndex = -1;
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
        },
        onLoadError: function () {
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
        },
        onBeforeLoad: function (param) {
            $(this).datagrid('destroyFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('disable');
                }
            })
            return true;
        },
        onSuccess: function (index, row) {
            $(this).edatagrid('reload');
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
            } else {
                $(this).edatagrid('enableEditing');
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onDestroy: function (index, row) {
            $(this).edatagrid('reload');
            if (row.success) {
                // $.messager.show({title: '提示', msg: row.message});
            } else {
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onError: function (index, row) {
            $.messager.show({title: '提示', msg: row.message});
        },
        onResizeColumn: function (f, w) {
            columnResizeEvent($('body').data('menu_id'), f, w);
        },
    })
}

function btn_search() {
    $('#e_grid').edatagrid('reload');
}

function btn_add() {
    $('#e_grid').edatagrid('addRow');
}

function btn_modify() {
    let selected = $('#e_grid').edatagrid('getSelected');
    if (selected != undefined) {
        let index = $('#e_grid').edatagrid('getRowIndex', selected);
        if (index != -1) {
            $('#e_grid').edatagrid('editRow', index);
        }
    } else {
        $.messager.alert('提示', '点击鼠标左键选中行后再执行本功能', 'warning');
    }
}

function btn_delete() {
    let selected = $('#e_grid').edatagrid('getSelected');
    if (selected != undefined) {
        let index = $('#e_grid').edatagrid('getRowIndex', selected);
        if (index != -1) {
            $('#e_grid').edatagrid('destroyRow', index);
        }
    } else {
        $.messager.alert('提示', '点击鼠标左键选中行后再执行本功能', 'warning');
    }
}

//  导入Excel
function btn_import() {
    $.uploadComponent('open');
}

function btn_save() {
    $('#e_grid').edatagrid('saveRow');
}

function btn_export() {
    exportExcel('#e_grid', '往来单位');
}