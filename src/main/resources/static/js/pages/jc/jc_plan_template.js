$(function () {
    //  按钮生成器
    generateButton($('#north'));
    let columns = [[
        {title: 'id', field: 'id', hidden: true,},
        {
            title: '是否设置为默认模板', field: 'setDefault', width: 50, editor: {
                type: 'combobox',
                options: {
                    required: true,
                    textField: 'text',
                    valueField: 'id',
                    panelHeight: 'auto',
                    editable: false,
                    data: [{id: true, text: '是'}, {id: false, text: '否'}]
                }
            },
            formatter: (v, r, i) => {
                return v == undefined ? '' : v ? '是' : '否';
            }
        },
        {
            title: '客户编号', field: 'contactCompanyId', editor: {
                type: 'combobox', options: {
                    url: 'contactCompany/listQuery',
                    method: 'GET', queryParams: {state: 1},
                    textField: 'companyName', valueField: 'id',
                    limitToList: true,
                    panelHeight: 200,
                    required: false
                }
            }
        },
        {
            title: '合&nbsp;&nbsp;同&nbsp;&nbsp;号',
            field: 'contractCode',
            editor: {type: 'textbox', options: {required: false}}
        },
        {title: '计划单号', field: 'planCode', editor: {type: 'textbox', options: {readonly: true}}},
        {
            title: '订&nbsp;&nbsp;单&nbsp;&nbsp;号',
            field: 'orderCode',
            editor: {type: 'textbox', options: {required: false}}
        },
        {
            title: '品&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名',
            field: 'productName',
            editor: {
                type: 'combobox',
                options: {
                    required: false,
                    url: 'productInfo/listQuery',
                    method: 'GET',
                    textField: 'productName',
                    valueField: 'productName',
                    panelHeight: 200
                }
            }
        },
        {
            title: '原料规格', field: 'materialSpecification', editor: {
                type: 'combobox',
                options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    panelHeight: 200
                }
            }
        },
        {title: '原料批号', field: 'materialLotNo', editor: {type: 'textbox', options: {}}},
        {
            title: '原料供应商', field: 'materialSupplierId', editor: {
                type: 'combobox', options: {
                    url: 'supplier/listQuery',
                    method: 'GET',
                    limitToList: true,
                    textField: 'supplierName',
                    valueField: 'id',
                    panelHeight: 200
                }
            }
        },
        {
            title: '白坯门幅', field: 'greyfabricWidth', editor: {
                type: 'combobox', options: {
                    url: 'widthOfFabric/listQuery',
                    method: 'GET',
                    textField: 'wofName',
                    limitToList: true,
                    valueField: 'wofName',
                    panelHeight: 200
                }
            }
        },
        {
            title: '头&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;份', field: 'greyfabricSpecification', editor: {
                type: 'combobox', options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    panelHeight: 200
                }
            }
        },
        {
            title: '中丝规格', field: 'middleSpecification', editor: {
                type: 'combobox', options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    panelHeight: 200
                }
            }
        },
        {
            title: '中丝供应商', field: 'middleSupplierId', editor: {
                type: 'combobox', options: {
                    url: 'supplier/listQuery',
                    method: 'GET',
                    textField: 'supplierName',
                    valueField: 'id',
                    panelHeight: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '底丝规格', field: 'bottomSpecification', editor: {
                type: 'combobox', options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    panelHeight: 200
                }
            }
        },
        {
            title: '底丝供应商', field: 'bottomSupplierId', editor: {
                type: 'combobox', options: {
                    url: 'supplier/listQuery',
                    method: 'GET',
                    textField: 'supplierName',
                    valueField: 'id',
                    panelHeight: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '机&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;型', field: 'machineType', editor: {
                type: 'textbox', options: {}
            }
        },
        {
            title: '针&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数', field: 'needleAmount', editor: {
                type: 'numberbox', options: {}
            }
        },
        {
            title: '下机克重', field: 'lowWeight', editor: {
                type: 'numberbox', options: {
                    precision: 1
                }
            }
        },
        {
            title: '计划人员', field: 'planEmployeeId', editor: {
                type: 'combobox', options: {
                    url: 'employee/listQuery',
                    method: 'GET',
                    textField: 'empName',
                    valueField: 'id',
                    panelHeight: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '生产人员', field: 'productEmployeeId', editor: {
                type: 'combobox', options: {
                    url: 'employee/listQuery',
                    method: 'GET',
                    textField: 'empName',
                    valueField: 'id',
                    panelHeight: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '复核人员', field: 'checkEmployeeId', editor: {
                type: 'combobox', options: {
                    url: 'employee/listQuery',
                    method: 'GET',
                    textField: 'empName',
                    valueField: 'id',
                    panelHeight: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {title: '毛&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高', field: 'mHeight', editor: {type: 'numberbox', options: {precision: 1}}},
        {title: '原料价格', field: 'materialPrice', editor: {type: 'numberbox', options: {precision: 2, min: 0}}},
        {title: '坯布要求', field: 'fabricRequirement', editor: {type: 'textbox', options: {width: 650}}},
        {title: '原料价格', field: 'materialPrice', editor: {type: 'numberbox', options: {width: 650, precision: 2}}},
        {title: '加工费', field: 'processingCost', editor: {type: 'numberbox', options: {width: 650, precision: 2}}},
        {title: '坯布价', field: 'greyfabricPrice', editor: {type: 'numberbox', options: {width: 650, precision: 2}}},
        {title: '喷码厂名', field: 'markCompanyName', editor: {type: 'textbox', options: {required: false}}},
        {
            title: '备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注',
            field: 'remarks',
            editor: {type: 'textbox', options: {width: 650}}
        }
    ]];
    setDatagridColumnOpts($('body').data('menu_id'), $('#e_grid'), columns, edatagrid);
    bindEnterEvents()
    $.uploadComponent({
        url: 'planTemplate/upload',
        method: 'POST',
        fields: columns[0],
        onSuccess: function (data) {
            if (data.success) {
                $.uploadComponent('close');
                $('#e_grid').edatagrid('reload');
                $.messager.show({title: '提示', msg: data.message});
            } else {
                $(this).edatagrid('cancelRow');
                $(this).edatagrid('editRow', index);
                $.messager.alert('提示', '上传失败!' + data.message, 'error');
            }
        }
    })
})

function edatagrid(columns) {
    $('#e_grid').edatagrid({
        method: 'GET',
        param: {},
        url: 'planTemplate/listQuery',
        saveUrl: 'planTemplate/insertSelective',
        updateUrl: 'planTemplate/updateSelective',
        destroyUrl: 'planTemplate/deleteByPk',
        idField: 'id',
        destroyMsg: {
            norecord: {
                title: '警告',
                msg: '未选中任何数据'
            },
            confirm: {
                title: '确认',
                msg: '确定删除吗?删除后不可恢复!'
            }
        },
        loadFilter: pageFilter,
        pagination: true,
        // pagePosition: 'top',
        pageSize: 200,
        pageNumber: 1,
        pageList: [200, 400, 600, 800, 1000],
        striped: true,
        fit: true,
        sortable: true,
        rownumbers: true,
        remoteSort: false,
        showFooter: true,
        singleSelect: true,
        columns: columns,
        onLoadSuccess: function () {
            $(this).datagrid('enableFilter');
            $(this).data('edatagrid').options.editIndex = -1;
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
        },
        onBeforeLoad: function (param) {
            $(this).datagrid('destroyFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('disable');
                }
            })
            return true;
        },
        onSuccess: function (index, row) {
            $(this).edatagrid('reload');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
            } else {
                $(this).edatagrid('enableEditing');
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onDestroy: function (index, row) {
            $(this).edatagrid('reload');
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
            } else {
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onError: function (index, row) {
            $.messager.show({title: '提示', msg: row.message});
        },
        onResizeColumn: function (f, w) {
            columnResizeEvent($('body').data('menu_id'), f, w);
        },
    })
}

function btn_search() {
    $('#e_grid').edatagrid('reload');
}

function btn_add() {
    $('#e_grid').edatagrid('addRow');
}

function btn_modify() {
    let selected = $('#e_grid').edatagrid('getSelected');
    if (selected != undefined) {
        let index = $('#e_grid').edatagrid('getRowIndex', selected);
        if (index != -1) {
            $('#e_grid').edatagrid('editRow', index);
        }
    } else {
        $.messager.alert('提示', '点击鼠标左键选中行后再执行本功能', 'warning');
    }
}

function btn_delete() {
    let selected = $('#e_grid').edatagrid('getSelected');
    if (selected != undefined) {
        let index = $('#e_grid').edatagrid('getRowIndex', selected);
        if (index != -1) {
            $('#e_grid').edatagrid('destroyRow', index);
        }
    } else {
        $.messager.alert('提示', '点击鼠标左键选中行后再执行本功能', 'warning');
    }
}

function btn_import() {
    $.uploadComponent('open');
}

function btn_save() {
    $('#e_grid').edatagrid('saveRow');
}

function btn_export() {
    exportExcel('#e_grid', '机台号');
}