$(function () {
    //  按钮生成器
    generateButton($('#north'));
    let columns = [[
        {
            field: 'id',
            title: '编号',
            width: 100,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: true}}
        },
        {
            field: 'supplierName',
            title: '名称',
            width: 200,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: true}}
        },
        /*{
            field: 'supplierCode',
            title: '编码',
            width: 200,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: false}}
        },
        {
            field: 'supplierAlias',
            title: '简称',
            width: 200,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: false}}
        },*/
        {
            field: 'phoneNumber',
            title: '联系电话',
            width: 200,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: false}}
        },
        {
            field: 'area',
            title: '地区',
            width: 200,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: false}}
        },
        {
            field: 'address',
            title: '地址',
            width: 200,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: false}}
        },
        {
            field: 'postCode',
            title: '邮编',
            width: 200,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: false}}
        },
        {
            field: 'remarks',
            title: '备注 ',
            width: 200,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: false}}
        },
    ]];
    setDatagridColumnOpts($('body').data('menu_id'), $('#e_grid'), columns, edatagrid);
    bindEnterEvents()
    $.uploadComponent({
        url: 'supplier/upload',
        method: 'POST',
        fields: columns[0],
        onSuccess: function (data) {
            if (data.success) {
                $.uploadComponent('close');
                $('#e_grid').edatagrid('reload');
                $.messager.show({title: '提示', msg: data.message});
            } else {
                $(this).edatagrid('cancelRow');
                $(this).edatagrid('editRow', index);
                $.messager.alert('提示', '上传失败!' + data.message, 'error');
            }
        }
    })
})

function edatagrid(columns) {
    $('#e_grid').edatagrid({
        method: 'GET',
        param: {},
        url: 'supplier/listQuery',
        saveUrl: 'supplier/insertSelective',
        updateUrl: 'supplier/updateSelective',
        destroyUrl: 'supplier/deleteByPk',
        idField: 'id',
        destroyMsg: {
            norecord: {
                title: '警告',
                msg: '未选中任何数据'
            },
            confirm: {
                title: '确认',
                msg: '确定删除吗?删除后不可恢复!'
            }
        },
        loadFilter: pageFilter,
        pagination: true,
        // pagePosition: 'top',
        pageSize: 200,
        pageNumber: 1,
        pageList: [200, 400, 600, 800, 1000],
        striped: true,
        fit: true,
        sortable: true,
        rownumbers: true,
        remoteSort: false,
        showFooter: true,
        singleSelect: true,
        columns: columns,
        onLoadSuccess: function () {
            $(this).datagrid('enableFilter');
            $(this).data('edatagrid').options.editIndex = -1;
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
        },
        onBeforeLoad: function (param) {
            $(this).datagrid('destroyFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('disable');
                }
            })
            return true;
        },
        onSuccess: function (index, row) {
            $(this).edatagrid('reload');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
            } else {
                $(this).edatagrid('enableEditing');
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onDestroy: function (index, row) {
            $(this).edatagrid('reload');
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
            } else {
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onError: function (index, row) {
            $.messager.show({title: '提示', msg: row.message});
        },
        onResizeColumn: function (f, w) {
            columnResizeEvent($('body').data('menu_id'), f, w);
        },
    })
}

function btn_search() {
    $('#e_grid').edatagrid('reload');
}

function btn_add() {
    $('#e_grid').edatagrid('addRow');
}

function btn_modify() {
    let selected = $('#e_grid').edatagrid('getSelected');
    if (selected != undefined) {
        let index = $('#e_grid').edatagrid('getRowIndex', selected);
        if (index != -1) {
            $('#e_grid').edatagrid('editRow', index);
        }
    } else {
        $.messager.alert('提示', '点击鼠标左键选中行后再执行本功能', 'warning');
    }
}

function btn_delete() {
    let selected = $('#e_grid').edatagrid('getSelected');
    if (selected != undefined) {
        let index = $('#e_grid').edatagrid('getRowIndex', selected);
        if (index != -1) {
            $('#e_grid').edatagrid('destroyRow', index);
        }
    } else {
        $.messager.alert('提示', '点击鼠标左键选中行后再执行本功能', 'warning');
    }
}

function btn_import() {
    $.uploadComponent('open');
}

function btn_save() {
    $('#e_grid').edatagrid('saveRow');
}

function btn_export() {
    exportExcel('#e_grid', '供应商');
}