/**
 * 2021年11月19日 10:40:11 chanchaw
 * 成品计划序时表 - 色布计划序时表
 */
$(function () {
    let buttonGroup = [
        {
            id: 'btn_add',
            size: 'small',
            text: '新增单据',
            iconCls: 'icon-add',
            onClick: btn_add,
            plain: true
        },
        {
            id: 'btn_cancel_filter',
            size: 'small',
            text: '清除过滤',
            iconCls: 'icon-clear',
            plain: true,
            onClick: btn_cancel_filter
        },
        {
            id: 'btn_export',
            size: 'small',
            text: '导出Excel',
            iconCls: 'icon-page_excel',
            plain: true,
            onClick: btn_export_excel
        },
        {
            id: 'btn_finish',
            size: 'small',
            text: '订单完成',
            iconCls: 'icon-user_gray',
            onClick: btn_finish,
            plain: true
        },
        {
            id: 'btn_cancel_finish',
            size: 'small',
            text: '取消完成',
            iconCls: 'icon-user_delete',
            onClick: btn_cancel_finish,
            plain: true
        },
        /*{
            id: 'btn_delete',
            size: 'small',
            text: '作废',
            iconCls: 'icon-cancel',
            onClick: btn_delete,
            plain: true
        },*/
        {
            id: 'btn_cancel_search',
            size: 'small',
            text: '作废查询',
            iconCls: 'icon-search',
            onClick: btn_cancel_search,
            plain: true
        },
        {
            id: 'btn_print',
            size: 'small',
            text: '打印',
            iconCls: 'icon-print',
            plain: true,
            onClick: btn_print
        },
        {
            id: 'btn_grid_columns',
            size: 'small',
            text: '列',
            iconCls: 'icon-text_columns',
            plain: true,
            onClick: btn_open_column_list
        },
        {
            id: 'btn_cancel_audit',
            size: 'small',
            disabled: true,
            text: '消审',
            iconCls: 'icon-exclamation',
            onClick: btn_cancel_audit,
            plain: true
        },
        {
            id: 'btn_close', size: 'small', text: '关闭', iconCls: 'icon-no', onClick: btn_close,
            plain: true
        },
    ];
    buttonGen($('#north'), buttonGroup);
    grid_1();
    bindEnterEvents();

    let kGrid = $('#v_grid').data('kendoGrid');
    var options = kGrid.getOptions();
    if (options.columns.length > 0)
        initCombobox(options)
})

const planColumns = [
    {
        title: 'id',
        field: 'id',
        hidden: true,
    },
    {
        title: '日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;期',
        field: 'planDate',
        editor: {type: 'datebox', options: {value: getCurrentDate(), cls: 'input-blue',}}
    },
    {
        title: '客&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;户',
        field: 'contactCompanyId',
        editor: {
            type: 'combobox', options: {
                url: 'contactCompany/listQuery',
                method: 'GET', queryParams: {state: 1},
                textField: 'companyName', valueField: 'id',
                cls: 'input-blue',
                panelHeight: 200,
                required: true,
                onLoadSuccess: function () {
                    $(this).combobox('options').limitToList = true;
                }
            }
        }
    },
    {
        title: '合&nbsp;&nbsp;同&nbsp;&nbsp;号',
        field: 'contractCode',
        editor: {type: 'textbox', options: {required: false, cls: 'input-blue',}}
    },
    {
        title: '计划单号',
        field: 'planCode',
        editor: {type: 'textbox', options: {readonly: true, cls: 'input-blue',}}},
    {
        title: '订&nbsp;&nbsp;单&nbsp;&nbsp;号',
        field: 'orderCode',
        editor: {type: 'textbox', options: {required: false, cls: 'input-blue',}}
    },
    {
        title: '品&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名',
        field: 'productName',
        editor: {
            type: 'combobox',
            options: {
                required: true,
                cls: 'input-red',
                url: 'dyeProductInfo/listQuery',
                method: 'GET',
                textField: 'productName',
                valueField: 'productName',
            }
        }
    },
    {
        title: '类&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;型',
        field: 'processType',
        editor: {
            type: 'combobox',
            options: {
                url: 'processType/listQuery',
                method: 'GET',
                textField: 'typeName',
                valueField: 'typeName',
                cls: 'input-blue',
                panelHeight: 200,
                required: true,
                onLoadSuccess: function () {
                    $(this).combobox('options').limitToList = true;
                    if (!$(this).combobox('getValue')) {
                        $(this).combobox('setValue', '染色');
                    }
                }
            }
        }
    },
    {
        title: '规&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;格',
        field: 'materialSpecification',
        editor: {
            type: 'combobox',
            options: {
                cls: 'input-blue',
                required: true,
                url: 'dyeSpecification/listQuery',
                method: 'GET',
                textField: 'specName',
                valueField: 'specName',
                panelHeight: 200
            }
        }
    },
    {title: '色号花号', field: 'colorNo', editor: {type: 'textbox', options: {cls: 'input-blue', required: false}}},
    {title: '颜色花型', field: 'color', editor: {type: 'textbox', options: {cls: 'input-blue', required: true}}},
    {
        title: '毛&nbsp;&nbsp;净&nbsp;&nbsp;差',
        field: 'netDiff',
        editor: {type: 'numberbox', options: {cls: 'input-red', precision: 1}}
    },
    {
        title: '计米系数',
        field: 'rate',
        id: 'rateInput',
        editor: {type: 'numberbox', options: {width: 50, cls: 'input-red', precision: 1, value: 1, editable: false}}
    },
    {
        title: '计价',
        width: '85px',
        field: 'priceStandard',
        editor: {
            type: 'combobox',
            options: {
                width: 85,
                url: 'dyeFabricPlan/list/pricestandard',
                method: 'GET',
                value: 1,
                textField: 'key',
                valueField: 'value',
                panelHeight: 'auto',
                onLoadSuccess: function () {
                    $(this).combobox('options').limitToList = true;
                },
                onChange: function (newVal, oldVal) {
                    if (!newVal) return;
                    switch (newVal) {
                        case 1:
                        case 2:
                        case 4:
                            $('#rateInput').numberbox('clear');
                            $('#rateInput').numberbox({editable: false, required: false});
                            $('#meterCounterEditor').val(0);
                            break;
                        case 3:
                            $('#rateInput').numberbox({editable: true, required: true});
                            $('#meterCounterEditor').val(1);
                            break;
                    }
                }
            }
        }
    },
    {
        title: '计米',
        width: '50px',
        field: 'meterCounter',
        id: 'meterCounterEditor',
        hidden: true,
        /*editor: {
            type: 'checkbox', options: {
                cls: 'input-red', value: 1, onChange: function (checked) {
                    if (checked) {
                        $('#rateInput').numberbox({editable: true});
                    } else {
                        $('#rateInput').numberbox('clear');
                        $('#rateInput').numberbox({editable: false});
                    }
                }
            }
        }*/
    },
    {
        title: '计划重量',
        field: 'planKilo',
        editor: {type: 'numberbox', options: {precision: 1, required: false, cls: 'input-blue',}}
    },
    {
        title: '计划匹数',
        field: 'planPairs',
        editor: {type: 'numberbox', options: {precision: 0, required: false, cls: 'input-blue',}}
    },
    {
        title: '投坯重量',
        field: 'planUseKilo',
        editor: {type: 'numberbox', options: {precision: 1, required: false, cls: 'input-blue',}}
    },
    {
        title: '投坯匹数',
        field: 'planUsePairs',
        editor: {type: 'numberbox', options: {precision: 0, required: false, cls: 'input-blue',}}
    },
    {
        title: '单&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;价',
        field: 'materialPrice',
        editor: {type: 'numberbox', options: {precision: 2, min: 0, cls: 'input-blue',}}
    },
    {
        title: '跟单人员', field: 'planEmployeeId',
        editor: {
            type: 'combobox', options: {
                url: 'employee/listQuery',
                method: 'GET',
                textField: 'empName',
                cls: 'input-blue',
                onLoadSuccess: function () {
                    $(this).combobox('options').limitToList = true;
                },
                valueField: 'id',
                panelHeight: 200
            }
        }
    },
    // {
    //     title: '纸&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;管',
    //     field: 'papertube',
    //     editor: {type: 'numberbox', options: {width: 51, cls: 'input-blue', precision: 1, value: 1, editable: true}}
    // },
    // {
    //     title: '袋&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;重',
    //     field: 'bagweight',
    //     editor: {type: 'numberbox', options: {width: 51, cls: 'input-blue', precision: 1, value: 1, editable: true}}
    // },
    // {
    //     title: '空&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;加',
    //     field: 'emptyaddition',
    //     editor: {type: 'numberbox', options: {width: 51, cls: 'input-blue', precision: 1, value: 1, editable: true}}
    // },
    // {
    //     title: '空加隐藏',
    //     field: 'emptyadditionhide',
    //     editor: {type: 'numberbox', options: {width: 51, cls: 'input-blue', precision: 1, value: 1, editable: true}}
    // },
    {
        title: '制&nbsp;&nbsp;单&nbsp;&nbsp;人',
        field: 'createUserId',
        editor: {
            type: 'combobox',
            options: {
                readonly: true,
                url: 'sysUser/query',
                method: 'GET',
                textField: 'userName',
                cls: 'input-blue',
                valueField: 'id',
                onLoadSuccess: function () {
                    $(this).combobox('options').limitToList = true;
                }
            }
        }
    },
    {
        title: '备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注',
        field: 'remarks',
        editor: {type: 'textbox', options: {width: 496, cls: 'input-blue',}}
    },
];

function grid_1() {
    let SUFFIX = "";
    let company_name = new kendo.data.DataSource({data: []});
    let contract_code = new kendo.data.DataSource({data: []});
    let plan_code = new kendo.data.DataSource({data: []});
    let order_code = new kendo.data.DataSource({data: []});
    let product_name = new kendo.data.DataSource({data: []});
    let process_type = new kendo.data.DataSource({data: []});
    let material_specification = new kendo.data.DataSource({data: []});
    let machine_type = new kendo.data.DataSource({data: []});
    let color_no = new kendo.data.DataSource({data: []});
    let paper_tube = new kendo.data.DataSource({data: []});
    let bag_weight = new kendo.data.DataSource({data: []});
    let empty_addition = new kendo.data.DataSource({data: []});
    let empty_addition_hide = new kendo.data.DataSource({data: []});
    let color = new kendo.data.DataSource({data: []});
    let process_cpmpany = new kendo.data.DataSource({data: []});
    let plan_employee_name = new kendo.data.DataSource({data: []});
    let create_user_name = new kendo.data.DataSource({data: []});
    let price_standard_name = new kendo.data.DataSource({data: []});
    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "dyeFabricPlan/report",
                dataType: "json",
                type: "GET",
                data: {
                    start: $('#start').datebox('getValue'),
                    end: $('#end').datebox('getValue'),
                    markFinished: (function () {
                        let checked = $('#state').switchbutton('options').checked;
                        return checked ? checked : null;
                    }),
                    textField: $('#column').combobox('getValue'),
                    valueField: $('#value').textbox('getValue'),
                }
            }
        },
        schema: {
            model: {
                id: 'id',
                fields: {
                    plan_date: {type: "date"},
                    submit_date: {type: "date"},
                    greyfabric_price: {type: "number"},
                    plan_kilo: {type: "number"},
                    plan_pairs: {type: "number"},
                    plan_use_pairs: {type: "number"},
                    plan_use_kilo: {type: "number"},
                    material_price: {type: "number"},
                    net_diff: {type: "number"},
                    input_pairs: {type: "number"},
                    delivery_pairs: {type: "number"},
                    remain_pairs: {type: "number"},
                    input_kilo: {type: "number"},
                    paper_tube: {type: "number"},
                    bag_weight: {type: "number"},
                    empty_addition: {type: "number"},
                    empty_addition_hide: {type: "number"},
                    delivery_kilo: {type: "number"},
                    remain_kilo: {type: "number"},
                    delivery_price: {type: "number"},
                    delivery_money: {type: "number"},
                    destroy_qty: {type: "number"},
                    delivery_meter: {type: "number"},
                    delivery_net_kilo: {type: "number"},
                }
            }
        },
        aggregate: [
            {field: "plan_kilo", aggregate: "sum"},
            {field: "plan_pairs", aggregate: "sum"},
            {field: "plan_use_pairs", aggregate: "sum"},
            {field: "plan_use_kilo", aggregate: "sum"},
            {field: "paper_tube", aggregate: "sum"},
            {field: "bag_weight", aggregate: "sum"},
            {field: "empty_addition", aggregate: "sum"},
            {field: "empty_addition_hide", aggregate: "sum"},
            {field: "input_pairs", aggregate: "sum"},
            {field: "input_kilo", aggregate: "sum"},
            {field: "delivery_pairs", aggregate: "sum"},
            {field: "delivery_kilo", aggregate: "sum"},
            {field: "remain_pairs", aggregate: "sum"},
            {field: "remain_kilo", aggregate: "sum"},
            {field: "delivery_money", aggregate: "sum"},
            {field: "delivery_meter", aggregate: "sum"},
            {field: "delivery_net_kilo", aggregate: "sum"},
        ],
        change: function (e) {
            let data = this.data();
            company_name.data(unique(data, 'company_name'));
            color_no.data(unique(data, 'color_no'));
            color.data(unique(data, 'color'));
            contract_code.data(unique(data, 'contract_code'));
            plan_code.data(unique(data, 'plan_code'));
            bag_weight.data(unique(data, 'bag_weight'));
            paper_tube.data(unique(data, 'paper_tube'));
            empty_addition_hide.data(unique(data, 'empty_addition_hide'));
            empty_addition.data(unique(data, 'empty_addition'));
            order_code.data(unique(data, 'order_code'));
            product_name.data(unique(data, 'product_name'));
            material_specification.data(unique(data, 'material_specification'));
            machine_type.data(unique(data, 'machine_type'));
            plan_employee_name.data(unique(data, 'plan_employee_name'));
            create_user_name.data(unique(data, 'create_user_name'));
            process_cpmpany.data(unique(data, 'process_cpmpany'));
            price_standard_name.data(unique(data, 'price_standard_name'));
            $('#btn_cancel_audit').linkbutton('disable')
            //获取过滤条件
            var filtrer = this.filter();
            //获取所有数据
            var allData = this.data();
            //进行过滤条件的筛选
            var query = new kendo.data.Query(allData);
            //获取过过滤后的数据
            var dat = query.filter(filtrer).data;
            // grid_1(query.filter().data);

            $('#btn_cancel_audit').linkbutton('disable')
        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })
            // $('body').find('th a[class="k-grid-filter"]').css({display: 'none'});
        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })
            // $('body').find('th a[class="k-grid-filter"]').css({display: 'none'});
        }
    });
    let columns = [
        {field: "no", title: ".", filterable: false, template: "<span class='row_number'></span>", width: 40},
        // {
        //     field: "id",
        //     title: '编号',
        //     width: 93,
        //     hidden: true,
        //     attributes: {'class': 'kendo-custum-char'},
        //     headerAttributes: {style: 'text-align:center;'},
        //     footerAttributes: {style: 'color: red;text-align:center;'},
        // },
        // {
        //     field: "contact_company_id",
        //     title: '客户编号',
        //     width: 105,
        //     hidden: true,
        //     attributes: {'class': 'kendo-custum-char'},
        //     headerAttributes: {style: 'text-align:center;'},
        //     footerAttributes: {style: 'color: red;text-align:center;'},
        // },
        // {
        //     field: "plan_employee_id",
        //     title: '计划人员编号',
        //     width: 105,
        //     hidden: true,
        //     attributes: {'class': 'kendo-custum-char'},
        //     headerAttributes: {style: 'text-align:center;'},
        //     footerAttributes: {style: 'color: red;text-align:center;'},
        // },
        {
            field: "plan_date",
            title: '计划日期',
            width: 105,
            format: "{0: yyyy-MM-dd}",
            filterable: false,
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'}
        },
        {
            field: "company_name",
            title: '客户',
            width: 93,
            footerTemplate: '总计:',
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: company_name}
        },
        {
            field: "price_standard_name",
            title: '计价标准',
            width: 105,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: price_standard_name}
        },
        {
            field: "contract_code",
            title: '合同号',
            width: 95,
            filterable: {multi: true, search: true, dataSource: contract_code},
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'}
        },
        {
            field: "process_cpmpany",
            title: '加工单位',
            width: 105,
            filterable: {multi: true, search: true, dataSource: process_cpmpany},
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'}
        },
        {
            field: "plan_code",
            title: '计划单号',
            width: 105,
            filterable: {multi: true, search: true, dataSource: plan_code},
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'}
        },
        {
            field: "order_code",
            title: '订单号',
            width: 95,
            filterable: {multi: true, search: true, dataSource: order_code},
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'}
        },
        {
            field: "product_name",
            title: '品名',
            width: 95,
            filterable: {multi: true, search: true, dataSource: product_name},
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'}
        },
        {
            field: "process_type",
            title: '加工类型',
            width: 88,
            filterable: {multi: true, search: true, dataSource: process_type},
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'}
        },
        {
            field: 'material_specification',
            title: '规格',
            width: 95,
            filterable: {multi: true, search: true, checkAll: true, dataSource: material_specification},
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: "color_no",
            title: '色号/花号',
            width: 70,
            filterable: {multi: true, search: true, dataSource: color_no},
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'}
        },
        {
            field: "color",
            title: '颜色/花型',
            width: 70,
            filterable: {multi: true, search: true, dataSource: color},
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'}
        },
        {
            field: 'net_diff',
            title: '毛净差',
            width: 95,
            template: item => {
                return item.net_diff ? parseFloat(item.net_diff).toFixed(1) : '';
            },
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'rate',
            title: '计米系数',
            width: 105,
            template: item => {
                return item.rate ? parseFloat(item.rate).toFixed(1) : '';
            },
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'destroy_qty',
            title: '损耗',
            width: 95,
            template: item => {
                return item.destroy_qty ? parseFloat(item.destroy_qty).toFixed(1) : '';
            },
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'plan_kilo',
            title: '计划重',
            width: 95,
            template: item => {
                return item.plan_kilo ? parseFloat(item.plan_kilo).toFixed(1) : '';
            },
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'plan_pairs',
            title: '计划匹',
            width: 95,
            template: item => {
                return item.plan_pairs ? parseFloat(item.plan_pairs).toFixed(0) : '';
            },
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'plan_use_kilo',
            title: '投坯重',
            width: 95,
            template: item => {
                return item.plan_use_kilo ? parseFloat(item.plan_use_kilo).toFixed(1) : '';
            },
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'plan_use_pairs',
            title: '投坯匹',
            width: 95,
            template: item => {
                return item.plan_use_pairs ? parseFloat(item.plan_use_pairs).toFixed(0) : '';
            },
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'material_price',
            title: '价格',
            width: 95,
            template: item => {
                return item.material_price ? parseFloat(item.material_price).toFixed(2) : '';
            },
            footerAttributes: {style: 'text-align:right;'},
            filterable: false,
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;'}
        },
        // {
        //     field: 'delivery_price',
        //     title: '发货单价',
        //     width: 80,
        //     template: item => {
        //         return item.delivery_price ? parseFloat(item.delivery_price).toFixed(2) : '';
        //     },
        //     footerAttributes: {style: 'text-align:right;'},
        //     filterable: false,
        //     attributes: {'class': 'kendo-custum-number'},
        //     headerAttributes: {style: 'text-align:center;'}
        // },
        {
            field: 'delivery_money',
            title: '发货金额',
            width: 105,
            template: item => {
                return item.delivery_money ? parseFloat(item.delivery_money).toFixed(2) : '';
            },
            footerTemplate: '#=kendo.format("{0:n2}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            filterable: false,
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: "submit_date",
            title: '交期',
            width: 95,
            format: "{0: yyyy-MM-dd}",
            headerAttributes: {style: 'text-align:center;'},
            filterable: false,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'}
        },
        {
            field: 'plan_employee_name',
            title: '跟单人员',
            width: 105,
            filterable: {multi: true, search: true, dataSource: plan_employee_name},
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'remarks',
            title: '备注',
            width: 95,
            filterable: {multi: true, search: true},
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'input_pairs',
            title: '入库匹',
            width: 95,
            template: item => {
                return item.input_pairs ? parseFloat(item.input_pairs).toFixed(0) : '';
            },
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'delivery_pairs',
            title: '发货匹',
            width: 95,
            template: item => {
                return item.delivery_pairs ? parseFloat(item.delivery_pairs).toFixed(0) : '';
            },
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'delivery_net_kilo',
            title: '发货净重',
            width: 105,
            template: item => {
                return item.delivery_net_kilo ? parseFloat(item.delivery_net_kilo).toFixed(1) : '';
            },
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'delivery_meter',
            title: '发货米数',
            width: 105,
            template: item => {
                return item.delivery_meter ? parseFloat(item.delivery_meter).toFixed(1) : '';
            },
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'remain_pairs',
            title: '库存匹',
            width: 95,
            template: item => {
                return item.remain_pairs ? parseFloat(item.remain_pairs).toFixed(0) : '';
            },
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'input_kilo',
            title: '入库重',
            width: 95,
            template: item => {
                return item.input_kilo ? parseFloat(item.input_kilo).toFixed(1) : '';
            },
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'delivery_kilo',
            title: '发货重',
            width: 95,
            template: item => {
                return item.delivery_kilo ? parseFloat(item.delivery_kilo).toFixed(1) : '';
            },
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'remain_kilo',
            title: '库存重',
            width: 95,
            template: item => {
                return item.remain_kilo ? parseFloat(item.remain_kilo).toFixed(1) : '';
            },
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            filterable: false,
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'create_user_name',
            title: '制单人',
            width: 95,
            filterable: {multi: true, search: true, dataSource: create_user_name},
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {style: 'text-align:center;'}
        },
        {
            field: 'mark_finished',
            title: '完工标记',
            width: 105,
            hidden: true,
            attributes: {'class': 'kendo-custum-char'},
            headerAttributes: {style: 'text-align:center;'}
        },
        // {
        //     field: 'paper_tube',
        //     title: '纸管',
        //     width: 95,
        //     template: item => {
        //         return item.paper_tube ? parseFloat(item.paper_tube).toFixed(1) : '';
        //     },
        //     footerTemplate: '#=kendo.format("{0:n1}",sum)#',
        //     footerAttributes: {style: 'text-align:right;'},
        //     attributes: {'class': 'kendo-custum-number'},
        //     filterable: false,
        //     headerAttributes: {style: 'text-align:center;'},
        // },
        // {
        //     field: 'bag_weight',
        //     title: '袋重',
        //     width: 95,
        //     template: item => {
        //         return item.bag_weight ? parseFloat(item.bag_weight).toFixed(1) : '';
        //     },
        //     footerTemplate: '#=kendo.format("{0:n1}",sum)#',
        //     footerAttributes: {style: 'text-align:right;'},
        //     attributes: {'class': 'kendo-custum-number'},
        //     filterable: false,
        //     headerAttributes: {style: 'text-align:center;'}
        // },
        // {
        //     field: 'empty_addition',
        //     title: '空加',
        //     width: 95,
        //     template: item => {
        //         return item.empty_addition ? parseFloat(item.empty_addition).toFixed(1) : '';
        //     },
        //     footerTemplate: '#=kendo.format("{0:n1}",sum)#',
        //     footerAttributes: {style: 'text-align:right;'},
        //     attributes: {'class': 'kendo-custum-number'},
        //     filterable: false,
        //     headerAttributes: {style: 'text-align:center;'}
        // },
        // {
        //     field: 'empty_addition_hide',
        //     title: '空加隐藏',
        //     width: 105,
        //     template: item => {
        //         return item.empty_addition_hide ? parseFloat(item.empty_addition_hide).toFixed(1) : '';
        //     },
        //     footerTemplate: '#=kendo.format("{0:n1}",sum)#',
        //     footerAttributes: {style: 'text-align:right;'},
        //     attributes: {'class': 'kendo-custum-number'},
        //     filterable: false,
        //     headerAttributes: {style: 'text-align:center;'}
        // },
    ];
    let grid = $("#v_grid").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "计划序时表.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            let kGrid = $('#v_grid').data('kendoGrid');
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
                let dataItem = kGrid.dataItem($(this));
                let type = dataItem['mark_finished'];
                if (type) {
                    $(this).removeClass('k-alt');
                    $(this).addClass('kendo-background-color-yellow');
                }
                if (dataItem['is_audit'] != undefined && !dataItem['is_audit']) {
                    $(this).addClass('kendo-custum-non-checked');
                }
            });
        },
        columnHide: function (e) {
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null);
        },
        columnShow: function (e) {
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null);
        },
        columnResize: function (e) {
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null);
            }
        },
        columnReorder: function (e) {
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field']
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid01.setOptions({
        columns: columns
    });

    //region 初始化下拉框
    initCombobox(grid01.getOptions());
    //endregion

    grid.on('click', 'tr', function (e) {
        let kGrid = $("#v_grid").data("kendoGrid");
        let dataItem = kGrid.dataItem(e.currentTarget);
        if (dataItem == undefined) {
            $.messager.alert('提示', '未选中!', 'warning');
            return;
        }
        let mark_finished = dataItem['mark_finished'];
        if (mark_finished) {
            $('#btn_finish').linkbutton('disable');
            $('#btn_cancel_finish').linkbutton('enable');
        } else {
            $('#btn_finish').linkbutton('enable');
            $('#btn_cancel_finish').linkbutton('disable');
        }
        if (dataItem['is_audit']) {
            $('#btn_cancel_audit').linkbutton('enable')
        } else {
            $('#btn_cancel_audit').linkbutton('disable')
        }
    })
    grid.on('dblclick', 'tr', function (e) {
        //  双击打开原单
        let kGrid = $("#v_grid").data("kendoGrid");
        let dataItem = kGrid.dataItem(e.currentTarget);
        if (dataItem == undefined) {
            $.messager.alert('提示', '未选中!', 'warning');
            return;
        }
        $.ajax({
            url: `dyeFabricPlan/queryByPk/${dataItem.id}`,
            type: 'GET',
            dataType: 'json'
        }).then(data => {
            if (data != undefined && data.state == 1)
                $.formDialog.open({
                    mode: '编辑',
                    loadData: data,
                    columns: planColumns,
                    buttons: [{
                        text: '刷新缓存', iconCls: 'icon-reload', handler: e => {
                            $('input').each(function (item, index) {
                                try {
                                    if ($(this).hasClass('easyui-combobox')) {
                                        $(this).combobox('reload');
                                    } else if ($(this).hasClass('easyui-combogrid')) {
                                        $(this).combogrid('grid').datagrid('reload');
                                    } else if ($(this).hasClass('easyui-combotree')) {
                                        $(this).combotree('tree').tree('reload');
                                    }
                                } catch (e) {
                                    console.table(e);
                                }
                            });
                        }
                    }, {
                        text: '保存',
                        iconCls: 'icon-save',
                        handler: e => {
                            let dialog = $('.form-dialog-edit');
                            if (!$(dialog.find('form')).form('validate')) {
                                return;
                            }
                            let param = $(dialog.find('form')).serializeObject();
                            let url = 'dyeFabricPlan/updateSelective';
                            //  修改
                            $.messager.progress({
                                title: '',
                                msg: '请稍等...',
                                interval: 5000
                            });
                            $.ajax({
                                url: url,
                                type: 'POST',
                                dataType: 'json',
                                data: JSON.stringify(param),
                                contentType: 'application/json',
                                success: function (data) {
                                    if (data.success) {
                                        btn_search();
                                        dialog.dialog('close');
                                        $.messager.show({title: '提示', msg: data.message});
                                    } else {
                                        $.messager.alert('提示', data.message, 'warning');
                                    }
                                },
                                complete: function () {
                                    $.messager.progress('close');
                                }
                            })
                        }
                    }, {
                        text: '取消',
                        iconCls: 'icon-cancel', handler: e => {
                            $('.form-dialog-edit').dialog('close');
                        }
                    }],
                    width: 639,
                    height: 'auto',
                })
        }, () => {
        });
    });
    grid.on('contextmenu', 'tr', function (e) {
        e.preventDefault();
        let kGrid = $('#v_grid').data('kendoGrid');
        kGrid.clearSelection();
        kGrid.select(e.currentTarget);
        let dataItem = kGrid.dataItem(e.currentTarget);
        let div = $(`<div class="easyui-menu">
                         <div data-options="name:'查看明细',iconCls:'icon-pictures'">查看明细</div>
                     </div>`);
        $('body').append(div);
        div.menu({
            onClick: function (item) {
                let name = item.text;
                switch (name) {
                    case '查看明细':
                        let dialog = $(`
                            <div>
                                <div class="easyui-dialog" data-options="width:800,height:800,modal:true,title:'查看明细',
                                                                         onClose:function(){$(this).dialog('destroy')},   
                                                                         iconCls:'icon-edit'">
                                    <div class="easyui-layout" data-options="fit:true">
                                        <div data-options="region:'north',height:'auto'" style="padding: 5px;">
                                            <a class="easyui-linkbutton btn-modify" id="btn_add_modify" data-options="iconCls:'icon-add',disabled:true,onClick:btn_add_modify">增加调整</a>
                                            <a class="easyui-linkbutton btn-remove-modify" id="btn_remove_modify" data-options="iconCls:'icon-cancel',disabled:true,onClick:btn_remove_modify">删除调整</a>
                                            <a class="easyui-linkbutton btn-order-finished" id="btn_finished" data-options="iconCls:'icon-ok',onClick:btn_finish">订单完成</a>
                                            <a class="easyui-linkbutton btn-cancel" data-options="iconCls:'icon-no',onClick:btn_cancel">作废</a>
                                        </div>
                                        <div data-options="region:'center'">
                                            <div class="easyui-layout" data-options="fit:true">
                                                <div data-options="region:'north',height:'auto'">
                                                   <form id="input_form" style="width: 100%;height: 100%;">
                                                    <div class="form-row">
                                                        <input name="contactCompanyName" class="easyui-textbox" data-options="cls: 'input-blue',readonly:true,width:670,label:'客户：'">
                                                    </div>
                                                   <div class="form-row">
                                                        <input name="productName" class="easyui-textbox" data-options="cls: 'input-blue',readonly:true,width:670,label:'品名规格：'">
                                                     </div>
                                                    <div class="form-row">
                                                        <input name="planKilo" class="easyui-numberbox" data-options="cls: 'input-blue',width:163,readonly:true,label:'计划公斤：',precision:1">
                                                        <input name="sendKilo" class="easyui-numberbox" data-options="cls: 'input-blue',width:163,readonly:true,label:'已发公斤：',precision:1">
                                                        <input name="planRemainKilo" class="easyui-numberbox" data-options="cls: 'input-blue',width:163,readonly:true,label:'剩余公斤：',precision:1">
                                                    </div>
                                                     <div class="form-row">
                                                        <input name="inputPairs" class="easyui-numberbox" data-options="cls: 'input-blue',width:163,readonly:true,label:'总入库匹：',precision:0">
                                                        <input name="inputKilo" class="easyui-numberbox" data-options="cls: 'input-blue',width:163,readonly:true,label:'总入库重：',precision:1">
                                                        <input name="price" class="easyui-numberbox" data-options="cls: 'input-blue',width:163,readonly:true,label:'默认单价：',precision:2">
                                                    </div>
                                                    <div class="form-row">
                                                        <input name="remainPairs" class="easyui-numberbox" data-options="cls: 'input-blue',width:163,label:'库存匹数：',precision:0">
                                                        <input name="remainKilo" class="easyui-numberbox" data-options="cls: 'input-blue',width:163,label:'库存公斤：',precision:1">
                                                    </div>
                                                    <div class="form-row">
                                                        <a style="margin-left: 70px;" class="easyui-linkbutton pull-right" data-options="iconCls:'icon-reload',onClick:btn_update_storage">库存更新</a>
                                                     </div>
                                                   </form>
                                                </div>
                                                <div data-options="region:'center'">
                                                    <div class="easyui-tabs" data-options="fit:true,onSelect:function(title,index){
                                                                 switch(index){
                                                                    case 0:
                                                                         $('#btn_add_modify').linkbutton('disable');
                                                                         $('#btn_remove_modify').linkbutton('disable');
                                                                        break;
                                                                    case 1:
                                                                         $('#btn_add_modify').linkbutton('disable');
                                                                         $('#btn_remove_modify').linkbutton('disable');
                                                                        break;
                                                                    case 2:
                                                                         $('#btn_add_modify').linkbutton('enable');
                                                                         $('#btn_remove_modify').linkbutton('enable');
                                                                        break;        
                                                                 }   
                                                        }">
                                                        <div data-options="title:'入库明细'">
                                                            <table id="input_dtl"></table>
                                                        </div>
                                                        <div data-options="title:'发货明细'">
                                                            <table id="send_dtl"></table>
                                                        </div>
                                                        <div data-options="title:'库存调整'">
                                                            <table id="storage_dtl"></table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `);
                        $.parser.onComplete = function (context) {
                            //  入库明细
                            let column1 = [[
                                {field: 'id', hidden: true, title: '编号'},
                                {field: 'createdate', title: '入库时间', width: 120, halign: 'center', align: 'center'},
                                {
                                    field: 'computer_name',
                                    title: '打卷机号',
                                    width: 160,
                                    halign: 'center',
                                    align: 'center'
                                },
                                {
                                    field: 'countable',
                                    title: '件数',
                                    width: 135,
                                    halign: 'center',
                                    align: 'center',
                                    formatter: (v, r, i) => v ? parseFloat(v).toFixed(0) : ''
                                },
                                {
                                    field: 'input_pairs',
                                    title: '匹数',
                                    width: 80,
                                    halign: 'center',
                                    align: 'right',
                                    formatter: (v, r, i) => v ? parseFloat(v).toFixed(0) : ''
                                },
                                {
                                    field: 'input_kilo',
                                    title: '公斤数',
                                    width: 80,
                                    halign: 'center',
                                    align: 'right',
                                    formatter: (v, r, i) => v ? parseFloat(v).toFixed(1) : ''
                                },
                                {field: 'remarks', title: '备注', width: 120, halign: 'center', align: 'center'},
                            ]];

                            function rk(columns) {
                                $('#input_dtl').datagrid({
                                    striped: true,
                                    fit: true,
                                    sortable: true,
                                    columns: columns,
                                    rownumbers: true,
                                    remoteSort: false,
                                    showFooter: true,
                                    singleSelect: true,
                                    onLoadSuccess: function () {
                                        let rows = $(this).datagrid('getRows') || [];
                                        $(this).datagrid('reloadFooter', [{
                                            createdate: '合计',
                                            input_pairs: calculateSum(rows, 'input_pairs', 0),
                                            countable: calculateSum(rows, 'countable', 0),
                                            input_kilo: calculateSum(rows, 'input_kilo', 1),
                                        }])
                                    },
                                    onResizeColumn: function (f, w) {
                                        columnResizeEvent($('body').data('menu_id'), f, w, null, null, '_rk');
                                    },
                                })

                            }

                            //  发货明细
                            let column2 = [[
                                {field: 'send_date', title: '发货时间', width: 120, halign: 'center', align: 'center'},
                                {field: 'billcode', title: '发货单号', width: 120, halign: 'center', align: 'center'},
                                {field: 'client_name', title: '客户', width: 120, halign: 'center', align: 'center'},
                                {
                                    field: 'countable', title: '发货件数', width: 120, halign: 'center', align: 'right',
                                    formatter: (v, r, i) => v ? parseFloat(v).toFixed(0) : ''
                                },
                                {
                                    field: 'send_pairs',
                                    title: '匹数',
                                    width: 80,
                                    halign: 'center',
                                    align: 'right',
                                    formatter: (v, r, i) => v ? parseFloat(v).toFixed(0) : ''
                                },
                                {
                                    field: 'send_kilo',
                                    title: '重量',
                                    width: 80,
                                    halign: 'center',
                                    align: 'right',
                                    formatter: (v, r, i) => v ? parseFloat(v).toFixed(1) : ''
                                },
                                {field: 'remarks', title: '备注', width: 120, halign: 'center', align: 'center'},
                            ]];

                            function fh(columns) {
                                $('#send_dtl').datagrid({
                                    data: [],
                                    striped: true,
                                    fit: true,
                                    sortable: true,
                                    rownumbers: true,
                                    remoteSort: false,
                                    showFooter: true,
                                    singleSelect: true,
                                    columns: columns,
                                    onLoadSuccess: function () {
                                        let rows = $(this).datagrid('getRows') || [];
                                        $(this).datagrid('reloadFooter', [{
                                            send_date: '合计',
                                            send_pairs: calculateSum(rows, 'send_pairs', 0),
                                            send_kilo: calculateSum(rows, 'send_kilo', 1),
                                        }])
                                    },
                                    onResizeColumn: function (f, w) {
                                        columnResizeEvent($('body').data('menu_id'), f, w, null, null, '_fh');
                                    },
                                })
                            }

                            //  库存调整
                            let column3 = [[
                                {field: 'id', hidden: true, title: '编号'},
                                {field: 'jrkbill_date', width: 135, halign: 'center', align: 'center', title: '日期'},
                                {field: 'bill_type', width: 100, halign: 'center', align: 'center', title: '类型'},
                                {
                                    field: 'pairs',
                                    width: 80,
                                    halign: 'center',
                                    align: 'right',
                                    title: '匹数',
                                    formatter: (v, r, i) => v ? parseFloat(v).toFixed(0) : ''
                                },
                                {
                                    field: 'kilo',
                                    width: 115,
                                    halign: 'center',
                                    align: 'right',
                                    title: '公斤',
                                    formatter: (v, r, i) => v ? parseFloat(v).toFixed(1) : ''
                                },
                                {
                                    field: 'remain_pairs',
                                    width: 90,
                                    halign: 'center',
                                    align: 'right',
                                    title: '库存匹数',
                                    formatter: (v, r, i) => v ? parseFloat(v).toFixed(0) : ''
                                },
                                {
                                    field: 'remain_kilo',
                                    width: 115,
                                    halign: 'center',
                                    align: 'right',
                                    title: '库存公斤',
                                    formatter: (v, r, i) => v ? parseFloat(v).toFixed(1) : ''
                                },
                                {field: 'remarks', width: 200, halign: 'center', align: 'center', title: '备注'},
                            ]];

                            function kc(columns) {
                                $('#storage_dtl').datagrid({
                                    data: [],
                                    striped: true,
                                    fit: true,
                                    sortable: true,
                                    rownumbers: true,
                                    remoteSort: false,
                                    showFooter: true,
                                    singleSelect: true,
                                    columns: columns,
                                    onDblClickRow: function (index, row) {
                                        if (row != undefined && row['id']) {
                                            let loadData = {
                                                id: row.id,
                                                billDate: kendo.toString(kendo.parseDate(row['order_date']), 'yyyy-MM-dd HH:mm:ss') || '',
                                                planId: row['plan_id'],
                                                billType: row['bill_type'],
                                                pairs: row['pairs'],
                                                qty: row['kilo'],
                                                price: row['price'] || 0,
                                                countable: row['countable'],
                                                clientName: row['client_name'],
                                                remarks: row['remarks'],
                                            }
                                            btn_add_modify(loadData);
                                        }
                                    },
                                    onLoadSuccess: function () {
                                        let rows = $(this).datagrid('getRows') || [];
                                        $(this).datagrid('reloadFooter', [{
                                            plan_date: '合计',
                                            pairs: calculateSum(rows, 'pairs', 0),
                                            kilo: calculateSum(rows, 'kilo', 1),
                                        }])
                                    },
                                    onResizeColumn: function (f, w) {
                                        columnResizeEvent($('body').data('menu_id'), f, w, null, null, '_kc');
                                    },
                                });
                            }

                            setDatagridColumnOpts($('body').data('menu_id'), $('#input_dtl'), column1, rk, '_rk');
                            setDatagridColumnOpts($('body').data('menu_id'), $('#send_dtl'), column2, fh, '_fh');
                            setDatagridColumnOpts($('body').data('menu_id'), $('#storage_dtl'), column3, kc, '_kc');
                            $.get('dyeFabricPlan/report/detail/' + dataItem.id, {}, data => {
                                if (data) {
                                    $('#input_form').form('load', data);
                                    $('#input_dtl').datagrid('loadData', data['inputDtl'] || []);
                                    $('#send_dtl').datagrid('loadData', data['sendDtl'] || []);
                                    $('#storage_dtl').datagrid('loadData', data['storageDtl'] || []);
                                }
                            }, 'json');
                            $.parser.onComplete = function () {
                            }
                        };
                        $.parser.parse(dialog);
                        break;
                }
            },
        });
        div.menu('show', {
            left: e.pageX,
            top: e.pageY
        })
    });
}

// 更新库存
function btn_update_storage() {
    let kGrid = $('#v_grid').data('kendoGrid');
    let dataItem = kGrid.dataItem(kGrid.select());
    let formData = $('#input_form').serializeObject() || {};
    if (!dataItem) {
        $.messager.alert('提示', '不允许更新库存', 'warning');
        return;
    }
    $.messager.confirm('提示', '是否确定更新库存?', function (r) {
        if (r) {
            $.post('cpAdjust/insertSelective/adjust',
                {
                    planId: dataItem.id,
                    pairs: formData['remainPairs'] || 0,
                    qty: formData['remainKilo'] || 0
                },
                function (data) {
                    if (data.success) {
                        $.get(`dyeFabricPlan/report/detail/${dataItem.id}`, {}, data => {
                            if (data) {
                                $('#input_form').form('load', data);
                                $('#input_dtl').datagrid('loadData', data['inputDtl'] || []);
                                $('#send_dtl').datagrid('loadData', data['sendDtl'] || []);
                                $('#storage_dtl').datagrid('loadData', data['storageDtl'] || []);
                            }
                        }, 'json');
                    } else {
                        $.messager.alert('提示', data.message, 'error');
                    }
                }, 'json');
        }
    })
}

function btn_add_modify(loadParam) {
    let kGrid = $('#v_grid').data('kendoGrid');
    let dataItem = kGrid.dataItem(kGrid.select());
    if (dataItem == undefined) {
        return;
    }
    let formData = $('#input_form').serializeObject() || {};
    let loadData = (loadParam && typeof loadParam == 'object') ? loadParam : {
        planId: dataItem.id,
        billDate: getCurrentDate(0, 'yyyy-MM-dd HH:mm:ss'),
        clientName: formData['contactCompanyName'],
        countable: 0,
        price: dataItem['greyfabric_price'] || 0
    };
    let column = [
        {
            title: '主键',
            field: 'id',
            hidden: true,
        }, {
            title: '计划单编号',
            field: 'planId',
            hidden: true,
        }, {
            title: '日期',
            field: 'billDate',
            editor: {type: 'datetimebox', options: {required: true}}
        },
        {
            title: '类型',
            field: 'billType',
            id: 'inputBillType',
            editor: {
                type: 'combobox',
                options: {
                    required: true,
                    data: [{text: '手动入库'}, {text: '盘点'}, {text: '手动出库'}],
                    textField: 'text',
                    valueField: 'text',
                    panelHeight: 'auto',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                        if (!$(this).combobox('getValue')) {
                            let loadData = $(this).combobox('getData') || [];
                            let valueField = $(this).combobox('options').valueField;
                            $(this).combobox('setValue', loadData [0][valueField]);
                        }
                    },
                }
            }
        },
        {title: '匹数', field: 'pairs', editor: {type: 'numberbox', options: {precision: 0, required: true}}},
        {title: '公斤', field: 'qty', editor: {type: 'numberbox', options: {precision: 1, required: true}}},
        {
            title: '供货单位',
            field: 'clientName',
            id: 'inputClientName',
            editor: {
                type: 'combobox', options: {
                    url: 'contactCompany/listQuery', method: 'GET',
                    textField: 'companyName', valueField: 'companyName',
                    panelHeight: 300
                }
            }
        },
        {
            title: '件数', field: 'countable', editor: {
                type: 'numberbox', options: {
                    precision: 0, min: 0
                }
            }
        },
        {title: '单价', field: 'price', id: 'inputPrice', editor: {type: 'numberbox', options: {precision: 2,}}},
        {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {}}},
    ];
    $.formDialog.open({
        mode: loadData['id'] ? '修改调整' : '新增调整',
        columns: column,
        loadData: loadData,
        width: 330,
        height: 380,
        onOpen: function () {
            function exec(newVal) {
                if (newVal != '手动出库') {
                    $('#inputPrice').numberbox('clear');
                    $('#inputPrice').numberbox({required: false});
                    $('#inputPrice').numberbox('readonly', true);
                } else {
                    $('#inputPrice').numberbox({required: true})
                    $('#inputPrice').numberbox('readonly', false);
                    let formData = $('#input_form').serializeObject() || {};
                    if (!$('#inputPrice').numberbox('getValue')) {
                        $('#inputPrice').numberbox('setValue', formData['price'] || 0);
                    }
                }
            }

            let _value = $('#inputBillType').combobox('getValue');
            $('#inputBillType').combobox({
                onChange: function (newVal, oldVal) {
                    exec(newVal);
                },
                value: _value
            });
            exec(_value);
        },
        buttons: [{
            text: '保存',
            iconCls: 'icon-save',
            handler: e => {
                let dialog = $('.form-dialog-edit');
                if (!$(dialog.find('form')).form('validate')) {
                    return;
                }
                let param = $(dialog.find('form')).serializeObject();
                let url = '';
                if (param.id) {
                    url = 'cpAdjust/updateSelective';
                } else {
                    url = 'cpAdjust/insertSelective';
                }
                //  保存
                $.messager.progress({
                    title: '',
                    msg: '请稍等...',
                    interval: 5000
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: param,
                    success: function (data) {
                        if (data.success) {
                            $.formDialog.disableEditing();
                            $.formDialog.enableBtn(['退出']);
                            $(dialog.find('form')).eq(0).form('load', data.data);
                            $.messager.show({title: '提示', msg: data.message});
                            $.get('dyeFabricPlan/report/detail/' + dataItem.id, {}, function (data) {
                                if (data) {
                                    $('#input_form').form('load', data);
                                    $('#input_dtl').datagrid('loadData', data['inputDtl'] || []);
                                    $('#send_dtl').datagrid('loadData', data['sendDtl'] || []);
                                    $('#storage_dtl').datagrid('loadData', data['storageDtl'] || []);
                                }
                            }, 'json');
                        } else {
                            $.messager.alert('提示', data.message, 'warning');
                        }
                    },
                    complete: function () {
                        $.messager.progress('close');
                    }
                })
            }
        }, {
            text: '退出',
            iconCls: 'icon-cancel',
            handler: e => {
                $.formDialog.close();
            }
        }],
    });
}

/**
 * 2021年8月29日 16:21:26 chanchaw
 * 删除调整
 */
function btn_remove_modify() {
    let selected = $('#storage_dtl').datagrid('getSelected');
    if (selected == undefined) {
        $.messager.alert('提示', '未选中！', 'warning');
        return;
    }
    if (!selected['id']) {
        $.messager.alert('提示', '禁止删除非手动添加的调整！', 'warning');
        return;
    }
    $.messager.confirm('提示', '是否确定删除调整？', function (r) {
        if (r) {
            $.ajax({
                url: 'cpAdjust/deleteByPk',
                data: {id: selected.id},
                type: 'POST',
                dataType: 'json',
            }).then(function (data) {
                if (data.success) {
                    $.messager.show({title: '提示', msg: data.message});
                    return $.ajax({
                        url: 'dyeFabricPlan/report/detail/' + selected['plan_id'],
                        dataType: 'json',
                        type: 'GET'
                    })
                } else {
                    $.messager.alert('提示', data.message, 'warning');
                }
            }).then(function (data) {
                if (data) {
                    $('#input_form').form('load', data);
                    $('#input_dtl').datagrid('loadData', data['inputDtl'] || []);
                    $('#send_dtl').datagrid('loadData', data['sendDtl'] || []);
                    $('#storage_dtl').datagrid('loadData', data['storageDtl'] || []);
                }
            });
        }
    });
}

//  作废
function btn_cancel() {
    let kGrid = $('#v_grid').data('kendoGrid');
    let dataItem = kGrid.dataItem(kGrid.select());
    if (dataItem != undefined) {
        $.messager.confirm('提示', '是否确认作废', r => {
            if (r)
                $.post('dyeFabricPlan/cancelByPk', {id: dataItem.id}, function (data) {
                    if (data.success) {
                        btn_search();
                        $.messager.show({title: '提示', msg: data.message});
                    } else {
                        $.messager.alert('提示', data.message, 'warning');
                    }
                }, 'json');
        })
    }
}

//  清楚过滤
function btn_cancel_filter() {
    $('#v_grid').data('kendoGrid').dataSource.filter({});
}

//  查询
function btn_search() {
    $('#v_grid').data('kendoGrid').dataSource.read({
        start: $('#start').datebox('getValue'),
        end: $('#end').datebox('getValue'),
        markFinished: (function () {
            let checked = $('#state').switchbutton('options').checked;
            return checked ? checked : null;
        }),
        textField: $('#column').combobox('getValue'),
        valueField: $('#value').textbox('getValue'),
    })
}

//  标记完成
function btn_finish() {
    let kGrid = $('#v_grid').data('kendoGrid');
    let dataItem = kGrid.dataItem(kGrid.select());
    if (dataItem == undefined) {
        $.messager.alert('提示', '未选中!', 'warning');
        return;
    }
    $.messager.confirm('提示', '是否完工计划单:' + dataItem.plan_code + '?', function (r) {
        if (r) {
            $.post('dyeFabricPlan/finished', {id: dataItem.id, finished: true}, function (data) {
                if (data.success) {
                    btn_search();
                } else {
                    $.messager.alert('提示', data.message, 'error');
                }
            }, 'json');
        }
    })
}

//  删除
function btn_delete() {
    let kGrid = $('#v_grid').data('kendoGrid');
    let dataItem = kGrid.dataItem(kGrid.select());
    if (dataItem == undefined) {
        $.messager.alert('提示', '未选中!', 'warning');
        return;
    }
    $.messager.confirm('提示', '是否删除计划?', function (r) {
        if (r) {
            $.post('dyeFabricPlan/cancelByPk', {id: dataItem.id}, function (data) {
                if (data.success) {
                    btn_search();
                } else {
                    $.messager.alert('提示', data.message, 'error');
                }
            }, 'json');
        }
    })
}

function btn_cancel_finish() {
    let kGrid = $('#v_grid').data('kendoGrid');
    let dataItem = kGrid.dataItem(kGrid.select());
    if (dataItem == undefined) {
        $.messager.alert('提示', '未选中!', 'warning');
        return;
    }
    $.messager.confirm('提示', '是否取消完工?', function (r) {
        if (r) {
            $.post('dyeFabricPlan/finished', {id: dataItem.id, finished: false}, function (data) {
                if (data.success) {
                    btn_search();
                } else {
                    $.messager.alert('提示', data.message, 'error');
                }
            }, 'json');
        }
    })
}

//  打印
function btn_print() {
    let kGrid = $("#v_grid").data("kendoGrid");
    let selections = kGrid.select();
    if (selections.length <= 0) {
        $.messager.alert('提示', '请选中计划!', 'warning')
        return;
    }
    let arr = new Set();
    $(selections).each(function () {
        arr.add(kGrid.dataItem(this)['id']);
    })
    let reportlets = [];
    $.get('customize/getDatabase', {}, data => {
        if (data.success) {
            arr.forEach(item => {
                reportlets.push({
                    reportlet: data.data + '/发货结算通知单_成品.cpt',
                    id: item,
                    database: data.data
                })
            });
            window.parent.FR.doURLPDFPrint({
                url: $(window.parent.document.body).data('report_url'),
                isPopUp: false,
                data: {
                    reportlets: reportlets
                }
            })
        }
    }, 'json');
}

//  新增计划
function btn_add() {
    //  新增计划
    $.formDialog.open({
        mode: '编辑',
        columns: planColumns,
        width: 692,
        height: 'auto',
        buttons: [{
            text: '保存当前模板',
            iconCls: 'icon-plugin_link',
            handler: e => {
                let dialog = $('.form-dialog-edit');
                let param = $(dialog.find('form')).serializeObject();
                $.messager.progress({
                    title: '',
                    msg: '请稍等...',
                    interval: 5000
                });
                $.ajax({
                    url: 'dyeFabricPlanTemplate/updateSelective/default',
                    type: 'POST',
                    dataType: 'json',
                    data: param,
                    success: function (data) {
                        if (data.success) {
                            $.messager.show({title: '提示', msg: data.message});
                        } else {
                            $.messager.alert('提示', data.message, 'warning');
                        }
                    },
                    complete: function () {
                        $.messager.progress('close');
                    }
                })
            }
        },
        {
            text: '刷新缓存', iconCls: 'icon-reload', handler: e => {
                $('input').each(function (item, index) {
                    try {
                        if ($(this).hasClass('easyui-combobox')) {
                            $(this).combobox('reload');
                        } else if ($(this).hasClass('easyui-combogrid')) {
                            $(this).combogrid('grid').datagrid('reload');
                        } else if ($(this).hasClass('easyui-combotree')) {
                            $(this).combotree('tree').tree('reload');
                        }
                    } catch (e) {
                        console.table(e);
                    }
                });
            }
        },
        {
            text: '保存并新增',
            iconCls: 'icon-add',
            handler: e => {
                let dialog = $('.form-dialog-edit');
                if (!$(dialog.find('form')).form('validate')) {
                    return;
                }
                let param = $(dialog.find('form')).serializeObject();
                let url = '';
                if (param.id) {
                    url = 'dyeFabricPlan/updateSelective';
                } else {
                    url = 'dyeFabricPlan/insertSelective';
                }
                //  保存
                $.messager.progress({
                    title: '',
                    msg: '请稍等...',
                    interval: 5000
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(param),
                    contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            btn_search();
                            loadPlanForm($('.form-dialog-edit form'));
                            $.messager.show({title: '提示', msg: data.message});
                        } else {
                            $.messager.alert('提示', data.message, 'warning');
                        }
                    },
                    complete: function () {
                        $.messager.progress('close');
                    }
                })
            }
        },
        {
            text: '复制',
            iconCls: 'icon-page_copy',
            handler: e => {
                let dialog = $('.form-dialog-edit');
                $(dialog.find('form')).form('load', {
                    id: null,
                    planCode: ''
                });
                $.formDialog.enableEditing();
                $.formDialog.enableBtn(['保存并新增', '保存', '退出']);
            }
        },
        {
            text: '修改',
            iconCls: 'icon-edit',
            handler: e => {
                $.formDialog.enableEditing();
                $.formDialog.enableBtn(['保存并新增', '保存', '退出']);
            }
        },
        {
            text: '保存',
            iconCls: 'icon-save',
            handler: e => {
                let dialog = $('.form-dialog-edit');
                if (!$(dialog.find('form')).form('validate')) {
                    return;
                }
                let param = $(dialog.find('form')).serializeObject();
                let url = '';
                if (param.id) {
                    url = 'dyeFabricPlan/updateSelective';
                } else {
                    url = 'dyeFabricPlan/insertSelective';
                }
                //  保存
                $.messager.progress({
                    title: '',
                    msg: '请稍等...',
                    interval: 5000
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(param),
                    contentType: 'application/json',
                    success: function (data) {
                        if (data.success) {
                            btn_search();
                            $.formDialog.disableEditing();
                            $.formDialog.enableBtn(['复制', '修改', '退出']);
                            $(dialog.find('form')).eq(0).form('load', data.data);
                            $.messager.show({title: '提示', msg: data.message});
                        } else {
                            $.messager.alert('提示', data.message, 'warning');
                        }
                    },
                    complete: function () {
                        $.messager.progress('close');
                    }
                })
            }
        },
        {
            text: '退出',
            iconCls: 'icon-cancel',
            handler: e => {
                $.formDialog.close();
            }
        }],
    });
    loadPlanForm($('.form-dialog-edit form'));
    $.formDialog.disableBtn(['修改', '复制']);
}

//  作废查询
function btn_cancel_search() {
    $.createSimpleDialog.create({
        title: '成品计划作废查询',
        width: 746,
        height: 405,
        buttons: [
            {
                text: '还原', iconCls: 'icon-add',
                handler: e => {
                    let selected = $.createSimpleDialog.getDataGrid().datagrid('getSelected');
                    if (selected == undefined) {
                        messagerAlert('请选中作废单号');
                        return;
                    }
                    messagerConfirm('是否确定恢复？', r => {
                        if (r) {
                            $.ajax({
                                url: 'dyeFabricPlan/recoverByPk',
                                data: {id: selected.id},
                                dataType: 'json',
                                type: 'POST',
                                success: function (data) {
                                    if (data.success) {
                                        $.createSimpleDialog.destroy();
                                        btn_search();
                                    } else {
                                        messagerAlert(data.message);
                                    }
                                }
                            })
                        }
                    })
                }
            },
            {
                text: '删除', iconCls: 'icon-cancel', handler: e => {
                    let datagrid = $.createSimpleDialog.getDataGrid();
                    let selected = datagrid.datagrid('getSelected');
                    if (selected == undefined) {
                        messagerAlert('请选中作废单号');
                        return;
                    }
                    messagerConfirm('是否确定删除？删除后不可恢复？', r => {
                        if (r) {
                            $.ajax({
                                url: 'dyeFabricPlan/deleteByPk',
                                data: {id: selected.id},
                                type: 'POST',
                                dataType: 'json',
                                success: function (data) {
                                    if (data.success) {
                                        datagrid.datagrid('reload');
                                        btn_search();
                                    } else {
                                        messagerAlert(data.message);
                                    }
                                }
                            })
                        }
                    })
                }
            },
            {
                text: '取消', iconCls: 'icon-no', handler: e => {
                    $.createSimpleDialog.destroy();
                }
            }],
        url: 'dyeFabricPlan/report/canceled',
        columns: [[
            {title: '编号', hidden: true, width: 100, field: 'id', align: 'center', halign: 'center'},
            {title: '计划单号', width: 100, field: 'planCode', align: 'center', halign: 'center'},
            {title: '客户', width: 100, field: 'companyName', align: 'center', halign: 'center'},
            {title: '坯布规格', width: 351, field: 'materialSpecification', align: 'center', halign: 'center'},
            {title: '计划数量', width: 70, field: 'planKilo', align: 'right', halign: 'center'},
            {title: '操作员', width: 80, field: 'cancelUserName', align: 'center', halign: 'center'},
        ]],
    })
}

function btn_open_column_list() {
    let kendoGridID = '#v_grid';
    let options = $(kendoGridID).data('kendoGrid').getOptions().columns;
    if ($('#btn_open_column_list').length <= 0) {
        let div = $(`<div data-options="resizable:true,iconCls:'icon-text_columns',width:200,height:300,modal:true,title:'列'" 
                          class="easyui-dialog" id="btn_open_column_list">
                   </div>`);
        let form = $('<form style="width: 100%;height: 100%;"></form>')
        for (let i of options) {
            if (i.field) {
                let checkbox = $(`<div style="width: 100%;height: auto;padding: 5px;">
                <input class="easyui-checkbox" name="${i.field}" data-options="label:'${i.title}',
                labelWidth:120,value:true,name:'${i.field}'">
                </div>`);
                form.append(checkbox);
            }
        }
        let container = $(`<div></div>`);
        form.appendTo(div);
        div.appendTo(container);
        $('body').append(container);
        $.parser.parse($('#btn_open_column_list').parent());
        let formData = {};
        options.map(item => {
            formData[item['field']] = item['hidden'] == undefined ? true : !item['hidden'];
        })
        $('#btn_open_column_list form').eq(0).form('load', formData);
        $('#btn_open_column_list form .easyui-checkbox').each(function () {
            //  绑定事件
            $(this).checkbox({
                onChange: function (checked) {
                    let opts = $(this).checkbox('options');
                    if (checked) {
                        $(kendoGridID).data('kendoGrid').showColumn(opts.name);
                    } else {
                        $(kendoGridID).data('kendoGrid').hideColumn(opts.name);
                    }
                }
            })
        })
    } else {
        $('#btn_open_column_list').dialog('open');
    }
}

function btn_export_excel() {
    $('#v_grid').data('kendoGrid').saveAsExcel();
}

function btn_close() {
    window.parent.closeTab();
}

//  消审
function btn_cancel_audit() {
    let kGrid = $('#v_grid').data('kendoGrid');
    let selected = kGrid.select();
    if (selected == undefined) {
        messagerAlert('请选中！');
        return;
    }
    let dataItems = [];
    selected.each(function () {
        let dataItem = kGrid.dataItem(this);
        if (dataItem.id) {
            dataItems.push(dataItem.id);
        }
    });
    if (dataItems.length <= 0) {
        messagerAlert('请选中！');
        return;
    }
    messagerConfirm('确定消审？', r => {
        if (r) {
            $.ajax({
                url: 'dyeFabricPlan/cancelAudit',
                data: {id: dataItems},
                type: 'POST',
                dataType: 'json',
                traditional: true,
            }).then(function (data) {
                if (data.success) {
                    btn_search();
                } else {
                    messagerAlert(data.message);
                }
            })
        }
    })
}

function loadPlanForm(form) {
    $.get('dyeFabricPlanTemplate/listQuery/default', {}, data => {
        if (Object.getOwnPropertyNames(data).length > 0) {
            delete data.id;
            $(form).form('load', data);
        }
    }, 'json');
}

function stateChange(checked) {
    setState(checked);
    btn_search();
}

function setState(state) {
    if (state) {
        $('#start').datebox('enable');
        $('#end').datebox('enable');
    } else {
        $('#start').datebox('disable');
        $('#end').datebox('disable');
    }
}

//选中事件
function initCombobox(options) {
    var columns = options.columns;
    columns.splice(0, 1);
    $('#column').combobox({
        valueField: 'field',
        textField: 'title',
        data: columns,
        editable: false
    })
}