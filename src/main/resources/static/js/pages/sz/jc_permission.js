$(function () {

})
/**
 * 角色权限分配表
 */
$('#grid_03').edatagrid({
    updateUrl: 'sysPermission/updateSelective',
    method: 'GET',
    striped: true,
    fit: true,
    sortable: true,
    rownumbers: true,
    autoSave: true,
    remoteSort: false,
    fitColumns: true,
    idField: 'id',
    showFooter: true,
    singleSelect: true,
    columns: [[
        {title: '编号', field: 'id', width: 50, hidden: true, sortable: true},
        {title: '角色编号', field: 'sysRoleId', width: 50, hidden: true, sortable: true},
        {title: '菜单编号', field: 'sysMenuId', width: 50, hidden: true, sortable: true},
        {
            title: '', field: 'checkAll', width: 50, sortable: true, formatter: (v, r, i) => '', editor: {
                type: 'checkbox', options: {
                    on: "1", off: "0",
                }
            }, halign: 'center', align: 'center'
        },
        {
            title: '可见',
            field: 'sysShow',
            width: 50,
            sortable: true,
            formatter: formatter,
            editor: {type: 'checkbox', options: {on: "1", off: "0"}},
            halign: 'center',
            align: 'center'
        },
        {
            title: '新增',
            field: 'sysAdd',
            width: 50,
            sortable: true,
            formatter: formatter,
            editor: {type: 'checkbox', options: {on: "1", off: "0"}},
            halign: 'center',
            align: 'center'
        },
        {
            title: '修改',
            field: 'sysModify',
            width: 50,
            sortable: true,
            formatter: formatter,
            editor: {type: 'checkbox', options: {on: "1", off: "0"}},
            halign: 'center',
            align: 'center'
        },
        {
            title: '删除',
            field: 'sysDelete',
            width: 50,
            sortable: true,
            formatter: formatter,
            editor: {type: 'checkbox', options: {on: "1", off: "0"}},
            halign: 'center',
            align: 'center'
        },
        {
            title: '审核',
            field: 'sysAudit',
            width: 50,
            sortable: true,
            formatter: formatter,
            editor: {type: 'checkbox', options: {on: "1", off: "0"}},
            halign: 'center',
            align: 'center'
        },
        {
            title: '打印',
            field: 'sysPrint',
            width: 50,
            sortable: true,
            formatter: formatter,
            editor: {type: 'checkbox', options: {on: "1", off: "0"}},
            halign: 'center',
            align: 'center'
        },
        {
            title: '导出',
            field: 'sysExport',
            width: 50,
            sortable: true,
            formatter: formatter,
            editor: {type: 'checkbox', options: {on: "1", off: "0"}},
            halign: 'center',
            align: 'center'
        },
        {
            title: '单价',
            field: 'sysPrice',
            width: 50,
            sortable: true,
            formatter: formatter,
            editor: {type: 'checkbox', options: {on: "1", off: "0"}},
            halign: 'center',
            align: 'center'
        },
        {
            title: '金额',
            field: 'sysMoney',
            width: 50,
            sortable: true,
            formatter: formatter,
            editor: {type: 'checkbox', options: {on: "1", off: "0"}},
            halign: 'center',
            align: 'center'
        },
    ]],
    onSuccess: function (index, row) {
        if (row.success) {
            $.messager.show({title: '提示', msg: row.message});
        } else {
            $.messager.show({title: '提示', msg: row.message});
            $(this).edatagrid('reload');
        }
    },
    onEdit: function (index, row) {
        let editors = $(this).edatagrid('getEditors', index);
        $(editors[0].target).on('click', function (e) {
            if ($(this).is(':checked')) {
                for (let i = 1; i < editors.length; i++) {
                    $(editors[i].target).prop('checked', true);
                }
            } else {
                for (let i = 1; i < editors.length; i++) {
                    $(editors[i].target).prop('checked', false);
                }
            }
        })
    }
});

$('#grid_02').tree({
    url: 'sysMenu/tree',
    method: 'GET',
    animate: true,
    lines: true,
    onLoadSuccess: function (node, data = []) {
        if (data.length > 0) {
            let n = $(this).tree('find', data[0].id);
            //调用选中事件
            $(this).tree('select', n.target);
        }
    },
    onClick: function (node) {
        let selectRow = $('#grid_01').datagrid('getSelected');
        if (!selectRow) return;
        let roleId = selectRow.id;
        let menuId = node.id;
        $('#grid_03').datagrid('options').url = 'sysPermission/listQuery';
        $('#grid_03').datagrid('options').queryParams = {
            sysMenuId: menuId,
            sysRoleId: roleId
        }
        $('#grid_03').datagrid('reload');
    }
});

$('#grid_01').datagrid({
    url: 'sysRole/listQuery',
    queryParams: {
        // roleState: 1
    },
    striped: true,
    fit: true,
    sortable: true,
    rownumbers: true,
    method: 'GET',
    remoteSort: false,
    idField: 'id',
    showFooter: true,
    fitColumns: true,
    singleSelect: true,
    columns: [[
        {title: '角色编号', field: 'id', width: 60, sortable: true, hidden: true, halign: 'center', align: 'center'},
        {title: '角色名称', field: 'roleName', width: 80, sortable: true, halign: 'center', align: 'center'},
        {
            title: '是否管理员', field: 'roleAdminSign', width: 60, sortable: true, halign: 'center', align: 'center',
            formatter: (v, r, i) => v == 1 ? '是' : '否'
        },
        {
            title: '状态', field: 'roleState', width: 60, sortable: true, halign: 'center', align: 'center',
            formatter: (v, r, i) => v == 1 ? '' : '失效'
        },
    ]],
    onLoadSuccess: function (data) {
        if (data) {
            $(this).datagrid('selectRow', 0);
            let roleId = $(this).datagrid('getSelected').id;
            let selectedNode = $('#grid_02').tree('getSelected');
            if (selectedNode == undefined) return;
            let menuId = selectedNode.id;
            $('#grid_03').datagrid('options').url = 'sysPermission/listQuery';
            $('#grid_03').datagrid('options').queryParams = {
                sysMenuId: menuId,
                sysRoleId: roleId
            }
            $('#grid_03').datagrid('reload');
        }
    },
    onClickRow: function (index, row) {
        let roleId = $(this).datagrid('getSelected').id;
        let menuId = $('#grid_02').tree('getSelected').id;
        $('#grid_03').datagrid('options').url = 'sysPermission/listQuery';
        $('#grid_03').datagrid('options').queryParams = {
            sysMenuId: menuId,
            sysRoleId: roleId
        }
        $('#grid_03').datagrid('reload');
    }
});

function formatter(v, r, i) {
    return v == 1 ? '是' : '否';
}

/**按钮组 */
/**保存 */
function btn_save() {

}

/**关闭 */
function btn_close() {
    window.parent.closeTab();
}

//角色操作
function btn_role(item) {
    let itemName = item.text;
    switch (itemName) {
        case '新增角色':
            $('#dia_role').dialog('open');
            $('#dia_role').dialog('setTitle', '新增角色');
            $('#dia_role').dialog('options').title = '新增角色';
            break;
        case '修改角色':
            let selected = $('#grid_01').datagrid('getSelected');
            if (selected == undefined) return;
            $('#dia_role').dialog('open');
            $('#dia_role').dialog('setTitle', '修改角色');
            $('#dia_role').dialog('options').title = '修改角色';
            $('#dia_role').find('form').form('load', {
                id: selected.id,
                roleName: selected.roleName,
                roleAdminSign: selected.roleAdminSign,
                roleWorkshopOnly: selected.roleWorkshopOnly,
            });
            break;
        case '删除角色':
            let roleSelected = $('#grid_01').datagrid('getSelected');
            if (roleSelected == undefined) return;
            $.messager.confirm('提示', '是否删除该角色?', function (r) {
                if (r) {
                    $.post('sysRole/deleteByPk', {id: roleSelected.id}, function (data) {
                        if (data.success) {
                            let index = $('#grid_01').datagrid('getRowIndex', roleSelected.id);
                            $('#grid_01').datagrid('deleteRow', index);
                        }
                        $.messager.show({title: '提示', msg: data.message})
                    }, 'json');
                }
            })
            break;
        default:
    }
}

//菜单操作
function btn_menu(item) {
    let itemName = item.text;
    switch (itemName) {
        case '新增菜单':
            $('#dia_menu').dialog('open');
            $('#dia_menu').dialog('setTitle', '新增菜单');
            $('#dia_menu').dialog('options').title = '新增菜单';
            break;
        case '修改菜单':
            var selected = $('#grid_02').tree('getSelected');
            if (selected == undefined) return;
            $('#dia_menu').dialog('open');
            $('#dia_menu').dialog('setTitle', '修改菜单');
            $('#dia_menu').dialog('options').title = '修改菜单';
            let parent = $('#grid_02').tree('getParent', selected.target);
            $('#dia_menu').find('form').form('load', {
                id: selected.id,
                menuName: selected.text,
                menuPath: selected.attribute.path,
                menuIcon: selected.iconCls,
                serialNo: selected.serialNo,
                parentId: parent ? parent.id : null,
            });
            break;
        case '删除菜单':
            var selected = $('#grid_02').tree('getSelected');
            if (selected == undefined) return;
            $.messager.confirm('提示', '是否删除该菜单?', function (r) {
                if (r) {
                    $.post('sysMenu/deleteByPk', {id: selected.id}, function (data) {
                        if (data.success) {
                            $('#grid_02').tree('remove', selected.target);
                        }
                        $.messager.show({title: '提示', msg: data.message})
                    }, 'json');
                }
            })
            break;
        default:
    }
}

// 角色编辑面板按钮
function btn_role_save() {
    let title = $('#dia_role').dialog('options').title;
    let url = "";
    if (!title) return;
    switch (title) {
        case '新增角色':
            url = "sysRole/insertSelective";
            break;
        case '修改角色':
            url = "sysRole/updateSelective";
            break;
    }
    $('#dia_role').find('form').form('submit', {
        url: url,
        onSubmit: function (param) {
            return $(this).form('validate');
        },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.success) {
                $('#dia_role').dialog('close');
                $('#grid_01').datagrid('reload');
            }
            $.messager.show({title: '提示', msg: data.message})
        }
    });
}

function btn_role_cancel() {
    $('#dia_role').dialog('close');
}

// 菜单编辑面板按钮
function btn_menu_save() {
    let title = $('#dia_menu').dialog('options').title;
    let url = "";
    if (!title) return;
    switch (title) {
        case '新增菜单':
            url = "sysMenu/insertSelective";
            break;
        case '修改菜单':
            url = "sysMenu/updateSelective";
            break;
    }
    $('#dia_menu').find('form').form('submit', {
        url: url,
        onSubmit: function (param) {
            return $(this).form('validate');
        },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.success) {
                $('#dia_menu').dialog('close');
                $('#grid_02').tree('reload');
            }
            $.messager.show({title: '提示', msg: data.message})
        }
    });
}

function btn_menu_cancel() {
    $('#dia_menu').dialog('close');
}