$(function () {
    //  按钮生成器
    generateButton($('#north'));
    let columns = [[
        {
            field: 'configField',
            title: '配置名',
            width: 100,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: true}}
        },
        {
            field: 'configValue',
            title: '配置值',
            width: 100,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: true}}
        },
        {
            field: 'configDesc',
            title: '配置说明',
            width: 250,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: false}}
        },
    ]];
    let columnsSub = [[
        {
            field: 'configField',
            title: '配置名',
            width: 100,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: true}}
        },
        {
            field: 'configValue',
            title: '配置值',
            width: 100,
            align: 'right',
            halign: 'center',
            sortable: true,
            editor: {
                type: 'numberbox',
                options: {
                    required: true,
                    precision: 0
                }
            }
        },
        {
            field: 'configDesc',
            title: '配置说明',
            width: 250,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: false}}
        },
    ]]
    let columnsDecimal = [[
        {
            field: 'config_field',
            title: '配置名',
            width: 100,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: true}}
        },
        {
            field: 'config_default',
            title: '配置默认值',
            width: 100,
            align: 'right',
            halign: 'center',
            sortable: true,
            editor: {
                type: 'numberbox',
                options: {
                    required: true,
                    precision: 1
                }
            }
        },
        {
            field: 'config_smallvalue',
            title: '配置最小值',
            width: 100,
            align: 'right',
            halign: 'center',
            sortable: true,
            editor: {
                type: 'numberbox',
                options: {
                    required: true,
                    precision: 1
                }
            }
        },
        {
            field: 'config_bigvalue',
            title: '配置最大值',
            width: 100,
            align: 'right',
            halign: 'center',
            sortable: true,
            editor: {
                type: 'numberbox',
                options: {
                    required: false,
                    precision: 1
                }
            }
        },

        {
            field: 'config_desc',
            title: '配置说明',
            width: 250,
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {type: 'textbox', options: {required: false}}
        },
    ]]
    setDatagridColumnOpts($('body').data('menu_id'), $('#e_grid'), columns, edatagrid);
    setDatagridColumnOpts($('body').data('menu_id'), $('#e_grid_sub'), columnsSub, edatagridsub);
    setDatagridColumnOpts($('body').data('menu_id'), $('#e_grid_decimal'), columnsDecimal, edatagriddecimal);
    bindEnterEvents();
})

function edatagrid(columns) {
    $('#e_grid').edatagrid({
        method: 'GET',
        param: {},
        url: 'sysConfigVarchar/listQuery',
        saveUrl: 'sysConfigVarchar/insertSelective',
        updateUrl: 'sysConfigVarchar/updateSelective',
        destroyUrl: 'sysConfigVarchar/deleteByPk',
        idField: 'configField',
        destroyMsg: {
            norecord: {
                title: '警告',
                msg: '未选中任何数据'
            },
            confirm: {
                title: '确认',
                msg: '确定删除吗?删除后不可恢复!'
            }
        },
        loadFilter: pageFilter,
        pagination: true,
        // pagePosition: 'top',
        pageSize: 200,
        pageNumber: 1,
        pageList: [200, 400, 600, 800, 1000],
        striped: true,
        fit: true,
        sortable: true,
        rownumbers: true,
        remoteSort: false,
        showFooter: true,
        singleSelect: true,
        columns: columns,
        onLoadSuccess: function () {
            $(this).datagrid('enableFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
        },
        onBeforeLoad: function (param) {
            $(this).datagrid('destroyFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('disable');
                }
            })
            return true;
        },
        onSuccess: function (index, row) {
            $(this).edatagrid('reload');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
            } else {
                $(this).edatagrid('enableEditing');
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onDestroy: function (index, row) {
            $(this).edatagrid('reload');
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
            } else {
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onError: function (index, row) {
            $.messager.show({title: '提示', msg: row.message});
        },
        onResizeColumn: function (f, w) {
            columnResizeEvent($('body').data('menu_id'), f, w);
        },
        onEdit: function (index, row) {
            let editors = $(this).edatagrid('getEditors', index);
            let idField = $(this).edatagrid('options').idField;
            for (let editor of editors) {
                if (editor.field == idField) {
                    $(editor.target).textbox('readonly');
                }
            }
        }
    })
}

function edatagridsub(columns) {
    $('#e_grid_sub').edatagrid({
        method: 'GET',
        param: {},
        url: 'sysConfigInt/listQuery',
        saveUrl: 'sysConfigInt/insertSelective',
        updateUrl: 'sysConfigInt/updateSelective',
        destroyUrl: 'sysConfigInt/deleteByPk',
        idField: 'configField',
        destroyMsg: {
            norecord: {
                title: '警告',
                msg: '未选中任何数据'
            },
            confirm: {
                title: '确认',
                msg: '确定删除吗?删除后不可恢复!'
            }
        },
        loadFilter: pageFilter,
        pagination: true,
        // pagePosition: 'top',
        pageSize: 200,
        pageNumber: 1,
        pageList: [200, 400, 600, 800, 1000],
        striped: true,
        fit: true,
        sortable: true,
        rownumbers: true,
        remoteSort: false,
        showFooter: true,
        singleSelect: true,
        columns: columns,
        onLoadSuccess: function () {
            $(this).datagrid('enableFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
        },
        onBeforeLoad: function (param) {
            $(this).datagrid('destroyFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('disable');
                }
            })
            return true;
        },
        onSuccess: function (index, row) {
            $(this).edatagrid('reload');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
            } else {
                $(this).edatagrid('enableEditing');
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onDestroy: function (index, row) {
            $(this).edatagrid('reload');
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
            } else {
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onError: function (index, row) {
            $.messager.show({title: '提示', msg: row.message});
        },
        onResizeColumn: function (f, w) {
            columnResizeEvent($('body').data('menu_id'), f, w);
        },
        onEdit: function (index, row) {
            let editors = $(this).edatagrid('getEditors', index);
            let idField = $(this).edatagrid('options').idField;
            for (let editor of editors) {
                if (editor.field == idField) {
                    $(editor.target).textbox('readonly');
                }
            }
        }
    })
}

function edatagriddecimal(columns) {
    $('#e_grid_decimal').edatagrid({
        method: 'GET',
        param: {},
        url: 'sysConfigDecimal/listQuery',
        saveUrl: 'sysConfigDecimal/insertSelective',
        updateUrl: 'sysConfigDecimal/updateSelective',
        destroyUrl: 'sysConfigDecimal/deleteByPrimaryKey',
        // headers: { contentType: "application/json;charset=UTF-8" },
        idField: 'config_field',
        destroyMsg: {
            norecord: {
                title: '警告',
                msg: '未选中任何数据'
            },
            confirm: {
                title: '确认',
                msg: '确定删除吗?删除后不可恢复!'
            }
        },
        loadFilter: pageFilter,
        pagination: true,
        // pagePosition: 'top',
        pageSize: 200,
        pageNumber: 1,
        pageList: [200, 400, 600, 800, 1000],
        striped: true,
        fit: true,
        sortable: true,
        rownumbers: true,
        remoteSort: false,
        showFooter: true,
        singleSelect: true,
        columns: columns,
        onLoadSuccess: function () {
            $(this).datagrid('enableFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
        },
        onBeforeLoad: function (param) {
            $(this).datagrid('destroyFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('disable');
                }
            })
            return true;
        },
        onSuccess: function (index, row) {
            $(this).edatagrid('reload');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
            } else {
                $(this).edatagrid('enableEditing');
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onDestroy: function (index, row) {
            $(this).edatagrid('reload');
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
            } else {
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onError: function (index, row) {
            $.messager.show({title: '提示', msg: row.message});
        },
        onResizeColumn: function (f, w) {
            columnResizeEvent($('body').data('menu_id'), f, w);
        },
        onEdit: function (index, row) {
            let editors = $(this).edatagrid('getEditors', index);
            let idField = $(this).edatagrid('options').idField;
            for (let editor of editors) {
                if (editor.field == idField) {
                    $(editor.target).textbox('readonly');
                }
            }
        }
    })
}

function btn_search() {
    switch (getSelectedIndex()) {
        case 0:
            $('#e_grid').edatagrid('reload');
            break;
        case 1:
            $('#e_grid_sub').edatagrid('reload');
            break;
        case 2:
            $('#e_grid_decimal').edatagrid('reload');
            break;
    }
}

function btn_add() {
    switch (getSelectedIndex()) {
        case 0:
            $('#e_grid').edatagrid('addRow');
            break;
        case 1:
            $('#e_grid_sub').edatagrid('addRow');
            break;
        case 2:
            $('#e_grid_decimal').edatagrid('addRow');
            break;
    }
}

function btn_modify() {
    let datagrid;
    switch (getSelectedIndex()) {
        case 0:
            datagrid = $('#e_grid');
            break;
        case 1:
            datagrid = $('#e_grid_sub');
            break;
        case 2:
            datagrid = $('#e_grid_decimal');
            break;
    }
    if (datagrid == undefined) return;
    let selected = datagrid.edatagrid('getSelected');
    if (selected != undefined) {
        let index = datagrid.edatagrid('getRowIndex', selected);
        if (index != -1) {
            datagrid.edatagrid('editRow', index);
        }
    } else {
        $.messager.alert('提示', '点击鼠标左键选中行后再执行本功能', 'warning');
    }
}

function btn_delete() {
    let datagrid;
    switch (getSelectedIndex()) {
        case 0:
            datagrid = $('#e_grid');
            break;
        case 1:
            datagrid = $('#e_grid_sub');
            break;
        case 2:
            datagrid = $('#e_grid_decimal');
            break;
    }
    if (datagrid == undefined) return;
    let selected = datagrid.edatagrid('getSelected');
    if (selected != undefined) {
        let index = datagrid.edatagrid('getRowIndex', selected);
        if (index != -1) {
            datagrid.edatagrid('destroyRow', index);
        }
    } else {
        $.messager.alert('提示', '点击鼠标左键选中行后再执行本功能', 'warning');
    }
}

function btn_import() {

}

function btn_save() {
    switch (getSelectedIndex()) {
        case 0:
            $('#e_grid').edatagrid('saveRow');
            break;
        case 1:
            $('#e_grid_sub').edatagrid('saveRow');
            break;
        case 2:
            $('#e_grid_decimal').edatagrid('saveRow');
            break;
    }
}

function btn_export() {
    switch (getSelectedIndex()) {
        case 0:
            exportExcel('#e_grid', '配置1');
            break;
        case 1:
            exportExcel('#e_grid_sub', '配置2');
            break;
        case 2:
            exportExcel('#e_grid_decimal', '配置3');
            break;
    }
}

function getSelectedIndex() {
    let tab = $('#tabs').tabs('getSelected');
    let index = $('#tabs').tabs('getTabIndex', tab);
    return index;
}