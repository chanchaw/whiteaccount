$(function () {
    $('#dialog').dialog({
        title: '账套重置',
        iconCls: 'icon-shape_square_error',
        width: 347,
        height: 255,
        modal: true,
        resizable: false,
        onOpen: onOpen,
        onClose: btn_close
    });
    bindEnterEvents();
})

function onOpen() {
    if ($('#step1').length > 0) return;
    let step1 = $(`
            <div id="step1" style="width: 100%;height: 100%;">
                <div class="easyui-layout" data-options="fit:true,border:false">
                    <div class="span-style" data-options="region:'north',height:73">
                        <span>重置账套分别为业务数据清除及基础资料清除，请选择所将清除的数据类别</span>
                    </div>
                    <div data-options="region:'south',height:'auto'">
                        <div class="padding5">
                        <a class="easyui-linkbutton linkbutton-style" data-options="iconCls:'icon-arrow_right',onClick:btn_next">下一步</a>
                        <a class="easyui-linkbutton linkbutton-style" data-options="iconCls:'icon-cancel',onClick:btn_close">取消</a>
                        </div>
                    </div>
                    <div data-options="region:'center'">
                        <div class="padding5 input-style"><input class="easyui-checkbox" name="type" 
                            data-options="labelPosition:'after',label:'业务数据清除',labelWidth:200,value:1,checked:true,readonly:true,
                            onChange:function(checked){
                                if(!checked){
                                    $(this).checkbox('check');
                                }
                            }"></div>
                        <div class="padding5 input-style"><input class="easyui-checkbox" name="type" 
                            data-options="labelPosition:'after',label:'基础数据清除',labelWidth:200,value:0,checked:false"></div>
                    </div>
                </div>
            </div>
        `);
    $(this).dialog('body').find('form').append(step1);
    $.parser.parse(step1);
    $(this).dialog('center');
}

function btn_close() {
    window.parent.closeTab();
}

function btn_next() {
    $('#step1').hide();
    if ($('#step2').length <= 0) {
        let step2 = $(`
                <div id="step2" style="width: 100%;height: 100%;">
                    <div class="easyui-layout" data-options="fit:true,border:false">
                        <div class="span-style" data-options="region:'north',height:73">
                            <span>重置账套后，所有的数据都将清空及无法恢复，为了慎重起见，请将下面的文本框中输入你的删除口令</span>
                        </div>
                        <div data-options="region:'south',height:'auto'">
                            <div class="padding5">
                            <a class="easyui-linkbutton linkbutton-style" data-options="iconCls:'icon-arrow_left',onClick:btn_prev">上一步</a>
                            <a class="easyui-linkbutton linkbutton-style" data-options="iconCls:'icon-arrow_right',onClick:btn_finish">完成</a>
                            <a class="easyui-linkbutton linkbutton-style" data-options="iconCls:'icon-cancel',onClick:btn_close">取消</a>
                            </div>
                        </div>
                        <div data-options="region:'center'">
                            <div class="padding5"><input class="easyui-passwordbox" name="password" 
                                data-options="label:'删除口令：',width:316,labelWidth:86,required:true,showEye:false"></div>
                            <div class="padding5">
                                <span>按完成后开始备份账套以及重置账套</span>
                            </div>    
                        </div>
                    </div>    
                </div>
    `);
        $('#dialog').dialog('body').find('form').append(step2);
        $.parser.parse(step2);
    }
    $('#step2').show();
    $('#dialog').find('input:visible').focus();
}

function btn_finish() {
    let form = $('#dialog').dialog('body').find('form');
    if (form.form('validate')) {
        let formData = $(form).serializeObject();
        $.messager.progress({title: '提示', text: '请稍候...', interval: 15000});
        $.ajax({
            url: 'system/resetting',
            data: formData,
            type: 'POST',
            traditional: true,
            dataType: 'json',
            success: data => {
                if (data.success) {
                    $.messager.alert('提示', '备份及重置账套完成，请退出后重新登录！', 'info', () => {
                        window.parent.closeTab();
                    });
                } else {
                    $.messager.alert('提示', data.message, 'error');
                }
            },
            complete: function () {
                $.messager.progress('close');
            }
        })
    }
}

function btn_prev() {
    $('#step1').show();
    $('#step2').hide();
}