$(function () {
    //  按钮生成器
    generateButton($('#north'));
    let columns = [[
        {field: 'id', title: '编号', align: 'center', halign: 'center', sortable: true, hidden: true},
        {
            field: 'groupName',
            width: 100,
            title: '角色组',
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {
                type: 'textbox', options: {required: true}
            }
        },
        {
            field: 'roleGroupAdminSign',
            width: 100,
            title: '是否管理员',
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {
                type: 'checkbox', options: {on: '1', off: '0'}
            },
            formatter: (v, r, i) => {
                return v ? `<input checked type="checkbox" readonly onclick="return false">`
                    : `<input type="checkbox" readonly onclick="return false">`;
            }
        },
        {
            field: 'isInitialSelect',
            width: 100,
            title: '是否初始角色',
            align: 'center',
            halign: 'center',
            sortable: true,
            editor: {
                type: 'checkbox', options: {on: '1', off: '0'}
            },
            formatter: (v, r, i) => {
                return v ? `<input checked type="checkbox" readonly onclick="return false">`
                    : `<input type="checkbox" readonly onclick="return false">`;
            }
        },
    ]];
    setDatagridColumnOpts($('body').data('menu_id'), $('#e_grid'), columns, edatagrid);
    edatagridDtl();
    bindEnterEvents()
})

function edatagrid(columns) {
    $('#e_grid').edatagrid({
        method: 'GET',
        param: {},
        url: 'sysRoleGroup/listQuery',
        saveUrl: 'sysRoleGroup/insertSelective',
        updateUrl: 'sysRoleGroup/updateSelective',
        destroyUrl: 'sysRoleGroup/deleteByPk',
        idField: 'id',
        destroyMsg: {
            norecord: {	// when no record is selected
                title: '警告',
                msg: '未选中任何数据'
            },
            confirm: {	// when select a row
                title: '确认',
                msg: '确定删除吗?删除后不可恢复!'
            }
        },
        loadFilter: pageFilter,
        pagination: true,
        // pagePosition: 'top',
        pageSize: 200,
        pageNumber: 1,
        pageList: [200, 400, 600, 800, 1000],
        striped: true,
        fit: true,
        sortable: true,
        rownumbers: true,
        remoteSort: false,
        showFooter: true,
        singleSelect: true,
        columns: columns,
        onLoadSuccess: function () {
            $(this).datagrid('enableFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
        },
        onLoadError: function () {
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
        },
        onBeforeLoad: function (param) {
            $(this).datagrid('destroyFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('disable');
                }
            })
            return true;
        },
        onSuccess: function (index, row) {
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
                sessionStorage.clear();
            } else {
                $.messager.alert('提示', row.message, 'warning')
            }
            $(this).edatagrid('reload');
        },
        onDestroy: function (index, row) {
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
                sessionStorage.clear();
            } else {
                $.messager.alert('提示', row.message, 'warning')
            }
            $(this).edatagrid('reload');
        },
        onError: function (index, row) {
            $.messager.show({title: '提示', msg: row.message});
        },
        onResizeColumn: function (f, w) {
            columnResizeEvent($('body').data('menu_id'), f, w);
        },
        onClickRow: function (index, row) {
            //  更改明细的URL
            $('#e_grid_dtl').edatagrid({
                url: 'sysRoleGroupDtl/listQuery',
                queryParams: {roleGroupId: row.id}
            })
        }
    })
}

function edatagridDtl() {
    $('#e_grid_dtl').edatagrid({
        method: 'GET',
        param: {},
        title: '对应角色',
        url: '',
        saveUrl: 'sysRoleGroupDtl/insertSelective',
        updateUrl: 'sysRoleGroupDtl/updateSelective',
        destroyUrl: 'sysRoleGroupDtl/deleteByPk',
        idField: 'id',
        destroyMsg: {
            norecord: {	// when no record is selected
                title: '警告',
                msg: '未选中任何数据'
            },
            confirm: {	// when select a row
                title: '确认',
                msg: '确定删除吗?删除后不可恢复!'
            }
        },
        // loadFilter: pageFilter,
        pagination: true,
        pageSize: 200,
        pageNumber: 1,
        pageList: [200, 400, 600, 800, 1000],
        striped: true,
        fit: true,
        sortable: true,
        rownumbers: true,
        remoteSort: false,
        showFooter: true,
        singleSelect: true,
        columns: [[
            {field: 'id', title: '编号', align: 'center', halign: 'center', sortable: true, hidden: true},
            {
                field: 'roleGroupId',
                width: 100,
                title: '角色组',
                align: 'center',
                halign: 'center',
                sortable: true,
                editor: {
                    type: 'combobox', options: {
                        url: 'sysRoleGroup/listQuery',
                        method: 'GET',
                        textField: 'groupName',
                        valueField: 'id',
                        limitToList: true,
                        panelHeight: 200,
                        required: true
                    }
                },
                formatter: (v, r, i) => {
                    if (!v) return '';
                    let prefix = 'sz_role_group';
                    if (sessionStorage.getItem(prefix + `_` + v) == undefined) {
                        let name = JSON.parse($.ajax({
                            url: `sysRoleGroup/queryByPk/${v}`,
                            async: false,
                            dataType: 'json',
                            type: 'GET'
                        }).responseText)['groupName'];
                        sessionStorage.setItem(prefix + `_` + v, name);
                    }
                    return sessionStorage.getItem(prefix + `_` + v);
                }
            },
            {
                field: 'roleId',
                width: 100,
                title: '角色',
                align: 'center',
                halign: 'center',
                editor: {
                    type: 'combobox', options: {
                        url: 'sysRole/listQuery',
                        method: 'GET',
                        textField: 'roleName',
                        valueField: 'id',
                        limitToList: true,
                        panelHeight: 200,
                        required: true
                    }
                },
                formatter: (v, r, i) => {
                    if (!v) return '';
                    let prefix = 'sz_role_group_role';
                    if (sessionStorage.getItem(prefix + `_` + v) == undefined) {
                        let name = JSON.parse($.ajax({
                            url: `sysRole/queryByPk/${v}`,
                            async: false,
                            dataType: 'json',
                            type: 'GET'
                        }).responseText)['roleName'];
                        sessionStorage.setItem(prefix + `_` + v, name);
                    }
                    return sessionStorage.getItem(prefix + `_` + v);
                },
                sortable: true,
            },
        ]],
        onAdd: function (index, row) {
            let editor = $(this).edatagrid('getEditor', {
                index: index,
                field: 'roleGroupId'
            })
            let selected = $('#e_grid').edatagrid('getSelected');
            if (selected != undefined) {
                $(editor.target).combobox('setValue', selected.id);
            } else {
                $.messager.alert('提示', '请选中角色组', 'warning');
                $(this).edatagrid('cancelRow');
            }
        },
        onEdit: function (index, row) {
        },
        onLoadSuccess: function () {
            $(this).datagrid('enableFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
        },
        onLoadError: function () {
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('enable');
                }
            })
        },
        onBeforeLoad: function (param) {
            $(this).datagrid('destroyFilter');
            $('.easyui-linkbutton').each(function () {
                if ($(this).linkbutton('options').text == '查询') {
                    $(this).linkbutton('disable');
                }
            })
            return true;
        },
        onSuccess: function (index, row) {
            $(this).edatagrid('reload');
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
                sessionStorage.clear();
            } else {
                $.messager.alert('提示', row.message, 'warning')
            }
        },
        onDestroy: function (index, row) {
            if (row.success) {
                $.messager.show({title: '提示', msg: row.message});
                sessionStorage.clear();
            } else {
                $.messager.alert('提示', row.message, 'warning')
            }
            $(this).edatagrid('reload');
        },
        onError: function (index, row) {
            $.messager.show({title: '提示', msg: row.message});
        },
        toolbar: [
            {
                iconCls: 'icon-reload', text: '刷新', handler: function () {
                    $('#e_grid_dtl').edatagrid('reload');
                }
            },
            {
                iconCls: 'icon-add', text: '新增', handler: function () {
                    $('#e_grid_dtl').edatagrid('addRow');
                }
            },
            {
                iconCls: 'icon-edit', text: '修改', handler: function () {
                    let selected = $('#e_grid_dtl').edatagrid('getSelected');
                    if (selected != undefined && selected.id) {
                        let selectIndex = $('#e_grid_dtl').edatagrid('getRowIndex', selected);
                        $('#e_grid_dtl').edatagrid('editRow', selectIndex);
                    }
                }
            },
            {
                iconCls: 'icon-save', text: '保存', handler: function () {
                    $('#e_grid_dtl').edatagrid('saveRow');
                }
            },
            {
                iconCls: 'icon-application_delete', text: '取消更改', handler: function () {
                    $('#e_grid_dtl').edatagrid('cancelRow');
                }
            },
            {
                iconCls: 'icon-cancel', text: '删除', handler: function () {
                    let selected = $('#e_grid_dtl').edatagrid('getSelected');
                    if (selected != undefined && selected.id) {
                        $('#e_grid_dtl').edatagrid('destroyRow');
                    }
                }
            },
        ]
    })
}

function btn_search() {
    $('#e_grid').edatagrid('reload');
}

function btn_add() {
    $('#e_grid').edatagrid('addRow');
}

function btn_modify() {
    let selected = $('#e_grid').edatagrid('getSelected');
    if (selected != undefined) {
        let index = $('#e_grid').edatagrid('getRowIndex', selected);
        if (index != -1) {
            $('#e_grid').edatagrid('editRow', index);
        }
    }
}

function btn_delete() {
    let selected = $('#e_grid').edatagrid('getSelected');
    if (selected != undefined) {
        let index = $('#e_grid').edatagrid('getRowIndex', selected);
        if (index != -1) {
            $('#e_grid').edatagrid('destroyRow', index);
        }
    }
}

function btn_import() {

}

function btn_save() {
    $('#e_grid').edatagrid('saveRow');
}

function btn_export() {
    $('#e_grid').datagrid('toExcel', {
        filename: '用户列表.xls',
        worksheet: 'Worksheet',
        caption: 'Caption',
        rows: $('#e_grid').edatagrid('getFormatRows'),
    });
}