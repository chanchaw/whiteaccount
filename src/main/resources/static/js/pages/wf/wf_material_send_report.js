$(function () {
    $('#A').window({
        width: 1100,
        height: 600,
        modal:true,
        title:'待导入白坯',
        collapsible:false,//定义是否显示可折叠按钮
        minimizable:false,
        maximizable:false,
        closable:true,
        maximized:false,  //最大化
        closed:true      //初始化页面关闭不显示

    });

    // 设置按钮
    let buttonGroup = [
        /* {
             id: 'btn_cancel',
             size: 'small',
             text: '作废',
             iconCls: 'icon-cancel',
             plain: true,
             onClick: btn_cancel
         },
         {
             id: 'btn_material_returning',
             size: 'small',
             text: '原料退回',
             iconCls: 'icon-edit',
             plain: true,
             disable: true,
             onClick: btn_material_returning
         },*/
        {
            id: 'btn_cancel_filter',
            size: 'small',
            text: '清除过滤',
            iconCls: 'icon-clear',
            plain: true,
            onClick: btn_cancel_filter
        },
        {
            id: 'btn_export',
            size: 'small',
            text: '导出Excel',
            iconCls: 'icon-page_excel',
            plain: true,
            onClick: btn_export_excel
        },
        {
            id: 'btn_print',
            size: 'small',
            text: '打印',
            iconCls: 'icon-print',
            plain: true,
            onClick: btn_print
        },
        {
            id: 'btn_grid_columns',
            size: 'small',
            text: '列',
            iconCls: 'icon-text_columns',
            plain: true,
            onClick: btn_open_column_list
        },
        {
            id: 'btn_grid_delete',
            size: 'small',
            text: '删除入库',
            disabled: true,
            iconCls: 'icon-cancel',
            plain: true,
            onClick: btn_grid_delete
        },
        {
            id: 'btn_close', size: 'small', text: '关闭', iconCls: 'icon-no', onClick: btn_close,
            plain: true
        },
    ];
    buttonGen($('#north'), buttonGroup);
    bindEnterEvents();
    setBtnState(false);
    let kGrid = $('#v_grid').data('kendoGrid');
    var options = kGrid.getOptions();
    if (options.columns.length > 0)
        initCombobox(options)
});

/**
 * 2021年8月22日 14:03:23 chanchaw
 * 外发管理 - 第一个标签页 - 原料外发表格参数和数据源
 */
function grid_1() {
    let SUFFIX = "_a";
    let order_code = new kendo.data.DataSource({data: []});
    let bill_type_name = new kendo.data.DataSource({data: []});
    let process_company = new kendo.data.DataSource({data: []});
    let material_spec = new kendo.data.DataSource({data: []});
    let batch_num = new kendo.data.DataSource({data: []});
    let supplier_name = new kendo.data.DataSource({data: []});
    let handler_name = new kendo.data.DataSource({data: []});
    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "productOutwardOrder/report/materialoutward",
                dataType: "json",
                type: "GET",
                data: {
                    start: $('#start').datebox('getValue'),
                    end: $('#end').datebox('getValue'),
                    processName: $('#contactCompanyId').combobox('getValue'),
                    // textField: $('#column').combobox('getValue'),
                    // valueField: $('#value').textbox('getValue'),
                }
            }
        },
        schema: {
            model: {
                // id: 'id',
                fields: {
                    order_date: {type: "date"},
                    create_time: {type: "date"},
                    input_kilo: {type: "number"},
                    box_amount: {type: "number"},
                }
            }
        },
        aggregate: [
            {field: "input_kilo", aggregate: "sum"},
            {field: "box_amount", aggregate: "sum"},
        ],
        change: function (e) {
            let data = this.data();
            batch_num.data(unique(data, 'batch_num'));
            bill_type_name.data(unique(data, 'bill_type_name'));
            process_company.data(unique(data, 'process_company'));
            supplier_name.data(unique(data, 'supplier_name'));
            order_code.data(unique(data, 'order_code'));
            material_spec.data(unique(data, 'material_spec'));
            handler_name.data(unique(data, 'handler_name'));
            $('#btn_material_returning').linkbutton('disable');
        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })
        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })
        }
    });
    let columns = [
        {field: "no", title: "#", filterable: false, template: "<span class='row_number'></span>", width: 40},
        {
            field: "id",
            title: '编号',
            width: 93,
            hidden: true,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
        },
        {
            field: "is_returning",
            title: '是否退回',
            width: 93,
            hidden: true,
            template: item => item['is_returning'] ? '是' : '否',
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
        },
        {
            field: "supplier_id",
            title: '供应商编号',
            width: 93,
            hidden: true,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
        },
        {
            field: "order_date",
            title: '单据日期',
            width: 85,
            format: "{0: MM-dd}",
            filterable: false,
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            footerTemplate: '总计:',
        },
        {
            field: "batch_num",
            title: '批次号',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: batch_num}
        },
        {
            field: "bill_type_name",
            title: '单据类型',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: bill_type_name}
        },
        {
            field: "supplier_name",
            title: '原料商',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: supplier_name}
        },
        {
            field: "process_company",
            title: '加工户',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: process_company}
        },
        {
            field: "order_code",
            title: '单据编号',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: false,
        },
        {
            field: "material_spec",
            title: '原料规格',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: material_spec}
        },
        {
            field: "handler_name",
            title: '经手人',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "box_amount",
            title: '件数',
            width: 93,
            template: item => item['box_amount'] ? parseFloat(item['box_amount']).toFixed(0) : '',
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            filterable: false,
        },
        {
            field: "input_kilo",
            title: '入库重量',
            width: 93,
            template: item => item['input_kilo'] ? parseFloat(item['input_kilo']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false,
        },
        {
            field: "is_send_directly",
            title: '直发',
            width: 93,
            template: item => item['is_send_directly'] == '是' ? `<input type="checkbox" checked onclick="return false;">` : '',
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true}
        },
        {
            field: "remarks",
            title: '备注',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false,
        },
        {
            title: '制单时间',
            field: "create_time",
            width: 85,
            format: "{0: MM-dd HH:mm}",
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            filterable: false,
        },
    ];
    let grid = $("#v_grid").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "原料外发序时表.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
            });
        },
        columnHide: function (e) {
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
        },
        columnShow: function (e) {
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null, SUFFIX);
        },
        columnResize: function (e) {
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null, SUFFIX);
            }
        },
        columnReorder: function (e) {
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field'] + SUFFIX
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid01.setOptions({
        columns: columns
    });
    grid.on('click', 'tr', function (e) {
        // $('#btn_material_returning').linkbutton('enable');
    });
    grid.on('dblclick', 'tr', function (e) {
        //  修改
        let dataItem = $("#v_grid").data('kendoGrid').dataItem(e.currentTarget);
        // btn_material_returning_modify(dataItem.id);
    });
}

/**
 * 2021年8月22日 14:03:23 chanchaw
 * 外发管理 - 第二个标签页 - 坯布入库表格参数和数据源
 */
function grid_2() {
    let SUFFIX = "_b";
    let process_company = new kendo.data.DataSource({data: []});
    let batch_num = new kendo.data.DataSource({data: []});
    let material_spec = new kendo.data.DataSource({data: []});
    let supplier_name = new kendo.data.DataSource({data: []});
    let is_finished_product = new kendo.data.DataSource({data: []});
    let create_user_name = new kendo.data.DataSource({data: []});
    let mode = new kendo.data.DataSource({data: []});
    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "productOutwardOrder/report/productinbound",
                dataType: "json",
                type: "GET",
                data: {
                    start: $('#start').datebox('getValue'),
                    end: $('#end').datebox('getValue'),
                    processName: $('#contactCompanyId').combobox('getValue'),
                    // textField: $('#column').combobox('getValue'),
                    // valueField: $('#value').textbox('getValue'),
                }
            }
        },
        schema: {
            model: {
                id: 'id',
                fields: {
                    create_time: {type: "date"},
                    order_date: {type: "date"},
                    input_kilo: {type: "number"},
                    box_amount: {type: "number"},
                    input_pairs: {type: "number"},
                    processing_cost: {type: "number"},
                    money: {type: "number"},
                }
            }
        },
        aggregate: [
            {field: "input_pairs", aggregate: "sum"},
            {field: "input_kilo", aggregate: "sum"},
            {field: "box_amount", aggregate: "sum"},
            {field: "money", aggregate: "sum"},
        ],
        change: function (e) {
            let data = this.data();
            is_finished_product.data(unique(data, 'is_finished_product'));
            batch_num.data(unique(data, 'batch_num'));
            mode.data(unique(data, 'mode'));
            material_spec.data(unique(data, 'material_spec'));
            supplier_name.data(unique(data, 'supplier_name'));
            create_user_name.data(unique(data, 'create_user_name'));
            process_company.data(unique(data, 'process_company'));

        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })
        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })
        }
    });
    let columns = [
        {field: "no", title: "#", filterable: false, template: "<span class='row_number'></span>", width: 40},
        {
            field: "id",
            title: '编号',
            width: 93,
            hidden: true,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
        },
        {
            field: "supplier_id",
            title: '供应商编号',
            width: 93,
            hidden: true,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
        },
        {
            field: "order_date",
            title: '单据日期',
            width: 85,
            format: "{0: MM-dd}",
            filterable: false,
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            footerTemplate: '总计:',
        },
        {
            field: "mode",
            title: '入库方式',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: mode}
        },
        {
            field: "batch_num",
            title: '批次号',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: batch_num}
        },
        {
            field: "isSendDirectly",
            title: '直发',
            template: item => item['is_send_directly'] == '是' ? `<input type="checkbox" checked onclick="return false;">` : '',
            width: 93,
            attributes: {'style': 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {'style': 'text-align:center;'},
            footerAttributes: {'style': 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "consignee",
            title: '收货单位',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: false
        },
        /*{
            field: "supplier_name",
            title: '供应商',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: supplier_name}
        },*/
        {
            field: "is_finished_product",
            title: '加工类型',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: is_finished_product}
        },
        {
            field: "process_company",
            title: '加工户',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: process_company}
        },
        {
            field: "order_code",
            title: '单据编号',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: false,
        },
        {
            field: "material_spec",
            title: '坯布规格',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: material_spec}
        },
        {
            field: "handler_name",
            title: '经手人',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "box_amount",
            title: '件数',
            width: 93,
            template: item => item['box_amount'] ? parseFloat(item['box_amount']).toFixed(0) : '',
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            filterable: false,
        },
        {
            field: "input_pairs",
            title: '入库匹数',
            width: 93,
            template: item => item['input_pairs'] ? parseFloat(item['input_pairs']).toFixed(0) : '',
            footerTemplate: '#=kendo.format("{0:n0}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false,
        },
        {
            field: "input_kilo",
            title: '入库重量',
            width: 93,
            template: item => item['input_kilo'] ? parseFloat(item['input_kilo']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false,
        }, {
            field: "processing_cost",
            title: '加工费',
            width: 93,
            template: item => item['processing_cost'] ? parseFloat(item['processing_cost']).toFixed(2) : '',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false,
        }, {
            field: "money",
            title: '金额',
            width: 93,
            template: item => item['money'] ? parseFloat(item['money']).toFixed(2) : '',
            footerTemplate: '#=kendo.format("{0:n2}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false,
        },
        {
            field: "remarks",
            title: '备注',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false,
        },
        {
            field: "create_user_name",
            title: '制单人',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: {multi: true, dataSource: create_user_name},
        },
        {
            title: '制单时间',
            field: "create_time",
            width: 85,
            format: "{0: MM-dd HH:mm}",
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            filterable: false,
        },
    ];
    let grid = $("#v_grid_a").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "原料外发入库.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
            });
        },
        columnHide: function (e) {
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
        },
        columnShow: function (e) {
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null, SUFFIX);
        },
        columnResize: function (e) {
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null, SUFFIX);
            }
        },
        columnReorder: function (e) {
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field'] + SUFFIX
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid_a').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid01.setOptions({
        columns: columns
    });

    grid.on('dblclick', 'tr', function (e) {
        let kGrid = $("#v_grid_a").data("kendoGrid");
        let dataItem = kGrid.dataItem(e.currentTarget);
        if (dataItem == undefined) {
            $.messager.alert('提示', '未选中!', 'warning');
            return;
        }
        if (dataItem['order_code'].indexOf('JB') > -1) {
            g(dataItem.id)
        } else if (dataItem['order_code'].indexOf('BC') > -1) {
            f(dataItem.id);
        }
    });
}


function setBtnState(b) {
    if (b) {
        $('#btn_import').linkbutton('enable');
        $('#btn_grid_delete').linkbutton('enable');
    } else {
        $('#btn_import').linkbutton('disable');
        $('#btn_grid_delete').linkbutton('disable');
    }
}

// 标签页的点击事件
function onSelect(title, index) {
    switch (index) {
        case 0:
            if ($('#v_grid').data('kendoGrid') == undefined) {
                grid_1();
            }
            setBtnState(false);
            break;
        case 1:
            if ($('#v_grid_a').data('kendoGrid') == undefined) {
                grid_2();
            }
            setBtnState(true);
            break;
        case 2:
            if ($('#v_grid_b').data('kendoGrid') == undefined) {
                grid_3();
            }
            break;
        case 3:
            if ($('#v_grid_c').data('kendoGrid') == undefined) {
                grid_4();
            }
            break;
        case 4:
            if ($('#v_grid_d').data('kendoGrid') == undefined) {
                grid_5();
            }
            break;
    };
console.log('进入onSelect事件！'+ index)
    if (index == 1) {
        $('#btn_grid_delete').linkbutton('enable');
    } else {
        $('#btn_grid_delete').linkbutton('disable');
    }
}

function btn_cancel() {
    //作废
    switch (getCurrentIndex()) {
        case 0:
            (function () {
                let kGrid = $('#v_grid').data('kendoGrid');
                let dataItem = kGrid.dataItem(kGrid.select());
                if (dataItem != undefined) {
                    $.messager.confirm('提示', '是否确定删除？删除后不可恢复！', function (r) {
                        if (r) {
                            $.post('materialOrder/cancelByPk', {id: dataItem.id || 0}, function (data) {
                                if (data.success) {
                                    btn_search();
                                } else {
                                    $.messager.alert('提示', data.message, 'warning');
                                }
                            }, 'json');
                        }
                    })
                } else {
                    $.messager.alert('提示', '请选中删除行！', 'warning');
                }
            })();
            break;
        case 1:
            (function () {
                let kGrid = $('#v_grid_a').data('kendoGrid');
                let dataItem = kGrid.dataItem(kGrid.select());
                if (dataItem != undefined) {
                    $.messager.confirm('提示', '是否确定删除？删除后不可恢复！', function (r) {
                        if (r) {
                            $.post('productOutwardOrder/cancelByPk', {id: dataItem.id || 0}, function (data) {
                                if (data.success) {
                                    btn_search();
                                } else {
                                    $.messager.alert('提示', data.message, 'warning');
                                }
                            }, 'json');
                        }
                    })
                } else {
                    $.messager.alert('提示', '请选中删除行！', 'warning');
                }
            })();
            break;
    }
}

function btn_material_returning() {
//  原料退回
    if (getCurrentIndex() == 0) {
        let kGrid = $("#v_grid").data("kendoGrid");
        let dataItem = kGrid.dataItem(kGrid.select());
        if (dataItem == undefined) {
            $.messager.alert('提示', '请选中！！', 'warning');
            return;
        }
        if (dataItem['bill_type_name'] == '原料退回') {
            $.messager.alert('提示', '不允许从原料退回单据生成！', 'warning');
            return;
        }
        let id = dataItem.id;
        $.ajax({
            url: 'materialOrder/queryByPk/' + id,
            type: 'GET',
            dataType: 'json',
        }).then(function (data) {
            let subData = {...data};
            data.id = null;
            data.billType = 'LLTH';
            data.boxAmount = null;
            data.inputKilo = null;
            openReturningDialog(data, 'ADD', subData);
            $.formDialog.enableBtn(['保存', '关闭']);
        })
    }
}

function btn_material_returning_modify(id) {
//  原料退回
    if (getCurrentIndex() == 0) {
        $.ajax({
            url: 'materialOrder/queryByPk/' + id,
            type: 'GET',
            dataType: 'json',
        }).then(function (data) {
            openReturningDialog(data, 'MODIFY', data);
            $.formDialog.disableEditing();
            $.formDialog.enableBtn(['修改', '关闭']);
        })
    }
}

function openReturningDialog(loadData, mode, sourceData) {
    const materialOutboundColumns = [
        {title: 'id', field: 'id', hidden: true,},
        {title: '制单日期', field: 'orderDate', editor: {type: 'datebox', options: {value: getCurrentDate()}}},
        {
            title: '单据类型', field: 'billType', hidden: true
        },
        {
            title: '供应商', field: 'supplierId', hidden: true
        },
        {
            title: '原料规格', field: 'materialSpec', hidden: true
        },
        {title: '批号', field: 'batchNum', hidden: true},
        {title: '件数', field: 'boxAmount', editor: {type: 'numberbox', options: {precision: 0, required: false}}},
        {
            title: '重量',
            id: 'inputKiloId',
            field: 'inputKilo',
            editor: {type: 'numberbox', options: {precision: 1, required: true}}
        },
        {
            title: '加工户', field: 'processCompany', id: 'processCompanyInput', hidden: true
        },
        {
            title: '经手人', field: 'handlerName', hidden: true, editor: {
                type: 'combobox', options: {
                    required: false,
                    url: 'employee/listQuery',
                    method: 'GET',
                    queryParams: {state: 1},
                    textField: 'empName',
                    valueField: 'empName'
                }
            }
        },
        {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {required: false}}},
        {
            title: '制单人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
    ];
    const materialOutboundModifyColumns = [
        {title: 'id', field: 'id', hidden: true,},
        {title: '制单日期', field: 'orderDate', editor: {type: 'datebox', options: {value: getCurrentDate()}}},
        {title: '单据编号', field: 'orderCode', editor: {type: 'textbox', options: {required: false, readonly: true}}},
        {
            title: '单据类型', field: 'billType', editor: {
                type: 'combobox', options: {
                    url: 'billType/listQuery',
                    queryParams: {storageId: 1},
                    readonly: true,
                    method: 'GET',
                    textField: 'billTypeName', valueField: 'id',
                    panelHeight: 200,
                    required: true,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                        let _data = $(this).combobox('getData') || [];
                        let _opts = $(this).combobox('options');
                        if (_data instanceof Array && !$(this).combobox('getValue') && _data.length > 0) {
                            for (let i of _data) {
                                if (i['isSelected']) {
                                    $(this).combobox('setValue', i[_opts.valueField]);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        },
        {
            title: '供应商', field: 'supplierId', editor: {
                type: 'combobox', options: {
                    url: 'supplier/listQuery',
                    method: 'GET',
                    textField: 'supplierName', valueField: 'id',
                    panelHeight: 200,
                    required: true,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '原料规格', field: 'materialSpec', editor: {
                type: 'combobox', options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    panelHeight: 200,
                    required: true,
                }
            }
        },
        {title: '批号', field: 'batchNum', editor: {type: 'textbox', options: {required: false}}},
        {title: '件数', field: 'boxAmount', editor: {type: 'numberbox', options: {precision: 0, required: false}}},
        {
            title: '重量',
            id: 'inputKiloId',
            field: 'inputKilo',
            editor: {type: 'numberbox', options: {precision: 1, required: true}}
        },
        {
            title: '加工户', field: 'processCompany', id: 'processCompanyInput', editor: {
                type: 'combobox', options: {
                    required: false,
                    url: 'processCompany/listQuery',
                    method: 'GET',
                    textField: 'companyName',
                    valueField: 'companyName',
                    onLoadSuccess: function () {
                        let _data = $(this).combobox('getData') || [];
                        let valueField = $(this).combobox('options').valueField;
                        if (!$(this).combobox('getValue'))
                            for (let i of _data) {
                                if (i['isSelected']) {
                                    $(this).combobox('setValue', i[valueField]);
                                    break;
                                }
                            }
                    }
                }
            }
        },
        {
            title: '经手人',
            field: 'handlerName',
            editor: {
                type: 'combobox', options: {
                    required: false,
                    url: 'employee/listQuery',
                    method: 'GET',
                    queryParams: {state: 1},
                    textField: 'empName',
                    valueField: 'empName'
                }
            }
        },
        {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {required: false}}},
        {
            title: '制单人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
    ];
    $.formDialog.open({
        columns: mode == 'MODIFY' ? materialOutboundModifyColumns : materialOutboundColumns,
        onOpen: function () {
            $('.form-dialog-edit').find('.form-row:visible').eq(0).css({'margin-top': '10px'});
        },
        loadData: loadData,
        mode: '原料退货',
        width: 400,
        height: 'auto',
        buttons: [],
        toolbar: [
            {
                text: '修改', iconCls: 'icon-edit', handler: e => {
                    $.formDialog.enableBtn(['保存', '关闭']);
                    $.formDialog.enableEditing();
                }
            },
            {
                text: '保存', iconCls: 'icon-save', handler: e => {
                    let url = '';
                    let formData = $('.form-dialog-edit').eq(0).dialog('panel').find('form').serializeObject();
                    if (formData['id']) {
                        url = 'materialOrder/updateSelective';
                    } else {
                        url = 'materialOrder/insertSelective';
                    }
                    if (mode == 'ADD' && sourceData != undefined) {
                        if (sourceData['inputKilo'] < formData['inputKilo']) {
                            $.messager.confirm('提示', '超出出库数，是否继续？', function (r) {
                                if (r) {
                                    ex();
                                }
                            })
                        } else {
                            ex();
                        }
                    } else {
                        ex();
                    }

                    function ex() {
                        $('.form-dialog-edit').eq(0).dialog('panel').find('form').form('submit', {
                            url: url,
                            success: function (data) {
                                data = eval(`(${data})`);
                                if (data.success) {
                                    $.formDialog.disableBtn(['保存']);
                                    $.formDialog.disableEditing();
                                    $('.form-dialog-edit').eq(0).dialog('panel').find('form').form('load', data.data);
                                    btn_search();
                                    $.messager.show({title: '提示', msg: data.message});
                                } else {
                                    $.messager.alert('提示', data.message, 'error');
                                }
                            }
                        });
                    }

                }
            },
            {
                text: '作废', iconCls: 'icon-cancel', handler: e => {
                    $.messager.confirm('提示', '是否作废？', function (r) {
                        if (r) {
                            let formData = $('.form-dialog-edit').find('form').serializeObject();
                            if (!formData['id']) {
                                $.messager.alert('提示', '单据未保存!', 'warning');
                                return;
                            }
                            $.ajax({
                                url: 'materialOrder/cancelByPk',
                                data: {id: formData.id},
                                type: 'POST',
                                dataType: 'json',
                                success: function (data) {
                                    if (data.success) {
                                        $.formDialog.close();
                                        btn_search();
                                        $.messager.show({title: '提示', msg: data.message});
                                    } else {
                                        $.messager.alert('提示', data.message, 'error');
                                    }
                                }
                            });
                        }
                    })
                }
            },
            {
                text: '关闭', iconCls: 'icon-no', handler: e => {
                    $('.form-dialog-edit').eq(0).dialog('close');
                }
            }]
    })
}

function btn_cancel_filter() {
    switch (getCurrentIndex()) {
        case 0:
            $('#v_grid').data('kendoGrid').dataSource.filter({});
            break;
        case 1:
            $('#v_grid_a').data('kendoGrid').dataSource.filter({});
            break;
        case 2:
            $('#v_grid_b').data('kendoGrid').dataSource.filter({});
            break;
        case 3:
            $('#v_grid_c').data('kendoGrid').dataSource.filter({});
            break;
        case 4:
            $('#v_grid_d').data('kendoGrid').dataSource.filter({});
            break;
    }
}

// 查询事件
function btn_search() {
    function research(index) {
        switch (index) {
            case 0:
                if ($('#v_grid').data('kendoGrid') != undefined)
                    $('#v_grid').data('kendoGrid').dataSource.read({
                        start: $('#start').datebox('getValue'),
                        end: $('#end').datebox('getValue'),
                        processName: $('#contactCompanyId').combobox('getValue'),
                        // textField: $('#column').combobox('getValue'),
                        // valueField: $('#value').textbox('getValue'),
                    });
                break;
            case 1:
                if ($('#v_grid_a').data('kendoGrid') != undefined)
                    $('#v_grid_a').data('kendoGrid').dataSource.read({
                        start: $('#start').datebox('getValue'),
                        end: $('#end').datebox('getValue'),
                        processName: $('#contactCompanyId').combobox('getValue'),
                        // textField: $('#column').combobox('getValue'),
                        // valueField: $('#value').textbox('getValue'),
                    });
                break;
            case 2:
                if ($('#v_grid_b').data('kendoGrid') != undefined)
                    $('#v_grid_b').data('kendoGrid').dataSource.read({
                        start: $('#start').datebox('getValue'),
                        end: $('#end').datebox('getValue'),
                        // textField: $('#column').combobox('getValue'),
                        // valueField: $('#value').textbox('getValue'),
                    });
                break;
        }
    }

    for (let i = 0; i < 5; i++) {
        research(i);
    }
}

function btn_add() {
    let param = {};
    if (getCurrentIndex() == 1) {
        let kGrid = getSelectedGrid();
        let dataItem = kGrid.dataItem(kGrid.select());
        if (dataItem != undefined) {
            param.batchNum = dataItem['batch_num'];
            param.materialSpec = dataItem['material_spec'];
            param.supplierId = dataItem['supplier_id'];
        }
    }
    openDialog(param);
    $.formDialog.enableBtn(['保存', '关闭']);
}

function btn_print() {
    let reportlets = [];
    $.get('customize/getDatabase', {}, data => {
        if (data.success) {
            switch (getCurrentIndex()) {
                case 0:
                    reportlets.push({
                        reportlet: `${data.data}/原料入库序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 1:
                    reportlets.push({
                        reportlet: `${data.data}/原料库存序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 2:
                    reportlets.push({
                        reportlet: `${data.data}/原料领料序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 3:
                    if (!$('#contactCompanyId').combobox('getValue')) {
                        $.messager.alert('提示', '请选择客户再打印！', 'warning');
                        return;
                    }
                    reportlets.push({
                        reportlet: `${data.data}/应付款序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 4:
                    if (!$('#contactCompanyId').combobox('getValue')) {
                        $.messager.alert('提示', '请选择客户再打印！', 'warning');
                        return;
                    }
                    reportlets.push({
                        reportlet: `${data.data}/已付款序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
            }
        }
    }, 'json');
}

function btn_open_column_list() {
    let kendoGridID = '#v_grid';
    switch (getCurrentIndex()) {
        case 0:
            kendoGridID = '#v_grid';
            break;
        case 1:
            kendoGridID = '#v_grid_a';
            break;
        case 2:
            kendoGridID = '#v_grid_b';
            break;
        case 3:
            kendoGridID = '#v_grid_c';
            break;
        case 4:
            kendoGridID = '#v_grid_d';
            break;
    }
    let options = $(kendoGridID).data('kendoGrid').getOptions().columns;
    if ($('#btn_open_column_list').length <= 0) {
        let div = $(`<div data-options="
                    resizable:true,iconCls:'icon-text_columns',width:200,height:300,modal:true,title:'列',
                    onClose:function(){$(this).dialog('destroy')}" 
                          class="easyui-dialog" id="btn_open_column_list">
                   </div>`);
        let form = $('<form style="width: 100%;height: 100%;"></form>')
        for (let i of options) {
            if (i.field) {
                let checkbox = $(`<div style="width: 100%;height: auto;padding: 5px;">
                <input class="easyui-checkbox" name="${i.field}" data-options="label:'${i.title}',
                labelWidth:120,value:true,name:'${i.field}'">
                </div>`);
                form.append(checkbox);
            }
        }
        let container = $(`<div></div>`);
        form.appendTo(div);
        div.appendTo(container);
        $('body').append(container);
        $.parser.parse($('#btn_open_column_list').parent());
        let formData = {};
        options.map(item => {
            formData[item['field']] = item['hidden'] == undefined ? true : !item['hidden'];
        })
        $('#btn_open_column_list form').eq(0).form('load', formData);
        $('#btn_open_column_list form .easyui-checkbox').each(function () {
            //  绑定事件
            $(this).checkbox({
                onChange: function (checked) {
                    let opts = $(this).checkbox('options');
                    if (checked) {
                        $(kendoGridID).data('kendoGrid').showColumn(opts.name);
                    } else {
                        $(kendoGridID).data('kendoGrid').hideColumn(opts.name);
                    }
                }
            })
        })
    } else {
        $('#btn_open_column_list').dialog('open');
    }
}

function btn_export_excel() {
    getSelectedGrid().saveAsExcel();
}

function btn_close() {
    window.parent.closeTab();
}

function getSelectedGrid() {
    let kendoGridID = '';
    switch (getCurrentIndex()) {
        case 0:
            kendoGridID = '#v_grid';
            break;
        case 1:
            kendoGridID = '#v_grid_a';
            break;
        case 2:
            kendoGridID = '#v_grid_b';
            break;
        case 3:
            kendoGridID = '#v_grid_c';
            break;
        case 4:
            kendoGridID = '#v_grid_d';
            break;
    }
    return $(kendoGridID).data('kendoGrid');
}

function stateChange(checked) {
    if (!checked && getCurrentIndex() == 3) {
        $('#contactCompanyId').combobox('clear');
        $('#contactCompanyId').combobox('disable');
        $('#start').datebox('disable');
        $('#end').datebox('disable');
    } else {
        $('#start').datebox('enable');
        $('#end').datebox('enable');
        $('#contactCompanyId').combobox('enable');
    }
    btn_search();
}

// 获取当前标签页页码
function getCurrentIndex() {
    let tab = $('#tabs').tabs('getSelected');
    let index = $('#tabs').tabs('getTabIndex', tab);
    return index;
}

//  外发入库编辑模态框开启
function f(id) {
    let loadData = {};
    if (id == undefined) return;
    let column = [
        {
            title: '主键',
            field: 'id',
            hidden: true,
        },
        {
            title: '是否成品',
            field: 'isFinishedProduct',
            hidden: true,
        },
        {
            title: '单据日期',
            field: 'orderDate',
            editor: {type: 'datebox', options: {required: true, value: getCurrentDate()}}
        },
        {
            title: '单据编号',
            field: 'orderCode',
            editor: {
                type: 'textbox',
                options: {
                    readonly: true
                }
            }
        },
        {
            title: '加工户', field: 'processCompany', id: 'processCompanyInput', editor: {
                type: 'combobox', options: {
                    required: false,
                    url: 'processCompany/listQuery',
                    method: 'GET',
                    textField: 'companyName',
                    valueField: 'companyName',
                    onLoadSuccess: function () {
                        let _data = $(this).combobox('getData') || [];
                        let valueField = $(this).combobox('options').valueField;
                        if (!$(this).combobox('getValue'))
                            for (let i of _data) {
                                if (i['isSelected']) {
                                    $(this).combobox('setValue', i[valueField]);
                                    break;
                                }
                            }
                    }
                }
            }
        },
        {
            title: '批次号',
            field: 'batchNum',
            editor: {
                type: 'textbox',
                options: {}
            }
        },
        {
            title: '供应商', field: 'supplierName', editor: {
                type: 'combobox', options: {
                    url: 'supplier/listQuery',
                    method: 'GET',
                    textField: 'supplierName',
                    valueField: 'supplierName',
                    cls: 'input-blue',
                    panelHeight: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '计划单号',
            field: 'planCode',
            editor: {
                type: 'textbox',
                options: {readonly: true, required: true}
            }
        },
        {
            title: '品名规格',
            field: 'materialSpecification',
            editor: {
                type: 'textbox',
                options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    cls: 'input-blue',
                    panelHeight: 200,
                    required: true,
                }
            }
        },
        {
            title: '坯布规格',
            field: 'greyfabricSpecification',
            editor: {
                type: 'combobox',
                options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    cls: 'input-blue',
                    panelHeight: 200,
                    required: true,
                }
            }
        },
        {title: '匹数', field: 'inputPairs', editor: {type: 'numberbox', options: {precision: 0, required: true}}},
        {
            title: '公斤',
            field: 'inputKilo',
            id: 'inputKiloId',
            editor: {type: 'numberbox', options: {precision: 1, required: true}}
        },
        {
            title: '加工费',
            field: 'processingCost',
            id: 'processingCostId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: '金额',
            field: 'money',
            id: 'moneyId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: 'A/B',
            field: 'abSurface',
            id: 'abSurfaceId',
            editor: {
                type: 'combobox', options: {
                    data: [{id: 'A'}, {id: 'B'}], panelHeight: 'auto', textField: 'id',
                    valueField: 'id', required: true, onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '收货单位',
            field: 'consignee',
            id: 'consigneeId',
            editor: {
                type: 'combobox', options: {
                    width: 110, cls: 'input-red', readonly: true,
                    url: 'takeDeliveryAddress/listQuery',
                    method: 'GET',
                    textField: 'company',
                    valueField: 'company',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '直发',
            width: '50px',
            field: 'isSendDirectly',
            editor: {
                type: 'checkbox', options: {
                    cls: 'input-red', value: 1, onChange: function (checked) {
                        if (checked) {
                            $('#consigneeId').combobox({readonly: false});
                        } else {
                            $('#consigneeId').combobox('clear');
                            $('#consigneeId').combobox({readonly: true});
                        }
                    }
                }
            }
        },
        {
            title: '经手人', field: 'handlerName', editor: {
                type: 'combobox', options: {
                    required: false,
                    url: 'employee/listQuery',
                    method: 'GET',
                    queryParams: {state: 1},
                    textField: 'empName',
                    valueField: 'empName'
                }
            }
        },
        {
            title: '制单人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    cls: 'input-blue',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {width: 514,}}},
    ];
    $.ajax({
        url: 'productOutwardOrder/queryByPk/' + id,
        type: 'GET',
        async: false,
        dataType: 'json',
    }).then(function (data) {
        loadData = data;
        if (!loadData['state']) {
            $.messager.alert('提示', '已作废!', 'warning');
            return;
        }
        $.formDialog.open({
            mode: '修改',
            columns: column,
            loadData: loadData,
            width: 656,
            height: 417,
            onOpen: function () {
                $('#inputKiloId').numberbox('textbox').bind('keyup', function (e) {
                    let price = $('#processingCostId').numberbox('getValue') || 0;
                    let _value = $(this).val();
                    $('#moneyId').numberbox('setValue', price * _value);
                })
                $('#processingCostId').numberbox('textbox').bind('keyup', function (e) {
                    let qty = $('#inputKiloId').numberbox('getValue') || 0;
                    let _value = $(this).val();
                    $('#moneyId').numberbox('setValue', qty * _value);
                });
                // $('.form-dialog-edit').find('.form-row:visible').eq(0).css({'margin-top': '10px'});
            },
            buttons: [{
                text: '保存',
                iconCls: 'icon-save',
                handler: e => {
                    let dialog = $('.form-dialog-edit');
                    if (!$(dialog.find('form')).form('validate')) {
                        return;
                    }
                    let param = $(dialog.find('form')).serializeObject();
                    let url = 'productOutwardOrder/updateSelective';
                    //  保存
                    $.messager.progress({
                        title: '',
                        msg: '请稍等...',
                        interval: 5000
                    });
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        data: param,
                        success: function (data) {
                            if (data.success) {
                                $.formDialog.close();
                                btn_search();
                                $.messager.show({title: '提示', msg: data.message});
                            } else {
                                $.messager.alert('提示', data.message, 'warning');
                            }
                        },
                        complete: function () {
                            $.messager.progress('close');
                        }
                    })
                }
            }, {
                text: '退出',
                iconCls: 'icon-cancel',
                handler: e => {
                    $.formDialog.close();
                }
            }],
        });
        $.formDialog.disableEditing();
        $.formDialog.disableBtn(['保存','修改']);
    }, function () {
        $.messager.alert('提示', '连接服务器失败!', 'warning');
    });
}

function g(id) {
    let loadData = {};
    if (id == undefined) return;
    let column = [
        {
            title: '主键',
            field: 'id',
            hidden: true,
        },
        {
            title: '单据日期',
            field: 'orderDate',
            editor: {type: 'datebox', options: {required: true, value: getCurrentDate()}}
        },
        {
            title: '单据编号',
            field: 'orderCode',
            editor: {
                type: 'textbox',
                options: {
                    readonly: true
                }
            }
        },
        {
            title: '加工户', field: 'processCompany', id: 'processCompanyInput', editor: {
                type: 'combobox', options: {
                    required: true,
                    url: 'processCompany/listQuery',
                    method: 'GET',
                    textField: 'companyName',
                    valueField: 'companyName',
                    onLoadSuccess: function () {
                    }
                }
            }
        },
        {
            title: '加工类型',
            field: 'isFinishedProduct',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    data: [{id: 0, text: '未剖幅'}, {id: 1, text: '已剖幅'}],
                    limitToList: true,
                    value: 1,
                    required: true,
                    textField: 'text',
                    valueField: 'id',
                    panelHeight: 'auto',
                    onChange: function (newVal, oldVal) {
                        if (newVal) {
                            $('#boxAmountField').numberbox('clear');
                            $('#boxAmountField').numberbox({required: false, readonly: true});
                            $('#inputPairsField').numberbox({readonly: false, required: true});
                            $('#abSurfaceId').combobox('readonly', false);
                            $('#fieldIsSendDirectly').checkbox('enable');
                        } else {
                            $('#abSurfaceId').combobox('clear');
                            $('#abSurfaceId').combobox('readonly', true);
                            $('#consigneeId').combobox('clear');
                            $('#fieldIsSendDirectly').checkbox('disable');
                            $('#inputPairsField').numberbox('clear');
                            $('#inputPairsField').numberbox({required: false, readonly: true});
                            $('#boxAmountField').numberbox({readonly: false, required: true});
                        }
                    }
                }
            }
        },
        {
            title: '批次号',
            field: 'batchNum',
            id: 'batchNumInput',
            editor: {
                type: 'combobox',
                options: {
                    url: 'planBatchnum/listQuery/planCode',
                    method: 'GET',
                    panelHeight: 300,
                    textField: 'batchNum',
                    valueField: 'batchNum',
                    onLoadSuccess: function (data) {
                        let fieldValue = $(this).combobox('options').valueField;
                        if (data instanceof Array && data.length > 0 && !$(this).combobox('getValue')) {
                            $(this).combobox('setValue', data[0][fieldValue]);
                        }
                    }
                }
            }
        },
        {
            title: '客户', id: 'supplierNameInput', field: 'clientName', editor: {
                type: 'textbox', options: {}
            }
        },
        /*{
            title: '供应商', field: 'supplierName', editor: {
                type: 'combobox', options: {
                    url: 'supplier/listQuery',
                    method: 'GET',
                    textField: 'supplierName',
                    valueField: 'supplierName',
                    cls: 'input-blue',
                    panelHeight: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },*/
        {
            title: '计划单号',
            field: 'planCode',
            id: 'planCodeInput',
            editor: {
                type: 'textbox',
                options: {
                    icons: [{
                        iconCls: 'icon-remove', handler: function (e) {
                            $(e.data.target).textbox('clear');
                        }
                    }, {
                        iconCls: 'icon-add', handler: function (e) {
                            let div = $(`<div>
                                        <div id="t_dialog" 
                                             data-options="title:'白坯计划',width:730,height:500,resizable:true,
                                                           modal:true,iconCls:'icon-search',
                                                           onClose:function(){$(this).dialog('destroy')},
                                                           /*
                                                           buttons:[{text:'取消',iconCls:'icon-cancel',handler:function(){
                                                                        $('#t_dialog').dialog('close');
                                                                    }},
                                                                    {text:'保存',iconCls:'icon-save',handler:function(){
                                                                        let _selected = $('#t_plancode').datagrid('getSelected');
                                                                        if(_selected != undefined){
                                                                               delete _selected['createUserId'];
                                                                                 let specification = [
                                                                                    _selected['orderCode'],
                                                                                    _selected['productName'],
                                                                                    _selected['greyfabricWidth'] + '*' + _selected['greyfabricWeight'],
                                                                                    _selected['mHeight'],
                                                                                    _selected['greyfabricSpecification'],
                                                                                    _selected['materialSpecification'],
                                                                                ];
                                                                                _selected['greyfabricSpecification'] = specification.join(' ');
                                                                               $('.form-dialog-edit form').eq(0).form('load',_selected);
                                                                               $('#batchNumInput').combobox('options').queryParams =  {planCode: _selected['planCode']};
                                                                               $('#batchNumInput').combobox('reload');
                                                                               $('#t_dialog').dialog('close');
                                                                        } else {
                                                                          $.messager.alert('提示','未选中!','warning')  
                                                                        }
                                                                    }}]
                                                                    */
                                                           " 
                                             class="easyui-dialog">
                                            <table id="t_plancode"></table>
                                        </div>
                                    </div>`);
                            $.parser.parse(div);
                            $('#t_plancode').datagrid({
                                url: 'plan/listQuery',
                                queryParams: {state: 1, markFinished: false},
                                method: 'GET',
                                striped: true,
                                fit: true,
                                sortable: true,
                                columns: [[
                                    {
                                        title: '计划日期',
                                        hidden: true,
                                        field: 'planDate',
                                        width: 120,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '计划单号', field: 'planCode', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '门幅',
                                        field: 'greyfabricWidth',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '克重', field: 'lowWeight', width: 60, align: 'center', halign: 'center'},
                                    {
                                        title: '头份',
                                        field: 'greyfabricSpecification',
                                        width: 60,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                    {title: '毛高', field: 'mHeight', width: 120, align: 'center', halign: 'center'},
                                    {title: '品名', field: 'productName', width: 120, align: 'center', halign: 'center'},
                                    {
                                        title: '原料规格',
                                        field: 'materialSpecification',
                                        width: 100,
                                        align: 'center',
                                        halign: 'center'
                                    },
                                ]],
                                rownumbers: true,
                                remoteSort: false,
                                showFooter: true,
                                singleSelect: true,
                                onBeforeLoad: function () {
                                },
                                onLoadSuccess: function () {
                                },
                                onDblClickRow: function (index, row) {
                                    delete row['createUserId'];
                                    let specification = [
                                        row['orderCode'],
                                        row['productName'],
                                        row['greyfabricWidth'] + '*' + row['greyfabricWeight'],
                                        row['mHeight'],
                                        row['greyfabricSpecification'],
                                        row['materialSpecification'],
                                    ];
                                    row['greyfabricSpecification'] = specification.join(' ');
                                    row['greyfabricSpecification'] = specification.join(' ');
                                    load($('.form-dialog-edit form').eq(0), row);
                                    $('#batchNumInput').combobox('options').queryParams = {planCode: row['planCode']};
                                    $('#batchNumInput').combobox('reload');
                                    $('#t_dialog').dialog('close');
                                }
                            });
                        }
                    }],
                    required: true,
                }
            }
        },
        /*{
            title: '原料规格',
            field: 'materialSpecification',
            editor: {
                type: 'textbox',
                options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    cls: 'input-blue',
                    panelHeight: 200,
                    required: true,
                }
            }
        },*/
        {
            title: '件数',
            field: 'boxAmount',
            id: 'boxAmountField',
            editor: {type: 'numberbox', options: {precision: 0, required: false, readonly: true}}
        },
        {
            title: '坯布规格',
            field: 'greyfabricSpecification',
            editor: {
                type: 'textbox',
                options: {
                    cls: 'input-blue',
                    width: 514,
                    required: true,
                }
            }
        },
        {
            title: '匹数',
            field: 'inputPairs',
            id: 'inputPairsField',
            editor: {type: 'numberbox', options: {precision: 0, required: true}}
        },
        {
            title: '公斤',
            field: 'inputKilo',
            id: 'inputKiloId',
            editor: {type: 'numberbox', options: {precision: 1, required: true}}
        },
        {
            title: '加工费',
            field: 'processingCost',
            id: 'processingCostId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: '金额',
            field: 'money',
            id: 'moneyId',
            editor: {type: 'numberbox', options: {precision: 2, required: false}}
        },
        {
            title: 'A/B',
            field: 'abSurface',
            id: 'abSurfaceId',
            editor: {
                type: 'combobox', options: {
                    buttonIcon: 'icon-add',
                    onClickButton: function () {
                        let _value = $('#abSurfaceId').combobox('getValue');
                        if (_value) {
                            switch (_value) {
                                case 'A':
                                    $('#abSurfaceId').combobox('setValue', 'B');
                                    break;
                                case 'B':
                                    $('#abSurfaceId').combobox('clear');
                                    break;
                            }
                        } else {
                            $('#abSurfaceId').combobox('setValue', 'A');
                        }
                    },
                    data: [{id: 'A'}, {id: 'B'}],
                    panelHeight: 'auto',
                    textField: 'id',
                    valueField: 'id',
                    required: false,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '收货单位',
            field: 'consignee',
            id: 'consigneeId',
            editor: {
                type: 'combobox', options: {
                    width: 120, cls: 'input-red', readonly: true,
                    url: 'takeDeliveryAddress/listQuery',
                    method: 'GET',
                    textField: 'company',
                    valueField: 'company',
                    panelWidth: 200,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '直发',
            width: '40px',
            field: 'isSendDirectly',
            id: 'fieldIsSendDirectly',
            editor: {
                type: 'checkbox', options: {
                    cls: 'input-red', value: true, onChange: function (checked) {
                        if (checked) {
                            let _value = $('#consigneeId').combobox('getValue');
                            $('#consigneeId').combobox({readonly: false, required: true});
                            $('#consigneeId').combobox('setValue', _value);
                        } else {
                            $('#consigneeId').combobox('clear');
                            $('#consigneeId').combobox({readonly: true, required: false});
                        }
                    }
                }
            }
        },
        {
            title: '经手人', field: 'handlerName', editor: {
                type: 'combobox', options: {
                    required: false,
                    method: 'GET',
                    url: 'employee/listQuery',
                    queryParams: {state: 1},
                    textField: 'empName',
                    valueField: 'empName'
                }
            }
        },
        {
            title: '制单人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    cls: 'input-blue',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {width: 514,}}},
    ];
    $.ajax({
        url: 'productOutwardOrder/queryByPk/' + id,
        type: 'GET',
        async: false,
        dataType: 'json',
    }).then(function (data) {
        loadData = data;
        if (!loadData['state']) {
            $.messager.alert('提示', '已作废!', 'warning');
            return;
        }
        $.formDialog.open({
            mode: '修改',
            columns: column,
            loadData: loadData,
            width: 656,
            height: 460,
            onOpen: function () {
                $('#inputKiloId').numberbox('textbox').bind('keyup', function (e) {
                    let price = $('#processingCostId').numberbox('getValue') || 0;
                    let _value = $(this).val();
                    $('#moneyId').numberbox('setValue', price * _value);
                })
                $('#processingCostId').numberbox('textbox').bind('keyup', function (e) {
                    let qty = $('#inputKiloId').numberbox('getValue') || 0;
                    let _value = $(this).val();
                    $('#moneyId').numberbox('setValue', qty * _value);
                });
                // $('.form-dialog-edit').find('.form-row:visible').eq(0).css({'margin-top': '10px'});
            },
            buttons: [{
                text: '保存',
                iconCls: 'icon-save',
                handler: e => {
                    let dialog = $('.form-dialog-edit');
                    if (!$(dialog.find('form')).form('validate')) {
                        return;
                    }
                    let param = $(dialog.find('form')).serializeObject();
                    let url = 'productOutwardOrder/updateSelective';
                    //  保存
                    $.messager.progress({
                        title: '',
                        msg: '请稍等...',
                        interval: 5000
                    });
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        data: param,
                        success: function (data) {
                            if (data.success) {
                                $.formDialog.close();
                                btn_search();
                                $.messager.show({title: '提示', msg: data.message});
                            } else {
                                $.messager.alert('提示', data.message, 'warning');
                            }
                        },
                        complete: function () {
                            $.messager.progress('close');
                        }
                    })
                }
            }, {
                text: '退出',
                iconCls: 'icon-cancel',
                handler: e => {
                    $.formDialog.close();
                }
            }],
        });
        $.formDialog.disableEditing();
        $.formDialog.disableBtn(['保存','修改']);
    }, function () {
        $.messager.alert('提示', '连接服务器失败!', 'warning');
    });
}

function getDataA() {

    var m_param=$('#contactCompanyId').combobox('getValue');  //参数  加工户
    if (m_param.length<=0){
        // messagerShow('未选中加工户！');
        $.messager.alert('提示', '未选中加工户!', 'warning');
        return
    }
    $('#A').window('open');
    $.ajax({
        url: 'productOutwardOrder/getUnImportedJRK',
        contentType: "application/json;charset=UTF-8",
        type: 'post',
        // traditional: true,
        data: JSON.stringify({"processCompname":m_param}),
        success: function (cdata) {
            $('#detaiA').datagrid('loadData', cdata.data);
        }
    });

};



function closeA() {
    var m_param=[];  //参数
    $('#A').window('close');  // close a window
    $('#detaiA').datagrid('loadData', m_param);
}

/**
 * 2021年8月20日 13:20:08 chanchaw
 * 创建外发入库
 */
function createOutward(){
    // const selectedRows = getEasyuiGridSelectedRows('detaiA');
    var selectedRows = $('#detaiA').datagrid('getSelected');
    if( selectedRows != undefined ) {

        var m_param=$('#contactCompanyId').combobox('getValue');  //参数  加工户
        var ids = [];
        var rows = $('#detaiA').datagrid('getSelections');
        for(var i=0; i<rows.length; i++){
            ids.push(rows[i].id);
        }
        console.log('选中的行：',selectedRows);
        console.log('选中行的ids：',ids);
        let params = {'processCompanyName':m_param,'ids':ids};
        console.log('用于请求的参数是：',params);

        $.ajax({
            url: 'productOutwardOrder/mapping8jrk',
            contentType: "application/json;charset=UTF-8",
            type: 'post',
            data: JSON.stringify(params),
            success: function (cdata) {
                console.log('生产外发入库后端响应来的数据：',cdata);
                closeA();
                btn_search();
            }
        });

    }else{
        // messagerShow('至少选中一行后再进行本操作');
        selectedRows
        // return
    }


}

// 删除
function btn_grid_delete() {
    console.log('进入删除！')
    if (getCurrentIndex() == 1){

        let kGrid = $('#v_grid_a').data('kendoGrid');
        let dataItem = kGrid.dataItem(kGrid.select());
        if (dataItem == undefined) {
            $.messager.alert('提示', '未选中需要删除行!', 'warning');
            return;
        }
        $.messager.confirm('提示', '是否确定删除当前选中行数据?', function (r) {
            if (r) {
                $.post('productOutwardOrder/cancelByPk', {id: dataItem.id}, function (data) {
                    if (data.success) {
                        btn_search();
                    } else {
                        $.messager.alert('提示', data.message, 'error');
                    }
                }, 'json');
            }
        });
        return;
    }


}

function add0(m){return m<10?'0'+m:m }
function formatDateTime(shijianchuo)
{
//shijianchuo是整数，否则要parseInt转换
    var time = new Date(shijianchuo);
    var y = time.getFullYear();
    var m = time.getMonth()+1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    var s = time.getSeconds();
    return y+'-'+add0(m)+'-'+add0(d)+' '+add0(h)+':'+add0(mm)+':'+add0(s);
}

//选中事件
function initCombobox(options) {
    var columns = options.columns;
    columns.splice(0, 1);
    $('#column').combobox({
        valueField: 'field',
        textField: 'title',
        data: columns,
        editable: false
    })
}