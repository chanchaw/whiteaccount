/**
 * 2021年10月28日 17:06:32 chanchaw
 * 原料管理 - 报表
 */
$(function () {
    let buttonGroup = [
        {
            id: 'btn_add',
            size: 'small',
            text: '新增入库',
            iconCls: 'icon-add',
            onClick: btn_add,
            plain: true
        },
        {
            id: 'btn_material_returining',
            size: 'small',
            text: '原料退回',
            iconCls: 'icon-add',
            onClick: btn_material_returining,
            plain: true
        },
        {
            id: 'btn_add_outbound',
            size: 'small',
            text: '新增出库',
            iconCls: 'icon-add',
            onClick: btn_add_outbound,
            plain: true
        },
        {
            id: 'btn_process_returining',
            size: 'small',
            text: '领料退回',
            iconCls: 'icon-add',
            onClick: btn_process_returining,
            plain: true
        },
        {
            id: 'btn_material_check',
            size: 'small',
            text: '盘点',
            iconCls: 'icon-add',
            onClick: btn_material_check,
            plain: true
        },
        {
            id: 'btn_cancel_filter',
            size: 'small',
            text: '清除过滤',
            iconCls: 'icon-clear',
            plain: true,
            onClick: btn_cancel_filter
        },
        {
            id: 'btn_export',
            size: 'small',
            text: '导出Excel',
            iconCls: 'icon-page_excel',
            plain: true,
            onClick: btn_export_excel
        },
        {
            id: 'btn_print',
            size: 'small',
            text: '打印',
            iconCls: 'icon-print',
            plain: true,
            onClick: btn_print
        },
        {
            id: 'btn_grid_columns',
            size: 'small',
            text: '列',
            iconCls: 'icon-text_columns',
            plain: true,
            onClick: btn_open_column_list
        },
        {
            id: 'btn_cancel_audit',
            size: 'small',
            disabled: true,
            text: '消审',
            iconCls: 'icon-exclamation',
            onClick: btn_cancel_audit,
            plain: true
        },
        {
            id: 'btn_close', size: 'small', text: '关闭', iconCls: 'icon-no', onClick: btn_close,
            plain: true
        },
    ];
    buttonGen($('#north'), buttonGroup);
    bindEnterEvents();
    let kGrid = $('#v_grid').data('kendoGrid');
    var options = kGrid.getOptions();
    if (options.columns.length > 0)
        initCombobox(options)
})
const materialColumns = [
    {title: 'id', field: 'id', hidden: true,},
    {
        title: '制单日期',
        field: 'orderDate',
        id: 'orderDateInput',
        editor: {type: 'datebox', options: {value: getCurrentDate()}}
    },
    {title: '单据编号', field: 'orderCode', editor: {type: 'textbox', options: {required: false, readonly: true}}},
    {
        title: '单据类型', field: 'billType', id: 'billTypeInput', editor: {
            type: 'combobox', options: {
                url: 'billType/listQuery',
                queryParams: {storageId: 1, accessMode: 1},
                method: 'GET',
                textField: 'billTypeName', valueField: 'id',
                panelHeight: 200,
                required: true,
                onLoadSuccess: function () {
                    $(this).combobox('options').limitToList = true;
                    let _data = $(this).combobox('getData') || [];
                    let _opts = $(this).combobox('options');
                    if (_data instanceof Array && !$(this).combobox('getValue') && _data.length > 0) {
                        for (let i of _data) {
                            if (i['isSelected']) {
                                $(this).combobox('setValue', i[_opts.valueField]);
                                break;
                            }
                        }
                    }
                }
            }
        }
    },
    {
        title: '付款方式', field: 'accountType', id: 'accountTypeInput', editor: {
            type: 'combobox', options: {
                url: 'materialOrder/listQuery/accountType',
                method: 'GET',
                textField: 'value', valueField: 'value',
                panelHeight: 'auto',
                required: true,
                value: '预付',
                onLoadSuccess: function () {
                    $(this).combobox('options').limitToList = true;
                    if (!$(this).combobox('getValue')) {
                        let options = $(this).combobox('options');
                        let loadData = $(this).combobox('getData') || [];
                        if (loadData.length > 0)
                            $(this).combobox('setValue', loadData[0][options.valueField]);
                    }
                }
            }
        }
    },
    {
        title: '原料商', field: 'supplierId', editor: {
            type: 'combobox', options: {
                url: 'supplier/listQuery',
                method: 'GET',
                textField: 'supplierName', valueField: 'id',
                panelHeight: 200,
                required: true,
                onLoadSuccess: function () {
                    $(this).combobox('options').limitToList = true;
                }
            }
        }
    },
    {
        title: '原料规格', field: 'materialSpec', editor: {
            type: 'combobox', options: {
                url: 'specification/listQuery',
                method: 'GET',
                textField: 'specName',
                valueField: 'specName',
                panelHeight: 200,
                required: true,
            }
        }
    },
    {title: '批号', field: 'batchNum', editor: {type: 'textbox', options: {required: true}}},
    {title: '件数', field: 'boxAmount', editor: {type: 'numberbox', options: {precision: 1, required: false}}},
    // {title: '入库匹数', field: 'inputAmount', editor: {type: 'numberbox', options: {precision: 0, required: true}}},
    {
        title: '重量',
        id: 'inputKiloId',
        field: 'inputKilo',
        editor: {type: 'numberbox', options: {precision: 1, required: true}}
    },
    {title: '单价', id: 'priceId', field: 'price', editor: {type: 'numberbox', options: {precision: 2, required: false}}},
    {
        title: '金额',
        id: 'inputMoneyId',
        field: 'inputMoney',
        editor: {type: 'numberbox', options: {precision: 2, readonly: true}}
    },
    {
        title: '加工户',
        field: 'processCompany',
        id: 'consigneeId',
        editor: {
            type: 'combobox', options: {
                width: 110, cls: 'input-red',
                url: 'processCompany/listQuery',
                queryParams: {isSelected: null},
                method: 'GET',
                textField: 'companyName',
                valueField: 'companyName',
                readonly: true,
                onLoadSuccess: function () {
                    $(this).combobox('options').limitToList = true;
                }
            }
        }
    },
    {
        title: '直发',
        width: '50px',
        field: 'isSendDirectly',
        editor: {
            type: 'checkbox', options: {
                cls: 'input-red', value: true, onChange: function (checked) {
                    let _consigneeValue = $('#consigneeId').combobox('getValue');
                    if (checked) {
                        $('#consigneeId').combobox({queryParams: {isSelected: false}, required: true, readonly: false});
                        $('#consigneeId').combobox('setValue', _consigneeValue);
                    } else {
                        $('#consigneeId').combobox('clear');
                        $('#consigneeId').combobox({queryParams: {isSelected: null}, required: false, readonly: true});
                    }
                }
            }
        }
    },
    {
        title: '凭证号',
        field: 'handlerName',
        editor: {
            type: 'textbox', options: {
                required: false,
            }
        }
    },
    {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {required: false}}},
    {
        title: '制单人',
        field: 'createUserId',
        editor: {
            type: 'combobox',
            options: {
                readonly: true,
                url: 'sysUser/query',
                method: 'GET',
                textField: 'userName',
                valueField: 'id',
                onLoadSuccess: function () {
                    $(this).combobox('options').limitToList = true;
                }
            }
        }
    },
];
const materialOutboundColumns = [
    {title: 'id', field: 'id', hidden: true,},
    {title: '制单日期', field: 'orderDate', editor: {type: 'datebox', options: {value: getCurrentDate()}}},
    {title: '单据编号', field: 'orderCode', editor: {type: 'textbox', options: {required: false, readonly: true}}},
    {
        title: '单据类型', field: 'billType', editor: {
            type: 'combobox', options: {
                url: 'billType/listQuery',
                queryParams: {storageId: 1, accessMode: -1},
                method: 'GET',
                textField: 'billTypeName', valueField: 'id',
                panelHeight: 200,
                required: true,
                readonly: true,
                onLoadSuccess: function () {
                    $(this).combobox('options').limitToList = true;
                    let _data = $(this).combobox('getData') || [];
                    let _opts = $(this).combobox('options');
                    if (_data instanceof Array && !$(this).combobox('getValue') && _data.length > 0) {
                        for (let i of _data) {
                            if (i['isSelected']) {
                                $(this).combobox('setValue', i[_opts.valueField]);
                                break;
                            }
                        }
                    }
                }
            }
        }
    },
    {
        title: '原料商', field: 'supplierId', editor: {
            type: 'combobox', options: {
                url: 'supplier/listQuery',
                method: 'GET',
                textField: 'supplierName', valueField: 'id',
                panelHeight: 200,
                required: true,
                readonly: true,
                onLoadSuccess: function () {
                    $(this).combobox('options').limitToList = true;
                }
            }
        }
    },
    {
        title: '原料规格', field: 'materialSpec', editor: {
            type: 'combobox', options: {
                url: 'specification/listQuery',
                method: 'GET',
                textField: 'specName',
                valueField: 'specName',
                panelHeight: 200,
                required: true,
                readonly: true,
            }
        }
    },
    {title: '批号', field: 'batchNum', editor: {type: 'textbox', options: {readonly: true,required: true}}},
    {title: '件数', field: 'boxAmount', editor: {type: 'numberbox', options: {precision: 1, required: false}}},
    {
        title: '重量',
        id: 'inputKiloId',
        field: 'inputKilo',
        editor: {type: 'numberbox', options: {precision: 1, required: true}}
    },
    {
        title: '加工户', field: 'processCompany', id: 'processCompanyInput', editor: {
            type: 'combobox', options: {
                required: false,
                url: 'processCompany/listQuery',
                method: 'GET',
                textField: 'companyName',
                valueField: 'companyName',
                onLoadSuccess: function () {
                    let _data = $(this).combobox('getData') || [];
                    let valueField = $(this).combobox('options').valueField;
                    if (!$(this).combobox('getValue'))
                        for (let i of _data) {
                            if (i['isSelected']) {
                                $(this).combobox('setValue', i[valueField]);
                                break;
                            }
                        }
                }
            }
        }
    },
    {
        title: '凭证号',
        field: 'handlerName',
        editor: {
            type: 'textbox', options: {
                required: false,
            }
        }
    },
    {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {required: false}}},
    {
        title: '制单人',
        field: 'createUserId',
        editor: {
            type: 'combobox',
            options: {
                readonly: true,
                url: 'sysUser/query',
                method: 'GET',
                textField: 'userName',
                valueField: 'id',
                onLoadSuccess: function () {
                    $(this).combobox('options').limitToList = true;
                }
            }
        }
    },
];

function setStatus(index) {
    let msg = {
        'tab0-1': ['新增出库', '领料退回', '盘点'],
        'tab1-1': ['原料退回', '领料退回'],
        'tab2-1': ['新增入库', '原料退回', '新增出库', '盘点'],
        'tab0': ['原料退回', '新增出库', '领料退回', '盘点'],
        'tab1': ['新增入库', '原料退回', '新增出库', '领料退回', '盘点'],
        'tab2': ['新增入库', '原料退回', '新增出库', '领料退回', '盘点'],
    };
    setTimeout(function () {
        $('#north').find('a[class*="easyui-linkbutton"]').each(function (item, i) {
            let text = $(i).linkbutton('options').text;
            if (msg[`tab${index}`] == undefined) return false;
            if (msg[`tab${index}`].indexOf(text) > -1) {
                $(i).linkbutton('disable');
            } else {
                $(i).linkbutton('enable');
            }
        })
    }, 0);
    if (index >= 3) {
        $('#state').switchbutton('enable');
    } else {
        $('#state').switchbutton('disable');
    }
}

/**
 * 原料管理 - 第一个标签页 - 原料入库表格数据源
 */
function grid_1() {
    let SUFFIX = "";
    let batch_num = new kendo.data.DataSource({data: []});
    let account_type = new kendo.data.DataSource({data: []});
    let supplier_name = new kendo.data.DataSource({data: []});
    let order_code = new kendo.data.DataSource({data: []});
    let create_user_name = new kendo.data.DataSource({data: []});
    let bill_type_name = new kendo.data.DataSource({data: []});
    let material_spec = new kendo.data.DataSource({data: []});
    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "materialOrder/report",
                dataType: "json",
                type: "GET",
                data: {
                    start: $('#start').datebox('getValue'),
                    end: $('#end').datebox('getValue'),
                    supplierName: $('#contactCompanyId').combobox('getValue'),
                    // textField: $('#column').combobox('getValue'),
                    // valueField: $('#value').textbox('getValue'),
                }
            }
        },
        schema: {
            model: {
                id: 'id',
                fields: {
                    order_date: {type: "date"},
                    create_time: {type: "date"},
                    input_kilo: {type: "number"},
                    input_money: {type: "number"},
                    box_amount: {type: "number"},
                }
            }
        },
        aggregate: [
            {field: "input_kilo", aggregate: "sum"},
            {field: "input_money", aggregate: "sum"},
            {field: "box_amount", aggregate: "sum"},
        ],
        change: function (e) {
            let data = this.data();
            batch_num.data(unique(data, 'batch_num'));
            account_type.data(unique(data, 'account_type'));
            supplier_name.data(unique(data, 'supplier_name'));
            order_code.data(unique(data, 'order_code'));
            material_spec.data(unique(data, 'material_spec'));
            create_user_name.data(unique(data, 'create_user_name'));
            bill_type_name.data(unique(data, 'bill_type_name'));
        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })
            // $('body').find('th a[class="k-grid-filter"]').css({display: 'none'});
        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })
            // $('body').find('th a[class="k-grid-filter"]').css({display: 'none'});
        }
    });
    let columns = [
        {field: "no", title: "#", filterable: false, template: "<span class='row_number'></span>", width: 40},
        {
            field: "id",
            title: '编号',
            width: 93,
            hidden: true,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
        },
        {
            field: "supplier_id",
            title: '原料商编号',
            width: 93,
            hidden: true,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
        },
        {
            field: "order_date",
            title: '单据日期',
            width: 85,
            format: "{0: MM-dd}",
            filterable: false,
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            footerTemplate: '总计:',
        },
        {
            field: "batch_num",
            title: '批次号',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: batch_num}
        },
        {
            field: "handler_name",
            title: '凭证号',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: false
        },
        {
            field: "supplier_name",
            title: '原料商',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: supplier_name}
        },
        {
            field: "order_code",
            title: '单据编号',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: false,
        },
        {
            field: "material_spec",
            title: '原料规格',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: material_spec}
        },
        {
            field: "bill_type_name",
            title: '单据类型',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: bill_type_name}
        },
        {
            field: "is_send_directly",
            title: '直发',
            width: 93,
            template: item => item['is_send_directly'] == '是' ? `<input type="checkbox" checked onclick="return false;">` : '',
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true}
        },
        {
            field: "account_type",
            title: '结算',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {checkAll: true, multi: true, search: true, dataSource: account_type}
        },
        {
            field: "box_amount",
            title: '件数',
            width: 93,
            template: item => item['box_amount'] ? parseFloat(item['box_amount']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            filterable: false,
        },
        {
            field: "input_kilo",
            title: '入库重量',
            width: 93,
            template: item => item['input_kilo'] ? parseFloat(item['input_kilo']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false,
        },
        {
            field: "price",
            title: '单价',
            width: 93,
            template: item => item['price'] ? parseFloat(item['price']).toFixed(2) : '',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            filterable: false,
        },
        {
            field: "input_money",
            title: '入库金额',
            width: 93,
            template: item => item['input_money'] ? parseFloat(item['input_money']).toFixed(2) : '',
            footerTemplate: '#=kendo.format("{0:n2}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            filterable: false,
        },
        {
            field: "remarks",
            title: '备注',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false,
        },
        {
            title: "制单人",
            field: 'create_user_name',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false,
        },
        {
            title: '制单时间',
            field: "create_time",
            width: 85,
            format: "{0: MM-dd HH:mm}",
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            filterable: false,
        },
    ];
    let grid = $("#v_grid").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "原料序时表.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            let kGrid = $('#v_grid').data('kendoGrid');
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
                let dataItem = kGrid.dataItem($(this));
                if (dataItem['is_audit'] != undefined && !dataItem['is_audit']) {
                    $(this).addClass('kendo-custum-non-checked');
                }
            });
        },
        columnHide: function (e) {
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
        },
        columnShow: function (e) {
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null, SUFFIX);
        },
        columnResize: function (e) {
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null, SUFFIX);
            }
        },
        columnReorder: function (e) {
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field'] + SUFFIX
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid01.setOptions({
        columns: columns
    });
    grid.on('click', 'tr', function (e) {
        setStatus('0-1')
        let kGrid = $("#v_grid").data("kendoGrid");
        let dataItem = kGrid.dataItem(e.currentTarget);
        if (dataItem['is_audit']) {
            $('#btn_cancel_audit').linkbutton('enable')
        } else {
            $('#btn_cancel_audit').linkbutton('disable')
        }
    })
    grid.on('dblclick', 'tr', function (e) {
        //  双击打开原单
        let kGrid = $("#v_grid").data("kendoGrid");
        let dataItem = kGrid.dataItem(e.currentTarget);
        if (dataItem == undefined) {
            $.messager.alert('提示', '未选中!', 'warning');
            return;
        }
        $.ajax({
            url: `materialOrder/queryByPk/${dataItem.id}`,
            type: 'GET',
            dataType: 'json'
        }).then(data => {
            if (data != undefined && data.state == 1) {
                openDialog(data, '原料入库');
                $.formDialog.disableBtn(['保存']);
                $.formDialog.disableEditing();
            }
        }, () => {
        })
    });
}

/**
 * 原料管理 - 第二个标签页 - 原料库存表格数据源
 */
function grid_2() {
    let SUFFIX = "_a";
    let batch_num = new kendo.data.DataSource({data: []});
    let material_spec = new kendo.data.DataSource({data: []});
    let supplier_name = new kendo.data.DataSource({data: []});
    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "materialOrder/report/storage",
                dataType: "json",
                type: "GET",
                data: {
                    supplierName: $('#contactCompanyId').combobox('getValue')
                    // textField: $('#column').combobox('getValue'),
                    // valueField: $('#value').textbox('getValue'),
                }
            }
        },
        schema: {
            model: {
                id: 'id',
                fields: {
                    input_kilo: {type: "number"},
                    box_amount: {type: "number"},
                }
            }
        },
        aggregate: [
            {field: "input_kilo", aggregate: "sum"},
            {field: "box_amount", aggregate: "sum"},
        ],
        change: function (e) {
            let data = this.data();
            batch_num.data(unique(data, 'batch_num'));
            material_spec.data(unique(data, 'material_spec'));
            supplier_name.data(unique(data, 'supplier_name'));
        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })
            // $('body').find('th a[class="k-grid-filter"]').css({display: 'none'});
        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })
        }
    });
    let columns = [
        {field: "no", title: "#", filterable: false, template: "<span class='row_number'></span>", width: 40},
        {
            field: "supplier_id",
            title: '原料商编号',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            hidden: true
        },
        {
            field: "batch_num",
            title: '批次号',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: batch_num}
        },
        {
            field: "supplier_name",
            title: '原料商',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: supplier_name}
        },
        {
            field: "material_spec",
            title: '原料规格',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: material_spec}
        },
        {
            field: "input_kilo",
            title: '重量',
            width: 93,
            template: item => item['input_kilo'] ? parseFloat(item['input_kilo']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false
        },
        {
            field: "box_amount",
            title: '件数',
            width: 93,
            template: item => item['box_amount'] ? parseFloat(item['box_amount']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false
        },
    ];
    let grid = $("#v_grid_a").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "原料库存.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
            });
        },
        columnHide: function (e) {
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
        },
        columnShow: function (e) {
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null, SUFFIX);
        },
        columnResize: function (e) {
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null, SUFFIX);
            }
        },
        columnReorder: function (e) {
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field'] + SUFFIX
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid_a').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid01.setOptions({
        columns: columns
    });
    grid.on('click', 'tr', function (e) {
        setStatus('1-1');
    })
}

/**
 * 原料管理 - 第三个标签页 - 原料出库表格数据源
 */
function grid_3() {
    let SUFFIX = "_b";
    let batch_num = new kendo.data.DataSource({data: []});
    let bill_type_name = new kendo.data.DataSource({data: []});
    let handler_name = new kendo.data.DataSource({data: []});
    let supplier_name = new kendo.data.DataSource({data: []});
    let order_code = new kendo.data.DataSource({data: []});
    let material_spec = new kendo.data.DataSource({data: []});
    let dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "materialOrder/report/outbound",
                dataType: "json",
                type: "GET",
                data: {
                    start: $('#start').datebox('getValue'),
                    end: $('#end').datebox('getValue'),
                    processName: $('#contactCompanyId').combobox('getValue'),
                    // textField: $('#column').combobox('getValue'),
                    // valueField: $('#value').textbox('getValue'),
                }
            }
        },
        schema: {
            model: {
                id: 'id',
                fields: {
                    order_date: {type: "date"},
                    create_time: {type: "date"},
                    input_kilo: {type: "number"},
                    input_money: {type: "number"},
                    box_amount: {type: "number"},
                }
            }
        },
        aggregate: [
            {field: "input_kilo", aggregate: "sum"},
            {field: "input_money", aggregate: "sum"},
            {field: "box_amount", aggregate: "sum"},
        ],
        change: function (e) {
            let data = this.data();
            batch_num.data(unique(data, 'batch_num'));
            bill_type_name.data(unique(data, 'bill_type_name'));
            handler_name.data(unique(data, 'handler_name'));
            supplier_name.data(unique(data, 'supplier_name'));
            order_code.data(unique(data, 'order_code'));
            material_spec.data(unique(data, 'material_spec'));

            $('#btn_cancel_audit').linkbutton('disable')
        },
        pageSize: 200,
        requestStart: function (e) {
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('disable')
                }
            })
            // $('body').find('th a[class="k-grid-filter"]').css({display: 'none'});
        },
        requestEnd: function (e) {
            let response = e.response;
            if (response && response['message']) {
                $.messager.show({title: '提示', msg: response['message']})
            }
            $('.easyui-linkbutton').each(function (index, item) {
                let btn = $(item);
                if (btn.linkbutton('options').text === '查询' || (btn.linkbutton('options').text === '过滤')) {
                    btn.linkbutton('enable')
                }
            })
            // $('body').find('th a[class="k-grid-filter"]').css({display: 'none'});
        }
    });
    let columns = [
        {field: "no", title: "#", filterable: false, template: "<span class='row_number'></span>", width: 40},
        {
            field: "id",
            title: '编号',
            width: 93,
            hidden: true,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
        },
        {
            field: "access_mode",
            title: '扣库系数',
            width: 93,
            hidden: true,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
        },
        {
            field: "supplier_id",
            title: '原料商编号',
            width: 93,
            hidden: true,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
        },
        {
            field: "order_date",
            title: '单据日期',
            width: 85,
            format: "{0: MM-dd}",
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            footerTemplate: '总计:',
            filterable: false,
        },
        {
            field: "bill_type_name",
            title: '单据类型',
            width: 93,
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: bill_type_name}
        },
        {
            field: "batch_num",
            title: '批次号',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: batch_num}
        },
        {
            field: "supplier_name",
            title: '原料商',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: supplier_name}
        },
        {
            field: "process_company",
            title: '加工户',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: supplier_name}
        },
        {
            field: "is_send_directly",
            title: '直发',
            width: 93,
            template: item => item['is_send_directly'] == '是' ? `<input type="checkbox" checked onclick="return false;">` : '',
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true}
        },
        {
            field: "handler_name",
            title: '凭证号',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: handler_name}
        },
        {
            field: "order_code",
            title: '单据编号',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: order_code}
        },
        {
            field: "material_spec",
            title: '原料规格',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;text-align:center;'},
            headerAttributes: {style: 'text-align:center;'},
            footerAttributes: {style: 'color: red;text-align:center;'},
            filterable: {multi: true, search: true, dataSource: material_spec}
        },
        {
            field: "input_kilo",
            title: '领料重量',
            width: 93,
            template: item => item['input_kilo'] ? parseFloat(item['input_kilo']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            filterable: false,
        },
        {
            field: "box_amount",
            title: '领料件数',
            width: 93,
            template: item => item['box_amount'] ? parseFloat(item['box_amount']).toFixed(1) : '',
            footerTemplate: '#=kendo.format("{0:n1}",sum)#',
            footerAttributes: {style: 'text-align:right;'},
            attributes: {'class': 'kendo-custum-number'},
            headerAttributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            filterable: false,
        },
        {
            title: '制单时间',
            field: "create_time",
            width: 85,
            format: "{0: MM-dd HH:mm}",
            headerAttributes: {style: 'text-align:center;'},
            attributes: {style: 'text-align:center;white-space:nowrap;text-overflow:ellipsis;'},
            filterable: false,
        },
        {
            field: "create_user_name",
            title: '制单人',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false,
        },
        {
            field: "remarks",
            title: '备注',
            width: 93,
            attributes: {style: 'white-space:nowrap;text-overflow:ellipsis;'},
            headerAttributes: {style: 'text-align:center;'},
            filterable: false,
        },
    ];
    let grid = $("#v_grid_b").kendoGrid({
        sortable: true,
        resizable: true,
        selectable: "multiple row",
        editable: false,
        filterable: {
            extra: false
        },
        dataSource: dataSource,
        columnMenu: false,
        excel: {
            fileName: "原料领料.xlsx",
            proxyURL: "",
            allPages: true,
            filterable: true
        },
        height: '99%',
        pageable: {
            pageSize: 200
        },
        reorderable: true,
        dataBound: function () {
            let rows = this.items();
            let kGrid = $('#v_grid_b').data('kendoGrid');
            $(rows).each(function () {
                let index = $(this).index() + 1;
                let rowLabel = $(this).find(".row_number");
                $(rowLabel).html(index);
                let dataItem = kGrid.dataItem($(this));
                if (dataItem['is_audit'] != undefined && !dataItem['is_audit']) {
                    $(this).addClass('kendo-custum-non-checked');
                }
            });
        },
        columnHide: function (e) {
            //列隐藏事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
        },
        columnShow: function (e) {
            //列显示事件
            let field = e.column.field;
            columnResizeEvent($('body').data('menu_id'), field, null, false, null, SUFFIX);
        },
        columnResize: function (e) {
            //  列伸缩事件
            let field = e.column.field;
            let newWidth = e.newWidth;
            if (newWidth < 20) {
                columnResizeEvent($('body').data('menu_id'), field, null, true, null, SUFFIX);
            } else {
                columnResizeEvent($('body').data('menu_id'), field, newWidth, null, null, SUFFIX);
            }
        },
        columnReorder: function (e) {
            //  列拖动事件
            let newColumns = JSON.parse(JSON.stringify(e.sender.columns))
            newColumns = reorder(newColumns, e.oldIndex, e.newIndex);
            let menuId = $('body').data('menu_id');
            let col = []
            for (let i = 0; i < newColumns.length; i++) {
                if (newColumns[i]['field'])
                    col.push({
                        serialNum: i,
                        sysMenuId: menuId,
                        field: newColumns[i]['field'] + SUFFIX
                    });
            }
            $.ajax({
                url: 'sysUserTableConfig/modify/reorder',
                contentType: 'application/json',
                data: JSON.stringify(col),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                }
            })
        },
    });
    let grid01 = $('#v_grid_b').data('kendoGrid');
    loadOptions(grid01, columns, SUFFIX, $('body').data('menu_id'));
    grid01.setOptions({
        columns: columns
    });
    grid.on('click', 'tr', function (e) {
        setStatus('2-1')
        let kGrid = $("#v_grid_b").data("kendoGrid");
        let dataItem = kGrid.dataItem(e.currentTarget);
        if (dataItem['is_audit']) {
            $('#btn_cancel_audit').linkbutton('enable')
        } else {
            $('#btn_cancel_audit').linkbutton('disable')
        }
    });
    grid.on('dblclick', 'tr', function (e) {
        //  双击打开原单
        let kGrid = $("#v_grid_b").data("kendoGrid");
        let dataItem = kGrid.dataItem(e.currentTarget);
        if (dataItem == undefined) {
            $.messager.alert('提示', '未选中!', 'warning');
            return;
        }
        $.ajax({
            url: `materialOrder/queryByPk/${dataItem.id}`,
            type: 'GET',
            dataType: 'json'
        }).then(data => {
            if (data != undefined && data.state == 1) {
                if (dataItem['access_mode'] > 0) {
                    openDialog(data, dataItem['bill_type_name']);
                } else {
                    openOutboundDialog(data, dataItem['bill_type_name']);
                }
                $.formDialog.disableBtn(['保存']);
                $.formDialog.disableEditing();
            }
        }, () => {
        })
    });
}

function onSelect(title, index) {
    function setContactCompanyState(index) {
        if (index <= 1) {
            $('#contactCompanyId').combobox('options').label = '原料商：';
            $('#contactCompanyId').combobox('options').url = 'supplier/listQuery';
            $('#contactCompanyId').combobox('options').valueField = 'supplierName';
            $('#contactCompanyId').combobox('options').textField = 'supplierName';
        } else {
            $('#contactCompanyId').combobox('options').label = '加工户：';
            $('#contactCompanyId').combobox('options').url = 'processCompany/listQuery';
            $('#contactCompanyId').combobox('options').valueField = 'companyName';
            $('#contactCompanyId').combobox('options').textField = 'companyName';
        }
        $('#contactCompanyId').combobox();
    }

    setContactCompanyState(index);
    setStatus(index);
    switch (index) {
        case 0:
            if ($('#v_grid').data('kendoGrid') == undefined) {
                grid_1();
            }
            break;
        case 1:
            if ($('#v_grid_a').data('kendoGrid') == undefined) {
                grid_2();
            }
            break;
        case 2:
            if ($('#v_grid_b').data('kendoGrid') == undefined) {
                grid_3();
            }
            break;
    }
}

function btn_cancel_filter() {
    getSelectedGrid().dataSource.filter({});
}

function btn_search() {
    function setCol(ele, show = []) {
        let kGrid = ele.data('kendoGrid');
        let columns = kGrid.columns;
        let checked = $('#state').switchbutton('options').checked;
        if (!checked)
            for (let col of columns) {
                if (col['field'] && show.indexOf(col['field']) == -1) {
                    kGrid.hideColumn(col);
                } else if (col['field']) {
                    kGrid.showColumn(col);
                }
            }
        else {
            for (let col of columns) {
                if (col['title'] && col['title'].indexOf('编号') > -1)
                    kGrid.hideColumn(col);
                else
                    kGrid.showColumn(col);
            }
        }
    }

    function research(index) {
        switch (index) {
            case 0:
                if ($('#v_grid').data('kendoGrid') != undefined)
                    $('#v_grid').data('kendoGrid').dataSource.read({
                        start: $('#start').datebox('getValue'),
                        end: $('#end').datebox('getValue'),
                        supplierName: $('#contactCompanyId').combobox('getValue'),
                        // textField: $('#column').combobox('getValue'),
                        // valueField: $('#value').textbox('getValue'),
                    });
                break;
            case 1:
                if ($('#v_grid_a').data('kendoGrid') != undefined)
                    $('#v_grid_a').data('kendoGrid').dataSource.read({
                        supplierName: $('#contactCompanyId').combobox('getValue'),
                        // textField: $('#column').combobox('getValue'),
                        // valueField: $('#value').textbox('getValue'),
                    });
                break;
            case 2:
                if ($('#v_grid_b').data('kendoGrid') != undefined)
                    $('#v_grid_b').data('kendoGrid').dataSource.read({
                        start: $('#start').datebox('getValue'),
                        end: $('#end').datebox('getValue'),
                        processName: $('#contactCompanyId').combobox('getValue'),
                        // textField: $('#column').combobox('getValue'),
                        // valueField: $('#value').textbox('getValue'),
                    });
                break;
        }
    }

    for (let i = 0; i < 3; i++) {
        research(i);
    }
    setStatus(getCurrentIndex());
}

/**
 * 2021年10月28日 17:07:09 chanchaw
 * 新增入库
 */
function btn_add() {
    let param = {};
    if (getCurrentIndex() == 1) {
        let kGrid = getSelectedGrid();
        let dataItem = kGrid.dataItem(kGrid.select());

        if (dataItem != undefined) {
            param.batchNum = dataItem['batch_num'];
            param.materialSpec = dataItem['material_spec'];
            param.supplierId = dataItem['supplier_id'];
        }
    }
    openDialog(param, '原料入库');
    $.formDialog.enableBtn(['保存', '关闭']);
}

function btn_add_outbound(param = {}) {
    param.orderDate = getCurrentDate();
    if (getCurrentIndex() == 1) {
        let kGrid = $("#v_grid_a").data("kendoGrid");
        let dataItem = kGrid.dataItem(kGrid.select());
        if (dataItem == undefined) {
            $.messager.alert('提示', '请选中库存！！', 'warning');
            return;
        }
        param.batchNum = dataItem['batch_num'];
        param.materialSpec = dataItem['material_spec'];
        param.supplierId = dataItem['supplier_id'];
        if ($('#processCompanyInput').length > 0) {
            let _data = $('#processCompanyInput').combobox('getData') || [];
            for (let i of _data) {
                if (i['isSelected']) {
                    let valueField = $('#processCompanyInput').combobox('options').valueField;
                    param.processCompany = i[valueField];
                    break;
                }
            }
        }
    }
    openOutboundDialog(param);
    $.formDialog.enableBtn(['保存', '关闭']);
}

function openDialog(loadData, title) {
    $.formDialog.open({
        columns: materialColumns,
        onOpen: function () {
            $('#inputKiloId').numberbox('textbox').bind('keyup', function (e) {
                let price = $('#priceId').numberbox('getValue') || 0;
                let _value = $(this).val();
                $('#inputMoneyId').numberbox('setValue', price * _value);
            })
            $('#priceId').numberbox('textbox').bind('keyup', function (e) {
                let qty = $('#inputKiloId').numberbox('getValue') || 0;
                let _value = $(this).val();
                $('#inputMoneyId').numberbox('setValue', qty * _value);
            });
            $('.form-dialog-edit').find('.form-row:visible').eq(0).css({'margin-top': '10px'});
        },
        loadData: loadData,
        mode: title ? title : '新增入库',
        width: 459,
        height: 653,
        buttons: [
            {
                text: '新增', iconCls: 'icon-add', handler: e => {
                    $('.form-dialog-edit').eq(0).dialog('panel').find('form').form('clear');
                    $.formDialog.enableBtn(['保存', '关闭']);
                    $.formDialog.enableEditing();
                    $.formDialog.close();
                    btn_add();
                }
            },
            {
                text: '修改', iconCls: 'icon-edit', handler: e => {
                    $.formDialog.enableBtn(['保存', '关闭']);
                    $.formDialog.enableEditing();
                }
            },
            {
                text: '保存', iconCls: 'icon-save', handler: e => {
                    let url = '';
                    let formData = $('.form-dialog-edit').eq(0).dialog('panel').find('form').serializeObject();
                    if (formData['id']) {
                        url = 'materialOrder/updateSelective';
                    } else {
                        url = 'materialOrder/insertSelective';
                    }
                    $('.form-dialog-edit').eq(0).dialog('panel').find('form').form('submit', {
                        url: url,
                        success: function (data) {
                            data = eval(`(${data})`);
                            if (data.success) {
                                $.formDialog.disableBtn(['保存']);
                                $.formDialog.disableEditing();
                                $('.form-dialog-edit').eq(0).dialog('panel').find('form').form('load', data.data);
                                btn_search();
                                $.messager.show({title: '提示', msg: data.message});
                            } else {
                                $.messager.alert('提示', data.message, 'error');
                            }
                        }
                    });
                }
            },
            {
                text: '复制', iconCls: 'icon-page_copy', handler: e => {
                    $('.form-dialog-edit').eq(0).dialog('panel').find('form').form('load', {
                        id: null
                    });
                    $.formDialog.enableEditing();
                    $.formDialog.enableBtn(['保存', '关闭']);
                }
            },
            {
                text: '作废', iconCls: 'icon-cancel', handler: e => {
                    $.messager.confirm('提示', '是否作废？', function (r) {
                        if (r) {
                            let formData = $('.form-dialog-edit').find('form').serializeObject();
                            if (!formData['id']) {
                                $.messager.alert('提示', '单据未保存!', 'warning');
                                return;
                            }
                            $.ajax({
                                url: 'materialOrder/cancelByPk',
                                data: {id: formData.id},
                                type: 'POST',
                                dataType: 'json',
                                success: function (data) {
                                    if (data.success) {
                                        $.formDialog.close();
                                        btn_search();
                                        $.messager.show({title: '提示', msg: data.message});
                                    } else {
                                        $.messager.alert('提示', data.message, 'error');
                                    }
                                }
                            });
                        }
                    })
                }
            },
            {
                text: '关闭', iconCls: 'icon-no', handler: e => {
                    $('.form-dialog-edit').eq(0).dialog('close');
                }
            }]
    })
}

function openDialogByOpts(opts) {
    $.formDialog.open(opts);
}

function openOutboundDialog(loadData, title) {
    $.formDialog.open({
        columns: materialOutboundColumns,
        onOpen: function () {
            $('.form-dialog-edit').find('.form-row:visible').eq(0).css({'margin-top': '10px'});
        },
        loadData: loadData,
        mode: title ? title : '新增出库',
        width: 459,
        height: 587,
        buttons: [
            {
                text: '新增', iconCls: 'icon-add', handler: e => {
                    $('.form-dialog-edit').eq(0).dialog('panel').find('form').form('clear');
                    $.formDialog.enableBtn(['保存', '关闭']);
                    $.formDialog.enableEditing();
                    $.formDialog.close();
                    btn_add_outbound();
                }
            },
            {
                text: '修改', iconCls: 'icon-edit', handler: e => {
                    $.formDialog.enableBtn(['保存', '关闭']);
                    $.formDialog.enableEditing();
                }
            },
            {
                text: '保存', iconCls: 'icon-save', handler: e => {
                    let url = '';
                    let formData = $('.form-dialog-edit').eq(0).dialog('panel').find('form').serializeObject();
                    if (formData['id']) {
                        url = 'materialOrder/updateSelective';
                    } else {
                        url = 'materialOrder/insertSelective';
                    }
                    ex();

                    function ex() {
                        $('.form-dialog-edit').eq(0).dialog('panel').find('form').form('submit', {
                            url: url,
                            success: function (data) {
                                data = eval(`(${data})`);
                                if (data.success) {
                                    $.formDialog.disableBtn(['保存']);
                                    $.formDialog.disableEditing();
                                    $('.form-dialog-edit').eq(0).dialog('panel').find('form').form('load', data.data);
                                    btn_search();
                                    $.messager.show({title: '提示', msg: data.message});
                                } else {
                                    $.messager.alert('提示', data.message, 'error');
                                }
                            }
                        });
                    }

                }
            },
            {
                text: '复制', iconCls: 'icon-page_copy', handler: e => {
                    $('.form-dialog-edit').eq(0).dialog('panel').find('form').form('load', {
                        id: null
                    });
                    $.formDialog.enableEditing();
                    $.formDialog.enableBtn(['保存', '关闭']);
                }
            },
            {
                text: '作废', iconCls: 'icon-cancel', handler: e => {
                    $.messager.confirm('提示', '是否作废？', function (r) {
                        if (r) {
                            let formData = $('.form-dialog-edit').find('form').serializeObject();
                            if (!formData['id']) {
                                $.messager.alert('提示', '单据未保存!', 'warning');
                                return;
                            }
                            $.ajax({
                                url: 'materialOrder/cancelByPk',
                                data: {id: formData.id},
                                type: 'POST',
                                dataType: 'json',
                                success: function (data) {
                                    if (data.success) {
                                        $.formDialog.close();
                                        btn_search();
                                        $.messager.show({title: '提示', msg: data.message});
                                    } else {
                                        $.messager.alert('提示', data.message, 'error');
                                    }
                                }
                            });
                        }
                    })
                }
            },
            {
                text: '关闭', iconCls: 'icon-no', handler: e => {
                    $('.form-dialog-edit').eq(0).dialog('close');
                }
            }]
    })
}

function btn_print() {
    let reportlets = [];
    $.get('customize/getDatabase', {}, data => {
        if (data.success) {
            switch (getCurrentIndex()) {
                case 0:
                    reportlets.push({
                        reportlet: `${data.data}/原料入库序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 1:
                    reportlets.push({
                        reportlet: `${data.data}/原料库存序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 2:
                    reportlets.push({
                        reportlet: `${data.data}/原料领料序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 3:
                    if (!$('#contactCompanyId').combobox('getValue')) {
                        $.messager.alert('提示', '请选择客户再打印！', 'warning');
                        return;
                    }
                    reportlets.push({
                        reportlet: `${data.data}/应付款序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
                case 4:
                    if (!$('#contactCompanyId').combobox('getValue')) {
                        $.messager.alert('提示', '请选择客户再打印！', 'warning');
                        return;
                    }
                    reportlets.push({
                        reportlet: `${data.data}/已付款序时表打印.cpt`,
                        Sdate: $('#start').datebox('getValue'),
                        Edate: $('#end').datebox('getValue'),
                        clientName: $('#contactCompanyId').combobox('getValue') || '',
                        database: data.data
                    });
                    window.parent.FR.doURLPDFPrint({
                        url: $(window.parent.document.body).data('report_url'),
                        isPopUp: false,
                        data: {
                            reportlets: reportlets
                        }
                    })
                    break;
            }
        }
    }, 'json');
}

function btn_open_column_list() {
    let kGrid = getSelectedGrid();
    let options = kGrid.getOptions().columns;
    if ($('#btn_open_column_list').length <= 0) {
        let div = $(`<div data-options="
                    resizable:true,iconCls:'icon-text_columns',width:200,height:300,modal:true,title:'列',
                    onClose:function(){$(this).dialog('destroy')}" 
                          class="easyui-dialog" id="btn_open_column_list">
                   </div>`);
        let form = $('<form style="width: 100%;height: 100%;"></form>')
        for (let i of options) {
            if (i.field) {
                let checkbox = $(`<div style="width: 100%;height: auto;padding: 5px;">
                <input class="easyui-checkbox" name="${i.field}" data-options="label:'${i.title}',
                labelWidth:120,value:true,name:'${i.field}'">
                </div>`);
                form.append(checkbox);
            }
        }
        let container = $(`<div></div>`);
        form.appendTo(div);
        div.appendTo(container);
        $('body').append(container);
        $.parser.parse($('#btn_open_column_list').parent());
        let formData = {};
        options.map(item => {
            formData[item['field']] = item['hidden'] == undefined ? true : !item['hidden'];
        })
        $('#btn_open_column_list form').eq(0).form('load', formData);
        $('#btn_open_column_list form .easyui-checkbox').each(function () {
            //  绑定事件
            $(this).checkbox({
                onChange: function (checked) {
                    let opts = $(this).checkbox('options');
                    if (checked) {
                        kGrid.showColumn(opts.name);
                    } else {
                        kGrid.hideColumn(opts.name);
                    }
                }
            })
        })
    } else {
        $('#btn_open_column_list').dialog('open');
    }
}

function btn_export_excel() {
    getSelectedGrid().saveAsExcel();
}

//  消审
function btn_cancel_audit() {
    let kGrid = getCurrentSelectedGrid();
    if (kGrid == undefined) return;
    let selected = kGrid.select();
    if (selected == undefined) {
        messagerAlert('请选中！');
        return;
    }
    let dataItems = [];
    selected.each(function () {
        let dataItem = kGrid.dataItem(this);
        if (dataItem.id) {
            dataItems.push(dataItem.id);
        }
    });
    if (dataItems.length <= 0) {
        messagerAlert('请选中！');
        return;
    }
    messagerConfirm('确定消审？', r => {
        if (r) {
            $.ajax({
                url: 'materialOrder/cancelAudit',
                data: {id: dataItems},
                type: 'POST',
                dataType: 'json',
                traditional: true,
            }).then(function (data) {
                if (data.success) {
                    btn_search();
                } else {
                    messagerAlert(data.message);
                }
            })
        }
    })
}

function getCurrentSelectedGrid() {
    switch (getCurrentIndex()) {
        case 0:
            return $('#v_grid').data('kendoGrid')
        case 2:
            return $('#v_grid_b').data('kendoGrid');
    }
}

function btn_close() {
    window.parent.closeTab();
}

function btn_add_payment() {
    let columns = [
        {
            field: 'clientName',
            title: '供应商',
            editor: {
                type: 'combobox', options: {
                    url: 'supplier/listQuery',
                    method: 'GET', queryParams: {state: 1},
                    textField: 'supplierName', valueField: 'supplierName',
                    panelHeight: 200,
                    required: true,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                        if (!$(this).combobox('getValue')) {
                            let loadData = $(this).combobox('getData') || [];
                            let opts = $(this).combobox('options');
                            if (loadData.length > 0) {
                                if ($('#contactCompanyId').combobox('getValue')) {
                                    $(this).combobox('setValue', $('#contactCompanyId').combobox('getValue'));
                                } else {
                                    $(this).combobox('setValue', loadData[0][opts.valueField]);
                                }
                            }
                        }
                    }
                }
            }
        },
        {
            field: 'billDate',
            title: '日期',
            editor: {type: 'datebox', options: {required: true, value: getCurrentDate()}}
        },
        {
            field: 'billtype',
            title: '项目',
            editor: {
                type: 'combobox', options: {
                    required: true,
                    data: [{value: '付款'}, {value: '付款调整'}],
                    textField: 'value',
                    valueField: 'value',
                    limitToList: true,
                    panelHeight: 'auto',
                    onChange: function (newVal, oldVal) {
                        if (newVal == '付款调整') {
                            $('#collectiontypeInput').combobox('clear');
                            $('#collectiontypeInput').combobox('disable');
                        } else {
                            $('#collectiontypeInput').combobox('enable');
                        }
                    }
                }
            }
        },
        {
            field: 'money',
            title: '金额',
            editor: {type: 'numberbox', options: {precision: 2, required: true,}}
        },
        {
            field: 'payableType',
            id: 'collectiontypeInput',
            title: '付款类型',
            editor: {
                type: 'combobox', options: {
                    data: [{value: '转账'}, {value: '承兑'}, {value: '现金'}],
                    textField: 'value',
                    valueField: 'value',
                    limitToList: true,
                    panelHeight: 'auto'
                }
            }
        },
        {
            field: 'billmemo',
            title: '备注',
            editor: {type: 'textbox', options: {multiline: true, height: 100}}
        }
    ];
    let loadData = {};
    openDialogByOpts({
        loadData: loadData,
        columns: columns,
        height: 380,
        width: 330,
        mode: '新增付款',
        buttons: [{
            text: '保存', iconCls: 'icon-save', handler: e => {
                let dialog = $('.form-dialog-edit');
                if (!$(dialog.find('form')).form('validate')) {
                    return;
                }
                let param = $(dialog.find('form')).serializeObject();
                let url = 'materialAccountPayable/insertSelective';
                //  修改
                $.messager.progress({
                    title: '',
                    msg: '请稍等...',
                    interval: 5000
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: param,
                    success: function (data) {
                        if (data.success) {
                            btn_search();
                            $.formDialog.close();
                            $.messager.show({title: '提示', msg: data.message});
                        } else {
                            $.messager.alert('提示', data.message, 'warning');
                        }
                    },
                    complete: function () {
                        $.messager.progress('close');
                    }
                })
            }
        }, {
            text: '取消', iconCls: 'icon-cancel', handler: e => {
                $.formDialog.close();
            }
        }]
    });
}

function getSelectedGrid() {
    let kendoGridID = '';
    switch (getCurrentIndex()) {
        case 0:
            kendoGridID = '#v_grid';
            break;
        case 1:
            kendoGridID = '#v_grid_a';
            break;
        case 2:
            kendoGridID = '#v_grid_b';
            break;
    }
    return $(kendoGridID).data('kendoGrid');
}

function stateChange(checked) {
    if (!checked && getCurrentIndex() == 3) {
        $('#contactCompanyId').combobox('clear');
        $('#contactCompanyId').combobox('disable');
        $('#start').datebox('disable');
        $('#end').datebox('disable');
    } else {
        $('#start').datebox('enable');
        $('#end').datebox('enable');
        $('#contactCompanyId').combobox('enable');
    }
    btn_search();
}

function getCurrentIndex() {
    let tab = $('#tabs').tabs('getSelected');
    let index = $('#tabs').tabs('getTabIndex', tab);
    return index;
}

function btn_material_check(param = {}) {
    param.orderDate = getCurrentDate();
    if (getCurrentIndex() == 1) {
        let kGrid = $("#v_grid_a").data("kendoGrid");
        let dataItem = kGrid.dataItem(kGrid.select());
        if (dataItem == undefined) {
            $.messager.alert('提示', '请选中库存！！', 'warning');
            return;
        }
        param.batchNum = dataItem['batch_num'];
        param.materialSpec = dataItem['material_spec'];
        param.supplierId = dataItem['supplier_id'];
        param.boxAmount = -dataItem['box_amount'];
        param.inputKilo = -dataItem['input_kilo'];
        if ($('#processCompanyInput').length > 0) {
            let _data = $('#processCompanyInput').combobox('getData') || [];
            for (let i of _data) {
                if (i['isSelected']) {
                    let valueField = $('#processCompanyInput').combobox('options').valueField;
                    param.processCompany = i[valueField];
                    break;
                }
            }
        }
    } else {
        return;
    }
    //  原料盘点
    param.id = null;
    param.billType = 'YLPD';
    openReturningDialog(param, 'ADD');
}

function btn_material_returining() {
    //  原料退回
    if (getCurrentIndex() == 0) {
        let kGrid = $("#v_grid").data("kendoGrid");
        let dataItem = kGrid.dataItem(kGrid.select());
        if (dataItem == undefined) {
            $.messager.alert('提示', '请选中！！', 'warning');
            return;
        }
        if (dataItem['bill_type_name'] == '原料退回') {
            $.messager.alert('提示', '不允许从原料退回单据生成！', 'warning');
            return;
        }
        let id = dataItem.id;
        $.ajax({
            url: 'materialOrder/queryByPk/' + id,
            type: 'GET',
            dataType: 'json',
        }).then(function (data) {
            let subData = {...data};
            data.id = null;
            data.billType = 'YLTH';
            data.boxAmount = null;
            data.inputKilo = null;
            openReturningDialog(data, 'ADD', subData);
            $.formDialog.enableBtn(['保存', '关闭']);
        })
    }
}

function btn_process_returining() {
    //  领料退回
    if (getCurrentIndex() == 2) {
        let kGrid = $("#v_grid_b").data("kendoGrid");
        let dataItem = kGrid.dataItem(kGrid.select());
        if (dataItem == undefined) {
            $.messager.alert('提示', '请选中！！', 'warning');
            return;
        }
        if (dataItem['bill_type_name'] == '原料退回') {
            $.messager.alert('提示', '不允许从原料退回单据生成！', 'warning');
            return;
        }
        let id = dataItem.id;
        $.ajax({
            url: 'materialOrder/queryByPk/' + id,
            type: 'GET',
            dataType: 'json',
        }).then(function (data) {
            let subData = {...data};
            data.id = null;
            data.billType = 'LLTH';
            data.boxAmount = null;
            data.inputKilo = null;
            openReturningDialog(data, 'ADD', subData);
            $.formDialog.enableBtn(['保存', '关闭']);
        })
    }
}

function openReturningDialog(loadData, mode, sourceData) {
    const materialOutboundColumns = [
        {title: 'id', field: 'id', hidden: true,},
        {title: '制单日期', field: 'orderDate', editor: {type: 'datebox', options: {value: getCurrentDate()}}},
        {
            title: '单据类型', field: 'billType', hidden: true
        },
        {
            title: '供应商', field: 'supplierId', hidden: true
        },
        {
            title: '原料规格', field: 'materialSpec', hidden: true
        },
        {title: '批号', field: 'batchNum', hidden: true},
        {title: '件数', field: 'boxAmount', editor: {type: 'numberbox', options: {precision: 1, required: false}}},
        {
            title: '重量',
            id: 'inputKiloId',
            field: 'inputKilo',
            editor: {type: 'numberbox', options: {precision: 1, required: true}}
        },
        {
            title: '加工户', field: 'processCompany', id: 'processCompanyInput', hidden: true
        },
        {
            title: '凭证号', field: 'handlerName', hidden: true, editor: {
                type: 'textbox', options: {
                    required: false,
                }
            }
        },
        {title: '单价', hidden: true, field: 'price'},
        {title: '金额', hidden: true, field: 'inputMoney'},
        {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {required: false}}},
        {
            title: '制单人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
    ];
    const materialOutboundModifyColumns = [
        {title: 'id', field: 'id', hidden: true,},
        {title: '制单日期', field: 'orderDate', editor: {type: 'datebox', options: {value: getCurrentDate()}}},
        {title: '单据编号', field: 'orderCode', editor: {type: 'textbox', options: {required: false, readonly: true}}},
        {
            title: '单据类型', field: 'billType', editor: {
                type: 'combobox', options: {
                    url: 'billType/listQuery',
                    queryParams: {storageId: 1},
                    readonly: true,
                    method: 'GET',
                    textField: 'billTypeName', valueField: 'id',
                    panelHeight: 200,
                    required: true,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                        let _data = $(this).combobox('getData') || [];
                        let _opts = $(this).combobox('options');
                        if (_data instanceof Array && !$(this).combobox('getValue') && _data.length > 0) {
                            for (let i of _data) {
                                if (i['isSelected']) {
                                    $(this).combobox('setValue', i[_opts.valueField]);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        },
        {
            title: '原料商', field: 'supplierId', editor: {
                type: 'combobox', options: {
                    url: 'supplier/listQuery',
                    method: 'GET',
                    textField: 'supplierName', valueField: 'id',
                    panelHeight: 200,
                    required: true,
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
        {
            title: '原料规格', field: 'materialSpec', editor: {
                type: 'combobox', options: {
                    url: 'specification/listQuery',
                    method: 'GET',
                    textField: 'specName',
                    valueField: 'specName',
                    panelHeight: 200,
                    required: true,
                }
            }
        },
        {title: '批号', field: 'batchNum', editor: {type: 'textbox', options: {required: true}}},
        {title: '件数', field: 'boxAmount', editor: {type: 'numberbox', options: {precision: 1, required: false}}},
        {
            title: '重量',
            id: 'inputKiloId',
            field: 'inputKilo',
            editor: {type: 'numberbox', options: {precision: 1, required: true}}
        },
        {
            title: '加工户', field: 'processCompany', id: 'processCompanyInput', editor: {
                type: 'combobox', options: {
                    required: false,
                    url: 'processCompany/listQuery',
                    method: 'GET',
                    textField: 'companyName',
                    valueField: 'companyName',
                    onLoadSuccess: function () {
                        let _data = $(this).combobox('getData') || [];
                        let valueField = $(this).combobox('options').valueField;
                        if (!$(this).combobox('getValue'))
                            for (let i of _data) {
                                if (i['isSelected']) {
                                    $(this).combobox('setValue', i[valueField]);
                                    break;
                                }
                            }
                    }
                }
            }
        },
        {
            title: '凭证号',
            field: 'handlerName',
            editor: {
                type: 'textbox', options: {
                    required: false,
                }
            }
        },
        {title: '单价', hidden: true, field: 'price'},
        {title: '金额', hidden: true, field: 'inputMoney'},
        {title: '备注', field: 'remarks', editor: {type: 'textbox', options: {required: false}}},
        {
            title: '制单人',
            field: 'createUserId',
            editor: {
                type: 'combobox',
                options: {
                    readonly: true,
                    url: 'sysUser/query',
                    method: 'GET',
                    textField: 'userName',
                    valueField: 'id',
                    onLoadSuccess: function () {
                        $(this).combobox('options').limitToList = true;
                    }
                }
            }
        },
    ];
    $.formDialog.open({
        columns: mode == 'MODIFY' ? materialOutboundModifyColumns : materialOutboundColumns,
        onOpen: function () {
            $('.form-dialog-edit').find('.form-row:visible').eq(0).css({'margin-top': '10px'});
        },
        loadData: loadData,
        mode: '原料退货',
        width: 459,
        height: 'auto',
        buttons: [
            {
                text: '修改', iconCls: 'icon-edit', handler: e => {
                    $.formDialog.enableBtn(['保存', '关闭']);
                    $.formDialog.enableEditing();
                }
            },
            {
                text: '保存', iconCls: 'icon-save', handler: e => {
                    let url = '';
                    let formData = $('.form-dialog-edit').eq(0).dialog('panel').find('form').serializeObject();
                    if (!formData['inputKilo'] && !formData['boxAmount']) {
                        $.messager.alert('提示', '件数与重量不允许为0!', 'warninig');
                        return;
                    }
                    if (formData['id']) {
                        url = 'materialOrder/updateSelective';
                    } else {
                        url = 'materialOrder/insertSelective';
                    }
                    if (mode == 'ADD' && sourceData != undefined) {
                        if (sourceData['inputKilo'] < formData['inputKilo']) {
                            $.messager.confirm('提示', '超出出库数，是否继续？', function (r) {
                                if (r) {
                                    ex();
                                }
                            })
                        } else {
                            ex();
                        }
                    } else {
                        ex();
                    }

                    function ex() {
                        $('.form-dialog-edit').eq(0).dialog('panel').find('form').form('submit', {
                            url: url,
                            success: function (data) {
                                data = eval(`(${data})`);
                                if (data.success) {
                                    $.formDialog.disableBtn(['保存']);
                                    $.formDialog.disableEditing();
                                    $('.form-dialog-edit').eq(0).dialog('panel').find('form').form('load', data.data);
                                    btn_search();
                                    $.messager.show({title: '提示', msg: data.message});
                                } else {
                                    $.messager.alert('提示', data.message, 'error');
                                }
                            }
                        });
                    }

                }
            },
            {
                text: '作废', iconCls: 'icon-cancel', handler: e => {
                    $.messager.confirm('提示', '是否作废？', function (r) {
                        if (r) {
                            let formData = $('.form-dialog-edit').find('form').serializeObject();
                            if (!formData['id']) {
                                $.messager.alert('提示', '单据未保存!', 'warning');
                                return;
                            }
                            $.ajax({
                                url: 'materialOrder/cancelByPk',
                                data: {id: formData.id},
                                type: 'POST',
                                dataType: 'json',
                                success: function (data) {
                                    if (data.success) {
                                        $.formDialog.close();
                                        btn_search();
                                        $.messager.show({title: '提示', msg: data.message});
                                    } else {
                                        $.messager.alert('提示', data.message, 'error');
                                    }
                                }
                            });
                        }
                    })
                }
            },
            {
                text: '关闭', iconCls: 'icon-no', handler: e => {
                    $('.form-dialog-edit').eq(0).dialog('close');
                }
            }]
    })
}

function enableBtn(arr) {
    let ele = $('.' + cls).eq(0);
    $('#north').find('.easyui-linkbutton').each(function () {
        let btnOpts = $(this).linkbutton('options');
        if (arr.indexOf(btnOpts.text) >= 0) {
            $(this).linkbutton('enable');
        } else {
            $(this).linkbutton('disable');
        }
    })
}

//选中事件
function initCombobox(options) {
    var columns = options.columns;
    columns.splice(0, 1);
    $('#column').combobox({
        valueField: 'field',
        textField: 'title',
        data: columns,
        editable: false
    })
}