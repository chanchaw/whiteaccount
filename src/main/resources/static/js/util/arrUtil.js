/**
 *  数组操作方法工具类
 */


/**
 * 将数组去重
 * @param arr
 * @param attribute
 * @returns {[]}
 */
function unique(arr, attribute) {
    let new_arr = [];
    let json_arr = [];
    for (let i = 0; i < arr.length; i++) {
        if (new_arr.indexOf(arr[i][attribute]) == -1) {
            new_arr.push(arr[i][attribute]);
            json_arr.push(arr[i]);
        }
    }
    return json_arr;
}

/**
 * 计算合计
 * @param rows  rows
 * @param field 列
 */
function calculateSum(rows, field) {
    if (rows instanceof Array && rows.length > 0) {
        return rows.map(item => item[field] ? parseFloat(item[field]) : 0).reduce((prev, curr) => prev + curr, 0);
    }
    return 0;
}

/**
 * 获取Form数据
 * @param form
 */
function getFormData(form) {
    let formObj = {};
    let o = $(form).serializeArray();
    for (let i of o) {
        formObj[i['name']] = i['value'];
    }
    return formObj;
}

/**
 * 数组排序
 * @param arr
 * @param oldIndex
 * @param newIndex
 * @returns {*}
 */
function reorder(arr, oldIndex, newIndex) {
    if (oldIndex == newIndex) return arr;
    if (oldIndex > newIndex) {
        arr.splice(newIndex, 0, arr[oldIndex])
        arr.splice(oldIndex + 1, 1);
    } else {
        arr.splice(newIndex, 0, arr[oldIndex]);
        arr.splice(oldIndex - 1, 1);
    }
    return arr;
}

/**
 * 装载kendoGrid网格
 * @param grid01        dom
 * @param columnsName   列字段名
 * @param columns       定义的列属性数组
 * @param suffix        后缀
 */
function loadOptions(grid01, columnsName, columns, suffix) {
    //后端获取记忆列
    $.ajax({
        url: 'deploy/query.do',
        traditional: true,
        type: 'POST',
        dataType: "json",
        data: {menuId: menuId, name: columnsName},
        success: function (data) {
            if (!data) {
                $.messager.alert('提示', '无法加载网格配置,请重新打开页面!', 'warning')
                return;
            }
            for (let x of columns) {
                let col = x['field'];
                if (suffix)
                    col += suffix;
                if (data[col] == undefined) continue;
                let width = data[col]['width'];
                let hidden = data[col]['hidden'];
                x['width'] = width ? parseFloat(width) : 0;
                x['hidden'] = hidden;
                x['serialNumber'] = data[col]['serialNumber'];
            }
            columns.sort((x, y) => x.serialNumber - y.serialNumber);
            grid01.setOptions({
                columns: columns
            });
            if (typeof initCombobox == 'function')
                initCombobox(grid01.getOptions())
        },
        error: function () {
            grid01.setOptions({
                columns: columns
            });
        }
    });
}

/**
 * 读取网格列配置
 * @param element   dom元素
 * @param menuId    菜单编号
 */
function getColumnsConfiguration(element, menuId, suffix = '') {
    if ($(element).length <= 0) return;
    let opts = $(element).datagrid('getColumnFields');
    if (opts.length <= 0) return;
    $.ajax({
        url: "deploy/query.do",
        data: {menuId: menuId, name: opts.map(item => item + suffix)},
        dataType: "json",
        type: "post",
        traditional: true,
        success: function (data) {
            if (data) {
                for (let i of opts) {
                    let col = $(element).datagrid('getColumnOption', i);
                    let configItem = data[i + suffix];
                    if (!configItem) continue;
                    if (configItem['width'] != undefined && configItem['width'] != null)
                        col.width = configItem['width'];
                    if (configItem['hidden']) {
                        $(element).datagrid('hideColumn', i);
                    } else {
                        $(element).datagrid('showColumn', i);
                    }
                }
                $(element).datagrid();
            }
        }
    });
}