/**
 Copyright 2021年4月16日 张柯

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
(function ($) {
    $.extend($.fn.datagrid.defaults, {
        onReorderStart: function (startIndex, options) {
            /*
             * 拖拽开始事件,如果返回false则提前结束拖拽
             * @param startIndex    起始索引
             * @param options       当前网格配置
             */
            return true;
        },
        onReorderEnd: function (startIndex, endIndex, success, options) {
            /*
             * 拖拽结束事件
             * @param startIndex    起始索引
             * @param endIndex      终止索引
             * @param success       是否拖拽成功
             * @param options       当前网格配置
             */
        }
    })
    $.extend($.fn.datagrid.methods, {
        enableReorder: function (jq) {
            //  开启拖拽列功能
            console.log($(jq).data('datagrid').dc.header2);
            $(jq).data('datagrid').dc.header2.find('tr .datagrid-cell').each(function () {
                $(this).draggable({
                    proxy: 'clone',
                    revert: true,
                    cursor: 'pointer',
                    edge: 5,
                    onBeforeDrag: function (e) {
                        let options = $(jq).data('datagrid').options;
                        let field = $(this).parent().attr('field');
                        return options.onReorderStart.call(this, getFieldIndex(options, field), options);
                    }
                }).droppable({
                    onDrop: function (e, source) {
                        let options = $(jq).data('datagrid').options;
                        let endField = $(this).parent().attr('field');
                        let startField = $(source).parent().attr('field');
                        options.onReorderEnd.call(this, getFieldIndex(options, startField), getFieldIndex(options, endField), true, options);
                        let columns = options.columns[0];
                        if (reorder(columns, getFieldIndex(options, startField), getFieldIndex(options, endField))) {
                            let view = $(jq).data('datagrid').dc;
                            //  修改视图
                            console.log(view);
                        }
                    },
                    onDragEnter: function (e, source) {
                        //  移入溢出样式
                    },
                    onDragLeave: function (e, source) {

                    }
                })
            });
        },
        disableReorder: function (jq) {
            //  关闭拖拽功能
            $(jq).data('datagrid').dc.header2.find('tr .datagrid-cell').each(function () {
                $(this).draggable('disable');
                $(this).droppable('disable');
            })
        }
    })

    //  获取当前索引
    function getFieldIndex(options, field) {
        if (options['columns'] == undefined) return -1;
        let columns = options['columns'][0];
        let _index;
        if (columns == undefined) {
            _index = -1;
        } else {
            _index = columns.filter(item => item['field'] != undefined).map(item => item['field']).indexOf(field);
        }
        return _index;
    }

    //  重新排序列
    function reorder(columns, sourceIndex, targetIndex) {
        if (columns == undefined || !(columns instanceof Array)) return false;
        let sourceItem = columns.splice(sourceIndex, 1);
        columns.splice(sourceIndex >= targetIndex ? targetIndex : targetIndex - 1, 0, sourceItem[0]);
        return true;
    }
})(jQuery)