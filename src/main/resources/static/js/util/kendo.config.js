kendo.ui.FilterMenu.fn.options.operators.string = {
    contains: '包含',
    doesnotcontain: '不包含',
    eq: '等于',
    neq: '不等于',
};
kendo.ui.FilterMenu.fn.options.operators.number = {
    gte: "大于等于",
    gt: '大于',
    lte: "小于等于",
    lt: '小于',
    eq: '等于'
};