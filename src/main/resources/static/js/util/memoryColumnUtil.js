/**
 * 设置Easy-ui记忆列宽
 * @param sysMenuId
 * @param e
 * @param columns
 * @param fn
 * @param suffix
 */
function setDatagridColumnOpts(sysMenuId, e, columns, fn, suffix = '') {
    let handlerColumns = []
    if (!columns) {
        handlerColumns = $(e).datagrid('options').columns[0];
    } else {
        handlerColumns = columns[0];
    }
    let sending = JSON.parse(JSON.stringify(handlerColumns)) || [];
    $.ajax({
            url: 'sysUserTableConfig/calculate',
            data: JSON.stringify(sending.map(item => {
                item.sysMenuId = sysMenuId;
                item.field = item.field + suffix;
                return item;
            })),
            type: 'POST',
            async: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data) {
                    for (let i of handlerColumns) {
                        let obj = data[i.field + suffix];
                        if (typeof obj == 'object' && Object.keys(obj).length > 0) {
                            i.width = obj['width'] || 10;
                            i.hidden = obj['hidden'] || false;
                        }
                    }
                    if (fn == undefined) {
                        $(e).datagrid('options').columns = [handlerColumns];
                        $(e).datagrid();
                    } else {
                        fn([handlerColumns]);
                    }
                }
            }
        }
    )
}

/**
 * 后端修改列
 * @param sysMenuId
 * @param field
 * @param width
 * @param hidden
 * @param serialNum
 * @param suffix
 */
function columnResizeEvent(sysMenuId, field, width, hidden, serialNum, suffix = '') {
    $.ajax({
        url: 'sysUserTableConfig/modify/column',
        type: 'PUT',
        data: {
            field: field + suffix,
            width: width || null,
            hidden: hidden == undefined ? null : hidden,
            sysMenuId: sysMenuId || null,
            serialNum: serialNum || null
        },
        dataType: 'json',
        success: function (data) {
        }
    })
}

/**
 * 装载kendoGrid网格
 * @param grid01        dom
 * @param columns       定义的列属性数组
 * @param suffix        后缀
 * @param menuId        菜单编号
 */
function loadOptions(grid01, columns, suffix, menuId) {
    let newColumns = [...columns];
    newColumns = newColumns.map(x => {
        x = {...x};
        x.sysMenuId = menuId;
        if (suffix) {
            x.field = x['field'] + suffix;
        }
        return x;
    });
    //后端获取记忆列
    $.ajax({
        url: 'sysUserTableConfig/calculate',
        type: 'POST',
        dataType: "json",
        contentType: 'application/json',
        data: JSON.stringify(newColumns),
        success: function (data) {
            if (!data) {
                $.messager.alert('提示', '无法加载网格配置,请重新打开页面!', 'warning')
                return;
            }
            for (let x of columns) {
                let col = x['field'];
                if (suffix)
                    col += suffix;
                if (data[col] == undefined) continue;
                let width = data[col]['width'];
                let hidden = data[col]['hidden'];
                x['width'] = width ? parseFloat(width) : 0;
                // if (x['hidden'] != true)
                x['hidden'] = hidden;
                x['serialNumber'] = data[col]['serialNum'];
            }
            columns.sort((x, y) => x.serialNumber - y.serialNumber);
            grid01.setOptions({
                columns: columns
            });
        },
        error: function () {
            grid01.setOptions({
                columns: columns
            });
        }
    });
}

/**
 * 重置
 * @param sysMenuId
 */
function resetColumnOptions(sysMenuId, success) {
    $.ajax({
        url: 'sysUserTableConfig/reset/all',
        data: {sysMenuId: sysMenuId},
        type: 'PUT',
        dataType: 'json',
        success: success
    })
}