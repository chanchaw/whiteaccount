(function ($) {
    const methods = {
        open: function (param) {
            if (param != undefined && typeof param == 'object') {
                $(this).data('opts', $.extend({}, $(this).data('opts'), param));
            }
            $(this).dialog('open');
        },
        close: function () {
            $(this).dialog('close');
        },
        destroy: function () {
            //  销毁方法
            let opts = $(this).data('opts');
            if (opts.onBeforeDestroy.call(this)) {
                $(this).remove();
            }
        },
        options: function () {
            //  返回配置表
            return $(this).data('opts') || {};
        },
        submit: function () {
            if (!$(this).find('form').eq(0).form('validate')) {
                $.messager.show({title: '提示', msg: '请输入完整'})
                return;
            }
            let formData = new FormData($(this).find('form').eq(0)[0]);
            let opts = $(this).data('opts');
            $.messager.progress({title: '正在提交...', interval: 50000});
            $.ajax({
                url: opts.url,
                data: formData,
                type: opts.method,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: opts.onSuccess,
                error: opts.onError,
                complete: opts.complete,
            })
        }
    }
    const defaultOptions = {
        fields: [],
        url: '',
        method: 'POST',
        onSuccess: function (data) {
            if (data.success) {
                $('#uploadFileDialog').dialog('close');
                $.messager.show({title: '提示', msg: data.message});
            } else {
                $.messager.alert('提示', '上传失败!', 'error');
            }
        },
        complete: function () {
            $.messager.progress('close');
        },
        onError: function () {
            $.messager.alert('提示', '上传失败!', 'error');
        },
        onBeforeDestroy: function () {
            return true;
        }
    }
    $.extend({
        uploadComponent: function (options, param) {
            if (typeof options == "object") {
                let opts = $.extend({}, defaultOptions, options);
                let div = $(`
            <div id="uploadFileDialog"
                 class="easyui-dialog"
                 data-options="title:'导入向导',
                 iconCls:'icon-edit',
                 width:420,
                 height:400,
                 modal:true,
                 resizable:true,
                 closed:true,
                 buttons:[{text:'取消',iconCls:'icon-cancel',
                 onClose:function(){
                    $(this).find('form').eq(0).form('load',{
                        file: null
                    })
                 },
                 handler:function(){
                        $('#uploadFileDialog').dialog('close');
                 }},
                 {text:'导入',iconCls:'icon-save',handler:function(){
                        $.uploadComponent('submit');
                 }}]
">
    <form method="post" style="width: 100%;height: 100%;">
        <div class="easyui-layout" data-options="fit:true">
            <div data-options="region:'west',width:210,title:'Excel列对应序号(起始为0)'">
             <div class="upload-setting">
               <ul id="uploadFileDialogUl"/>
             </div>   
            </div>
            <div data-options="region:'center',title:'配置'">
                <div class="upload-setting">
                    <ul>
                        <li><input name="from" class="easyui-numberbox"
                                   data-options="precision:0,value:0,min:0,required:true,label:'起始行',labelWidth:65"></li>
                        <li><input name="to" class="easyui-numberbox"
                                   data-options="precision:0,label:'结束行',min:0,labelWidth:65"></li>
                        <li><input name="sheetIndex" class="easyui-numberbox"
                                   data-options="precision:0,label:'sheet序号',min:0,value:0,labelWidth:65"></li>
                         <li><input name="len" class="easyui-numberbox"
                                   data-options="precision:0,label:'BOM最大值',min:0,labelWidth:65,value:19"></li>                  
                        <li><input name="file" class="easyui-filebox"
                                   data-options="buttonText:'选择文件',required:true,buttonAlign:'right',label:'Excel文件'"></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
            `);
                if ($('#uploadFileDialog').length <= 0) {
                    let container = $(`<div></div>`)
                    div.appendTo(container);
                    $('body').append(container);
                    if (opts.fields.length > 0) {
                        let newFields = opts.fields.filter(item =>
                            item['hidden'] == undefined || !item['hidden']
                        );
                        for (let i = 0; i < newFields.length; i++) {
                            $('#uploadFileDialogUl').append(`
                            <li>
                                <input name="${newFields[i].field}" 
                                class="easyui-numberbox" 
                                data-options="label:'${newFields[i].title}',precision:0,min:0,required:false">
                            </li>
                            `)
                        }
                    }
                    $.parser.parse(container);
                }
                $('#uploadFileDialog').data('opts', opts);
            } else if (typeof options == "string") {
                if ($('#uploadFileDialog').length <= 0)
                    throw new Error("未初始化组件!");
                let fn = methods[options];
                if (fn == undefined) {
                    throw new Error("方法不存在");
                }
                fn.call($('#uploadFileDialog'), param);
            }
        }
    })
})(jQuery)