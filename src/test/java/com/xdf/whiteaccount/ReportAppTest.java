package com.xdf.whiteaccount;

import com.xdf.whiteaccount.service.MaterialOrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

/**
 * @program: white-account
 * @description:
 * @author: 张柯
 * @create: 2021-07-19 14:24
 **/
//@SpringBootTest
public class ReportAppTest {
    @Autowired
    private MaterialOrderService service;

    @Test
    public void test() {
        List<Map<String, Object>> list = service.getMaterialStorageDropdownList("", null, 0);
        System.out.println(list.toString());
    }
}
