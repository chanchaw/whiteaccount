package com.xdf.whiteaccount;

import com.xdf.whiteaccount.config.datasource.DynamicDataSource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ReqTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;// 伪造 mvc 运行环境

    // 在测试用例执行之前就执行本方法
    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void get() throws Exception {
//        DynamicDataSource ds = DynamicDataSource.getInstance();
//        ds.switchDataSource("齐力达针纺");

        String url = "/machineType/listQuery";
        MockHttpServletResponse res = mockMvc.perform(MockMvcRequestBuilders.get(url)//发送 get 请求
                //.param("username", "jojo")// 发送的请求中带名称为 username 的参数
                .contentType(MediaType.APPLICATION_JSON_UTF8))// 要求是 json
                .andExpect(MockMvcResultMatchers.status().isOk())// 要求返回200
                .andReturn().getResponse();
        // 获取 response 后设置字符集然后返回
        res.setCharacterEncoding("UTF-8");
        String ret = res.getContentAsString();
        System.out.println(ret);
    }
}