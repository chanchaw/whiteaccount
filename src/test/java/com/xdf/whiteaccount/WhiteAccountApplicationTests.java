package com.xdf.whiteaccount;

import com.xdf.whiteaccount.async.WechatPushNtfBody;
import com.xdf.whiteaccount.async.properties.WechatProp;
import com.xdf.whiteaccount.async.util.WechatPushBodyBuilder;
import com.xdf.whiteaccount.entity.WechatBody;
import com.xdf.whiteaccount.qrcodeutil.Result;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SpringBootTest
class WhiteAccountApplicationTests {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private WechatProp wechatProp;

//    @Test
    void contextLoads() {
        WechatPushNtfBody body = new WechatPushNtfBody();
        body.setTemplateId(wechatProp.getTemplateId());
        List<String> openIdList = new ArrayList<>();
        openIdList.add("oSIC352XEwRjxdMbyRs5cFKV8FZk");
//        openIdList.add("oSIC35zpBotNrNd2er5FUFcMSbAM");
        body.setOpenidList(openIdList);
        WechatBody wechatBody = new WechatBody();
        wechatBody.setA("");
        wechatBody.setB("");
        wechatBody.setC("");
        wechatBody.setD("");
        wechatBody.setE("");
        wechatBody.setF("");
        wechatBody.setG("");
        wechatBody.setH("https://www.jzy.world/wxfe/wx_permission.html");
        body.setMetaList(getWechatBodyFirstParam(wechatBody));
        System.out.println(body.toString());
        ResponseEntity<Result> result =
                restTemplate.postForEntity(wechatProp.getUrl(), body, Result.class);
        Result r = result.getBody();
        System.out.println(r.toString());
    }

    private static List<Map<String, String>> getWechatBodyFirstParam(WechatBody item) {
        List<Map<String, String>> param = new ArrayList<>();
        param.add(WechatPushBodyBuilder.buildMap(item.getA()));
        param.add(WechatPushBodyBuilder.buildMap(item.getB()));
        param.add(WechatPushBodyBuilder.buildMap(item.getC()));
        param.add(WechatPushBodyBuilder.buildMap(item.getD()));
        param.add(WechatPushBodyBuilder.buildMap(item.getE(), "#FF1722"));
        param.add(WechatPushBodyBuilder.buildMap(item.getF()));
        param.add(WechatPushBodyBuilder.buildMap(item.getG()));
        param.add(WechatPushBodyBuilder.buildMap(item.getH()));
        return param;
    }

}
